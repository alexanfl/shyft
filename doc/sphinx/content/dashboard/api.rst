***************
Dashboard
***************


This package contains the following classes and functions.

.. note::
    
    This is the complete exposed API. 
    Not all classes and functions are meant for the End-user


shyft.dashboard
===============

.. autosummary::
   :toctree: _autosummary
   :recursive:

   shyft.dashboard.base
   shyft.dashboard.time_series
   shyft.dashboard.graph
   shyft.dashboard.maps
   shyft.dashboard.widgets
   shyft.dashboard.util

.. autosummary::
   :toctree: _autosummary

   shyft.dashboard


Start bokeh apps
================
.. autofunction:: shyft.dashboard.apps.start_bokeh_apps.start_bokeh_apps


