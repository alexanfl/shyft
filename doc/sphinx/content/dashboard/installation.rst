.. _PackageDashboard:

***********************
Package shyft.dashboard
***********************

The Python module ``shyft.dashboard`` provides `Bokeh <https://bokeh.org/>`_ based generic web dashboard components
to display time series.

The module is currently bundled together with shyft:

1. using **pip** from `PyPI <https://pypi.org/project/shyft/>`_

   .. code-block:: bash

      pip install shyft

2. using **anaconda/conda** from the `Shyft channel <https://anaconda.org/shyft-os>`_

   .. code-block:: bash

      conda install -c shyft-os shyft



