.. _PackageEnergyMarket:

***************************
Package shyft.energy-market
***************************

The Python module ``shyft.energy-market`` provides the classes and functions required to model an energy market with
its underlying forces of production and consumption.

The module is currently bundled together with shyft:

1. using **pip** from `PyPI <https://pypi.org/project/shyft/>`_

   .. code-block:: bash

      pip install shyft

2. using **anaconda/conda** from the `Shyft channel <https://anaconda.org/shyft-os/>`_

   .. code-block:: bash

      conda install -c shyft-os shyft

