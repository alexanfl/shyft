*************
Energy Market
*************

.. note::
    
    This is the complete exposed API.
    Not all classes and functions are meant for the End-user, the documentation is a work in progress


Core
####
.. automodule:: shyft.energy_market.core
    :members:
    :show-inheritance:

STM
###
The detailed energy market model contains all essential energy market objects, entities, that
Shyft manages.


For example,  power plants with units, unit-groups, reservoirs, waterways, market areas, power-modules, contracts, contract portfolios,
network with transmission lines and bus-bars for electrical connections between the elements.


The module also provides ready made services, with clients and api's, in python, as well as web/socket protocols for
setting up a complete application system covering the energy management process tasks.

The design and architecture of the system utilizes the energy market models, combined with the `DTSS`,
the distributed time-series services, and provides a model-driven approach with time-series expressions derived
from the model, so that it scales and maintains well.

The Task Services also support the workflow that are operating on StmSystem, to be efficiently orchestrated, including
running optimization tasks with Sintef SHOP engine. Generic python callback allows extensions to arbitrary tasks that
can be performed using python.

The UI Services and classes completes the Task Services with a a framework for model and data-driven front end.,  that fits the task-workflow, showing relevant data/information at the workflow process.
E.g: if you are planning a power plant, you can configure the Task and UI service to provide a suitable view, e.g. chart/tables
with data-bindings, that works with the backend services, in real time.

The test-suites of Shyft for the energy-market includes ready made examples that includes starting up the services,
adding energy-market models, running SHOP optimization and getting results back for testing/regressions.

E.g:
https://gitlab.com/shyft-os/shyft/-/blob/master/python/test_suites/energy_market/stm/shop/test_dstm_with_shop.py#L114


.. automodule:: shyft.energy_market.stm
    :members:
    :show-inheritance:

STM Compute and Shop
####################
A scalable compute node service, currently implementing the running Sintef SHOP optimizations.
It is automatically managed through the shyft.energy_market.stm.DStmServer class.

.. note::
    This module require license from Sintef, and that you have a in-house build setup
    according to recipe as described on Shyft wiki:
    https://gitlab.com/shyft-os/shyft/-/wikis/Reference-docs/Shyft-for-SHOP

.. automodule:: shyft.energy_market.stm.compute
    :members:
    :show-inheritance:

STM Shop
########

This module provide the direct interface to emit StmSystem to the Shop engine, running commands etc.

.. note::
    This module require license from Sintef, and that you have a in-house build setup
    according to recipe as described on Shyft wiki:
    https://gitlab.com/shyft-os/shyft/-/wikis/Reference-docs/Shyft-for-SHOP

.. automodule:: shyft.energy_market.stm.shop
    :members:
    :show-inheritance:

Service
#######
This module contains scripts and setup for starting up the Shyft energy market model services.

.. automodule:: shyft.energy_market.service
    :members:
    :show-inheritance:

UI
#######

The 'UI' module utilizes pyside/Qt to express web layouts, json-formatted information,
that is feed back the front-end.

The sections of the layout can be filled with charts, tables etc,
and where the data-binding of the content of those, can be setup to reflect
back to the Shyft energy market servers, like the 'DStmServer', 'DtsServer', 'TaskServer' etc.

Note that currently Shyft does not currently contain a  front end framework needed to render this
on web, but the examples/demo section of the Shyft repository to contain angular examples using
the Shyft services.

There are example that companies have created such front end in angular, data-driven, using these services,
to support intraday and day-ahead market operations.

The basic idea here is to enable the cooperation of python skilled business users and professional software developers
to work together efficiently.

The professional/dedicated software developers provide the tools/framework as needed, and the python skilled business
users can then utilize their domain knowledge to orchestrate and provide operational software.

Thus, the goal is ensures fast time-to-useful software in operations, combining all the skills, energy market domain,
front end/ux, and backend high performance services.

Since we currently uses Qt to express the UI layouts, there is no explicit doc for this, as it is standard
documentation of Qt that applies to this part.

Note that the Qt/pyside2 is only used as a robust well documented tool to create data structures,
from where we harvest/generate a json, using the attributes of Qt.

Any other framework, including python could do the job as well, as long as it at the end, generates
the json formatted as required from the front end framework that you use.

To get an idea for how this works, with PySide2/Qt, look into the test_suites for shyft,
https://gitlab.com/shyft-os/shyft/-/blob/master/python/test_suites/energy_market/ui/test_ui.py#L35


.. automodule:: shyft.energy_market.ui
    :members:
    :show-inheritance:
