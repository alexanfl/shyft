.. _TutorialTimeSeries:

TimeSeries
==========

After the successful creation of a :ref:`UsageTimeAxis`, a Shyft ``TimeSeries`` can be 
instantiated. In Shyft we see the ``TimeSeries`` as a function that can be evaluated at any time 
point ``f(t)``. If we evaluate the ``TimeSeries`` outside the time axis range, it will
return ``NaN``. Inside the defined intervals of the time axis,  it will interpolate between values, how it will
be interpolated will depend on what type of ``TimeSeries`` we use.

A concrete time series can be instantiated by giving a ``TimeAxis``, ``ta``, a set of values, ``values``,
and the point interpretation, ``point_fx``, you want the time series to have.
The available point interpretations are ``POINT_INSTANT_VALUE`` and ``POINT_AVERAGE_VALUE``.
These are available in ``shyft.time_series.point_interpretation_policy``.

As mentioned above, the time series value f(t) is defined by its numbers and point-interpretation within the ``TimeAxis`` total_period,
and is ``NaN`` outside this definition interval.

From this it mathematically follows that a binary operation between two time-series will
yield a time series with at time axis covering the intersection of the two operands.
Thus the result will also be ``NaN`` outside the resulting time axis.


For all time-series representation, we strongly recommend to use un-prefixed SI-units.

E.g: W (Watt) not MW (MegaWatt)


This greatly simplify doing math using formulas, and also works well with integral, and derivative functions.
Avoid using anything but un-prefixed SI-units until presentation level (UI, or export to 3rd party system interfaces).

Recall that `time` in shyft is simply a number, SI-unit is `s` (Seconds).

Time series types
"""""""""""""""""

``POINT_INSTANT_VALUE``
^^^^^^^^^^^^^^^^^^^^^^^

A ``POINT_INSTANT_VALUE`` is a time series where a value is linearly interpolated between
the start and end points of each interval. So the term ``linear between points`` captures an important implication
of this time-series: it requires at a minimum two points to define that line.

This representation is useful for signals that represents 'state', like observed water level at a point in time.

So if we create a 4 interval time axis and input the values ``[0, 3, 1, 4]`` as shown below

.. literalinclude:: code/time_series_intro.py
   :language: python
   :lines: 1-12

it will produce a time series on the form

.. code-block:: python
                   
                t3_____t4
          t1     /
          /\    /
         /  \  /
        /    \/
    t0 /     t2

and evaluating the time series at different time points we can see how it 
interpolates between the points.

.. literalinclude:: code/time_series_intro.py
   :language: python
   :lines: 14-18

.. note:: 

    Worth noticing is that in the last time interval it extrapolates the last value
    as a straight line since it does not have a last value to interpolate to.
    

``POINT_AVERAGE_VALUE``
^^^^^^^^^^^^^^^^^^^^^^^

A ``POINT_AVERAGE_VALUE`` is a time series type where the whole interval has 
the same value. It is typically used to represent signals that are constant over a time interval.
Like effect produced [W], or water-flow [m3/s].

.. literalinclude:: code/time_series_intro.py
   :language: python
   :lines: 20-24

.. code-block:: python

                   t3______t4
         t1_____    |
           |   |    |
           |   |    |
           |   |____|
    t0_____|   t2  


And evaluating the average time series at the same points as the instant series
shows the differences between their interpolation

.. literalinclude:: code/time_series_intro.py
    :language: python
    :lines: 27-31


Inspection functions
""""""""""""""""""""

To inspect the time series there exists a few utility functions we should
know about.

TimeSeries(t: int/float/time)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If we call the time series with an ``int``, ``float`` or ``time`` object it will
evaluate itself on the specific time point and return the value.

.. literalinclude:: code/time_series_intro.py
    :language: python
    :lines: 36-45

point_interpretation()
^^^^^^^^^^^^^^^^^^^^^^

Returns the point interpretation of the time series.

.. literalinclude:: code/time_series_intro.py
    :language: python
    :lines: 47-47

size()
^^^^^^

Returns the number of intervals in the time series

.. literalinclude:: code/time_series_intro.py
    :language: python
    :lines: 49-49

time_axis
^^^^^^^^^

Returns the time axis of the time series

.. literalinclude:: code/time_series_intro.py
    :language: python
    :lines: 51-51

values.to_numpy()
^^^^^^^^^^^^^^^^^

Returns a numpy array with the values it was set up with

.. literalinclude:: code/time_series_intro.py
    :language: python
    :lines: 53-53


General time series manipulation
""""""""""""""""""""""""""""""""
For a comprehensive list of available functions see :meth:`shyft.time_series.TimeSeries`.
The time series in Shyft are thought of as mathematical expressions and not indexed values.
This makes the manipulation of time series a bit different than with numpy arrays or pandas series.
We set up some convenience functions to inspect the time series.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 1-9

Arithmetics
^^^^^^^^^^^
Doing basic arithmetics with time series is as simple as with numbers.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 11-34

We can also do the same arthmetic on two time series

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 35-47

One thing to notice when we do arithmetics on time series is if they have different 
time axis. So if we for example have two time series with partial overlap in time, 
only overlapping time intervals will be defined.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 48-103

Since ``ts2`` is not defined in the intervals 1 to 11, the resulting time series
will not be defined in that period either. The same goes for where ``ts1`` is not defined.


Mathematical operations
^^^^^^^^^^^^^^^^^^^^^^^
There are many built-in functions to help with manipulating time series in Shyft. These examples are
not exhaustive, so please refer to the documentation on :meth:`shyft.time_series.TimeSeries` for a complete
list.

Average
+++++++
The average function takes only one argument, the time axis. The resulting expression (still a time-series), yields
the true average over the time periods of that time axis.

The value of each time period interval of the resulting time-series is equal to the true average of the non-nan
sections of that interval. E.g. the i'th interval, ranging from ``period(i).start`` to ``period(i).end``


.. math::
    \begin{align*}
    TS(t_i) &=  \frac{1}{p_{notnan}(i)}\int_{t=period(i).start}^{t=period(i).end}ts(t)*dt\\
    \text{where}&\\
    TS &= \text{true average time series}\\
    ts &= \text{original time series}\\
    p_{notnan}(i) &= \text{The period in seconds, where ts(t) is not nan}\\
    \end{align*}

Example: We create two time axis, both with a period of a week,
but one with daily resolution and one with hourly resolution. Then we create two hourly time series,
one stair case and one linear, with linearly increasing values. After that we average both of them
with the daily time axis.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 106-131

.. note::
    
    The point interpretation of a time series that is created from an averaging will always be
    a stair case series, as per definition: it represents the true average of that interval.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 133-136

Accumulate
++++++++++
Accumulate takes a time axis as input and returns a new time series where the i'th value
is the integral of non-nan fragments from ``t0`` to ``ti``.

.. math::
    \begin{align*}
    TS(t_i) &= \int_{t=t_0}^{t=t_i}ts(t)dt\\
    \text{where}&\\
    TS &= \text{Accumulated time series}\\
    ts &= \text{original time series}\\
    \end{align*}

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 138-149

.. note::
    Integral operations on shyft time series are done with a ``dt`` of seconds, which is the SI unit for time.
    It implies that if the source unit of the time-series is W (watt), and you integrate, or accumulate it,
    it gives the correct unit of Ws -> J/s x s -> J (Joule).


Derivative
++++++++++
We can compute the derivative of a time series forwards, backwards or center. As with accumulate the operations
happen on second resolution so the resulting time-unit is pr standard SI system.

E.g.: if you have a time series with SI-unit J (Joule), and apply the .derivative() function,
the resulting time-unit will accordingly be J/s (Joule pr second), e.g. W (Watt).


.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 151-161


Integral
++++++++
We can integrate a time series over a specified time axis. As for the average function, it works
with the non-nan section of each interval of the time axis.

.. math::
    \begin{align*}
    TS(T) &= \int_{t=T_{\text{start}}}^{T_{\text{end}}}ts(t)dt && \text{if } ts(t) \neq NaN\\
    \text{where}&\\
    TS &= \text{integrated time series}\\
    ts &= \text{original time series}\\
    T &= \text{period to integrate over}\\
    \text{note: the value computed is for the non-nan sections of the interval}\\
    \end{align*}

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 163-165

Statistics
++++++++++
With the statistics function we can directly get the different percentiles over a specified
time axis.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 167-177


Utility functions
^^^^^^^^^^^^^^^^^
Here is a small collection of helpful functions when manipulating or extracting information
from time series.

Inside
++++++
This function creates a new time series with values where it is either inside or outside a 
defined range. We can set the minimum and maximum value of the range, the value it should use where
it  meets ``NaN``, and also the values to set where it is inside or outside the range.
By default ``NaN`` will continue to be ``NaN``, inside range will be 1 and outside range 0.

.. math::
    \begin{equation*}
    TS(t) =
    \begin{cases}
    1, &\text{if } \text{min_v} \leq ts(t) < \text{max_v}\\
    0 &\text{otherwise}
    \end{cases}
    \end{equation*}

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 180-191

To have no upper or lower limit we set the ``min_v`` or ``max_v`` to ``NaN``.

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 193-195

Here we check if values are inside the range 25-65 and set map inside values to 10 and outside values
to 20.

Max/min
+++++++
These function returns a new time series with filled in values of whichever value that is 
maximum/minimum of the input value or value in the time series.

.. math::
    \begin{equation*}
    max_v = 10\\
    TS(t) = 
    \begin{cases}
    10, &\text{if } ts(t) \leq 10\\
    ts(t) &\text{if } ts(t) \gt 10
    \end{cases}
    \end{equation*}
    

.. math::
    \begin{equation*}
    min_v = 10\\
    TS(t) = 
    \begin{cases}
    ts(t), &\text{if } ts(t) \leq 10\\
    10 &\text{if } ts(t) \gt 10
    \end{cases}
    \end{equation*}
    
.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 197-203

Time shift
++++++++++
This function shifts the values forward or backward in time on the basis of a ``dt``.
It moves forward for positive time step and backwards for negative time step. 

.. literalinclude:: code/time_series_expressions.py
    :language: python
    :lines: 205-214

As we can see here the values stay the same, but the time axis has been shifted an hour forward.

