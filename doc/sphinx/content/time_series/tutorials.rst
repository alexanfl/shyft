*********
Tutorials
*********

:ref:`setup-shyft`

This section contains practical examples of how to use the types and functions provided
in this package.

.. toctree::
    :maxdepth: 1

    tutorials/time.rst
    tutorials/calendar.rst
    tutorials/time_axis.rst
    tutorials/time_series.rst
    tutorials/dts.rst