.. _PackageShyft:

*************
Package shyft
*************

The Python module ``shyft.hydrology`` provides a high performance framework for spatially distributed conceptual hydrological
models for use in the the energy-market domain. The hydrological forecasting model framework allows for simulation
of different algorithms in a highly efficient manner, while providing an interface to conduct more explorative
evaluations of the hydrological performance of the available algorithms.

The package can be installed the following way:

1. using **pip** from `PyPI <https://pypi.org/project/shyft/>`_

   .. code-block:: bash

      pip install shyft

2. using **anaconda/conda** from the `Shyft channel <https://anaconda.org/shyft-os/shyft>`_

   .. code-block:: bash

      conda install -c shyft-os shyft

