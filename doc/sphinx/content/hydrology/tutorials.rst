*************************************
Tutorials for Shyft Hydrology toolbox
*************************************

:ref:`setup-shyft`

Shyft API introduction:

.. toctree::
    :maxdepth: 1

    tutorials/shyft_api_essentials/shyft_api_essentials.rst
    tutorials/shyft_intro/shyft_intro.rst
    tutorials/shyft_api/shyft_api.rst
    tutorials/single_cell/single_cell.rst

Simulation and calibration using Nidelva dataset:

.. toctree::
    :maxdepth: 1

    tutorials/run_nea_nidelva/run_nea_nidelva.rst
    tutorials/run_nea_nidelva_part2/run_nea_nidelva_part2.rst
    tutorials/run_nea_calibration/run_nea_calibration.rst
    tutorials/run_nea_configured_simulations/run_nea_configured_simulations.rst

Shyft Repository:

.. toctree::
    :maxdepth: 1

    tutorials/repository_intro/repository_intro.rst

Kalman filters and grid processing:

.. toctree::
    :maxdepth: 1

    tutorials/gridpp_simple/gridpp_simple.rst
    tutorials/gridpp_geopoints/gridpp_geopoints.rst
    tutorials/kalman_updating/kalman_updating.rst
    tutorials/ordinary_kriging_precipitation/ordinary_kriging_precipitation.rst

Penman monteith equation:

.. toctree::
    :maxdepth: 1

    tutorials/penman-monteith-sensitivity/penman-monteith-sensitivity.rst
    tutorials/penman-monteith-verification-single-method/penman-monteith-verification-single-method.rst

Radiation:

.. toctree::
    :maxdepth: 1

    tutorials/radiation_sensitivity_analysis/radiation_sensitivity_analysis.rst
    tutorials/radiation_polar_region/radiation_polar_region.rst
    tutorials/radiation_camels_data/radiation_camels_data.rst
