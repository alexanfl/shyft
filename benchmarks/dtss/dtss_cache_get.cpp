/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <algorithm>
#include <cstdint>
#include <string>
#include <ranges>
#include <random>
#include <vector>

#include <fmt/core.h>
#include <benchmark/benchmark.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/dtss_cache.h>

int main(int argc, char** argv) {
  std::size_t n = 100000;
  shyft::dtss::cache<shyft::dtss::apoint_ts_frag, shyft::time_series::dd::apoint_ts> cache{n};
  std::vector<std::string> ids{};
  shyft::time_series::dd::ats_vector ts{};
  auto time_axis = shyft::time_axis::generic_dt{shyft::core::from_seconds(0), shyft::core::from_seconds(10), 3};
  auto point_ts = shyft::time_series::dd::apoint_ts{
    time_axis, {1.0, 2.0, 3.0},
     shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
  };
  ids.reserve(n);
  ts.reserve(n);
  for (auto i : std::views::iota(0uz, n)) {
    ids.push_back(fmt::format("id_{}", i));
    ts.push_back(point_ts);
  }
  cache.add(std::views::zip(ids, ts), true);
  // shuffle ids:
  std::random_device rd;
  std::mt19937 gen{rd()};
  std::ranges::shuffle(ids, gen);

  auto get = [&](benchmark::State& state, auto&& ids, auto&& ts) {
    while (state.KeepRunning()) {
      benchmark::DoNotOptimize(cache.get(ids, time_axis.total_period()));
    }
  };

  auto get_with = [&](benchmark::State& state, auto&& ids, auto&& ts) {
    std::vector<shyft::time_series::dd::apoint_ts> points{ids.size()};
    while (state.KeepRunning()) {
      cache.get_with(ids, time_axis.total_period(), [&](auto const & id, auto const & index, auto const & ts) {
        if (ts) {
          points[index] = std::move(*ts);
        } else {
          points[index].ts.reset();
        }
      });
      benchmark::DoNotOptimize(points);
    }
  };
  auto r = benchmark::RegisterBenchmark("get", get, ids, ts);
  r->Iterations(1000);
  r->Unit(benchmark::kMillisecond);
  auto r2 = benchmark::RegisterBenchmark("get_with", get_with, ids, ts);
  r2->Iterations(1000);
  r2->Unit(benchmark::kMillisecond);
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}