/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <ranges>
#include <string>
#include <vector>

#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/dtss_cache.h>

int main(int argc, char** argv) {
  std::size_t n = 100000;
  shyft::dtss::cache<shyft::dtss::apoint_ts_frag, shyft::time_series::dd::apoint_ts> cache{n};
  std::vector<std::string> ids{};
  shyft::time_series::dd::ats_vector ts{};
  auto time_axis = shyft::time_axis::generic_dt{shyft::core::from_seconds(0), shyft::core::from_seconds(10), 3};
  auto point_ts = shyft::time_series::dd::apoint_ts{
    time_axis, {1.0, 2.0, 3.0},
     shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
  };
  ids.reserve(n);
  ts.reserve(n);
  for (auto i : std::views::iota(0uz, n)) {
    ids.push_back(fmt::format("id_{}", i));
    ts.push_back(point_ts);
  }

  auto add_func = [&](benchmark::State& state, auto&& ids, auto&& ts) {
    while (state.KeepRunning()) {
      cache.add(std::views::zip(ids, ts), true);
    }
  };
  benchmark::RegisterBenchmark("add", add_func, ids, ts);
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}