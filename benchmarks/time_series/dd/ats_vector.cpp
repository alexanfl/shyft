/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>
#include <benchmark/benchmark.h>
#include <vector>
#include <ranges>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

int main(int argc, char** argv) {
  // two empty ones
  shyft::time_series::dd::ats_vector zero_1;
  shyft::time_series::dd::ats_vector zero_2;
  size_t n_ts = 1000;
  shyft::time_series::dd::ats_vector nonzero_1;
  shyft::time_series::dd::ats_vector nonzero_2;
  nonzero_1.reserve(n_ts);
  nonzero_2.reserve(n_ts);

  auto time_axis = shyft::time_axis::generic_dt{shyft::core::from_seconds(0), shyft::core::from_seconds(10), 3};
  auto point_ts = shyft::time_series::dd::apoint_ts{
    time_axis, {1.0, 2.0, 3.0},
     shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
  };

  std::ranges::for_each(std::views::iota(0uz, n_ts), [&](auto i) {
    nonzero_1.push_back(point_ts);
    if (i == 900) {
      nonzero_2.push_back(2 * point_ts);
    } else {
      nonzero_2.push_back(point_ts);
    }
  });

  auto cmp = [&](benchmark::State& state, auto&& lhs, auto&& rhs) {
    while (state.KeepRunning()) {
      benchmark::DoNotOptimize(lhs.operator==(rhs));
    }
  };
  benchmark::RegisterBenchmark("operator==_zero_zero", cmp, zero_1, zero_2);
  benchmark::RegisterBenchmark("operator==_zero_nonzero", cmp, zero_1, nonzero_1);
  benchmark::RegisterBenchmark("operator==_nonzero_nonzero", cmp, nonzero_1, nonzero_2);

  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}