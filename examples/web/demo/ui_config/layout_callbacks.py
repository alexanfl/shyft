from shyft.energy_market import ui
import functools
import random
import json

from PySide2.QtCore import Qt, QDate, QTime, QDateTime
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QWidget, QFrame, QLabel,\
    QPushButton, QVBoxLayout, QTableWidget,\
    QTableWidgetItem, QGridLayout, QMenu, QAction
from PySide2.QtCharts import QtCharts


def as_json_string(f):
    """
    Post-processing of Widget-generating function to return a JSON-string
    decoding its data.
    :param f: A function returning a QtWidget
    :return: A function returning the ui.export(QtWidget)
    """
    @functools.wraps(f) # To keep function name and documentation
    def wrapped_func(*args, **kwargs):
        qtobj = f(*args, **kwargs)
        return ui.export(qtobj)
    return wrapped_func


class LayoutCallback:
    """
    Class for generating QtWidgets and
    """
    def __init__(self):
        pass

    def navigation_menu(self, argmap):
        window = QWidget()
        layout = QVBoxLayout()
        window.setLayout(layout)
        btn_region = QPushButton(window)
        btn_region.setText("Select user")

        layout.addWidget(btn_region)
        btn_region_mnu = QMenu()
        btn_region.setMenu(btn_region_mnu)
        btn_region_mnu_entry = QAction("Nord", window)
        btn_region_mnu.addAction(btn_region_mnu_entry)
        btn_region_mnu_entry = QAction("Sør", window)
        btn_region_mnu.addAction(btn_region_mnu_entry)

        btn_region_mnu_entry = QAction("Øst", window)
        btn_region_mnu.addAction(btn_region_mnu_entry)
        btn_region_mnu_entry_sub = QMenu()
        btn_region_mnu_entry.setMenu(btn_region_mnu_entry_sub)

        btn_region_mnu_entry_sub_entry = QAction("Roberto Firmino", window)
        btn_region_mnu_entry_sub_entry.setIconText("account_circle")
        btn_region_mnu_entry_sub_entry.setProperty("id", 0)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub_entry.setProperty("callback", 'get_layouts { "request_id": "hello" }')
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Sadio Mane", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 1)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Mohamed Salah", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 2)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Alisson Becker", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 3)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Virgil van Dijk", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 4)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Trent Alexander-Arnold", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 5)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry = QAction("Vest", window)
        btn_region_mnu.addAction(btn_region_mnu_entry)
        btn_region_mnu_entry_sub = QMenu()
        btn_region_mnu_entry.setMenu(btn_region_mnu_entry_sub)

        btn_region_mnu_entry_sub_entry = QAction("Tommy Høiland", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 6)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Yann-Erik de Lanlay", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 7)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Veton Berisha", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 8)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Johnny Furdal", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 9)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Kristoffer Løkberg", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 10)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        btn_region_mnu_entry_sub_entry = QAction("Zymer Bytyqi", window)
        btn_region_mnu_entry_sub_entry.setProperty("id", 11)
        btn_region_mnu_entry_sub_entry.setProperty("tag", "user")
        btn_region_mnu_entry_sub.addAction(btn_region_mnu_entry_sub_entry)

        return window

    def box(self, argmap):
        window = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(QLabel(argmap.get("top", "Top")))
        layout.addWidget(QLabel(argmap.get("bottom", "Bottom")))
        window.setWindowTitle(argmap.get("title", "Title"))
        window.setLayout(layout)
        return window

    def multilevel_grid(self, argmap):
        window = QWidget()
        mainLayout = QGridLayout()
        childLayout = QGridLayout()
        childLayout.addWidget(QLabel(argmap.get("a", "A")))
        childLayout.addWidget(QLabel(argmap.get("b", "B")))
        mainLayout.addLayout(childLayout, 0, 0)
        window.setLayout(mainLayout)
        return window

    def multicell_grid(self, argmap):
        window = QWidget()
        mainGrid = QGridLayout()
        for i in range(3):
            for j in range(3):
                if j == 1 and i == 1:
                    subGrid = QGridLayout()
                    subGrid.addWidget(QLabel("A"))
                    subGrid.addWidget(QLabel("B"))
                    wm = QWidget()
                    wm.setLayout(subGrid)
                    mainGrid.addWidget(wm, i, j)
                else:
                    mainGrid.addWidget(QLabel(f"B({i}, {j})"), i, j)
        window.setLayout(mainGrid)
        return window

    def chart(self, argmap):
        # function to generate chart Widget
        pass

    def chart_and_table_grid(self, argmap):
        fill_data = True
        window = QWidget()
        mainGrid = QGridLayout()
        counter = 0
        for i in range(3):
            for j in range(3):
                if i == 0 and j == 0:
                    subGrid = QGridLayout()
                    for k in range(7):
                        subItem = QPushButton(f"Item {counter} [{i}, {j}]/[{k}]")
                        subItem.setFlat(True if k % 2 == 0 else False)
                        subGrid.addWidget(subItem)
                    subGridWidget = QWidget()
                    subGridWidget.setLayout(subGrid)
                    mainGrid.addWidget(subGridWidget, i, j)
                elif i == 1 and j == 0:
                    table = QTableWidget()
                    table.setRowCount(10)
                    table.setColumnCount(3)
                    tableHeader = ["A", "B", "C"]
                    table.setHorizontalHeaderLabels(tableHeader)
                    table.verticalHeader().setVisible(False)
                    table.setShowGrid(False)
                    table.setStyleSheet("QTableView {selection-background-color: red;}")
                    if fill_data:
                        table.setItem(0, 0, QTableWidgetItem("Item 0.0"))
                        table.setItem(0, 1, QTableWidgetItem("Item 0.1"))
                        table.setItem(0, 2, QTableWidgetItem("Item 0.2"))
                        table.setItem(1, 0, QTableWidgetItem("Item 1.0"))
                        table.setItem(1, 1, QTableWidgetItem("Item 1.1"))
                        table.setItem(1, 2, QTableWidgetItem("Item 1.2"))
                    mainGrid.addWidget(table, i, j)
                elif i == 1 and j == 1:
                    axis_x = QtCharts.QDateTimeAxis()
                    axis_x.setTickCount(10)
                    axis_x.setFormat("dd.MM (h:mm)")
                    axis_x.setTitleText("This is a datetime axis")

                    axis_y = QtCharts.QValueAxis()
                    axis_y.setTickCount(10)
                    axis_y.setLabelFormat("%.2f")
                    axis_y.setTitleText("This is a value axis")
                    axis_y.setMin(10)
                    axis_y.setMax(100)

                    serie1 = QtCharts.QLineSeries()
                    serie1.setName("This is a series")

                    if fill_data:
                        t = QDateTime.currentDateTime()
                        t.setTime(QTime())
                        for _ in range(24):
                            v = random.randrange(10, 100)
                            serie1.append(float(t.toMSecsSinceEpoch()), v)
                            t = t.addSecs(3600)
                        chart = QtCharts.QChart()
                        chart.setTitle("This is a chart")
                        chart.addAxis(axis_x, Qt.AlignBottom)
                        chart.addAxis(axis_y, Qt.AlignLeft)

                        chart.addSeries(serie1)
                        serie1.attachAxis(axis_x)
                        serie1.attachAxis(axis_y)
                        chartView = QtCharts.QChartView(chart)
                        mainGrid.addWidget(chartView, i, j)
                else:
                    label = QLabel(f"Item {counter} [{i}, {j}]")
                    label.setFont(QFont("Consolas", 24))
                    label.setFrameShape(QFrame.StyledPanel)
                    mainGrid.addWidget(label, i, j)
                counter += 1
        # Stretch
        mainGrid.setColumnStretch(1, 1) # Prefer stretching the chart when the horizontal resize

        window.setLayout(mainGrid)
        window.setGeometry(200, 200, 1000, 750)
        window.setWindowTitle("This is a window")
        return window

    @as_json_string
    def __call__(self, name: str, args: str):
        print(f"Trying to generate a new layout with name: {name}")
        try:
            argmap = json.loads(args)
        except json.JSONDecodeError:
            raise RuntimeError("Unable to parse args as a JSON-struct.")
        tpe = argmap.pop("type") # Should raise a KeyError if not present
        return getattr(self, tpe)(argmap)
        raise RuntimeError(f"Unknown widget type: '{tpe}'")
