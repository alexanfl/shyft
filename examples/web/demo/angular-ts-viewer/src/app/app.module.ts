import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { MessagesComponent } from './messages/messages.component';
import { TsGraphComponent } from './ts-graph/ts-graph.component';
import { TsTableComponent } from './ts-table/ts-table.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    MessagesComponent,
    TsGraphComponent,
    TsTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
