import {Injectable, OnDestroy} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {catchError, switchMap, finalize} from 'rxjs/operators';

import makeWebSocketObservable, {
  GetWebSocketResponses
} from 'rxjs-websockets';

import {QueueingSubject } from 'queueing-subject';
import { TsInfo, TimeSeries   } from './ts-info';
import {MessageService} from './message.service';


@Injectable({
  providedIn: 'root'
})
export class TimeSeriesService implements OnDestroy {
  private rootPath = 'shyft://test/';

  // outgoing messages : this subject queues as necessary to ensure every message is delivered
  private input$ = new QueueingSubject<string>();

  // sock message exchange: create the websocket observable, does *not* open the websocket connection
  private socket$ = makeWebSocketObservable(`ws://${location.host}`);

  // incoming message stream from the socket$, passes outgoing messages from the input$ to the socket$
  // and pipes the responses back to the subscription multiplexer using the request-id to
  // dispatch the message back to the original request-handler.
  private messages$: Observable<string> = this.socket$.pipe(
    switchMap(
      (getResponse: GetWebSocketResponses<string>): Observable<string> => {
        return getResponse(this.input$);
      }),
      catchError(this.handleError<any>('GetResponse'))
  );
  selectedTsInfo = new Subject<TsInfo>(); // currently just a bit clumsy place-holder for the search dialog subject TODO: relocate it
  requestId = 1; // a unique request id-counter, that is incremented for each new request
  requestMap = new Map<string, (response: object) => void >(); // map request-id to its fx that deals with upstream dispatch

  ngOnDestroy(): void {
  }

  constructor(
    private messageService: MessageService,
  ) {
    // here we rig the response-handler to parse the responses, get the request-id, and lookup the
    // callback/completion-handler for that specific request-id
    this.messages$.subscribe(response => {
      // this.log(`Got response ${response}`);
      const responseStruct = JSON.parse(response); // parse the json, all of them do have a unique request_id
      if (this.requestMap.has(responseStruct.request_id)) { // do we have the request-id subscription fx ?
        const fx: (o: object) => void = this.requestMap.get(responseStruct.request_id); // get it out from map
        fx(responseStruct); // invoke the upstream handling (could be a long pipe-line, here its just a function
      } else {
        this.log(`Unable to route response ${response}`);
      }
    });
  }


  findTimeSeries(term: string): Observable<TsInfo[]> {
    if ( !term.trim()) {
      return of([]); // if no search term, return empty  array
    }
    const rid = `${this.requestId++}` ;
    const findRequest = `find { "request_id": "${rid}", "find_pattern": "${this.rootPath + term}"}`;
    const tsi = new Subject<TsInfo[]>();
    this.requestMap.set(rid,
      (response) => {
        this.requestMap.delete(rid); // we delete the mapping since this is a single shot
        // @ts-ignore  because we 'know' its a find-request response
        tsi.next(response.result as TsInfo[]);
    });
    this.input$.next(findRequest); // send message through socket
    return tsi.asObservable();
  }

  // unsubscribes the request-id 'rid' so that the dtss web-api stops emitting changes
  // for this subscription on the server-side.
  unsubscribe(rid: string): void {
    const urid = `${this.requestId++}` ;
    // this.log(`unsubscribe ${rid}`);
    const unsubscribeRequest = {
      request_id: urid,
      subscription_id : rid
    };
    this.requestMap.set(urid,
      (response) => { // arrange what is going to happen when we receive the response
        this.requestMap.delete(urid); // pull our self out of mapping
        this.requestMap.delete(rid); // and get rid of the subscription id
      });
    this.input$.next(`unsubscribe ${JSON.stringify(unsubscribeRequest)}`); // pass the unsubscribe to the server
  }

  // given tsInfo, do a subscription on that time-series on the dtss-web-api.
  // returns the observable stream of updates, that will get an initial update
  // followed by all changes (pushed from server) when the server-side time-series
  // is updated.
  getTimeSeries(tsInfo: TsInfo): Observable<TimeSeries[]> {
    const rid = `${this.requestId++}` ;
    const readRequest = { // Create read request:
      request_id: rid,
      read_period: tsInfo.data_period,
      clip_period: tsInfo.data_period,
      cache: true,
      ts_ids: [`${this.rootPath + tsInfo.name}`],
      subscribe: true // retransmitt on change
    };
    const readRequestString = `read ${JSON.stringify(readRequest)}`;
    const timeSeries = new Subject<TimeSeries[]>(); // create a subject/observable for this request subscription
    this.requestMap.set(rid,
      (response) => {
        // @ts-ignore because we know the response is a time-series response
        const tsv = response.tsv as TimeSeries[];
        if (timeSeries.closed) { // if it is closed, by the subscriber, then we give up
            this.unsubscribe(rid);  // this.requestMap.delete(rid);
        } else {
          timeSeries.next(tsv);
        }
    });
    this.input$.next(readRequestString); // Send message through socket
    return timeSeries.pipe(finalize(() => {this.unsubscribe(rid); } )); // Return the received timeSeries as observable
  }


  private log(message: string) {
    this.messageService.add(`TimeSeriesService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging...
      console.error(error);
      // TODO: better job of transforming:
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
