from shyft.time_series import DtsServer, DtssCfg, time
from shyft.energy_market.service.boot import Exit
from time import sleep
from pathlib import Path

def start_geo_dtss(root_dir: str, port_no: int, compress: bool, cache_target_mb: int) -> DtsServer:
    s = DtsServer()
    s.cache_ts_initial_size_estimate = 8*1024  # get initial guess there ar many small ts.
    s.cache_max_items = 20*1000*1000  # 20 mill timeseries
    s.cache_memory_target = cache_target_mb*1000*1000  # 5000 MB is ok.
    M = 1024*1024
    fc_cfg = DtssCfg(ppf=1024, compress=compress, max_file_size=100*M, write_buffer_size=4*M, log_level=200)
    s.default_geo_db_config = fc_cfg  # if the client creates new geo/ts db, these settings will apply.
    s.configuration_file = str(Path(root_dir).parent / 'dtss.container.cfg')
    s.set_container('', root_dir)  # first .. to ensure startable system, needs root dir scan geo .. db.

    s.set_listening_port(port_no)
    s.start_server()

    return s


# Create and start a server instance `s`. We must keep a reference to `s`, otherwise it is garbage collected
# Using port 20000 as example, but any available port should work.
# If you shall access the server from other hosts, then remember to open firewall for the selected port
if __name__ == '__main__':
    import sys

    if len(sys.argv) != 4:
        print(
            "Usage: start_geo_dtss port_no cache_target_mb root_dir \n"
            " port_no should be a *free*  ip port number, for example 20000.\n"
            " cache_mb should be  number of MB for cache, e.g. 5000.\n"
            " root_dir should be a directory on a device sufficient to store your data.\n"
        )
        exit(0)

    port_no = int(sys.argv[1])
    cache_mb = int(sys.argv[2])
    root_dir = sys.argv[3]
    Path(root_dir).mkdir(parents=True, exist_ok=True)
    if cache_mb < 1000:
        cache_mb = 1000  # set reasonable minimum size 1 G
    s = start_geo_dtss(root_dir=root_dir, port_no=port_no, compress=False, cache_target_mb=cache_mb)
    ex = Exit()
    t_report = time.now()
    report_interval = time(1*10)  # emmit log entry every x 10s
    print(f"Starting dtss at port {port_no} , data root directory at {root_dir}, cache  {cache_mb} MB\n Ctrl-C to quit")
    try:
        while True:
            sleep(0.1)
            if ex.now:
                break
            if time.now() - t_report > report_interval:
                c = s.cache_stats
                print(f'cache # = {c.id_count} #hits={c.hits} #misses={c.misses} #point-sz={c.point_count*8/1e6} Mbytes')
                t_report = time.now()
    except KeyboardInterrupt:
        print('Ctrl-C')
    finally:
        print(f'terminating services ...')
        s.stop_server(1000)
        del s
        print('exit')
