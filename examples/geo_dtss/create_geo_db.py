from shyft.time_series import (DtsClient, GeoGridSpec, GeoTimeSeriesConfiguration, GeoPoint,
                               GeoPointVector, StringVector, time, UtcTimeVector, Calendar)


def create_geo_db(client: DtsClient, container_name: str = "geo_test",
                  descr: str = 'test geo dtss', n_x: int = 3, n_y: int = 4,
                  n_ensembles: int = 1, n_variables: int = 2, dt: time = Calendar.WEEK):
    variables = StringVector([f'v{i}' for i in range(n_variables)])  # minimalistic variable names
    grid_points = [GeoPoint(x*1000, y*1000, (x + y)*100) for x in range(n_x) for y in range(n_y)]  # (x, y, z) coordinates
    points = GeoPointVector(grid_points)  # Vector of the grid points, length 12
    grid = GeoGridSpec(epsg=32633, points=points)

    gdb = GeoTimeSeriesConfiguration('shyft://', container_name, descr, grid,
                                     UtcTimeVector(), dt, n_ensembles, variables)
    client.add_geo_ts_db(gdb)


def create_concat_geo_db(client: DtsClient, t0_dt=time(3600*6), n_x: int = 10, n_y: int = 20,
                         n_ensembles: int = 1, n_variables: int = 5):
    geos = [g.name for g in client.get_geo_db_ts_info() ] # get the configurations from  the server
    if 'fc' not in geos:
        create_geo_db(client, "fc", 'individual arome-foreast test', n_x, n_y, n_ensembles, n_variables, t0_dt)
    else:
        print(f"dtss/geo db 'fc' already exists")
    if 'cc' not in geos:
        create_geo_db(client, "cc", ')concatenated arome-forecasts', n_x, n_y, n_ensembles, n_variables, Calendar.YEAR*30)
    else:
        print(f"dtss/geo db 'cc' already exists")


if __name__ == '__main__':
    dtss_address = f'localhost:20000'
    print(f"Starting create_concat_geo_db, assuming dtss is available at {dtss_address}")
    c = DtsClient(dtss_address)
    create_concat_geo_db(c)
    c.close()
    del c
    print("done")
