from shyft.time_series import (DtsClient, GeoEvalArgs, GeoQuery, GeoGridSpec, GeoTimeSeriesConfiguration, GeoPoint,
                               GeoPointVector, StringVector, time, UtcTimeVector, Calendar, GeoSlice, utctime_now,
                               TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case,
                               time_axis_extract_time_points_as_utctime)


def read_geo_forecasts(c: DtsClient, use_cache: bool = True, update_cache=True):
    utc = Calendar()
    geos = c.get_geo_db_ts_info()  # get the configurations from  the server
    gdb = None
    gcc = None
    for geo in geos:
        if geo.name == 'fc':
            gdb = geo
        elif geo.name == 'cc':
            gcc = geo
    assert gdb and gcc, 'db-server must have gdb and gcc setup'
    # ta_t0 = TimeAxis(utc.time(2021, 7, 1), time(3600*6), 4*(10))
    print(f'Read in all available forecast')  # {repr(ta_t0)}')
    fc_ea = GeoEvalArgs(geo_ts_db_id='fc', variables=[f'v{i}' for i in range(5)], ensembles=[0], time_axis=gdb.t0_time_axis,
                        ts_dt=time(3600*24*10),  # default 10 days of data in each fc
                        geo_range=GeoQuery(), concat=False, cc_dt0=time(0))
    fc_cc_ea = GeoEvalArgs(geo_ts_db_id='fc', variables=[f'v{i}' for i in range(5)], ensembles=[0], time_axis=gdb.t0_time_axis,
                           ts_dt=time(0),
                           geo_range=GeoQuery(), concat=True, cc_dt0=time(0))
    cc_ea = GeoEvalArgs(geo_ts_db_id='cc', variables=[f'v{i}' for i in range(5)], ensembles=[0], time_axis=gcc.t0_time_axis,
                        ts_dt=time(0),
                        geo_range=GeoQuery(), concat=False, cc_dt0=time(0))
    cc0 = c.cache_stats
    tx1 = utctime_now()
    smart_update_cache = True if cc0.id_count == 0 else update_cache  # smart update of cache
    gtsm3 = c.geo_evaluate(eval_args=fc_ea, use_cache=use_cache, update_cache=smart_update_cache)
    cc_used = utctime_now() - tx1
    cc1 = c.cache_stats
    print(f"Read fc={cc_used}, id_count={cc1.id_count} hits-delta={cc1.hits - cc0.hits} misses-delta={cc1.misses - cc0.misses}")
    cc0 = c.cache_stats
    tx1 = utctime_now()
    gtsm4 = c.geo_evaluate(eval_args=cc_ea, use_cache=use_cache, update_cache=update_cache)
    ea_used = utctime_now() - tx1
    cc1 = c.cache_stats
    print(f"Read one year ={ea_used}, id_count={cc1.id_count} hits-delta={cc1.hits - cc0.hits} misses-delta={cc1.misses - cc0.misses}")
    tx1 = utctime_now()
    gtsm5 = c.geo_evaluate(eval_args=fc_cc_ea, use_cache=use_cache, update_cache=smart_update_cache)
    ea_used = utctime_now() - tx1
    cc2 = c.cache_stats
    print(f"Read one year fc server side cc ={ea_used}, id_count={cc2.id_count} hits-delta={cc2.hits - cc1.hits} misses-delta={cc2.misses - cc1.misses}")
    print(f'fc.time-axis={gtsm3.get_ts(0, 1, 0, 1).time_axis}')
    print(f'fc.values={gtsm3.get_ts(0, 1, 0, 1).values.to_numpy()}')
    print(f'fc_cc.time-axis={gtsm5.get_ts(0, 1, 0, 1).time_axis}')
    print(f'fc_cc.values={gtsm5.get_ts(0, 1, 0, 1).values.to_numpy()}')
    print(f'cc.time-axis={gtsm4.get_ts(0, 1, 0, 1).time_axis}')
    print(f'cc.values={gtsm4.get_ts(0, 1, 0, 1).values.to_numpy()}')

if __name__ == '__main__':
    dtss_address = f'localhost:20000'
    print(f"Starting reading, assume dtss is available at {dtss_address}")
    c = DtsClient(dtss_address)
    read_geo_forecasts(c, use_cache=True, update_cache=True)
