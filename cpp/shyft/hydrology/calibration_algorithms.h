/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <cmath>
#include <utility>
#include <memory>
#include <stdexcept>

#include <dlib/optimization.h>
#include <dlib/global_optimization.h>
#include <dlib/matrix.h>
#include <dlib/statistics.h>

#include <shyft/hydrology/optimizers/dream_optimizer.h>
#include <shyft/hydrology/optimizers/sceua_optimizer.h>

namespace shyft::core::model_calibration {
  using std::vector;
  using std::true_type;
  using std::false_type;
  using std::shared_ptr;
  using std::atomic_bool;
  using std::move;
  using shyft::time_series::dd::apoint_ts;

  /**
   * @brief Minimize the functional defined by model using the BOBYQA method
   * @details
   * Solve the optimization problem defined by the model M using the Bounded Optimization by Quadratic Approximation
   * method by Michael J. D. Powell. See the "The BOBYQA Algorithm for Bound Constrained Optimization Without
   * Derivatives", Technical report, Department of Applied Mathematics and Theoretical Physics, University of Cambridge,
   * 2009.
   *
   * We use the dlib implementation of this algorithm, see <a href="www.dlib.net/optimization.html#find_min_bobyqa">dlib
   * bobyqua</a>.
   *
   * @tparam M Model to be optimized, implementing the interface:
   *   -# double operator()(const dlib::matrix<double, 0, 1> params) --> Evaluate the functional for parameter set
   * params.
   *   -# vector<double> to_scaled(std::vector<double> params) --> Scale original parameters to [0,1]
   *   -# vector<double> from_scaled(const dlib::matrix<double, 0, 1> scaled_params) --> Unscale parameters
   *  @param model the model to be optimized
   *  @param x Initial guess
   *  @param max_n_evaluations stop/throw if not converging after n evaluations
   *  @param tr_start Initial trust region radius
   *  @param tr_stop Stopping trust region radius
   */
  template <class M>
  double min_bobyqa(M& model, vector<double>& x, int max_n_evaluations, double tr_start, double tr_stop) {

    typedef dlib::matrix<double, 0, 1> column_vector;

    // Scale all parameter ranges to [0, 1] for better initial trust region radius balance.
    std::vector<double> x_s = model.to_scaled(x);
    if (x.size() == 1) { // BOBYQA does not support 1 parameter, defer to dlib alternative
      auto result = dlib::find_min_single_variable(
        [&](double x) {
          return model(std::vector<double>{x});
        },
        x_s[0],
        0.0,              // begin of the searching area
        1.0,              // end of the searching area
        0.001,            // epsilon the solver stops on
        max_n_evaluations // max number of objective function evaluations
      );
      x = model.from_scaled(x_s);
      return result;
    }

    column_vector _x = dlib::mat(x_s);

    // Scaled region min and max
    column_vector x_l(_x.nr());
    x_l = 0.0;
    column_vector x_u(_x.nr());
    x_u = 1.0;

    double res = find_min_bobyqa(
      [&model](column_vector x) {
        return model(x);
      },
      _x,
      2 * _x.nr() + 1, // Recommended number of interpolation points
      x_l,
      x_u,
      tr_start,         // initial trust region radius
      tr_stop,          // stopping trust region radius
      max_n_evaluations // max number of objective function evaluations
    );

    // Convert back to real parameter range
    x = model.from_scaled(_x);
    return res;
  }

  /** @brief Minimize the functional defined by model using the DLIB find_min_global
   *
   *
   * For reference , see <a href="http://dlib.net/optimization.html#global_function_search">dlib
   * global_function_search</a>.
   *
   * @tparam M Model to be optimized, implementing the interface:
   *   -# double operator()(const dlib::matrix<double, 0, 1> params) --> Evaluate the functional for parameter set
   * params.
   *   -# vector<double> to_scaled(std::vector<double> params) --> Scale original parameters to [0,1]
   *   -# vector<double> from_scaled(const dlib::matrix<double, 0, 1> scaled_params) --> Unscale parameters
   *  @param model the model to be optimized
   *  @param x Initial guess
   *  @param max_n_evaluations stop  after n evaluations
   *  @param max_seconds stop search after  time used
   *  @param solver_epsilon conclude on minimum within this accuracy before stop or search other minimums
   */
  template <class M>
  double min_global(M& model, vector<double>& x, int max_n_evaluations, double max_seconds, double solver_epsilon) {

    typedef dlib::matrix<double, 0, 1> column_vector;

    // Scale all parameter ranges to [0, 1] for better initial trust region radius balance.
    std::vector<double> x_s = model.to_scaled(x);

    column_vector _x = dlib::mat(x_s);

    // Scaled region min and max
    column_vector x_l(_x.nr());
    x_l = 0.0;
    column_vector x_u(_x.nr());
    x_u = 1.0;

    auto res = dlib::find_min_global(
      [&model](column_vector x) {
        return model(x);
      },
      //_x,
      x_l,
      x_u,
      dlib::max_function_calls(max_n_evaluations), // max number of objective function evaluations
      std::chrono::nanoseconds{int64_t(max_seconds * 1e9)},
      solver_epsilon);

    // Convert back to real parameter range
    x = model.from_scaled(res.x);
    return res.y;
  }

  ///< Template class to transform model evaluation into something that dream can run
  template <class M>
  struct dream_fx : public shyft::core::optimizer::ifx {
    M& m;

    explicit dream_fx(M& m)
      : m(m) {
    }

    double evaluate(vector<double> const & x) {
      return -m(x); // notice that dream find maximumvalue, so we need to negate the goal function, effectively finding
                    // the minimum value.
    }
  };

  /**
   * @brief template function that find the x that minimizes the evaluated value of model M using DREAM algorithm
   * @tparam M the model that need to have .to_scaled(x) and .from_scaled(x) to normalize the supplied parameters to
   * range 0..1
   * @param model a reference to the model to be evaluated
   * @param x     the initial x parameters to use (actually not, dream is completely random driven), filled in with the
   * optimal values on return
   * @param max_n_evaluations is the maximum number of iterations, currently not used, the routine returns when
   * convergence is reached
   * @return the goal function of m value, corresponding to the found x-vector
   */
  template <class M>
  double min_dream(M& model, vector<double>& x, int max_n_evaluations) {
    // Scale all parameter ranges to [0, 1]
    std::vector<double> x_s = model.to_scaled(x);
    dream_fx<M> fx_m(model);
    shyft::core::optimizer::dream dr;
    double res = dr.find_max(fx_m, x_s, max_n_evaluations);
    // Convert back to real parameter range
    x = model.from_scaled(x_s);
    return res;
  }

  ///< Template class to transform model evaluation into something that dream can run
  template <class M>
  struct sceua_fx : public shyft::core::optimizer::ifx {
    M& m;

    explicit sceua_fx(M& m)
      : m(m) {
    }

    double evaluate(vector<double> const & x) {
      return m(x);
    }
  };

  /**
   * @brief template for the function that finds the x that minimizes the evaluated value of model M using SCEUA
   * algorithm
   * @tparam M the model, that provide .to_scaled(x) and .from_scaled(x) to normalize the parameters to 0..1 range
   * @param model a reference to the model evaluated
   * @param x starting point for the parameters
   * @param max_n_evaluations stop after max_n_interations reached(keep best x until then)
   * @param x_eps stop when all x's changes less than x_eps(recall range 0..1), convergence in x
   * @param y_eps stop when last y-values (model goal functions) seems to have converged (no improvements)
   * @return the goal function of m value, and x is the corresponding parameter-set.
   * @throw runtime_error with text sceua: terminated before convergence or max iterations
   */
  template <class M>
  double
    min_sceua(M& model, vector<double>& x, size_t max_n_evaluations, double x_eps = 0.0001, double y_eps = 0.0001) {
    // Scale all parameter ranges to [0, 1]
    vector<double> x_s = model.to_scaled(x);
    vector<double> x_min(x_s.size(), 0.0); /// normalized range is 0..1 so is min..max
    vector<double> x_max(x_s.size(), 1.0);
    vector<double> x_epsv(x_s.size(), x_eps);
    // notice that there is some adapter code here, for now, just to keep
    // the sceua as close to origin as possible(it is fast&efficient, with stack-based mem.allocs)
    double* xv = __autoalloc__(double, x_s.size());
    shyft::core::optimizer::fastcopy(xv, x_s.data(), x_s.size());
    sceua_fx<M> fx_m(model);
    shyft::core::optimizer::sceua opt;
    double y_result = 0;
    // optimize with no specific range for y-exit (max-less than min):
    auto opt_state = opt.find_min(
      x_s.size(), x_min.data(), x_max.data(), xv, y_result, fx_m, y_eps, -1.0, -2.0, x_epsv.data(), max_n_evaluations);
    for (size_t i = 0; i < x_s.size(); ++i)
      x_s[i] = xv[i]; // copy from raw vector
    // Convert back to real parameter range
    x = model.from_scaled(x_s);
    if (!(opt_state == shyft::core::optimizer::OptimizerState::FinishedFxConvergence
          || opt_state == shyft::core::optimizer::OptimizerState::FinishedXconvergence
          || opt_state == shyft::core::optimizer::FinishedMaxIterations))
      throw runtime_error("sceua: terminated before convergence or max iterations"); // FinishedMaxIterations)
    return y_result;
  }
}
