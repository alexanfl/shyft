#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <cstdint>
#include <memory>
#include <vector>

#include <boost/variant.hpp> // boost::variant supports serialization
#include <shyft/hydrology/srv/msg_defs.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/api/a_region_environment.h> // looking for region environment
#include <shyft/hydrology/api/api_state.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/model_calibration.h>

namespace shyft::hydrology::srv {

  using std::vector;
  using std::shared_ptr;
  using std::make_shared;
  using std::string;
  using shyft::core::utctime;
  // using cellstate_t = shyft::api::cell_state_with_id<shyft::core::api::pt_gs_k::state>;
  using shyft::time_series::dd::apoint_ts;
  using shyft::core::region_model;
  using namespace shyft::core;
  using shyft::api::a_region_environment;

  using shyft::api::cell_state_with_id;
  using shyft::core::model_calibration::optimizer;


  using model_variant_t = boost::variant<
    shared_ptr<region_model<pt_gs_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<pt_gs_k::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<pt_ss_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<pt_ss_k::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<pt_hs_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<pt_hs_k::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<pt_hps_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<pt_hps_k::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<pt_st_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<pt_st_k::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<pt_st_hbv::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<pt_st_hbv::cell_discharge_response_t, a_region_environment>>,

    shared_ptr<region_model<r_pt_gs_k::cell_complete_response_t, a_region_environment>>,
    shared_ptr<region_model<r_pt_gs_k::cell_discharge_response_t, a_region_environment>>

    // hbv-stack not supported due to quality issues.

    >;

  using calibration_variant_t = boost::variant<
    shared_ptr< optimizer<region_model<pt_gs_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr< optimizer<region_model<pt_gs_k::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<pt_ss_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<pt_ss_k::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<pt_hs_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<pt_hs_k::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<pt_hps_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<pt_hps_k::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<r_pm_gs_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<r_pm_gs_k::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<pt_st_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<pt_st_k::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<pt_st_hbv::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<pt_st_hbv::cell_discharge_response_t, a_region_environment>>>,

    shared_ptr<optimizer<region_model<r_pt_gs_k::cell_complete_response_t, a_region_environment>>>,
    shared_ptr<optimizer<region_model<r_pt_gs_k::cell_discharge_response_t, a_region_environment>>>

    // hbv-stack not supported due to quality issues.

    >;


}
