#pragma once


/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <cstdint>
#include <memory>
#include <vector>

#include <boost/variant.hpp> // boost::variant supports serialization
#include <shyft/hydrology/srv/msg_defs.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/api/a_region_environment.h> // looking for region environment
#include <shyft/hydrology/api/api_state.h>

// #include <shyft/hydrology/model_calibration.h>

namespace shyft::hydrology::srv {

  using std::vector;
  using std::shared_ptr;
  using std::make_shared;
  using std::string;
  using shyft::core::utctime;
  // using cellstate_t = shyft::api::cell_state_with_id<shyft::core::api::pt_gs_k::state>;
  using shyft::time_series::dd::apoint_ts;
  // using shyft::core::region_model;
  using namespace shyft::core;
  using shyft::api::a_region_environment;

  using shyft::api::cell_state_with_id;


  /** wrap state into variant that support serialization and python exposure automagically */
  using state_variant_t = boost::variant<
    shared_ptr<vector<cell_state_with_id<pt_gs_k::state_t>>>,
    shared_ptr<vector<cell_state_with_id<pt_ss_k::state_t>>>,
    shared_ptr<vector<cell_state_with_id<pt_hs_k::state_t>>>,
    shared_ptr<vector<cell_state_with_id<pt_hps_k::state_t>>>,
    shared_ptr<vector<cell_state_with_id<r_pm_gs_k::state_t>>>,
    shared_ptr<vector<cell_state_with_id<pt_st_k::state_t>>>,
    shared_ptr<vector<cell_state_with_id<pt_st_hbv::state_t>>>,
    shared_ptr<vector<cell_state_with_id<r_pt_gs_k::state_t>>> >;

  struct state_model {
    std::int64_t id{0};
    state_variant_t states;

    SHYFT_DEFINE_STRUCT(state_model, (), (id, states));
    auto operator<=>(state_model const &) const = default;

    ///< equal by value equal
    bool operator==(state_model const &o) const {
      if (id != o.id)
        return false;
      return boost::apply_visitor(
        [](auto const &a_, auto const &b_) -> bool {
          if constexpr (std::is_same_v<decltype(a_), decltype(b_)>)
            return *a_ == *b_;
          else
            return false;
        },
        states,
        o.states);
    }

    x_serialize_decl();
  };

  /** wrap state into variant that support serialization and python exposure automagically */
  using parameter_variant_t = boost::variant<
    shared_ptr<pt_gs_k::parameter>,
    shared_ptr<pt_ss_k::parameter>,
    shared_ptr<pt_hs_k::parameter>,
    shared_ptr<pt_hps_k::parameter>,
    shared_ptr<r_pm_gs_k::parameter>,
    shared_ptr<pt_st_k::parameter>,
    shared_ptr<pt_st_hbv::parameter>,
    shared_ptr<r_pt_gs_k::parameter> >;

  /** @brief variant template class for parameter map */
  template <class... P> // takes parameter type pack, likg pt_gs_k::parameter,pt_st_k::parameter..
  using parameter_map_variant =
    boost::variant< std::map<int64_t, std::shared_ptr<P>>... // roll out parameter pack to a set of map to shared_ptr p
                    >;

  /**
   * @brief parameter map variant for model expose
   * @details
   * We have already exposed the type std::map int to shared_ptr parameter,
   * so utilizing this when creating the variant.
   */
  using parameter_map_variant_t = parameter_map_variant<
    pt_gs_k::parameter,
    pt_ss_k::parameter,
    pt_hs_k::parameter,
    pt_hps_k::parameter,
    r_pm_gs_k::parameter,
    pt_st_k::parameter,
    pt_st_hbv::parameter,
    r_pt_gs_k::parameter >;

  namespace _ {
    /**
     * @brief utility to compare two map(int,shared_ptr(T)) for eqality
     *
     */
    template <class T>
    bool equal_ptr_map(T const &a, T const &b) {
      if (a.size() != b.size())
        return false;
      for (auto ai = std::begin(a), bi = std::begin(b); ai != std::end(a) && bi != std::end(b); ++ai, ++bi) {
        if (ai->first != bi->first) // keys assume map, which is ordered
          return false;
        if (ai->second && bi->second) {
          if (*(ai->second) != *(bi->second))
            return false;
        } else if (!(ai->second == nullptr && bi->second == nullptr))
          return false;
      }
      return true;
    }
  }

  /**
   * @brief parameter model for dealing with parameters for region model
   * @details
   * When orchestrating models, we need to keep several identified models stored in service
   * where we can retrieve them, as part of orchestrating a run or a calibration for hydrology region models.
   */
  struct parameter_model {
    std::int64_t id{0};
    parameter_map_variant_t parameters; //

    SHYFT_DEFINE_STRUCT(parameter_model, (), (id, parameters));
    auto operator<=>(parameter_model const &) const = default;

    ///< equal by value equal
    bool operator==(parameter_model const &o) const {
      if (id != o.id)
        return false;
      return boost::apply_visitor(
        [](auto const &a_, auto const &b_) -> bool {
          if constexpr (std::is_same_v<decltype(a_), decltype(b_)>)
            return _::equal_ptr_map(a_, b_);
          else
            return false;
        },
        parameters,
        o.parameters);
    }

    x_serialize_decl();
  };
  /** enumerate the supported model types above (also exposed as constants to python)
   * The _opt enum should map to the discharge/snow only models used for speed/resource optimal templates.
   */
  enum struct rmodel_type : int8_t {
    pt_gs_k,
    pt_gs_k_opt,
    pt_ss_k,
    pt_ss_k_opt,
    pt_hs_k,
    pt_hs_k_opt,
    pt_hps_k,
    pt_hps_k_opt,
    r_pm_gs_k,
    r_pm_gs_k_opt,
    pt_st_k,
    pt_st_k_opt,
    pt_st_hbv,
    pt_st_hbv_opt,
    r_pt_gs_k,
    r_pt_gs_k_opt
  };

  /** @brief calibration status informs about the drms calibration process
   */
  struct calibration_status {
    vector<parameter_variant_t> p_trace; ///< the current parameter trace
    vector<double> f_trace;              ///< the current goal function trace
    bool running{false};                 ///< true if still running otherwise false
    parameter_variant_t p_result;        ///< eventual results
    x_serialize_decl();
  };

  enum optimizer_method : int8_t {
    BOBYQA,
    GLOBAL,
    DREAM,
    SCEUA
  };

  /** @brief optimizer parameter and selector
   *
   * @details the purpose to help the drms system protocol by keeping
   * a data-description of the wanted optimizer along with options
   * that differs slightly between each of them.
   */
  struct calibration_options {
    optimizer_method method{optimizer_method::BOBYQA};
    // common
    size_t max_n_iterations{1500};
    utctime time_limit{0};
    // global
    double solver_epsilon{0.0001};
    // sceua
    double x_epsilon{0.0001};
    double y_epsilon{0.0001};
    // bobyqa
    double tr_start{0.1};
    double tr_stop{1e-5};
    calibration_options() = default;

    calibration_options(
      optimizer_method m,
      size_t max_n_iterations = 1500,
      utctime time_limit = utctime(0),
      double solver_epsilon = 0.001,
      double x_epsilon = 0.001,
      double y_epsilon = 0.001,
      double tr_start = 0.1,
      double tr_stop = 1e-5)
      : method{m}
      , max_n_iterations{max_n_iterations}
      , time_limit{time_limit}
      , solver_epsilon{solver_epsilon}
      , x_epsilon{x_epsilon}
      , y_epsilon{y_epsilon}
      , tr_start{tr_start}
      , tr_stop{tr_stop} {
    }

    // dreams have currently no specific limits.
    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::hydrology::srv::calibration_options);
x_serialize_export_key(shyft::hydrology::srv::calibration_status);

x_serialize_export_key(shyft::hydrology::srv::state_model);
x_serialize_export_key(shyft::hydrology::srv::parameter_model);

template <typename Char>
struct fmt::formatter<shyft::hydrology::srv::state_model, Char>
  : shyft::fn_formatter<&shyft::hydrology::srv::state_model::id, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::hydrology::srv::state_model>, Char>
  : shyft::ptr_formatter<shyft::hydrology::srv::state_model, Char> { };

template <typename Char>
struct fmt::formatter<shyft::hydrology::srv::parameter_model, Char>
  : shyft::fn_formatter<&shyft::hydrology::srv::parameter_model::id, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::hydrology::srv::parameter_model>, Char>
  : shyft::ptr_formatter<shyft::hydrology::srv::parameter_model, Char> { };
