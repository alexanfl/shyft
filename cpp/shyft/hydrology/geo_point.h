/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cmath>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/core/cs.hpp>
#include <shyft/core/epsg_fast.h>
#include <shyft/core/core_serialization.h>

namespace shyft::core {

  /**
   * @brief GeoPoint a 3D point
   *
   * @details
   *  Commonly used in the shyft::core for
   *  representing a 3D point in the terrain model.
   *  Primary usage is in the dtss/geo functionality and the interpolation routines.
   *
   *  The point model aiming for efficiency and simplicity.
   *
   *  Units of x,y,z are metric, z positive upwards to sky, represents elevation
   *  x is east-west axis
   *  y is south-north axis
   */
  struct geo_point {
    double x, y, z;

    geo_point()
      : x(0)
      , y(0)
      , z(0) {
    }

    geo_point(double x, double y = 0.0, double z = 0.0)
      : x(x)
      , y(y)
      , z(z) {
    }

    bool operator==(geo_point const &o) const {
      double const accuracy = 1e-3; // mm
      return distance2(*this, o) < accuracy;
    }

    bool operator!=(geo_point const &o) const {
      return !operator==(o);
    }

    /**
     * @brief Squared eucledian distance
     *
     * @return the 3D distance^2 between supplied arguments a and b
     */
    static inline double distance2(geo_point const &a, geo_point const &b) {
      return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z);
    }

    /**
     * @brief Distance measure on the form sum(abs(a.x - b.x)^p + abs(a.y - b.y)^p + (zscale * abs(a.z - b.z))^p)
     *
     * @return the 3D distance measure between supplied arguments a and b, by using an optional z scale factor
     */
    static inline double distance_measure(geo_point const &a, geo_point const &b, double p, double zscale) {
      double const d = (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)
                     + (a.z - b.z) * (a.z - b.z) * zscale * zscale;
      return fabs(p - 2.0) < 1e-8 ? d : std::pow(d, p / 2.0);
    }

    /**
     * @brief Z scaled, non-eucledian distance between points a and b.
     *
     * @return sqrt( (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale)
     */
    static inline double zscaled_distance(geo_point const &a, geo_point const &b, double zscale) {
      return std::sqrt(
        (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z) * zscale * zscale);
    }

    /**
     * @brief Z scaled, non-eucledian distance between points a and b.
     *
     * @return  (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale
     */
    static inline double zscaled_distance2(geo_point const &a, geo_point const &b, double zscale) {
      return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z) * zscale * zscale;
    }

    /** @brief Euclidian distance between points a and b, first projected onto x-y plane.
     *
     * return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y))
     */
    static inline double xy_distance(geo_point const &a, geo_point const &b) {
      return std::sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    /** @brief Euclidian distance^2 between points a and b, first projected onto x-y plane.
     *
     * return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y)
     */
    static inline double xy_distance2(geo_point const &a, geo_point const &b) {
      return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
    }

    static inline double z_distance2(geo_point const &a, geo_point const &b) {
      return (a.z - b.z) * (a.z - b.z);
    }

    /** @brief Difference between points a and b, i.e. a - b. Should perhaps return a
     * GeoDifference instead for mathematical correctness.
     *
     * @return GeoPoint(a.x - b.x, a.y - b.y, a.z - b.z)
     */
    static inline geo_point difference(geo_point const &a, geo_point const &b) {
      return geo_point(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    x_serialize_decl();
  };

  /** @brief plain struct to represent longitude, latitude pairs */
  struct geo_point_ll {
    double longitude{0.0};
    double latitude{0.0};
  };

  /** Given geo_point and it's epsg_id, provide the longitude, latitude coord of the points */
  extern geo_point_ll convert_to_longitude_latitude(geo_point const &p, int geo_epsg_id);

  /** Given a geo_point, from_epsg to_epsg, do the transform of the x-y part */
  extern geo_point epsg_map(geo_point a_, int from_epsg, int to_epsg);

  namespace detail {
    namespace bg = boost::geometry;
    using point_ll =
      bg::model::point<double, 2, bg::cs::geographic<bg::degree> >; ///< point (latitude,longitude),  epsg 4326 ,wgs 84
    using point_xy =
      bg::model::point<double, 2, bg::cs::cartesian>; /// < x and y of geo_point, in some metric epsg zone, utm.. etc.

    template <class pxy>
    pxy epsg_map(pxy a, int from_epsg, int to_epsg) {
      if (from_epsg == to_epsg) {
        return a;
      } else {
        bg::srs::projection<> a_prj{bg::srs::epsg_fast{from_epsg}};
        point_ll a_ll;
        a_prj.inverse(a, a_ll);
        bg::srs::projection<> r_prj{bg::srs::epsg_fast{to_epsg}};
        pxy r;
        r_prj.forward(a_ll, r);
        return r;
      }
    }

    extern template point_xy epsg_map<point_xy>(point_xy, int from_epsg, int to_epsg);
  }
}

//-- serialization support shyft
x_serialize_export_key(shyft::core::geo_point);
