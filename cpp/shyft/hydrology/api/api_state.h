/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <stdexcept>
#include <shyft/core/core_serialization.h>

#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/cell_model.h>
//-include stacks here, we need to make cell_state_with_id serializable
#include <shyft/hydrology/stacks/r_pm_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/r_pt_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_hs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_st_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_st_hbv_cell_model.h>
#include <shyft/hydrology/stacks/pt_ss_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_hps_k_cell_model.h>

namespace shyft::api {

  /** @brief unique pseudo identifier of cell state
   *
   * A lot of Shyft models can appear in different geometries and
   * resolutions. In a few rare cases we would like to store the *state* of
   * a model. This would be typical for example for hydrological new-year 1-sept in Norway.
   * To ensure that each cell-state have a unique identifier so that we never risk
   * mixing state from different cells or different geometries, we create a pseudo unique
   * id that helps identifying unique cell-states given this usage and context.
   *
   * The primary usage is to identify which cell a specific identified state belongs to.
   *
   */
  struct cell_state_id {
    int64_t cid;               ///< the catchment id, if entirely different model, this might change
    int64_t x;                 ///< the cell mid-point x (west-east), truncated to integer [meter]
    int64_t y;                 ///< the cell mid-point y (south-north), truncated to integer [meter]
    int64_t area;              ///< the area in m[m2], - if different cell geometries, this changes
    cell_state_id() = default; // python exposure

    cell_state_id(int64_t cid, int64_t x, int64_t y, int64_t area)
      : cid(cid)
      , x(x)
      , y(y)
      , area(area) {
    }

    bool operator==(cell_state_id const & o) const {
      return cid == o.cid && x == o.x && y == o.y && area == o.area;
    }

    bool operator!=(cell_state_id const & o) const {
      return !operator==(o);
    }

    bool operator<(cell_state_id const & o) const {
      if (cid < o.cid)
        return true;
      if (cid > o.cid)
        return false;
      if (x < o.x)
        return true;
      if (x > o.x)
        return false;
      if (y < o.y)
        return true;
      if (y > o.y)
        return false;
      return area < o.area;
    }

    x_serialize_decl();
  };

  /** create the cell_state_id based on specified cell.geo part*/
  inline cell_state_id cell_state_id_of(shyft::core::geo_cell_data const & c_geo) {
    return cell_state_id(
      c_geo.catchment_id(), (int) c_geo.mid_point().x, (int) c_geo.mid_point().y, (int) c_geo.area());
  }

  /** A cell state with a cell_state identifier */
  template <class CS>
  struct cell_state_with_id {
    typedef CS cell_state_t;
    cell_state_id id;
    cell_state_t state;
    cell_state_with_id(){};

    cell_state_with_id(cell_state_id idx, cell_state_t const & statex)
      : id{idx}
      , state{statex} {
    }

    bool operator==(const cell_state_with_id o) const {
      return id == o.id; // only id equality, for vector support in boost python
    }

    /** do the magic given a cell, create the id, stash away the id:state*/
    template <class C>
    explicit cell_state_with_id(C const & c)
      : id(cell_state_id_of(c.geo))
      , state(c.state) {
    }

    x_serialize_decl();
  };


  template <class CS>
  std::vector<char> serialize_to_bytes(std::shared_ptr<std::vector<CS>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_gs_k::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pm_gs_k::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pt_gs_k::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_ss_k::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hs_k::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_st_k::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_st_hbv::state>>> const & states);
  extern template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hps_k::state>>> const & states);

  template <class CS>
  void deserialize_from_bytes(std::vector<char> const & bytes, std::shared_ptr<std::vector<CS>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_gs_k::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pm_gs_k::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pt_gs_k::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hs_k::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_st_k::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_st_hbv::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_ss_k::state>>>& states);
  extern template void deserialize_from_bytes(
    std::vector<char> const & bytes,
    std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hps_k::state>>>& states);

  using cids_t = std::vector<int64_t>;

  /** @brief state_io_handler for efficient handling of cell-identified states
   *
   * This class provides functionality to extract/apply state based on a
   * pseudo unique id of each cell.
   * A cell is identified by the integer portions of:
   * catchment_id, mid_point.x,mid_point.y,area
   */
  template <class C>
  struct state_io_handler {
    using state_t = typename C::state_t;
    using cell_state_id_t = cell_state_with_id<state_t>;
    std::shared_ptr<std::vector<C>> cells; // shared alias region model cells.

    state_io_handler() {
    }

    /** construct a handler for the cells provided*/
    explicit state_io_handler(std::shared_ptr<std::vector<C>> const & cellx)
      : cells(cellx) {
    }

    /** Extract cell identified state
     *@return the state for the cells, optionally filtered by the supplied catchment ids (cids)
     */
    std::shared_ptr<std::vector<cell_state_id_t>> extract_state(cids_t const & cids) const {
      if (!cells)
        throw std::runtime_error("No cells to extract state from");
      auto r = std::make_shared<std::vector<cell_state_id_t>>();
      r->reserve(cells->size());
      for (auto const & c : *cells)
        if (cids.size() == 0 || std::find(cids.begin(), cids.end(), c.geo.catchment_id()) != cids.end())
          r->emplace_back(c); // creates a a cell_state_with_id based on cell
      return r;
    }

    /** Restore cell identified state, filtered by cids.
     * @return a list identifying states that where not applied to cells(filtering out all that is related to
     * non-matching cids)
     */
    cids_t apply_state(std::shared_ptr< std::vector<cell_state_id_t> > const & s, cids_t const & cids) {
      if (!cells)
        throw std::runtime_error("No cells to apply state into");
      std::map<cell_state_id, C*> cmap; // yes store pointers, we know the scope is this routine
      for (auto& c : *cells) {
        if (cids.size() == 0 || std::find(cids.begin(), cids.end(), c.geo.catchment_id()) != cids.end())
          cmap[cell_state_id_of(c.geo)] = &c; // fix the map
      }
      cids_t missing;
      for (size_t i = 0; i < s->size(); ++i) {
        if (cids.size() == 0 || std::find(cids.begin(), cids.end(), (*s)[i].id.cid) != cids.end()) {
          auto f = cmap.find((*s)[i].id); // log(n)
          if (f != cmap.end())
            f->second->state = (*s)[i].state;
          else
            missing.push_back(i);
        }
      }
      return missing;
    }
  };

  using namespace shyft::core;
  extern template struct state_io_handler<pt_gs_k::cell_complete_response_t>;
  extern template struct state_io_handler<pt_gs_k::cell_discharge_response_t>;
  extern template struct state_io_handler<r_pm_gs_k::cell_complete_response_t>;
  extern template struct state_io_handler<r_pm_gs_k::cell_discharge_response_t>;
  extern template struct state_io_handler<r_pt_gs_k::cell_complete_response_t>;
  extern template struct state_io_handler<r_pt_gs_k::cell_discharge_response_t>;
  extern template struct state_io_handler<pt_ss_k::cell_complete_response_t>;
  extern template struct state_io_handler<pt_ss_k::cell_discharge_response_t>;
  extern template struct state_io_handler<pt_hs_k::cell_complete_response_t>;
  extern template struct state_io_handler<pt_hs_k::cell_discharge_response_t>;
  extern template struct state_io_handler<pt_st_k::cell_complete_response_t>;
  extern template struct state_io_handler<pt_st_k::cell_discharge_response_t>;
  extern template struct state_io_handler<pt_st_hbv::cell_complete_response_t>;
  extern template struct state_io_handler<pt_st_hbv::cell_discharge_response_t>;
  extern template struct state_io_handler<pt_hps_k::cell_complete_response_t>;
  extern template struct state_io_handler<pt_hps_k::cell_discharge_response_t>;

}

//-- serialization support shyft
x_serialize_export_key(shyft::api::cell_state_id);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::pt_gs_k::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::r_pm_gs_k::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::r_pt_gs_k::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::pt_ss_k::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::pt_hs_k::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::pt_st_k::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::pt_st_hbv::state>);
x_serialize_export_key(shyft::api::cell_state_with_id<shyft::core::pt_hps_k::state>);
