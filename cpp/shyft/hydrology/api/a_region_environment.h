/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <stdexcept>


#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/geo_point.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/geo_ts.h>

namespace shyft::api {
  using namespace shyft::core;

  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::geo_ts;

  /**
   * @brief GeoPointSource
   * @details contains common properties, functions
   * for the geo-point located time-series.
   * Typically it contains a GeoPoint (3d position), plus a timeseries
   */
  struct GeoPointSource {
    using ts_t = apoint_ts;
    using geo_point_t = geo_point;

    GeoPointSource() = default;

    GeoPointSource(geo_point const & midpoint, apoint_ts const & ts)
      : mid_point_(midpoint)
      , ts(ts) {
    }

    GeoPointSource(geo_ts const & gts)
      : mid_point_(gts.mid_point)
      , ts(gts.ts) {
    }

    virtual ~GeoPointSource() {
    }

    geo_point mid_point_;
    apoint_ts ts;
    std::string uid; ///< user-defined id, for external mapping,

    // boost python fixes for attributes and shared_ptr
    apoint_ts get_ts() {
      return ts;
    }

    void set_ts(apoint_ts x) {
      ts = x;
    }

    bool is_equal(GeoPointSource const & x) const {
      return uid == x.uid && mid_point_ == x.mid_point_ && ts == x.ts;
    }

    geo_point mid_point() const {
      return mid_point_;
    }

    bool operator==(GeoPointSource const & x) const {
      return is_equal(x);
    }

    bool operator!=(GeoPointSource const & o) const {
      return !operator==(o);
    }

    geo_ts get_geo_ts() const {
      return geo_ts{mid_point_, ts};
    }

    x_serialize_decl();
  };

  struct TemperatureSource : GeoPointSource {
    TemperatureSource() = default;

    TemperatureSource(geo_point const & p, apoint_ts const & ts)
      : GeoPointSource(p, ts) {
    }

    TemperatureSource(geo_ts const & gts)
      : GeoPointSource(gts) {
    }

    apoint_ts const & temperatures() const {
      return ts;
    }

    void set_temperature(size_t ix, double v) {
      ts.set(ix, v);
    } // btk dst compliant signature, used during btk-interpolation in gridpp exposure

    void set_value(size_t ix, double v) {
      ts.set(ix, v);
    }

    bool operator==(TemperatureSource const & x) const {
      return is_equal(x);
    }

    bool operator!=(TemperatureSource const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  struct PrecipitationSource : GeoPointSource {
    PrecipitationSource() = default;

    PrecipitationSource(geo_point const & p, apoint_ts const & ts)
      : GeoPointSource(p, ts) {
    }

    PrecipitationSource(geo_ts const & gts)
      : GeoPointSource(gts) {
    }

    apoint_ts const & precipitations() const {
      return ts;
    }

    void set_value(size_t ix, double v) {
      ts.set(ix, v);
    }

    bool operator==(PrecipitationSource const & x) const {
      return is_equal(x);
    }

    bool operator!=(PrecipitationSource const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  struct WindSpeedSource : GeoPointSource {
    WindSpeedSource() = default;

    WindSpeedSource(geo_point const & p, apoint_ts const & ts)
      : GeoPointSource(p, ts) {
    }

    WindSpeedSource(geo_ts const & gts)
      : GeoPointSource(gts) {
    }

    bool operator==(WindSpeedSource const & x) const {
      return is_equal(x);
    }

    bool operator!=(WindSpeedSource const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  struct RelHumSource : GeoPointSource {
    RelHumSource() = default;

    RelHumSource(geo_point const & p, apoint_ts const & ts)
      : GeoPointSource(p, ts) {
    }

    RelHumSource(geo_ts const & gts)
      : GeoPointSource(gts) {
    }

    bool operator==(RelHumSource const & x) const {
      return is_equal(x);
    }

    bool operator!=(RelHumSource const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  struct RadiationSource : GeoPointSource {
    RadiationSource() = default;

    RadiationSource(geo_point const & p, apoint_ts const & ts)
      : GeoPointSource(p, ts) {
    }

    RadiationSource(geo_ts const & gts)
      : GeoPointSource(gts) {
    }

    bool operator==(RadiationSource const & x) const {
      return is_equal(x);
    }

    x_serialize_decl();
  };

  struct a_region_environment {
    using precipitation_t = PrecipitationSource;
    using temperature_t = TemperatureSource;
    using radiation_t = RadiationSource;
    using rel_hum_t = RelHumSource;
    using wind_speed_t = WindSpeedSource;

    /** make vectors non nullptr by  default */
    a_region_environment() {
      temperature = std::make_shared<std::vector<TemperatureSource>>();
      precipitation = std::make_shared<std::vector<PrecipitationSource>>();
      radiation = std::make_shared<std::vector<RadiationSource>>();
      rel_hum = std::make_shared<std::vector<RelHumSource>>();
      wind_speed = std::make_shared<std::vector<WindSpeedSource>>();
    }

    std::shared_ptr<std::vector<TemperatureSource>> temperature;
    std::shared_ptr<std::vector<PrecipitationSource>> precipitation;
    std::shared_ptr<std::vector<RadiationSource>> radiation;
    std::shared_ptr<std::vector<WindSpeedSource>> wind_speed;
    std::shared_ptr<std::vector<RelHumSource>> rel_hum;
    bool operator==(a_region_environment const & o) const;

    bool operator!=(a_region_environment const & o) const {
      return !operator==(o);
    }

    // our boost python needs these methods to get properties straight (most likely it can be fixed by other means but..)
    std::shared_ptr<std::vector<TemperatureSource>> get_temperature() {
      return temperature;
    }

    void set_temperature(std::shared_ptr<std::vector<TemperatureSource>> x) {
      temperature = x;
    }

    std::shared_ptr<std::vector<PrecipitationSource>> get_precipitation() {
      return precipitation;
    }

    void set_precipitation(std::shared_ptr<std::vector<PrecipitationSource>> x) {
      precipitation = x;
    }

    std::shared_ptr<std::vector<RadiationSource>> get_radiation() {
      return radiation;
    }

    void set_radiation(std::shared_ptr<std::vector<RadiationSource>> x) {
      radiation = x;
    }

    std::shared_ptr<std::vector<WindSpeedSource>> get_wind_speed() {
      return wind_speed;
    }

    void set_wind_speed(std::shared_ptr<std::vector<WindSpeedSource>> x) {
      wind_speed = x;
    }

    std::shared_ptr<std::vector<RelHumSource>> get_rel_hum() {
      return rel_hum;
    }

    void set_rel_hum(std::shared_ptr<std::vector<RelHumSource>> x) {
      rel_hum = x;
    }

    std::vector<char> serialize_to_bytes() const;
    static a_region_environment deserialize_from_bytes(vector<char> const & ss);
    x_serialize_decl();
  };


}

x_serialize_export_key(shyft::api::GeoPointSource);
x_serialize_export_key(shyft::api::TemperatureSource);
x_serialize_export_key(shyft::api::PrecipitationSource);
x_serialize_export_key(shyft::api::RadiationSource);
x_serialize_export_key(shyft::api::WindSpeedSource);
x_serialize_export_key(shyft::api::RelHumSource);
x_serialize_export_key(shyft::api::a_region_environment);
