/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/api/api_pch.h>
#include <shyft/hydrology/api/api.h>

namespace shyft::core {
  using shyft::api::a_region_environment;
  ///---------------------------
  //  Expose the stacks as external templates here:
  template class region_model<pt_hps_k::cell_complete_response_t, a_region_environment>;
  template class region_model<pt_hps_k::cell_discharge_response_t, a_region_environment>;

}
