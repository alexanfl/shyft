/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/api/api_pch.h>
#include <shyft/hydrology/api/api.h>

namespace shyft::core {
  using shyft::api::a_region_environment;
  template class region_model<pt_st_hbv::cell_complete_response_t, a_region_environment>;
  template class region_model<pt_st_hbv::cell_discharge_response_t, a_region_environment>;
}
