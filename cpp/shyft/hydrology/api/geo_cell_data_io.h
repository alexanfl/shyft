/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <stdexcept>


#include <shyft/hydrology/geo_cell_data.h>

namespace shyft::api {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  // using std::map;
  using std::shared_ptr;
  using std::runtime_error;
  using std::invalid_argument;

  /**
   * @brief geo_cell_data_io provide fast conversion to-from string
   *
   * @details
   * In the context of orchestration/repository, we found that it could be useful to
   * cache information as retrieved from a GIS system
   *
   * This only makes sense to keep as long as we need it for performance reasons
   *
   */
  struct geo_cell_data_io {
    static size_t size() {
      return 11;
    } // number of doubles to store a gcd

    static size_t tin_size() {
      return 15;
    } // number of doubles to store a TIN gcd

    static void push_to_vector(vector<double>& v, geo_cell_data const & gcd) {
      v.push_back(gcd.mid_point().x);
      v.push_back(gcd.mid_point().y);
      v.push_back(gcd.mid_point().z);
      v.push_back(gcd.area());
      v.push_back(int(gcd.catchment_id()));
      v.push_back(gcd.radiation_slope_factor());
      v.push_back(gcd.land_type_fractions_info().glacier());
      v.push_back(gcd.land_type_fractions_info().lake());
      v.push_back(gcd.land_type_fractions_info().reservoir());
      v.push_back(gcd.land_type_fractions_info().forest());
      v.push_back(
        gcd.land_type_fractions_info().unspecified()); // not really needed, since it can be computed from 1- theothers
    }

    static vector<double> to_vector(geo_cell_data const & gcd) {
      vector<double> v;
      v.reserve(11);
      push_to_vector(v, gcd);
      return v;
    }

    static geo_cell_data from_raw_vector(double const * v) {
      land_type_fractions ltf;
      ltf.set_fractions(v[6], v[7], v[8], v[9]);
      //                               x    y    z    a    cid       rsl
      return geo_cell_data(geo_point(v[0], v[1], v[2]), v[3], int(v[4]), v[5], ltf);
    }

    static geo_cell_data from_vector(vector<double> const & v) {
      if (v.size() != size())
        throw std::invalid_argument(
          "geo_cell_data_io::from_vector: size of vector must be equal to geo_cell_data_io::size()");
      return from_raw_vector(v.data());
    }

    // TIN -- in case it is useful.
    static geo_cell_data from_raw_vector_to_tin(double const * v) {
      // the v layout should be like this, all double
      //  012  345 678    9          10  11,12,13,14
      //   p1   p2    p3   epsg_id cid   ltf
      land_type_fractions ltf;
      ltf.set_fractions(v[11], v[12], v[13], v[14]); // automagically figure out monocell or not
      return geo_cell_data(
        geo_point(v[0], v[1], v[2]),
        geo_point(v[3], v[4], v[5]),
        geo_point(v[6], v[7], v[8]),
        int64_t(v[9]),
        (v[10]),
        ltf);
    }
  };

}
