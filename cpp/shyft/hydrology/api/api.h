/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <stdexcept>


/**
 * This file contains mostly typedefs and some few helper classes to
 * provide a python friendly header file to the api.python project.
 *
 *
 */

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>
#include <shyft/hydrology/spatial/bayesian_kriging.h>
#include <shyft/hydrology/spatial/inverse_distance.h>

#include <shyft/hydrology/stacks/pt_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/r_pm_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/r_pt_gs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_hs_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_st_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_st_hbv_cell_model.h>
#include <shyft/hydrology/stacks/pt_ss_k_cell_model.h>
#include <shyft/hydrology/stacks/pt_hps_k_cell_model.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/hydrology/api/a_region_environment.h>

namespace shyft::api {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::map;
  using std::shared_ptr;
  using std::runtime_error;
  using std::invalid_argument;

  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::geo_ts;
  using shyft::core::stat_scope;

  using result_ts_t = shyft::time_series::point_ts<time_axis::fixed_dt>;
  using result_ts_t_ = std::shared_ptr<result_ts_t>;
  using core::cell_statistics;
  using cids_t = std::vector<int64_t>;

}

namespace shyft::core {
  using shyft::api::a_region_environment;
  ///---------------------------
  //  Expose the stacks as external templates here:
  extern template class region_model<pt_gs_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<pt_gs_k::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<pt_ss_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<pt_ss_k::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<pt_hs_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<pt_hs_k::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<pt_hps_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<pt_hps_k::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<r_pm_gs_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<r_pm_gs_k::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<pt_st_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<pt_st_k::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<pt_st_hbv::cell_complete_response_t, a_region_environment>;
  extern template class region_model<pt_st_hbv::cell_discharge_response_t, a_region_environment>;

  extern template class region_model<r_pt_gs_k::cell_complete_response_t, a_region_environment>;
  extern template class region_model<r_pt_gs_k::cell_discharge_response_t, a_region_environment>;

}
