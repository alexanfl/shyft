#pragma once
#include <shyft/hydrology/api/api.h>

namespace shyft::api {

  template <typename cell>
  struct basic_cell_statistics {
    shared_ptr<vector<cell>> cells;

    explicit basic_cell_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    double total_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (size_t j = 0; j < cells->size(); ++j) {
            auto const & c = (*cells)[j];
            if (cell_statistics::is_match(c, ix_type, cid, j))
              sum += c.geo.area();
          }
      }
      return sum;
    }

    double forest_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area() * c.geo.land_type_fractions_info().forest();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid)
              sum += c.geo.area() * c.geo.land_type_fractions_info().forest();
      }
      return sum;
    }

    double glacier_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area() * c.geo.land_type_fractions_info().glacier();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid)
              sum += c.geo.area() * c.geo.land_type_fractions_info().glacier();
      }
      return sum;
    }

    double lake_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area() * c.geo.land_type_fractions_info().lake();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid)
              sum += c.geo.area() * c.geo.land_type_fractions_info().lake();
      }
      return sum;
    }

    double reservoir_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area() * c.geo.land_type_fractions_info().reservoir();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid)
              sum += c.geo.area() * c.geo.land_type_fractions_info().reservoir();
      }
      return sum;
    }

    double unspecified_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area() * c.geo.land_type_fractions_info().unspecified();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid)
              sum += c.geo.area() * c.geo.land_type_fractions_info().unspecified();
      }
      return sum;
    }

    double snow_storage_area(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells)
          sum += c.geo.area() * c.geo.land_type_fractions_info().snow_storage();
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid)
              sum += c.geo.area() * c.geo.land_type_fractions_info().snow_storage();
      }
      return sum;
    }

    double elevation(cids_t const & indexes, stat_scope ix_type) const {
      double sum = 0.0;
      double area_sum = 0;
      if (indexes.size() == 0) {
        for (auto const & c : *cells) {
          sum += c.geo.mid_point().z * c.geo.area();
          area_sum += c.geo.area();
        }
      } else {
        cell_statistics::verify_cids_exist(*cells, indexes, ix_type);
        for (auto cid : indexes)
          for (auto const & c : *cells)
            if ((int) c.geo.catchment_id() == cid) {
              sum += c.geo.mid_point().z * c.geo.area();
              area_sum += c.geo.area();
            }
      }
      return sum / area_sum;
    }

    apoint_ts discharge(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.avg_discharge;
        },
        ix_type));
    }

    vector<double> discharge(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.avg_discharge;
        },
        ith_timestep,
        ix_type);
    }

    double discharge_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.avg_discharge;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts charge(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.charge_m3s;
        },
        ix_type));
    }

    vector<double> charge(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.charge_m3s;
        },
        ith_timestep,
        ix_type);
    }

    double charge_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.charge_m3s;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts snow_swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ix_type));
    }

    vector<double> snow_swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    double snow_swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts snow_sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ix_type));
    }

    vector<double> snow_sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    double snow_sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts temperature(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.temperature;
        },
        ix_type));
    }

    vector<double> temperature(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.temperature;
        },
        ith_timestep,
        ix_type);
    }

    double temperature_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.temperature;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts precipitation(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.precipitation;
        },
        ix_type));
    }

    vector<double> precipitation(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.precipitation;
        },
        ith_timestep,
        ix_type);
    }

    double precipitation_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.precipitation;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts radiation(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.radiation;
        },
        ix_type));
    }

    vector<double> radiation(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.radiation;
        },
        ith_timestep,
        ix_type);
    }

    double radiation_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.radiation;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts wind_speed(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.wind_speed;
        },
        ix_type));
    }

    vector<double> wind_speed(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.wind_speed;
        },
        ith_timestep,
        ix_type);
    }

    double wind_speed_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.wind_speed;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts rel_hum(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.rel_hum;
        },
        ix_type));
    }

    vector<double> rel_hum(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.rel_hum;
        },
        ith_timestep,
        ix_type);
    }

    double rel_hum_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.env_ts.rel_hum;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct kirchner_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit kirchner_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts discharge(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.kirchner_discharge;
        },
        ix_type));
    }

    vector<double> discharge(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.kirchner_discharge;
        },
        ith_timestep,
        ix_type);
    }

    double discharge_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.kirchner_discharge;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct hbv_soil_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_soil_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts sm(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.soil_sm;
        },
        ix_type));
    }

    vector<double> sm(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.soil_sm;
        },
        ith_timestep,
        ix_type);
    }

    double sm_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.soil_sm;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct hbv_tank_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_tank_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts uz(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.tank_uz;
        },
        ix_type));
    }

    vector<double> uz(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.tank_uz;
        },
        ith_timestep,
        ix_type);
    }

    double uz_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.tank_uz;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts lz(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.tank_lz;
        },
        ix_type));
    }

    vector<double> lz(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.tank_lz;
        },
        ith_timestep,
        ix_type);
    }

    double lz_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.tank_lz;
        },
        ith_timestep,
        ix_type);
    }
  };

  ///< cells with gamma_snow state collection gives access to time-series for state
  template <typename cell>
  struct gamma_snow_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit gamma_snow_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts albedo(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_albedo;
        },
        ix_type));
    }

    vector<double> albedo(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_albedo;
        },
        ith_timestep,
        ix_type);
    }

    double albedo_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_albedo;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts lwc(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_lwc;
        },
        ix_type));
    }

    vector<double> lwc(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_lwc;
        },
        ith_timestep,
        ix_type);
    }

    double lwc_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_lwc;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts surface_heat(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_surface_heat;
        },
        ix_type));
    }

    vector<double> surface_heat(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_surface_heat;
        },
        ith_timestep,
        ix_type);
    }

    double surface_heat_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_surface_heat;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts alpha(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_alpha;
        },
        ix_type));
    }

    vector<double> alpha(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_alpha;
        },
        ith_timestep,
        ix_type);
    }

    double alpha_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_alpha;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sdc_melt_mean(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_sdc_melt_mean;
        },
        ix_type));
    }

    vector<double> sdc_melt_mean(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_sdc_melt_mean;
        },
        ith_timestep,
        ix_type);
    }

    double sdc_melt_mean_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_sdc_melt_mean;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts acc_melt(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_acc_melt;
        },
        ix_type));
    }

    vector<double> acc_melt(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_acc_melt;
        },
        ith_timestep,
        ix_type);
    }

    double acc_melt_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_acc_melt;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts iso_pot_energy(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_iso_pot_energy;
        },
        ix_type));
    }

    vector<double> iso_pot_energy(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_iso_pot_energy;
        },
        ith_timestep,
        ix_type);
    }

    double iso_pot_energy_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_iso_pot_energy;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts temp_swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_temp_swe;
        },
        ix_type));
    }

    vector<double> temp_swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_temp_swe;
        },
        ith_timestep,
        ix_type);
    }

    double temp_swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.gs_temp_swe;
        },
        ith_timestep,
        ix_type);
    }
  };

  ///< cells with gamma_snow response give access to time-series for these
  template <typename cell>
  struct gamma_snow_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit gamma_snow_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ix_type));
    }

    vector<double> swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    double swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts outflow(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ix_type));
    }

    vector<double> outflow(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    double outflow_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts glacier_melt(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ix_type));
    }

    vector<double> glacier_melt(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }

    double glacier_melt_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }
  };

  ///<-- universal snow section

  ///<-- end universal snow section
  ///< access to skaugen's snow routine's state statistics
  template <typename cell>
  struct skaugen_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit skaugen_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts alpha(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_alpha;
        },
        ix_type));
    }

    vector<double> alpha(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_alpha;
        },
        ith_timestep,
        ix_type);
    }

    double alpha_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_alpha;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts nu(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_nu;
        },
        ix_type));
    }

    vector<double> nu(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_nu;
        },
        ith_timestep,
        ix_type);
    }

    double nu_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_nu;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts lwc(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_lwc;
        },
        ix_type));
    }

    vector<double> lwc(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_lwc;
        },
        ith_timestep,
        ix_type);
    }

    double lwc_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_lwc;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts residual(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_residual;
        },
        ix_type));
    }

    vector<double> residual(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_residual;
        },
        ith_timestep,
        ix_type);
    }

    double residual_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_residual;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_swe;
        },
        ix_type));
    }

    vector<double> swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    double swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_sca;
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }
  };

  ///< access to skaugen's snow routine response statistics
  template <typename cell>
  struct skaugen_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit skaugen_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts outflow(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ix_type));
    }

    vector<double> outflow(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    double outflow_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts total_stored_water(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ix_type));
    }

    vector<double> total_stored_water(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    double total_stored_water_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts glacier_melt(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ix_type));
    }

    vector<double> glacier_melt(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }

    double glacier_melt_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }
  };

  ///< access to hbv snow routine's state statistics
  template <typename cell>
  struct hbv_snow_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_snow_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(std::move(cells)) {
    }

    apoint_ts swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_swe;
        },
        ix_type));
    }

    vector<double> swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    double swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_sca;
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    size_t find_size(cids_t const & indexes, stat_scope /*ix_type.. regardless ix-type,this is size of sp */) const {
      if (indexes.size() == 0)
        return (*cells)[0].sc.sp.size();
      auto ci = indexes[0];
      for (auto const & c : *cells) {
        if (c.geo.catchment_id() == ci)
          return c.sc.sp.size();
      }
      return 0;
    }

    ats_vector sp(cids_t const & indexes, stat_scope ix_type) const {
      ats_vector r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(apoint_ts(*cell_statistics::average_catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sp[i];
          },
          ix_type)));
      }
      return r;
    }

    vector<vector<double>> sp(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<vector<double>> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sp[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    vector<double> sp_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<double> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::average_catchment_feature_value(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sp[i];
          },
          ith_timestep));
      }
      return r;
    }

    ats_vector sw(cids_t const & indexes, stat_scope ix_type) const {
      ats_vector r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(apoint_ts(*cell_statistics::average_catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sw[i];
          },
          ix_type)));
      }
      return r;
    }

    vector<vector<double>> sw(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<vector<double>> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sw[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    vector<double> sw_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<double> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::average_catchment_feature_value(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sw[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }
  };

  ///< access to hbv snow routine response statistics
  template <typename cell>
  struct hbv_snow_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_snow_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts outflow(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ix_type));
    }

    vector<double> outflow(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    double outflow_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts glacier_melt(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ix_type));
    }

    vector<double> glacier_melt(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }

    double glacier_melt_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }
  };

  ///< access to snow tiles routine response statistics
  template <typename cell>
  struct snow_tiles_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit snow_tiles_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts outflow(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ix_type));
    }

    vector<double> outflow(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    double outflow_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_outflow;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ix_type));
    }

    vector<double> swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    double swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.snow_sca;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts glacier_melt(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ix_type));
    }

    vector<double> glacier_melt(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }

    double glacier_melt_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct snow_tiles_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    explicit snow_tiles_cell_state_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.swe(c.parameter->st, c.geo.land_type_fractions_info().snow_storage());
        },
        ix_type));
    }

    vector<double> swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.swe(c.parameter->st, c.geo.land_type_fractions_info().snow_storage());
        },
        ith_timestep,
        ix_type);
    }

    double swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.swe(c.parameter->st, c.geo.land_type_fractions_info().snow_storage());
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.sca(c.parameter->st);
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.sca(c.parameter->st);
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.sca(c.parameter->st);
        },
        ith_timestep,
        ix_type);
    }
  };

  ///< access to hbv physical snow routine's state statistics
  template <typename cell>
  struct hbv_physical_snow_cell_state_statistics {
    shared_ptr<vector<cell>> cells;

    hbv_physical_snow_cell_state_statistics(shared_ptr<vector<cell>> cells)
      : cells(std::move(cells)) {
    }

    apoint_ts swe(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_swe;
        },
        ix_type));
    }

    vector<double> swe(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_swe;
        },
        ith_timestep,
        ix_type);
    }

    double swe_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_swe;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts sca(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_sca;
        },
        ix_type));
    }

    vector<double> sca(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_sca;
        },
        ith_timestep,
        ix_type);
    }

    double sca_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_sca;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts surface_heat(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_surface_heat;
        },
        ix_type));
    }

    vector<double> surface_heat(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_surface_heat;
        },
        ith_timestep,
        ix_type);
    }

    double surface_heat_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.sc.hps_surface_heat;
        },
        ith_timestep,
        ix_type);
    }

    size_t find_size(cids_t const & indexes, stat_scope ix_type) const {
      if (indexes.size() == 0)
        return (*cells)[0].sc.sp.size();
      auto ci = indexes[0];
      for (size_t j = 0; j < cells->size(); ++j) {
        auto const & c = (*cells)[j];
        if (cell_statistics::is_match(c, ix_type, ci, j))
          return c.sc.sp.size();
      }
      return 0;
    }

    ats_vector albedo(cids_t const & indexes, stat_scope ix_type) const {
      ats_vector r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(apoint_ts(*cell_statistics::average_catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.albedo[i];
          },
          ix_type)));
      }
      return r;
    }

    vector<vector<double>> albedo(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<vector<double>> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.albedo[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    vector<double> albedo_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<double> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::average_catchment_feature_value(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.albedo[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    ats_vector iso_pot_energy(cids_t const & indexes, stat_scope ix_type) const {
      ats_vector r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(apoint_ts(*cell_statistics::average_catchment_feature(
          *cells,
          indexes,
          [&](cell const & c) {
            return c.sc.iso_pot_energy[i];
          },
          ix_type)));
      }
      return r;
    }

    vector<vector<double>> iso_pot_energy(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<vector<double>> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.iso_pot_energy[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    vector<double> iso_pot_energy_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<double> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::average_catchment_feature_value(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.iso_pot_energy[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    ats_vector sp(cids_t const & indexes, stat_scope ix_type) const {
      ats_vector r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(apoint_ts(*cell_statistics::average_catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sp[i];
          },
          ix_type)));
      }
      return r;
    }

    vector<vector<double>> sp(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<vector<double>> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sp[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    vector<double> sp_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<double> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::average_catchment_feature_value(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sp[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    ats_vector sw(cids_t const & indexes, stat_scope ix_type) const {
      ats_vector r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(apoint_ts(*cell_statistics::average_catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sw[i];
          },
          ix_type)));
      }
      return r;
    }

    vector<vector<double>> sw(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<vector<double>> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::catchment_feature(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sw[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }

    vector<double> sw_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      vector<double> r;
      size_t size = find_size(indexes, ix_type);
      for (size_t i = 0; i < size; ++i) {
        r.push_back(cell_statistics::average_catchment_feature_value(
          *cells,
          indexes,
          [&i](cell const & c) {
            return c.sc.sw[i];
          },
          ith_timestep,
          ix_type));
      }
      return r;
    }
  };

  ///< access to hbv physical snow routine response statistics
  template <typename cell>
  struct hbv_physical_snow_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    hbv_physical_snow_cell_response_statistics(shared_ptr<vector<cell>> cells)
      : cells(cells) {
    }

    apoint_ts outflow(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.hps_outflow;
        },
        ix_type));
    }

    vector<double> outflow(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.hps_outflow;
        },
        ith_timestep,
        ix_type);
    }

    double outflow_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.hps_outflow;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts glacier_melt(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::sum_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ix_type));
    }

    vector<double> glacier_melt(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }

    double glacier_melt_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::sum_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.glacier_melt;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct priestley_taylor_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit priestley_taylor_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts output(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ix_type));
    }

    vector<double> output(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ith_timestep,
        ix_type);
    }

    double output_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct penman_monteith_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit penman_monteith_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts output(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ix_type));
    }

    vector<double> output(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ith_timestep,
        ix_type);
    }

    double output_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct radiation_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit radiation_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts output(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ix_type));
    }

    vector<double> output(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ith_timestep,
        ix_type);
    }

    double output_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.pe_output;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct hbv_soil_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_soil_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts soil_ae(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.soil_ae;
        },
        ix_type));
    }

    vector<double> soil_ae(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.soil_ae;
        },
        ith_timestep,
        ix_type);
    }

    double soil_ae_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.soil_ae;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts inuz(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.inuz;
        },
        ix_type));
    }

    vector<double> inuz(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.inuz;
        },
        ith_timestep,
        ix_type);
    }

    double inuz_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.inuz;
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct hbv_tank_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_tank_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts elake(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.elake;
        },
        ix_type));
    }

    vector<double> elake(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.elake;
        },
        ith_timestep,
        ix_type);
    }

    double elake_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.elake;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts qlz(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.qlz;
        },
        ix_type));
    }

    vector<double> qlz(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.qlz;
        },
        ith_timestep,
        ix_type);
    }

    double qlz_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.qlz;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts quz0(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz0;
        },
        ix_type));
    }

    vector<double> quz0(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz0;
        },
        ith_timestep,
        ix_type);
    }

    double quz0_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz0;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts quz1(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz1;
        },
        ix_type));
    }

    vector<double> quz1(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz1;
        },
        ith_timestep,
        ix_type);
    }

    double quz1_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz1;
        },
        ith_timestep,
        ix_type);
    }

    apoint_ts quz2(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz2;
        },
        ix_type));
    }

    vector<double> quz2(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz2;
        },
        ith_timestep,
        ix_type);
    }

    double quz2_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.quz2;
        },
        ith_timestep,
        ix_type);
    }
  };

  /** helper class for pot_ratio method of ae cell_response */
  struct pot_ratio_single_value_ts {
    double v;

    double value(size_t) const {
      return v;
    };
  };

  template <typename cell>
  struct actual_evapotranspiration_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit actual_evapotranspiration_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts output(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.ae_output;
        },
        ix_type));
    }

    vector<double> output(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.ae_output;
        },
        ith_timestep,
        ix_type);
    }

    double output_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.ae_output;
        },
        ith_timestep,
        ix_type);
    }

    /**  pot_ratio is needed to study the dry-level of kirchner and pot/act evapo ratio.
     *
     *  pot_ratio(t) = (1.0 - std::exp(-water_level*3.0/ scale_factor))  [ratio]
     *
     * where
     *   water_level = kirchner.state.q(t) [mm/h]
     *   scale_factor = cell.parameter.ae.scale_factor
     */
    apoint_ts pot_ratio(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          vector<double> v;
          v.reserve(c.sc.kirchner_discharge.size());
          auto scale_factor = c.parameter->ae.ae_scale_factor;
          auto area_m2 = c.geo.area();
          for (size_t i = 0; i < c.sc.kirchner_discharge.size(); ++i) {
            v.emplace_back(shyft::core::actual_evapotranspiration::calc_pot_ratio(
              m3s_to_mmh(c.sc.kirchner_discharge.value(i), area_m2), scale_factor));
          }
          return shyft::core::pts_t(
            c.sc.kirchner_discharge.time_axis(), std::move(v), c.sc.kirchner_discharge.point_interpretation());
        },
        ix_type));
    }

    vector<double> pot_ratio(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [ith_timestep](cell const & c) {
          auto scale_factor = c.parameter->ae.ae_scale_factor;
          auto area_m2 = c.geo.area();
          auto water_level = m3s_to_mmh(c.sc.kirchner_discharge.value(ith_timestep), area_m2);
          double pot_ratio_value = shyft::core::actual_evapotranspiration::calc_pot_ratio(water_level, scale_factor);
          return pot_ratio_single_value_ts{pot_ratio_value};
        },
        ith_timestep,
        ix_type);
    }

    double pot_ratio_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [ith_timestep](cell const & c) {
          auto scale_factor = c.parameter->ae.ae_scale_factor;
          auto area_m2 = c.geo.area();
          auto water_level = m3s_to_mmh(c.sc.kirchner_discharge.value(ith_timestep), area_m2);
          double pot_ratio_value = shyft::core::actual_evapotranspiration::calc_pot_ratio(water_level, scale_factor);
          return pot_ratio_single_value_ts{pot_ratio_value};
        },
        ith_timestep,
        ix_type);
    }
  };

  template <typename cell>
  struct hbv_actual_evapotranspiration_cell_response_statistics {
    shared_ptr<vector<cell>> cells;

    explicit hbv_actual_evapotranspiration_cell_response_statistics(shared_ptr<vector<cell>> const & cells)
      : cells(cells) {
    }

    apoint_ts output(cids_t const & indexes, stat_scope ix_type) const {
      return apoint_ts(*cell_statistics::average_catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.ae_output;
        },
        ix_type));
    }

    vector<double> output(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::catchment_feature(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.ae_output;
        },
        ith_timestep,
        ix_type);
    }

    double output_value(cids_t const & indexes, size_t ith_timestep, stat_scope ix_type) const {
      return cell_statistics::average_catchment_feature_value(
        *cells,
        indexes,
        [](cell const & c) {
          return c.rc.ae_output;
        },
        ith_timestep,
        ix_type);
    }
  };

  // extern template decl so that we get them instantiated one place in api.cpp(save compile time)
#define external_cell_template_decl(C) \
  extern template struct C<pt_gs_k::cell_complete_response_t>; \
  extern template struct C<pt_gs_k::cell_discharge_response_t>; \
  extern template struct C<r_pm_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pm_gs_k::cell_discharge_response_t>; \
  extern template struct C<r_pt_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pt_gs_k::cell_discharge_response_t>; \
  extern template struct C<pt_ss_k::cell_complete_response_t>; \
  extern template struct C<pt_ss_k::cell_discharge_response_t>; \
  extern template struct C<pt_hs_k::cell_complete_response_t>; \
  extern template struct C<pt_hs_k::cell_discharge_response_t>; \
  extern template struct C<pt_st_k::cell_complete_response_t>; \
  extern template struct C<pt_st_k::cell_discharge_response_t>; \
  extern template struct C<pt_st_hbv::cell_complete_response_t>; \
  extern template struct C<pt_st_hbv::cell_discharge_response_t>; \
  extern template struct C<pt_hps_k::cell_complete_response_t>; \
  extern template struct C<pt_hps_k::cell_discharge_response_t>

#define external_r_template_decl(C) \
  extern template struct C<r_pm_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pt_gs_k::cell_complete_response_t>;

#define external_st_template_decl(C) \
  extern template struct C<pt_st_k::cell_complete_response_t>; \
  extern template struct C<pt_st_hbv::cell_complete_response_t>;

#define external_hbv_soil_template_decl(C) extern template struct C<pt_st_hbv::cell_complete_response_t>;

#define external_hbv_tank_template_decl(C) extern template struct C<pt_st_hbv::cell_complete_response_t>;

#define external_ss_template_decl(C) extern template struct C<pt_ss_k::cell_complete_response_t>;

#define external_hs_template_decl(C) extern template struct C<pt_hs_k::cell_complete_response_t>;

#define external_hps_template_decl(C) extern template struct C<pt_hps_k::cell_complete_response_t>;

#define external_pm_template_decl(C) extern template struct C<r_pm_gs_k::cell_complete_response_t>;

#define external_pt_template_decl(C) \
  extern template struct C<pt_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pt_gs_k::cell_complete_response_t>; \
  extern template struct C<pt_ss_k::cell_complete_response_t>; \
  extern template struct C<pt_hs_k::cell_complete_response_t>; \
  extern template struct C<pt_st_k::cell_complete_response_t>; \
  extern template struct C<pt_hps_k::cell_complete_response_t>;


#define external_gs_template_decl(C) \
  extern template struct C<pt_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pm_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pt_gs_k::cell_complete_response_t>;

#define external_k_template_decl(C) \
  extern template struct C<pt_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pm_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pt_gs_k::cell_complete_response_t>; \
  extern template struct C<pt_ss_k::cell_complete_response_t>; \
  extern template struct C<pt_hs_k::cell_complete_response_t>; \
  extern template struct C<pt_st_k::cell_complete_response_t>; \
  extern template struct C<pt_hps_k::cell_complete_response_t>;

#define external_ae_template_decl(C) \
  extern template struct C<pt_gs_k::cell_complete_response_t>; \
  extern template struct C<pt_gs_k::cell_discharge_response_t>; \
  extern template struct C<r_pm_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pm_gs_k::cell_discharge_response_t>; \
  extern template struct C<r_pt_gs_k::cell_complete_response_t>; \
  extern template struct C<r_pt_gs_k::cell_discharge_response_t>; \
  extern template struct C<pt_ss_k::cell_complete_response_t>; \
  extern template struct C<pt_ss_k::cell_discharge_response_t>; \
  extern template struct C<pt_hs_k::cell_complete_response_t>; \
  extern template struct C<pt_hs_k::cell_discharge_response_t>; \
  extern template struct C<pt_st_k::cell_complete_response_t>; \
  extern template struct C<pt_st_k::cell_discharge_response_t>; \
  extern template struct C<pt_hps_k::cell_complete_response_t>; \
  extern template struct C<pt_hps_k::cell_discharge_response_t>

  external_cell_template_decl(basic_cell_statistics);
  external_k_template_decl(kirchner_cell_state_statistics);

  external_hbv_soil_template_decl(hbv_soil_cell_state_statistics);
  external_hbv_tank_template_decl(hbv_tank_cell_state_statistics);

  external_gs_template_decl(gamma_snow_cell_state_statistics);
  external_gs_template_decl(gamma_snow_cell_response_statistics);

  external_ss_template_decl(skaugen_cell_state_statistics);
  external_ss_template_decl(skaugen_cell_response_statistics);
  external_hs_template_decl(hbv_snow_cell_state_statistics);
  external_hs_template_decl(hbv_snow_cell_response_statistics);

  external_st_template_decl(snow_tiles_cell_response_statistics);
  external_st_template_decl(snow_tiles_cell_state_statistics);

  external_hps_template_decl(hbv_physical_snow_cell_state_statistics);
  external_hps_template_decl(hbv_physical_snow_cell_response_statistics);

  external_pt_template_decl(priestley_taylor_cell_response_statistics);
  external_pm_template_decl(penman_monteith_cell_response_statistics);
  external_r_template_decl(radiation_cell_response_statistics);

  external_ae_template_decl(actual_evapotranspiration_cell_response_statistics);

#undef external_cell_template_decl
#undef external_hbv_soil_template_decl
#undef external_hbv_tank_template_decl
#undef external_r_template_decl
#undef external_pm_template_decl
#undef external_pt_template_decl
#undef external_hps_template_decl
#undef external_st_template_decl
#undef external_hs_template_decl
#undef external_ss_template_decl
#undef external_gs_template_decl
#undef external_k_template_decl
}
