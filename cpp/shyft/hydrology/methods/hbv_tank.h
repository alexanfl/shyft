/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <iostream>
#include <algorithm>
#include <cmath>
#include <stdexcept>

#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/math_utilities.h>

namespace shyft::core::hbv_tank {

  /** @brief HBV ground water tank parameters
   *
   * TODO: ensure 0 <= kuz0, kuz1, kuz2 <= 1
   *
   * @param uz1 [ mm ] Mid-threshold for upper ground water zone
   * @param uz2 [ mm ] High-threshold for upper ground water zone
   * @param kuz0 [ 1/step ] Slow response coefficient upper ground water zone
   * @param kuz1 [ 1/step ] Mid response coefficient upper ground water zone
   * @param kuz2 [ 1/step ] Fast response coefficient upper ground water zone
   * @param perc [ mm/step ] Perculation to lower ground water zone
   * @param klz [ 1/step ] Slow response coefficient lower ground water zone
   * @param ce [ mm/deg/day ] Evapotranspiration constant (degree-day-factor)
   * @param cevpl [ unitless ], adjustment factor for potential evapotranspiration on lakes
   */
  struct parameter {
    parameter(
      double uz1 = 10.,
      double uz2 = 50.,
      double kuz0 = 0.05,
      double kuz1 = 0.1,
      double kuz2 = 0.,
      double perc = 0.6,
      double klz = 0.05,
      double ce = 0.17 / 24.,
      double cevpl = 1.1)
      : uz1(uz1)
      , uz2(uz2)
      , kuz0(kuz0)
      , kuz1(kuz1)
      , kuz2(kuz2)
      , perc(perc)
      , klz(klz)
      , ce(ce)
      , cevpl(cevpl) {
    }

    double uz1 = 10.;
    double uz2 = 50.;
    double kuz0 = 0.05;
    double kuz1 = 0.1;
    double kuz2 = 0.;
    double perc = 0.6;
    double klz = 0.05;
    double ce = 0.17 / 24.;
    double cevpl = 1.1;

    bool operator==(parameter const & x) const {
      return nan_equal(uz1, x.uz1) && nan_equal(uz2, x.uz2) && nan_equal(kuz0, x.kuz0) && nan_equal(kuz1, x.kuz1)
          && nan_equal(kuz2, x.kuz2) && nan_equal(perc, x.perc) && nan_equal(klz, x.klz) && nan_equal(ce, x.ce)
          && nan_equal(cevpl, x.cevpl);
    }

    bool operator!=(parameter const & x) const {
      return !operator==(x);
    }

    x_serialize_decl();
  };

  /**
   * HBV ground water tank state
   *
   * @param uz [mm], Upper ground water zone content
   * @param lz [mm], Lower ground water zone content
   */
  struct state {
    state(double uz = 10, double lz = 10)
      : uz{uz}
      , lz{lz} {
    }

    double uz = 10.; ///< [mm], Upper ground water zone content
    double lz = 10.; ///< [mm], lz [mm], Lower ground water zone content

    bool operator==(state const & x) const {
      return (nan_equal(uz, x.uz) && nan_equal(lz, x.lz));
    }

    x_serialize_decl();
  };

  /**
   * HBV ground water tank response
   * TODO: raise error for any param < 0
   *
   * @param quz [mm/step] Total for upper zone
   * @param quz0 [mm/step] Upper zone slow response
   * @param quz1 [mm/step] Upper zone mid response
   * @param quz2 [mm/step] Upper zone fast response
   * @param qlz [mm/step] Lower zone response
   * @param elake [mm/step] Lake evaporation
   * @param q [mm/step] Total discharge from tank
   */
  struct response {
    response(
      double quz0 = 0,
      double quz1 = 0,
      double quz2 = 0,
      double qlz = 0,
      double elake = 0,
      double perc_effective = 0)
      : quz0{quz0}
      , quz1{quz1}
      , quz2{quz2}
      , qlz{qlz}
      , elake{elake}
      , perc_effective{perc_effective} {
    }

    double quz0{0}; ///< quz0 [mm/step]
    double quz1{0};
    double quz2{0};
    double qlz{0};
    double elake{0};
    double perc_effective{0};

    bool operator==(response const & x) const {
      return nan_equal(quz0, x.quz0) && nan_equal(quz1, x.quz1) && nan_equal(quz2, x.quz2) && nan_equal(qlz, x.qlz)
          && nan_equal(elake, x.elake) && nan_equal(perc_effective, x.perc_effective);
    }

    double quz() const noexcept {
      return quz0 + quz1 + quz2;
    }

    double q() const noexcept {
      return quz() + qlz;
    }
  };

  /** @brief HBV ground water tank
  *
  * Computing water through the HBV ground water zone of the HBV model.
  *
  *
  * @note For reference, see
  *       Nils Roar Sæhlthun: The Nordic HBV model 1996
  *       https://publikasjoner.nve.no/publication/1996/publication1996_07.pdf
  *
  *
  * @note Lake and glacier are treated in the stack
  *

  */
  struct calculator {
    parameter param;
    double const lake_fraction;
    double const non_lake_fraction;

    explicit calculator(parameter const & p, double lake_fraction)
      : param{p}
      , lake_fraction{lake_fraction}
      , non_lake_fraction{1 - lake_fraction} {
    }

    /**
     * @brief step one time forward
     * TODO: units
     * @param from_soil [mm/step] Inflow to upper zone (from soil moisture)
     * @param precip [mm/step] Routed precipitation. Assumed that it is corrected for lake fraction already.
     * @param t2m [deg C] Air-temperature at 2m (proxy for water temperature)
     */
    void step(state& s, response& r, double from_soil, double precip, double t2m) const noexcept {
      calc_upper_zone(s, r, from_soil);
      calc_lower_zone(s, r, precip, t2m);
    }

    /** @brief Response and perculation from upper zone
     *
     * Note that twe choose following "time-order":
     *     1) Add inflows to state,
     *     2) subtract response,
     *     3) subtract perculation (water to lower zone uz).
     *
     * This order is arbirary and an improved solution would be to set up a differential equation.
     */

    void calc_upper_zone(state& s, response& r, double from_soil) const noexcept {
      double uz = s.uz + from_soil;
      auto [quz0, quz1, quz2] = calc_response_upper(uz);
      r.quz0 = quz0;
      r.quz1 = quz1;
      r.quz2 = quz2;
      uz = uz - r.quz();

      r.perc_effective = std::min(param.perc, uz) * non_lake_fraction;
      s.uz = uz - r.perc_effective;
    }

    /** @brief Calculate response of the three levels in the upper zone.**/
    std::tuple<double, double, double> calc_response_upper(double uz) const noexcept {
      using std::min;
      using std::max;
      // if ( uz < 0) {
      //         throw std::invalid_argument( "uz needs to be larger than 0. Supplied value: " + std::to_string(uz));
      //     }

      double level_uz2 = max(0., uz - param.uz2);
      double quz2 = level_uz2 * param.kuz2 * non_lake_fraction;

      double level_uz1 = min(uz, param.uz2) - param.uz1;
      level_uz1 = max(0., level_uz1);
      double quz1 = level_uz1 * param.kuz1 * non_lake_fraction;

      double level_uz0 = min(uz, param.uz1);
      double quz0 = level_uz0 * param.kuz0 * non_lake_fraction;
      return std::make_tuple(quz0, quz1, quz2);
    }

    /** @brief Response q from lower zone.
     *
     * Note that twe choose following "time-order":
     *     1) Add inflows to state,
     *     2) subtract evaporation,
     *     3) calculate response.
     *
     * This order is arbirary and an improved solution would be to set up a differential equation.
     */

    void calc_lower_zone(state& s, response& r, double percip, double t2m) const noexcept {
      double lz = s.lz + r.perc_effective + percip;
      double elake = std::min(lz, calc_elake(t2m) * lake_fraction);
      lz = lz - elake;

      double qlz = param.klz * lz;
      lz = lz - qlz;

      s.lz = lz;
      r.qlz = qlz;
      r.elake = elake;
    }

    /** @brief Evaporation from a lake
     *
     * @param t2m surface temperature
     */
    double calc_elake(double temp) const noexcept {
      return (temp > 0 ? temp * param.ce * param.cevpl : 0.0);
    }
  };
}

x_serialize_export_key(shyft::core::hbv_tank::state);
x_serialize_export_key(shyft::core::hbv_tank::parameter);
