/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <algorithm>
#include <cmath>
#include <stdexcept>

#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/math_utilities.h>

namespace shyft::core::hbv_soil {
  using std::vector;
  using std::fill;
  using std::fabs;
  using std::pow;

  /**
   * Soil Moisture Parameters
   *
   * @param fc (mm) maximum water content of the soil (Field Capacity).
   * @param lpdel (unitless) dimensionless parameter (<1), from which level evapotranspiration is potential.
   *      Some publications use fcdel instead.
   * @param beta (unitless) exponent in the nonlinear relationship between soil moisture and field capacity.
   * @param infmax (mm/h) maximum input rate to the soil moisture zone. Excess water will go directly to upper ground
   * water zone.
   */
  struct parameter {
    parameter(double fc = 32.2755689, double lpdel = 0.848557794, double beta = 1.50738409, double infmax = 2)
      : fc(fc)
      , lpdel(lpdel)
      , beta(beta)
      , infmax(infmax) {
    }

    double fc = 32.2755689;
    double lpdel = 0.848557794;
    double beta = 1.50738409;
    double infmax = 2;

    bool operator==(parameter const & x) const {
      return nan_equal(fc, x.fc) && nan_equal(lpdel, x.lpdel) && nan_equal(beta, x.beta) && nan_equal(infmax, x.infmax);
    }

    bool operator!=(parameter const & x) const {
      return !operator==(x);
    }

    x_serialize_decl();
  };

  /**
   * Soil Moisture State
   *
   * @param sm (mm) soil moisture
   */
  struct state {
    state(double sm = 10.)
      : sm(sm) {
    }

    double sm = 10.;

    bool operator==(state const & x) const {
      return nan_equal(sm, x.sm);
    }

    x_serialize_decl();
  };

  /**
   * Soil Moisture response
   *
   * @param inuz (mm/h) Perculation to upper ground water zone. Called `cuz` by Nils Roar.
   * @param ae (mm/h) Actual evapotranspiration
   */
  struct response {
    response(double inuz = 0., double ae = 0.)
      : inuz(inuz)
      , ae(ae) {
    }

    double inuz;
    double ae;

    bool operator==(response const & x) const {
      return (nan_equal(inuz, x.inuz) && nan_equal(ae, x.ae));
    }

    double sum() {
      return inuz + ae;
    }
  };

  /** @brief Hbv soil moisture
  *
  * Computing water through the soil moisture zone of the HBV model.
  *
  *
  * @note For reference, see
  *       Nils Roar Sæhlthun: The Nordic HBV model 1996
  *       https://publikasjoner.nve.no/publication/1996/publication1996_07.pdf
  *
  *
  * @note Lake and glacier are treated in the stack
  *

  * @param land_fraction Land-fraction of the cell. Not directly used/desribed in ref. 'Nils Roar', but
  *   introduced here to adopt to the stack.
  */

  struct calculator {
    parameter param;
    double const land_fraction;

    explicit calculator(parameter const & p, double land_fraction)
      : param{p}
      , land_fraction{land_fraction} {
    }

    /**
     * @brief step one step forward
     * @param insoil inflow to soil
     * @param pe potential evaporation
     * @param sca fraction of snow covered area
     */
    void step(state& s, response& r, double insoil, double pe, double sca) const noexcept {
      using std::min;
      using std::max;

      // Calculate input to UZ: Surplus if insoil>infmax
      double inuz_surplus_infmax = max(insoil - param.infmax, 0.);
      insoil = min(insoil, param.infmax);
      double inuz = inuz_surplus_infmax + insoil;

      // Soil moisture state after percolation
      double delta_sm = insoil * (1. - pow(s.sm / param.fc, param.beta));
      r.inuz = inuz - delta_sm;

      // Calculate actual evaporation
      double evaporation_snowfree = (1 - sca) * pe;
      double lp = param.fc * param.lpdel;
      lp = min(1., s.sm / lp);
      // multiplication with land_fraction is a "shyft-ification" of hbv (not directly in 'Nils Roar')
      // evaporation from lake is taken into account in hbv tank routine
      r.ae = min(evaporation_snowfree * lp, s.sm) * land_fraction;

      // Update state
      s.sm = min(s.sm + delta_sm - r.ae, param.fc);
    }
  };
}

x_serialize_export_key(shyft::core::hbv_soil::state);
x_serialize_export_key(shyft::core::hbv_soil::parameter);
