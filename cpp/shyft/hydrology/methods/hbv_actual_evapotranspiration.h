/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/math_utilities.h>

namespace shyft::core::hbv_actual_evapotranspiration {

  /**<  keeps the parameters (potential calibration/localization) for the AE */
  struct parameter {
    double lp = 150.0; ///< default value is 150.0

    parameter(double lp = 150.0)
      : lp(lp) {
    }

    bool operator==(parameter const & x) const {
      return nan_equal(lp, x.lp);
    }

    bool operator!=(parameter const & x) const {
      return !operator==(x);
    }

    x_serialize_decl();
  };

  /**<  keeps the formal response of the AE method */
  struct response {
    double ae = 0.0;
  };

  /** @brief actual_evapotranspiration calculates actual evapotranspiration
   * based on supplied parameters
   *
   * @param soil_moisture, sm
   * @param potential_evapotranspiration, pot_evapo
   * @param soil threshold for evapotranspiration, lp
   * @param snow_fraction 0..1
   * @return calculated actual evapotranspiration
   *
   */

  inline double calculate_step(
    double const soil_moisture,
    double const pot_evapo,
    double const lp,
    double const snow_fraction,
    const utctimespan) {
    return (1.0 - snow_fraction) * (soil_moisture < lp ? pot_evapo * (soil_moisture / lp) : pot_evapo);
  }
}

x_serialize_export_key(shyft::core::hbv_actual_evapotranspiration::parameter);
