/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <cmath>
#include <limits>
#include <stdexcept>

#include <shyft/hydrology/cell_model.h>
#include <shyft/hydrology/stacks/pt_st_hbv.h>

namespace shyft::core {
  /** @brief pt_st_hbv namespace declares the needed pt_st_hbv specific parts of a cell
   * that is:
   * -# the pt_st_hbv parameter,state and response types
   * -# the response collector variants
   */
  namespace pt_st_hbv {
    using std::vector;
    using std::string;
    using std::runtime_error;


    typedef parameter parameter_t;
    typedef state state_t;
    typedef response response_t;
    typedef shared_ptr<parameter_t> parameter_t_;
    typedef shared_ptr<state_t> state_t_;
    typedef shared_ptr<response_t> response_t_;

    /** @brief all_reponse_collector aims to collect all output from a cell run so that it can be studied afterwards.
     *
     * @note could be quite similar between variants of a cell, e.g. pt_gs_k pt_st_hbv pt_ss_k, ptss..
     * TODO: could we use another approach to limit code pr. variant ?
     * TODO: Really make sure that units are correct, precise and useful..
     *       both with respect to measurement unit, and also specifying if this
     *       a 'state in time' value or a average-value for the time-step.
     */
    struct all_response_collector {
      double destination_area; ///< in [m^2]
      // these are the one that we collects from the response, to better understand the model::
      pts_t avg_discharge; ///< HBV Discharge given in [m^3/s] for the timestep

      pts_t soil_ae; ///< HBV soil actual evaporation given in [mm/h] for the timestep
      pts_t inuz;    ///< HBV soil perculation to upper zone given in [mm/h] for the timestep
      pts_t elake;   ///< HBV tank lake evaporation given in [mm/h] for the timestep
      pts_t qlz;     ///< HBV tank lower zone in [m^3/s] for the timestep
      pts_t quz0;    ///< HBV tank upper zone slow response in [m^3/s] for the timestep
      pts_t quz1;    ///< HBV tank upper zone mid response in [m^3/s] for the timestep
      pts_t quz2;    ///< HBV tank upper zone fast response in [m^3/s] for the timestep

      pts_t charge_m3s;   ///< = precip + glacier - evap - avg_discharge [m^3/s] for the timestep
      pts_t snow_outflow; ///<  snow output [m3/s] for the timestep
      pts_t snow_sca;
      pts_t snow_swe;
      pts_t glacier_melt;     ///< galcier melt output [m3/s] for the timestep
      pts_t pe_output;        ///< actual evap mm/h
      response_t end_reponse; ///<< end_response, at the end of collected

      all_response_collector()
        : destination_area(0.0) {
      }

      explicit all_response_collector(double const destination_area)
        : destination_area(destination_area) {
      }

      all_response_collector(double const destination_area, timeaxis_t const & time_axis)
        : destination_area(destination_area)
        , avg_discharge(time_axis, 0.0)
        , soil_ae(time_axis, 0.0)
        , inuz(time_axis, 0.0)
        , elake(time_axis, 0.0)
        , qlz(time_axis, 0.0)
        , quz0(time_axis, 0.0)
        , quz1(time_axis, 0.0)
        , quz2(time_axis, 0.0)
        , charge_m3s(time_axis, 0.0)
        , snow_outflow(time_axis, 0.0)
        , snow_sca(time_axis, 0.0)
        , snow_swe(time_axis, 0.0)
        , glacier_melt(time_axis, 0.0)
        , pe_output(time_axis, 0.0) {
      }

      /**@brief called before run to allocate space for results */
      void initialize(timeaxis_t const & time_axis, int start_step, int n_steps, double area) {
        destination_area = area;
        ts_init(avg_discharge, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(soil_ae, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(inuz, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(elake, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(qlz, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(quz0, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(quz1, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(quz2, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(charge_m3s, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(snow_outflow, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(snow_sca, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(snow_swe, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(glacier_melt, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(pe_output, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
      }

      /**@brief Call for each time step, to collect needed information from R
       *
       * The R in this case is the Response type defined for the pt_g_s_k stack
       * and in principle, we can pick out all needed information from this.
       * The values are put into the plain point time-series at the i'th position
       * corresponding to the i'th simulation step, .. on the time-axis.. that
       * again gives us the concrete period in time that this value applies to.
       *
       */
      void collect(size_t idx, response_t const & response) {
        avg_discharge.set(
          idx,
          mmh_to_m3s(
            response.total_discharge,
            destination_area));              // wants m3/s, q_avg is given in mm/h, so compute the totals in  mm/s
        soil_ae.set(idx, response.soil.ae);  ///< [mm/h]
        inuz.set(idx, response.soil.inuz);   ///< [mm/h]
        elake.set(idx, response.tank.elake); ///< [mm/h]
        qlz.set(
          idx,
          mmh_to_m3s(response.tank.qlz, destination_area)); // wants m3/s, given in mm/h, so compute the totals in  mm/s
        quz0.set(
          idx,
          mmh_to_m3s(response.tank.quz0, destination_area)); // wants m3/s, given in mm/h, so compute the totals in mm/s
        quz1.set(
          idx,
          mmh_to_m3s(response.tank.quz1, destination_area)); // wants m3/s, given in mm/h, so compute the totals in mm/s
        quz2.set(
          idx,
          mmh_to_m3s(response.tank.quz2, destination_area)); // wants m3/s, given in mm/h, so compute the totals in mm/s
        charge_m3s.set(idx, response.charge_m3s);
        snow_outflow.set(idx, mmh_to_m3s(response.snow.outflow, destination_area));
        snow_sca.set(idx, response.snow.sca);
        snow_swe.set(idx, response.snow.swe);
        glacier_melt.set(idx, response.gm_melt_m3s);
        pe_output.set(idx, response.pt.pot_evapotranspiration);
      }

      void set_end_response(response_t const & r) {
        end_reponse = r;
      }
    };

    /** @brief a collector that collects/keep discharge only */
    struct discharge_collector {
      double destination_area;
      pts_t avg_discharge;     ///< Discharge given in [m^3/s] as the average of the timestep
      pts_t charge_m3s;        ///< = precip + glacier - act_evap - avg_discharge [m^3/s] for the timestep
      response_t end_response; ///<< end_response, at the end of collected
      bool collect_snow;
      pts_t snow_sca;
      pts_t snow_swe;

      discharge_collector()
        : destination_area(0.0)
        , collect_snow(false) {
      }

      explicit discharge_collector(double const destination_area)
        : destination_area(destination_area)
        , collect_snow(false) {
      }

      discharge_collector(double const destination_area, timeaxis_t const & time_axis)
        : destination_area(destination_area)
        , avg_discharge(time_axis, 0.0)
        , charge_m3s(time_axis, 0.0)
        , collect_snow(false)
        , snow_sca(timeaxis_t(time_axis.start(), time_axis.delta(), 0), 0.0)
        , snow_swe(timeaxis_t(time_axis.start(), time_axis.delta(), 0), 0.0) {
      }

      void initialize(timeaxis_t const & time_axis, int start_step, int n_steps, double area) {
        destination_area = area;
        auto ta = collect_snow ? time_axis : timeaxis_t(time_axis.start(), time_axis.delta(), 0);
        ts_init(avg_discharge, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(charge_m3s, time_axis, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(snow_sca, ta, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
        ts_init(snow_swe, ta, start_step, n_steps, ts_point_fx::POINT_AVERAGE_VALUE);
      }

      void collect(size_t idx, response_t const & response) {
        avg_discharge.set(
          idx, mmh_to_m3s(response.total_discharge, destination_area)); // q_avg is given in mm, so compute the totals
        charge_m3s.set(idx, response.charge_m3s);
        if (collect_snow) {
          snow_sca.set(idx, response.snow.sca);
          snow_swe.set(idx, response.snow.swe);
        }
      }

      void set_end_response(response_t const & response) {
        end_response = response;
      }
    };

    /**@brief a state null collector
     *
     * Used during calibration/optimization when there is no need for state,
     * and we need all the RAM for useful purposes.
     */
    struct null_collector {
      void initialize(parameter_t const *, timeaxis_t const &, int = 0, int = 0, double = 0.0) {
      }

      void collect(size_t, state_t const &) {
      }
    };

    /** @brief the state_collector collects all state if enabled
     *
     *  @note that the state collected is instant in time, valid at the beginning of period
     */
    struct state_collector {
      bool collect_state{
        false}; ///< if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)
      // these are the one that we collects from the response, to better understand the model::
      double destination_area{0.0};
      pts_t soil_sm;    ///< soil.sm mm
      pts_t tank_uz;    ///< tank.uz mm
      pts_t tank_lz;    ///< tank.lz mm
      vector<pts_t> fw; ///< fw state as in mm (over what area? cell, or cell-storage area ? could be hard to crack)
      vector<pts_t> lw; ///< lw state, ref above

      timeaxis_t time_axis;
      int start_step{0};
      int n_steps{0};
      //-- to provide python with properities .swe, .sca -> ts, we need
      //   to store the snow-tiles-parameter, and the snow_area_fraction
      snow_tiles::parameter st;       ///< the snow-tiles parameter as in snap-shot from .initialize
      double snow_area_fraction{1.0}; ///< the snow_area_fraction as in snap-shot from .initialize

      /** given cell snow_tiles parameter compute the swe in mm
       */
      pts_t swe(snow_tiles::parameter const & p, double snow_area_fraction = 1.0) const {
        pts_t r{time_axis, 0.0, ts_point_fx::POINT_INSTANT_VALUE}; // result ts with all zero's'
        auto af{p.get_area_fractions()};
        for (size_t t = 0; t < time_axis.size(); ++t) { // for all time-steps
          for (size_t i = 0; i < fw.size(); ++i)        // compute
            r.v[t] += af[i] * (lw[i].v[t] + fw[i].v[t]);
          r.v[t] *= snow_area_fraction; // scale to cell area compatible units
        }
        return r;
      }

      pts_t sca(snow_tiles::parameter const & p) const {
        pts_t r{time_axis, 0.0, ts_point_fx::POINT_INSTANT_VALUE}; // result ts with all zero's'
        auto af{p.get_area_fractions()};
        for (size_t t = 0; t < time_axis.size(); ++t) // for all time-steps
          for (size_t i = 0; i < fw.size(); ++i)      // compute
            r.v[t] += fw[i].v[t] > 0.0 ? af[i] : 0.0;
        return r;
      }

      /** return sca based on captured state and snow-tile parameter */
      pts_t sca_() const {
        return sca(st);
      }

      /** return swe based on captured state, snow-tile parameter and snow_area_fraction */
      pts_t swe_() const {
        return swe(st, snow_area_fraction);
      }

      state_collector() = default;

      explicit state_collector(timeaxis_t const & time_axis)
        : collect_state{false}
        , destination_area{0.0}
        , soil_sm{time_axis, 0.0}
        , tank_uz{time_axis, 0.0}
        , tank_lz{time_axis, 0.0}
        , time_axis(time_axis) { /* Do nothing */
      }

      /** brief called before run, prepares state time-series
       *
       * with preallocated room for the supplied time-axis.
       *
       * @note if collect_state is false, a zero length time-axis is used to ensure data is wiped/out.
       */
      void initialize(
        parameter_t const & p,
        timeaxis_t const & time_axis,
        int start_step,
        int n_steps,
        double area,
        double a_snow_area_fraction) {
        destination_area = area;
        this->time_axis = time_axis; // make a copy of this for later
        this->start_step = start_step;
        this->n_steps = n_steps;
        timeaxis_t ta = collect_state ? time_axis : timeaxis_t(time_axis.start(), time_axis.delta(), 0);
        ts_init(soil_sm, ta, start_step, n_steps, ts_point_fx::POINT_INSTANT_VALUE);
        ts_init(tank_uz, ta, start_step, n_steps, ts_point_fx::POINT_INSTANT_VALUE);
        ts_init(tank_lz, ta, start_step, n_steps, ts_point_fx::POINT_INSTANT_VALUE);
        st = p.st;
        snow_area_fraction = a_snow_area_fraction;
        auto size = p.st.number_of_tiles();
        if (size != fw.size() || size != lw.size()) {
          fw.resize(0);
          lw.resize(0);
          fw.resize(size);
          lw.resize(size);
        }
        for (auto& item : fw)
          ts_init(item, ta, start_step, n_steps, ts_point_fx::POINT_INSTANT_VALUE);
        for (auto& item : lw)
          ts_init(item, ta, start_step, n_steps, ts_point_fx::POINT_INSTANT_VALUE);
      }

      /** called by the cell.run for each new state*/
      void collect(size_t idx, state_t const & state) {
        // if ((fw.size() != state.snow.fw.size()) || (lw.size() != state.snow.lw.size()))
        //     initialize_vector_states(state.snow.fw.size());

        if (collect_state) {
          soil_sm.set(idx, state.soil.sm);
          tank_uz.set(idx, state.tank.uz);
          tank_lz.set(idx, state.tank.lz);
          for (size_t i = 0; i < state.snow.fw.size(); ++i) {
            fw[i].set(idx, state.snow.fw[i]);
            lw[i].set(idx, state.snow.lw[i]);
          }
        }
      }
    };

    // typedef the variants we need exported.
    typedef cell<parameter_t, state_t, state_collector, all_response_collector> cell_complete_response_t;
    typedef cell<parameter_t, state_t, null_collector, discharge_collector> cell_discharge_response_t;

  } // pt_st_hbv

  // specialize run method for all_response_collector
  template <>
  inline void
    cell<pt_st_hbv::parameter_t, pt_st_hbv::state_t, pt_st_hbv::state_collector, pt_st_hbv::all_response_collector>::run(
      timeaxis_t const & ta,
      int start_step,
      int n_steps) {
    if (parameter.get() == nullptr)
      throw std::runtime_error("pt_st_hbv::run with null parameter attempted");

    // begin_run(time_axis,start_step,n_steps);
    //  need to pass parameter to determine number_of_tiles
    rc.initialize(ta, start_step, n_steps, geo.area());
    sc.initialize(
      *parameter,
      timeaxis_t(ta.start(), ta.delta(), ta.size() + 1),
      start_step,
      n_steps > 0 ? n_steps + 1 : 0,
      geo.area(),
      geo.land_type_fractions_info()
        .snow_storage()); // state collection is both first and last state, so, time-axis +1 step

    pt_st_hbv::run<direct_accessor, pt_st_hbv::response_t>(
      geo,
      *parameter,
      ta,
      start_step,
      n_steps,
      env_ts.temperature,
      env_ts.precipitation,
      env_ts.wind_speed,
      env_ts.rel_hum,
      env_ts.radiation,
      state,
      sc,
      rc);
  }

  template <>
  inline void
    cell<pt_st_hbv::parameter_t, pt_st_hbv::state_t, pt_st_hbv::state_collector, pt_st_hbv::all_response_collector>::
      begin_run(timeaxis_t const &, int /*start_step*/, int /*n_steps*/) {
    // impl. in run override
  }

  template <>
  inline void
    cell<pt_st_hbv::parameter_t, pt_st_hbv::state_t, pt_st_hbv::state_collector, pt_st_hbv::all_response_collector>::
      set_state_collection(bool on_or_off) {
    sc.collect_state = on_or_off;
  }

  // specialize run method for discharge_collector
  template <>
  inline void
    cell<pt_st_hbv::parameter_t, pt_st_hbv::state_t, pt_st_hbv::null_collector, pt_st_hbv::discharge_collector>::run(
      timeaxis_t const & ta,
      int start_step,
      int n_steps) {
    if (parameter.get() == nullptr)
      throw std::runtime_error("pt_st_hbv::run with null parameter attempted");
    // begin_run(time_axis, start_step, n_steps);
    //  need to pass parameter to determine number_of_tiles
    rc.initialize(ta, start_step, n_steps, geo.area());
    // sc.initialize(*parameter,timeaxis_t(ta.start(),ta.delta(),ta.size()+1),start_step,n_steps>0?n_steps+1:0,geo.area());//state
    // collection is both first and last state, so, time-axis +1 step

    pt_st_hbv::run<direct_accessor, pt_st_hbv::response_t>(
      geo,
      *parameter,
      ta,
      start_step,
      n_steps,
      env_ts.temperature,
      env_ts.precipitation,
      env_ts.wind_speed,
      env_ts.rel_hum,
      env_ts.radiation,
      state,
      sc,
      rc);
  }

  template <>
  inline void
    cell<pt_st_hbv::parameter_t, pt_st_hbv::state_t, pt_st_hbv::null_collector, pt_st_hbv::discharge_collector>::
      begin_run(timeaxis_t const &, int, int) {
    // impl. in run override
  }

  template <>
  inline void
    cell<pt_st_hbv::parameter_t, pt_st_hbv::state_t, pt_st_hbv::null_collector, pt_st_hbv::discharge_collector>::
      set_snow_sca_swe_collection(bool on_or_off) {
    rc.collect_snow = on_or_off; // possible, if true, we do collect both swe and sca, default is off
  }
}
