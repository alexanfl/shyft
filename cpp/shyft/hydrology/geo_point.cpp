
#include <shyft/core/epsg_fast.h>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/algorithms/distance.hpp>
#include <shyft/hydrology/geo_point.h>

namespace shyft::core {
  namespace detail {
    static geo_point_ll impl_to_longitude_latitude(geo_point const &p, int geo_epsg_id) {
      point_xy p_xy(p.x, p.y);
      point_ll p_ll;
      bg::srs::epsg_fast e{geo_epsg_id};
      bg::srs::projection<> prj{e};
      prj.inverse(p_xy, p_ll);
      return geo_point_ll{bg::get<0>(p_ll), bg::get<1>(p_ll)};
    }

    template point_xy
      epsg_map<point_xy>(point_xy, int from_epsg, int to_epsg); // instantiate the extern decl in header here.

  }

  geo_point_ll convert_to_longitude_latitude(geo_point const &p, int geo_epsg_id) {
    return detail::impl_to_longitude_latitude(p, geo_epsg_id);
  }

  geo_point epsg_map(geo_point a_, int from_epsg, int to_epsg) {
    if (from_epsg == to_epsg) {
      return a_;
    } else {
      auto a = detail::epsg_map(detail::point_xy(a_.x, a_.y), from_epsg, to_epsg);
      return geo_point(detail::bg::get<0>(a), detail::bg::get<1>(a), a_.z);
    }
  }
}
