/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/core/formatters.h>

namespace shyft::core {

  template <class P>
  std::string parameter_formatter(P const & t) {
    std::string r;
    auto out = std::back_inserter(r);
    fmt::format_to(out, "{{");
    for (auto i = 0u; i < t.size(); ++i)
      fmt::format_to(out, "{} .{} = {}", (i > 0 ? ',' : ' '), t.get_name(i), t.get(i));
    fmt::format_to(out, "}}");
    return r;
  }

/**
 * @brief macro oneliner
 * @details
 * Oneline define hydrology parameterformatter
 * to be used on the trailer of typ. stack/pt_gs_k.h
 * efffect: declares suitable formatter for parameter types
 * that have signature .size(), .get_name(i), .get(i)
 *
 */
#define x_def_parameter_formatter(param_type) \
  template <typename Char> \
  struct fmt::formatter<param_type, Char> \
    : shyft::fn_formatter<&shyft::core::parameter_formatter<param_type>, Char> { }; \
\
  template <typename Char> \
  struct fmt::formatter<std::shared_ptr<param_type>, Char> : shyft::ptr_formatter<param_type, Char> { };

}
