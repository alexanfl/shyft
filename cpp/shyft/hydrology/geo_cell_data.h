/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cmath>
#include <cstdint>
#include <functional>
#include <memory>
#include <stdexcept>
#include <vector>

#include <boost/math/constants/constants.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/export.hpp>
#include <armadillo>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/formatters.h>
#include <shyft/hydrology/geo_point.h>

namespace shyft::core {

  using cid_t = std::int64_t;
  using cids_t = std::vector<cid_t>;

  /**@brief land_type as in land-type related to behavior
   *
   * During construction of geo-cells, the gis-system properties should be mapped
   * to those entities that creates the wanted behavior.
   *
   *  E.g: land_type  class should match the representation from the source, for example PLT (plant-functional types) in
   * MODIS, or whatever in LANDSAT
   */
  enum struct land_type {
    glacier,
    lake,
    reservoir,
    forest,
    bare_ground,
    unspecified
  };

  /**
   * @brief land_type_fractions are used to describe 'type of land'
   *   like glacier, lake, reservoir and forest.
   *
   * @details
   *  It is designed as a part of geo_cell_data. It's of course
   *   questionable, since we could have individual models for each type
   *   of land, - but current approach is to use a cell-area, and then
   *   describe fractional properties.
   */
  struct land_type_fractions {
    land_type_fractions() = default;

    /** construct a monocell of specified land-type, throw on unspecified */
    explicit land_type_fractions(land_type lt) {
      switch (lt) {
      case land_type::glacier:
        glacier_ = 1.0;
        break;
      case land_type::lake:
        lake_ = 1.0;
        break;
      case land_type::reservoir:
        reservoir_ = 1.0;
        break;
      case land_type::forest:
        forest_ = 1.0;
        break;
      case land_type::bare_ground:
        break;
      case land_type::unspecified:
        throw std::runtime_error("land_type_fractions need to be speficied");
      }
    }

    /** @brief LandTypeFraction constructor
     *
     *  Construct the land type fractions based on the sizes of the
     *  different land types. Each size should be greater or equal to
     *  zero.
     */
    land_type_fractions(
      double glacier_size,
      double lake_size,
      double reservoir_size,
      double forest_size,
      double unspecified_size) {
      glacier_size = std::max(0.0, glacier_size);
      lake_size = std::max(0.0, lake_size);
      reservoir_size = std::max(0.0, reservoir_size);
      forest_size = std::max(0.0, forest_size);
      unspecified_size = std::max(0.0, unspecified_size);
      double const sum = glacier_size + lake_size + reservoir_size + forest_size + unspecified_size;
      if (sum > 0) {
        glacier_ = glacier_size / sum;
        lake_ = lake_size / sum;
        reservoir_ = reservoir_size / sum;
        forest_ = forest_size / sum;
      } else
        glacier_ = lake_ = reservoir_ = forest_ = 0.0;
    }

    double glacier() const {
      return glacier_;
    }

    double lake() const {
      return lake_;
    } // not regulated, assume time-delay until discharge

    double reservoir() const {
      return reservoir_;
    } // regulated, assume zero time-delay to discharge

    double forest() const {
      return forest_;
    }

    double unspecified() const {
      return 1.0 - glacier_ - lake_ - reservoir_ - forest_;
    }

    double snow_storage() const {
      return 1.0 - lake_ - reservoir_;
    } ///< the fraction that allow snow storage

    void set_fractions(double glacier, double lake, double reservoir, double forest) {
      double const tol = 1.0e-3; // Allow small round off errors
      double const sum = glacier + lake + reservoir + forest;
      if (sum > 1.0 && sum < 1.0 + tol) {
        glacier /= sum;
        lake /= sum;
        reservoir /= sum;
        forest /= sum;
      } else if (sum > 1.0 || (glacier < 0.0 || lake < 0.0 || reservoir < 0.0 || forest < 0.0))
        throw std::invalid_argument("LandTypeFractions:: must be >=0.0 and sum <= 1.0");
      glacier_ = glacier;
      lake_ = lake;
      reservoir_ = reservoir;
      forest_ = forest;
    }

    bool operator==(land_type_fractions const &o) const {
      return std::abs(glacier_ - o.glacier_) + std::abs(lake_ - o.lake_) + std::abs(reservoir_ - o.reservoir_)
             + std::abs(forest_ - o.forest_)
           < 0.001;
    }

    bool operator!=(land_type_fractions const &o) const {
      return !operator==(o);
    }

    /** return true if the fractions indicate this is a monocell*/
    bool is_monocell() const {
      return is_one(glacier_) || is_one(lake_) || is_one(reservoir_) || is_one(forest_) || is_one(unspecified());
    }

    /** @brief returns monocell land-type
     *
     * If the cell is monocell return the kind, if not monocell, return unspecified kind.
     */
    land_type monocell_landtype() const {
      if (is_one(glacier_))
        return land_type::glacier;
      if (is_one(lake_))
        return land_type::lake;
      if (is_one(reservoir_))
        return land_type::reservoir;
      if (is_one(forest_))
        return land_type::forest;
      if (is_one(unspecified()))
        return land_type::bare_ground;
      return land_type::unspecified;
    }

   private:
    bool is_one(double x) const {
      return std::abs(1.0 - x) < 0.001;
    }

    double glacier_{0.0};
    double lake_{0.0};      // not regulated, assume time-delay until discharge
    double reservoir_{0.0}; // regulated, assume zero time-delay to discharge
    double forest_{0.0};
    x_serialize_decl();
  };

  /** The routing_info contains the geo-static parts of the relation between
   * the cell and the routing sink point */
  struct routing_info {
    routing_info(std::int64_t destination_id = 0, double distance = 0.0)
      : id(destination_id)
      , distance(distance) {
    }

    std::int64_t id = 0;   ///< target routing input identifier (similar to catchment_id), 0 means nil,none
    double distance = 0.0; ///< static routing distance[m] to the routing point

    bool operator==(routing_info const &o) const {
      return id == o.id && std::abs(distance - o.distance) < 0.1;
    }

    bool operator!=(routing_info const &o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  double const default_radiation_slope_factor = 0.9;

  /** @brief geo_cell_data represents common constant geo_cell properties across several possible models and cell
   * assemblies. The idea is that most of our algorithms uses one or more of these properties, so we provide a common
   * aspect that keeps this together. Currently it keep the
   *   - mid-point geo_point, (x,y,z) (default 0)
   *     the area in m^2, (default 1000 x 1000 m2)
   *     land_type_fractions (unspecified=1)
   *     catchment_id   def (-1)
   *     radiation_slope_factor def 0.9
   */
  struct geo_cell_data {
    static int const default_area_m2 = 1000000;

    geo_cell_data()
      : catchment_ix(0)
      , area_m2(default_area_m2)
      , catchment_id_(-1)
      , radiation_slope_factor_(default_radiation_slope_factor) {
    }

    geo_cell_data(
      geo_point const &mid_point,
      double area = default_area_m2,
      std::int64_t catchment_id = -1,
      double radiation_slope_factor = default_radiation_slope_factor,
      land_type_fractions const &land_type_fraction = land_type_fractions(),
      routing_info routing_inf = routing_info())
      : routing(routing_inf)
      , mid_point_(mid_point)
      , area_m2(area)
      , catchment_id_(catchment_id)
      , radiation_slope_factor_(radiation_slope_factor)
      , fractions(land_type_fraction) {
    }

    geo_cell_data(
      geo_point const &p1,
      geo_point const &p2,
      geo_point const &p3,
      std::int64_t epsg_id = 0,
      std::int64_t catchment_id = -1,
      land_type_fractions const &fractions = land_type_fractions(),
      routing_info routing_inf = routing_info())
      : routing(routing_inf)
      , catchment_id_(catchment_id)
      , fractions(fractions)
      , v1{p1}
      , v2{p2}
      , v3{p3}
      , epsg_id{epsg_id} {
      mid_point_ = calc_mid_point();
      area_m2 = calc_tin_horizontal_projected_area();
      radiation_slope_factor_ = 1.0; // no factor required if we have TIN
      set_catchment_id(catchment_id);
    }

    geo_point const &mid_point() const {
      return mid_point_;
    }

    cid_t catchment_id() const {
      return catchment_id_;
    }

    void set_catchment_id(cid_t catchmentid) {
      catchment_id_ = catchmentid;
    }

    double radiation_slope_factor() const {
      return radiation_slope_factor_;
    }

    land_type_fractions const &land_type_fractions_info() const {
      return fractions;
    }

    void set_land_type_fractions(land_type_fractions const &ltf) {
      fractions = ltf;
    }

    double area() const {
      return area_m2;
    }

    double rarea() const {
      return calc_tin_face_area();
    }

    bool operator==(geo_cell_data const &o) const {
      return o.catchment_id_ == catchment_id_ && mid_point_ == o.mid_point_ && std::abs(area_m2 - o.area_m2) < 0.1
          && fractions == o.fractions && routing == o.routing && (v1 == o.v1 && v2 == o.v2 && v3 == o.v3);
    }

    bool operator!=(geo_cell_data const &o) const {
      return !operator==(o);
    }

    // if tin-cell
    bool is_monocell() const {
      return fractions.is_monocell();
    }

    double slope() const {
      return compute_slope(normal());
    }

    double aspect() const {
      return compute_aspect(normal());
    }

    /** @brief compute and return the representative mid-point, as longitude, latitude point
     *
     * @note require the epsg_id to be set properly
     */
    geo_point_ll mid_point_as_long_lat() const {
      if (epsg_id == 0)
        throw std::runtime_error(
          "geo_cell_data require epsg_id to be set in order to provide longitude, latitude calculations");
      return convert_to_longitude_latitude(mid_point_, epsg_id);
    }

    std::vector<geo_point> vertexes() const {
      return std::vector<geo_point>{v1, v2, v3};
    }

    // deprecated, we should use constructor only, and then consider this data-type immutable/const
    void set_tin_data(std::vector<geo_point> const &v) {
      if (v.size() != 3)
        throw std::runtime_error("geo_cell_data::set_tin_data needs 3 geo_points to form a tin");
      v1 = v[0];
      v2 = v[1];
      v3 = v[2];
      mid_point_ = calc_mid_point();
      area_m2 = calc_tin_horizontal_projected_area();
    }
   public:
    size_t catchment_ix =
      0; // internally generated zero-based catchment index, used to correlate to calc-filter, ref. region_model
    routing_info routing; ///< keeps the geo-static routing info, where it routes to, and routing distance.
   private:
    geo_point mid_point_; // midpoint, representative midpoint in metric cartesian coordinates. z denotes representative
                          // m.a.s.l (in case of tin: computed)
    double area_m2{0};    // m2, area as projected down to horizontal earth-plane (in case of tin, computed)
    cid_t catchment_id_;  // catchment id, cells in same catchment share this catchment_id.
    double radiation_slope_factor_; // in case of tin, set to 1.0
    land_type_fractions fractions;
    geo_point v1, v2, v3; // 3 vertexes of the tin, in case no tin: they are 0.0, slope/aspect etc. -> nan
   public:
    std::int64_t epsg_id{0}; // the coordinate projection of geo_points, _must_ be consistent accross cells in a model,
                             // used for converting to longitude,latitude
   private:
    geo_point calc_mid_point() const {
      return geo_point((v1.x + v2.x + v3.x) / 3, (v1.y + v2.y + v3.y) / 3, (v1.z + v2.z + v3.z) / 3);
    }

    /** @brief compute tin area as projected down to horizontal plane */
    double calc_tin_horizontal_projected_area() const {
      // here area of projected cell should be calculated, not full cell
      // so I'm taking only x,y -- coordinates
      geo_point v1_(v1.x, v1.y, 0.0);
      geo_point v2_(v2.x, v2.y, 0.0);
      geo_point v3_(v3.x, v3.y, 0.0);
      double a = std::sqrt(geo_point::distance2(v1_, v2_));
      double b = std::sqrt(geo_point::distance2(v2_, v3_));
      double c = std::sqrt(geo_point::distance2(v3_, v1_));
      double s = (a + b + c) * 0.5;                      // semiperimeter
      return std::sqrt(s * (s - a) * (s - b) * (s - c)); // Heron's formula
    }

    /** @brief compute tin face area (total area) */
    double calc_tin_face_area() const {
      // tin sides
      // real cell area
      double a = std::sqrt(geo_point::distance2(v1, v2));
      double b = std::sqrt(geo_point::distance2(v2, v3));
      double c = std::sqrt(geo_point::distance2(v3, v1));
      double s = (a + b + c) * 0.5;                      // semiperimeter
      return std::sqrt(s * (s - a) * (s - b) * (s - c)); // Heron's formula
    }

    /** @brief return the upward-pointing normal vector using  [v2-v1][ and [v3-v1]*/
    arma::vec normal() const {
      const arma::vec::fixed<3> vv0 = {v2.x - v1.x, v2.y - v1.y, v2.z - v1.z};
      const arma::vec::fixed<3> vv1 = {v3.x - v1.x, v3.y - v1.y, v3.z - v1.z};
      arma::vec::fixed<3> n = arma::cross(vv0, vv1);
      n /= arma::norm(n);
      return n[2] >= 0.0 ? arma::vec{n[0], n[1], n[2]}
                         : arma::vec{-n[0], -n[1], -n[2]}; // TODO: explain normal convention, upward pointing?
    }

    /** @brief computes the slope given normal-vector */
    double compute_slope(arma::vec const &normal) const {
      return std::atan2(pow(pow(normal[0], 2) + pow(normal[1], 2), 0.5), normal[2]);
    }

    /** @brief computes the aspect  given normal-vector */
    double compute_aspect(arma::vec const &normal) const {
      return std::atan2(normal[0], normal[1]);
    }

    //
    // geo-type  parts, interesting for some/most response routines, sum fractions should be <=1.0
    x_serialize_decl();
  };

  /**
   * @brief
   * A minimal model with id for a vector of geo_cell_data
   * @details
   * The bare minimum for a generic model is that it keeps an id
   * Usually, we could add name, description, etc. but we leave that for now
   */
  struct gcd_model {
    std::int64_t id{0};
    std::vector<geo_cell_data> gcd;

    SHYFT_DEFINE_STRUCT(gcd_model, (), (id, gcd));
    auto operator<=>(gcd_model const &) const = default;

    x_serialize_decl();
  };

}

//-- serialization support shyft
x_serialize_export_key(shyft::core::land_type_fractions);
x_serialize_export_key(shyft::core::geo_cell_data);
x_serialize_export_key(shyft::core::routing_info);
x_serialize_export_key(shyft::core::gcd_model);
BOOST_CLASS_VERSION(shyft::core::geo_cell_data, 1)

template <typename Char>
struct fmt::formatter<shyft::core::gcd_model, Char> : shyft::fn_formatter<&shyft::core::gcd_model::id, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::core::gcd_model>, Char>
  : shyft::ptr_formatter<shyft::core::gcd_model, Char> { };
