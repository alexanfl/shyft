/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <shyft/core/core_serialization.h>
#include <shyft/core/math_utilities.h>

namespace shyft::core {

  /**
   * @brief q_adjust_result
   * @details
   * The result of region_model state-adjustemt
   */
  struct q_adjust_result {
    double q_0{0.0};         ///< m3/s  for the timestep before adjustemnt
    double q_r{0.0};         ///< m3/s for the timestep after adjustemnt
    std::string diagnostics; ///< empty if ok, otherwise diagnostics
    q_adjust_result() = default;

    bool operator==(q_adjust_result const &x) const {
      return nan_equal(q_0, x.q_0) && nan_equal(q_r, x.q_r) && diagnostics == x.diagnostics;
    }

    bool operator!=(q_adjust_result const &x) const {
      return !operator==(x);
    }

    x_serialize_decl();
  };
}

//-- serialization support shyft
x_serialize_export_key(shyft::core::q_adjust_result);
