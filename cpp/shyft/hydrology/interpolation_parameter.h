/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/spatial/bayesian_kriging.h>
#include <shyft/hydrology/spatial/inverse_distance.h>

namespace shyft::core {
  namespace idw = inverse_distance;
  namespace btk = bayesian_kriging;

  /**
   * @brief interpolation parameter settings for the region-model
   * @details
   * The interpolation_parameter keeps parameter needed to perform the
   * interpolation steps as specified by the user.
   */
  struct interpolation_parameter {
    using btk_parameter_t = btk::parameter;
    using idw_precipitation_parameter_t = idw::precipitation_parameter;
    using idw_temperature_parameter_t = idw::temperature_parameter;
    using idw_parameter_t = idw::parameter;

    btk::parameter temperature;
    bool use_idw_for_temperature = false;
    idw::temperature_parameter temperature_idw;
    idw::precipitation_parameter precipitation;
    idw::parameter wind_speed;
    idw::parameter radiation;
    idw::parameter rel_hum;

    interpolation_parameter() {
    }

    interpolation_parameter(
      btk::parameter const & temperature,
      idw::precipitation_parameter const & precipitation,
      idw::parameter const & wind_speed,
      idw::parameter const & radiation,
      idw::parameter const & rel_hum)
      : temperature(temperature)
      , precipitation(precipitation)
      , wind_speed(wind_speed)
      , radiation(radiation)
      , rel_hum(rel_hum) {
    }

    interpolation_parameter(
      idw::temperature_parameter const & temperature,
      idw::precipitation_parameter const & precipitation,
      idw::parameter const & wind_speed,
      idw::parameter const & radiation,
      idw::parameter const & rel_hum)
      : use_idw_for_temperature(true)
      , temperature_idw(temperature)
      , precipitation(precipitation)
      , wind_speed(wind_speed)
      , radiation(radiation)
      , rel_hum(rel_hum) {
    }

    bool operator==(interpolation_parameter const & x) const {
      return temperature == x.temperature && use_idw_for_temperature == x.use_idw_for_temperature
          && temperature_idw == x.temperature_idw && precipitation == x.precipitation && wind_speed == x.wind_speed
          && radiation == x.radiation && rel_hum == x.rel_hum;
    }

    bool operator!=(interpolation_parameter const & x) const {
      return !operator==(x);
    }

    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::core::interpolation_parameter);
