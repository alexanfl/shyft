#include <sstream>
#include <mutex>
#include <shyft/core/fs_compat.h>
#include <shyft/srv/model_info.h>
#include <shyft/srv/db_rocks.h>
#include <rocksdb/write_batch.h>
#include <rocksdb/env.h>
#include <rocksdb/cache.h>
#include <rocksdb/utilities/leveldb_options.h>
#include <rocksdb/table.h>
#include <rocksdb/advanced_cache.h>
//#include <shyft/srv/db_file.h> // for automagic migration
#include <shyft/srv/db_level.h> // for automagic migration

namespace shyft::srv {

  void db_rocks_migrate(db_rocks &r, string root_dir) {
    db_level f{root_dir};
    auto all_mi = f.get_model_infos({});
    for (auto const &mi : all_mi) {
      auto m = f.read_model_blob(mi.id);
      r.store_model_blob(m, mi);
    }
  }

  db_rocks::~db_rocks() {
    // unique ptr goes out of scope here
  }

  db_rocks::db_rocks(string const &root_dir)
    : root_dir{root_dir} {
    bool migrate_check{true};
    if (!fs::is_directory(root_dir)) {
      migrate_check = false;
      if (!fs::exists(root_dir)) {
        if (!fs::create_directories(root_dir))
          throw runtime_error(string("m_db: failed to create root directory :") + root_dir);
      } else
        throw runtime_error(string("m_db: designated root directory is not a directory:") + root_dir);
    }

    options.create_if_missing = true;
    rocksdb::DB *dbx{nullptr};
    rocksdb::DB *db_mix{nullptr};

    auto open_or_throw = [&](auto path, auto &db) {
      auto s = rocksdb::DB::Open(options, path, &db);
      if (!s.ok())
        throw runtime_error(string("Failed to open db, maybe another instance running? path=") + path);
    };

    open_or_throw(root_dir + "/rd", dbx);
    db = unique_ptr<rocksdb::DB>(dbx);
    open_or_throw(root_dir + "/ri", db_mix);
    db_mi = unique_ptr<rocksdb::DB>(db_mix);
    uid = find_max_model_id(true);
    if (migrate_check && uid == 0) {
      db_rocks_migrate(*this, root_dir);
    }
  }

  int64_t db_rocks::find_max_model_id(bool fill_cache) {
    std::unique_ptr<rocksdb::Iterator> it(db->NewIterator(rocksdb::ReadOptions()));
    int64_t max_id = 0;
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
      string fn = it->key().ToString();
      int64_t f_id = stoi(fn);
      if (f_id > 0) {
        max_id = std::max(max_id, f_id);
        if (fill_cache) {
          model_info mi;
          if (!info_cache.try_get_item(f_id, mi)) { // only re-read items not in cache
            auto mi = read_model_info(fn);
            if (mi) {
              info_cache.add_item(f_id, mi.value());
            }
          }
          if (info_cache.size() * 2 > info_cache.get_capacity()) {
            info_cache.set_capacity(info_cache.size() * 2); // grow cap to double sz.
          }
        }
      }
    }
    return max_id;
  }

  std::optional<model_info> db_rocks::read_model_info(string const &fn) const {
    string mis;
    rocksdb::Status s = db_mi->Get(rocksdb::ReadOptions(), fn, &mis);
    if (s.ok()) {
      model_info r;
      std::istringstream ifs(mis, std::ios_base::binary);
      core_iarchive ia(ifs, core_arch_flags);
      ia >> r;
      return r;
    }
    return {};
  }

  std::optional<model_info> db_rocks::read_model_info(int64_t mid) const {
    return read_model_info(to_string(mid));
  }

  vector<model_info> db_rocks::get_model_infos(vector<int64_t> const &mids) {
    vector<model_info> r;
    vector<string> fns;
    if (!mids.size()) {
      lock_guard<mutex> guard(mx);   // we need to ensure exclusive access here
      uid = find_max_model_id(true); // rescan directory read items not in cache
      info_cache.apply_to_items([&r](int64_t /*mid*/, model_info const &mi) {
        r.push_back(mi);
      });
    } else {
      for (auto mid : mids) {
        model_info mi;
        if (try_get_info_item(mid, mi)) {
          r.push_back(mi);
        } else {
          auto omi = read_model_info(mid);
          if (omi) {
            r.push_back(omi.value());
            add_info_item(mid, omi.value());
          }
        }
      }
    }
    return r;
  }

  vector<model_info> db_rocks::get_model_infos(vector<int64_t> const &mids, utcperiod per) {
    vector<model_info> r;
    vector<string> fns;
    if (!mids.size()) {
      lock_guard<mutex> guard(mx); // we need to ensure exclusice access here
      uid = find_max_model_id(true);
      info_cache.apply_to_items([&r, &per](int64_t /*mid*/, model_info const &mi) {
        if (per.contains(mi.created)) {
          r.push_back(mi);
        }
      });
    } else {
      for (auto mid : mids) {
        model_info mi;
        if (try_get_info_item(mid, mi)) {
          if (per.contains(mi.created)) {
            r.push_back(mi);
          }
        } else {
          auto omi = read_model_info(mid);
          if (omi) {
            if (per.contains(omi.value().created)) {
              r.push_back(omi.value());
            }
            add_info_item(mid, omi.value());
          }
        }
      }
    }
    return r;
  }

  bool db_rocks::update_model_info(int64_t mid, model_info const &mi) {
    if (mi.id != mid) {
      throw runtime_error(
        "update_model_info: mid must equal mi.id (unfortunate design hmm.);" + to_string(mid)
        + "!=" + to_string(mi.id));
    }

    string smid = to_string(mid);
    std::ostringstream xmls(std::ios::binary);
    /* scope this, ensure archive is flushed/destroyd */
    // TODO This should probably be moved to dh::storemodel
    {
      core_oarchive osi(xmls, core_arch_flags);
      model_info tmi{mi};
      tmi.id = mid; // enforce same id
      osi << tmi;
      add_info_item(mid, tmi);
    }
    xmls.flush();
    auto s = db_mi->Put(rocksdb::WriteOptions(), smid, xmls.str());
    if (!s.ok())
      throw std::runtime_error("rocksdb::failed:" + s.ToString());
    return true;
  }

  int64_t db_rocks::store_model_blob(string const &m, model_info const &mi) {
    int64_t mid = mi.id;
    if (mid <= 0) {
      mid = mk_unique_model_id();
    } else {
      if (mid > uid) { // update uid to max uid when storing new models
        uid = mid;
      }
    }
    string smid = to_string(mid);
    auto s = db->Put(rocksdb::WriteOptions(), smid, m);
    if (!s.ok())
      throw std::runtime_error("rocksdb::failed:" + s.ToString());

    std::ostringstream xmls(std::ios::binary);
    /* scope this, ensure archive is flushed/destroyd */
    // TODO This should probably be moved to dh::storemodel
    {
      core_oarchive osi(xmls, core_arch_flags);
      model_info tmi{mi};
      tmi.id = mid; // enforce same id
      osi << tmi;
      add_info_item(mid, tmi);
    }
    xmls.flush();
    s = db_mi->Put(rocksdb::WriteOptions(), smid, xmls.str());
    if (!s.ok())
      throw std::runtime_error("rocksdb::failed:" + s.ToString());

    return mid;
  }

  string db_rocks::read_model_blob(int64_t mid) const {
    string smid = to_string(mid);
    string mb;
    auto s = db->Get(rocksdb::ReadOptions(), smid, &mb);
    if (!s.ok())
      throw std::runtime_error("rocksdb::failed:" + s.ToString());
    return mb;
  }

  int64_t db_rocks::remove_model(int64_t mid) {
    string smid = to_string(mid);
    db->Delete(rocksdb::WriteOptions(), smid);
    db_mi->Delete(rocksdb::WriteOptions(), smid);
    // here we think it is ok to just be silent if it fails
    remove_info_item(mid);
    return 0;
  }

}
