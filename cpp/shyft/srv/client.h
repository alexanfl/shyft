/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <shyft/srv/fast_iosockstream.h>
#include <thread>
#include <shyft/srv/msg_defs.h>
#include <shyft/srv/model_info.h>
#include <shyft/core/core_archive.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/dlib_utils.h>

namespace shyft::srv {
  using std::vector;
  using std::string;
  using std::string_view;
  using std::to_string;
  using std::runtime_error;
  using std::exception;
  using std::chrono::seconds;
  using std::chrono::milliseconds;
  using shyft::core::core_iarchive;
  using shyft::core::core_oarchive;
  using shyft::core::core_arch_flags;
  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using shyft::core::from_seconds;
  using std::unique_ptr;
  using std::shared_ptr;
  using std::make_unique;
  using std::max;
  using std::this_thread::sleep_for;
  using shyft::core::srv_connection;
  using shyft::core::scoped_connect;
  using shyft::core::do_io_with_repair_and_retry;
  using shyft::srv::model_info;

  /** @brief a patch for post processing of model after being read from server.
   *
   * Somewhere:
   *    template <>
   *  struct receive_patch<core_mdl> {
   *        static void apply(core_mdl& m) { <Do some processing>; }
   *    }
   */
  template <class M>
  struct receive_patch {
    static void apply(std::shared_ptr<M> const &) {
    }
  };

  /** @brief a client that matches the server for model type M
   *
   *
   * This class take care of message exchange to the remote server,
   * using the supplied connection parameters.
   *
   * It implements the message protocol of the server, sending
   * message-prefix, arguments, waiting for response
   * deserialize the response and handle it back to the user.
   *
   * @see server
   *
   * @tparam M a serializeable model type
   *
   *
   */
  template <class M>
  struct client {
    using M_t = M;
    srv_connection c;

    client() {
    }

    client(string host_port, int timeout_ms = 1000)
      : c{host_port, timeout_ms} {
    }

    /** @brief provide model information
     *
     *  @param mids model-info identifiers of interest, if empty, return all
     *  @param created_in: Utcperiod for which model-infos are of interest. Default is (jan 1 1970, jan 1 2038)
     *  @return a list of model-info
     */
    vector<model_info> get_model_infos(vector<int64_t> const & mids, utcperiod created_in = utcperiod()) {
      // no validation needed on input args,
      // if mids.size()==0, it means give me all you have.
      // if >0, it will succeed all, or fail
      scoped_connect sc(c);
      vector<model_info> r;
      do_io_with_repair_and_retry(c, [&mids, &r, &created_in](srv_connection& c) {
        auto& io = *c.io;
        core_oarchive oa(io, core_arch_flags);
        if (created_in.valid()) {
          msg::write_type(message_type::MODEL_INFO_FILTERED, io);
          oa << mids << created_in;
        } else {
          msg::write_type(message_type::MODEL_INFO, io);
          oa << mids;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
          auto re = msg::read_exception(io);
          throw re;
        } else if (response_type == message_type::MODEL_INFO) {
          core_iarchive ia(io, core_arch_flags);
          ia >> r;
        } else {
          throw runtime_error(string("Got unexpected response:") + to_string((int) response_type));
        }
      });
      return r;
    }

    /** @brief store the model to backend with model-info
     *
     * Forward the model to the backend for storage.
     *
     * @param m the model to store
     * @param mi the model-info to store
     * @return new model-id if m->id and m.id=0, otherwise returns the supplied model-id
     *
     * @note We still consider to remove const, and set model-id to the obtained value
     */
    int64_t store_model(shared_ptr<M> const & m, model_info const & mi) {
      scoped_connect sc(c);
      int64_t r{0};
      do_io_with_repair_and_retry(c, [&m, &mi, &r](srv_connection& c) {
        auto& io = *c.io;
        msg::write_type(message_type::MODEL_STORE, io);
        core_oarchive oa(io, core_arch_flags);
        oa << m << mi;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
          auto re = msg::read_exception(io);
          throw re;
        } else if (response_type == message_type::MODEL_STORE) {
          core_iarchive ia(io, core_arch_flags);
          ia >> r;
        } else {
          throw runtime_error(string("Got unexpected response:") + to_string((int) response_type));
        }
      });
      return r;
    }

    /** @brief read the model from server
     *
     * Read a model with specified model-id from server. The model with specified mid
     * must exists.
     *
     * @param mid the model-id to read
     * @return new read model
     *
     */
    shared_ptr<M> read_model(int64_t mid) {
      return read_models(vector<int64_t>{mid})[0];
    }

    /** @brief read multiple models from server
     *
     * Read a models with specified model-id from server. The model with specified mid
     * must exists. This is done by first posting all requests to the server,
     * then reading back the responses.
     *
     * @see read_model
     *
     * @param mids the model-ids to read
     * @return list of read models
     *
     */
    vector<shared_ptr<M>> read_models(vector<int64_t> mids) {
      if (mids.size() == 0)
        throw runtime_error("List of model-ids must hold at least one element");
      for (auto const & m : mids)
        if (m <= 0)
          throw runtime_error("The supplied model-id must be >0");
      scoped_connect sc(c);
      vector<shared_ptr<M>> rr;
      do_io_with_repair_and_retry(c, [&mids, &rr](srv_connection& c) {
        auto& io = *c.io;
        for (size_t i = 0; i < mids.size(); ++i) { // first just send all requests
          msg::write_type(message_type::MODEL_READ, io);
          core_oarchive oa(io, core_arch_flags);
          oa << mids[i];
        }
        for (size_t i = 0; i < mids.size(); ++i) { // then read back each request
          auto response_type = msg::read_type(io);
          if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
          } else if (response_type == message_type::MODEL_READ) {
            core_iarchive ia(io, core_arch_flags);
            shared_ptr<M> r;
            ia >> r;
            r->id = mids[i]; // ensure consistency regardless content of file.
            receive_patch<M>::apply(r);
            rr.emplace_back(r);
          } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int) response_type));
          }
        }
      });
      return rr;
    }

    /** @brief remove the model to backend with model-info
     *
     * Read a model with specified model-id from server. The model with specified mid
     * must exists.
     *
     * @param mid the model-id to remove
     * @return error code ?
     *
     */
    int64_t remove_model(int64_t mid) {
      if (mid <= 0)
        throw runtime_error("remove_model require model-id arg mid >0");
      scoped_connect sc(c);
      int64_t r;
      do_io_with_repair_and_retry(c, [&mid, &r](srv_connection& c) {
        auto& io = *c.io;
        msg::write_type(message_type::MODEL_DELETE, io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
          auto re = msg::read_exception(io);
          throw re;
        } else if (response_type == message_type::MODEL_DELETE) {
          core_iarchive ia(io, core_arch_flags);
          ia >> r;
        } else {
          throw runtime_error(string("Got unexpected response:") + to_string((int) response_type));
        }
      });
      return r;
    }

    /** @brief update model-info
     *
     * Update the model-info of specified model, with id=mid.The model with specified mid
     * must exists.
     *
     * @param mid the model-id to which we update the model-info on
     * @return true if succeeded (model exists, and was removed)
     *
     */
    bool update_model_info(int64_t mid, model_info const & mi) {
      scoped_connect sc(c);
      bool r{false};
      do_io_with_repair_and_retry(c, [&mid, &mi, &r](srv_connection& c) {
        auto& io = *c.io;
        msg::write_type(message_type::MODEL_INFO_UPDATE, io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << mi;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
          auto re = msg::read_exception(io);
          throw re;
        } else if (response_type == message_type::MODEL_INFO_UPDATE) {
          core_iarchive ia(io, core_arch_flags);
          ia >> r;
        } else {
          throw runtime_error(string("Got unexpected response:") + to_string((int) response_type));
        }
      });
      return r;
    }

    /** @brief close, until needed again, the server connection
     *
     */
    void close() {
      c.close(); // just close-down connection, it will auto-open if needed
    }
  };

}
