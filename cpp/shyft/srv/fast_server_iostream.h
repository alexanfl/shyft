// Copyright (C) 2006  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.
#pragma once

// notice: This file is based entirely on Davis Kings server_iostream file, only diffs are namespace and disable naggle
// on socket connections
//         so that we get faster small packets over the network (vital for the drms services that when scaled up have a
//         lot of small packet exchanges)
//

#include <iosfwd>
#include <dlib/logger.h>
#include <dlib/uintn.h>
#include <dlib/server/server_kernel.h>
#include <dlib/sockstreambuf.h>
#include <dlib/map.h>

namespace shyft::srv {
  using dlib::uint64;
  using dlib::auto_mutex;

  class fast_server_iostream : public dlib::server {

    /*!
        INITIAL VALUE
            - next_id == 0
            - con_map.size() == 0

        CONVENTION
            - next_id == the id of the next connection
            - for all current connections
                - con_map[id] == the connection object with the given id
            - m == the mutex that protects the members of this object
    !*/

    typedef dlib::map<uint64, dlib::connection*, dlib::memory_manager<char>::kernel_2a>::kernel_1b id_map;

   public:
    fast_server_iostream();

    ~fast_server_iostream();

   protected:
    void shutdown_connection(uint64 id);

   private:
    virtual void on_connect(
      std::istream& in,
      std::ostream& out,
      std::string const & foreign_ip,
      std::string const & local_ip,
      unsigned short foreign_port,
      unsigned short local_port,
      uint64 connection_id) = 0;

    void on_connect(dlib::connection& con);


    uint64 next_id;
    id_map con_map;
    const static dlib::logger _dLog;
    dlib::mutex m;
  };


}
