// Copyright (C) 2012  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.

// notice: This file is based entirely on Davis Kings iosockstream file, only diffs are namespace and disable naggle on
// socket connections
//         so that we get faster small packets over the network (vital for the drms services that when scaled up have a
//         lot of small packet exchanges)
//
#include <shyft/srv/fast_iosockstream.h>
#include <iostream>

namespace shyft::srv {

  fast_iosockstream::fast_iosockstream()
    : std::iostream(0) {
  }

  fast_iosockstream::fast_iosockstream(network_address const & addr)
    : std::iostream(0) {
    open(addr);
  }

  fast_iosockstream::fast_iosockstream(network_address const & addr, unsigned long timeout)
    : std::iostream(0) {
    open(addr, timeout);
  }

  fast_iosockstream::~fast_iosockstream() {
    close();
  }

  void fast_iosockstream::open(network_address const & addr) {
    auto_mutex lock(class_mutex);
    close();
    con.reset(connect(addr));
    con->disable_nagle();
    buf.reset(new sockstreambuf(con.get()));
    // Note that we use the sockstreambuf's ability to autoflush instead of
    // telling the iostream::tie() function to tie the stream to itself even though
    // that should work fine.  The reason we do it this way is because there is a
    // bug in visual studio 2012 that causes a program to crash when a stream is
    // tied to itself and then used.  See
    // http://connect.microsoft.com/VisualStudio/feedback/details/772293/tying-a-c-iostream-object-to-itself-causes-a-stack-overflow-in-visual-studio-2012
    // for further details.
    buf->flush_output_on_read();
    rdbuf(buf.get());
    clear();
  }

  void fast_iosockstream::open(network_address const & addr, unsigned long timeout) {
    auto_mutex lock(class_mutex);
    close(timeout);
    con.reset(connect(addr.host_address, addr.port, timeout));
    con->disable_nagle();
    buf.reset(new sockstreambuf(con.get()));
    buf->flush_output_on_read();
    rdbuf(buf.get());
    clear();
  }

  void fast_iosockstream::close(unsigned long timeout) {
    auto_mutex lock(class_mutex);
    rdbuf(0);
    try {
      if (buf) {
        dlib::timeout t(*con, &connection::shutdown, timeout);

        // This will flush the sockstreambuf and also destroy it.
        buf.reset();

        if (con->shutdown_outgoing()) {
          // there was an error so just close it now and return
          con->shutdown();
        } else {
          char junk[100] = {0};
          // wait for the other end to close their side
          while (con->read(junk, sizeof(junk)) > 0)
            ;
        }
      }
    } catch (...) {
      con.reset();
      throw;
    }
    con.reset();
  }

  void fast_iosockstream::terminate_connection_after_timeout(unsigned long timeout) {
    auto_mutex lock(class_mutex);
    if (con) {
      con_timeout.reset(new dlib::timeout(*this, &fast_iosockstream::terminate_connection, timeout, con));
    }
  }

  void fast_iosockstream::shutdown() {
    auto_mutex lock(class_mutex);
    if (con)
      con->shutdown();
  }

  void fast_iosockstream::terminate_connection(std::shared_ptr<connection> thecon) {
    thecon->shutdown();
  }

}
