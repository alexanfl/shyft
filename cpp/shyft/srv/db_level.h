#pragma once

#include <atomic>
#include <string>
#include <memory>

#include <leveldb/db.h>
#include <shyft/core/core_archive.h>
#include <shyft/srv/db_io.h>

namespace shyft::srv {

  using std::string;
  using std::runtime_error;
  using std::to_string;
  using std::stoi;
  using std::unique_ptr;
  using std::ifstream;

  using shyft::core::core_iarchive;
  using shyft::core::core_oarchive;
  using shyft::core::core_arch_flags;

  struct db_level : db_io {
    string root_dir;
    unique_ptr<leveldb::DB> db;
    unique_ptr<leveldb::DB> db_mi;
    leveldb::Options options;

    db_level() = delete;
    db_level(db_level const &) = delete;
    db_level(string const &root_dir);
    ~db_level();
    vector<model_info> get_model_infos(vector<int64_t> const &mids) override;
    vector<model_info> get_model_infos(vector<int64_t> const &mids, utcperiod per) override;
    bool update_model_info(int64_t mid, model_info const &mi) override;
    int64_t store_model_blob(string const &m, model_info const &mi) override;
    string read_model_blob(int64_t mid) const override;
    int64_t remove_model(int64_t mid) override;
    int64_t find_max_model_id(bool fill_cache = false);

   private:
    std::optional<model_info> read_model_info(string const &fn) const;
    std::optional<model_info> read_model_info(int64_t mid) const;
  };

}
