#include <memory>
#include <string>
#include <sstream>
#include <cstdint>
#include <exception>
#include <regex>
#include <atomic>
#include <fstream>
#include <unordered_map>
#include <list>
#include <mutex>
#include <optional>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <shyft/core/core_archive.h>
#include <shyft/time/utctime_utilities.h>

#include <shyft/core/fs_compat.h>
#include <shyft/srv/model_info.h>
#include <shyft/srv/db_file.h>

namespace shyft::srv {

  using std::to_string;
  using std::stoi;
  using std::shared_ptr;
  using std::ifstream;
  using std::ofstream;
  using std::lock_guard;

  using shyft::core::core_iarchive;
  using shyft::core::core_oarchive;
  using shyft::core::core_arch_flags;

  db_file::db_file(string const & root_dir)
    : root_dir{root_dir} {
    if (fs::is_directory(root_dir)) {
      uid = find_max_model_id(true); // initialize id-generator
      // should we also read and cache all model-infos?
      // .. and models ?
    } else {
      if (!fs::exists(root_dir)) {
        if (!fs::create_directories(root_dir))
          throw runtime_error(string("m_db: failed to create root directory :") + root_dir);
      } else
        throw runtime_error(string("m_db: designated root directory is not a directory:") + root_dir);
    }
  }

  int64_t db_file::find_max_model_id(bool fill_cache) {
    fs::path root(root_dir);
    string match = "\\d+\\.i\\.db";
    std::regex r_match(match, std::regex_constants::ECMAScript | std::regex_constants::icase);
    int64_t max_id = 0;
    for (auto& x : fs::directory_iterator(root)) {
      if (fs::is_regular_file(x.path())) {
        string fn = x.path().lexically_relative(root).generic_string(); // x.path() except root-part
        if (std::regex_search(fn, r_match)) {
          int64_t f_id = stoi(fn);
          if (f_id > 0) {
            max_id = std::max(max_id, f_id);
            if (fill_cache) {
              model_info mi;
              if (!info_cache.try_get_item(f_id, mi)) { // only re-read items not in cache
                auto mi = read_model_info(fn);
                if (mi) {
                  info_cache.add_item(f_id, mi.value());
                }
              }
            }
          }
        }
      }
    }
    return max_id;
  }

  std::optional<model_info> db_file::read_model_info(string const & fn) const {
    auto fpath = (root_dir / fs::path(fn)).generic_string();
    if (fs::exists(fpath)) {
      model_info r;
      ifstream ifs(fpath, std::ios_base::binary);
      core_iarchive ia(ifs, core_arch_flags);
      ia >> r;
      return r;
    }
    return {};
  }

  std::optional<model_info> db_file::read_model_info(int64_t mid) const {
    return read_model_info((fs::path(root_dir) / (to_string(mid) + ".i.db")).generic_string());
  }

  /** return available model-infos, refresh cache with new discovered files*/
  vector<model_info> db_file::get_model_infos(vector<int64_t> const & mids) {
    vector<model_info> r;
    vector<string> fns;
    if (!mids.size()) {
      lock_guard<mutex> guard(mx);   // we need to ensure exclusive access here
      uid = find_max_model_id(true); // rescan directory read items not in cache
      info_cache.apply_to_items([&r](int64_t /*mid*/, model_info const & mi) {
        r.push_back(mi);
      });
    } else {
      for (auto mid : mids) {
        model_info mi;
        if (try_get_info_item(mid, mi)) {
          r.push_back(mi);
        } else {
          auto omi = read_model_info(mid);
          if (omi) {
            r.push_back(omi.value());
            add_info_item(mid, omi.value());
          }
        }
      }
    }
    return r;
  }

  vector<model_info> db_file::get_model_infos(vector<int64_t> const & mids, utcperiod per) {
    vector<model_info> r;
    vector<string> fns;
    if (!mids.size()) {
      lock_guard<mutex> guard(mx); // we need to ensure exclusice access here
      uid = find_max_model_id(true);
      info_cache.apply_to_items([&r, &per](int64_t /*mid*/, model_info const & mi) {
        if (per.contains(mi.created)) {
          r.push_back(mi);
        }
      });
    } else {
      for (auto mid : mids) {
        model_info mi;
        if (try_get_info_item(mid, mi)) {
          if (per.contains(mi.created)) {
            r.push_back(mi);
          }
        } else {
          auto omi = read_model_info(mid);
          if (omi) {
            if (per.contains(omi.value().created)) {
              r.push_back(omi.value());
            }
            add_info_item(mid, omi.value());
          }
        }
      }
    }
    return r;
  }

  int64_t db_file::store_model_blob(string const & m, model_info const & mi) {
    int64_t mid = mi.id;
    if (mid <= 0) {
      mid = mk_unique_model_id();
    }
    { // TODO: consider mutable m, so that we can set the m.id to mid here
      auto fnm = (fs::path(root_dir) / (to_string(mid) + ".m.db")).generic_string();
      ofstream ofm(fnm, std::ios::binary | std::ios::trunc);
      ofm << m;
    }
    {
      auto fni = (fs::path(root_dir) / (to_string(mid) + ".i.db")).generic_string();
      ofstream ofi(fni, std::ios::binary | std::ios::trunc);
      core_oarchive osi(ofi, core_arch_flags);
      model_info tmi{mi};
      tmi.id = mid; // enforce same id
      osi << tmi;
      add_info_item(mid, tmi);
    }

    return mid;
  }

  // used to provide a raw blob for server-side read (avoid serialize cycle on server-side)
  string db_file::read_model_blob(int64_t mid) const {
    auto fn = (fs::path(root_dir) / (to_string(mid) + ".m.db")).generic_string();

    if (!fs::exists(fn))
      throw runtime_error("read_model: missing file:" + fn);
    if (!fs::is_regular_file(fn))
      throw runtime_error("read_model: not a regular file:" + fn);

    ifstream input(fn, std::ios::binary);
    std::ostringstream buf;
    buf << input.rdbuf();
    return buf.str();
  }

  int64_t db_file::remove_model(int64_t mid) {
    vector<fs::path> fns{
      (fs::path(root_dir) / (to_string(mid) + ".m.db")), (fs::path(root_dir) / (to_string(mid) + ".i.db"))};
    for (auto const & fn : fns) {
      if (fs::exists(fn)) {
        fs::remove(fn);
      }
    }

    remove_info_item(mid);
    return 0; // TODO: consider signature, should we say, remove succeed if model is gone after (regardless
              // precondition) ?
  }

  bool db_file::update_model_info(int64_t mid, model_info const & mi) {
    if (mi.id != mid) {
      throw runtime_error(
        "update_model_info: mid must equal mi.id (unfortunate design hmm.);" + to_string(mid)
        + "!=" + to_string(mi.id));
    }
    vector<int64_t> mids;
    mids.push_back(mid);
    auto me = get_model_infos(mids);
    auto fni = (fs::path(root_dir) / fs::path((to_string(mid) + ".i.db"))).generic_string();
    model_info tmi{mi};
    tmi.id = mid; // make sure to store correct id
    ofstream ofi(fni, std::ios::binary | std::ios::trunc);
    core_oarchive osi(ofi, core_arch_flags);
    osi << tmi;
    add_info_item(mid, tmi);
    return true;
  }


}
