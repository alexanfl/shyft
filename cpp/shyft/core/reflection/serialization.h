#pragma once

#include <concepts>
#include <cstdint>
#include <iosfwd>
#include <type_traits>

#include <boost/mp11/algorithm.hpp>
#include <boost/describe/bases.hpp>
#include <boost/describe/members.hpp>
#include <fmt/core.h>
#include <fmt/compile.h>

#include <shyft/core/reflection.h>
#include <shyft/core/core_archive.h>

namespace shyft::reflection {

  template <typename T>
  using portable_underlying_type_t =
    std::conditional_t<std::signed_integral<std::underlying_type_t<T>>, std::int64_t, std::uint64_t>;

  template <typename T>
  constexpr bool has_portable_underlying_type = (sizeof(T) <= sizeof(portable_underlying_type_t<T>));

  template <reflected_enum T>
  constexpr bool read_blob(std::istream &s, T &t) {
    static_assert(has_portable_underlying_type<T>);
    using U = std::underlying_type_t<T>;
    using V = portable_underlying_type_t<T>;
    V v{};
    s.read((char *) &v, sizeof(V));
    if (s.fail())
      return false;
    t = T{static_cast<U>(v)};
    return true;
  }

  template <reflected_enum T>
  constexpr bool write_blob(std::ostream &s, T const &t) {
    static_assert(has_portable_underlying_type<T>);
    using V = portable_underlying_type_t<T>;
    const V v = etoi(t);
    s.write((char const *) &v, sizeof(V));
    return !s.fail();
  }

  template <typename Archive, reflected_struct T>
  void serialize(Archive &a, T &t, unsigned int const) {

    using bases = boost::describe::describe_bases<T, boost::describe::mod_any_access>;
    using base_indices = boost::mp11::mp_iota<boost::mp11::mp_size<bases>>;
    boost::mp11::mp_for_each<base_indices>([&]<typename base_index>(base_index) {
      using B = typename boost::mp11::mp_at<bases, base_index>::type;
      if constexpr (boost::mp11::mp_size<bases>::value == 1)
        a &core::core_nvp("base", static_cast<B &>(t));
      else {
        static constexpr auto base_name = [] {
          // NOTE:
          //   consider asserting that these identifiers do not collide with
          //   any of the member-names.
          //   - jeh
          auto result = std::array<char, 4 + std::numeric_limits<std::size_t>::max_digits10 + 1>();
          result.fill('\0');
          fmt::format_to(result.data(), FMT_COMPILE("base{}"), base_index::value);
          return result;
        }();
        a &core::core_nvp(base_name.data(), static_cast<B &>(t));
      }
    });

    using members = boost::describe::describe_members<T, boost::describe::mod_any_access>;
    boost::mp11::mp_for_each<members>([&](auto m) {
      a &core::core_nvp(m.name, t.*m.pointer);
    });
  }

#define SHYFT_DEFINE_SERIALIZE(C) \
  template <typename A> \
  friend void serialize(A &a, C &x, const unsigned int v) { \
    ::shyft::reflection::serialize(a, x, v); \
  }

}
