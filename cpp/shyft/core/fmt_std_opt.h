#pragma once

#include <optional>
#include <type_traits>

#include <fmt/core.h>
#include <fmt/std.h>

// NOTE:
//  temporary formatted yanked from: https://github.com/fmtlib/fmt/pull/3303/files
//  remove when bump libfmt to 10. not good to define this here, but stopgap solution
//  and we control most downstream.
//  - jeh
#if FMT_VERSION < 100000
FMT_BEGIN_NAMESPACE

template <typename T, typename Char>
struct formatter<std::optional<T>, Char, std::enable_if_t<is_formattable<T, Char>::value>> {
 private:
  formatter<T, Char> underlying_;
  static constexpr basic_string_view<Char> optional =
    detail::string_literal<Char, 'o', 'p', 't', 'i', 'o', 'n', 'a', 'l', '('>{};
  static constexpr basic_string_view<Char> none = detail::string_literal<Char, 'n', 'o', 'n', 'e'>{};

  template <class U>
  FMT_CONSTEXPR static auto maybe_set_debug_format(U& u, bool set) -> decltype(u.set_debug_format(set)) {
    u.set_debug_format(set);
  }

  template <class U>
  FMT_CONSTEXPR static void maybe_set_debug_format(U&, ...) {
  }

 public:
  template <typename ParseContext>
  FMT_CONSTEXPR auto parse(ParseContext& ctx) {
    maybe_set_debug_format(underlying_, true);
    return underlying_.parse(ctx);
  }

  template <typename FormatContext>
  auto format(std::optional<T> const & opt, FormatContext& ctx) const -> decltype(ctx.out()) {
    if (!opt)
      return detail::write<Char>(ctx.out(), none);

    auto out = ctx.out();
    out = detail::write<Char>(out, optional);
    ctx.advance_to(out);
    out = underlying_.format(*opt, ctx);
    return detail::write(out, ')');
  }
};

FMT_END_NAMESPACE
#endif
