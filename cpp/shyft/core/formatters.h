#pragma once

#include <functional>
#include <type_traits>

#include <fmt/core.h>
#include <fmt/format.h>

#include <shyft/core/utility.h>

namespace shyft {

  template <typename T, typename Char>
  struct ptr_formatter {
    static_assert(fmt::is_formattable<T>::value);
    fmt::formatter<T, Char> underlying_;

    static constexpr fmt::basic_string_view<Char> ptr_ = fmt::detail::string_literal<Char, 'p', 't', 'r', '('>{};
    static constexpr fmt::basic_string_view<Char> nullptr_ =
      fmt::detail::string_literal<Char, 'n', 'u', 'l', 'l', 'p', 't', 'r'>{};

    FMT_CONSTEXPR auto parse(auto& ctx) {
      if constexpr (requires { underlying_.set_debug_format(true); })
        underlying_.set_debug_format(true);
      return underlying_.parse(ctx);
    }

    auto format(auto const & ptr, auto& ctx) const -> decltype(ctx.out()) {
      if (ptr == nullptr)
        return fmt::detail::write<Char>(ctx.out(), nullptr_);

      auto out = ctx.out();
      out = fmt::detail::write<Char>(out, ptr_);
      ctx.advance_to(out);
      out = underlying_.format(*ptr, ctx);
      return fmt::detail::write(out, ')');
    }
  };

  namespace detail {
    template <auto fn>
    struct fn_formatter_result_impl;

    template <auto fn>
    requires is_memfn_v<fn>
    struct fn_formatter_result_impl<fn> {
      using type = memfn_result_t<fn>;
    };

    template <typename T, typename A, T(A::*fn)>
    requires(!is_memfn_v<fn>)
    struct fn_formatter_result_impl<fn> {
      using type = T;
    };

    template <typename T, typename A, T (*fn)(A)>
    struct fn_formatter_result_impl<fn> {
      using type = T;
    };
  }

  template <auto fn>
  using fn_formatter_result_t = typename detail::fn_formatter_result_impl<fn>::type;

  template <auto fn, typename Char>
  struct fn_formatter {
    using T = std::remove_cvref_t<fn_formatter_result_t<fn>>;
    static_assert(fmt::is_formattable<T>::value);
    fmt::formatter<T, Char> underlying_;

    constexpr auto parse(auto& ctx) {
      if constexpr (requires { underlying_.set_debug_format(true); })
        underlying_.set_debug_format(true);
      return underlying_.parse(ctx);
    }

    auto format(auto const & obj, auto& ctx) const {
      return underlying_.format(std::invoke(fn, obj), ctx);
    }
  };

  namespace detail {
    template <typename From, typename To>
    To const & cast_formatter_fn(From const & f) {
      return static_cast< To const &>(f);
    }
  }

  template <typename From, typename To, typename Char>
  using cast_formatter = fn_formatter<&detail::cast_formatter_fn<From, To>, Char>;

#define SHYFT_DEFINE_SHARED_PTR_FORMATTER(C) \
  template <typename Char> \
  struct fmt::formatter<std::shared_ptr<C>, Char> : shyft::ptr_formatter<C, Char> { };


}
