#pragma once
#include <optional>
#include <boost/config.hpp>

#include <boost/serialization/split_free.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>
#include <boost/type_traits/is_pointer.hpp>
#include <boost/serialization/detail/stack_constructor.hpp>
#include <boost/serialization/detail/is_default_constructible.hpp>
#include <boost/serialization/force_include.hpp>

namespace boost::serialization {
  // work around compile-error related to https://github.com/boostorg/serialization/pull/144/files
  // just defines the class, we don't care about this thing.
  struct U { };
} // boost::serialization

namespace boost::serialization {
  // copied from MR that has not yet been merged into boost serialization:
  // https://github.com/boostorg/serialization/pull/163/files
  template <class Archive, class T>
  void save(
    Archive& ar,
    std::optional< T > const & t,
    unsigned int const /*version*/
  ) {
    // It is an inherent limitation to the serialization of optional.hpp
    // that the underlying type must be either a pointer or must have a
    // default constructor.  It's possible that this could change sometime
    // in the future, but for now, one will have to work around it.  This can
    // be done by serialization the optional<T> as optional<T *>
#if !defined(BOOST_NO_CXX11_HDR_TYPE_TRAITS)
    BOOST_STATIC_ASSERT(
      boost::serialization::detail::is_default_constructible<T>::value || boost::is_pointer<T>::value);
#endif
    bool const tflag = t.has_value();
    ar << boost::serialization::make_nvp("has_value", tflag);
    if (tflag) {
      ar << boost::serialization::make_nvp("value", *t);
    }
  }

  template <class Archive, class T>
  void load(
    Archive& ar,
    std::optional< T >& t,
    unsigned int const /*version*/
  ) {
    bool tflag;
    ar >> boost::serialization::make_nvp("has_value", tflag);
    if (!tflag) {
      t.reset();
      return;
    }

    if (!t.has_value())
      t = T();
    ar >> boost::serialization::make_nvp("value", *t);
  }

  template <class Archive, class T>
  void serialize(Archive& ar, std::optional< T >& t, unsigned int const version) {
    boost::serialization::split_free(ar, t, version);
  }

  template <class T>
  struct version<std::optional<T> > {
    BOOST_STATIC_CONSTANT(int, value = 1);
  };
}
