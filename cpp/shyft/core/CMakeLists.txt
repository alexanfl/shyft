# CMake file for compiling the C++ core library
add_library(shyft_core ${shyft_lib_type} ../time/utctime_utilities.cpp
    ../time_series/expression_serialization.cpp
    ../time_series/time_series_serialization.cpp
    ../time_series/dd/clone_expr.cpp
    ../time_series/dd/stringify.cpp
    ../time_series/dd/eval.cpp
    ../time_series/dd/impl.cpp
    ../time_series/dd/resolve_tsv.cpp
    ../dtss/geo.cpp
    ../dtss/ts_subscription.cpp
    ../dtss/master_slave_sync.cpp
    ../dtss/dtss_geo.cpp
    ../dtss/dtss_dispatch.cpp
    ../dtss/dtss_client.cpp
    ../dtss/dtss_db.cpp
    ../dtss/dtss_db_level.cpp
    ../dtss/dtss_db_rocks.cpp
    ../dtss/dtss_krls.cpp
    ../dtss/dtss.cpp
    ../dtss/queue.cpp
    ../dtss/queue_msg.cpp
    ../dtss/container_config.cpp
    ../srv/db_file.cpp
    ../srv/db_level.cpp
    ../srv/db_rocks.cpp
    ../srv/fast_server_iostream.cpp
    ../srv/fast_iosockstream.cpp
    ../mp/parser_utils.cpp
    ../web_api/beast_server.cpp
    ../web_api/beast_server_impl_beast.cpp
    ../web_api/beast_server_impl_bgwork.cpp
    ../web_api/beast_server_impl_detect_session.cpp
    ../web_api/beast_server_impl_http.cpp
    ../web_api/beast_server_impl_https.cpp
    ../web_api/beast_server_impl_listener.cpp
    ../web_api/base_request_handler.cpp
    ../web_api/dtss_web_api.cpp
    ../web_api/dtss_web_api_server.cpp
    ../web_api/dtss_client.cpp
    ../web_api/grammar/time.cpp
    ../web_api/grammar/utcperiod.cpp
    ../web_api/grammar/cf_time.cpp
    ../web_api/grammar/read_ts_request.cpp
    ../web_api/grammar/find_ts_request.cpp
    ../web_api/grammar/remove_ts_request.cpp
    ../web_api/grammar/info_request.cpp
    ../web_api/grammar/ts_points.cpp
    ../web_api/grammar/time_points.cpp
    ../web_api/grammar/ts_values.cpp
    ../web_api/grammar/time_axis.cpp
    ../web_api/grammar/apoint_ts.cpp
    ../web_api/grammar/ats_vector.cpp
    ../web_api/grammar/average_ts_request.cpp
    ../web_api/grammar/percentile_ts_request.cpp
    ../web_api/grammar/store_ts_request.cpp
    ../web_api/grammar/web_request.cpp
    ../web_api/grammar/quoted_string.cpp
    ../web_api/grammar/unsubscribe_request.cpp
    ../web_api/grammar/geo.cpp
    ../web_api/grammar/find_response.cpp
    ../web_api/grammar/ts_info.cpp
    ../web_api/grammar/request_reply.cpp
    ../web_api/grammar/tsv_reply.cpp
    ../web_api/grammar/web_reply.cpp
    ../web_api/grammar/q_grammars.cpp
    ../web_api/grammar/remove_ts_request.cpp
    ../web_api/generators/apoint_ts.cpp
    ../web_api/generators/point.cpp
    ../web_api/generators/ts_info.cpp
    ../web_api/generators/utctime.cpp
    ../web_api/generators/utcperiod.cpp
    ../web_api/generators/time_axis.cpp
    ../web_api/generators/geo_ts_url.cpp
    ../web_api/generators/find_ts.cpp
    ../web_api/generators/read_ts.cpp
    ../web_api/generators/average_ts.cpp
    ../web_api/generators/percentile_ts.cpp
    ../web_api/generators/store_ts.cpp
    ../web_api/generators/q_generators.cpp
    core_serialization.cpp
    ../hydrology/optimizers/dream_optimizer.cpp
    ../hydrology/optimizers/sceua_optimizer.cpp
    ../hydrology/geo_point.cpp
    epsg_fast.cpp
    dlib_utils.cpp
	)
if(MINGW)
    set(msys_extra_libs  ws2_32 wsock32 )
endif()
if(MSVC OR MINGW)
    # only managed to link static on windows
    set(rocksdb_deps RocksDB::rocksdb)
else()
    # a bit peculiar setup on linux, but ok, works
    set(rocksdb_deps RocksDB::rocksdb-shared)
endif()
target_link_libraries(
    shyft_core
    PUBLIC  shyft_public_flags ${boost_link_libraries} OpenSSL::SSL OpenSSL::Crypto dlib::dlib ${arma_libs} fmt::fmt-header-only ${msys_extra_libs}
    PRIVATE shyft_private_flags leveldb::leveldb ${rocksdb_deps}
)
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  target_link_libraries(shyft_core PUBLIC quadmath)
endif()

if (NOT MSVC AND NOT APPLE)
    # With GCC and other non-MSVC link with stdc++fs to get C++17 filesystem.
    # Note that starting with GCC 9 this is not required, as it is included
    # in stdc++, but linking with it will then just have zero effect.
    # On Apple, with Clang, stdc++ is replaced with libc++, and also
    # filesystem is included by default.
    target_link_libraries(shyft_core PUBLIC stdc++fs)
endif()

target_include_directories(
    shyft_core PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/cpp>    # this should be so that shyft is immediately below it
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>  # so that it finds the generated version.h
    $<INSTALL_INTERFACE:${SHYFT_INSTALL_INCLUDEDIR}>
)

set_target_properties(
    shyft_core PROPERTIES
    ARCHIVE_OUTPUT_NAME shyft_core.${SHYFT_VERSION}
    INSTALL_RPATH "$ORIGIN"
    VERSION ${SHYFT_VERSION}
    SOVERSION ${SHYFT_VERSION_MAJOR}.${SHYFT_VERSION_MINOR}.${SHYFT_VERSION_PATCH}
)

install( # this installs to inplace development for python
    TARGETS shyft_core ${shyft_runtime}
    LIBRARY DESTINATION ${SHYFT_PYTHON_DIR}/shyft/lib NAMELINK_SKIP # installing .so on linux
    ARCHIVE DESTINATION $<TARGET_FILE_DIR:shyft_core> # workaround to avoid win import lib being installed to main install prefix
    RUNTIME DESTINATION ${SHYFT_PYTHON_DIR}/shyft/lib # installing .dll on win
)

configure_file(  # this generates the version file based on the CMAKE variables that determines the current version
    ${PROJECT_SOURCE_DIR}/cmake/templates/version.h.in
    ${CMAKE_CURRENT_BINARY_DIR}/shyft/version.h
)
configure_file(${PROJECT_SOURCE_DIR}/cmake/templates/config.h.in
  ${CMAKE_CURRENT_BINARY_DIR}/shyft/config.h)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/shyft/version.h ${CMAKE_CURRENT_BINARY_DIR}/shyft/config.h
    COMPONENT development
    EXCLUDE_FROM_ALL
    DESTINATION ${SHYFT_INSTALL_INCLUDEDIR}/shyft
)

install(TARGETS shyft_core
    COMPONENT development
    EXPORT shyft-targets
    EXCLUDE_FROM_ALL
    LIBRARY DESTINATION ${SHYFT_INSTALL_LIBDIR} NAMELINK_SKIP
    ARCHIVE DESTINATION ${SHYFT_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${SHYFT_INSTALL_BINDIR}
)

install(DIRECTORY ${CMAKE_SOURCE_DIR}/cpp/shyft
    COMPONENT development
    EXCLUDE_FROM_ALL
    DESTINATION ${SHYFT_INSTALL_INCLUDEDIR}
    FILES_MATCHING PATTERN "*.h"
)
