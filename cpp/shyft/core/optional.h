/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#pragma once

#include <optional>

#include <shyft/core/utility.h>

namespace shyft {
  template <class T>
  inline constexpr auto none = std::optional<T>{};

  constexpr auto just(auto&& v) {
    return std::optional(SHYFT_FWD(v));
  }

  template <class T>
  constexpr auto just(auto&&... args) {
    return std::make_optional<T>(SHYFT_FWD(args)...);
  }
}