#pragma once
#include <vector>
#include <boost/math/special_functions/relative_difference.hpp>

namespace shyft::core {
  using boost::math::epsilon_difference;
  using std::vector;
  constexpr double max_epsilon = 2.0; // double less than 2 eps are equal

  /** practically equal to max_epsilon relative difference, including both nans */
  inline bool nan_equal(double a, double b) noexcept {
    if (!std::isfinite(a) && !std::isfinite(b))
      return true;
    if (std::isfinite(a) && std::isfinite(b))
      return epsilon_difference(a, b) < max_epsilon;
    return false;
  }

  inline bool nan_equal(vector<double> const & a, vector<double> const & b) noexcept {
    if (a.size() != b.size())
      return false;
    for (size_t i = 0; i < a.size(); ++i) {
      if (!nan_equal(a[i], b[i]))
        return false;
    }
    return true;
  }
}
