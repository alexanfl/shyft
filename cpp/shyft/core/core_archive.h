/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

namespace shyft::core {

  constexpr auto core_arch_flags = boost::archive::archive_flags::no_header;
  using core_iarchive = boost::archive::binary_iarchive;
  using core_oarchive = boost::archive::binary_oarchive;

  // note that no_header is required when doing compatible-enough
  // binary archives between the ms c++ compiler and gcc
  // The difference is that ms c++ have 32bits ints (!)
  //

  // core_nvp is a stand-in for make_nvp (could resolve to one)
  // that ensures that the serialization using core_nvp
  // can enforce use of properly sized integers enough to
  // provide serialization compatibility between ms c++ and gcc.
  // ref. different int-sizes, the strategy is to
  // forbid use of int which is different.

  template <class T>
  inline T&& core_nvp(char const *, T&& t) {
    static_assert(
      !std::is_same_v<std::int32_t, T>, "serialization require non-32bits ints(to ensure win/linux compat)");
    return t;
  }

#define x_arch(T) x_serialize_archive(T, core_oarchive, core_iarchive)

/** Use this macro in implementation files to register classes and instantiate templates.
 */
#define x_serialize_instantiate_and_register(C) \
  x_serialize_implement(C); \
  x_arch(C);

}
