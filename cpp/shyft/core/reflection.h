#pragma once

#include <concepts>
#include <type_traits>
#include <functional>
#include <string_view>

#include <boost/config.hpp>
#include <boost/describe/bases.hpp>
#include <boost/describe/class.hpp>
#include <boost/describe/enum.hpp>
#include <boost/describe/members.hpp>
#include <boost/describe/enumerators.hpp>
#include <boost/hof/detail/forward.hpp>
#include <boost/mp11.hpp>
#include <boost/hana/detail/struct_macros.hpp>
#include <boost/hana/find_if.hpp>
#include <boost/hana/pair.hpp>
#include <boost/preprocessor/punctuation/remove_parens.hpp>

#include <shyft/core/utility.h>

namespace shyft {

#define SHYFT_DEFINE_ENUM(name, underlying_type, enumerators) \
  BOOST_DEFINE_FIXED_ENUM_CLASS(name, underlying_type, BOOST_PP_REMOVE_PARENS(enumerators))

#define SHYFT_DEFINE_STRUCT(name, bases, members) \
  static consteval std::string_view _shyft_typename() { \
    return #name; \
  } \
  BOOST_DESCRIBE_CLASS(name, bases, members, (), ())

  template <typename T>
  concept reflected_enum = boost::describe::has_describe_enumerators<T>::value;

  template <reflected_enum T>
  inline constexpr auto enumerator_count = boost::mp11::mp_size<boost::describe::describe_enumerators<T>>::value;

  template <typename T>
  concept reflected_struct = boost::describe::has_describe_bases<T>::value
                          && boost::describe::has_describe_members<T>::value && !std::is_union_v<T>;

  template <typename T>
  constexpr auto reflected_typename = std::remove_cvref_t<T>::_shyft_typename();

  template <typename T>
  requires(std::is_enum_v<T>)
  constexpr auto etoi(T t) {
    return static_cast<std::underlying_type_t<T>>(t);
  }

  template <reflected_enum T>
  constexpr auto with_enum(T t, auto &&f) {
    return boost::mp11::mp_with_index<enumerator_count<T>>(etoi(t), [g = SHYFT_FWD(f)](auto t) {
      return std::invoke(g, std::integral_constant<T, T{t()}>{});
    });
  }

  template <reflected_enum T>
  constexpr auto with_enum_or(T t, auto &&f, auto &&g) {
    static_assert(std::unsigned_integral<std::underlying_type_t<T>>);
    constexpr auto N = enumerator_count<T>;
    if (etoi(t) > N)
      return std::invoke(g);
    return with_enum(t, SHYFT_FWD(f));
  }

  template <auto member_ptr>
  inline constexpr auto hana_member_name = [] {
    return boost::hana::first(
      boost::hana::find_if(boost::hana::accessors<object_type_t<member_ptr>>(), [](auto accessor) {
        return std::is_same<
          std::remove_cvref_t<decltype(boost::hana::second(accessor))>,
          boost::hana::struct_detail::member_ptr<decltype(member_ptr), member_ptr>>{};
      }).value());
  }();

  namespace detail {
    template <typename Tag, template <Tag> class Value>
    struct tagged_type_impl {
      template <typename Enum>
      using fn = Value<Enum::value>;
    };
  }
  template <typename Tag, template <Tag> class Value>
  using tagged_types_t = boost::mp11::
    mp_transform< detail::tagged_type_impl<Tag, Value>::template fn, boost::describe::describe_enumerators<Tag>>;

  namespace detail {
    template <typename T>
    struct unwrap_member_impl {
      using type = typename decltype([] {
        if constexpr (is_std_vector<T>) {
          return std::type_identity<typename unwrap_member_impl<typename T::value_type>::type>{};
        } else if constexpr (is_std_shared_ptr<T>) {
          return std::type_identity<typename unwrap_member_impl<typename T::element_type>::type>{};
        } else {
          return std::type_identity<T>{};
        }
      }())::type;
    };
  }

  template <typename T>
  using unwrap_member = detail::unwrap_member_impl<T>::type;

  template <typename T>
  using is_reflected = boost::mp11::mp_bool<reflected_struct<unwrap_member<member_type_t<T::pointer>>>>;

  template <typename T, unsigned filter = boost::describe::mod_any_access | boost::describe::mod_inherited>
  using members_of = boost::mp11::mp_rename<boost::describe::describe_members<T, filter>, std::tuple>;

#ifndef _MSC_VER
  // ms c++ does not yet support auto cast
  // so we have to wait(only used in STM currently not compiling due to this and other stuff)
  template <typename T>
  using member_pointer_of = constant_t<auto(T::pointer)>;

  template <typename T>
  using bases_of =
    boost::mp11::mp_rename<boost::describe::describe_bases<T, boost::describe::mod_any_access>, std::tuple>;

  template <typename T>
  using paths_of = boost::mp11::mp_rename<
    boost::mp11::mp_transform_q< boost::mp11::mp_compose< member_pointer_of, std::tuple>, members_of<T>>,
    std::tuple>;

#endif
}
