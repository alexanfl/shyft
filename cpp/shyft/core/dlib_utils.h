/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <atomic>
#include <shyft/srv/fast_iosockstream.h>
#include <dlib/logger.h>
#include <thread>

namespace shyft::core {
  using std::vector;
  using std::string;
  using std::string_view;
  using std::to_string;
  using std::runtime_error;
  using std::exception;
  using std::chrono::seconds;
  using std::chrono::milliseconds;
  using std::unique_ptr;
  using std::shared_ptr;
  using std::make_unique;
  using std::max;
  using std::this_thread::sleep_for;

  /** @brief resilient server connection
   *
   *  Helper class to connect/reconnect dlib::iosockstream connections
   *  that could break if server is rebootet, network is temporary out.
   *
   */
  struct srv_connection {
    using iosockstream_t = shyft::srv::fast_iosockstream;
    string host_port;              ///< like ip:port, or name:port
    int timeout_ms{1000};          ///< timout for operations, def.1s
    unique_ptr<iosockstream_t> io; ///< the io stream
    bool is_open{false};           ///< current observed/concluded state of the srv_connection
    size_t reconnect_count{0};     ///< reconnects resolved creating new connection to host

    srv_connection(string const & host_port, int timeout_ms = 1000)
      : host_port{host_port}
      , timeout_ms{timeout_ms}
      , io{make_unique<iosockstream_t>()} {
    }

    void open(int timeout_ms = 1000) {
      io->open(host_port, max(timeout_ms, this->timeout_ms));
      is_open = true;
    }

    void close(int timeout_ms = 1000) {
      is_open = false; // if next line throws, consider closed anyway
      io->close(max(timeout_ms, this->timeout_ms));
    }

    /** reopen after failure, count it, so master sync can resubscribe */
    void reopen(int timeout_ms = 1000) {
      ++reconnect_count;
      open(timeout_ms);
    }

    // allow move, so that we can keep them in containers(move on resize)
    srv_connection(srv_connection&&) = default;
    srv_connection& operator=(srv_connection&&) = default;

    // get rid of stuff we do not support (yet)
    srv_connection() = delete;
    srv_connection(srv_connection const &) = delete;
    srv_connection& operator=(srv_connection&) = delete;
  };

  /**@brief helper-class to enable 'autoconnect', lazy connect.
   *
   * Do some extra effort to establish connection, in case it's temporary down'
   *
   */
  struct scoped_connect {
    srv_connection& sc;

    scoped_connect(srv_connection& srv_con)
      : sc(srv_con) {
      if (!sc.is_open) { // auto-connect, and put some effort into succeeding
        bool rethrow = false;
        runtime_error rt_re("");
        bool attempt_more = false;
        int retry_count = 5;
        do {
          try {
            sc.open(); // either we succeed and finishes, or we get an exception
            attempt_more = false;
          } catch (dlib::socket_error const &
                     se) { // capture socket error that might go away if we stay alive a little bit more
            if (--retry_count > 0 && strstr(se.what(), "unable to connect")) {
              attempt_more = true;
              sleep_for(milliseconds(100));
            } else {
              rt_re = runtime_error(se.what());
              rethrow = true;
              attempt_more = false;
            }
          } catch (exception const & re) { // just give up this type of error,
            rt_re = runtime_error(re.what());
            rethrow = true;
            attempt_more = false;
          }
        } while (attempt_more);
        if (rethrow)
          throw rt_re;
      }
    }

    ~scoped_connect() noexcept {
      // we don't disconnect, rather we try to fix broken connects.
    }

    scoped_connect(scoped_connect const &) = delete;
    scoped_connect(scoped_connect&&) = delete;
    scoped_connect& operator=(scoped_connect const &) = delete;
    scoped_connect() = delete;
    scoped_connect& operator=(scoped_connect&&) = delete;
  };

  /** @brief utility to retry an io-operation that fails
   *
   * The socket connection created from the client stays connected from the first use until explicitely
   * closed by client.
   * The life time of the underlying socket *after* the close is ~ 120 seconds(windows, similar linux).
   * If the server is restarted we would enjoy that this layer 'auto-repair' with the server
   * if possible.
   * This routine ensures that this can happen for any io function sequence F.
   *
   * @note that there is a requirement that f(sc) can be invoced several times
   * without (unwanted) side-effects.
   * @tparam F a callable that accepts a srv_connection
   *
   * @param sc srv_connection that's repairable
   * @param f the callable'
   */
  template <class F>
  void do_io_with_repair_and_retry(srv_connection& sc, F&& f) {
    for (int retry = 0; retry < 3; ++retry) {
      try {
        f(sc);
        return;
      } catch (dlib::socket_error const &) {
        sc.reopen();
      }
    }
    throw runtime_error("Failed to establish connection with " + sc.host_port);
  }

  constexpr const size_t max_msg_str_size = 10000000; // 10MB for strings and exceptions, surely enough

  // msg provide commonly used socket message level functions
  // like read one-byte message-type(enum) from stream,
  // read/write exception content to stream
  //
  template <typename message_type>
  struct msg_util {
    using type_ = typename message_type::type;

    template <class T>
    static type_ read_type(T& in) {
      int32_t mtype;
      in.read((char*) &mtype, sizeof(mtype));
      if (!in)
        throw dlib::socket_error(string("failed to read message type"));
      return (type_) mtype;
    }

    template <class T>
    static void write_type(type_ mt, T& out) {
      int32_t mtype = (int32_t) mt;
      out.write((char const *) &mtype, sizeof(mtype));
      if (!out.good())
        throw dlib::socket_error(string("failed writing message type"));
    }

    template <class T>
    static void write_string(std::string const & s, T& out) {
      int32_t sz = std::min(s.size(), max_msg_str_size);
      out.write((char const *) &sz, sizeof(sz));
      out.write(s.data(), sz);
      if (!out.good())
        throw dlib::socket_error(string("failed writing string"));
    }

    template <class T>
    static std::string read_string(T& in) {
      std::int32_t sz;
      in.read((char*) &sz, sizeof(sz));
      if (!in || (sz < 0 || static_cast<size_t>(sz) > max_msg_str_size))
        throw dlib::socket_error(string("failed reading size of string"));
      std::string msg(sz, '\0');
      in.read((char*) msg.data(), sz);
      if (!in)
        throw dlib::socket_error(string("failed reading string"));
      return msg;
    }

    template <class T>
    static void write_exception(std::exception const & e, T& out) {
      int32_t sz = strnlen(e.what(), max_msg_str_size);
      out.write((char const *) &sz, sizeof(sz));
      out.write(e.what(), sz);
    }

    template <class T>
    static void send_exception(std::exception const & e, T& out) {
      write_type(message_type::SERVER_EXCEPTION, out);
      int32_t sz = strnlen(e.what(), max_msg_str_size);
      out.write((char const *) &sz, sizeof(sz));
      out.write(e.what(), sz);
    }

    template <class T>
    static std::runtime_error read_exception(T& in) {
      int32_t sz;
      in.read((char*) &sz, sizeof(sz));
      if (!in || (sz < 0 || static_cast<size_t>(sz) > max_msg_str_size))
        throw dlib::socket_error(string("failed reading exception size"));
      std::string msg(sz, '\0');
      in.read((char*) msg.data(), sz);
      if (!in)
        throw dlib::socket_error(string("failed reading exception data"));
      return std::runtime_error(msg);
    }
  };

  /** simple helper class to safely count live-connections, ref. on_connect */
  struct scoped_count {
    std::atomic_size_t& c; ///< a ref to the atomic size we use for counting

    scoped_count(std::atomic_size_t& c)
      : c{c} {
      ++c;
    } // add count at construct

    ~scoped_count() {
      --c;
    } // decrement when leaving scope

    //-- ensure safe use
    scoped_count() = delete;
    scoped_count(scoped_count const &) = delete;
    scoped_count(scoped_count&&) = delete;
    scoped_count& operator=(scoped_count const &) = delete;
    scoped_count& operator=(scoped_count&&) = delete;
  };

  /** simple helper class to set dlib global logging properties */
  struct logging {
    /**
     * @brief ties all log output to fname
     * @details
     * if fname "" then cout is used,
     * current file is flushed/closed prior to opening a new file.
     *
     * @param fname name of the file, make sure there are room, and make sure its on separate partitions for
     * critical/sensitive systems
     */
    static void output_file(std::string const & fname);
    /**
     * @brief set global log level
     * @details
     * Set the global log level, for all the loggers.
     * @param ll log-level as pr. dlib
     *   -100= TRACE
     *    0  = DEBUG
     *  100  = INFO
     *  200  = WARN
     *  300  = ERR
     *  400  = FATAL
     */
    static void level(const dlib::log_level ll);
  };

}
