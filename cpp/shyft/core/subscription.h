/** This file is part of Shyft. Copyright 2015-2019 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <map>
#include <future>
#include <atomic>
#include <mutex>
#include <array>
#include <functional>

namespace shyft::core::subscription {
  using std::vector;
  using std::make_shared;
  using std::string;
  using std::shared_ptr;
  // using std::string_view;
  using std::to_string;
  using std::runtime_error;
  using std::end;
  using std::unordered_map;
  using std::array;
  using std::recursive_mutex;

  using std::function;
  using std::scoped_lock;
  using std::begin;
  using std::end;
  using std::atomic_int64_t;
  using std::atomic_int_fast64_t;

  /** @brief an observable item (like ts)
   *
   * We track the terminal-nodes of expression(s) of  ts using their unique id, a string.
   *
   * For general observable items of other types, they are always terminal nodes, and the same logic applies.
   *
   * Once a ts becomes a part of an expression that needs to be observed,
   * we create a ts_observable object, with the intial 'version' of 0.
   * The same applies to any observable.
   *
   * For each invocation of changed(), we increase this version by one.
   *
   * The version v is atomic, thread-safe.
   * The id-string, is const, so we can use it efficiently
   * in unordered_map as key.
   *
   * @note consider change the id member to a template type with minimal requirements(hashable/movable)
   */
  struct observable {
    // delete copy-ct and assign
    observable() = delete;
    observable(observable const &) = delete;
    observable& operator=(observable&) = delete;

    // implement moveable
    observable(observable&& o)
      : id{std::move(o.id)}
      , v{o.v.load()} {
    }

    ~observable() = default;

    template <class S>
    explicit observable(S&& ids)
      : id{std::forward<S>(ids)} {
    }

    // --  members
    string const id; ///< promise to keep id invariant in the lifetime of object, ref use in unordered_map, later where
                     ///< we use pointer to string
    atomic_int_fast64_t v{0}; ///< version number, born 0, and strictly increasing

    /** atomic increment version-number to indicate change*/
    void changed() noexcept {
      ++v;
    }
  };

  using observable_ = shared_ptr<observable>;

  namespace detail {
    // - so we want to avoid one copy of string, using string const* for
    // the ts-id.
    // provide the hash functions here:
    // consider: reworking core find_ts_bind_info, providing only ref. to terminals
    //           .. avoid the string copy entirely ..
    // some other issues: removal of time-series, should that be allowed(its optional now), how to notify?
    //
    struct str_ptr_hash {
      inline std::size_t operator()(string const * s) const noexcept {
        return std::hash<string>{}(*s);
      }
    };

    struct str_ptr_eq {
      inline bool operator()(string const * lhs, string const * rhs) const {
        return *lhs == *rhs;
      }
    };
  }

  /** @brief subscription manager.
   *
   * This class has the responsiblity of keeping subscriptions of items,
   * and to notify the observers when the observable has changed.
   *
   * client                             server
   *
   *      sock                           dtss
   *         subscr_ctx                  subscription
   *            .template                  umap<string,count>.. update count on change..
   *            .terminals
   *            .server_ref(shared_ptr<>..,mycount>   diff mycouunt server-count -> is a change
   *            .umap_updated_count        * want subscriptions to be ref-counted (goes away when clients dead)
   *
   *      thread.:
   *          if server.umap_updated..
   *   Q: use boost.signal2 library to handle this ?
   *   A: for now, it seems better with a tailored approach (due to termination/cost of server-side to atomic increment
   * of sharing, nothing more.)
   */
  struct manager {

    mutable recursive_mutex mx; ///< protect insert/removal of the active subscribers
    unordered_map<string const *, observable_, detail::str_ptr_hash, detail::str_ptr_eq>
      active; ///< active subscriptions,  by key(to be optimized to key*) ts_sub scription shared ptrs.

    bool is_active() const noexcept {
      scoped_lock<decltype(mx)> sl(mx);
      return active.size() > 0;
    }

    // NOTE: the observable_ shared_ptr  will have ONE extra ref. due to our approach here. (alt: pointer + dt, or
    // weak-ptr, both also need some care)
    //-- we want to keep track of 'master' change as well:
    atomic_int_fast64_t total_change_count{0};
    atomic_int_fast64_t total_unsubscribe_count{
      0}; ///< to provide the master-slave sync worker with hint about un-sub events

    function<void(void)> master_changed_cb = []() {
    };                   ///< invoced after a notify change that some observed ts was changed
    manager() = default; // ok

    template <class Fx>
    explicit manager(Fx&& fx)
      : master_changed_cb{fx} {
    }

    ~manager() = default;
    //-- get rid of fx we do not support
    manager(manager const &) = delete;
    manager(manager&&) = delete;
    manager& operator=(manager const &) = delete;
    manager& operator=(manager&&) = delete;

    /** called by the dtss during write operation
     *
     * We want this to be as fast as possible, with minimal interlocks.
     * We take the approach to lock the active subscription list while working with it
     * .. then lookup each ts-id (hash table lookup, approx constant lin time)
     * .. and atomic increment each matching ts version number (>0).
     *
     * The observers, share access to this data structure, and can observe the
     * version number, (or the sum of many) and thus determine that the
     * time-series they monitor(or derived expression) has changed the value.
     *
     */
    template <class T>
    void notify_change(T id_first, T id_last) {
      scoped_lock<decltype(mx)> sl(mx);
      bool a_change{false};
      for (T i = id_first; i != id_last; ++i) {
        auto s = active.find(&(*i));
        if (s != active.end()) {
          (*s).second->changed();
          a_change = true;
        }
      }
      if (a_change) {
        ++total_change_count;
        master_changed_cb();
      }
    }

    /** utility, @see notify_change above*/
    void notify_change(string const & id) {
      array<string, 1> one{id};
      notify_change(begin(one), end(one));
    }

    void notify_change(vector<string> const & c) {
      notify_change(c.cbegin(), c.cend());
    }

    /** @brief add a list of subscriptions
     *
     *  To be called by observers/clients to activate change subscription of the named time-series.
     *
     * If the ts-id is already subscribed on, (dependency of several expressions), we just
     * return shared_ptr to the same ts_sub type.
     * If the ts-id is new, a ts_sub item is created, added, and added the return list.
     *
     * The observer/client share of the complete set of subscribed time-series is automatically
     * maintained by the shared_ptr returned in the list. When ref-count goes to zero, it unsubscribes it self.
     *
     * @note the above approach could be ineffective when subscribed to 10k time-series (single piece removal etc.)
     *
     *
     * @return a vector of shared_ptr to ts_sub, so we keep the subs alive at the client, in the same order as the list.
     */
    template <class T>
    vector<observable_> add_subscriptions(T id_first, T id_last) {
      scoped_lock<decltype(mx)> sl(mx);
      auto n = distance(id_first, id_last);
      vector<observable_> r;
      r.reserve(n);
      for (T i = id_first; i != id_last; ++i) {
        auto s = active.find(&(*i));
        if (s == active.end()) {
          auto sub = make_shared<observable>(*i);
          r.push_back(sub);
          active[&sub->id] = sub;
        } else {
          r.push_back(s->second);
        }
      }
      return r;
    }

    vector<observable_> add_subscription(string const & id) {
      array<string, 1> one{id};
      return add_subscriptions(begin(one), end(one));
    }

    vector<observable_> add_subscriptions(vector<string> const & c) {
      return add_subscriptions(begin(c), end(c));
    }

    /** @brief remove subscription vector
     *
     * If ref.use_count()==2 , it means two references, one is by the member in the vector, the second is from the
     * active map we have so if use_count() === 2, we remove it from the active-list, because it's about to be removed
     * (last observer)
     *
     * @param c a vector of shared_ptrs to subs that was previousely return by add-subscriptions
     */
    void remove_subscriptions(vector<observable_>& c) {
      scoped_lock<decltype(mx)> sl(mx);
      size_t unsubscribed_count{0};
      for (auto const & sub : c) {
        auto s = active.find(&sub->id);
        if (
          s != active.end()
          && s->second.use_count()
               == 2) { // a bit clumsy, but as long as we keep a shared_ptr to it ,refcount 2 means vector plus us refs,
          active.erase(s); // unless we did the above, the ts_sub destructor *might be called *, and thus recursive into
                           // this routine.
          ++unsubscribed_count;
        }
      }
      total_unsubscribe_count += unsubscribed_count;
    }
  };

  using manager_ = shared_ptr<manager>;

  /** basic observer class
   *
   * Given :
   *  1. a subscription_manager (provided by a server. e.g. dtss or dstm)
   *  2. an expression (provided by the end -user/web_api)
   *
   * Then:
   *  1. create one or more subscriptions based on the provided expression
   *  2. keep and maintain the published version number
   *  3. When changes are detected (separate thread, or event), then recompute expression and update the published
   * version.
   *
   */
  struct observer_base {
    //--- inputs, as passed on by the constructor
    manager_ sm;                   ///< ref to manager that provides the subscription. Notification needed.
    string request_id;             ///< client-side request-id that for the client identifies the subscription.
    vector<observable_> terminals; ///< Entities that we are observing
    //--- required internal stuff.
    int64_t published_version{-1};

    // Constructors:
    observer_base(manager_ const & sm, string const & request_id)
      : sm{sm}
      , request_id{request_id} {
    }

    virtual ~observer_base() {
      if (sm)
        sm->remove_subscriptions(terminals);
      terminals.clear();
    }

    /** @brief the version number of the terminals
     *
     * We use this to keep track of any change of the terminals that
     * defines the values of this expression.
     *
     * Is defined as the sum of all terminal version values.
     * Since each of them are >0, and strictly increasing, the sum
     * also share this property
     * @return terminal version as sum of observed version values
     */
    virtual int64_t terminal_version() const noexcept {
      int64_t r{0};
      for (auto const & t : terminals)
        r += t->v; // sum all values, each is >0 and strictly increasing -> sum share same property
      return r;
    }

    /** @brief check whether the observed value is not the same as the published version
     */
    bool has_changed() const noexcept {
      return terminal_version() != published_version;
    };

    /** @brief recalculate the value of the observed
     *
     * Here given as a pure virtual function, and needs to
     * be overwritten by specializing observers.
     *
     * The function should return true if the observed value has changed
     */
    virtual bool recalculate() = 0;
  };

  using observer_base_ = shared_ptr<observer_base>;
}
