/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#pragma once

#include <list>
#include <string>
#include <utility>
#include <stdexcept>
#include <cassert>
#include <type_traits>

namespace shyft::core {
  using std::list;
  using std::pair;
  using std::string;
  using std::runtime_error;
  using std::size_t;
  using std::to_string;

  /** need to have key_as_string */
  inline string key_as_string(string const &s) {
    return s;
  }

  template <class K, typename std::enable_if<std::is_integral< K>::value, int>::type = 0>
  inline string key_as_string(K const &r) {
    return to_string(r);
  }

  /** @brief lru-cache
   *
   *  Based on https://timday.bitbucket.io/lru.html#x1-8007r1 and other that
   *  have done similar minimal approaches.
   *
   *  There are even more advanced open-source caches available, with a lot of features
   *  that are interesting, but currently we keep a minimalistic approach and
   *  with features that is closely related to our needs.
   *
   *  In the dtss context we use it as *tool* for the thread-safe and more specific
   *  caching, including sparse ts cache, possibly update with merge.
   *
   */
  template < typename K, typename V, template <typename...> class MAP >
  struct lru_cache {
    using key_type = K;
    using value_type = V;
    using key_tracker_type = list<key_type>; ///< Key access history, most recent at back

    /** Key to value and key history iterator */
    using key_to_value_type = MAP< key_type, pair<value_type, typename key_tracker_type::iterator> >;
    /** Optionally report which value is removed from cache when evicted to a callable*/
    using report_eviction = std::function<void(value_type const &)>;

    /** Constructor specifies the cached function and
     *  the maximum number of records to be stored
     *  @param c maximum id-count in the cache, required to be > 0
     *
     */
    lru_cache(size_t c, report_eviction re = nullptr)
      : _capacity(c)
      , _report_eviction(re) {
      assert(_capacity != 0);
    }

    /** check if a item with key k exists in the cache
     * @param k key to check for
     * @return true if item with key k exists
     */
    bool item_exists(key_type const &k) const {
      return _key_to_value.find(k) != _key_to_value.end();
    }

    /**@return a reference to item with key k, or throws */
    value_type &get_item(key_type const &k) {
      auto const it = _key_to_value.find(k);
      if (it == _key_to_value.end()) { // We don't have it:
        throw runtime_error(string("attempt to get non-existing key:") + key_as_string(k));
      } else { // We do have it, rotate the element into front of list
        _key_tracker.splice(_key_tracker.end(), _key_tracker, (*it).second.second);
        return (*it).second.first; // Return the retrieved value
      }
    }

    /**  try to get a value of the cached function for k */
    bool try_get_item(key_type const &k, value_type &r) {
      if (item_exists(k)) {
        r = get_item(k);
        return true;
      }
      return false;
    }

    /** add an item(that does not exists) into cache, or if exist, update its value */
    void add_item(key_type const &k, value_type const &v) {
      auto it = _key_to_value.find(k);
      if (it == _key_to_value.end()) {
        insert(k, v);
      } else {
        (*it).second.first = v;
        _key_tracker.splice(_key_tracker.end(), _key_tracker, (*it).second.second); // rotate into 1st position
      }
    }

    /** remove and item that *might* exist */
    void remove_item(key_type const &k) {
      auto const it = _key_to_value.find(k);
      if (it != _key_to_value.end()) { // We do have it:
        if (_report_eviction)          // Report evicted value to callable if existing
          _report_eviction(it->second.first);
        _key_tracker.erase(it->second.second);
        _key_to_value.erase(it);
      }
    }

    /** Obtain the cached keys, most recently used element
     *  at head, least recently used at tail.
     *  This method is provided purely to support testing. */
    template <typename IT>
    void get_mru_keys(IT dst) const {
      auto src = _key_tracker.rbegin();
      while (src != _key_tracker.rend())
        *dst++ = *src++;
    }

    /** scan items calling fx(key,vale) for each */
    template <typename Fx>
    void apply_to_items(Fx &&fx) const {
      for (auto const &kv : _key_to_value) {
        fx(kv.first, kv.second.first);
      }
    }

    /**adjust capacity, evict excessive items as needed */
    void set_capacity(size_t cap) {
      if (cap == 0)
        throw runtime_error("cache capacity must be >0");
      if (cap < _capacity) {
        while (_key_to_value.size() > cap)
          evict();
      }
      _capacity = cap;
    }

    size_t get_capacity() const {
      return _capacity;
    }

    size_t size() const {
      return _key_to_value.size();
    }

    /** Flush cache */
    void flush() {
      _key_tracker.clear();
      _key_to_value.clear();
    }
   private:
    /** Record a fresh key-value pair in the cache */
    void insert(key_type const &k, value_type const &v) {
      assert(_key_to_value.find(k) == _key_to_value.end()); // Method is only called on cache misses
      if (_key_to_value.size() >= _capacity)                // Make space if necessary
        evict();

      // Record k as most-recently-used key
      auto it = _key_tracker.insert(_key_tracker.end(), k);
      // Create the key-value entry, linked to the usage record.
      _key_to_value.insert(std::make_pair(k, std::make_pair(v, it)));
    }

    /** Purge the least-recently-used element in the cache */
    void evict() {
      assert(!_key_tracker.empty()); // Assert method is never called when cache is empty
                                     // Identify least recently used key
      auto const it = _key_to_value.find(_key_tracker.front());
      assert(it != _key_to_value.end());
      if (_report_eviction) // Report evicted value to callable if existing
        _report_eviction(it->second.first);
      // Erase both elements to completely purge record
      _key_to_value.erase(it);
      _key_tracker.pop_front();
    }

    size_t _capacity;                ///< Maximum number of key-value pairs to be retained
    key_tracker_type _key_tracker;   ///< Key access history
    key_to_value_type _key_to_value; ///< Key-to-value lookup
    report_eviction _report_eviction;
  };
}
