#include <fstream>
#include <iostream>
#include <shyft/core/dlib_utils.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::core {

  namespace {
    using namespace std;

    /**
     * @brief Configuration class for Dlib logger:
     */
    struct server_log_hook {
      ofstream fout;                              ///< if configured, as in open, make logs flow into this
      string log_file;                            ///< name of file to log to
      dlib::log_level current_level{dlib::LWARN}; ///< only log to file for these levels

      void log(
        string const & logger_name,
        dlib::log_level const & ll,
        const dlib::uint64 thread_id,
        char const * message_to_log);
      static void set_file(string const & fname);
      static void set_level(const dlib::log_level ll);
    };

    void server_log_hook::log(
      string const & logger_name,
      dlib::log_level const & ll,
      const dlib::uint64 thread_id,
      char const * message_to_log) {
      static calendar utc{}; // make it once, keep it
      if (ll >= current_level) {
        ostream* log_output = fout.is_open() ? &fout : &std::cout;
        (*log_output) << utc.to_string(utctime_now()) << " " << ll << " [" << thread_id << "] " << logger_name << ": "
                      << message_to_log << std::endl;
      }
    }

    static server_log_hook l; // one hook, and dlib ensures thread safety before calling log

    void server_log_hook::set_file(std::string const & fname) {
      if (l.fout.is_open()) {
        l.fout.flush();
        l.fout.close();
      } // close previous stream
      // prepare the global static logger,
      l.log_file = fname;
      if (fname.size()) // set blank to log to stdout (default)
        l.fout.open(l.log_file);
      // connect to dlib
      dlib::set_all_logging_output_hooks(l);
    }

    void server_log_hook::set_level(const dlib::log_level ll) {
      l.current_level = ll;
      dlib::set_all_logging_levels(ll);
    }
  }

  void logging::output_file(std::string const & fname) {
    server_log_hook::set_file(fname);
  }

  void logging::level(const dlib::log_level ll) {
    server_log_hook::set_level(ll);
  }
}
