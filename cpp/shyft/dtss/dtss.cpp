#include <fmt/core.h>

#include <shyft/dtss/dtss.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/dtss/master_slave_sync.h>

#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/resolve_tsv.h>
#include <shyft/core/fs_compat.h>

namespace shyft::dtss {

  using shyft::time_series::dd::ts_as;
  using shyft::time_series::dd::ats_vector;
  using shyft::core::utcperiod_hasher;

  /**
   * @brief reconstruct and add correct idb at specified location
   *
   */
  void server::reconstruct_db_at(std::string const &c_name, std::string const &c_pth, db_cfg const &default_cfg) {
    if (ts_db_level::exists_at(c_pth))
      container[c_name] = std::make_unique<ts_db_level>(c_pth, default_cfg);
    else if (ts_db_rocks::exists_at(c_pth))
      container[c_name] = std::make_unique<ts_db_rocks>(c_pth, default_cfg);
    else
      container[c_name] = std::make_unique<ts_db>(c_pth);
    // ensure to pick up geo.cfg at location:
    auto found_geo = geo_ts_db_scan(c_pth);
    if (found_geo)
      geo[c_name] = *found_geo;
  }

  void server::set_container_cfg(std::string const &cfg_path) {
    if (cfg_path.empty())
      return; // null op.
    unique_lock<mutex> sl(c_mx);
    if (!fs::exists(cfg_path)) {
      // create cfg, update with current map.
      // and store it.(if non zero)
      fs::path p{cfg_path};
      auto p_dir = p.parent_path();
      if (!fs::exists(p_dir)) {
        fs::create_directories(p_dir);
      }
      container_config cc;
      for (auto const &[c_name, db] : container) {
        cc.container_cfg[c_name] = db->root_dir();
      }
      container_config::store(cfg_path, cc);
    } else {
      // there is one, read it
      //  add//merge existing containers to the map and store
      //  construct those in the map, that are missing in current.
      if (!fs::is_regular_file(cfg_path))
        throw std::runtime_error(fmt::format("dtss config must be a file: {}", cfg_path));
      auto cc = container_config::read(cfg_path);
      bool modified{false};
      for (auto const &[c_name, db] : container) {
        if (cc.container_cfg.find(c_name) == cc.container_cfg.end()) {
          cc.container_cfg[c_name] = db->root_dir(); // add container we are missing to cc
          modified = true;
        } else {
          // its already there, we could check for db->root_dir() vs. cc[c_name]
          // and if different:
          //  what could we do?
          // .. [X] just update the cc!
          // .. [ ] we could update location
          // .. [ ] we could throw.
          if (cc.container_cfg[c_name] != db->root_dir()) {
            cc.container_cfg[c_name] = db->root_dir();
            modified = true;
          }
        }
      }
      if (modified)
        container_config::store(cfg_path, cc); // store it
      // now iterate over the cc, and reconstruct missing
      for (auto const &[c_name, db_root] : cc.container_cfg) {
        if (container.find(c_name) != container.end())
          continue; // skip those that already exist
        // create and add those not yet in container map:
        reconstruct_db_at(c_name, db_root, default_geo_db_cfg);
      }
    }
    cfg_file = cfg_path;
  }

  server::~server() {
    if (msync) {
      terminate = true;
      if (msync_worker.valid())
        msync_worker.get();
    }
  }

  void server::set_master(
    string ip,
    int port,
    double master_poll_time,
    size_t unsubscribe_min_threshold,
    double unsubscribe_max_delay) {
    msync = make_unique<master_slave_sync>(
      this, ip, port, master_poll_time, unsubscribe_min_threshold, unsubscribe_max_delay);

    msync_worker = std::async(std::launch::async, [this]() {
      msync->worker();
    });
  }

  void server::add_container(
    std::string const &container_name,
    std::string const &root_path,
    std::string container_type,
    db_cfg cfg) {
    unique_lock<mutex> sl(c_mx);
    // factory.. CD::create_container(container_name, container_type, root_dir, *this);
    std::string server_container_name;
    if (
      container_type.empty() || container_type == "ts_db" || container_type == "ts_ldb" || container_type == "ts_rdb") {
      if (container.find(container_name) != container.end())
        return; // if container is already there, then ignore the call, the caller can ask for containers, remove and
                // add.
      // above is also useful in hybrid/server/client situations, where some containers are added server-side,
      // and others are added by client later (on restart server, we might end up adding twice, so we rather accept that)
      server_container_name = container_name;
      if (container_type == "ts_ldb") {
        container[server_container_name] = std::make_unique<ts_db_level>(root_path, cfg);
      } else if (container_type == "ts_rdb") {
        container[server_container_name] = std::make_unique<ts_db_rocks>(root_path, cfg);
      } else {
        container[server_container_name] = std::make_unique<ts_db>(root_path);
      }
      // pickup any geo db at location:
      auto found_geo = geo_ts_db_scan(root_path);
      if (found_geo)
        geo[container_name] = *found_geo;

      // NOTE: The "" empty root do have specific semantics: scan and load auto geo
      // db on 1st level directories
      if (server_container_name.empty()) {
        fs::directory_iterator dir(root_path), end;
        for (; dir != end; ++dir) {
          if (!fs::is_directory(dir->path()))
            continue;
          auto found_geo = geo_ts_db_scan(dir->path()); // and
          if (!found_geo)
            continue;
          // yes, there is a geo db here..
          auto geo_container = (*found_geo)->name; // the shyft://geo_container/...
          if (container.find(geo_container) != container.end())
            continue; // if it's already there, do not touch
          // now, register found geo/container
          // could be a ldb, or a plain file db
          reconstruct_db_at(geo_container, dir->path().generic_string(), default_geo_db_cfg);
        }
      }
      if (!cfg_file.empty()) { // update the persisted storage here.
        auto cc = container_config::read(cfg_file);
        cc.container_cfg[server_container_name] = root_path;
        container_config::store(cfg_file, cc);
      }
    } else if (container_type == "krls") { // deprecated really, but keep for now
      server_container_name = std::string{"KRLS_"} + container_name;
      container[server_container_name] = std::make_unique<krls_pred_db>(
        root_path,
        [this](
          std::string const &tsid, utcperiod period, bool use_ts_cached_read, bool update_ts_cache) -> ts_vector_t {
          id_vector_t id_vec{tsid};
          return do_read(id_vec, period, use_ts_cached_read, update_ts_cache);
        });
    } else {
      throw std::runtime_error{std::string{"Cannot construct unknown container type: "} + container_type};
    }
  }

  void server::remove_container(std::string const &container_url, bool remove_from_disk) {
    unique_lock<mutex> sl(c_mx);
    if (container_url.empty()) {
      // do nothing, silent return
      return;
    }
    auto container_name = extract_shyft_url_container(container_url);
    if (container_name.size() > 0) { // is non root shyft container
      auto itr = container.find(container_name);
      if (itr != container.end()) {
        if (remove_from_disk) {
          itr->second->mark_for_deletion();
        }
        container.erase(itr);
        if (!cfg_file.empty()) { // if persistent cfg configure, ensure to remove
          auto cc = container_config::read(cfg_file);
          cc.container_cfg.erase(container_name);
          container_config::store(cfg_file, cc);
        }
      }
      return;
    }
    if (remove_external_cb) {
      remove_external_cb(container_url, remove_from_disk);
    }
  }

  void server::swap_container(std::string const &container_name_a, std::string const &container_name_b) {
    unique_lock<mutex> sl(c_mx);
    auto a = container.find(container_name_a);
    if (a == container.end())
      throw std::runtime_error(fmt::format("container a: {} not found", container_name_a));
    auto b = container.find(container_name_b);
    if (b == container.end())
      throw std::runtime_error(fmt::format("container b: {} not found", container_name_b));
    // also ensure we verify and swap config
    if (!cfg_file.empty()) {
      auto cc = container_config::read(cfg_file);
      auto a = cc.container_cfg.find(container_name_a);
      // if anything goes wrong here.. it is consistent
      // currently we just throw here, but we could
      // attempt to repair later.
      if (a == cc.container_cfg.end())
        throw std::runtime_error(fmt::format("container.cfg a: {} not found", container_name_a));
      auto b = cc.container_cfg.find(container_name_b);
      if (b == cc.container_cfg.end())
        throw std::runtime_error(fmt::format("container.cfg b: {} not found", container_name_b));
      std::swap(a->second, b->second);
      container_config::store(cfg_file, cc);
    }
    std::swap(a->second, b->second);
  }

  server::container_t::iterator
    server::container_find(std::string const &container_name, std::string const &container_query) {
    container_t::iterator f;
    if (
      container_query.empty() || container_query == "ts_db" || container_query == "ts_ldb"
      || container_query == "ts_rdb") {
      f = container.find(container_name);
      if (f == std::end(container)) {
        f = container.find(""); // try to find default container
      }
    } else if (container_query == "krls") {
      f = container.find(std::string{"KRLS_"} + container_name);
    }
    return f;
  }

  its_db &server::internal(std::string const &container_name, std::string const &container_query) {
    auto f = container_find(container_name, container_query);
    if (f == std::end(container))
      throw std::runtime_error(std::string("Failed to find shyft container: ") + container_name);
    return *f->second;
  }

  ts_info_vector_t server::do_find_ts(std::string const &search_expression) {
    if (msync) {
      return msync->find(search_expression);
    } else {
      // 1. filter shyft://<container>/
      auto container = extract_shyft_url_container(search_expression);
      if (container.size() > 0) {
        // assume it is a shyft url -> look for query flags
        auto queries = extract_shyft_url_query_parameters(search_expression);
        auto container_query_it = queries.find(container_query);
        if (!queries.empty() && container_query_it != queries.end()) {
          auto container_query = container_query_it->second;
          filter_shyft_url_parsed_queries(queries, remove_queries);
          return internal(container, container_query)
            .find(extract_shyft_url_path(search_expression, container), queries);
        } else {
          filter_shyft_url_parsed_queries(queries, remove_queries);
          // carefule here, the extract_shyft_url_path, do interpret ? etc.
          string eff_rexp = search_expression.substr(char_str_length(shyft_prefix) + container.size() + 1);
          return internal(container).find(eff_rexp, queries);
        }
      } else if (find_ts_cb) {
        return find_ts_cb(search_expression);
      } else {
        return ts_info_vector_t();
      }
    }
  }

  ts_info server::do_get_ts_info(std::string const &ts_name) {
    if (msync) {
      return msync->get_ts_info(ts_name);
    }
    // 1. filter shyft://<container>/
    auto pattern = extract_shyft_url_container(ts_name);
    if (pattern.size() > 0) {
      // assume it is a shyft url -> look for query flags
      auto queries = extract_shyft_url_query_parameters(ts_name);
      auto container_query_it = queries.find(container_query);
      if (!queries.empty() && container_query_it != queries.end()) {
        auto container_query = container_query_it->second;
        filter_shyft_url_parsed_queries(queries, remove_queries);
        return internal(pattern, container_query).get_ts_info(extract_shyft_url_path(ts_name, pattern), queries);
      } else {
        filter_shyft_url_parsed_queries(queries, remove_queries);
        return internal(pattern).get_ts_info(extract_shyft_url_path(ts_name, pattern), queries);
      }
    } else {
      return ts_info{};
    }
  }

  /** if overwrite on write, then flush the cache prior to writing */
  void server::do_cache_update_on_write(ts_vector_t const &tsv, bool overwrite_on_write) {
    auto add_range = std::views::transform(tsv, [](auto &&ts) {
      auto rts = ts_as<aref_ts>(ts.ts);
      if (!rts) {
        throw std::runtime_error("do_cache_update_on_write called with non aref ts");
      }
      return std::make_tuple(rts->id, apoint_ts(rts->rep));
    });
    ts_cache.add(add_range, overwrite_on_write);
  }

  void server::do_store_ts(ts_vector_t const &tsv, bool overwrite_on_write, bool cache_on_write) {
    if (tsv.size() == 0)
      return;

    if (msync) {
      msync->store_ts(tsv, overwrite_on_write, cache_on_write);
      return;
    }
    // 1. filter out all shyft://<container>/<ts-path> elements
    //    and route these to the internal storage controller (threaded)
    //    std::map<std::string, ts_db> shyft_internal;
    //
    std::vector<std::size_t> other;
    other.reserve(tsv.size());
    std::vector<string> subs;
    bool sub_active = sm && sm->is_active();
    if (sub_active)
      subs.reserve(tsv.size());
    using ts_store_item_t =
      std::tuple<string, gts_t const &>; // need persistent string, a string view would save the day!
    map<string, vector<ts_store_item_t>> internals;
    for (std::size_t i = 0; i < tsv.size(); ++i) {
      auto rts = ts_as<aref_ts>(tsv[i].ts);
      if (!rts)
        throw std::runtime_error("dtss store: require ts with url-references");
      if (sub_active)
        subs.push_back(rts->id);
      auto c = extract_shyft_url_container(rts->id);
      if (c.size() > 0) {
        auto queries = extract_shyft_url_query_parameters(rts->id);
        auto container_query_it = queries.find(container_query);
        if (!queries.empty() && container_query_it != queries.end()) {
          auto container_query = container_query_it->second;
          filter_shyft_url_parsed_queries(queries, remove_queries);
          internal(c, container_query)
            .save(
              extract_shyft_url_path(rts->id, c), // path
              rts->core_ts(),                     // ts to save
              overwrite_on_write,                 // should do overwrite instead of merge
              queries                             // query key/values from url
            );
        } else {
          filter_shyft_url_parsed_queries(queries, remove_queries);
          auto fc = internals.find(c);
          auto ts_name = extract_shyft_url_path(rts->id, c);
          if (fc == internals.end()) {
            vector<ts_store_item_t> cx;
            cx.reserve(tsv.size());
            cx.emplace_back(ts_name, rts->core_ts());
            internals[c] = std::move(cx);
          } else {
            fc->second.emplace_back(ts_name, rts->core_ts());
          }
        }
        // TODO: consider move cache on internal to after successful store, avoid stuff in cache that are not stored
        if (cache_on_write) { // ok, this ends up in a copy, and lock for each item(can be optimized if many)
          if (overwrite_on_write)
            ts_cache.remove(rts->id); // invalidate previous defs. if any
          ts_cache.add(rts->id, apoint_ts(rts->rep));
        }
      } else {
        other.push_back(i); // keep idx of those we have not saved
      }
    }
    // 1.b.. multisave internals
    for (auto const &e : internals) {
      internal(e.first).save(
        e.second.size(),
        [&](size_t i) {
          return ts_item_t{std::get<0>(e.second[i]), std::get<1>(e.second[i])};
        },
        overwrite_on_write);
    }

    // 2. for all non shyft:// forward those to the
    //    store_ts_cb
    if (store_ts_cb && other.size()) {
      if (other.size() == tsv.size()) { // avoid copy/move if possible
        store_ts_cb(tsv);
        if (cache_on_write)
          do_cache_update_on_write(tsv, overwrite_on_write);
      } else { // have to do a copy to new vector
        ts_vector_t r;
        for (auto i : other)
          r.push_back(tsv[i]);
        store_ts_cb(r);
        if (cache_on_write)
          do_cache_update_on_write(r, overwrite_on_write);
      }
    }
    if (sub_active)
      sm->notify_change(subs);
  }

  void server::do_merge_store_ts(ts_vector_t const &tsv, bool cache_on_write) {
    if (tsv.size() == 0)
      return;

    if (msync) {
      msync->merge_store_ts(tsv, cache_on_write);
      return;
    }

    //
    // 0. check & prepare the read time-series in tsv for the specified period of each ts
    //    (we optimize a little bit grouping on common period, and reading in batches with equal periods)
    //
    id_vector_t ts_ids;
    ts_ids.reserve(tsv.size());
    std::unordered_map<utcperiod, id_vector_t, utcperiod_hasher> read_map;
    std::unordered_map<std::string, apoint_ts> id_map;

    for (std::size_t i = 0; i < tsv.size(); ++i) {
      auto rts = ts_as<aref_ts>(tsv[i].ts);
      if (!rts)
        throw std::runtime_error("dtss store merge: require ts with url-references");
      // sanity check
      if (id_map.find(rts->id) != end(id_map))
        throw std::runtime_error("dtss store merge requires distinct set of ids, first duplicate found:" + rts->id);
      id_map[rts->id] = apoint_ts(rts->rep);
      // then just build up map[period] = list of time-series to read
      auto rp = rts->rep->total_period();
      if (read_map.find(rp) != end(read_map)) {
        read_map[rp].push_back(rts->id);
      } else {
        read_map[rp] = id_vector_t{rts->id};
      }
    }

    //
    // 1. do the read-merge for each common period, append to final minimal write list
    //
    ts_vector_t tsv_store;
    tsv_store.reserve(tsv.size());
    for (auto rr = read_map.begin(); rr != read_map.end(); ++rr) {
      auto read_ts = do_read(rr->second, rr->first, false, cache_on_write);
      // read_ts is in the order of the ts-id-list rr->second
      for (std::size_t i = 0; i < read_ts.size(); ++i) {
        auto ts_id = rr->second[i];
        read_ts[i].merge_points(id_map[ts_id]);
        tsv_store.push_back(apoint_ts(ts_id, read_ts[i]));
      }
    }

    //
    // 2. finally write the merged result back to whatever store is there
    //
    do_store_ts(tsv_store, false, cache_on_write);
  }

  ts_vector_t server::do_slave_read(id_vector_t const &ts_ids, utcperiod p, bool use_ts_cached_read) {
    return clip_to_period(do_read(ts_ids, p, use_ts_cached_read, false), p);
  };

  ts_vector_t server::do_read(id_vector_t const &ts_ids, utcperiod p, bool use_ts_cached_read, bool update_ts_cache) {
    if (ts_ids.size() == 0)
      return ts_vector_t{};

    // should cache?
    bool cache_read_results = update_ts_cache || cache_all_reads;

    // 0. filter out ts's we can get from cache, given we are allowed to use cache
    std::unordered_map<std::string, apoint_ts> cc; // cached series
    if (use_ts_cached_read)
      cc = ts_cache.get(ts_ids, p);

    ts_vector_t results(ts_ids.size());
    std::vector<std::size_t> external_idxs;
    if (cc.size() == ts_ids.size()) {
      // if we got all ts's from cache -> map in the results
      for (std::size_t i = 0; i < ts_ids.size(); ++i)
        results[i] = cc[ts_ids[i]];
    } else {
      // 1. filter out shyft://
      //    if all shyft: return internal read
      if (msync) {                            // if master/slave sync, then just collect things to read here
        external_idxs.reserve(ts_ids.size()); // only reserve space when needed
        for (std::size_t i = 0; i < ts_ids.size(); ++i) {
          if (cc.find(ts_ids[i]) == cc.end()) { // not found in cache ?
            external_idxs.push_back(i);         // we need to read this from master dtss
          } else {
            results[i] = cc[ts_ids[i]]; // stash stuff we found in cache
          }
        }
      } else {
        external_idxs.reserve(ts_ids.size()); // only reserve space when needed
        for (std::size_t i = 0; i < ts_ids.size(); ++i) {
          if (cc.find(ts_ids[i]) == cc.end()) {
            auto c = extract_shyft_url_container(ts_ids[i]);
            if (c.size() > 0) {
              // check for queries in shyft:// url's
              auto queries = extract_shyft_url_query_parameters(ts_ids[i]);
              auto container_query_it = queries.find(container_query);
              if (!queries.empty() && container_query_it != queries.end()) {
                auto container_query = container_query_it->second;
                filter_shyft_url_parsed_queries(queries, remove_queries);
                results[i] = apoint_ts(make_shared<gpoint_ts const>(
                  internal(c, container_query).read(extract_shyft_url_path(ts_ids[i], c), p, queries)));
              } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                results[i] = apoint_ts(
                  make_shared<gpoint_ts const>(internal(c).read(extract_shyft_url_path(ts_ids[i], c), p, queries)));
              }
              // caching?
              if (cache_read_results)
                ts_cache.add(ts_ids[i], results[i]);
            } else
              external_idxs.push_back(i);
          } else {
            results[i] = cc[ts_ids[i]];
          }
        }
      }
    }

    // 2. if other/more than shyft get all those
    if (external_idxs.size() > 0) {
      if (msync) {
        // Filter out DSTM urls and use the bind_ts_cb (callback)
        // collect & handle external references
        vector<string> dstm_ts_ids;
        dstm_ts_ids.reserve(external_idxs.size());
        vector<size_t> dstm_idx;
        dstm_idx.reserve(external_idxs.size());
        vector<string> master_ts_ids;
        master_ts_ids.reserve(external_idxs.size());
        vector<size_t> master_idx;
        master_idx.reserve(external_idxs.size());
        for (auto i : external_idxs) {
          if (strncmp(ts_ids[i].c_str(), "dstm://", 7) == 0) {
            dstm_ts_ids.push_back(ts_ids[i]);
            dstm_idx.push_back(i);
          } else {
            master_ts_ids.push_back(ts_ids[i]);
            master_idx.push_back(i);
          }
        }
        if (master_ts_ids.size()) {
          auto rts = msync->read(master_ts_ids, p, true, true); // read from master
          if (cache_read_results) // the ts reads from master might be cached(probably smart)
            ts_cache.add(std::views::zip(master_ts_ids, rts));

          // merge external results into output results
          for (std::size_t i = 0; i < rts.size(); ++i)
            results[master_idx[i]] = rts[i];
        }
        if (dstm_ts_ids.size() && bind_ts_cb) { // if dstm , deal with that
          auto dstm_rts = bind_ts_cb(dstm_ts_ids, p);
          for (std::size_t i = 0; i < dstm_rts.size(); ++i)
            results[dstm_idx[i]] = dstm_rts[i];
          // notice that we do not cache dstm lookups!(they are in memory, attached to STM model)
        }

      } else {
        if (!bind_ts_cb)
          throw std::runtime_error("dtss: read-request to external ts, without external handler");

        // only externaly handled series => return only external result
        if (external_idxs.size() == ts_ids.size()) {
          auto rts = bind_ts_cb(ts_ids, p);
          if (cache_read_results && !ts_ids.front().starts_with("dstm://")) // never cache dstm results
            ts_cache.add(std::views::zip(ts_ids, rts));

          return rts;
        }

        // collect & handle external references
        vector<string> external_ts_ids;
        external_ts_ids.reserve(external_idxs.size());
        for (auto i : external_idxs)
          external_ts_ids.push_back(ts_ids[i]);
        auto ext_resolved = bind_ts_cb(external_ts_ids, p);
        // caching?
        if (cache_read_results) {
          ts_cache.add(std::views::filter(std::views::zip(external_ts_ids, ext_resolved), [](auto &&its) {
            const auto &[id, _] = its;
            // only cache non dstm:
            return !id.starts_with("dstm://");
          }));
        }
        // merge external results into output results
        for (std::size_t i = 0; i < ext_resolved.size(); ++i)
          results[external_idxs[i]] = ext_resolved[i];
      }
    }
    return results;
  }

  void server::do_bind_ts(utcperiod bind_period, ts_vector_t &atsv, bool use_ts_cached_read, bool update_ts_cache) {
    using shyft::time_series::dd::resolve_symbols;
    // explanation compressed: this will resolve pure symbolic refs (e.g. unbound terminal of type aref_ts)
    // by calling our resolver (the lambda below), that will eventually call to do_read,
    // that first will attempt doing cache lookup,
    //  and further more, for those not resolved by cache lookup aboive,
    //  then do msync read(if master/slave config),
    //  or do internal read of shyft://, or callback for non shyft:// refs
    resolve_symbols(atsv, [&](vector<string> const &tsids) {
      return do_read(tsids, bind_period, use_ts_cached_read, update_ts_cache);
    });
  }

  ts_vector_t server::do_evaluate_ts_vector(
    utcperiod bind_period,
    ts_vector_t &atsv,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period) {
    do_bind_ts(bind_period, atsv, use_ts_cached_read, update_ts_cache);
    if (clip_period.valid())
      return clip_to_period(ts_vector_t{shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv)}, clip_period);
    else
      return ts_vector_t{shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv)};
  }

  ts_vector_t server::do_evaluate_percentiles(
    utcperiod bind_period,
    ts_vector_t &atsv,
    gta_t const &ta,
    std::vector<int64_t> const &percentile_spec,
    bool use_ts_cached_read,
    bool update_ts_cache) {
    do_bind_ts(bind_period, atsv, use_ts_cached_read, update_ts_cache);
    std::vector<int64_t> p_spec;
    for (auto const p : percentile_spec)
      p_spec.push_back(int(p));           // convert
    return percentiles(atsv, ta, p_spec); // we can assume the result is trivial to serialize
  }

  void server::do_remove_ts(std::string const &ts_url) {
    if (!can_remove) {
      throw std::runtime_error("dtss::server: server does not support removing");
    }

    if (msync) {
      msync->remove(ts_url);
      return;
    }

    // 1. filter shyft://<container>/
    auto pattern = extract_shyft_url_container(ts_url);
    if (pattern.size() > 0) {
      // assume it is a shyft url -> look for query flags
      auto queries = extract_shyft_url_query_parameters(ts_url);
      auto container_query_it = queries.find(container_query);
      auto shyft_ts_url = extract_shyft_url_path(ts_url, pattern);
      if (!queries.empty() && container_query_it != queries.end()) {
        auto container_query = container_query_it->second;
        filter_shyft_url_parsed_queries(queries, remove_queries);
        internal(pattern, container_query).remove(shyft_ts_url, queries);
      } else {
        filter_shyft_url_parsed_queries(queries, remove_queries);
        internal(pattern).remove(shyft_ts_url, queries);
      }
      ts_cache.remove(ts_url); // remove it from cache as well!
    } else {
      throw std::runtime_error("dtss::server: server does not allow removing for non shyft-url type data");
    }
  }

  id_vector_t server::do_get_container_names() {
    id_vector_t container_names;
    container_names.reserve(container.size());
    for (auto it = container.begin(); it != container.end(); it++) {
      container_names.emplace_back(it->first);
    }
    return container_names;
  }

  void server::do_set_container(std::string const &name, std::string const &path, std::string type, db_cfg cfg) {
    if (msync) {
      msync->set_container(name, path, type, cfg);
      return;
    }
    if (path.empty()) {
      throw std::runtime_error(fmt::format("Empty path not supported"));
    }
    if (type != "ts_db" && type != "ts_ldb" && type != "ts_rdb") {
      throw std::runtime_error(
        fmt::format("Type {} not supported, supported types are [\"ts_db\", \"ts_ldb\", \"ts_rdb\"]", type));
    }
    // find root container:
    auto pc = container_find("", "");

    if (pc == container.end()) {
      throw std::runtime_error(fmt::format("Root container does not exist"));
    }
    auto exists_with_same_name = container_find(name, type);
    if (exists_with_same_name != pc) {
      throw std::runtime_error(fmt::format("Attempting to add a container name that exists via client."));
    }

    fs::path root_dir{pc->second->root_dir()};
    fs::path new_path = root_dir;
    // NOTE: assign should not be nessecary, but lexically_normal does not get applied if run through python
    // because of some reason
    new_path = new_path.append(path).lexically_normal();

    if (fs::exists(new_path)) {
      throw std::runtime_error(fmt::format("There already exists a container at path {}", new_path.string()));
    }
    if (!std::equal(root_dir.begin(), root_dir.end(), new_path.begin())) {
      throw std::runtime_error(fmt::format(
        "Absolute container path {} has to be a subpath of the root path {}", new_path.string(), root_dir.string()));
    }

    add_container(name, new_path.string(), type, cfg);
  }

  void server::do_remove_container(std::string const &container_url, bool remove_from_disk) {
    if (msync) {
      msync->remove_container(container_url, remove_from_disk);
      return;
    }
    if (container_url.empty()) {
      throw std::runtime_error("Removing an emtpy url is not supported");
    }
    remove_container(container_url, remove_from_disk);
  }

  void server::do_swap_container(std::string const &a, std::string const&b) {
    if (msync) {
      msync->swap_container(a, b);
      return;
    }
    swap_container(a, b);
  }
}
