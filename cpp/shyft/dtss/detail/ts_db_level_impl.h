/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string_view>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/common.h>
#include <boost/endian/conversion.hpp>
#include <leveldb/slice.h>
#include <leveldb/iterator.h>

namespace shyft::dtss::detail {
  using namespace shyft::core;

/**
 * @brief The header-info for time-series
 * @details
 * We need it to be minimal, fast, aligned,
 * since there will be multi-million number of them.
 * Also need to be simple POD
 */
#pragma pack(push, 1)

  struct ts_db_level_header { // we put easily aligned data-members first
    utctime t0{no_utctime};   ///< [from..until> period range
    utctime dt{
      0}; ///< delta t step for fixed interval and calendar type, total time-span for variable timestep/point time-axis
    uint64_t n{0};                ///< number of points in the time-series (time-axis and values)
    utctime tff{no_utctime};      ///< start time of the first data fragment
                                  // we could consider points pr fragemnt on individual level here.
    uint64_t ts_id{0};            ///< Unique id that serves as key for the data related to this time series
    utctime modified{no_utctime}; ///< modification time
    char tz[64]{0};               // Char array to store time-zone for calendar case, aligned to 8 byte boundaries
    // the next start on 8byte aligned boundary, and takes 2xbytes +2x1bytes, 4 bytes
    time_series::ts_point_fx point_fx{time_series::POINT_AVERAGE_VALUE};       ///< point_fx feature
    time_axis::generic_dt::generic_type ta_type{time_axis::generic_dt::FIXED}; ///< time-axis type
    int16_t version{0}; ///< we keep a version number, to allow change later

    ts_db_level_header() = default;

    ts_db_level_header(
      time_series::ts_point_fx point_fx,
      time_axis::generic_dt::generic_type ta_type,
      utctime t0,
      utctime dt,
      uint64_t n,
      utctime tff,
      uint64_t ts_id,
      utctime modified,
      std::string_view tzs)
      : t0{t0}
      , dt{dt}
      , n{n}
      , tff{tff}
      , ts_id{ts_id}
      , modified{modified}
      , point_fx{point_fx}
      , ta_type{ta_type} {
      tzs.copy(
        tz,
        sizeof(tz)
          - 1); // string_view becomes useful here dealing with string, const char* in a transparent efficient way
    }

    /**
     * @brief compute total_period
     * @details
     * Based on the kind of header, provide the total period of the stored ts.
     * @tparam FX given const char *tz_id, return an object that can do add(t0,dt,n) for that tz_id
     * @param cal_lookup of type FX callable
     * @return the computed total period for this stored time-series
     */
    template <class FX>
    inline utcperiod total_period(FX&& cal_lookup) const {
      switch (ta_type) {
      case time_axis::generic_dt::FIXED:
        return utcperiod{t0, t0 + n * dt};
      case time_axis::generic_dt::CALENDAR:
        return utcperiod{t0, cal_lookup(tz).add(t0, dt, n)};
      case time_axis::generic_dt::POINT:
        break;
      }
      return utcperiod{t0, t0 + dt};
    }
  };

#pragma pack(pop)

  /**
   * @brief zero weigth class view into something that should have frag_key format.
   * @details
   * The purpose of this is to create light weigth read-only memory view into something
   * that provides the memory,(e.g. leveldb string key, or Slice key).
   */
  struct frag_key_view {
    static constexpr size_t sz{8 + 1 + 8};
    unsigned char const * const b;

    template <class V>
    frag_key_view(V const & s)
      : b{reinterpret_cast<unsigned char const *>(s.data())} {
      if (s.size() != sz)
        throw std::runtime_error("illegal size for frag-key");
    }

    inline uint64_t ts_id() const noexcept {
      return boost::endian::load_big_u64(b);
    }

    inline char frag_type() const noexcept {
      return *(reinterpret_cast<char const *>(b) + 8);
    }

    inline utctime time() const noexcept {
      return utctime{int64_t(boost::endian::load_big_u64(b + 9) - std::numeric_limits<int64_t>::max())};
    }

    auto operator<=>(frag_key_view const & o) const {
      return std::memcmp(reinterpret_cast<void const *>(b), reinterpret_cast<void const *>(o.b), sz);
    }

    char const * data() const noexcept {
      return reinterpret_cast<char const *>(b);
    }

    constexpr size_t size() const noexcept {
      return sz;
    }
  };

  /**
   * @brief Efficient frag_key
   * @details
   * The time-series values/timepoints are stored like fragments of specified sizes.
   * The construction of the keys of these are important, because we need to utilize
   * the ordered sequencing of iterators in leveldb, that
   * ensure iterators work in key-order.
   * The order of the keys are checked by memcmp of the byte-strings.
   * The anticipated usage of this key, is that 'we' construct the key,
   * from parameters like ts-id, key-type(v,t), and utctime,
   *
   * and then pass it to leveldb in some write operations.
   * The frag_key_view supports zero copy inspection/observation
   * of the keys provided by leveldb during iteration.
   * The layout of the key is:
   *
   * - ts-id (8 bytes,bigendian, to ensure correct order)
   * - type (v,t) (1 byte)
   * - utctime (8 bytes, bigendian, translated to unsigned form to ensure we get correct order)
   */
  struct frag_key {
    static constexpr size_t sz{8 + 1 + 8};
    unsigned char b[sz]{0};

    frag_key() = default;

    frag_key(uint64_t id, char tp, utctime t) {
      set_ts_id(id);
      set_frag_type(tp);
      set_time(t);
    }

    frag_key(frag_key_view const & o)
      : frag_key(o.ts_id(), o.frag_type(), o.time()) {
    }

    explicit frag_key(std::string_view v)
      : frag_key(frag_key_view(v)) {
    }

    inline uint64_t ts_id() const noexcept {
      return boost::endian::load_big_u64(b);
    }

    inline char frag_type() const noexcept {
      return *(reinterpret_cast<char const *>(b) + 8);
    }

    inline utctime time() const noexcept {
      return utctime{int64_t(boost::endian::load_big_u64(b + 9) - std::numeric_limits<int64_t>::max())};
    }

    inline void set_ts_id(uint64_t v) noexcept {
      boost::endian::store_big_u64(b, v);
    }

    inline void set_frag_type(char v) noexcept {
      *(reinterpret_cast<char*>(b) + 8) = v;
    }

    inline void set_time(utctime v) noexcept {
      boost::endian::store_big_u64(b + 9, uint64_t(v.count() + std::numeric_limits<int64_t>::max()));
    }

    inline bool empty() const noexcept {
      return frag_type() == 0;
    }

    /**
     * @brief auto cast to leveldb::Slice
     * @details
     * We want to ensure zero copy as well as avoid heap allocs while working.
     * So we provide auto cast to the slice type, in this case for the
     * key, because the bathc.Put, takes Slice as type for key.
     * @return leveldb::Slice view into the data of frag-key.
     * @note we have to do some cast here, that are valid for the usage patterns we aim at
     */
    inline operator leveldb::Slice() const {
      return leveldb::Slice(reinterpret_cast<char*>(const_cast<unsigned char*>(b)), sz);
    }

    /**
     * @brief a leveldb::Slice with ts_id only
     * @details
     * While iterating, we utilize key.starts_with(Slice..),
     * this allow us to have zero copy op of this operations(otherwise it will promote to string. etc)
     * @return leveldb::Slice that captures the ts_id only
     */
    leveldb::Slice slice_ts_id() const noexcept {
      return leveldb::Slice(reinterpret_cast<char*>(const_cast<unsigned char*>(b)), sizeof(uint64_t));
    }

    /**
     * @brief a leveldb::Slice with ts_id and type only
     * @details
     * While iterating, we utilize key.starts_with(Slice..),
     * this allow us to have zero copy op of this operations(otherwise it will promote to string. etc)
     * @return leveldb::Slice that captures the ts_id only
     */
    leveldb::Slice slice_ts_id_type() const noexcept {
      return leveldb::Slice(reinterpret_cast<char*>(const_cast<unsigned char*>(b)), sizeof(uint64_t) + 1);
    }

    auto operator<=>(
      frag_key const & o) const = default; // the default comparison is memcmp, otherwise we need to override..

    char const * data() const noexcept {
      return reinterpret_cast<char const *>(b);
    }

    constexpr size_t size() const noexcept {
      return sz;
    }
  };

  /**
   * @brief frag_value helper to deal with string -> utctime|double vectors
   * @details
   * leveldb  iterator return it->value as Slice,,
   * so this class make it appear as it returns
   * values as a sequence of values(double) or timepoints (utctime).
   * usage is like
   * frag_v<double>(it->value()).copy(val);
   * frag_v<double>(it).copy(val)..;
   *  promising no allocation on construction (using move).
   */
  template <class T>
  struct frag_v {
    leveldb::Slice s; ///<  leveldb iterator return a slice

    explicit frag_v(leveldb::Slice const & sv)
      : s{sv} {
      // we could check s.size() is multiple of sizeof(T)
    }

    explicit frag_v(std::unique_ptr<leveldb::Iterator> const & i)
      : s{i->value()} {
    }

    size_t size() const noexcept {
      return s.size() / sizeof(T);
    }

    T const * data() const noexcept {
      return reinterpret_cast<T const *>(s.data());
    }

    void copy(std::vector<T>& dest) const noexcept {
      std::copy(data(), data() + size(), std::back_inserter(dest));
    }

    void copy_slice(size_t skip_n, size_t n, std::vector<T>& dest) const noexcept {
      // we could check skip_n+n < size()..
      std::copy(data() + skip_n, data() + skip_n + n, std::back_inserter(dest));
    }

    void copy_slice(size_t skip_n, std::vector<T>& dest) const noexcept {
      // we could check skip_n+n < size()..
      std::copy(data() + skip_n, data() + size(), std::back_inserter(dest));
    }

    T front() const noexcept {
      return *data(); // consider check size()>0
    }

    T back() const noexcept {
      return *(data() + size() - 1);
    }
  };

}
