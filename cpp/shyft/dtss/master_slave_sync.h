/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>
#include <utility>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/dtss/time_series_info.h>
#include <shyft/time_series/dd/geo_ts.h>
#include <shyft/dtss/geo.h>
#include <shyft/dtss/db_cfg.h>

namespace shyft::dtss {
  using shyft::core::utcperiod;
  using shyft::core::no_utctime;
  using shyft::time_series::dd::apoint_ts;
  using id_vector_t = std::vector<std::string>;
  using ts_info_vector_t = std::vector<ts_info>;
  using ts_vector_t = shyft::time_series::dd::ats_vector;


  // fwd decl.
  struct client;
  using client_ = std::shared_ptr<client>;
  struct server;

  /** @brief client side master-slave sync item */
  struct ts_sub_item {
    utcperiod p;          ///< keep track of grand period we are subscribing to
    std::string const id; // need this to promise it is never changing after created.

    template <class S>
    ts_sub_item(utcperiod p, S &&s)
      : p{p}
      , id{std::forward<S>(s)} {
    }

    void extend_period(utcperiod const &x) noexcept {
      if (x.start < p.start)
        p.start = x.start;
      if (x.end > p.end)
        p.end = x.end;
    }
  };

  namespace detail { // stash we need to operate transparently with guaranteed pointer to string instead of strings

    struct str_ptr_hash {
      inline std::size_t operator()(std::string const *s) const noexcept {
        return std::hash<std::string>{}(*s);
      }
    };

    struct str_ptr_eq {
      inline bool operator()(std::string const *lhs, std::string const *rhs) const noexcept {
        return *lhs == *rhs;
      }
    };
  }

  /** @brief the ts_sub_map is used at the dtss client slave to keep a local registry of subscriptions
   * @details
   * We use unordered_map, with the keys being stored in the members of the map, avoiding to copy
   * strings (that could be lenghty). This is similar to the subscription manager strategy(have a look at sm.active
   * map).
   */
  using ts_sub_map =
    std::unordered_map<std::string const *, std::unique_ptr<ts_sub_item>, detail::str_ptr_hash, detail::str_ptr_eq>;

  /**
   * @brief dtss master slave sync
   *
   * @details
   * Keeps the dtss slave in sync with the master for the minimal
   * list of time-series known to the users of the slave dtss.
   *
   * The minimal list of time-series is
   *
   *   (a) the ones with active subscription (through subscription-manager, typically
   *       setup on the web-api on websockets.)
   *   (b) any time-series in the cache(regardless subscription).
   *       (these needs to be in sync with master in the case there are read requests
   *       that will be satisfied using the cached value)
   *
   *
   * This class implements the vital parts of orchestrating this logic.
   *
   * Things to know:
   *
   * (1) The slave forwards all io- operations to the master,
   * but takes care of all computation and eventual caching  of
   * the time-series as read from the master.
   *
   * (2) Since the slave have local memory it needs to be notified, and
   * updated if there are any changes to the time-series at the master.
   *
   * (3) On the master side, this is kept as a local subscription kept
   * at lifetime equal to the connection lifetime. Thus we rely on long(er)
   * or stable connections between master and slaves. The slave, regularly polls the master
   * about the changes in the subscribed set of timeseries.
   * Since this is kept on the master, there is minimal communication overhead.
   *
   * (4) The Client ALSO keeps a list of the subscribed time-series (along with the grand period
   * of interest in case of several fragments(currently not supporting smart frags))
   * This allow the client on its own to figure out if it should unsubscribe
   * timeseries on the events of web-api unsubscribe, or later a cache-eviction of
   * a cached timeseries (both of them require a live subscription to be valid and working).
   * A nice side-effect of the client side register is that the client is capable of
   * restoring the subscriptions in the case of communication failure, or server restart.
   *
   * (5) The direct cost of the subscription in terms of memory is the size of the ts-ids plus 8 bytes,
   * applies to both server and client (both have a copy).
   *
   * (6) The computational cost for the writer is the hash-lookup and the atomic increment of the version
   * number. The computation cost for the observer, is recomputing/comparing published view against
   * the new current value of the view (for time-series version, this boils down to checking the ts
   * version number).
   *
   * (7) There is only one raw socket connection between the master..slave. It is shared between
   * the master-slave-sync thread worker, and ALL other threads that does ts terminal-IO ops
   * (e.g. reading/writing finding/removing etc.)
   * This single rawsocket connection, is protected by a mutex, ensuring sequential access.
   *  (it is possible to have several sockets, but we doubt the benfit, and it would require
   * a raw-socket protocol similar to websocket(a subject agreed to by both parties that serve as
   * the mailbox on the serverside).
   *
   *(8) The master-slave sync thread performs these tasks:
   *   a. polls for changes at regular intervals (we could piggyback changes on other requests..)
   *   b. try to minimize the master subscription by monitoring slave-side unsubscriptions
   *      and cache evictions.
   *   c. keeps the slave side structures(with mx protection).
   *
   *(9) The OTHER threads, that do terminal IO as described above, also uses the same socket
   *    to satisfy basic io requirements and
   *   a. update sthe master-slave-sync with *NEW* subscriptions when writing
   *   b. cache locals write (so that it apears immediate consistent if followed by a read)
   *   c. if we do piggyback feature, also take care of processing the payload from
   *      the server, e.g. push updates to local cache, and notify subscriptions.
   *
   */
  struct master_slave_sync {
    master_slave_sync(
      server *slf,
      std::string ip,
      int port,
      double master_poll_time,
      size_t unsubscribe_min_threshold,
      double unsubscribe_max_delay);
    server *self; ///< the server, owning this master_slave_sync
    // from self, we are interested in
    // A. minimum cost event indicators:
    //  (1) the cache (knowing about evicted items)
    //  (2) the sm (knowing about unsubscribed items)
    // B. computing exact updated subscription scope reduction
    //   based on
    //    (1) self.cache items (all of them are subject to subs)
    //    (2) the active sm
    // Assuming our own subs and the above is same size(except when the one are zero)
    //  .. collect the set of ts_ids from subs and cache
    //  .. the  section of subs and (union of self.subs, self.cache)
    //  ..    can be removed..

    // data we keep
    std::mutex mx_subs; ///< mx for the subs(at least..)
    ts_sub_map subs; ///< these are the one we are keeping track of at the client side, so that we can unsubscribe, or
                     ///< recover lost io conn
    utctime subs_updated{no_utctime}; ///< last time subs was updated from master, protected by mx_subs
    std::mutex mx_master;             ///< mx for the master(at least..)
    client_ master; ///< the raw socket interface to the master (we should allow this to fail, with repair).

    // -- parameters for the worker
    double max_defer_update_time{1.0};     ///< maximum time to defer a poll-cycle(if few subs)
    size_t unsubscribe_minimum_count{100}; ///< minimum unsubscribed entities before direct unsubcribe is done
    double master_poll_time{0.01};         ///< maximum delay before worker thread synchronizes with master
    void worker();                         ///< the worker thread main routine

    /**
     * @brief repair subs
     * @details
     * If the connection to the master was broken (as in really lost, and
     * reconnected).
     * The repair_subs(), simply does a
     * full read of the existing subs,
     * cache and notify.
     */
    void repair_subs();

    /** @brief safely add subs for specified period  */
    template <typename R>
    requires std::ranges::input_range<R> && std::convertible_to<std::ranges::range_reference_t<R>, std::string>
    void add_subs(R &&ids, utcperiod p) {
      std::scoped_lock _(mx_subs);
      std::ranges::for_each(SHYFT_FWD(ids), [&](auto &&id) {
        auto f = subs.find(&id);
        if (f == subs.end()) {
          auto i = std::make_unique<ts_sub_item>(p, SHYFT_FWD(id));
          subs[&i->id] = std::move(i); // important to move
        } else {
          f->second->extend_period(p);
        }
      });
    }

    /**
     * Let ALL
     * interaction with the master dtss go through this class
     * so that we easily can control the local subs, as well as doing
     * smart recovery of lost connections.
     *
     */
    std::vector<apoint_ts> read(id_vector_t const &ts_urls, utcperiod p, bool use_ts_cached_read, bool subscribe);
    ts_info_vector_t find(std::string const &search_expression);
    void store_ts(ts_vector_t const &tsv, bool overwrite_on_write, bool cache_on_write);
    void merge_store_ts(ts_vector_t const &tsv, bool cache_on_write);
    ts_info get_ts_info(std::string const &ts_url);
    void remove(std::string const &name);
    bool has_subscription(std::string_view container_url);

    //-- geo related, currently just pass through, to ensure that it still works
    geo::geo_ts_matrix geo_evaluate(geo::eval_args const &ea, bool use_cache, bool update_cache);
    void geo_store(std::string const &geo_db_name, geo::ts_matrix const &tsm, bool replace, bool update_cache);
    std::vector<geo::ts_db_config_> get_geo_ts_db_info();
    void add_geo_ts_db(geo::ts_db_config_ const &gdb);
    void remove_geo_ts_db(std::string const &geo_db_name);
    void set_container(std::string const &name, std::string const &path, std::string type, db_cfg cfg);
    void remove_container(std::string const &container_url, bool remove_from_disk);
    void swap_container(std::string const&a,std::string const& b);
  };
}
