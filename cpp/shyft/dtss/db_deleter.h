#pragma once
#include <string>
#include <shyft/core/fs_compat.h>

namespace shyft::dtss {

  /**
   * @brief Struct to hold RAII deletion state of db objects.
   * Will delete underlying storage on destruction
   */
  struct db_deleter {
    std::optional<std::string> root_dir;

    db_deleter()
      : root_dir(std::nullopt) {
    }

    db_deleter(db_deleter const &) = delete;            // non construction-copyable
    db_deleter &operator=(db_deleter const &) = delete; // non copyable

    // Non moveable since destructor is defined


    ~db_deleter() {
      if (root_dir && fs::is_directory(*root_dir)) {
        fs::remove_all(*root_dir);
      }
    }

    void arm(std::string_view const root) {
      if (!root_dir) {
        root_dir = std::string{root};
      }
    }

    void defuse() {
      root_dir = std::nullopt;
    }
  };
}