#include <ranges>

#include <shyft/dtss/dtss_db_level.h>

#include <algorithm>
#include <atomic>
#include <cstdint>
#include <exception>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <regex>
#include <stdexcept>
#include <string>

#include <fmt/core.h>
#include <fmt/chrono.h>
#include <leveldb/db.h>
#include <leveldb/write_batch.h>
#include <leveldb/env.h>
#include <leveldb/cache.h>
#include <dlib/logger.h>

#include <shyft/core/fs_compat.h>
#include <shyft/dtss/detail/ts_db_level_impl.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>

namespace shyft::dtss::detail {

  using shyft::core::calendar;
  using shyft::core::deltahours;
  using shyft::core::max_utctime;
  using shyft::core::min_utctime;
  using shyft::core::utctime_now;
  using shyft::time_series::ts_point_fx;
  using ta_generic_type = shyft::time_axis::generic_dt::generic_type;

  //------------------ ts_db_level_impl --------
  /**
   * @brief configuration for each instance
   *
   * @details
   *
   * We store, and later retrieve the level db configuration when opening a database.
   *
   */
  struct level_db_cfg {
    static constexpr std::uint64_t magic1{0xdeadbeefbaadf00dul};
    static constexpr std::uint64_t ppf_default{1024 * 1024 * 10}; ///< 10 M frags, gives 80 MB pr frag
    std::uint64_t magic{magic1};                                  ///< Ensure some kind of integrity
    std::uint64_t ppf{ppf_default};                               ///< points per fragment for the database
    std::uint64_t uid{0};                                         ///< current uid counter of the database
    std::uint64_t clean_shutdown{0}; ///< if 0, shutdown was clean otherwise recover uid is needed

    bool valid() const {
      return magic == magic1 && ppf > 1;
    }
  };

  /** @brief logger */
  struct db_log : public leveldb::Logger {
    static dlib::logger dlog;

    void Logv(char const * format, std::va_list ap) override {
      if (dlog.level() >= dlib::LDEBUG) {
        char buf[1024];
        std::vsnprintf(buf, sizeof(buf), format, ap);
        dlog << dlib::LINFO << buf;
      }
    }
  };

  dlib::logger db_log::dlog{"lvldb"};

  struct ts_db_level_impl {
    const std::string cfg_key;
    std::string root_dir;                      ///< root_dir points to the top of the container
    mutable std::atomic_uint64_t uid{0};       ///< uid used while creating new tsids, synced to cfg.
    std::unique_ptr<leveldb::Cache> db_cache;  ///< db block cache
    std::unique_ptr<leveldb::Cache> dbh_cache; ///< dbh block cache
    std::unique_ptr<leveldb::DB> db;           ///<  database for data (could be very large)
    std::unique_ptr<leveldb::DB> dbh;          ///< database for headers (small amount of data)
    level_db_cfg cfg;                          ///< the in memory configuration, stored at dbh.cfg key

    static constexpr char const * data_dir = "/data";      ///< root_dir/data
    static constexpr char const * header_dir = "/headers"; ///< root_dir/headers

    std::map<std::string, std::shared_ptr<calendar>> calendars; ///< fast lookup of all calendars

    std::uint64_t test_mode{0}; ///< as pr. db_cfg for testing only
    db_log log;

    static bool exists_at(std::string const & root_dir) {
      if (!fs::exists(root_dir))
        return false;
      return fs::exists(root_dir + data_dir) && fs::exists(root_dir + header_dir);
    }

    ts_db_level_impl(std::string const & root_dir, db_cfg const & _cfg)
      : cfg_key{"cfg"}
      , root_dir{root_dir} {
      if (_cfg.ppf < 1)
        throw std::runtime_error(fmt::format("ts_db_level: ppf must be >0, attempt at {}", root_dir));
      bool needs_create{false};
      test_mode = _cfg.test_mode;
      if (!fs::is_directory(root_dir)) {
        if (!fs::exists(root_dir)) {
          if (!fs::create_directories(root_dir)) {
            throw std::runtime_error(fmt::format("ts_db_level: failed to create root directory '{}'", root_dir));
          }
        } else {
          throw std::runtime_error(
            fmt::format("ts_db_level: designated root directory is not a directory '{}'", root_dir));
        }
        cfg.ppf = _cfg.ppf;
        needs_create = true;
      }
      log.dlog.set_level(dlib::log_level{int(_cfg.log_level), "ldb_cfg"});
      leveldb::Options options;
      options.info_log = &log; // route logs to our logger.
      options.compression = _cfg.compression ? leveldb::kSnappyCompression : leveldb::kNoCompression;
      if (_cfg.ix_cache)
        dbh_cache.reset(leveldb::NewLRUCache(_cfg.ix_cache));
      if (_cfg.ts_cache)
        db_cache.reset(leveldb::NewLRUCache(_cfg.ts_cache));
      options.block_cache = dbh_cache.get();
      log.dlog << dlib::LINFO << "Create level db @" << root_dir;
      if (_cfg.test_mode)
        log.dlog << dlib::LWARN << "Level db driver in test-mode";
      // for header use standard settings.
      //-- for data consider:
      // options.max_file_size=_cfg.max_file_size;//100*1024*1024;//100 MB
      // options.write_buffer_size=_cfg.write_buffer_size;//50*1024*1024;// 50 MB
      // interesting options:
      // .max_file_size = 2 * 1024 * 1024;     .. 2Mbytes. is small in our context
      //            we should consider 100x ?
      // .block_size = 4 * 1024;               .. unzipped data block
      //            we should consider 10x?
      // .write_buffer_size = 4 * 1024 * 1024; .. greater means more performance, at cost of startup.
      //            we could consider 2x100   ... 100 Mega points (is not much)
      //
      // .max_open_files = 1000;               ..
      //            most likely ok for us
      //
      // .block_cache= leveldb::NewLRUCache(100*1024*1024)
      //           remember to remove cache at exit...
      //           also readoptions.fill_cache can be set false for this one.
      // keep in mind we are also caching ts at the ts-cache(so this would be for the internals of leveldb)
      //
      leveldb::DB* dbxh{nullptr};
      options.create_if_missing = needs_create;

      if (!leveldb::DB::Open(options, root_dir + header_dir, &dbxh).ok()) {
        if (needs_create)
          throw std::runtime_error(fmt::format("ts_db_level: failed to create header db at {}", root_dir));
        options.create_if_missing = true; // ok, the directory was there, but no db, automagically create.
        cfg.ppf = _cfg.ppf;               // ensure we put this inplace for new db
        log.dlog << dlib::LINFO << "Creating fresh db @" << root_dir;
        if (!leveldb::DB::Open(options, root_dir + header_dir, &dbxh).ok())
          throw std::runtime_error(fmt::format("ts_db_level: failed to create fresh header db at {}", root_dir));
      }
      dbh = std::unique_ptr<leveldb::DB>(dbxh); // if we get here, we succeeded with header.
      if (options.create_if_missing) { // then, give the above, for sure, a fresh db is in place, and we put the cfg
                                       // inside it to mark it.
        leveldb::WriteOptions wsync;
        wsync.sync = true;
        if (!dbh->Put(wsync, cfg_key, leveldb::Slice{(char const *) &cfg, sizeof(level_db_cfg)}).ok())
          throw std::runtime_error(
            fmt::format("ts_db_level: failed to store initial cfg key to header db at {}", root_dir));
      } else {
        std::string cfgs;
        if (!dbh->Get(leveldb::ReadOptions(), cfg_key, &cfgs).ok()) {
          throw std::runtime_error(
            fmt::format("ts_db_level: failed to read cfg key from existing header db at {}", root_dir));
        }
        if (cfgs.size() != sizeof(level_db_cfg)) {
          throw std::runtime_error(
            fmt::format("ts_db_level: failed to read cfg key, wrong size, from existing header db at {}", root_dir));
        }
        cfg = *reinterpret_cast<level_db_cfg const *>(cfgs.data());
        if (!cfg.valid()) {
          throw std::runtime_error(
            fmt::format("ts_db_level: cfg from header db failed to validate(propably corrupt) at {}", root_dir));
          // here we could try to recover the db.
        }
        uid.store(cfg.uid);            // update the running uid we are using.
        if (cfg.clean_shutdown == 0) { // was not clean shutdown
          log.dlog << dlib::LWARN << "Shutdown was not clean, last uid=" << cfg.uid;
          auto recovered_uid = recover_uid(); // recover the uid, and let it remain 'open'/unclean
          uid = recovered_uid;
          log.dlog << dlib::LWARN << "Recovered updated uid=" << recovered_uid;
          sync_uid(1); // write back clean shutdown
        }
        // since we open existing clean, db. make it unclean by default, so if we have unclean shutdown, we track it
        cfg.clean_shutdown = 0;
        dbh->Put(
          leveldb::WriteOptions(),
          cfg_key,
          leveldb::Slice{(char const *) &cfg, sizeof(level_db_cfg)}); // best effort write.
      }

      // ensure to use file options for data part.
      leveldb::Options ts_options;                           //{options};
      ts_options.max_file_size = _cfg.max_file_size;         // 100*1024*1024;//100 MB
      ts_options.write_buffer_size = _cfg.write_buffer_size; // 50*1024*1024;// 50 MB
      ts_options.create_if_missing = options.create_if_missing;
      ts_options.compression = options.compression;
      ts_options.info_log = options.info_log;
      ts_options.block_cache = db_cache.get();
      leveldb::DB* dbx{nullptr};
      leveldb::DB::Open(ts_options, root_dir + data_dir, &dbx);
      db = std::unique_ptr<leveldb::DB>(dbx);
      make_calendar_lookups();
    }

    ~ts_db_level_impl() {
      if (test_mode == 0)
        sync_uid(1); // ensure to sync uid before closing.
    }

    void sync_uid(std::uint64_t clean_shutdown = 0) {
      if (cfg.uid != uid || clean_shutdown) {
        cfg.uid = uid;
        cfg.clean_shutdown = clean_shutdown;
        dbh->Put(
          leveldb::WriteOptions(),
          cfg_key,
          leveldb::Slice{(char const *) &cfg, sizeof(level_db_cfg)}); // best effort write.
      }
    }

    std::uint64_t recover_uid() {
      std::unique_ptr<leveldb::Iterator> it(dbh->NewIterator(leveldb::ReadOptions()));
      std::uint64_t max_uid = 0;
      for (it->SeekToFirst(); it->Valid(); it->Next()) {
        if (it->key().ToString() == cfg_key)
          continue;
        auto h = reinterpret_cast< ts_db_level_header const *>(it->value().data());
        max_uid = std::max(max_uid, h->ts_id);
      }
      return max_uid; /// recovered value.
    }

    std::uint64_t mk_unique_ts_id() {
      return ++uid;
    }

    auto lookup_calendar(std::string const & tz) const {
      auto it = calendars.find(tz);
      if (it == calendars.end())
        return std::make_shared<calendar>(tz);
      return it->second;
    }

    ts_info get_ts_info(std::string const & fn, queries_t const &) {
      auto h = read_header(fn);
      ts_info i;
      i.name = fn;
      i.point_fx = h.point_fx;
      i.modified = h.modified;
      // i.data_period = h.data_period;
      switch (h.ta_type) {
      case gta_t::CALENDAR:
      case gta_t::FIXED: {
        i.delta_t = h.dt; //(h.data_period.end-h.data_period.start)/h.n;;
        if (h.ta_type == gta_t::CALENDAR) {
          // read tz_info
          i.olson_tz_id = std::string(h.tz);
          i.data_period = utcperiod{h.t0, lookup_calendar(h.tz)->add(h.t0, h.dt, h.n)};
        } else {
          i.data_period = utcperiod{h.t0, h.t0 + h.n * h.dt};
        }
      } break;
      case gta_t::POINT: {
        i.delta_t = utctime{0};
        i.data_period = utcperiod{h.t0, h.t0 + h.dt};
      } break;
      }
      return i;
    }

    std::vector<ts_info> find(std::string const & match, queries_t const & queries) {
      std::vector<ts_info> r;
      std::regex r_match(
        match,
        std::regex_constants::ECMAScript | std::regex_constants::icase); // question if we should ignore case on
                                                                         // leveldb, it was a win problem originally..
      std::unique_ptr<leveldb::Iterator> it(dbh->NewIterator(leveldb::ReadOptions()));
      // find prefix
      static char const re_char[] = {'\\', '.', '(', '[', '^', '$', '*', '?', '+', '{', '|'};
      char const * e = match.data();
      while (*e && std::ranges::find(re_char, *e) == std::ranges::end(re_char))
        e++;
      auto prefix_size = e - match.data();
      leveldb::Slice prefix(match.data(), prefix_size);

      for (it->Seek(prefix); it->Valid() && it->key().starts_with(prefix); it->Next()) {
        std::string key = it->key().ToString();
        if (key == cfg_key)
          continue;
        // match require full match to the expression, search allows trailing characters.
        if (std::regex_match(key, r_match)) {
          r.push_back(get_ts_info(key, queries)); // TODO: maybe multi-core this into a job-queue
        }
      }
      return r;
    }

    void make_calendar_lookups() {
      for (int hour = -11; hour < 12; hour++) { // common fixed interval/nodst tzones
        auto c = std::make_shared<calendar>(deltahours(hour));
        calendars[c->tz_info->name()] = c;
      }
      for (auto tzid : calendar::region_id_list()) { // ensure we have the std list available
        calendars[tzid] = std::make_shared<calendar>(tzid);
      }
    }

    void remove(std::string const & fn) {
      std::string old_header_s;
      auto s = dbh->Get(leveldb::ReadOptions(), fn, &old_header_s);
      if (!s.ok()) {
        throw std::runtime_error(fmt::format("leveldb::failed: '{}', '{}'", fn, s.ToString()));
      } else {
        leveldb::WriteBatch b;
        auto old_header = reinterpret_cast<ts_db_level_header*>(old_header_s.data());
        std::unique_ptr<leveldb::Iterator> it(db->NewIterator(leveldb::ReadOptions()));
        frag_key tskey{old_header->ts_id, 'v', utctime{0}};
        auto key = tskey.slice_ts_id();
        for (it->Seek(key); it->Valid() && it->key().starts_with(key); it->Next())
          b.Delete(key);
        dbh->Delete(leveldb::WriteOptions(), fn);
        db->Write(leveldb::WriteOptions(), &b);
      }
    }

    utcperiod header_total_period(ts_db_level_header const & h) const {
      return h.total_period([this](char const * tz) {
        return *lookup_calendar(tz);
      });
    }

    void
      save(std::string const & fn, gts_t const & ts, bool overwrite, leveldb::WriteBatch& b, leveldb::WriteBatch& bh) {
      std::string old_header_s;
      auto s = dbh->Get(leveldb::ReadOptions(), fn, &old_header_s);
      if (s.IsNotFound()) {
        auto ts_id = mk_unique_ts_id();
        write_new_ts(fn, ts, ts_id, b, bh);
      } else if (!s.ok()) {
        throw std::runtime_error(fmt::format("leveldb::failed: '{}', '{}'", fn, s.ToString()));
      } else {
        auto old_header = reinterpret_cast<ts_db_level_header*>(old_header_s.data());
        auto covered = ts.total_period().contains(header_total_period(*old_header)); // totally covered.
        if (overwrite || covered) {
          gts_t cts;        // in case we need to transform before overwrite.
          if (!overwrite) { // semantically different!! check if it is allowed.. because we will keep ta/dt type.
            check_ta_alignment(*old_header, ts);
            if (old_header->ta_type == time_axis::generic_dt::POINT && ts.ta.gt() != time_axis::generic_dt::POINT) {
              // cover write, with fixed/cal dt over the point, shall not change this property,
              // so we need to make a point dt version of the ts.
              cts = gts_t{time_axis::generic_dt{convert_to_point_dt(ts.ta)}, ts.v, ts.point_interpretation()};
            }
          }
          remove(fn);
          auto ts_id = mk_unique_ts_id();
          write_new_ts(fn, cts.size() ? cts : ts, ts_id, b, bh); // overwrite(with a converted ts if needed)
        } else {
          merge_ts(fn, ts, *old_header, b, bh);
        }
      }
    }

    inline ts_db_level_header mk_header(
      ts_point_fx pfx,
      ta_generic_type gt,
      utctime t0,
      utctime dt,
      std::uint64_t n,
      utctime tff,
      std::uint64_t ts_id,
      std::string tz = "") {
      auto modified = utctime_now();
      return ts_db_level_header{pfx, gt, t0, dt, n, tff, ts_id, modified, tz};
    }

    inline ts_db_level_header mk_header(
      ts_point_fx pfx,
      ta_generic_type gt,
      utctime t0,
      utctime dt,
      std::uint64_t n,
      std::uint64_t ts_id,
      std::string tz = "") {
      auto tff = t0;
      return mk_header(pfx, gt, t0, dt, n, tff, ts_id, tz);
    }

    inline ts_db_level_header mk_header(gts_t const & ts, std::uint64_t ts_id) {
      std::string tz;
      switch (ts.ta.gt()) {
      case time_axis::generic_dt::CALENDAR:
        tz = ts.ta.c().cal->tz_info->name();
        [[fallthrough]];
      case time_axis::generic_dt::FIXED:
        return mk_header(
          ts.point_interpretation(), ts.ta.gt(), ts.ta.total_period().start, ts.time_axis().dt(), ts.size(), ts_id, tz);
      case time_axis::generic_dt::POINT:
        break;
      }
      return mk_header(
        ts.point_interpretation(),
        ts.ta.gt(),
        ts.ta.total_period().start,
        ts.ta.total_period().timespan(),
        ts.size(),
        ts_id,
        tz);
    }

    void write_header(std::string const & fn, ts_db_level_header const & h, leveldb::WriteBatch& b) {
      b.Put(fn, leveldb::Slice(reinterpret_cast<char const *>(&h), sizeof(ts_db_level_header)));
    }

    /**
     * @brief write to batch key, value
     * @tparam TV std::vector type, like std::vector<utctime> or std::vector<double>, required .data() and .size()
     * @param df data fragment
     * @param key the key, as slice, by value is ok since it's a POD, pointer,size
     * @param b the leveldb batch structure
     */
    template <std::ranges::contiguous_range TV>
    static void db_write(TV&& df, leveldb::Slice key, leveldb::WriteBatch& b) {
      using T = std::ranges::range_value_t<TV>;
      b.Put(key, leveldb::Slice(reinterpret_cast<char const *>(df.data()), sizeof(T) * df.size()));
    }

    /**
     * @brief Write time-axis/values to db
     * @precondtions:
     * there are no existing entries/keys for this ts
     */
    void write_new_data(gts_t const & ts, ts_db_level_header const & h, leveldb::WriteBatch& b) {
      using std::views::counted;

      auto const write_values = [&](auto&& frag_key) {
        auto const nff = h.n / cfg.ppf; // Number of full fragments
        for (std::uint64_t i = 0; i < nff; ++i) {
          db_write(counted(ts.v.begin() + i * cfg.ppf, cfg.ppf), frag_key(i), b);
        }
        if (h.n % cfg.ppf) {
          auto n = h.n % cfg.ppf;
          db_write(counted(ts.v.cbegin() + nff * cfg.ppf, n), frag_key(nff), b);
        }
      };

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        write_values([&](auto i) {
          return frag_key{h.ts_id, 'v', h.t0 + i * cfg.ppf * h.dt};
        });
      } break;
      case time_axis::generic_dt::CALENDAR: {
        auto cal = lookup_calendar(h.tz);
        write_values([&](auto i) {
          return frag_key{h.ts_id, 'v', cal->add(h.t0, h.dt, i * cfg.ppf)};
        });
      } break;
      case time_axis::generic_dt::POINT: {
        auto nlf = h.n / cfg.ppf;

        auto const frag_key_fx = [&](auto i, char tv) {
          auto tstp = i == ts.size() ? ts.ta.p().t_end : ts.ta.p().t[i];
          return frag_key{h.ts_id, tv, tstp}; // the time-point where fragment ends is used to timestamp the key
        };

        for (std::uint64_t i = 0; i < nlf; ++i) {
          db_write(counted(ts.ta.p().t.begin() + i * cfg.ppf, cfg.ppf), frag_key_fx((i + 1) * cfg.ppf, 't'), b);
          db_write(counted(ts.v.begin() + i * cfg.ppf, cfg.ppf), frag_key_fx((i + 1) * cfg.ppf, 'v'), b);
        }
        auto npts = ts.size() % cfg.ppf; // last fragment might be less points.
        if (npts) {
          db_write(counted(ts.ta.p().t.begin() + nlf * cfg.ppf, npts), frag_key_fx(nlf * cfg.ppf + npts, 't'), b);
          db_write(counted(ts.v.begin() + nlf * cfg.ppf, npts), frag_key_fx(nlf * cfg.ppf + npts, 'v'), b);
        }

      } break;
      }
    }

    void write_new_ts(
      std::string const & fn,
      gts_t const & ts,
      const std::uint64_t ts_id,
      leveldb::WriteBatch& b,
      leveldb::WriteBatch& bh) {
      auto h = mk_header(ts, ts_id);
      write_header(fn, h, bh);
      write_new_data(ts, h, b);
    }

    gts_t read(std::string const & fn, utcperiod p) {
      auto h = read_header(fn);
      std::uint64_t skip_n = 0u;
      std::uint64_t skip_nb = 0u; // Number of blocks to skip (only used by time_axis::generic_dt::POINT)
      auto ta = read_time_axis(h, p, skip_n, skip_nb);
      auto v = read_values(h, ta, skip_n, skip_nb);
      return gts_t{std::move(ta), std::move(v), h.point_fx};
    }

    ts_db_level_header read_header(std::string const & fn) {
      std::string hs;
      auto s = dbh->Get(leveldb::ReadOptions(), fn, &hs);
      if (!s.ok())
        throw std::runtime_error(fmt::format("leveldb::failed: '{}', '{}'", fn, s.ToString()));
      return *reinterpret_cast<ts_db_level_header*>(hs.data());
    }

    /**
     * @brief compute read-time-axis
     * @details
     * Given header(with its implication on period, dt, calendar),
     * and the read period, then
     * compute the resulting time-axis, and the skip sizes relative fragments.
     */
    gta_t
      read_time_axis(ts_db_level_header const & h, const utcperiod p, std::uint64_t& skip_n, std::uint64_t& skip_nb) {
      gta_t ta;
      ta.set_gt(h.ta_type);
      if(h.ta_type == time_axis::generic_dt::CALENDAR) {
          ta.c().cal = lookup_calendar(h.tz);// ensure to set correct calendar
          ta.c().dt= h.dt;// also ensure to set dt, n is 0/empty
      } else if( h.ta_type == time_axis::generic_dt::FIXED) {
          ta.f().dt= h.dt;// also ensure to set dt, n is 0/empty
      }
      utctime t_start = p.start;
      utctime t_end = p.end;
      if (t_start == no_utctime)
        t_start = min_utctime;
      if (t_end == no_utctime)
        t_end = max_utctime;

      // no overlap?
      auto h_data_period = header_total_period(h);
      if (h_data_period.end <= t_start || h_data_period.start >= t_end) {
        return ta;
      }

      skip_n = 0;

      auto const compute_ta = [&](auto& rta, auto&& diff_units, auto&& add_units) {
        rta.dt = h.dt;
        // handle various overlaping periods
        if (t_start <= h_data_period.start && t_end >= h_data_period.end) {
          // fully around or exact
          rta.t = h.t0;
          rta.n = h.n;
        } else {
          std::uint64_t drop_n = 0;
          if (t_start > h_data_period.start) // start inside
            skip_n = diff_units(h_data_period.start, t_start, rta.dt);
          rta.t = add_units(h.t0, rta.dt, skip_n);
          if (t_end < h_data_period.end) { // end inside, take care to add terminating point (linear case needed)
            if (
              h_data_period.end
              > add_units(t_end, rta.dt, 1)) // more than one units to the end(e.g. not the last interval)
              drop_n = diff_units(t_end, h_data_period.end, rta.dt) - 1; // then include one ore(drop one less)
          }
          // -----
          rta.n = h.n - skip_n - drop_n;
        }
      };

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        compute_ta(
          ta.f(),
          [](auto t0, auto t1, auto dt) {
            return (t1 - t0) / dt;
          },
          [](auto t0, auto dt, auto n) {
            return t0 + dt * n;
          });
      } break;
      case time_axis::generic_dt::CALENDAR: {
        compute_ta(
          ta.c(),
          [&cal = ta.c().cal](auto t0, auto t1, auto dt) {
            return cal->diff_units(t0, t1, dt);
          },
          [&cal = ta.c().cal](auto t0, auto dt, auto n) {
            return cal->add(t0, dt, n);
          });

      } break;
      case time_axis::generic_dt::POINT: {
        frag_key tkey{h.ts_id, 't', utctime{0}};
        auto stkey = tkey.slice_ts_id_type();
        utctime tstp{max_utctime};
        leveldb::ReadOptions ro;
        ro.fill_cache = false; // do not try to use cache, we cache ts.internally
        std::unique_ptr<leveldb::Iterator> it(db->NewIterator(ro));
        for (it->Seek(stkey); it->Valid() && it->key().starts_with(stkey); it->Next()) {
          tstp = frag_key_view(it->key()).time();
          if (tstp <= t_start) {
            skip_nb++;
          } else
            break;
        }

        if (it->Valid() && it->key().starts_with(stkey)) {
          frag_v<utctime> ts(it); // get a view to it, no copy
          std::vector<utctime> tmp;
          tmp.reserve(1 + ts.size()); // at least the size of what we will copy
          ts.copy(tmp);
          if (ts.back() < t_end) { // read more frag? we need surrounding reads (for linear btw. points ts)
            it->Next();
            for (; it->Valid() && it->key().starts_with(stkey); it->Next()) {
              tstp = frag_key_view(it->key()).time();
              frag_v<utctime>(it).copy(tmp);
              if (tmp.back() >= t_end) // did we get timepoints that covers t_end?(either exactly of or after)
                break;                 // we are done reading frags.
            }
          } // else we are done with the one frag that we read,

          //-- now we figure out what part of tmp that should be our result
          auto it_b = tmp.begin();     // first the start of the sequence
          if (t_start > tmp.front()) { // get rid of all excessive points at the begining up to last <=t_start
            it_b = std::upper_bound(tmp.begin(), tmp.end(), t_start, std::less<utctime>());
            if (it_b != tmp.begin()) { // back off one to have surrounding beginning point
              std::advance(it_b, -1);
            }
          }
          // then the end condition, recall: surrounding end point(linear between points)
          utctime f_time = tstp; // end of ts or end of last frag read.
          auto it_e = tmp.end();
          if (t_end <= tmp.back()) {
            it_e = std::lower_bound(
              it_b, tmp.end(), t_end, std::less<utctime>()); // allow us to find t_end, if it exists.
            if (it_e != tmp.end()) {
              std::advance(it_e, 1); // ensure to include point at or first after t_end
              if (it_e != tmp.end()) // are we still interior of a frag?
                f_time = *it_e;      // mark our ts-fragment end where the next(unread) point starts.
            }
          }
          // -----
          skip_n = std::distance(tmp.begin(), it_b);
          ta.p().t.reserve(std::distance(it_b, it_e));
          ta.p().t.assign(it_b, it_e); // excluding  the t_end..
          ta.p().t_end = f_time;
        } // else: there was no frags to read, so time-axis is zero
      } break;
      }
      return ta;
    }

    std::vector<double> read_values(
      ts_db_level_header const & h,
      gta_t const & ta,
      const std::uint64_t skip_n,
      const std::uint64_t skip_nb) {
      auto h_data_period = header_total_period(h);
      auto const read_fixed_values = [&](auto& rta) {
        std::vector<double> val;
        if (rta.n == 0u) // early exit for zero reads
          return val;
        leveldb::ReadOptions ro;
        ro.fill_cache = false; // we do not want to copy content to cache, since we are caching ts in dtss(increases
                               // performance, drops a copy)
        std::unique_ptr<leveldb::Iterator> it(db->NewIterator(ro));
        // Initial fragment might have values to skip_n
        auto n_nan_ff = (h_data_period.start - h.tff) / h.dt; // Number of leading NaNs in the first fragment
        auto skip_n_sf = (skip_n + n_nan_ff)
                       % cfg.ppf; // Number of values to be skipped in the starting fragment to be read
        frag_key sfrag{
          h.ts_id, 'v', h.tff + cfg.ppf * ((skip_n + n_nan_ff) / cfg.ppf) * h.dt}; // complete start frag key
        auto tskey = sfrag.slice_ts_id_type();
        it->Seek(sfrag);
        if (skip_n_sf) {
          if (!it->key().starts_with(tskey)) {
            throw std::runtime_error("leveldb::failed: reading from illegal fragment");
          }
          frag_v<double> f(it);
          const auto n = f.size();
          const auto m = std::min(cfg.ppf - skip_n_sf, rta.n);
          if (skip_n_sf < n) {
            f.copy_slice(skip_n_sf, std::min(rta.n, n - skip_n_sf), val);
            if (m > n)
              val.resize(val.size() + m - n, shyft::nan);
          } else
            val.resize(val.size() + m, shyft::nan);
          it->Next();
        }

        // Read full fragments
        int64_t nff = skip_n_sf ? int64_t(rta.n - cfg.ppf + skip_n_sf) / int64_t(cfg.ppf)
                                : rta.n / cfg.ppf; // Number of full fragments
        for (int i = 0; i < nff; ++i) {
          if (!it->key().starts_with(tskey)) {
            throw std::runtime_error("leveldb::failed: reading from illegal fragment");
          }
          frag_v<double> f(it);
          f.copy(val); // the frag might be less thatn cfg.ppf..
          for (auto i = f.size(); i < cfg.ppf; ++i) {
            val.push_back(shyft::nan); // remaining is nan.
          }
          it->Next();
        }
        // Last fragment might have values remaining to read
        auto nvr = rta.n - val.size(); // Number of values left to read
        if (nvr) {
          if (!it->key().starts_with(tskey)) {
            throw std::runtime_error("leveldb::failed: reading from illegal fragment");
          }
          frag_v<double>(it).copy_slice(0, nvr, val);
        }

        return val;
      };

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        return read_fixed_values(ta.f());
      } break;
      case time_axis::generic_dt::CALENDAR: {
        return read_fixed_values(ta.c());
      } break;

      case time_axis::generic_dt::POINT: {
        frag_key fkey{h.ts_id, 'v', utctime{0}};
        auto vtkey = fkey.slice_ts_id_type(); //(h.ts_id, 'v');
        std::unique_ptr<leveldb::Iterator> it(db->NewIterator(leveldb::ReadOptions()));
        it->Seek(vtkey);
        for (std::uint64_t i = 0; i < skip_nb; ++i)
          it->Next();
        auto needed_size = ta.p().t.size();
        std::vector<double> tmp;
        tmp.reserve(needed_size);
        auto skip_first = skip_n; // we might have to skip some at the first frag size,
        for (; it->Valid() && it->key().starts_with(vtkey); it->Next()) {
          frag_v<double>(it).copy_slice(skip_first, tmp);
          skip_first = 0; // reset the skip_first, remaining blocks are ready as they are
          if (tmp.size() >= needed_size)
            break;
        }
        if (tmp.size() < needed_size) {
          throw std::runtime_error(fmt::format(
            "leveldb::failed: read values, skip_n={}, skip_nb={}, size={} < needed_size={}",
            skip_n,
            skip_nb,
            tmp.size(),
            needed_size));
        }
        if (tmp.size() > needed_size)
          tmp.resize(needed_size); // we might get more than needed, trailing values, due to reading full frags, so we
                                   // reduce the size
        return tmp;
      } break;
      }
      throw std::runtime_error("leveldb::failed: unknown time-axis type");
    }

    void check_ta_alignment(ts_db_level_header const & old_header, gts_t const & ats) {
      if (ats.ta.gt() != old_header.ta_type && old_header.ta_type != time_axis::generic_dt::POINT) {
        throw std::runtime_error(fmt::format(
          "dtss_store: cannot merge with different ta type old {} != new {}",
          (int) old_header.ta_type,
          (int) ats.ta.gt()));
      } else {
        // parse specific ta data to determine compatibility
        constexpr auto diag_string = [](char const * preamble, utctime ta, utctime tb, utctime da, utctime db) {
          calendar utc;
          return fmt::format(
            "{} old t0={},dt={} vs. new t0={},dt={}",
            preamble,
            utc.to_string(ta),
            shyft::core::to_seconds(da),
            utc.to_string(tb),
            shyft::core::to_seconds(db));
        };
        switch (old_header.ta_type) {
        case time_axis::generic_dt::FIXED: {
          if (old_header.dt != ats.ta.f().dt || (old_header.t0 - ats.ta.f().t).count() % old_header.dt.count() != 0) {
            throw std::runtime_error(diag_string(
              "dtss_store: cannot merge unaligned fixed_dt:",
              old_header.t0,
              ats.ta.f().t,
              old_header.dt,
              ats.ta.f().dt));
          }
        } break;
        case time_axis::generic_dt::CALENDAR: {
          if (ats.ta.c().cal->tz_info->tz.tz_name == old_header.tz) {
            utctimespan remainder;
            ats.ta.c().cal->diff_units(old_header.t0, ats.ta.c().t, old_header.dt, remainder);
            if (old_header.dt != ats.ta.c().dt || remainder != utctimespan{0}) {
              throw std::runtime_error(diag_string(
                "dtss_store: cannot merge unaligned calendar_dt:",
                old_header.t0,
                ats.ta.c().t,
                old_header.dt,
                ats.ta.c().dt));
            }
          } else {
            throw std::runtime_error(fmt::format(
              "dtss_store: cannot merge calendar_dt with different calendars, old={},new={}",
              old_header.tz,
              ats.ta.c().cal->tz_info->tz.tz_name));
          }
        } break;
        case time_axis::generic_dt::POINT: {
          // point and point timeaxis are always compatible
        }
        }
      }
    }

    template <class AU, class DU, class FT>
    void do_fixed_merge(
      std::string const & fn,
      ts_db_level_header const & old_header,
      gts_t const & new_ts,
      leveldb::WriteBatch& b,
      leveldb::WriteBatch& bh,
      AU&& add_units,
      DU&& diff_units,
      FT&& frag_time) {
      auto const frag_key_fx = [&](utctime t) {
        return frag_key{old_header.ts_id, 'v', t};
      };
      // assume the two time-axes have the same type and are aligned
      using std::views::counted;

      auto const old_p = header_total_period(old_header);
      auto new_p = new_ts.ta.total_period();


      utcperiod ofp{old_header.tff, frag_time(add_units(old_p.end, cfg.ppf - 1))};          // [ tff, tef)
      utcperiod tsfp{frag_time(new_p.start), frag_time(add_units(new_p.end, cfg.ppf - 1))}; // [ tff, tef)
      utcperiod nfp{std::min(ofp.start, tsfp.start), std::max(ofp.end, tsfp.end)}; // the complete new fragment range.

      std::vector<double> nan_frag;       //(cfg.ppf,shyft::nan);
      auto const ts_v = new_ts.v.begin(); // points to current pos we want to write.
      std::uint64_t i = 0;                // current position into ts_v, updated as we consume values from it.

      for (utctime t = nfp.start; t < nfp.end;
           t = add_units(t, cfg.ppf)) {     // iterate over ALL fragments of the new ts, then consider each of them.
        utctime te = add_units(t, cfg.ppf); // end of current, start of next..
        if (!ofp.contains(t) && !tsfp.contains(t)) { // empty nan areas to fill,with complete nan frags.
          if (nan_frag.size() == 0)
            nan_frag = std::vector<double>(cfg.ppf, shyft::nan);
          db_write(nan_frag, frag_key_fx(t), b);
        } else if (ofp.contains(t) && tsfp.contains(t)) {                                    // tangling with old frags
          auto ti = i < new_ts.ta.size() ? new_ts.ta.time(i) : new_ts.ta.total_period().end; // new_ts.ta.time(i)
          // do we need to read the old ?
          if (ti != t || (ti == t && (new_ts.size() - i < cfg.ppf))) { // we need if we start into the frag, or if we
                                                                       // start at frag, but end before
            std::vector<double> val;
            val.reserve(cfg.ppf);
            std::uint64_t ts_start_ix = diff_units(t, ti);
            utcperiod nts_frag_period{ti, std::min(te, new_p.end)};                       // new ts...
            utcperiod ots_frag_period{std::max(t, old_p.start), std::min(old_p.end, te)}; //
            double const * fvs_begin{nullptr};
            std::uint64_t fvs_size{0};
            std::uint64_t n_ts_first{0};
            std::string fvs; // needs to stay here because, refs fvs_begin etc. refs to it
            if (!nts_frag_period.contains(ots_frag_period)) { // if not new ts cover effective area. existing part:
              auto s = db->Get(leveldb::ReadOptions(), frag_key_fx(t), &fvs);
              fvs_size = fvs.size() / sizeof(double);
              fvs_begin = reinterpret_cast<double const *>(fvs.data());
              n_ts_first = std::min(fvs_size, ts_start_ix);
              std::copy(fvs_begin, fvs_begin + n_ts_first, std::back_inserter(val)); // opt:old contrib goes here.
            }
            val.insert(val.end(), ts_start_ix - n_ts_first, shyft::nan); // opt:nan up to new contrib
            auto n_ts_fill = std::min(cfg.ppf - val.size(), new_ts.size() - i);
            std::copy(ts_v + i, ts_v + i + n_ts_fill, std::back_inserter(val)); // new contrib,
            i += n_ts_fill;

            if (fvs_size > val.size()) {
              std::copy(
                fvs_begin + val.size(), fvs_begin + fvs_size, std::back_inserter(val)); // opt: old contrib trailer part
            }
            db_write(counted(val.begin(), val.size()), frag_key_fx(t), b);
          } else { // we are covering the cfg.ppf, due to check on the if part,
            db_write(counted(ts_v + i, cfg.ppf), frag_key_fx(t), b); // ts does contain enough values.
            i += cfg.ppf;
          }
        } else if (!ofp.contains(t) && tsfp.contains(t)) { // only new frags
          // just write new frags .. trival, almost..nans, in the beginning, the last ones..
          auto ti = i < new_ts.ta.size() ? new_ts.ta.time(i) : new_ts.ta.total_period().end; // new_ts.ta.time(i)
          if (ti != t) {
            std::vector<double> val;
            val.reserve(cfg.ppf);
            std::uint64_t n_nan = diff_units(t, ti);  // diff units(..)
            val.insert(val.end(), n_nan, shyft::nan); // prefill nans
            std::uint64_t n_vals = std::min(
              static_cast<std::uint64_t>(cfg.ppf - n_nan), static_cast<std::uint64_t>(new_ts.v.size() - i));
            std::copy(ts_v + i, ts_v + i + n_vals, std::back_inserter(val)); // fill with avail values..
            i += n_vals;
            // if out of values, this is the last frag.
            if (t < ofp.start) { // need to fill because there are nan blocks to fill after
              val.insert(val.end(), cfg.ppf - val.size(), shyft::nan); // post fill nans
            }
            db_write(counted(val.begin(), val.size()), frag_key_fx(t), b);
          } else { // starts at beginning of frag, flush out to cfg.ppf, or end of ts
            auto n = std::min(cfg.ppf, new_ts.size() - i);
            db_write(counted(ts_v + i, n), frag_key_fx(t), b);
            i += n;
          }
        } // else  case only old values, do nothing
      }
      // update header
      // nfp fragment period, tff,
      utcperiod total_p{std::min(old_p.start, new_p.start), std::max(old_p.end, new_p.end)};
      auto points_n = diff_units(total_p.start, total_p.end); // diffunits

      auto new_header = mk_header(
        new_ts.fx_policy,
        old_header.ta_type,
        total_p.start,
        old_header.dt,
        points_n,
        nfp.start,
        old_header.ts_id,
        old_header.tz);
      write_header(fn, new_header, bh);
    }

    void do_point_merge(
      std::string const & fn,
      ts_db_level_header const & old_header,
      gts_t const & new_ts,
      leveldb::WriteBatch& b,
      leveldb::WriteBatch& bh) {
      using std::views::counted;

      auto const old_p = header_total_period(old_header);
      auto const new_p = new_ts.ta.total_period();
      // determine if we are entirely before or after old ts, because then we do not touch existing frags.

      auto const db_write_tv = [&](auto trange, auto vrange, utctime lts) {
        db_write(trange, frag_key{old_header.ts_id, 't', lts}, b);
        db_write(vrange, frag_key{old_header.ts_id, 'v', lts}, b);
      };
      std::vector<utctime> tx_; // in case merge with any other ta, fixed/cal, make timepoints here.
      if (new_ts.ta.gt() != time_axis::generic_dt::POINT) {
        tx_.reserve(new_ts.ta.size());
        for (std::uint64_t i = 0; i < new_ts.ta.size(); ++i)
          tx_.push_back(new_ts.ta.time(i));
      }

      auto t_ = tx_.size() == 0 ? new_ts.ta.p().t.cbegin() : tx_.cbegin(); // shorthands for t_,v_ begin/iterators
      auto v_ = new_ts.v.cbegin();
      std::uint64_t ndp{0}; // number of deleted points (we need to track what we delete, hmm)
      std::uint64_t nip{0}; // number of inserted points(could be one nan-insert to keep f(t) semantics

      if (new_p.end <= old_p.start) { // entirely before, in a way that we do not have to touch existing frags

        // we fill up the last frag with more date than nppf.
        std::uint64_t n_frags = (new_ts.size() + cfg.ppf - 1) / cfg.ppf;
        for (std::uint64_t i = 0; i < n_frags; ++i) {
          if (i + 1 == n_frags) { // last frag? (and possibly first!)
            auto n_left = new_ts.size() - i * cfg.ppf;
            if (
              new_p.end < old_p.start) { // last frag might need extra nan at the end of it, if new_p.end < old_p.start,
              std::vector<double> v;
              v.reserve(1 + n_left);
              std::vector<utctime> t;
              t.reserve(1 + n_left);
              std::copy(t_ + i * cfg.ppf, t_ + i * cfg.ppf + n_left, std::back_inserter(t)); // then fill in with rest.
              std::copy(v_ + i * cfg.ppf, v_ + i * cfg.ppf + n_left, std::back_inserter(v));
              t.push_back(new_p.end);
              v.push_back(shyft::nan); // at the end, add t, nan to ensure f(t) is consistent
              db_write_tv(counted(t.cbegin(), t.size()), counted(v.cbegin(), v.size()), new_p.end);
              ++nip;
            } else {
              db_write_tv(counted(t_ + i * cfg.ppf, n_left), counted(v_ + i * cfg.ppf, n_left), new_p.end);
            }
          } else { // more than 1 fragment, so we can write cfg.ppf elements
            db_write_tv(
              counted(t_ + i * cfg.ppf, cfg.ppf),
              counted(v_ + i * cfg.ppf, cfg.ppf),
              new_ts.ta.time((i + 1) * cfg.ppf)); // write cfg.ppf
          }
        }
      } else if (new_p.start >= old_p.end) { // entirely after,  in a way that we can leave existing frags as they are.
        std::uint64_t n_frags = (new_ts.size() + cfg.ppf - 1)
                              / cfg.ppf; // we fill up the last frag with more date than nppf.
        for (std::uint64_t i = 0; i < n_frags; ++i) {
          // first frag (potentially also the last)
          if (i == 0) {
            // might be cfg.ppf, or remainder of ts.
            auto n_left = std::min(std::uint64_t(new_ts.size()), cfg.ppf);
            // first frag might need extra nan in front, to ensure f(t) consistency is prevailed
            if (new_p.start > old_p.end) {
              std::vector<double> v;
              v.reserve(1 + n_left);
              std::vector<utctime> t;
              t.reserve(1 + n_left);
              t.push_back(old_p.end);
              v.push_back(shyft::nan); // at beginning, add t, nan to ensure f(t) is consistent
              std::copy(t_ + i * cfg.ppf, t_ + i * cfg.ppf + n_left, std::back_inserter(t)); // then fill in with rest.
              std::copy(v_ + i * cfg.ppf, v_ + i * cfg.ppf + n_left, std::back_inserter(v));
              db_write_tv(
                counted(t.cbegin(), t.size()),
                counted(v.cbegin(), v.size()),
                n_left == new_ts.size() ? new_p.end : new_ts.ta.time(cfg.ppf));
              ++nip;
            } else {
              // write cfg.ppf
              db_write_tv(
                counted(t_ + i * cfg.ppf, n_left),
                counted(v_ + i * cfg.ppf, n_left),
                n_left == new_ts.size() ? new_p.end : new_ts.ta.time(cfg.ppf));
            }
          } else if (i + 1 == n_frags) { // last frag.
            auto n_left = new_ts.size() - i * cfg.ppf;
            db_write_tv(
              counted(t_ + i * cfg.ppf, n_left), counted(v_ + i * cfg.ppf, n_left), new_p.end); // write cfg.ppf
          } else {
            db_write_tv(
              counted(t_ + i * cfg.ppf, cfg.ppf),
              counted(v_ + i * cfg.ppf, cfg.ppf),
              new_ts.ta.time((i + 1) * cfg.ppf)); // write cfg.ppf
          }
        }
      } else { // some kind of overlap (beginning, interior-only, or end), so we  must iterate over existing frags.
        frag_key fkey{old_header.ts_id, 't', utctime{0}};
        auto stkey = fkey.slice_ts_id_type();
        std::unique_ptr<leveldb::Iterator> it(db->NewIterator(leveldb::ReadOptions()));
        std::unique_ptr<leveldb::Iterator> iv(db->NewIterator(leveldb::ReadOptions()));
        utctime t_frag_start{old_header.t0}; // the first frag from the old ts will have this start-time
        // question: should the first loop just read/prep
        utctime t_frag_end;
        std::vector<utctime> t_prepend, t_append; // next loop will add values to prepend//append
        std::vector<double> v_prepend, v_append;  // for existing frags that we partly overlap before and/or after

        for (it->Seek(stkey); it->Valid() && it->key().starts_with(stkey);
             it->Next()) { // ts_id t, we are searching time index, ordered
          auto tkey = it->key();
          t_frag_end = frag_key_view(tkey).time();
          if (t_frag_end < new_p.start) { // the frag is entirely before our period range, so untouched.
            t_frag_start = t_frag_end;    // use the end of this frag as estimate for the start of next frag.
            continue;
          }
          frag_key vkey{old_header.ts_id, 'v', t_frag_end};
          // our range cover everyting, nothing to keep from the old frag,
          if (new_p.start <= t_frag_start && new_p.end >= t_frag_end) {
            // the frag is entirely covered by new,
            // so delete t&v key -- done conditionally at the end of the loop
            // conseptually,we need to count how many points we delete, thus we need to read (ouch) the time-index.
            ndp += frag_v<utctime>(it).size(); // a read, ..just to count number of deleted points.
          } else {                             // partial overlap
            iv->Seek(vkey);                    // we need to get the values as well, for this specific frag
            if (!iv->Valid()) {
              std::string tstamp{calendar().to_string(t_frag_end)};
              throw std::runtime_error(
                fmt::format("leveldb::failed: no such v-key: {} @ {}", old_header.ts_id, tstamp));
            }
            frag_v<double> fv(iv); // a slice, zero copy
            frag_v<utctime> ft(it);

            std::uint64_t n_kept{0};        // we need to keep track of points keept/ npd count
            if (ft.front() < new_p.start) { // keep first value case
              t_prepend.reserve(cfg.ppf);
              v_prepend.reserve(cfg.ppf);
              auto ti = ft.data();
              auto te = ti + ft.size();
              auto vi = fv.data();
              while (ti < te && *ti < new_p.start) {
                t_prepend.push_back(*ti++);
                v_prepend.push_back(*vi++);
              }
              n_kept += std::distance(ft.data(), ti);
            }

            if (new_p.end < t_frag_end) { // keep last value case
              auto ti = ft.data();
              auto te = ti + ft.size();
              ti = std::lower_bound(ti, te, new_p.end);
              auto n = std::distance(ft.data(), ti);
              t_append.reserve(cfg.ppf);
              v_append.reserve(cfg.ppf);
              auto vi = fv.data() + n;
              if (new_p.end != *ti) { // new end INSIDE a period of old  =>  insert value from old where new end
                t_append.push_back(new_p.end);
                vi--;
                v_append.push_back(*vi++);
                n_kept++;
              }
              n_kept += std::distance(ti, te);
              while (ti < te) {
                t_append.push_back(*ti++);
                v_append.push_back(*vi++);
              }
            }
            ndp += ft.size() - n_kept; // update effective number of deleted points
          }
          // delete
          b.Delete(tkey);
          b.Delete(vkey);

          if (new_p.end < t_frag_end) { // next frags will be entirely new ts period, we are done iterating old frags
            break;
          }
          t_frag_start = t_frag_end; // use the end of this frag as estimate for the start of next frag.
        }

        // we are done deleting overlapped t/v keys, and collected prepend/append values for frag overlaps.
        // Straight forward: write prepend, then ts, then append fragment
        //  TODO: possible to do some optimiziation to prune out small frags:
        //        if prepend.size()+ts.size()+append.size() < 2*cfg.ppf: Then merge and write one frag.
        if (t_prepend.size()) { // write the overlap frag at the beginning
          db_write_tv(
            counted(t_prepend.cbegin(), t_prepend.size()), counted(v_prepend.cbegin(), v_prepend.size()), new_p.start);
        }

        std::uint64_t n_frags = (new_ts.size() + cfg.ppf - 1) / cfg.ppf; // the frags from ts
        for (std::uint64_t i = 0; i < n_frags; ++i) {
          auto n = std::min(cfg.ppf, new_ts.size() - i * cfg.ppf);
          db_write_tv(
            counted(t_ + i * cfg.ppf, n),
            counted(v_ + i * cfg.ppf, n),
            i == n_frags - 1 ? new_p.end : new_ts.ta.time((i + 1) * cfg.ppf));
        }

        if (t_append.size()) { // and eventually a part of the trailing values.
          db_write_tv(
            counted(t_append.cbegin(), t_append.size()), counted(v_append.cbegin(), v_append.size()), t_frag_end);
        }
      }
      // finally write the header:
      utcperiod tot_p{std::min(old_p.start, new_p.start), std::max(old_p.end, new_p.end)};
      auto new_header = mk_header(
        new_ts.fx_policy,
        old_header.ta_type,
        tot_p.start,
        tot_p.timespan(), // dt is the complete span of the time-series in this case
        old_header.n + nip + new_ts.size() - ndp,
        old_header.ts_id);
      write_header(fn, new_header, bh);
    }

    void merge_ts(
      std::string const & fn,
      gts_t const & new_ts,
      ts_db_level_header const & old_header,
      leveldb::WriteBatch& b,
      leveldb::WriteBatch& bh) {
      // assume the two time-axes have the same type and are aligned
      check_ta_alignment(old_header, new_ts);

      switch (old_header.ta_type) {
      case time_axis::generic_dt::FIXED: {
        do_fixed_merge(
          fn,
          old_header,
          new_ts,
          b,
          bh,
          [&](utctime t0, int n) { // add
            return t0 + n * old_header.dt;
          },

          [&](utctime t0, utctime t1) {       // diff
            return (t1 - t0) / old_header.dt; // fixed interval def.
          },

          [&](utctime t) { // fragtime
            if (t >= old_header.tff) {
              return old_header.tff
                   + old_header.dt * cfg.ppf * std::int64_t((t - old_header.tff) / (cfg.ppf * old_header.dt));
            } else {
              return old_header.tff
                   - old_header.dt * cfg.ppf * (1 + std::int64_t((old_header.tff - t) / (cfg.ppf * old_header.dt)));
            }
          });

      } break;
      case time_axis::generic_dt::CALENDAR: {
        auto cal = lookup_calendar(old_header.tz); // maybe better/faster to use new_ts.ta.c().cal, avoid  lookup.
        do_fixed_merge(
          fn,
          old_header,
          new_ts,
          b,
          bh,
          [&](utctime t0, int n) { // add
            return cal->add(t0, old_header.dt, n);
          },
          [&](utctime t0, utctime t1) { // diff
            return cal->diff_units(t0, t1, old_header.dt);
          },
          [&](utctime t) { // fragtime
            if (t >= old_header.tff) {
              auto n_ppf_units = cal->diff_units(old_header.tff, t, old_header.dt) / cfg.ppf;
              return cal->add(old_header.tff, old_header.dt, cfg.ppf * n_ppf_units);
            } else {
              auto n_ppf_units = 1 + cal->diff_units(t, old_header.tff, old_header.dt) / cfg.ppf;
              return cal->add(old_header.tff, old_header.dt, -cfg.ppf * n_ppf_units);
            }
          });

      } break;
      case time_axis::generic_dt::POINT: {
        do_point_merge(fn, old_header, new_ts, b, bh);
      } break;
      }
    }
  };
}

//------------------ ts_db_level -------------
namespace shyft::dtss {
  using namespace detail;

  bool ts_db_level::exists_at(std::string const & root_dir) {
    return ts_db_level_impl::exists_at(root_dir);
  }

  ts_db_level::ts_db_level(std::string const & root_dir, db_cfg const & cfg)
    : impl(new ts_db_level_impl(root_dir, cfg)) {
  }

  ts_db_level::~ts_db_level() {
  }

  void ts_db_level::save(std::string const & fn, gts_t const & ts, bool overwrite, queries_t const & q,bool strict_alignment) {
    save(
      1u,
      [&](std::uint64_t) {
        return ts_item_t{fn, ts};
      },
      overwrite,
      q);
  }

  void ts_db_level::save(std::size_t n, fx_ts_item_t const & fx_item, bool overwrite, queries_t const &,bool strict_alignment) {
    if (n == 0u)
      return; // noop as pr now
    leveldb::WriteBatch batch;
    leveldb::WriteBatch header_batch;
    for (std::size_t i = 0; i < n; ++i) {
      auto [fn, ts] = fx_item(i);
      try {
        impl->save(fn, ts, overwrite, batch, header_batch);
      } catch (std::runtime_error const & re) {
        throw std::runtime_error(fmt::format("leveldb save: {}, tsname={}", re.what(), fn)); // annotate problem ts.
      }
    }
    auto r1 = impl->db->Write(leveldb::WriteOptions(), &batch);
    if (!r1.ok())
      throw std::runtime_error(fmt::format("leveldb: batch ts data commit failed {}", r1.ToString()));

    auto r2 = impl->dbh->Write(leveldb::WriteOptions(), &header_batch);
    if (!r2.ok())
      throw std::runtime_error(fmt::format("leveldb: batch ts header commit failed {}", r2.ToString()));
  }

  gts_t ts_db_level::read(std::string const & fn, utcperiod p, queries_t const &) {
    return impl->read(fn, p);
  }

  void ts_db_level::remove(std::string const & fn, queries_t const &) {
    impl->remove(fn);
  }

  ts_info ts_db_level::get_ts_info(std::string const & fn, queries_t const & queries) {
    return impl->get_ts_info(fn, queries);
  }

  std::vector<ts_info> ts_db_level::find(std::string const & match, queries_t const & queries) {
    return impl->find(match, queries);
  }

  std::string ts_db_level::root_dir() const {
    return impl->root_dir;
  }

  void ts_db_level::mark_for_deletion() {
    delete_db_action.arm(impl->root_dir);
  }

}
