#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <memory>
#include <future>
#include <queue>
#include <unordered_map>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/queue_msg.h>

namespace shyft::dtss::queue {
  using std::string;
  using std::shared_ptr;
  using std::make_shared;
  using time_series::dd::apoint_ts;
  using time_series::dd::ats_vector;
  using namespace core;

  /**
   * @brief a queue for time-series vector messages
   * @details
   * A nonblocking, almost endless sized fifo-queue for time-series messages.
   * The producers put messages into the queue with enforced unique message-ids,
   * and the consumers gets these messages out of the queue, and acknowledge
   * the message by a 'done' when consumed.
   * The life-cycle for a message is as follows:
   * producer -> put    ; state=created,  .created=utctime_now() , placed in the queue and in the q.msg-map.
   * consumer -> try_get; state=fetched,  .fetched=utctime_now() , removed from the queue,(but still i q.msg-map)
   * consumer -> done   ; state=done,     .done=utctime_now()    , .. still in the q.msg-map ...
   * maintenance: if created+ttl < tnow: removed from the q.msg-map -> destroyed
   *
   */
  struct tsv_queue {
    using mx_ = std::mutex;
    using sl_ = std::lock_guard<mx_>;

    explicit tsv_queue(string const &name)
      : name{name} {
    }

    tsv_queue() = default;
    string name; ///< the name of the queue

    /**
     * @brief get all message infoes in the queue
     * @returns a list of msg_info objects, new, fetched and done messages.
     */
    vector<msg_info> get_msg_infos() const;

    /**
     * @brief get info for a specific msg_id
     * @param msg_id of the wanted message
     * @returns msg_info for msg_id, or throws if it's not there
     */
    msg_info get_msg_info(string const &msg_id) const;

    /**
     * @brief put message into the queue
     * @details
     * Put message into the queue with the specified info and payload.
     * The message info.created gets the time-stamp(currently downto micro second) of entering the queue.
     * @param msg_id unique id for the current scope of living messages in the queue
     * @param descript any description, json recommended
     * @param ttl time to live for the message, if 'done' it will be automatically removed after create+ttl.
     * @param tsv the time-series vector payload. Usually this is a bound/evaluated ts-vector.
     * @throws runtime_error if msg_id already exists in the queue
     */
    void put(string const &msg_id, string const &descript, utctime ttl, ats_vector const &tsv);

    /**
     * @brief try get a message from the queue
     * @details
     * If not queue empty, the first available element is fetched out, and the message info.fetched is set to
     * utctime_now() A shared pointer is returned, so that the message is still within the queue, but in the 'fetched'
     * state.
     * @returns nullptr or first available queue message.
     */
    tsv_msg_ try_get();

    /** @brief returns the current active(just created) elements waiting in the queue */
    size_t size() const;

    /**
     * @brief consumer mark the message as done
     * @details
     * Set the message info.done=utctime_now(), and the diagnostics to supplied parameters.
     * The message is now eglible for maintenance according to the ttl strategy for the message.
     * @param msg_id the message id
     * @param diag the diagnostics to be attached to the message(visible to the publisher)
     */
    void done(string const &msg_id, string const &diag);

    /**
     * @brief clear and reset queue to its empty state
     */
    void reset();

    /**
     * @brief maintain the queue, flush done items
     */
    size_t flush_done_items(bool keep_ttl_items, utctime x_now = no_utctime);

   private:
    mutable mx_ mx;                           ///< mutex to ensure threadsafe access to class members
    std::queue<tsv_msg_> mq;                  ///< the fifo queue with refs to tsv_msgs
    std::unordered_map<string, tsv_msg_> all; ///< key is .info.msg_id, all kept/traced messages.
  };

  using tsv_queue_ = shared_ptr<tsv_queue>;

  /**
   * @brief queue manager
   * @details
   * Provide the needed functionality for providing the dtss with
   * multiple named queues, add/remove, list and access named queues.
   *
   */
  struct q_mgr {
    using mx_ = std::mutex;
    using sl_ = std::lock_guard<mx_>;

    void add(string const &q_name);

    void remove(string const &q_name);

    size_t size() const;

    vector<string> queue_names() const;

    tsv_queue_ operator()(string const &q_name) const;

   private:
    tsv_queue_ q_(std::string const &n) const;
    mutable mx_ mx;
    std::map<string, tsv_queue_> q;
  };
}
