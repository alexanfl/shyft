#pragma once

#include <functional>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <memory>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/ts_db_interface.h>
#include <shyft/dtss/db_deleter.h>

namespace shyft::dtss {
  using std::unique_ptr;
  namespace ta = shyft::time_axis;
  namespace ts = shyft::time_series;
  using shyft::core::to_seconds64;
  using shyft::core::utctime_from_seconds64;


  /** @brief  Encapsulation of file io functionality.
   */

  struct krls_pred_db_impl; // fwd to impl in detail dtss_krls.cpp and detail/krls_db.h

  class krls_pred_db : public its_db {

   public:
    using gta_t = shyft::time_axis::generic_dt;
    using gts_t = shyft::time_series::point_ts<gta_t>;
    using ts_vector_t = shyft::time_series::dd::ats_vector;
    using queries_t = std::map<std::string, std::string>;
    using cb_fx = std::function<
      ts_vector_t(std::string const &ts_id, utcperiod period, bool use_ts_cached_read, bool update_ts_cache)>;
   private:
    db_deleter delete_db_action; // defined before impl so it gets destroyed last
    unique_ptr<krls_pred_db_impl> impl;

   public:
    krls_pred_db();
    ~krls_pred_db();

    /** Constructs a krls_pred_db with specified container root */

    krls_pred_db(std::string const &root_dir, cb_fx cb);

    krls_pred_db(krls_pred_db const &) = delete;
    krls_pred_db(krls_pred_db &&) = delete;

    krls_pred_db &operator=(krls_pred_db const &) = delete;
    krls_pred_db &operator=(krls_pred_db &&) = delete;

    /*  Container API
     * =============== */

   public:
    void save(std::string const &fn, gts_t const &ts, bool overwrite = true, queries_t const &queries = queries_t{},bool strict_alignment=true)
      override;
    void save(size_t n, fx_ts_item_t const &fx_item, bool overwrite, queries_t const &queries = queries_t{},bool strict_alignment=true) override;
    gts_t read(std::string const &fn, core::utcperiod period, queries_t const &queries = queries_t{}) override;
    void remove(std::string const &fn, queries_t const &queries = queries_t{}) override;
    ts_info get_ts_info(std::string const &fn, queries_t const &queries = queries_t{}) override;
    std::vector<ts_info> find(std::string const &match, queries_t const &queries = queries_t{}) override;

    /*  KRLS container API
     * ==================== */

    void register_rbf_series(
      std::string const &fn,
      std::string const &source_url, // series filename and source url
      core::utcperiod const &period, // period to train
      const core::utctimespan dt,
      const ts::ts_point_fx point_fx,
      const std::size_t dict_size,
      double const tolerance, // general parameters
      double const gamma      // rbf kernel parameters
    );
    void update_rbf_series(
      std::string const &fn,         // series filename
      core::utcperiod const &period, // period to train
      bool const allow_gap_periods = false);

    void move_predictor(std::string const &from_fn, std::string const &to_fn, bool const overwrite = false);

    gts_t predict_time_series(std::string const &fn, gta_t const &ta);

    void mark_for_deletion() override;
  };

}
