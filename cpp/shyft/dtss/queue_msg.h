#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <memory>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

namespace shyft::dtss::queue {
  using std::string;
  using std::shared_ptr;
  using std::make_shared;
  using time_series::dd::apoint_ts;
  using time_series::dd::ats_vector;
  using namespace core;

  /**
   * @brief Information about the tsv message
   */
  struct msg_info {
    msg_info() = default;

    msg_info(string const & msg_id, string const & descript, utctime ttl, utctime created)
      : msg_id{msg_id}
      , description{descript}
      , ttl{ttl}
      , created{created} {
    }

    msg_info(
      string const & msg_id,
      string const & descript,
      utctime ttl,
      utctime created,
      utctime fetched,
      utctime done,
      string const & diag)
      : msg_id{msg_id}
      , description{descript}
      , ttl{ttl}
      , created{created}
      , fetched{fetched}
      , done{done}
      , diagnostics{diag} {
    }

    string msg_id;               ///< id, enforced unique within the live queue messages
    string description;          ///< some kind of description to pass along with the message
    utctime ttl{no_utctime};     ///< eol = created +ttl, when to pruneout/ done messages
    utctime created{no_utctime}; ///< timestamp when the message entered into the queue
    utctime fetched{no_utctime}; ///< timestamp when the message was fetched from the queue
    utctime done{no_utctime};    ///< timestamp when the receiver that fetched the message, posted acknowledge
    string diagnostics;          ///< the diagnostics as passed from the receiver.

    utctime eol() const {
      return created + (ttl != no_utctime ? ttl : from_seconds(30));
    } // arbitrary default 30 sec.

    bool operator==(msg_info const & o) const {
      return msg_id == o.msg_id && description == o.description && ttl == o.ttl && created == o.created
          && fetched == o.fetched && done == o.done && diagnostics == o.diagnostics;
    }

    bool operator!=(msg_info const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  struct tsv_msg {
    tsv_msg();

    tsv_msg(msg_info const & i, ats_vector const & tsv)
      : info{i}
      , tsv{tsv} {
    }

    msg_info info;
    ats_vector tsv;

    bool operator==(tsv_msg const & o) const {
      return info == o.info && tsv == o.tsv;
    }

    bool operator!=(tsv_msg const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  using tsv_msg_ = std::shared_ptr<tsv_msg>;

}

x_serialize_export_key(shyft::dtss::queue::msg_info);
x_serialize_export_key(shyft::dtss::queue::tsv_msg);
