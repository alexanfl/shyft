/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <tuple>
#include <shyft/time_series/point_ts.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss {

  using std::vector;
  using std::map;
  using std::unique_ptr;
  using std::string;
  using std::size_t;

  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using gta_t = shyft::time_axis::generic_dt;
  using gts_t = shyft::time_series::point_ts<gta_t>;

  using queries_t = map<string, string>;
  using ts_item_t = std::tuple<string const &, gts_t const &>; ///< a tuple of reference to name,ts
  using fx_ts_item_t = std::function< ts_item_t(size_t)>;      ///< callable fx that should return ref to path,gts_t

  /**
   * @brief The abstract ts-db container interface
   *
   * @details
   * The dtss::server passes request to save/read/remove or find to instances
   * that implements this interface.
   *
   * The queries_t allow that particular container to utilize user specified data along with the time-series id,
   * and kind of give some parameter possiblities that is utilized in the krls_db class only.
   *
   */
  struct its_db {
    virtual ~its_db() {
    }

    /**
     * @brief Save a time-series to a db
     *
     * @details
     * If missing, then create, or just update the existing part of ts as covered by the passed ts-fragment.
     *
     * @param fn  'Path-name' to save the time-series at, relative the container name, so if shyft://container/a/b/c,
     * then a/b/c.
     * @param ts  Time-series to save.
     * @param overwrite  if true, replace existing stored ts with new (disregard any of previous stored content.
     * @param queries  key/value utilized by krls_db only
     * @param strict_alignment require strict_alignment of time-axis, otherwise true-average the ts to resolve to target time-axis
     *
     */
    virtual void save(string const &fn, gts_t const &ts, bool overwrite, queries_t const &queries = queries_t{}, bool strict_alignment=true) = 0;

    /**
     * @brief Save multiple time-series to db
     * @details
     * Allow implementation to efficiently save multiple time-series to db, using multithread/batch or even
     * transaction like mechanisms to improve throughput.
     * The the callable fx_item(i), will be invoked n-times(unless exception), and should return the i-th fx_ts_item_t.
     * The fx_ts_item_t is a tuple with refs to the name and ts that should be stored, so that we avoid
     * copy of the elements.
     * The lifetime of the returned tuple refs should of course be simliar to the lifetime of the call.
     * In case of exception, the behaviour is implementation specific.
     *
     * Usually the time-series up to the exception occured is stored (best effort).
     *
     * @param n number of time-series that should be saved.
     * @param fx_item callable that provides the reference to (name,ts) pair to be stored.
     * @param overwrite replaces previous stored value if any
     * @param queries key/value utilized by krls_db only (passed on to underlying implementation).
     * @param strict_alignment require strict_alignment of time-axis, otherwise true-average the ts to resolve to target time-axis
     */
    virtual void
      save(size_t n, fx_ts_item_t const &fx_item, bool overwrite, queries_t const &queries = queries_t{},
      bool strict_alignment = true) = 0;

    /**
     * @brief  read a ts
     * @details
     * Read enough part of ts to allow it to be evaluated over the period p.
     * This has the implication that the time-point preceeding the period p is included, if there
     * is no time-point exactly at p.start.
     * For linear point of series, an extra point might be included if there is not
     * time-point exactly at p.end.
     * @param fn name of ts
     * @param p period to read, empty means read entire ts
     * @param queries passed on to impl(only krls is currently using it)
     * @returns the resulting read time-series
     */
    virtual gts_t read(string const &fn, utcperiod p, queries_t const &queries = queries_t{}) = 0;

    /** @brief removes a ts from the container */
    virtual void remove(string const &fn, queries_t const &queries = queries_t{}) = 0;

    /** @brief get minimal ts-information from specified fn */
    virtual ts_info get_ts_info(string const &fn, queries_t const &queries = queries_t{}) = 0;

    /**
     * @brief find ts matching criteria
     * @details
     * Find all ts_info s that matches the specified regexp match string
     *
     * e.g.: match= 'hydmet_station/.*_id/temperature'
     *    would find all time-series /hydmet_station/xxx_id/temperature
     * @param match regular expression to match
     */
    virtual vector<ts_info> find(string const &match, queries_t const &queries = queries_t{}) = 0;

    /** @brief return the root_dir string, if applicable, default to empty string */
    virtual string root_dir() const {
      return string("");
    }

    inline static const string reserved_extension{
      ".cfg"}; ///< return reserved extension, like .cfg, that are disalloved for  ts file-names

    /**
     * @brief mark the container for disk deletion when the destructor is called
     */
    virtual void mark_for_deletion() = 0;
  };

}
