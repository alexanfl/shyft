/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <shyft/core/dlib_utils.h>

namespace shyft::dtss {

  /**
   * @brief dtss message-types
   * @details
   * The message types constants used for the wire-communication of dtss.
   *
   */
  struct message_type {
    using type = std::uint8_t;
    static constexpr type SERVER_EXCEPTION = 0;
    static constexpr type EVALUATE_TS_VECTOR = 1;
    static constexpr type EVALUATE_TS_VECTOR_PERCENTILES = 2;
    static constexpr type FIND_TS = 3;
    static constexpr type GET_TS_INFO = 4;
    static constexpr type STORE_TS = 5;
    static constexpr type CACHE_FLUSH = 6;
    static constexpr type CACHE_STATS = 7;
    static constexpr type EVALUATE_EXPRESSION = 8;
    static constexpr type EVALUATE_EXPRESSION_PERCENTILES = 9;
    static constexpr type MERGE_STORE_TS = 10;
    static constexpr type REMOVE_TS = 11;
    static constexpr type EVALUATE_TS_VECTOR_CLIP = 12;
    static constexpr type EVALUATE_EXPRESSION_CLIP = 13;
    static constexpr type EVALUATE_GEO = 14;
    static constexpr type GET_GEO_INFO = 15;
    static constexpr type STORE_GEO = 16;
    static constexpr type ADD_GEO_DB = 17;
    static constexpr type REMOVE_GEO_DB = 18;
    static constexpr type GET_CONTAINERS = 19;
    static constexpr type SLAVE_READ = 20;
    static constexpr type SLAVE_UNSUBSCRIBE = 21;
    static constexpr type SLAVE_READ_SUBSCRIPTION = 22;
    static constexpr type GET_VERSION = 23;
    static constexpr type Q_LIST = 24;
    static constexpr type Q_INFOS = 25;
    static constexpr type Q_INFO = 26;
    static constexpr type Q_PUT = 27;
    static constexpr type Q_GET = 28;
    static constexpr type Q_ACK = 29;
    static constexpr type Q_SIZE = 30;
    static constexpr type Q_ADD = 31;
    static constexpr type Q_REMOVE = 32;
    static constexpr type Q_MAINTAIN = 33;
    static constexpr type SET_CONTAINER = 34;
    static constexpr type REMOVE_CONTAINER = 35;
    static constexpr type SWAP_CONTAINER=36;
    // EVALUATE_TS_VECTOR_HISTOGRAM //-- tsv,period,ta,bin_min,bin_max -> ts_vector[n_bins]
  };

  /** @brief adapt low-level and message-type handling from the core/dblib_utils.h */
  using msg = core::msg_util<message_type>;

}
