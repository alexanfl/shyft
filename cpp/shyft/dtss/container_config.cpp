/** This file is part of Shyft. Copyright 2015-2023 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/dtss/container_config.h>

// impl
#include <shyft/core/core_archive.h>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <fstream>
// impl part
namespace shyft::dtss {
  using namespace shyft::core;

  template <class Archive>
  void shyft::dtss::container_config::serialize(Archive& ar, unsigned int const ) {
    ar& core_nvp("container_cfg", container_cfg);
  }

  container_config container_config::read(std::string const & path) {
    std::ifstream i(path, std::ios::binary);
    core_iarchive a(i, core_arch_flags);
    container_config r;
    a >> r;
    return r;
  }

  void container_config::store(std::string const & path, container_config const & cfg) {
    std::ofstream o(path, std::ios::binary | std::ios::trunc);
    core_oarchive a(o, core_arch_flags);
    a << cfg;
  }
}

x_serialize_instantiate_and_register(shyft::dtss::container_config);
