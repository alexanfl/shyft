/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <unordered_set>
#include <shyft/dtss/master_slave_sync.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/ts_subscription.h>

namespace shyft::dtss {
  using shyft::time_series::dd::ts_as;
  using shyft::time_series::dd::ats_vector;

  using shyft::core::from_seconds;
  using shyft::core::utctime_now;
  using shyft::core::utctime;
  using shyft::core::no_utctime;
  using detail::str_ptr_eq;
  using detail::str_ptr_hash;
  using c_string_hash = std::unordered_set<std::string const *, str_ptr_hash, str_ptr_eq>;

  namespace {

    // auto fix subscriptions if we get indication of reconnects
    struct subs_fixer {
      master_slave_sync &mss;    // just ref to mss so we can do cleanup
      size_t reconnect_count{0}; // reconnect_count before we starts

      subs_fixer(master_slave_sync &mss)
        : mss{mss} {
        reconnect_count = mss.master->reconnect_count(); // set it to current value
      }

      ~subs_fixer() {
        if (reconnect_count != mss.master->reconnect_count())
          mss.repair_subs(); // errors detected, fixup subs
      }
    };
  }

  master_slave_sync::master_slave_sync(
    server *slf,
    string ip,
    int port,
    double master_poll_time,
    size_t unsubscribe_min_threshold,
    double unsubscribe_max_delay)
    : self(slf)
    , max_defer_update_time{unsubscribe_max_delay}
    , unsubscribe_minimum_count{unsubscribe_min_threshold}
    , master_poll_time(master_poll_time) {
    master = std::make_unique<client>(ip + ":" + std::to_string(port));
    subs_updated = utctime_now();
  }

  void master_slave_sync::repair_subs() {
    if (self->terminate) {
      return; // do not do anything if terminated...
    }
    std::scoped_lock _(mx_subs); // assume that we already have a lock on mx_master!
    // 1. create a read request for all the subs
    id_vector_t ts_ids;
    utcperiod p;
    for (auto const &sub : subs) {
      ts_ids.emplace_back(*sub.first);
      if (!p.valid()) { // first time, assign current read period
        p = sub.second->p;
      } else { // otherwise extend the read period to cover everything (could be more than initial requests)
        if (sub.second->p.start < p.start)
          p.start = sub.second->p.start;
        if (sub.second->p.end > p.end)
          p.end = sub.second->p.end;
      }
    }
    ts_vector_t tsv;
    bool succeeded = false;
    auto t_giveup = utctime_now() + from_seconds(60);
    do {
      try {
        // 2. read  them
        tsv = master->read(ts_ids, p, true, true).result; // read cache and subscribe
        succeeded = true;
      } catch (dlib::socket_error const &se) {
        return;
      } catch (...) {
      }
    } while (!self->terminate && !succeeded && utctime_now() < t_giveup);
    if (!succeeded)
      return; // or throw??
    // 3. re-add them to cache (ensure that cache is correct)
    self->add_to_cache(ts_ids, tsv);
    // 4. notify about change(can trigger re-evaluate, and we are happy to have it in cache)
    self->sm->notify_change(ts_ids);
  }

  static void add_to_cache_and_notify(server &s, std::vector<apoint_ts> &r) {
    if (r.size()) {
      id_vector_t ids;
      ids.reserve(r.size());
      ts_vector_t tsv;
      tsv.reserve(r.size());
      for (auto const &ts : r) {
        ids.emplace_back(ts.id());
        apoint_ts tmp(std::dynamic_pointer_cast<aref_ts const>(ts.ts)->rep);
        tsv.emplace_back(tmp);
      }
      s.add_to_cache(ids, tsv);
      s.sm->notify_change(ids);
    }
  }

  /** @brief the master slave sync worker thread
   * @details
   * Work in progress:
   * (1) We need to factor out some of the algos here to facilitate testing
   * (2) Parameterization needs to be external, with good defaults
   * (3)  .. possibly with some adaptive adjustment within limits ..
   */
  void master_slave_sync::worker() {

    server &s = *self;                     // just a shorthand while prototyping.
    utctime t_unsub_detected = no_utctime; // time when we detected that we could do some unsubscribe work.
    size_t ev_count = s.ts_cache.get_total_evicted_count() + s.sm->total_unsubscribe_count.load();
    while (!s.terminate) {
      std::this_thread::sleep_for(from_seconds(master_poll_time));
      if (s.terminate)
        break;
      // TASK #1 Fetch subscription changes from the master
      {
        std::scoped_lock _(mx_subs);
        if (!subs.size()) {
          subs_updated = utctime_now();
          continue; // if nothing subscribed, just sleep
        }
        if (utctime_now() - subs_updated < from_seconds(master_poll_time)) {
          continue;
        }
      }
      bool subs_failed = false;
      try {
        std::scoped_lock _m(mx_master);
        auto reconnect_count = master->reconnect_count(); // we need to keep track of possibly lost comm.
        auto r = master->read_subscription();
        if (reconnect_count != master->reconnect_count()) { // bad luck, we lost connection and subs
          repair_subs();                                    // we do a repair_subs, that takes care of notify
          continue;                                         // and continue to do the master-sync tasks on next wakeup
        }
        add_to_cache_and_notify(s, r);
      } catch (...) {
        // what can we do.. at least we can try a repair subs.
        subs_failed = true;
      }
      if (subs_failed) {
        repair_subs();
        continue;
      }
      if (s.terminate)
        break;
      // ok, if we get here, system is working as normal

      // TASK #2 Minimize the subs to what is really used.

      // STEP #0 . check for events/hints that could lead to less subscriptions
      // these are
      // (1)  s.sm subs unsubscribed
      // (2)  s.cache.evicted events
      auto t_now = utctime_now();
      auto new_ev_count = s.ts_cache.get_total_evicted_count() + s.sm->total_unsubscribe_count.load();
      if (t_unsub_detected == no_utctime && new_ev_count > 0)
        t_unsub_detected = t_now; // record time when we detected that we should to unsub work...

      // we do unsub, if there are at least unsub_minimum_count, OR we have deferred something small more than
      // max_defer_update_time
      if (
        new_ev_count > ev_count + unsubscribe_minimum_count
        || (t_unsub_detected != no_utctime && (t_now - t_unsub_detected) > from_seconds(max_defer_update_time))) {
        size_t subs_to_remove_estimate = new_ev_count - ev_count;

        constexpr auto missing = [](auto &&key, auto &&container) noexcept -> bool { // candy for the search algo below
          return container.find(key) == std::end(container);
        };

        // STEP #1 figure out which subs  we can drop
        std::scoped_lock _(
          mx_subs, mx_master); // aquire both of them while doing updates, stopping other threads doing work.

        // compare current_required_set = s.sm.subs and s.cache.entries.
        // agains the current subs
        auto c_keys = s.ts_cache.get_keys(); // get the cache keys (cache is still open...)
        c_string_hash c_hkeys;
        c_hkeys.reserve(c_keys.size());
        for (auto &&e : c_keys)
          c_hkeys.insert(&e); // hash the cache keys, using the const ptr to string(avoid copy)

        id_vector_t keys_to_remove;
        keys_to_remove.reserve(subs_to_remove_estimate); // a good guess

        /* a local closed scope with lock of server subscription manager here, ..  we want to keep short! */ {
          std::scoped_lock sm_lock(s.sm->mx); // we need to lock it while comparing keys to remove
          for (auto &&e :
               subs) // iterate over the current subs terminals..(notice we avoid copy strings, only compare!)
            if (missing(e.first, c_hkeys) && missing(e.first, s.sm->active)) // if missing in cache and sm subs
              keys_to_remove.emplace_back(*e.first);                         /// then its a candidate for us to remove
        }

        // STEP #2 Just remove them from local and remote list of subs
        master->unsubscribe(keys_to_remove); // and from the server.
        for (auto const &e : keys_to_remove)
          subs.erase(
            &e); // remove them from the local list(maybe sep thread for this, so it can run in parallell with io
        // locks are released as we go out of scope
        t_unsub_detected = no_utctime; // clear out detection timer
        ev_count = new_ev_count;       // reset water-mark
        subs_updated = utctime_now();  // also mark time when we updated it
      }
    }
  }

  std::vector<apoint_ts>
    master_slave_sync::read(id_vector_t const &ts_urls, utcperiod p, bool use_ts_cached_read, bool subscribe) {
    msync_read_result r;
    /*mimimize the lock scope to the io operation */ {
      std::scoped_lock _(mx_master);
      subs_fixer _sf(*this);
      r = master->read(ts_urls, p, use_ts_cached_read, subscribe);
      if (subscribe)
        add_subs(ts_urls, p);
    }
    // handle the updates piggyback, that contains changes in the subscribed time-series.
    {
      std::scoped_lock __(mx_subs);
      subs_updated = utctime_now();
    } // update timestamp so that worker thread can skip its update cycle.
    add_to_cache_and_notify(*self, r.updates);
    return r.result;
  }

  ts_info_vector_t master_slave_sync::find(std::string const &search_expression) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    return master->find(search_expression);
  }

  void master_slave_sync::store_ts(ts_vector_t const &tsv, bool overwrite_on_write, bool cache_on_write) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    return master->store_ts(tsv, overwrite_on_write, cache_on_write);
  }

  void master_slave_sync::merge_store_ts(ts_vector_t const &tsv, bool cache_on_write) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    return master->merge_store_ts(tsv, cache_on_write);
  }

  ts_info master_slave_sync::get_ts_info(std::string const &ts_url) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    return master->get_ts_info(ts_url);
  }

  void master_slave_sync::remove(std::string const &name) {
    std::scoped_lock _(mx_subs, mx_master);
    subs_fixer _sf(*this);
    // first remove from cache an subs locally
    id_vector_t to_remove{name};
    self->remove_from_cache(to_remove);
    // TODO: forced remove from self->sm ?;
    auto f = subs.find(&name);
    if (f != subs.end()) {
      subs.erase(f);
    }
    return master->remove(name);
  }

  bool master_slave_sync::has_subscription(std::string_view container_url) {
    std::scoped_lock _(mx_subs); // assume that we already have a lock on mx_master!
    for (auto const &sub : subs) {
      std::string_view sub_url = *sub.first;
      if (sub_url.starts_with(container_url)) {
        return true;
      }
    }
    return false;
  }

  //-- geo redirected stuff, just to support every aspect

  geo::geo_ts_matrix master_slave_sync::geo_evaluate(geo::eval_args const &ea, bool use_cache, bool update_cache) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    return master->geo_evaluate(ea, use_cache, update_cache);
  }

  void master_slave_sync::geo_store(
    string const &geo_db_name,
    geo::ts_matrix const &tsm,
    bool replace,
    bool update_cache) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    master->geo_store(geo_db_name, tsm, replace, update_cache);
  }

  vector<geo::ts_db_config_> master_slave_sync::get_geo_ts_db_info() {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    return master->get_geo_ts_db_info();
  }

  void master_slave_sync::add_geo_ts_db(geo::ts_db_config_ const &gdb) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    master->add_geo_ts_db(gdb);
  }

  void master_slave_sync::remove_geo_ts_db(string const &geo_db_name) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    master->remove_geo_ts_db(geo_db_name);
  }

  void
    master_slave_sync::set_container(std::string const &name, std::string const &path, std::string type, db_cfg cfg) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    master->set_container(name, path, type, cfg);
  }

  void master_slave_sync::remove_container(std::string const &container_url, bool remove_from_disk) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    if (has_subscription(container_url)) {
      throw std::runtime_error(fmt::format(
        "Trying to remove container {} that has active subscriptions on dtss master server", container_url));
    }
    master->remove_container(container_url, remove_from_disk);
  }

  void master_slave_sync::swap_container(std::string const &a, std::string const &b) {
    std::scoped_lock _(mx_master);
    subs_fixer _sf(*this);
    master->swap_container(a, b); // should have zero semantic effect on cache if pre.cond. are ok.
  }

}
