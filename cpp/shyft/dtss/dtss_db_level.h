#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <shyft/dtss/ts_db_interface.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/db_cfg.h>
#include <shyft/dtss/db_deleter.h>

namespace shyft::dtss {

  namespace detail {
    struct ts_db_level_impl; // fwd. detailed impl.
  }

  /**
   * @brief The shyft backend ts store using google level db
   */
  struct ts_db_level : its_db {
    db_deleter delete_db_action; // defined before impl so it gets destroyed last
    std::unique_ptr<detail::ts_db_level_impl> impl;
    ts_db_level(std::string const & root_dir, db_cfg const & cfg);
    ~ts_db_level();

    std::string root_dir() const override;

    void save(
      std::string const & fn,
      gts_t const & ts,
      bool overwrite = true,
      queries_t const & queries = queries_t{},
      bool strict_alignment = true) override;
    void save(
      size_t n,
      fx_ts_item_t const & fx_item,
      bool overwrite,
      queries_t const & queries = queries_t{},
      bool strict_alignment = true) override;
    gts_t read(std::string const & fn, utcperiod p, queries_t const & queries = queries_t{}) override;
    void remove(std::string const & fn, queries_t const & queries = queries_t{}) override;
    ts_info get_ts_info(std::string const & fn, queries_t const & queries = queries_t{}) override;
    std::vector<ts_info> find(std::string const & match, queries_t const & queries = queries_t{}) override;
    static bool exists_at(std::string const & root_dir);

    void mark_for_deletion() override;
  };

}
