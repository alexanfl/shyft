#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <vector>
#include <cstdio>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss {
  using shyft::core::utctime;
  using std::vector;

  /** native-time io, using direct write/read utctime, that is micro seconds,
   * zero overhead.
   */
  struct native_time_io {
    static char version() {
      return '2';
    }

    static void write(FILE *fh, utctime const &t);
    static void write(FILE *fh, vector<utctime> const &t);
    static void read(FILE *fh, utctime &t);
    static void read(FILE *fh, vector<utctime> &t);
  };

  /** seconds-based -time io, needs conversion/to/from us/seconds
   * computational  overhead.
   */
  struct seconds_time_io {
    static char version() {
      return '1';
    }

    static void write(FILE *fh, utctime const &t_us);
    static void write(FILE *fh, vector<utctime> const &t_us);
    static void read(FILE *fh, utctime &t_us);
    static void read(FILE *fh, vector<utctime> &t_us);
  };
}
