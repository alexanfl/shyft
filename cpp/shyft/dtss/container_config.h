/** This file is part of Shyft. Copyright 2015-2023 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <map>
#include <shyft/core/core_serialization.h>

namespace shyft::dtss {
  /**
   * @brief dtss container_configuration
   * @details
   * The server needs to persist container container_configurations over restarts.
   * The minimal require information for that is the container-name path pairs.
   * The reason for this minimalism is that the container_configuration for the respective
   * containers (if any), are stored at the container root directory.
   *
   * We would like that information to reside outside the
   * container tree it self, to avoid any tampering with it.
   *
   * @note
   * If we later needs more information, then merely make a new version of the
   * container_configuration.
   *
   * Also if moving dtss to different servers, we need to provide means
   * of migrating/inspecting this container_configuration.
   *
   * Usual usage is to have a root directory for the dtss, and then organize
   * containers (one or more) under that root directory.
   *
   */
  struct container_config {
    std::map<std::string, std::string> container_cfg; ///< the container name and path map.
    auto operator<=>(container_config const &) const = default;

    static container_config read(std::string const & path);
    static void store(std::string const & path, container_config const & cfg);

    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::dtss::container_config);
