#pragma once
/** This file is part of Shyft. Copyright 2015-2019 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <vector>
#include <string>
#include <memory>
#include <unordered_map>
#include <atomic>
#include <shyft/core/subscription.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::subscription {
  using std::shared_ptr;
  using std::vector;
  using std::string;
  using std::end;
  using std::unordered_map;
  using std::array;
  using std::recursive_mutex;
  using std::scoped_lock;
  using std::begin;
  using std::end;
  using std::atomic_int64_t;

  using namespace shyft::core;
  using shyft::core::subscription::observer_base;
  using shyft::core::subscription::manager_;
  using shyft::core::subscription::observable_;
  using id_vector_t = vector<string>;

  /** @brief POD to keep  published version and period monitored as pair */
  struct ts_o {
    int64_t v{0}; ///< the version published
    utcperiod p;  ///< the period that we monitor for this specific time-series (grand union period)
    observable_
      o; ///< the shared_ptr to the dtss manager maintained observable, so the o->v is the observed change counter
  };

  /**
   * @brief Subscriptetd observer for  time-series, suitable for  dtss master-slave sync, master side of relation
   * @details
   * Given a subscription_manager (provided by the dtss, or as part of the dstm server ),
   * provide monitoring of changes in the time-series, so that
   * they can be propagated from the master dtss to the slave dtss (usually part of dstm).
   *
   * Typically, this is used in the web-api (part of web-socket context)
   * to keep track of changes to expression/time-series.
   *
   */
  struct ts_observer : observer_base {
    //-- needed internal stuff to ensure subscription logic works
    // Maps terminal node and version id
    unordered_map<std::string, ts_o> published_version; ///< since we have a strong promise to only send if it is
                                                        ///< changed we need to keep the last values

    ts_observer(manager_ const & sm, string const & request_id);
    ~ts_observer();

    /**
     * @brief add/extend subscriptions for  specified ts_ids  for specified period
     * @details
     * If a single item is already on subscription, extend the period with the new one
     * otherwise, insert it into the subscription manager list, set published version
     * to the initial version for that specific ts
     */
    void subscribe_to(id_vector_t const & ts_ids, utcperiod const & read_period);

    /**
     * @brief remove subscriptions for the specified ts_ids
     * @details
     * The supplied ts-ids are removed from the subscription manager list
     * if it not monitored by any other observer. It is removed from the
     * local published_version list regardless.
     * If item not found, it is a noop (no exception).
     */
    void unsubscribe(id_vector_t const & ts_ids);

    /** @brief recalculate definition, that in this case is a noop */
    bool recalculate() override;

    /**
     * @brief find changed ts_ids that are changed since last check
     * @details
     * Searches through the published_version  list, and compares
     * the published version against the terminal version (as managed by the subscription manager).
     * If it find an item changed, then it is inserted into the unordered_map
     * into individual lists for each of the periods that is changed.
     *  -AND- its published version is set to the current terminal version.
     * The idea is to produce a set of lists suitable for reading, so that
     * the read results can be propagated to the slave dtss in a msync relation.
     * A second call to find_changed_ts() would return an empty list.
     * The cost of the call is quite high, since it needs to loop through the
     * hashed set of time-series. For an empty case, no allocs occur.
     */
    unordered_map<utcperiod, id_vector_t, utcperiod_hasher> find_changed_ts();
  };

  using ts_observer_ = shared_ptr<ts_observer>; ///< short hand for shared_ptr to

}
