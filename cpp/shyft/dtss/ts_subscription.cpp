#include <shyft/dtss/ts_subscription.h>

namespace shyft::dtss::subscription {

  ts_observer::ts_observer(manager_ const & sm, string const & request_id)
    : observer_base(sm, request_id) {
  }

  ts_observer::~ts_observer() {
    // ok,the base class takes care of the manager subs part
    // protected by recursive mutex.
    // We only need to care about our internals
    published_version.clear(); // not really needed, it goes by auto.
  }

  void ts_observer::subscribe_to(id_vector_t const & ts_ids, utcperiod const & read_period) {
    auto t = sm->add_subscriptions(ts_ids);
    for (auto const & i : t) {
      if (auto f = published_version.find(i->id); f == published_version.end()) {
        // New entry, set version and period
        published_version[i->id] = ts_o{i->v, read_period, i};
        terminals.push_back(i);
      } else {
        // Extend current period on terminal
        utcperiod& p = f->second.p;
        p = utcperiod(std::min(p.start, read_period.start), std::max(p.end, read_period.end));
      }
    }
  }

  void ts_observer::unsubscribe(id_vector_t const & ts_ids) {
    vector<core::subscription::observable_> e;
    for (auto const & id : ts_ids) {
      if (auto f = published_version.find(id); f != published_version.end()) {
        auto ri = std::find_if(terminals.begin(), terminals.end(), [&id](auto const & x) {
          return id == x->id;
        });

        if (ri != terminals.end()) {
          e.emplace_back(*ri);
          terminals.erase(ri);
        }
        published_version.erase(f);
      }
    }
    sm->remove_subscriptions(e);
  }

  bool ts_observer::recalculate() {
    return false;
  }

  unordered_map<utcperiod, id_vector_t, utcperiod_hasher> ts_observer::find_changed_ts() {

    unordered_map<utcperiod, id_vector_t, utcperiod_hasher> r;
    for (auto& p : published_version) {
      if (p.second.v != p.second.o->v) {
        r[p.second.p].emplace_back(p.second.o->id); // TODO: First time emplace, be sure to resize id_vector_t .. to
                                                    // lets say size(published_version)? worst case?
        p.second.v = p.second.o->v;
      }
    }
    return r;
  }

}
