#include <shyft/dtss/dtss_krls.h>
#include <shyft/dtss/detail/krls_db.h>

namespace shyft::dtss {

  using gts_t = krls_pred_db::gts_t;

  krls_pred_db::krls_pred_db()
    : impl{new krls_pred_db_impl()} {
  }

  krls_pred_db::krls_pred_db(std::string const &root_dir, cb_fx cb)
    : impl(new krls_pred_db_impl(root_dir, cb)) {
  }

  krls_pred_db::~krls_pred_db() {
  }

  void krls_pred_db::save(std::string const &fn, gts_t const &ts, bool overwrite, queries_t const &queries,bool ) {
    impl->save(fn, ts, overwrite, queries);
  }

  void krls_pred_db::save(size_t n, fx_ts_item_t const &fx_item, bool overwrite, queries_t const &queries,bool ) {
    for (size_t i = 0; i < n; ++i) {
      auto [fn, ts] = fx_item(i);
      impl->save(fn, ts, overwrite, queries);
    }
  }

  gts_t krls_pred_db::read(std::string const &fn, core::utcperiod period, queries_t const &queries) {
    return impl->read(fn, period, queries);
  }

  void krls_pred_db::remove(std::string const &fn, queries_t const &) {
    return impl->remove(fn);
  }

  ts_info krls_pred_db::get_ts_info(std::string const &fn, queries_t const &) {
    return impl->get_ts_info(fn);
  }

  std::vector<ts_info> krls_pred_db::find(std::string const &match, queries_t const &) {
    return impl->find(match);
  }

  /* KRLS container API
   * ==================== */

  void krls_pred_db::register_rbf_series(
    std::string const &fn,
    std::string const &source_url, // series filename and source url
    core::utcperiod const &period, // period to train
    const core::utctimespan dt,
    const ts::ts_point_fx point_fx,
    const std::size_t dict_size,
    double const tolerance, // general parameters
    double const gamma      // rbf kernel parameters
  ) {
    impl->register_rbf_series(fn, source_url, period, dt, point_fx, dict_size, tolerance, gamma);
  }

  void krls_pred_db::update_rbf_series(
    std::string const &fn,         // series filename
    core::utcperiod const &period, // period to train
    bool const allow_gap_periods) {
    impl->update_rbf_series(fn, period, allow_gap_periods);
  }

  void krls_pred_db::move_predictor(std::string const &from_fn, std::string const &to_fn, bool const overwrite) {
    impl->move_predictor(from_fn, to_fn, overwrite);
  }

  gts_t krls_pred_db::predict_time_series(std::string const &fn, gta_t const &ta) {
    return impl->predict_time_series(fn, ta);
  }

  void krls_pred_db::mark_for_deletion() {
    delete_db_action.arm(impl->root_dir);
  }

}
