/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <functional>
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/ref.h> //needed for the bind operations
#include <shyft/time_series/time_shift.h>
#include <shyft/time_series/average.h>
#include <shyft/time_series/accumulate.h>
#include <shyft/time_series/convolve_w.h>
#include <shyft/time_series/glacier_melt.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::forward;
  using std::runtime_error;
  using std::string;
  using std::runtime_error;
  using std::isfinite;
  using std::enable_if;
  using std::enable_if_t;
  using std::out_of_range;
  using std::forward;
  using std::move;
  using std::plus;
  using std::divides;
  using std::minus;
  using std::multiplies;
  using std::is_floating_point;

  template <class A>
  constexpr void dbind_ts(A&) {
  }

  /** @brief Basic math operators
   *
   * Here we take a very direct approach, just create a bin_op object for
   * each bin_op :
   *   -# specialize for double vs. ts
   *   -# use perfect forwarding of the arguments, using d_ref to keep shared_ptr vs. object the same
   *
   */
  template <class A, class B, class O, class TA>
  struct bin_op {
    typedef TA ta_t;
    O op;
    A lhs;
    B rhs;
    TA ta;
    bool bind_done = false;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;
    bin_op() = default;

    template <class A_, class B_>
    bin_op(A_&& lhsx, O op, B_&& rhsx)
      : op(op)
      , lhs(std::forward<A_>(lhsx))
      , rhs(std::forward<B_>(rhsx)) {
      //-- if lhs and rhs allows us (i.e) !needs_bind, we do the binding now
      if (
        !(needs_bind<typename d_ref_t<A>::type >::value && e_needs_bind(d_ref(lhs)))
        && !(needs_bind<typename d_ref_t<B>::type >::value && e_needs_bind(d_ref(rhs))))
        do_bind();
    }

    void ensure_bound() const {
      if (!bind_done)
        throw runtime_error("bin_op: access to not yet bound attempted");
    }

    void do_bind() {
      if (!bind_done) {
        dbind_ts(d_ref(lhs));
        dbind_ts(d_ref(rhs));
        ta = time_axis::combine(d_ref(lhs).time_axis(), d_ref(rhs).time_axis());
        fx_policy = result_policy(d_ref(lhs).point_interpretation(), d_ref(rhs).point_interpretation());
        bind_done = true;
      }
    }

    const TA& time_axis() const {
      ensure_bound();
      return ta;
    }

    ts_point_fx point_interpretation() const {
      ensure_bound();
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      fx_policy = point_interpretation;
    }

    inline double value_at(utctime t) const {
      return op(d_ref(lhs)(t), d_ref(rhs)(t));
    }

    double operator()(utctime t) const {
      if (!time_axis().total_period().contains(t))
        return nan;
      return value_at(t);
    }

    double value(size_t i) const {
      if (i == string::npos || i >= time_axis().size())
        return nan;
      return value_at(ta.time(i));
    }

    point get(size_t i) const {
      return point{ta.time(i), value(i)};
    }

    size_t index_of(utctime t) const {
      return time_axis().index_of(t);
    }

    size_t size() const {
      ensure_bound();
      return ta.size();
    }

    x_serialize_decl();
  };

  template <class A, class B, class O, class TA>
  void dbind_ts(bin_op<A, B, O, TA>&& ts) {
    std::forward<bin_op<A, B, O, TA>>(ts).do_bind();
  }

  /** specialize for double bin_op ts */
  template <class B, class O, class TA>
  struct bin_op<double, B, O, TA> {
    typedef TA ta_t;
    double lhs;
    B rhs;
    O op;
    TA ta;
    bool bind_done = false;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;
    bin_op() = default;

    template <class A_, class B_>
    bin_op(A_&& lhsx, O op, B_&& rhsx)
      : lhs(std::forward<A_>(lhsx))
      , rhs(std::forward<B_>(rhsx))
      , op(op) {
      //-- if lhs and rhs allows us (i.e) !needs_bind, we do the binding now
      if (!(needs_bind<typename d_ref_t<B>::type >::value && e_needs_bind(d_ref(rhs))))
        do_bind();
    }

    void do_bind() {
      if (!bind_done) {
        dbind_ts(d_ref(rhs));
        ta = d_ref(rhs).time_axis();
        fx_policy = d_ref(rhs).point_interpretation();
        bind_done = true;
      }
    }

    void ensure_bound() const {
      if (!bind_done)
        throw runtime_error("bin_op: access to not yet bound attempted");
    }

    const TA& time_axis() const {
      ensure_bound();
      return ta;
    }

    ts_point_fx point_interpretation() const {
      ensure_bound();
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      ensure_bound(); // ensure it's done
      fx_policy = point_interpretation;
    }

    double operator()(utctime t) const {
      return op(lhs, d_ref(rhs)(t));
    }

    double value(size_t i) const {
      return op(lhs, d_ref(rhs).value(i));
    }

    point get(size_t i) const {
      return point{ta.time(i), value(i)};
    }

    size_t index_of(utctime t) const {
      return time_axis().index_of(t);
    }

    size_t size() const {
      ensure_bound();
      return ta.size();
    }

    x_serialize_decl();
  };

  /** specialize for ts bin_op double */
  template <class A, class O, class TA>
  struct bin_op<A, double, O, TA> {
    typedef TA ta_t;
    A lhs;
    double rhs;
    O op;
    TA ta;
    bool bind_done = false;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;
    bin_op() = default;

    template <class A_, class B_>
    bin_op(A_&& lhsx, O op, B_&& rhsx)
      : lhs(std::forward<A_>(lhsx))
      , rhs(std::forward<B_>(rhsx))
      , op(op) {
      //-- if hs allows us (i.e) !needs_bind, we do the binding now
      if (!(needs_bind<typename d_ref_t<A>::type >::value && e_needs_bind(d_ref(lhs))))
        do_bind();
    }

    void do_bind() {
      if (!bind_done) {
        dbind_ts(d_ref(lhs));
        ta = d_ref(lhs).time_axis();
        fx_policy = d_ref(lhs).point_interpretation();
        bind_done = true;
      }
    }

    void ensure_bound() const {
      if (!bind_done)
        throw runtime_error("bin_op: access to not yet bound attempted");
    }

    const TA& time_axis() const {
      ensure_bound();
      return ta;
    }

    ts_point_fx point_interpretation() const {
      ensure_bound();
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      ensure_bound(); // ensure it's done
      fx_policy = point_interpretation;
    }

    double operator()(utctime t) const {
      return op(d_ref(lhs)(t), rhs);
    }

    double value(size_t i) const {
      return op(d_ref(lhs).value(i), rhs);
    }

    point get(size_t i) const {
      return point{ta.time(i), value(i)};
    }

    size_t index_of(utctime t) const {
      return time_axis().index_of(t);
    }

    size_t size() const {
      ensure_bound();
      return ta.size();
    }

    x_serialize_decl();
  };

  /** @brief op_axis is about deriving the time-axis of a result
   *
   * When doing binary-ts operations, we need to deduce at run-time what will be the
   *  most efficient time-axis type of the binary result
   * We use time_axis::combine_type<..> if we have two time-series
   * otherwise we specialize for double binop ts, and ts binop double.
   * @sa shyft::time_axis::combine_type
   */
  template <typename L, typename R>
  struct op_axis {
    typedef
      typename time_axis::combine_type< typename d_ref_t<L>::type::ta_t, typename d_ref_t<R>::type::ta_t>::type type;
  };

  template <typename R>
  struct op_axis<double, R> {
    typedef typename d_ref_t<R>::type::ta_t type;
  };

  template <typename L>
  struct op_axis<L, double> {
    typedef typename d_ref_t<L>::type::ta_t type;
  };

  // --------------------

  /** The template is_ts<T> is used to enable operator overloading +-/  etc to time-series only.
   * otherwise the operators will interfere with other libraries doing the same.
   */
  // This is to allow this ts to participate in ts-math expressions

  template <class A, class B, class O, class TA>
  struct is_ts< bin_op<A, B, O, TA> > {
    static bool const value = true;
  };

  /** time_shift function, to ease syntax and usability */

  struct op_max {
    double operator()(double const & a, double const & b) const {
      return std::max(a, b);
    }
  };

  struct op_min {
    double operator()(double const & a, double const & b) const {
      return std::min(a, b);
    }
  };

  struct op_log {
    double operator()(double const a, double const) const {
      return std::log(a);
    }
  };

  template <
    class A,
    class B,
    typename = enable_if_t<
      (is_ts<A>::value && (is_floating_point<B>::value || is_ts<B>::value))
      || (is_ts<B>::value && (is_floating_point<A>::value || is_ts<A>::value)) > >

  auto operator+(A const & lhs, B const & rhs) {
    return bin_op<A, B, plus<double>, typename op_axis<A, B>::type>(lhs, plus<double>(), rhs);
  }

  template <
    class A,
    class B,
    typename = enable_if_t<
      (is_ts<A>::value && (is_floating_point<B>::value || is_ts<B>::value))
      || (is_ts<B>::value && (is_floating_point<A>::value || is_ts<A>::value)) > >
  auto operator-(A const & lhs, B const & rhs) {
    return bin_op<A, B, minus<double>, typename op_axis<A, B>::type>(lhs, minus<double>(), rhs);
  }

  /** unary minus implemented as -1.0* ts */
  template <class A, typename = enable_if_t< is_ts<A>::value >>
  auto operator-(A const & lhs) {
    return bin_op<A, double, multiplies<double>, typename op_axis<A, double>::type>(lhs, multiplies<double>(), -1.0);
  }

  template <
    class A,
    class B,
    typename = enable_if_t<
      (is_ts<A>::value && (is_floating_point<B>::value || is_ts<B>::value))
      || (is_ts<B>::value && (is_floating_point<A>::value || is_ts<A>::value)) > >
  auto operator*(A const & lhs, B const & rhs) {
    return bin_op<A, B, multiplies<double>, typename op_axis<A, B>::type>(lhs, multiplies<double>(), rhs);
  }

  template <
    class A,
    class B,
    typename = enable_if_t<
      (is_ts<A>::value && (is_floating_point<B>::value || is_ts<B>::value))
      || (is_ts<B>::value && (is_floating_point<A>::value || is_ts<A>::value)) > >
  auto operator/(A const & lhs, B const & rhs) {
    return bin_op<A, B, divides<double>, typename op_axis<A, B>::type>(lhs, divides<double>(), rhs);
  }

  template <
    class A,
    class B,
    typename = enable_if_t<
      (is_ts<A>::value && (is_floating_point<B>::value || is_ts<B>::value))
      || (is_ts<B>::value && (is_floating_point<A>::value || is_ts<A>::value)) > >
  auto max(A const & lhs, B const & rhs) {
    return bin_op<A, B, op_max, typename op_axis<A, B>::type>(lhs, op_max(), rhs);
  }

  template <
    class A,
    class B,
    typename = enable_if_t<
      (is_ts<A>::value && (is_floating_point<B>::value || is_ts<B>::value))
      || (is_ts<B>::value && (is_floating_point<A>::value || is_ts<A>::value)) > >
  auto min(A const & lhs, B const & rhs) {
    return bin_op<A, B, op_min, typename op_axis<A, B>::type>(lhs, op_min(), rhs);
  }

  /** unary base e logarithm implemented as log(ts), the extra 1.0 arg is ignored but must be present to use the bin_op
   * mechanics */
  template <class A, typename = enable_if_t< is_ts<A>::value >>
  auto log(A const & lhs) {
    return bin_op<A, double, op_log, typename op_axis<A, double>::type>(lhs, op_log(), 1.0);
  }

  /** @brief bind ref_ts
   * @details
   * default impl. does nothing (nothing to bind)
   */
  template <class Ts, class Fbind>
  void bind_ref_ts(Ts&, Fbind&&) {
  }

  template <class Ts, class Fbind>
  void bind_ref_ts(ref_ts<Ts>& ts, Fbind&& f_bind) {
    f_bind(ts);
  }

  template <class A, class B, class O, class TA, class Fbind>
  void bind_ref_ts(bin_op<A, B, O, TA>& ts, Fbind&& f_bind) {
    bind_ref_ts(d_ref(ts.lhs), f_bind);
    bind_ref_ts(d_ref(ts.rhs), f_bind);
    ts.do_bind();
  }

  template <class B, class O, class TA, class Fbind>
  void bind_ref_ts(bin_op<double, B, O, TA>& ts, Fbind&& f_bind) {
    bind_ref_ts(d_ref(ts.rhs), f_bind);
    ts.do_bind();
  }

  template <class A, class O, class TA, class Fbind>
  void bind_ref_ts(bin_op<A, double, O, TA>& ts, Fbind&& f_bind) {
    bind_ref_ts(d_ref(ts.lhs), f_bind);
    ts.do_bind();
  }

  template <class Ts, class Fbind>
  void bind_ref_ts(time_shift_ts<Ts>& time_shift, Fbind&& f_bind) {
    bind_ref_ts(d_ref(time_shift.ts, f_bind));
    time_shift.do_bind();
  }

  template <class Ts, class Ta, class Fbind>
  void bind_ref_ts(average_ts<Ts, Ta> const & avg, Fbind&& f_bind) {
    bind_ref_ts(d_ref(avg.ts), f_bind);
  }

  template <class Ts, class Ta, class Fbind>
  void bind_ref_ts(accumulate_ts<Ts, Ta>& acc, Fbind&& f_bind) {
    bind_ref_ts(d_ref(acc.ts), f_bind);
  }

  template <class TS_A, class TS_B, class Fbind>
  void bind_ref_ts(glacier_melt_ts<TS_A, TS_B>& glacier_melt, Fbind&& f_bind) {
    bind_ref_ts(d_ref(glacier_melt.temperature), f_bind);
    bind_ref_ts(d_ref(glacier_melt.sca_m2), f_bind);
  }

  /** @brief e_needs_bind(A const&ts) specializations
   *
   * @details
   *  recall that e_needs_bind(..) is called at run-time to
   *  determine if a ts needs bind before use of any evaluation method.
   *
   *  point_ts<ta> is known to not need bind (so we have to override needs_bind->false
   *   the default value is true.
   *  all others that can be a part of an expression
   *  are candidates, and needs to be visited.
   * this includes
   *  ref_ts : first and foremost, this is the *one* that is the root cause for bind-stuff
   *  bin_op : obvious, the rhs and lhs could contain a  ref_ts
   *  average_ts .. and family : less obvious, but they refer a ts like bin-op
   */

  template <class Ta>
  struct needs_bind<point_ts<Ta>> {
    static bool const value = false;
  };

  // the ref_ts conditionally needs a bind, depending on if it has a ts or not
  template <class Ts>
  bool e_needs_bind(ref_ts<Ts> const & rts) {
    return rts.ts == nullptr;
  }

  // the bin_op conditionally needs a bind, depending on their siblings(lhs,rhs)
  template <class A, class B, class O, class TA>
  bool e_needs_bind(bin_op<A, B, O, TA> const & e) {
    return !e.bind_done;
  }

  template <class A, class B, class O, class TA>
  bool e_needs_bind(bin_op<A, B, O, TA>&& e) {
    return !e.bind_done;
  }

}
