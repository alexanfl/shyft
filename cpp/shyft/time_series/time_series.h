/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <cstdint>
#include <cmath>
#include <string>
#include <stdexcept>
#include <vector>
#include <memory>
#include <utility>
#include <stdexcept>
#include <type_traits>
#include <algorithm>
#include <shyft/core/core_serialization.h>
#include <shyft/core/unit_conversion.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/accessor.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/time_shift.h>
#include <shyft/time_series/average.h>
#include <shyft/time_series/accumulate.h>
#include <shyft/time_series/periodic.h>
#include <shyft/time_series/glacier_melt.h>
#include <shyft/time_series/ref.h>
#include <shyft/time_series/convolve_w.h>
#include <shyft/time_series/uniform_sum.h>
#include <shyft/time_series/bin_op.h>
#include <shyft/time_series/rating_curve.h>
#include <shyft/time_series/ice_packing.h>
#include <shyft/time_series/goal_function.h>
#include <shyft/time_series/stack_ts.h>
