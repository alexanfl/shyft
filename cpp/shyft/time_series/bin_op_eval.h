/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <vector>
#include <utility>
#include <type_traits>
#include <cmath>
#include <shyft/core/math_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::time_series {

  using std::vector;
  using std::isfinite;
  using std::is_same;
  using std::enable_if;

  using shyft::nan;
  using gta_t = time_axis::generic_dt;

  namespace detail {

    namespace { // We need to use the checked fast _index_of() and _time_of()

      inline size_t ix_of(time_axis::fixed_dt const &ta, utctime t, size_t) noexcept {
        return ta._index_of(t);
      }

      inline size_t ix_of(time_axis::calendar_dt const &ta, utctime t, size_t) noexcept {
        return ta._index_of(t);
      }

      inline size_t ix_of(time_axis::point_dt const &ta, utctime t, size_t) noexcept {
        return ta.index_of(t);
      }

      // inline size_t ix_of(time_axis::generic_dt const&ta,utctime t, size_t i) noexcept {return ta.index_of(t,i);}
      inline utctime time_of(time_axis::fixed_dt const &ta, size_t i) noexcept {
        return ta._time(i);
      }

      inline utctime time_of(time_axis::calendar_dt const &ta, size_t i) noexcept {
        return ta._time(i);
      }

      inline utctime time_of(time_axis::point_dt const &ta, size_t i) noexcept {
        return ta._time(i);
      }

      // inline utctime time_of(time_axis::generic_dt const&ta, size_t i) noexcept {return ta.time(i);}
    }

    /** @brief f(t) for stair-case start of step type
     *
     * This is a verfy specialized wrapper to support the fxx_bin_op algorithm,
     * so that we can separate the task of optimized access to time-series
     * from the simple bin-op algorithm it self.
     */
    template <class TS, class TA>
    class fxx_step {
     protected:
      TS const &ts;              ///< the ts-type it self, we use it to pick values from
      TA const &ta;              ///< optimized(resolved to underlying type) ta for ts
      const size_t sz;           ///< local cached ta.size()
      utcperiod const p;         ///< local cached ts.total_period()
      size_t i_end{0};           ///< pos of next step
      utctime t_end{no_utctime}; ///< current period end, vx is valid up to this time-point

      double vx{nan}; ///< current value
     public:
      fxx_step(TS const &ts, TA const &ta)
        : ts{ts}
        , ta{ta}
        , sz{ta.size()}
        , p{ta.total_period()}
        , i_end{0} {
      }

      fxx_step() = delete;

      /** intializes the the f(t) fwd 'iterator' state with t */
      void init(utctime t) noexcept {
        if (t < p.start || t >= p.end) {
          vx = nan;
          t_end = max_utctime;
          i_end = sz;
          return;
        }
        // t is within the period of ts/ta etc
        i_end = ix_of(ta, t, i_end);                     // this is should always result in a valid ix, ref. test above
        vx = ts.value(i_end++);                          // get current step-value and advance
        t_end = i_end < sz ? time_of(ta, i_end) : p.end; // set the sentinel t_end
      }

      /** compute fxx(t), forward internal iterator state if needed */
      double operator()(utctime t) noexcept {
        if (t < t_end) // we are still at current step.
          return vx;
        if (i_end < sz) {         // advance to next value(if any)
          vx = ts.value(i_end++); // get new step-value, and advance
          t_end = i_end < sz ? time_of(ta, i_end) : p.end;
        } else { // remaining calls will get nan
          t_end = max_utctime;
          vx = nan;
        }
        return vx;
      }
    };

    template <typename TS, typename TA>
    fxx_step(TS, TA) -> fxx_step<TS, TA>; // add class template constructor deduction rule (optional)

    /** @brief f(t) for linear between points accessor
     *
     * This is a verfy specialized wrapper to support the fxx_bin_op algorithm,
     * so that we can separate the task of optimized access to time-series
     * from the simple bin-op algorithm it self.
     */
    template <class TS, class TA>
    class fxx_lin {
     protected:
      TS const &ts;              ///< the ts-type it self, we use it to pick values from
      TA const &ta;              ///< optimized(resolved to underlying type) ta for ts
      const size_t sz;           ///< local cached ta.size()
      utcperiod const p;         ///< local cached ts.total_period()
      size_t i_end{0};           ///< pos of next step
      utctime t_end{no_utctime}; ///< current period end, vx is valid up to this time-point
      double a{1.0};             ///< value(t) t*ax+bx
      double b{0.0};             ///< value(t) t*ax+bx

      inline void ax_b(utctime t1, double v1, utctime t2, double v2) noexcept {
        a = (v2 - v1) / to_seconds(t2 - t1);
        b = v1 - a * to_seconds(t1);
        if (!std::isfinite(v2)) { // flat out to next nan, i think this the current wanted behaviour
          b = v1;
          a = 0.0;
        }
      }

     public:
      fxx_lin(TS const &ts, TA const &ta)
        : ts{ts}
        , ta{ta}
        , sz{ta.size()}
        , p{ta.total_period()}
        , i_end{0} {
      }

      /** intializes the the f(t) fwd 'iterator' state with t */
      void init(utctime t) noexcept {
        if (t < p.start || t >= p.end) {
          b = nan;
          t_end = max_utctime;
          i_end = sz;
          return;
        }
        // t is within the period of ts/ta etc
        i_end = ix_of(ta, t, i_end);    // this is should always result in a valid ix, ref. test above
        auto v1 = ts.value(i_end);      // get current step-value
        auto t1 = time_of(ta, i_end++); // get point and advance.
        if (i_end < sz) {
          auto v2 = ts.value(i_end);
          auto t2 = time_of(ta, i_end);
          t_end = t2; // set the sentinel t_end
          ax_b(t1, v1, t2, v2);
        } else {
          b = v1;
          a = 0.0; // flat out to end of ts, then nan.
          t_end = p.end;
        }
      }

      /** compute fxx(t), forward internal iterator state if needed */
      double operator()(utctime t) noexcept {
        if (t < t_end) // we are still at current step
          return a * to_seconds(t) + b;
        if (i_end < sz) {              // advance to next value(if any)
          auto v1 = ts.value(i_end);   // have to pick value, here second time..hmm, maybe cache v2
          auto t1 = t_end;             // we can re-use t_end.
          if (++i_end < sz) {          // is there a next value ?
            auto v2 = ts.value(i_end); // then still linear-interpolation
            auto t2 = time_of(ta, i_end);
            t_end = t2;
            ax_b(t1, v1, t2, v2);
            return a * to_seconds(t) + b;
          } else {  // this was the last value in linear-ts. now.. we extend flat out to endof time-axis
            b = v1; // v1 could be nan here, that'sok
            a = 0.0;
            t_end = p.end;
            return v1;
          }
        } else { // remaining calls will get nan
          t_end = max_utctime;
          b = nan;
          a = 0.0;
        }
        return nan;
      }
    };

    template <typename TS, typename TA>
    fxx_lin(TS, TA) -> fxx_lin<TS, TA>; // add class template constructor deduction rule (optional)

    /** @brief fxx_bin_op efficient bin-op over common ta
     *
     *
     * Extract values from a bin-op using zip-traversal of time-axis with ix-hints
     * so that we avoid bsearch for point-type of time-axis.
     *
     * E.g.:
     *    lhs bin_op rhs
     *  where
     *    ta= combine(lhs.time_axis(),rhs.time_axis())
     *
     * The ts-accessors are there to ensure that we can access point-dt time-axis
     * in a efficient manner(at max one lookup on index_of) using index-hint,
     * and access-pattern that always goes in forward direction.
     *
     *
     * @note: IMPORANT: we assume that o.ta points are part of o.a.time_axis or o.b.time_axis
     *       so we know that for each step in the loop below
     *       ta or tb or both are equal to t.
     *       This is currently guaranteed using the time_axis::combine algorithm
     *
     * @tparam TA type of time-axis, the combined type time-axis of the two time-series
     * @tparam TSA1 time-series accessor for lhs side of the operation
     * @tparam TSA2 time-series accessor for rhs side of the operation
     * @see fxx_lin, fxx_step
     *
     * @param cta the combined time-axis
     * @param a the lhs side of the bin-op, notice that this is a ref/rvalue, and we modify it
     * @param op a callable operation, like std::plus etc.
     * @param b the rhs of the bin-op, notice that this is a ref/rvalue, and we modify it
     * @return the evaluated vector of the bin-op, will have the size of cta.
     */
    template <class TA, class TSA1, class TSA2, class OP>
    vector<double> fxx_bin_op(TA const &cta, TSA1 &&a, OP &&op, TSA2 &&b) {
      vector<double> r;
      if (cta.size() == 0) // trivial result
        return r;
      r.reserve(cta.size()); // avoid re-alloc

      auto t = cta.time(0); // t =time ta= time of a, tb= time of b oetc.
      a.init(t);
      b.init(t);
      for (size_t i = 0; i < cta.size(); ++i) {
        t = cta.time(i);
        r.emplace_back(op(a(t), b(t)));
      }
      return r;
    }

    /** unwrap combined result-timeaxis and fwd to fx_bin_op */
    template <typename... Args>
    inline vector<double> bin_op_resolve_result(gta_t const &c, Args &&...args) {
      if (c.gt() == gta_t::FIXED)
        return fxx_bin_op(c.f(), std::forward<Args>(args)...);
      if (c.gt() == gta_t::CALENDAR)
        return c.c().is_simple() ? fxx_bin_op(c.c().simplify(), std::forward<Args>(args)...)
                                 : fxx_bin_op(c.c(), std::forward<Args>(args)...);
      return fxx_bin_op(c.p(), std::forward<Args>(args)...);
    }

    /** for result-time-axis of know-type, just forward to fx_bin_op */
    template <typename TA, typename... Args>
    inline vector<double> bin_op_resolve_result(TA const &c, Args &&...args) {
      return fxx_bin_op(c, std::forward<Args>(args)...);
    }

    /** resolve step linear/stair-case permutation, and fwd. to bin_resolve_result */
    template < typename CTA, typename ATA, typename BTA, typename A, typename OP, typename B >
    inline vector<double>
      bin_op_resolve_step(CTA const &c, ATA const &a_ta, BTA const &b_ta, A const &a, OP &&o, B const &b) {
      if (a.point_interpretation() == POINT_AVERAGE_VALUE && b.point_interpretation() == POINT_AVERAGE_VALUE)
        return bin_op_resolve_result(c, fxx_step(a, a_ta), o, fxx_step(b, b_ta));
      if (a.point_interpretation() == POINT_AVERAGE_VALUE && b.point_interpretation() == POINT_INSTANT_VALUE)
        return bin_op_resolve_result(c, fxx_step(a, a_ta), o, fxx_lin(b, b_ta));
      if (a.point_interpretation() == POINT_INSTANT_VALUE && b.point_interpretation() == POINT_AVERAGE_VALUE)
        return bin_op_resolve_result(c, fxx_lin(a, a_ta), o, fxx_step(b, b_ta));
      return bin_op_resolve_result(c, fxx_lin(a, a_ta), o, fxx_lin(b, b_ta));
    }

    /** resolve b (rhs) argument time-axis type in case it is a generic_dt, and fwd to bin_op_resolve_step */
    template <
      typename CTA,
      typename ATA,
      typename A,
      typename OP,
      typename B,
      typename enable_if<is_same<gta_t, typename B::ta_t>::value, int>::type = 0>
    inline vector<double> bin_op_resolve_b(CTA const &c, ATA const &a_ta, A const &a, OP &&o, B const &b) {
      if (b.time_axis().gt() == gta_t::FIXED)
        return bin_op_resolve_step(c, a_ta, b.time_axis().f(), a, o, b);
      if (b.time_axis().gt() == gta_t::CALENDAR)
        return b.time_axis().c().is_simple()
               ? bin_op_resolve_step(c, a_ta, b.time_axis().c().simplify(), a, o, b)
               : bin_op_resolve_step(c, a_ta, b.time_axis().c(), a, o, b);
      return bin_op_resolve_step(c, a_ta, b.time_axis().p(), a, o, b);
    }

    /** resolve b (rhs) argument time-axis for plain time-axis,e.g. just fwd to bin_op_resolve_step */
    template <
      typename CTA,
      typename ATA,
      typename A,
      typename OP,
      typename B,
      typename enable_if<!is_same<gta_t, typename B::ta_t>::value, int>::type = 0>
    inline vector<double> bin_op_resolve_b(CTA const &c, ATA const &a_ta, A const &a, OP &&o, B const &b) {
      return bin_op_resolve_step(c, a_ta, b.ta, a, o, b);
    }

    /** resolve a (lhs) argument time-axis type in case it is a generic_dt, and fwd to bin_op_resolve_b */
    template <
      typename CTA,
      typename A,
      typename OP,
      typename B,
      typename enable_if<is_same<gta_t, typename A::ta_t>::value, int>::type = 0>
    inline vector<double> bin_op_resolve_a(CTA const &c, A const &a, OP &&o, B const &b) {
      if (a.time_axis().gt() == gta_t::FIXED)
        return bin_op_resolve_b(c, a.time_axis().f(), a, o, b);
      if (a.time_axis().gt() == gta_t::CALENDAR)
        return a.time_axis().c().is_simple()
               ? bin_op_resolve_b(c, a.time_axis().c().simplify(), a, o, b)
               : bin_op_resolve_b(c, a.time_axis().c(), a, o, b);
      return bin_op_resolve_b(c, a.time_axis().p(), a, o, b);
    }

    /** resolve a (lhs) argument time-axis for plain time-axis,e.g. just fwd to bin_op_resolve_b */
    template <
      typename CTA,
      typename A,
      typename OP,
      typename B,
      typename enable_if<!is_same<gta_t, typename A::ta_t>::value, int>::type = 0>
    inline vector<double> bin_op_resolve_a(CTA const &c, A const &a, OP &&o, B const &b) {
      return bin_op_resolve_b(c, a.ta, a, o, b);
    }
  }

  /** @brief evaulate a binary time-series operation
   *
   * This function reduces a single binary time-series vs time-series
   * operation to the matrialized time-series.
   *
   * Algorithm:
   *
   *  1) First compute resulting time-axis using the time_series::combine(ta1,ta2)
   *  2) Then unwrap the time-axis of type A and B down to it's primary types for fastest possible access.
   *  3) Use the detail::fx_lin, detail::fx_stair specialized ts-accessors to ensure fast access to time/value
   *
   * The primary usage is within the time-series engine while
   * computing/deflating time-series expressions.
   *
   * @tparam R wanted type/fx of result, must support signature R(time-axis,value<double>, ts_point_fx)
   * @tparam A type of the lhs, must have is_ts<A>::value true
   * @tparam OP any binary op callable (double,double)->double
   * @tparam B type of the rhs, bust have is_ts<B>::value true
   * @param a lhs time-series
   * @param b rhs time-series
   * @param op bin-operation, like std::plus
   * @return result of type R constructed as R(time-axis,values,
   * result_policy(a.point_interpretation(),b.point_interpretation()))
   * @see result_policy,is_ts
   */
  template <
    typename R,
    typename A,
    typename OP,
    typename B,
    typename enable_if<is_ts<A>::value && is_ts<B>::value, int>::type = 0>
  inline R bin_op_eval(A const &a, OP &&o, B const &b) {
    auto ta = combine(a.time_axis(), b.time_axis());
    return R(
      ta, detail::bin_op_resolve_a(ta, a, o, b), result_policy(a.point_interpretation(), b.point_interpretation()));
  }

}
