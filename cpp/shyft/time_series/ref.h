/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/point_ts.h>

// #include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;

  /** @brief a symbolic reference ts
   *
   * @details
   * A ref_ts is a time-series that have a ref, a uid, to some underlying
   * time-series data store.
   * It can be bound, or unbound.
   * If it's bound, then it behaves like the underlying time-series ts class, typically
   * a point_ts<ta> time-series, with a time-axis and some values associated.
   * If it's unbound, then any attempt to use it will cause runtime_error exception
   * to be thrown.
   *
   * The purpose of the ref_ts is to provide means of (unbound) time-series expressions
   * to be transferred to a node, where the binding process can be performed, and the resulting
   * values of the expression can be computed.
   * The result can then be transferred back to the origin (client) for further processing/presentation.
   *
   * This allow for execution of ts-expressions that reduces data
   *  to be executed at nodes close to data.
   *
   * Note that the mechanism is also useful, even for local evaluation of expressions, since it allow
   * analysis and binding to happen before the final evaluation of the values of the expression.
   * @tparam the type of ts refered to (keept by shared_ptr)
   */
  template <class TS>
  struct ref_ts {
    using ta_t = typename TS::ta_t;
    string ref; ///< reference to time-series supporting storage
    shared_ptr<TS> ts;

    TS& bts() {
      if (ts == nullptr)
        throw runtime_error("unbound access to ref_ts attempted");
      return *ts;
    }

    const TS& bts() const {
      if (ts == nullptr)
        throw runtime_error("unbound access to ref_ts attempted");
      return *ts;
    }

    ts_point_fx point_interpretation() const {
      return bts().point_interpretation();
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      bts().set_point_interpretation(point_interpretation);
    }

    // std. ct/dt etc.
    ref_ts() = default;

    void set_ts(shared_ptr<TS> const & tsn) {
      ts = tsn;
    }

    // useful constructors goes here:
    explicit ref_ts(string const & sym_ref)
      : ref(sym_ref) {
    } //, fx_policy(POINT_AVERAGE_VALUE) {}

    ta_t const & time_axis() const {
      return bts().time_axis();
    }

    /**@brief the function value f(t) at time t, fx_policy taken into account */
    double operator()(utctime t) const {
      return bts()(t);
    }

    /**@brief the i'th value of ts
     */
    double value(size_t i) const {
      return bts().value(i);
    }

    // BW compatiblity ?
    size_t size() const {
      return bts().size();
    }

    size_t index_of(utctime t) const {
      return bts().index_of(t);
    }

    utcperiod total_period() const {
      return bts().total_period();
    }

    utctime time(size_t i) const {
      return bts().time(i);
    }

    // to help average_value method for now!
    point get(size_t i) const {
      return bts().get(i);
    }

    // Additional write/modify interface to operate directly on the values in the time-series
    void set(size_t i, double x) {
      bts().set(i, x);
    }

    void add(size_t i, double value) {
      bts().add(i, value);
    }

    void add(point_ts<ta_t> const & other) {
      bts().add(other);
    }

    void add_scale(point_ts<ta_t> const & other, double scale) {
      bts().add_scale(other, scale);
    }

    void fill(double value) {
      bts().fill(value);
    }

    void fill_range(double value, int start_step, int n_steps) {
      bts().fill_range(value, start_step, n_steps);
    }

    void scale_by(double value) {
      bts().scale_by(value);
    }

    x_serialize_decl();
  };

  template <class T>
  struct is_ts<ref_ts<T>> {
    static bool const value = true;
  };

  template <class T>
  struct is_ts<shared_ptr<ref_ts<T>>> {
    static bool const value = true;
  };

}
