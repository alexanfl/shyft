/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
// #include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/methods/glacier_melt.h> // to get the glacier melt function

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;
  using std::forward;
  using std::move;

  /** @brief glacier melt ts
   *
   * @details
   * Using supplied temperature and snow covered area[m2] time-series
   * computes the glacier melt in units of [m3/s] using the
   * the following supplied parameters:
   *  -# dtf:day temperature factor (dtf),
   *  -# glacier_area_m2: glacier area in [m2] units
   *
   * @tparam TS_A a time-series type for the temperature
   * @tparam TS_B a time-series type for the snow covered area (defaults to TS_A)
   *
   * @note that both temperature and snow covered area (sca) TS is of same type
   * @ref shyft::core::glacier_melt::step function
   */
  template <class TS_A, class TS_B = TS_A>
  struct glacier_melt_ts {
    typedef typename d_ref_t<TS_A>::type::ta_t ta_t;
    TS_A temperature;
    TS_B sca_m2;
    double glacier_area_m2 = 0.0;
    double dtf = 0.0;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;

    ta_t const & time_axis() const {
      return d_ref(temperature).time_axis();
    }

    ts_point_fx point_interpretation() const {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      fx_policy = point_interpretation;
    }

    /** construct a glacier_melt_ts
     * @param temperature in [deg.C]
     * @param sca_m2 snow covered area [m2]
     * @param glacier_area_m2 [m2]
     * @param dtf degree timestep factor [mm/day/deg.C]; lit. values for Norway: 5.5 - 6.4 in Hock, R. (2003), J.
     * Hydrol., 282, 104-115.
     */
    template <class A_, class B_>
    glacier_melt_ts(A_&& temperature, B_&& sca_m2, double glacier_area_m2, double dtf)
      : temperature(std::forward<A_>(temperature))
      , sca_m2(std::forward<B_>(sca_m2))
      , glacier_area_m2(glacier_area_m2)
      , dtf(dtf) {
    }

    // std. ct etc
    glacier_melt_ts() = default;

    // ts property definitions
    point get(size_t i) const {
      return point(time_axis().time(i), value(i));
    }

    size_t size() const {
      return time_axis().size();
    }

    size_t index_of(utctime t) const {
      return time_axis().index_of(t);
    }

    utcperiod total_period() const {
      return time_axis().total_period();
    }

    //--
    double value(size_t i) const {
      if (i >= time_axis().size())
        return nan;
      utcperiod p = time_axis().period(i);
      double t_i = d_ref(temperature).value(i);
      size_t ix_hint = i; // assume same indexing of sca and temperature
      double sca_m2_i = average_value(
        d_ref(sca_m2), p, ix_hint, d_ref(sca_m2).point_interpretation() == ts_point_fx::POINT_INSTANT_VALUE);
      return shyft::core::glacier_melt::step(dtf, t_i, sca_m2_i, glacier_area_m2);
    }

    double operator()(utctime t) const {
      size_t i = index_of(t);
      if (i == string::npos)
        return nan;
      return value(i);
    }

    x_serialize_decl();
  };

  template <class T>
  struct is_ts<glacier_melt_ts<T>> {
    static bool const value = true;
  };

  template <class T>
  struct is_ts<shared_ptr<glacier_melt_ts<T>>> {
    static bool const value = true;
  };

}
