/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/fx_average.h>

namespace shyft::time_series {
  using namespace shyft::core;

  /** @brief average_accessor provides a projection/interpretation
   * of the values of a point source on to a time-axis as provided
   * with semantics as needed/practical for this project.
   *
   * This includes interpolation and true average,
   * @note point interpretation:
   *  We try to interpret the points as f(t),
   *   using either
   *    stair-case (start of step)
   *   or
   *    linear between points
   *  interpretations.
   *
   * @note stair-case is ideal for timeseries where each period on the timeaxis
   * contains a value that represents the average value for that timestep.
   *  E.g. average discharge, precipitation etc.
   *
   * @note linear-between points is ideal/better when the time-value points represents
   * an instantaneous value, observed at(or around) that particular timepoints
   *  and where the signal is assumed to follow 'natural' changes.
   *  eg. temperature: observed 12 degC at 8am, 16 degC at 10am, then a linear approximation
   *      is used to calculate f(t) in that interval
   *
   * @note nan-handling semantics:
   *   when a nan point is introduced at to,
   *   then f(t) is Nan from that point until next non-nan point.
   *
   * @note Computation of true average
   *    true-average = 1/(t1-t0) * integral (t0..t1) f(t) dt
   *
   *   Only non-nan parts of the time-axis contributes to the area, integrated time distance.
   *   e.g.: if half the interval have nan values, and remaining is 10.0, then true average is 10.0 (not 5.0).
   *
   *   TODO: maybe this approach fits for some cases, but not all? Then introduce parameters to get what we want ?
   *
   *
   * In addition the accessor allows fast sequential access to these values
   * using clever caching of the last position used in the underlying
   * point source. The time axis and point source can be of any type
   * as listed above as long as the basic type requirement as described below
   * are satisfied.
   * This is typically the case for observed time-series point sources, where values could
   * be missing, bad, or just irregularly sampled.
   *
   * @note Notice that the S and T objects are const referenced and not owned by Accessor,
   * the intention is that accessor is low-cost/light-weight class, to be created
   * and used in tight(closed) local (thread)scopes.
   * The variables it wrapped should have a lifetime longer than accessor.
   * TODO: consider to leave the lifetime-stuff to the user, by spec types..
   *
   * @tparam S point source, must provide:
   *  -# .size() const               -> number of points in the source
   *  -# .index_of(utctime tx) const -> return lower bound index or -1 for the supplied tx
   *  -# .get(size_t i) const        -> return the i'th point  (t,v)
   *  -# .point_fx const -> point_interpretation(POINT_INSTANT_VALUE|POINT_AVERAGE_VALUE)
   * @tparam TA time axis, must provide:
   *  -# operator()(size_t i) const --> return the i'th period of timeaxis
   *  -#
   * @return Accessor that provides:
   * -# value(size_t i) const -> double, the value at the i'th interval of the supplied time-axis
   */

  template <class S, class TA>
  class average_accessor {
   private:
    static const size_t npos = -1; // msc doesn't allow this std::basic_string::npos;
    mutable size_t last_idx = -1;
    mutable size_t q_idx = -1;    // last queried index
    mutable double q_value = nan; // outcome of
    const TA& time_axis;
    S const & source;
    std::shared_ptr<S> source_ref; // to keep ref.counting if ct with a shared-ptr. source will have a const ref to *ref
    bool linear_between_points = false;
   public:
    average_accessor(S const & source, const TA& time_axis)
      : last_idx(0)
      , q_idx(npos)
      , q_value(0.0)
      , time_axis(time_axis)
      , source(source)
      , linear_between_points(source.point_interpretation() == POINT_INSTANT_VALUE) { /* Do nothing */
    }

    average_accessor(std::shared_ptr<S> const & source, const TA& time_axis) // also support shared ptr. access
      : last_idx(0)
      , q_idx(npos)
      , q_value(0.0)
      , time_axis(time_axis)
      , source(*source)
      , source_ref(source)
      , linear_between_points(source->point_interpretation() == POINT_INSTANT_VALUE) {
    }

    double value(const size_t i) const {
      if (i == q_idx)
        return q_value; // 1.level cache, asking for same value n-times, have cost of 1.
      if (time_axis.time(i) >= source.total_period().end) {
        q_idx = i;
        q_value = nan;
      } else {
        q_value = average_value(source, time_axis.period(q_idx = i), last_idx, linear_between_points);
      }

      return q_value;
    }

    size_t size() const {
      return time_axis.size();
    }
  };

  /** @brief The direct_accessor template is a fast accessor
   *
   * that utilizes prior knowledge about the source ts, e.g. fixed regualar intervals,
   * where each values represents the average/representative value for the interval.
   * This is typical for our internal timeseries results, and we need to utilize that.
   *
   * @note life-time warning: The supplied constructor parameters are const ref.
   *
   * @tparam S the point_source that need to relate to the supplied timeaxis T
   * @tparam TA the timeaxis that need to match up with the S
   * @sa average_accessor that provides transformation of unknown source to the provide timeaxis
   */
  template <class S, class TA>
  class direct_accessor {
   private:
    const TA& time_axis;
    S const & source;
   public:
    direct_accessor(S const & source, const TA& time_axis)
      : time_axis(time_axis)
      , source(source) {
      if (source.size() != time_axis.size())
        throw std::runtime_error("Cannot use time axis to access source: Dimensions don't match.");
    }

    /** @brief Return value at pos, and check that the time axis agrees on the time.
     */
    double value(const size_t i) const {
      point pt = source.get(i);
      if (pt.t != time_axis.time(i))
        throw std::runtime_error("Time axis and source are not aligned.");
      return pt.v;
    }

    size_t size() const {
      return time_axis.size();
    }
  };

  /** @brief Specialization of the direct_accessor template for commonly used point_source<TA>
   *
   * @note life-time warning: the provided point_source is kept by const ref.
   * @sa direct_accessor
   * @tparam TA the time-axis
   */
  template <class TA>
  class direct_accessor<point_ts<TA>, TA> {
   private:
    point_ts<TA> const &
      source; //< @note this is a reference to the supplied point_source, so please be aware of life-time
   public:
    direct_accessor(point_ts<TA> const & source, const TA& /*ta*/)
      : source(source) {
    }

    /** @brief Return value at pos without check since the source has its own timeaxis
     */
    double value(const size_t i) const {
      return source.get(i).v;
    }

    size_t size() const {
      return source.size();
    }
  };

  /** Helper template for max_abs_average_accessor that wraps a ts-source,
   * and returns the max(0,source) or max(0,-source) values
   * @see max_abs_average_accessor and goal-functions in the optimizer
   */
  template <class S>
  struct source_max_abs {
    S const & source; ///< underlying source ts , must support usual source stuff, see below
    bool neg;         ///< if true, return max(0.0, -source), else max(0.0,source)

    source_max_abs(S const & s, bool neg)
      : source(s)
      , neg(neg) {
    }

    point get(size_t i) const {
      point p = source.get(i); // use std. get function
      if (std::isfinite(p.v))
        p.v = neg ? std::max(0.0, -p.v) : std::max(0.0, p.v); // manipulate the value
      return p;
    }

    // just pass through other methods needed
    size_t index_of(utctime tx) const {
      return source.index_of(tx);
    }

    size_t size() const {
      return source.size();
    }

    utctime time(size_t i) const {
      return source.time(i);
    }

    utcperiod total_period() const {
      return source.total_period();
    }
  };

  /** @brief max_abs_average_accessor returns the largest absolute average for each interval
   *
   *
   * @see optimizer, and goal-functions
   */
  template <class S, class TA>
  class max_abs_average_accessor {
   private:
    static const size_t npos = -1; // msc doesn't allow this std::basic_string::npos;
    mutable size_t last_idx = -1;
    mutable size_t q_idx = -1;    // last queried index
    mutable double q_value = nan; // outcome of
    const TA& time_axis;
    source_max_abs<S> mx_abs;
    source_max_abs<S> mn_abs;
    const std::shared_ptr<S>
      source_ref; // to keep ref.counting if ct with a shared-ptr. source will have a const ref to *ref
    bool linear_between_points = false;
    const utctime t_end;
   public:
    max_abs_average_accessor(S const & source, const TA& time_axis)
      : last_idx(0)
      , q_idx(npos)
      , q_value(0.0)
      , time_axis(time_axis)
      , mx_abs(source, false)
      , mn_abs(source, true)
      , linear_between_points(source.point_interpretation() == POINT_INSTANT_VALUE)
      , t_end(source.total_period().end) { /* Do nothing */
    }

    max_abs_average_accessor(std::shared_ptr<S> const & source, const TA& time_axis) // also support shared ptr. access
      : last_idx(0)
      , q_idx(npos)
      , q_value(0.0)
      , time_axis(time_axis)
      , mx_abs(*source, false)
      , mn_abs(*source, true)
      , source_ref(source)
      , linear_between_points(source->point_interpretation() == POINT_INSTANT_VALUE)
      , t_end((*source).total_period().end) {
    }

    double value(const size_t i) const {
      if (i == q_idx)
        return q_value;                 // 1.level cache, asking for same value n-times, have cost of 1.
      if (time_axis.time(i) >= t_end) { // always nan after t_end
        q_idx = i;
        q_value = nan;
      } else {
        double pos_val = average_value(mx_abs, time_axis.period(q_idx = i), last_idx, linear_between_points);
        double neg_val = average_value(mn_abs, time_axis.period(q_idx = i), last_idx, linear_between_points);
        q_value = std::max(pos_val, neg_val);
      }
      return q_value;
    }

    size_t size() const {
      return time_axis.size();
    }
  };

}
