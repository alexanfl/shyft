/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/fx_average.h>
#include <shyft/time_series/fx_resample.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::time_series {
  using core::utctime;
  using core::calendar;
  using gta_t = time_axis::generic_dt;
  using gts_t = point_ts<gta_t>;

  /**
   * @brief time-axis calc stub for plain fixed interval
   * @details To keep algorithms fixed/calendar agnostic
   */
  struct time_axis_fixed_dt {
    inline int64_t diff_units(utctime t1, utctime t2, utctime dt) const noexcept {
      return (t2 - t1) / dt;
    }

    inline utctime add(utctime t1, utctime dt, int64_t n) const noexcept {
      return t1 + n * dt;
    }

    time_axis::fixed_dt create(utctime t, utctime dt, size_t n) const {
      return time_axis::fixed_dt(t, dt, n);
    }
  };

  /**
   * @brief time-axis calc stub for calendar semantic sensitive axis
   * @details To keep algorithms fixed/calendar agnostic
   */
  struct time_axis_calendar_dt {
    std::shared_ptr<calendar> const &cal;

    inline int64_t diff_units(utctime t1, utctime t2, utctime dt) const noexcept {
      return cal->diff_units(t1, t2, dt);
    }

    inline utctime add(utctime t1, utctime dt, int64_t n) const noexcept {
      return cal->add(t1, dt, n);
    }

    time_axis::calendar_dt create(utctime t, utctime dt, size_t n) const {
      return time_axis::calendar_dt(cal, t, dt, n);
    }
  };

  /**
   * @brief make ts fragment aligned with target time-series
   * @details
   * Give a time-series fragment `ts` that should be projected (best effort/functional)
   * to the target time-series, that have restrictions on the time-axis,
   * e.g. a fixed_dt or calendar_dt interval type.
   * The most basic example is projecting a 1hour fragment to a 15 min
   * stair case kind of time-series. - which is straight forward.
   *
   * The usage context is in the dstm when editing local in-memory time-series,
   * or when those fragments are forwarded to the main dtss for storage.
   *
   * The default behavior/rules of dtss and time-series mutation is that the mutating fragment
   * must exactly match the target time-series with respect to requirements on time-axis.
   * This implies that a break-point time-series can be mutated by any fragment(because there are no requirement).
   * .. and similar a hour time-series can only be changed by a hourly aligned fragment.
   *
   * The more general approach, that covers all the use-cases, and corner-cases
   * is a bit more challenging.
   * The overall idea is that the fragment time-series f(t) is to be
   * projected to the target time-series so that the effect on the
   * target time-series becomes 'natural', or 'intuitive'.
   *
   * The definition that covers 'natural' and 'intuitive' is
   * a bit stretched, but it for sure covers the intial simple case.
   *
   * Main rules:
   * When the target is a stair-case kind of ts, we compute the true average of the fragment  period.
   * When the target is a linear kind of ts, we resample, evalute f(t) for t that satisfies the target.
   *
   * This gives an 'inutitive' projection of rate-units (J/s, m3/s) to the target,
   * as well as state units, resampled at target timepoints, like reservoir level masl, m, temperature C.
   * Further comments details is given in the implementation.
   *
   * @tparam C a time-axis calc type like class provides .add, .diff_units and .create
   * @param c the C time-axis calc type object to be used
   * @param t0 of the target time-series
   * @param dt of the target time-series
   * @param point_fx how the target-series understand its points
   * @param a point_ts that needs a functional projection to the target time-series
   * @return a time-series that is compatible with the target time-series, reflecting the f(t) projected to the target
   * ts
   */
  template <class C>
  gts_t fx_make_aligned(C const &c, utctime t0, utctime dt, ts_point_fx point_fx, gts_t const &ts) {
    auto p = ts.ta.total_period();
    if (point_fx == POINT_AVERAGE_VALUE) {
      // semantics: source could be lin or stair, we  compute true-average accordingly.
      auto i0 = c.diff_units(t0, p.start, dt);
      if (ts.point_interpretation() == POINT_AVERAGE_VALUE) {
        auto i1 = c.diff_units(t0, p.end, dt);
        if (c.add(t0, dt, i1) != p.end)
          ++i1; // include last if not exact
        auto rta = c.create(c.add(t0, dt, i0), dt, static_cast<size_t>(i1 - i0));
        vector<double> r(rta.size(), shyft::nan);
        _accumulate_stair_case(
          rta,
          ts,
          true, // average.
          [](auto const &ts, utctime t) {
            return ts.ta.index_of(t); //
          },
          [&r](size_t i, double v) noexcept {
            r[i] = v;
          });
        return gts_t(time_axis::generic_dt(rta), std::move(r), point_fx);
      } else {
        if (ts.size() == 1) { // if lin and just one point.. we assume flat signal
          auto i1 = c.diff_units(t0, p.end, dt);
          if (t0 + dt * i1 != p.end)
            ++i1; // include last if not exact
          auto rta = c.create(c.add(t0, dt, i0), dt, static_cast<size_t>(i1 - i0));
          vector<double> r(rta.size(), ts.v[0]);
          return gts_t(time_axis::generic_dt(rta), std::move(r), point_fx);
        } else {
          // ts.total_period.end vs rta.end ..
          //  we do not want lin ts to extend rta after last point (because it's nan/not flat
          // so if rta.time(n-1) > ts.time(ts.size()-1))
          // then
          auto t_last = ts.time_axis().time(ts.size() - 1);
          auto i1 = c.diff_units(t0, t_last, dt);
          if (c.add(t0, dt, i1) < t_last)
            ++i1; // include last if not exact
          auto rta = c.create(c.add(t0, dt, i0), dt, static_cast<size_t>(i1 - i0));
          vector<double> r(rta.size(), shyft::nan);
          _accumulate_linear(
            rta,
            ts,
            true, // average.
            [](auto const &ts, utctime t) {
              return ts.ta.index_of(t); //
            },
            [&r](size_t i, double v) noexcept {
              r[i] = v;
            });
          return gts_t(time_axis::generic_dt(rta), std::move(r), point_fx);
        }
      }
    } else { // target is linear:
      // semantics: incoming fragment could be lin| stair, target is lin
      //  resample at time-points? .. could make sense, we go for that!
      /// but then: what to do at the partial begin.. partial end?
      ///           note that the source ts is not defined outside it's intervals
      ///           so resampling to the destination ta.. is a semantic problem.
      ///              so rather than trim-out, trim-in for the effective time-axis? seems ok!
      ///              the essence: we resample source using time-points convered by source
      ///              .. with the anomaly of the fragment only covering interior target interval:
      ///              .. in that case there is zero/noop update (as a consequence of the definition above)
      ///
      auto i0 = c.diff_units(t0, p.start, dt);
      if (c.add(t0, dt, i0) < p.start)
        ++i0; // the fragment is not covering i0, so we step one more ahead
      auto i1 = c.diff_units(t0, p.end, dt);
      if (c.add(t0, dt, i1) > p.end)
        --i1; // ensure frag is covering re-sampled ta.
      if (i0 >= i1)
        return gts_t{};
      auto rta = c.create(c.add(t0, dt, i0), dt, static_cast<size_t>(i1 - i0));
      auto r = fx_resample(ts, rta);
      return gts_t(time_axis::generic_dt(rta), std::move(r), point_fx);
    }
  }

}
