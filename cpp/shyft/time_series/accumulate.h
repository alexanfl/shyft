/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;

  /** @brief accumulate_ts, accumulate time-series
   *
   * @details
   * Represents a ts that for
   * the specified time-axis the accumulated sum
   * of the underlying specified TS ts.
   * The i'th value in the time-axis is computed
   * as the sum of the previous true-averages.
   * The ts_point_fx is POINT_INSTANT_VALUE
   * definition:
   * The value of t0=time_axis(0) is zero.
   * The value of t1= time_axis(1) is defined as
   * integral_of f(t) dt from t0 to t1, skipping nan-areas.
   *
   * @tparam TS the time-series type
   * @tparam TA the time-axis type
   */
  template <class TS, class TA>
  struct accumulate_ts {
    typedef TA ta_t;
    TA ta;
    TS ts;
    ts_point_fx fx_policy = POINT_INSTANT_VALUE;

    const TA& time_axis() const {
      return ta;
    }

    ts_point_fx point_interpretation() const {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      fx_policy = point_interpretation;
    }

    accumulate_ts() = default;

    accumulate_ts(const TS& ts, const TA& ta)
      : ta(ta)
      , ts(ts) {
    } // because accumulate represents the integral of the distance from t0 to t, valid at t

    point get(size_t i) const {
      return point(ta.time(i), value(i));
    }

    size_t size() const {
      return ta.size();
    }

    size_t index_of(utctime t) const {
      return ta.index_of(t);
    }

    utcperiod total_period() const {
      return ta.total_period();
    }

    //--
    double value(size_t i) const {
      if (i >= ta.size())
        return nan;
      if (i == 0)
        return 0.0;
      size_t ix_hint = 0; // we have to start at the beginning
      utcperiod accumulate_period(ta.time(0), ta.time(i));
      // utctimespan tsum;
      return FX_accumulate_value(
        ts,
        accumulate_period,
        ix_hint,
        false,
        d_ref(ts).fx_policy == ts_point_fx::POINT_INSTANT_VALUE); // also note: average of non-nan areas !
    }

    double operator()(utctime t) const {
      size_t i = ta.index_of(t);
      if (i == string::npos || ta.size() == 0)
        return nan;
      if (t == ta.time(0))
        return 0.0; // by definition
      // utctimespan tsum;
      size_t ix_hint = 0;
      return FX_accumulate_value(
        ts,
        utcperiod(ta.time(0), t),
        ix_hint,
        false,
        d_ref(ts).fx_policy == ts_point_fx::POINT_INSTANT_VALUE); // also note: average of non-nan areas !;
    }

    x_serialize_decl();
  };


}
