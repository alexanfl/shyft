/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>

namespace shyft::time_series {
  using std::int8_t;

  /** @brief The convolve_policy determines how the convolve_w_ts function aligns the filter kernel,
   *  and how it deals with the boundary.
   *
   * @details
   * The policies BACKWARD, CENTER and FORWARD determines the alignment of the filter wrt
   * the output value: BACKWARD will align the kernel from value(i) and backwards, FORWARD
   * will align the kernel from value(i) and forwards, while CENTER will align the kernel
   * centrally on value(i).
   *
   * The policies USE_NEAREST, USE_ZERO and USE_NAN determines which padding value to use
   * for ts when the filter kernel slides outside ts, i.e. when trying to convolve with
   * values before value(0) or after value(n-1).
   *
   * Two policies (alignment + padding) may be or'ed together. The output result will be
   * undefined if an invalid combination of policies are supplied.
   *
   * @sa convolve_w_ts
   */
  enum convolve_policy : int8_t {
    BACKWARD = (1 << 6),
    CENTER = (1 << 5),
    FORWARD = (1 << 4),
    USE_NEAREST = (1 << 0), ///< ts.value(0) or ts.value(n-1) is used for all values outside ts: 'mass preserving'
    USE_ZERO = (1 << 1),    ///< fill in zero for all values outside ts: 'shape preserving'
    USE_NAN = (1 << 2),     ///< nan filled in outside the filter
  };

  inline convolve_policy operator|(convolve_policy a, convolve_policy b) {
    return static_cast<convolve_policy>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b));
  }
}
