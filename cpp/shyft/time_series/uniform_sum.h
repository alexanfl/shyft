/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;

  /** @brief a sum_ts computes the sum ts  of vector<ts>
   *
   * @details
   * Enforces all ts do have at least same number of elements in time-dimension
   * Require number of ts >0
   * computes .value(i) so that it is the sum of all tsv.value(i)
   * time-axis equal to tsv[0].time_axis
   * time-axis should be equal, notice that only .size() is equal is ensured in the constructor
   *
   * @tparam T the uniform type of time-series to compute the sum of.
   *
   * @note The current approach only works for ts of same type, enforced by the compiler
   *  The runtime enforces that each time-axis are equal as well, and throws exception
   *  if not (in the non-default constructor), - if user later modifies the tsv
   *  it could break this assumption.
   *  Point interpretation is default POINT_AVERAGE_VALUE, you can override it in ct.
   *  Currently ts-vector as expressions is not properly supported (they need to be bound before passed to ct!)
   */
  template <class T>
  struct uniform_sum_ts {
    typedef typename T::ta_t ta_t;
   private:
    std::vector<T> tsv; ///< need this private to ensure consistency after creation
   public:
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE; // inherited from ts
    uniform_sum_ts() = default;

    //-- useful ct goes here
    template <class A_>
    uniform_sum_ts(A_&& tsv, ts_point_fx fx_policy = POINT_AVERAGE_VALUE)
      : tsv(std::forward<A_>(tsv))
      , fx_policy(fx_policy) {
      if (tsv.size() == 0)
        throw std::runtime_error("vector<ts> size should be > 0");
      for (size_t i = 1; i < tsv.size(); ++i)
        if (!(tsv[i].time_axis() == tsv[i - 1].time_axis())) // todo: a more extensive test needed, @ to high cost.. so
                                                             // maybe provide a ta, and do true-average ?
          throw std::runtime_error("vector<ts> timeaxis should be aligned in sizes and numbers");
    }

    ta_t const & time_axis() const {
      return tsv[0].time_axis();
    }

    ts_point_fx point_interpretation() const {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      fx_policy = point_interpretation;
    }

    point get(size_t i) const {
      return point(tsv[0].time(i), value(i));
    }

    size_t size() const {
      return tsv.size() ? tsv[0].size() : 0;
    }

    size_t index_of(utctime t) const {
      return tsv.size() ? tsv[0].index_of(t) : std::string::npos;
    }

    utcperiod total_period() const {
      return tsv.size() ? tsv[0].total_period() : utcperiod();
    }

    utctime time(size_t i) const {
      if (tsv.size())
        return tsv[0].time(i);
      throw std::runtime_error("uniform_sum_ts:empty sum");
    }

    //--
    double value(size_t i) const {
      if (tsv.size() == 0 || i == std::string::npos)
        return nan;
      double v = tsv[0].value(i);
      for (size_t j = 1; j < tsv.size(); ++j)
        v += tsv[j].value(i);
      return v;
    }

    double operator()(utctime t) const {
      return value(index_of(t));
    }

    std::vector<double> values() const {
      std::vector<double> r;
      r.reserve(size());
      for (size_t i = 0; i < size(); ++i)
        r.push_back(value(i));
      return r;
    }
  };

  template <class T>
  struct is_ts<uniform_sum_ts<T>> {
    static bool const value = true;
  };

  template <class T>
  struct is_ts<shared_ptr<uniform_sum_ts<T>>> {
    static bool const value = true;
  };


}
