/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/time_series/expression_serialization.h>
#include <shyft/core/core_archive.h>

using shyft::core::core_iarchive;
using shyft::core::core_oarchive;

/** from  google how to serialize tuple using a straight forward tuple expansion */
namespace boost ::serialization {

  /** generic recursive template version of tuple_serialize */
  template <int N>
  struct tuple_serialize {
    template <class Archive, typename... tuple_types>
    static void serialize(Archive &ar, std::tuple<tuple_types...> &t, unsigned const version) {
      ar &std::get<N - 1>(t);
      tuple_serialize<N - 1>::serialize(ar, t, version); // recursive expand/iterate over members
    }
  };

  /** specialize recurisive template expansion termination at 0 args */
  template <>
  struct tuple_serialize<0> {
    template <class Archive, typename... tuple_types>
    static void serialize(Archive &, std::tuple<tuple_types...> &, unsigned const) {
      ; // zero elements to serialize, noop, btw: should we instead stop at <1>??
    }
  };

  template <class Archive, typename... tuple_types>
  void serialize(Archive &ar, std::tuple<tuple_types...> &t, unsigned const version) {
    tuple_serialize<sizeof...(tuple_types)>::serialize(ar, t, version);
  }


} // boost.serialization

template < class... srep_types>
template <class Archive>
void shyft::time_series::dd::ts_expression<srep_types...>::serialize(Archive &ar, unsigned int const /*version*/) {
  ar &ts_reps &roots; // we *could* use for_each tuple here
                      // we could just do ar  & rts & gts;
                      // but at least for the rts, serializing only strings means 2-3 times faster
                      // serialization step.
  if (Archive::is_loading::value) {
    size_t n;
    ar &n;
    rts.reserve(n);
    for (size_t i = 0; i < n; ++i) {
      auto ts = new aref_ts();
      bool has_real_ts = false;
      ar & ts->id &has_real_ts;
      if (has_real_ts) {
        gts_t rep;
        ar &rep;
        ts->rep = make_shared<gpoint_ts const>(std::move(rep));
      }
      rts.push_back(ts);
    }
    ar &n;
    gts.reserve(n);
    for (size_t i = 0; i < n; ++i) {
      auto ts = new gpoint_ts();
      ar & ts->rep;
      gts.push_back(ts);
    }
  } else { // saving (note that we need the const-casts since we are in same compilation fx, a split could resolve this)
    size_t n = rts.size();
    ar &n;
    for (size_t i = 0; i < n; ++i) {
      ar &const_cast<aref_ts *>(rts[i])->id;
      bool has_real_ts = !rts[i]->needs_bind();
      ar &has_real_ts;
      if (has_real_ts) {
        ar &const_cast<gts_t &>(rts[i]->rep->rep);
      }
    }
    n = gts.size();
    ar &n;
    for (size_t i = 0; i < n; ++i) {
      ar &const_cast<gpoint_ts *>(gts[i])->rep;
    }
  }
}

template <class Archive>
void shyft::time_series::dd::srep::saverage_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &ta;
}

template <class Archive>
void shyft::time_series::dd::srep::sstatistics_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &ta &p;
}

template <class Archive>
void shyft::time_series::dd::srep::sintegral_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &ta;
}

template <class Archive>
void shyft::time_series::dd::srep::sderivative_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &dm;
}

template <class Archive>
void shyft::time_series::dd::srep::saccumulate_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &ta;
}

template <class Archive>
void shyft::time_series::dd::srep::speriodic_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts;
}

template <class Archive>
void shyft::time_series::dd::srep::sconvolve_w_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &w &policy;
}

template <class Archive>
void shyft::time_series::dd::srep::srating_curve_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &rc_param;
}

template <class Archive>
void shyft::time_series::dd::srep::sice_packing_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &ip_param &ipt_policy;
}

template <class Archive>
void shyft::time_series::dd::srep::sice_packing_recession_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &flow_ts &ip_ts &ipr_param;
}

template <class Archive>
void shyft::time_series::dd::srep::skrls_interpolation_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &predictor;
}

template <class Archive>
void shyft::time_series::dd::srep::sqac_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &cts &p;
}

template <class Archive>
void shyft::time_series::dd::srep::srepeat_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &rta &ta &bound;
}

template <class Archive>
void shyft::time_series::dd::srep::snary_op_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &args &op &lead_time &fc_interval;
}

template <class Archive>
void shyft::time_series::dd::srep::stransform_spline_ts::serialize(Archive &ar, unsigned const /*version*/) {
  ar &ts &p;
}

x_serialize_instantiate_and_register(shyft::time_series::dd::srep::stransform_spline_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sstatistics_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::saverage_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sintegral_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sderivative_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::saccumulate_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::speriodic_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sconvolve_w_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::srating_curve_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sice_packing_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sice_packing_recession_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::skrls_interpolation_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::sqac_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::srep::snary_op_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::compressed_ts_expression);
