/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <boost/math/special_functions/relative_difference.hpp>
#include <shyft/time_series/common.h>
#include <shyft/time_series/ice_packing_policy.h>
#include <shyft/time_series/ice_packing_parameters.h>
#include <shyft/time_series/fx_average.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;

  /** @brief ice-packing ts that detect ice-packing from average temperature
   *
   * @tparam TS a time-series that satisfy std. time-series methods, or a shared ptr to
   */
  template < class TS >
  struct ice_packing_ts {

    // type api
    using ts_t = TS;
    using ipp_t = ice_packing_parameters;
    using ta_t = typename TS::ta_t;

    // data
    ts_t temp_ts;
    ipp_t ip_param;
    ice_packing_temperature_policy ipt_policy = ice_packing_temperature_policy::DISALLOW_MISSING;
    // -----
    ts_point_fx fx_policy = ts_point_fx::POINT_AVERAGE_VALUE;
    // -----
    bool bound = false;

    // con/de-struction, copy & move
    ice_packing_ts() {                   // can't use default here,
      if (!e_needs_bind(d_ref(temp_ts))) // because we need this to get right
        local_do_bind();
    }

    template < class TS_A, class IPP >
    ice_packing_ts(
      TS_A &&ts,
      IPP &&ipp,
      ice_packing_temperature_policy ipt_policy = ice_packing_temperature_policy::DISALLOW_MISSING,
      ts_point_fx fx_policy = ts_point_fx::POINT_AVERAGE_VALUE)
      : temp_ts{std::forward<TS_A>(ts)}
      , ip_param{std::forward<IPP>(ipp)}
      , ipt_policy{ipt_policy}
      , fx_policy{fx_policy} {
      if (fx_policy != POINT_AVERAGE_VALUE)
        throw std::runtime_error("ice_packing_ts: fx_policy should be POINT_AVERAGE_VALUE only");
      if (!e_needs_bind(d_ref(temp_ts)))
        local_do_bind();
    }

    // -----
    ~ice_packing_ts() = default;
    // -----
    ice_packing_ts(ice_packing_ts const &) = default;
    ice_packing_ts &operator=(ice_packing_ts const &) = default;
    // -----
    ice_packing_ts(ice_packing_ts &&) = default;
    ice_packing_ts &operator=(ice_packing_ts &&) = default;

    // usage
    bool needs_bind() const {
      return !bound;
    }

    void do_bind() {
      if (!bound) {
        d_ref(temp_ts).do_bind();
        local_do_bind();
      }
    }

    void do_unbind() {
      if (bound) {
        d_ref(temp_ts).do_unbind();
        local_do_unbind();
      }
    }

    void local_do_bind() {
      fx_policy = POINT_AVERAGE_VALUE;
      bound = true;
    }

    void local_do_unbind() {
      bound = false;
    }

    void ensure_bound() const {
      if (!bound) {
        throw runtime_error("ice_packing_ts: access to not yet bound attempted");
      }
    }

    // -----
    ts_point_fx point_interpretation() const {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx /*policy*/) {
      if (fx_policy != POINT_AVERAGE_VALUE)
        throw std::runtime_error("ice_packing_ts: fx_policy should be POINT_AVERAGE_VALUE only");
      // fx_policy = POINT_AVERAGE_VALUE;
    }

    // api
    std::size_t size() const {
      return d_ref(temp_ts).size();
    }

    utcperiod total_period() const {
      return d_ref(temp_ts).total_period();
    }

    ta_t const &time_axis() const {
      return d_ref(temp_ts).time_axis();
    }

    // -----
    std::size_t index_of(utctime t) const {
      return d_ref(temp_ts).index_of(t);
    }

    double operator()(utctime t) const {
      return detect_ice_packing(t);
    }

    // -----
    utctime time(std::size_t i) const {
      return d_ref(temp_ts).time(i);
    }

    double value(std::size_t i) const {
      return detect_ice_packing(time(i));
    }

    point get(size_t i) const {
      return point{time(i), value(i)};
    }

   private:
    /** @brief detect if ice-packing can occurs
     *
     * A simple approach base on the simple detector:
     *   if average temperature in [ t-window, t> is less than threshold_temp
     *     return 1.0 otherwise 0.0
     *   if not able to compute average, - return nan
     *
     *  policy for the average period:
     *     ALLOW_ANY_MISSING : as long as it's possible to compute average, use it
     *     DISALLOW_MISSING : average period must be complete (no nans)
     *     ALLOW_INITIAL_MISSING : clip window period to the beginning of the temperature ts, then apply disallow
     * missing
     *
     * @note this algorithm have rather large cost if called repeatedly for increasing time.
     *       in those cases a more clever algoritm using moving averages (in case window overlap multiple flow-points)
     *       or just one-pass scan from beginning.
     */
    double detect_ice_packing(utctime t) const {
      ensure_bound();
      utcperiod p(t - ip_param.window, t);
      if (ipt_policy != ice_packing_temperature_policy::DISALLOW_MISSING) {
        // adjust beginning of period if needed (could get zero period)
        if (p.start < total_period().start) {
          p.start = std::min(total_period().start, p.end);
        }
      }
      // compute the integral of non-nan areas, and t_sum gives the non-nan time-span.
      if (p.timespan() == utctimespan{0}) {
        return 0.0; // either it's clipped to zero, or window is 0, in both cases no ice-packing
        // sih: as an alternative, use instant value at t, but that does not make sense for the purpose of this algorithm
      }

      size_t ix_hint =
        -1; // if we could guess index from time t, we could set it here, for now it's just ok to pass none
      auto const lin = d_ref(temp_ts).point_interpretation() == ts_point_fx::POINT_INSTANT_VALUE;
      double p_avg = fx_accumulate_value(d_ref(temp_ts), p, ix_hint, true, lin);
      if (!isfinite(
            p_avg)) //|| t_sum==utctimespan{0}) // if nan, or t_sum = 0, temperature and average is nan or undefined
        return shyft::nan; /// we return nan
      if (ipt_policy != ice_packing_temperature_policy::ALLOW_ANY_MISSING) {
        ix_hint = -1; // scan for values that are nan inside  period p.
        auto s = d_ref(temp_ts).index_of(p.start);
        if (s == string::npos || d_ref(temp_ts).total_period().end < p.end)
          return shyft::nan; // because missing value at start, or at the end.
        auto n = d_ref(temp_ts).size();
        while (s < n && d_ref(temp_ts).time(s) < p.end) { //
          if (!std::isfinite(d_ref(temp_ts).value(s++)))
            return shyft::nan;
        }
        if (s < n && lin && !std::isfinite(d_ref(temp_ts).value(s))) // for lin, also check rhs value of last interval
          return shyft::nan;
      }
      return p_avg < ip_param.threshold_temp
             ? 1.0
             : 0.0; // finally a clear cut case, where we have the average, and can compare to threshold
    }

    x_serialize_decl();
  };

  template <class TS>
  struct is_ts<ice_packing_ts<TS>> {
    static bool const value = true;
  };

}
