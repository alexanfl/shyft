/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <cmath>
#include <stdexcept>
#include <shyft/time_series/common.h>

namespace shyft::time_series {
  // using namespace shyft::core;
  using std::runtime_error;
  using std::isfinite;

  /** @brief Discrete l2 norm of input time series treated as a vector: (sqrt(sum(x_i)))
   *
   * @note only used for debug/printout during calibration
   * @tparam A the accessor type that implement:
   * -#: .size() number of elements the accessor can provide
   * -#: .value(i) value of the i'th element in the accessor
   */
  template <class A>
  double l2_norm(A& ts) {
    double squared_sum = 0.0;
    for (size_t i = 0; i < ts.size(); ++i) {
      double tmp = ts.value(i);
      squared_sum += tmp * tmp;
    }
    return sqrt(squared_sum);
  }

  /** @brief Nash Sutcliffe model effiency coefficient based goal function
   * <a ref href=http://en.wikipedia.org/wiki/Nash%E2%80%93Sutcliffe_model_efficiency_coefficient">NS coeffecient</a>
   * @note throws runtime exception if supplied arguments differs in .size() or .size()==0
   * @note if obs. is a constant, we get 1/0
   * @note we skip any nans in obs/model
   * @tparam TSA1 a ts accessor for the observed ts ( support .size() and double .value(i))
   * @tparam TSA2 a ts accessor for the observed ts ( support .size() and double .value(i))
   * @param observed_ts contains the observed values for the model
   * @param model_ts contains the (simulated) model output values
   * @return 1- E, given E=n.s,  i.e. 0 is best performance > 0 .. +oo is less good performance.
   */
  template <class TSA1, class TSA2>
  double nash_sutcliffe_goal_function(const TSA1& observed_ts, const TSA2& model_ts) {
    if (observed_ts.size() != model_ts.size() || observed_ts.size() == 0)
      throw runtime_error("nash_sutcliffe needs equal sized ts accessors with elements >1");
    double sum_of_obs_measured_diff2 = 0;
    double obs_avg = 0;
    size_t obs_count = 0;
    for (size_t i = 0; i < observed_ts.size(); ++i) {
      double o = observed_ts.value(i);
      double m = model_ts.value(i);
      if (isfinite(o) && isfinite(m)) {
        double diff_i = o - m;
        sum_of_obs_measured_diff2 += diff_i * diff_i;
        obs_avg += observed_ts.value(i);
        ++obs_count;
      }
    }
    obs_avg /= double(obs_count);
    double sum_of_obs_obs_mean_diff2 = 0;
    for (size_t i = 0; i < observed_ts.size(); ++i) {
      double o = observed_ts.value(i);
      double m = model_ts.value(i);
      if (isfinite(o) && isfinite(m)) {
        double diff_i = o - obs_avg;
        sum_of_obs_obs_mean_diff2 += diff_i * diff_i;
      }
    }
    return sum_of_obs_measured_diff2 / sum_of_obs_obs_mean_diff2;
  }

  /**@brief root mean square effiency coefficient based goal function
   * <a ref href=https://en.wikipedia.org/wiki/Root-mean-square_deviation">RMSE</a>
   * @note throws runtime exception if supplied arguments differs in .size() or .size()==0
   * @note if obs. is a constant, we get 1/0
   * @note we skip any nans in obs/model
   * @tparam TSA1 a ts accessor for the observed ts ( support .size() and double .value(i))
   * @tparam TSA2 a ts accessor for the observed ts ( support .size() and double .value(i))
   * @param observed_ts contains the observed values for the model
   * @param model_ts contains the (simulated) model output values
   * @return RMSE range 0 +oo is best performance > 0 .. +oo is less good performance.
   */
  template <class TSA1, class TSA2>
  double rmse_goal_function(const TSA1& observed_ts, const TSA2& model_ts) {
    if (observed_ts.size() != model_ts.size() || observed_ts.size() == 0)
      throw runtime_error("rmse needs equal sized ts accessors with elements >1");
    double sum_of_obs_measured_diff2 = 0;
    double obs_avg = 0;
    size_t obs_count = 0;
    for (size_t i = 0; i < observed_ts.size(); ++i) {
      double o = observed_ts.value(i);
      double m = model_ts.value(i);
      if (isfinite(o) && isfinite(m)) {
        double diff_i = o - m;
        sum_of_obs_measured_diff2 += diff_i * diff_i;
        obs_avg += o;
        ++obs_count;
      }
    }
    obs_avg /= double(obs_count);
    return obs_count ? sqrt(sum_of_obs_measured_diff2 / obs_count) / obs_avg : nan;
  }

  /** @brief KLING-GUPTA Journal of Hydrology 377
   *              (2009) 80–91, page 83,
   *                     formula (10), where shorthands are
   *                     a=alpha, b=betha, q =sigma, u=my, s=simulated, o=observed
   *
   * @tparam  running_stat_calculator template class like dlib::running_scalar_covariance<double>
   *          that supports .add(x), hten mean_x|_y stddev_x|_y,correlation
   * @tparam TSA1 time-series accessor that supports .size(), .value(i)
   * @tparam TSA2 time-series accessor that supports .size(), .value(i)
   * @param observed_ts the time-series that is the target, observed true value
   * @param model_ts the time-series that is the model simulated/calculated ts
   * @param s_r the kling gupta scale r factor (weight the correlation of goal function)
   * @param s_a the kling gupta scale a factor (weight the relative average of the goal function)
   * @param s_b the kling gupta scale b factor (weight the relative standard deviation of the goal function)
   * @return EDs= 1-KGEs, that have a minimum at zero
   *
   */

  template <class running_stat_calculator, class TSA1, class TSA2>
  double kling_gupta_goal_function(const TSA1& observed_ts, const TSA2& model_ts, double s_r, double s_a, double s_b) {
    running_stat_calculator rs;
    for (size_t i = 0; i < observed_ts.size(); ++i) {
      double tv = observed_ts.value(i);
      double dv = model_ts.value(i);
      if (isfinite(tv) && isfinite(dv))
        rs.add(tv, dv);
    }
    double qo = rs.mean_x();
    double qs = rs.mean_y();
    double us = rs.stddev_y();
    double uo = rs.stddev_x();
    double r = rs.correlation();
    double a = qs / qo;
    double b = us / uo;
    if (!isfinite(a))
      a = 1.0; // could happen in theory if qo is zero
    if (!isfinite(b))
      b = 1.0; // could happen if uo is zero
    // We use EDs to scale, and KGEs = (1-EDs) with max at 1.0, we use 1-KGEs to get minimum as 0 for minbobyqa
    double eds2 = (s_r != 0.0 ? std::pow(s_r * (r - 1), 2) : 0.0) + (s_a != 0.0 ? std::pow(s_a * (a - 1), 2) : 0.0)
                + (s_b != 0.0 ? std::pow(s_b * (b - 1), 2) : 0.0);
    return /*EDs=*/sqrt(eds2);
  }

  /** compute the goal function for the absolute difference of finite-values */
  template <class TSA1, class TSA2>
  double abs_diff_sum_goal_function(const TSA1& observed_ts, const TSA2& model_ts) {
    double abs_diff_sum = 0.0;
    for (size_t i = 0; i < observed_ts.size(); ++i) {
      double tv = observed_ts.value(i);
      double dv = model_ts.value(i);
      if (isfinite(tv) && isfinite(dv))
        abs_diff_sum += std::fabs(tv - dv);
    }
    return abs_diff_sum;
  }

  /** compute the goal function for the absolute difference of finite-values
   *  scaled by the maximum positive or negative values over the same interval
   */
  template <class TSA1, class TSA2, class TSA3>
  double abs_diff_sum_goal_function_scaled(const TSA1& observed_ts, const TSA2& model_ts, const TSA3& model_scale_ts) {
    double abs_diff_sum = 0.0;
    double const scale_eps = 1e-20;
    for (size_t i = 0; i < observed_ts.size(); ++i) {
      double tv = observed_ts.value(i);
      double dv = model_ts.value(i);
      double sv = model_scale_ts.value(i);
      if (isfinite(tv) && isfinite(dv) && isfinite(sv) && std::fabs(sv) > scale_eps) {
        abs_diff_sum += std::fabs(tv - dv) / sv;
      }
    }
    return abs_diff_sum;
  }
}
