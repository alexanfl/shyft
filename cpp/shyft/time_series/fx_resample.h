/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <cmath>
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
  namespace detail {
    using shyft::core::to_seconds;
    using shyft::core::utctime;
    /**
     * @brief resample  a ts using stair-case interpretation
     */
    template <class TS, class TA>
    inline std::vector<double> resample_stair_case_values(TS const &ts, TA const &ta) {
      if (ta.size() == 0)
        return {}; // early exit
      if (ts.size() == 0)
        return std::vector<double>(ta.size(), shyft::nan); // early exit know result
      std::vector<double> r;
      r.reserve(ta.size());
      auto const &sta = ts.time_axis();
      auto p = sta.total_period();
      size_t ix = 0;//usually a safe initial guess
      for (size_t i = 0; i < ta.size(); ++i) {
        auto t = ta.time(i);                                  // direct lookup
        if (p.contains(t)) {                                  // compare
          r.emplace_back(ts.value(ix = sta.index_of(t, ix))); //. index_of() speculative, worstcase bin search
        } else {
          r.push_back(shyft::nan);
        }
      }
      return r;
    }

    /**
     * @brief resample  a ts using linear-between points interpretation
     */

    template <class TS, class TA>
    inline std::vector<double> resample_linear_values(TS const &ts, TA const &ta) {
      if (ta.size() == 0)
        return {}; // early exit
      if (ts.size() == 0)
        return std::vector<double>(ta.size(), shyft::nan); // early exit know result
      std::vector<double> r;
      r.reserve(ta.size());
      auto const &sta = ts.time_axis();
      auto p = sta.total_period();
      size_t ix = 0; // ix hint used to avoid bin-search on breakpoint ts
      for (size_t i = 0; i < ta.size(); ++i) {
        auto t = ta.time(i); // direct lookup
        if (!p.contains(t)) {
          r.emplace_back(shyft::nan);
          continue;
        }
        ix = sta.index_of(t, ix); // first case might be a lin search, if not ix hint hits(0 first, so we usually hit)
        auto t1 = sta.time(ix);
        auto v1 = ts.value(ix);

        if ((ix + 1 >= ts.size()) | (t1 == t)) {
          r.emplace_back(v1); // extend flat
          continue;
        }
        auto v2 = ts.value(ix + 1);
        if (std::isfinite(v2)) {
          auto t2 = sta.time(ix + 1);
          double f = to_seconds(t2 - t) / to_seconds(t2 - t1);
          r.emplace_back(v1 * f + (1.0 - f) * v2);
        } else {
          r.emplace_back(v1);
        }
      }
      return r;
    }
  }

  /**
   * @brief resample values of a ts-like structure using a time-axis
   * @details
   * interpret supplied ts(t) according to its point_interpretation, stair-case or linear between points.
   * Done in an efficient manner, using underlying time-axis.index_of(t,ix_hint) mechanism
   * to avoid binary search for matcing closure of t.
   * pr. definition of time-series is respected, so
   * any value outside ts total_period() is nan.
   * for linear-between interpretation, standard behavior is extend-flat if missing rh value.
   *
   * @tparam TS a time-series like structure
   * @tparam TA a time-axis like structure
   * @param ts of type TS, a ts like structure, have like .size, .time_axis .value.
   * @param ta of type TA, a time-axis like class
   * @return resampled values
   */

  template <class TS, class TA>
  inline std::vector<double> fx_resample(TS const &ts, TA const &ta) {
    return ts.point_interpretation() == POINT_AVERAGE_VALUE
           ? detail::resample_stair_case_values(ts, ta)
           : detail::resample_linear_values(ts, ta);
  }

}
