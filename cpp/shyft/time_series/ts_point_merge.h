/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <algorithm>
#include <utility>

#include <shyft/time_series/point_ts.h>

namespace shyft::time_series {

  /**
   * @brief merge points from b into a
   * @details
   * The result of the merge operation is the unique union set of time-points
   * with corresponding values from ts a or b, b has the priority for
   * equal time-points, replaces points in a.
   *
   * The a.total_period() is extended to max of a and b
   *
   * The function is assumed to be useful in the data-collection or
   * time-series point manipulation tasks
   *
   */
  template <class ts_b>
  void ts_point_merge(point_ts<time_axis::generic_dt>& a, ts_b const & b) {
    if (b.size() == 0)
      return;            // noop
    if (a.size() == 0) { // -> assign
      a.ta = b.time_axis();
      a.v = b.values();
      a.fx_policy = b.point_interpretation(); // kind of ok, practical?
    } else {                                  // a straight forward, not neccessary optimal algorithm for merge:
      vector<utctime> t;
      t.reserve(a.size() + b.size()); // assume worst case
      vector<double> v;
      v.reserve(a.size() + b.size());
      size_t ia = 0;
      size_t ib = 0;
      while (ia < a.size() && ib < b.size()) {
        auto ta = a.time(ia);
        auto tb = b.time(ib);
        if (ta == tb) { // b replaces value in a
          t.emplace_back(tb);
          v.emplace_back(b.value(ib));
          ++ia;
          ++ib;
        } else if (ta < tb) { // a contribute with it's own point
          t.emplace_back(ta);
          v.emplace_back(a.value(ia));
          ++ia;
        } else { // b contribute with it's own point
          t.emplace_back(tb);
          v.emplace_back(b.value(ib));
          ++ib;
        }
      }
      // fill in remaining a or b points(one of them got empty of points above)
      while (ia < a.size()) {
        t.emplace_back(a.time(ia));
        v.emplace_back(a.value(ia++));
      }
      while (ib < b.size()) {
        t.emplace_back(b.time(ib));
        v.emplace_back(b.value(ib++));
      }
      a.v = std::move(v);
      utctime t_end = std::max(a.ta.total_period().end, b.time_axis().total_period().end);
      // check if we can keep a.ta. time-axis type, otherwise just promote to point
      bool optimized_assign = false;
      if (a.ta.gt() == time_axis::generic_dt::FIXED) { // keep fixed
        auto dt = a.ta.f().dt;
        bool aligned = (a.ta.f().t - t.front()).count() % dt.count() == 0
                    && (a.ta.f().t - t_end).count() % dt.count() == 0;
        for (size_t i = 1; aligned && i < t.size(); ++i) {
          if (t[i] - t[i - 1] != dt) {
            aligned = false;
          }
        }
        if (aligned) {
          a.ta = time_axis::generic_dt{t[0], dt, t.size()};
          optimized_assign = true;
        }
      } else if (a.ta.gt() == time_axis::generic_dt::CALENDAR) { // try to keep it..
        utctimespan r1, r2;
        a.ta.c().cal->diff_units(a.ta.c().t, t.front(), a.ta.c().dt, r1);
        a.ta.c().cal->diff_units(a.ta.c().t, t_end, a.ta.c().dt, r2);
        auto aligned = r1.count() == 0 && r2.count() == 0; // promising..
        time_axis::generic_dt nta{a.ta.c().cal, t.front(), a.ta.c().dt, t.size()};
        for (size_t i = 0; aligned && i < nta.size(); ++i) {
          if (nta.time(i) != t[i])
            aligned = false;
        }
        if (aligned) {
          a.ta = nta;
          optimized_assign = true;
        }
      }
      if (!optimized_assign) {
        a.ta = time_axis::generic_dt(std::move(t), t_end);
      }
    }
  }

}
