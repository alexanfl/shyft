/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/fx_average.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;

  /** @brief average_ts, average time-series
   *
   * @details
   * Represents a ts that for
   * the specified time-axis returns the true-average of the
   * underlying specified TS ts.
   *
   * @tparam TS the time-series type
   * @tparam TA the time-axis type
   */
  template <class TS, class TA>
  struct average_ts {
    typedef TA ta_t;
    TA ta;
    TS ts;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;

    const TA& time_axis() const {
      return ta;
    }

    ts_point_fx point_interpretation() const {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      fx_policy = point_interpretation;
    }

    average_ts() = default; // allow default construct

    average_ts(const TS& ts, const TA& ta)
      : ta(ta)
      , ts(ts) {

    } // because true-average of periods is per def. POINT_AVERAGE_VALUE

    // to help average_value method for now!
    point get(size_t i) const {
      return point(ta.time(i), value(i));
    }

    size_t size() const {
      return ta.size();
    }

    size_t index_of(utctime t) const {
      return ta.index_of(t);
    }

    //--
    double value(size_t i) const {
      if (i >= ta.size())
        return nan;
      size_t ix_hint = (i * d_ref(ts).ta.size()) / ta.size(); // assume almost fixed delta-t.
      // TODO: make specialized pr. time-axis average_value, since average of fixed_dt is trivial compared to other ta.
      return average_value(
        ts,
        ta.period(i),
        ix_hint,
        d_ref(ts).fx_policy == ts_point_fx::POINT_INSTANT_VALUE); // also note: average of non-nan areas !
    }

    double operator()(utctime t) const {
      size_t i = ta.index_of(t);
      if (i == string::npos)
        return nan;
      return value(i);
    }

    x_serialize_decl();
  };

  template <class TS, class TA>
  struct is_ts<average_ts<TS, TA>> {
    static bool const value = true;
  };

  template <class TS, class TA>
  struct is_ts<shared_ptr<average_ts<TS, TA>>> {
    static bool const value = true;
  };


}
