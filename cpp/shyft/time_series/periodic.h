/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using std::string;
  using std::vector;
  using std::runtime_error;

  /**@brief A simple profile description defined by start-time and a equidistance by delta-t value-profile.
   *
   * The profile_description is used to define the profile by a vector of double-values.
   * It's first use is in conjunction with creating a time-series with repeated profile
   * that play a role when creating daily bias-profiles for forecast predictions.
   * The class provides t0,dt, ,size() and operator()(i) to access profile values.
   *  -thus we can use other forms of profile-descriptor, not tied to providing the points
   * \sa profile_accessor
   * \sa profile_ts
   */
  struct profile_description {
    utctime t0;
    utctimespan dt;
    std::vector<double> profile;

    profile_description(utctime t0, utctimespan dt, std::vector<double> const & profile)
      : t0(t0)
      , dt(dt)
      , profile(profile) {
    }

    profile_description() {
    }

    size_t size() const {
      return profile.size();
    }

    utctimespan duration() const {
      return dt * size();
    }

    void reset_start(utctime ta0) {
      auto offset = (t0 - ta0) / duration();
      t0 -= offset * duration();
    }

    double operator()(size_t i) const {
      if (i < profile.size())
        return profile[i];
      return nan;
    }

    /** equal within abs_e default 1e-10 */
    bool equal(profile_description const & o, double abs_e = 1e-10) const {
      if (t0 != o.t0 || dt != o.dt || profile.size() != o.profile.size())
        return false;
      for (size_t i = 0; i < profile.size(); ++i) {
        if (!std::isfinite(profile[i]) && !std::isfinite(o.profile[i]))
          continue;
        if (fabs(profile[i] - o.profile[i]) > abs_e)
          return false;
      }
      return true;
    }

    x_serialize_decl();
  };

  /** @brief profile accessor that enables use of average_value etc.
   *
   * The profile accessor provide a 'point-source' compatible interface signatures
   * for use by the average_value-functions.
   * It does so by repeating the profile-description values over the complete specified time-axis.
   * Each time-point within that time-axis can be mapped to an interval/index of the original
   * profile so that it 'appears' as the profile is repeated the number of times needed to cover
   * the time-axis.
   *
   * Currently we also delegate the 'hard-work' of the op(t) and value(i'th period) here so
   * that the periodic_ts, -using profile_accessor is at a minimun.
   *
   * @note that the profile can start anytime before or equal to the time-series time-axis.
   * @remark .. that we should add profile_description as template arg
   */
  template <class TA>
  struct profile_accessor {
    TA ta; ///< The time-axis that we want to map/repeat the profile on to
    profile_description
      profile; ///<< the profile description, we use .t0, dt, duration(),.size(),op(). reset_start_time()
    ts_point_fx fx_policy;

    profile_accessor(profile_description const & pd, const TA& ta, ts_point_fx fx_policy)
      : ta(ta)
      , profile(pd)
      , fx_policy(fx_policy) {
      profile.reset_start(ta.time(0));
    }

    profile_accessor() {
    }

    /** map utctime t to the index of the profile value array/function */
    size_t map_index(utctime t) const {
      return ((t - profile.t0) / profile.dt) % profile.size();
    }

    /** map utctime t to the profile pattern section,
     *  the profile is repeated n-section times to cover the complete time-axis
     */
    size_t section_index(utctime t) const {
      return (t - profile.t0) / profile.duration();
    }

    /** returns the value at time t, taking the point_interpretation policy
     * into account and provide the linear interpolated value between two points
     * in the profile if so specified.
     * If linear between points, and last point is nan, first point in interval-value
     * is returned (flatten out f(t)).
     * If point to the left is nan, then nan is returned.
     */
    double value(utctime t) const {
      auto i = map_index(t);
      if (fx_policy == ts_point_fx::POINT_AVERAGE_VALUE)
        return profile(i);
      //-- interpolate between time(i)    .. t  .. time(i+1)
      //                       profile(i)          profile((i+1) % nt)
      double p1 = profile(i);
      double p2 = profile((i + 1) % profile.size());
      if (!isfinite(p1))
        return nan;
      if (!isfinite(p2))
        return p1; // keep value until nan

      auto s = section_index(t);
      auto ti = profile.t0 + s * profile.duration() + i * profile.dt;
      double w1 = (t - ti) / profile.dt;
      return p1 * (1.0 - w1) + p2 * w1;
    }

    double value(size_t i) const {
      auto p = ta.period(i);
      size_t ix = index_of(p.start); // the perfect hint, matches exactly needed ix
      return average_value(
        *this,
        p,
        ix,
        ts_point_fx::POINT_INSTANT_VALUE == fx_policy); // is it important to extend flat the last point?, false);
    }

    // provided functions to the average_value<..> function
    utcperiod total_period() const {
      return ta.total_period();
    }

    size_t size() const {
      return profile.size() * (1 + ta.total_period().timespan() / profile.duration());
    }

    point get(size_t i) const {
      return point(profile.t0 + i * profile.dt, profile(i % profile.size()));
    }

    utctime time(size_t i) const {
      return get(i).t;
    }

    size_t index_of(utctime t) const {
      return map_index(t) + profile.size() * section_index(t);
    }

    bool equal(profile_accessor const & o, double abs_e = 1e-10) const {
      if (ta != o.ta || fx_policy != o.fx_policy)
        return false;
      return profile.equal(o.profile, abs_e);
    }

    x_serialize_decl();
  };

  /**@brief periodic_ts, periodic pattern time-series
   *
   * Represents a ts that for the specified time-axis returns:
   * - either the instant value of a periodic profile
   * - or the average interpolated between two values of the periodic profile
   */
  template <class TA>
  struct periodic_ts {
    TA ta;
    profile_accessor<TA> pa;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;

    const TA& time_axis() const {
      return ta;
    }

    ts_point_fx point_interpretation() const {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) {
      fx_policy = point_interpretation;
    }

    template <class PD>
    periodic_ts(const PD& pd, const TA& ta, ts_point_fx policy = ts_point_fx::POINT_AVERAGE_VALUE)
      : ta(ta)
      , pa(pd, ta, policy)
      , fx_policy(policy) {
    }

    periodic_ts(vector<double> const & pattern, utctimespan dt, const TA& ta)
      : periodic_ts(profile_description(ta.time(0), dt, pattern), ta) {
    }

    periodic_ts(vector<double> const & pattern, utctimespan dt, utctime pattern_t0, const TA& ta)
      : periodic_ts(profile_description(pattern_t0, dt, pattern), ta) {
    }

    periodic_ts() = default;

    double operator()(utctime t) const {
      return pa.value(t);
    }

    size_t size() const {
      return ta.size();
    }

    utcperiod total_period() const {
      return ta.total_period();
    }

    size_t index_of(utctime t) const {
      return ta.index_of(t);
    }

    double value(size_t i) const {
      return pa.value(i);
    }

    std::vector<double> values() const {
      std::vector<double> v;
      v.reserve(ta.size());
      for (size_t i = 0; i < ta.size(); ++i)
        v.emplace_back(value(i));
      return v;
    }

    bool operator==(periodic_ts const & o) const {
      return fx_policy == o.fx_policy && ta == o.ta && pa.equal(o.pa, 1e-30);
    }

    x_serialize_decl();
  };

}
