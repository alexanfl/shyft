#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <vector>
#include <map>
#include <stdexcept>
#include <string>
#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/time/utctime_utilities.h>

#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::time_series::dd {
  using std::string;
  using std::vector;
  using std::map;
  using std::int64_t;
  using std::to_string;
  using std::runtime_error;
  using shyft::time_series::dd::apoint_ts;
  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using shyft::core::geo_point;

  /** a geo-located time-series
   *
   * First version, using classic mid_point to store x,y,z
   * note that coord sys should be maintained in the
   * encapsulating classes/context.
   * In shyft geo_point is metric.
   */
  struct geo_ts {
    geo_point mid_point; ///< in some cell of a grid,or apoint location.TIN?
    apoint_ts ts;        ///< the time-series

    geo_ts() = default;

    geo_ts(geo_point const &p, apoint_ts const &ats)
      : mid_point{p}
      , ts{ats} {
    }

    bool operator==(geo_ts const &o) const {
      return mid_point == o.mid_point && ts == o.ts;
    }

    bool operator!=(geo_ts const &o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  /** @brief geo_ts_vector
   *
   * a consistent list of geo_ts, e.g. temperatures for a catchment for some 3d representative locations
   */
  using geo_ts_vector = vector<geo_ts>;

  using geo_ts_ens_vector = vector<geo_ts_vector>; ///< ensembles, a vector of geo_ts_vector


  /** @brief geo_ts_var_ens_vector,geo_ts_t0_var_ens_vector
   *
   * Result type for dtss geo evaluate function, that
   * is suitable for further use on the client side
   *
   * e.g. in python we can do this
   * r=geo_dtss.geo_evaluate(geo_ts_db_id='arome',t0_axis=ta,variables=['temperature'..],ens=[0,3,5],...)
   * and then easily construct what a shyft.hydrology region-model needs like this:
   * aregion_env.temperature=TempSourceVector(r[t0_idx][v_idx][e_idx] )
   * where
   *  #0 t0_idx corresponds to t0 forecast time as specified with t0_axis
   *  #1 v_idx correspond to index into request variables ['temperature','precipitation'] -> 0,1..
   *  #2 e_idx correspond to index into ensemble requested, e.g 0,1,2 corresponds to ens [0,3,5] as requested above
   * and the result type is a geo_ts_vector, vector<geo_ts> ..
   *
   * (alt: we could use map with direct keys, but we stick with dense vectors for now)
   */
  using geo_ts_var_ens_vector = vector<geo_ts_ens_vector>; ///< a variable( like temperature,precip) vector, of ens
  using geo_ts_t0_var_ens_vector = vector<geo_ts_var_ens_vector>; ///< for each t0, variable,ens, a geo_ts_vector

}

x_serialize_export_key(shyft::time_series::dd::geo_ts);
