/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/glacier_melt.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /**  glacier_melt_ts as apoint_ts with it's internal being a glacier_melt_ts */
  struct aglacier_melt_ts : ipoint_ts {
    glacier_melt_ts<ipoint_ts_ref> gm;
    //-- default stuff, ct/copy etc goes here
    aglacier_melt_ts() = default;

    //-- useful ct goes here
    aglacier_melt_ts(apoint_ts const & temp, apoint_ts const & sca_m2, double glacier_area_m2, double dtf)
      : gm(temp.ts, sca_m2.ts, glacier_area_m2, dtf) {
    }

    // aglacier_melt_ts(apoint_ts&& ats, utctimespan
    // dt):ts(std::move(ats.ts)),ta(time_axis::time_shift(ats.time_axis(),dt)),dt(dt) {} aglacier_melt_ts(const
    // shared_ptr<ipoint_ts> &ts, utctime dt ):ts(ts),ta(time_axis::time_shift(ts->time_axis(),dt)),dt(dt){}

    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override {
      return gm.fx_policy;
    }

    void set_point_interpretation(ts_point_fx point_interpretation) override {
      gm.fx_policy = point_interpretation;
    }

    gta_t const & time_axis() const override {
      return gm.time_axis();
    }

    utcperiod total_period() const override {
      return gm.time_axis().total_period();
    }

    size_t index_of(utctime t) const override {
      return gm.time_axis().index_of(t);
    }

    size_t size() const override {
      return gm.time_axis().size();
    }

    utctime time(size_t i) const override {
      return gm.time_axis().time(i);
    };

    double value(size_t i) const override {
      return gm.value(i);
    }

    double value_at(utctime t) const override {
      return gm(t);
    }

    vector<double> values() const override {
      vector<double> r;
      r.reserve(size());
      for (size_t i = 0; i < size(); ++i)
        r.push_back(value(i));
      return r;
    }

    bool needs_bind() const override {
      return gm.temperature->needs_bind() || gm.sca_m2->needs_bind();
    }

    void do_bind() override {
      dref(gm.temperature).do_bind();
      dref(gm.sca_m2).do_bind();
    }

    void do_unbind() override {
      dref(gm.temperature).do_unbind();
      dref(gm.sca_m2).do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    // TODO: x_serialize_decl(), ..
  };
}
