#pragma once
#include <memory>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/preprocessor/tuple/to_list.hpp>
#include <boost/preprocessor/list/for_each.hpp>

#include <shyft/core/reflection.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/functional.h>

namespace shyft::time_series::dd {

  /**
   * @brief resolver concept
   * @details
   * The resover takes a  ref to a shared ptr const ipoint_ts, and a symbol,
   * and should return the resolved ts for the symbol, that can be a nullptr, terminal or expression that needs more
   * resolve.
   * @tparam R the invocable type
   */
  template <typename R>
  concept resolver = std::is_invocable_v<R, std::shared_ptr<const ipoint_ts> const &, std::string_view>
    && std::is_same_v<std::remove_cvref_t<std::invoke_result_t<R, std::shared_ptr<const ipoint_ts> const &, std::string_view>>,
                      std::shared_ptr<const ipoint_ts>>;

  template <typename T>
  constexpr bool needs_resolve_v = std::is_same_v<std::remove_cvref_t<T>, aref_ts>;

  /** @brief Substitute symbolic time-series with concrete time-series
   *  @details
   *  Iteratively tries to substitute symbolic time-series with concrete time-series.
   *  Is parametrized on the specific resolution method. The @p resolver is passed a
   *  time-series id and the pointer to the symbolic time-series, and returns either
   *  - A different, resolved, time-series. The returned series will also be resolved.
   *  - The same time-series. The time-series will then be skipped.
   *  - A nullptr, in which case the resolution algorithm will fail.
   *  @param resolver ref concept `resolver`, invocable `shared_ptr<const ipoint_ts>( shared_ptr<const ipoint_ts>
   * &ts,string_view symbol)`
   *  @param root_ts
   *  @returns whether or not resolution was successful
   */
  bool resolve_ts(resolver auto &&resolver, apoint_ts &root_ts) {

    std::vector<std::shared_ptr<ipoint_ts const> *> queue{};

    auto try_push = [&](std::shared_ptr<ipoint_ts const> &ts) {
      if (ts) {
        queue.push_back(std::addressof(ts));
      }
    };

    try_push(root_ts.ts);
    while (!queue.empty()) {

      auto ts_ref = queue.back();
      queue.pop_back();

      auto resolve_or_push_operands = [&]<typename T>(T &&ts) {
        if constexpr (needs_resolve_v<T>) {
          auto &&resolved_ts = resolver(*ts_ref, std::string_view(ts.id));
          if (!resolved_ts)
            return false;
          else if (resolved_ts != *ts_ref)
            try_push(*ts_ref = SHYFT_FWD(resolved_ts));
        } else
          for_each_operands(ts, try_push);
        return true;
      };

      bool ok = with_operation(*ts_ref, resolve_or_push_operands, [] {
        return true;
      });
      if (!ok)
        return false;
    }
    return true;
  }

}
