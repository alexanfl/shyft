/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** @brief time_shift ts do a time-shift dt on the supplied ts
   *
   * @details
   * The values are exactly the same as the supplied ts argument to the constructor
   * but the time-axis is shifted utctimespan dt to the left.
   * e.g.: t_new = t_original + dt
   *
   *       lets say you have a time-series 'a'  with time-axis covering 2015
   *       and you want to time-shift so that you have a time- series 'b'data for 2016,
   *       then you could do this to get 'b':
   *
   *           utc = calendar() // utc calendar
   *           dt  = utc.time(2016,1,1) - utc.time(2015,1,1)
   *            b  = timeshift_ts(a, dt)
   *
   * @note If the ts given at constructor time is an unbound ts or expression,
   *       then .do_bind() needs to be called before any call to
   *       value or time-axis function calls.
   *
   */
  struct time_shift_ts : ipoint_ts {
    ipoint_ts_ref ts;
    gta_t ta;
    utctimespan dt{0}; // despite ta time-axis, we need it

    time_shift_ts() = default;

    //-- useful ct goes here
    time_shift_ts(apoint_ts const & ats, utctimespan adt);
    time_shift_ts(apoint_ts&& ats, utctimespan adt);

    time_shift_ts(ipoint_ts_ref const & ts, utctimespan adt)
      : ts(ts)
      , dt(adt) {
      if (!ts->needs_bind())
        local_do_bind();
    }

    void local_do_bind() {
      if (
        ta.size()
        == 0) { // TODO: introduce bound flag, and use that, using the ta.size() is a problem if ta *is* size 0.
        ta = time_axis::time_shift(ts->time_axis(), dt);
      }
    }

    void local_do_unbind() {
      if (
        ta.size()
        > 0) { // TODO: introduce bound flag, and use that, using the ta.size() is a problem if ta *is* size 0.
        ta = gta_t{};
      }
    }

    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override {
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx point_interpretation) override {
      if (ts)
        dref(ts).set_point_interpretation(point_interpretation);
    }

    gta_t const & time_axis() const override {
      return ta;
    }

    utcperiod total_period() const override {
      return ta.total_period();
    }

    size_t index_of(utctime t) const override {
      return ta.index_of(t);
    }

    size_t size() const override {
      return ta.size();
    }

    utctime time(size_t i) const override {
      return ta.time(i);
    };

    double value(size_t i) const override {
      return ts->value(i);
    }

    double value_at(utctime t) const override {
      return ts->value_at(t - dt);
    }

    vector<double> values() const override {
      return ts->values();
    }

    bool needs_bind() const override {
      return ts->needs_bind();
    }

    void do_bind() override {
      if (ts)
        dref(ts).do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      if (ts)
        dref(ts).do_unbind();
      local_do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::time_series::dd::time_shift_ts);
