/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /**
   * @brief use_time_axis_from_ts evaluate ts a on time_axis of b
   *
   * @details
   * r = a.use_time_axis_from(b)
   * The resulting time-series r have the values of a(t) over the time-axis provided by b.
   * as if
   *  r(t) = a(t) for all t, and t is in range of the b.time-axis
   * or
   *  r.time_axis = b.time_axis
   *  r.value(t) = a.value(t)
   *
   * @note If the ts, a or b,  given at constructor time is an unbound ts or expression,
   *       then .do_bind() needs to be called before any call to
   *       value or time-axis function calls.
   *
   */
  struct use_time_axis_from_ts : ipoint_ts {

    apoint_ts lhs;
    apoint_ts rhs;
    gta_t ta;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE; // how f(t) are mapped to t
    bool bound = false;

    ts_point_fx point_interpretation() const override {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx x) override {
      fx_policy = x;
    }

    void local_do_bind() {
      if (!bound) {
        fx_policy = lhs.point_interpretation();
        ta = rhs.time_axis();
        bound = true;
      }
    }

    void local_do_unbind() {
      if (bound) {
        bound = false;
      }
    }

    use_time_axis_from_ts() = default;

    use_time_axis_from_ts(apoint_ts const & lhs, apoint_ts const & rhs)
      : lhs{lhs}
      , rhs{rhs} {
      if (!needs_bind())
        local_do_bind();
    }

    void bind_check() const {
      if (!bound)
        throw runtime_error("attempting to use unbound timeseries, context fx_time_axis_ts");
    }

    //-- ipoint_ts interface
    utcperiod total_period() const override {
      return time_axis().total_period();
    }

    gta_t const & time_axis() const override {
      bind_check();
      return ta;
    }

    size_t index_of(utctime t) const override {
      return time_axis().index_of(t);
    };

    size_t size() const override {
      return time_axis().size();
    };

    utctime time(size_t i) const override {
      return time_axis().time(i);
    };

    double value_at(utctime t) const override;
    double value(size_t i) const override;
    vector<double> values() const override;

    bool needs_bind() const override {
      return lhs.needs_bind() || rhs.needs_bind();
    }

    void do_bind() override {
      lhs.do_bind();
      rhs.do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      lhs.do_unbind();
      rhs.do_unbind();
      local_do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::time_series::dd::use_time_axis_from_ts);
