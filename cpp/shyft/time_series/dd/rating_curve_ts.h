/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/rating_curve.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** compute water-flow in river based on rating_curves */
  struct rating_curve_ts : ipoint_ts {

    using rct_t = shyft::time_series::rating_curve_ts<apoint_ts>;
    using rc_param_t = shyft::time_series::rating_curve_parameters;

    rct_t ts;

    rating_curve_ts() = default;

    rating_curve_ts(apoint_ts &&ts, rc_param_t &&rcp)
      : ts{std::move(ts), std::move(rcp)} {
    }

    rating_curve_ts(apoint_ts const &ts, rc_param_t const &rcp)
      : ts{ts, rcp} {
    }

    // -----
    virtual ~rating_curve_ts() = default;
    // -----
    rating_curve_ts(rating_curve_ts const &) = default;
    rating_curve_ts &operator=(rating_curve_ts const &) = default;
    // -----
    rating_curve_ts(rating_curve_ts &&) = default;
    rating_curve_ts &operator=(rating_curve_ts &&) = default;

    bool needs_bind() const override {
      return ts.needs_bind();
    }

    void do_bind() override {
      ts.do_bind();
    }

    void do_unbind() override {
      ts.do_unbind();
    }

    // -----
    ts_point_fx point_interpretation() const override {
      return ts.point_interpretation();
    }

    void set_point_interpretation(ts_point_fx policy) override {
      return ts.set_point_interpretation(policy);
    }

    // -----
    size_t size() const override {
      return ts.size();
    }

    utcperiod total_period() const override {
      return ts.total_period();
    }

    gta_t const &time_axis() const override {
      return ts.time_axis();
    }

    // -----
    size_t index_of(utctime t) const override {
      return ts.index_of(t);
    }

    double value_at(utctime t) const override {
      return ts(t);
    }

    // -----
    utctime time(size_t i) const override {
      return ts.time(i);
    }

    double value(size_t i) const override {
      return ts.value(i);
    }

    // -----
    vector<double> values() const override {
      size_t dim = size();
      vector<double> ret;
      ret.reserve(dim);
      for (size_t i = 0u; i < dim; ++i) {
        ret.emplace_back(value(i));
      }
      return ret;
    }

    ipoint_ts_ref evaluate(eval_ctx &ctx, ipoint_ts_ref const &shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx &ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };


}

x_serialize_export_key(shyft::time_series::rating_curve_ts<shyft::time_series::dd::apoint_ts>);
x_serialize_export_key(shyft::time_series::dd::rating_curve_ts);
