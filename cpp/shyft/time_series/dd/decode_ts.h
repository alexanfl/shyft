/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** fast and branch free from https://stackoverflow.com/questions/1392059/algorithm-to-generate-bit-mask */
  template <typename R>
  constexpr R bitmask(unsigned int const onecount) {
    return static_cast<R>(-(onecount != 0)) & (static_cast<R>(-1) >> ((sizeof(R) * CHAR_BIT) - onecount));
  }

  /** @brief  bit_decoder
   *
   *  helper class for decoding bits of integers
   *  into numbers that can play a mathematical/statistical
   *  role in time-series computations.
   *  It is typically used as building block for
   *  extracting extra information from remote sensors/signals
   *  that provides both a value, and some kind of bit-encoded
   *  integer representing status-flags, like:
   *   battery_level_low, sensor signal stale, AD-converter failure etc.
   */
  struct bit_decoder {
    uint64_t start_bit{0}; ///< the start bit into the integer.
    uint64_t bit_mask{0};  ///< the bitmask used to extract only relevant bits.

    /** count the bits from mask */
    unsigned int n_bits() const {
      unsigned int n = 0;
      auto x = bit_mask;
      while (x & 1) {
        ++n;
        x >>= 1;
      }
      return n;
    }

    bit_decoder() = default;

    bit_decoder(unsigned int start_bit, unsigned int n_bits)
      : start_bit{start_bit}
      , bit_mask{bitmask<uint64_t>(n_bits)} {
    }

    /** return the decoded value of x, or nan if errors detected.
     *  x==nan, -> nan
     *  x < 0: -> nan
     *  x > 2<<52 -> nan (ref https://en.wikipedia.org/wiki/Double-precision_floating-point_format)
     *  else:
     *    return bit decoded value of start-bit plus n-bits as unsigned number casted to double
     *
     */
    inline double decode(double x) const {
      if (!isfinite(x))
        return shyft::nan;
      if (x < 0)
        return shyft::nan;
      if (x > double(uint64_t(1) << 52))
        return shyft::nan;
      return double((uint64_t(x) >> start_bit) & bit_mask);
    }

    bool operator==(bit_decoder const & o) const {
      return start_bit == o.start_bit && bit_mask == o.bit_mask;
    }

    // binary serialization, so no x_serialize_decl();
  };

  /** @brief The inside_ts maps a value range into 1.0 and 0.0
   *
   * The decode_ts provide needed function to transform the source time-series
   * into a sequence of is_inside_value and is_outside_value
   * based on a range-criteria [min .. max >
   */
  struct decode_ts : ipoint_ts {
    ipoint_ts_ref ts; ///< the source ts
    bit_decoder p;    ///< the parameters that control how the inside is done

    // useful constructors

    decode_ts(apoint_ts const & ats)
      : ts(ats.ts) {
    }

    decode_ts(apoint_ts&& ats)
      : ts(std::move(ats.ts)) {
    }

    decode_ts(apoint_ts const & ats, bit_decoder const & qp)
      : ts(ats.ts)
      , p(qp) {
    }

    // std copy ct and assign
    decode_ts() = default;

    void assert_ts() const {
      if (!ts)
        throw runtime_error("decode_ts:source ts is null");
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    ts_point_fx point_interpretation() const override {
      assert_ts();
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx pfx) override {
      assert_ts();
      dref(ts).set_point_interpretation(pfx);
    }

    gta_t const & time_axis() const override {
      assert_ts();
      return ts->time_axis();
    }

    utcperiod total_period() const override {
      assert_ts();
      return ts->time_axis().total_period();
    }

    size_t index_of(utctime t) const override {
      assert_ts();
      return ts->index_of(t);
    }

    size_t size() const override {
      return ts ? ts->size() : 0;
    }

    utctime time(size_t i) const override {
      assert_ts();
      return ts->time(i);
    };

    // methods that needs special implementation according to inside rules
    virtual double value(size_t i) const override;
    virtual double value_at(utctime t) const override;
    vector<double> values() const override;

    // methods for binding and symbolic ts
    bool needs_bind() const override {
      return ts ? ts->needs_bind() : false;
    }

    void do_bind() override {
      if (ts)
        dref(ts).do_bind();
    }

    void do_unbind() override {
      if (ts)
        dref(ts).do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_binary(shyft::time_series::dd::bit_decoder);
x_serialize_export_key(shyft::time_series::dd::decode_ts);
