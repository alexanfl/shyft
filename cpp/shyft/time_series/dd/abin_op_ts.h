/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** The iop_t represent the basic 'binary' operation,
   *   a stateless function that takes two doubles and returns the binary operation.
   *   E.g.: a+b
   *   The iop_t is used as the operation element of the abin_op_ts class
   */
  enum iop_t : int8_t {
    OP_NONE,
    OP_ADD,
    OP_SUB,
    OP_DIV,
    OP_MUL,
    OP_MIN,
    OP_MAX,
    OP_POW,
    OP_LOG
  };

  /** do_bind helps to defer the computation cost of the
   * expression bin-op variants until its actually used.
   * this is also needed when having ts_refs| unbound symbolic time-series references
   * that we would like to serialize and pass over to another server for execution.
   *
   * By inspecting the time-series in the construction phase (bin_op etc.)
   * we try to take the preparation for computation as early as possible,
   * so, only when there is a symbolic time-series reference, there will be
   * a do_bind() that will take action *after* the
   * symbolic time-series has been prepared with real values (bound).
   *
   */

  /** @brief The binary operation for type ts op ts
   *
   * The binary operation is lazy, and only keep the reference to the two operands
   * that are of the @ref apoint_ts type.
   * The operation is of @ref iop_t, and details for plus minus divide multiply etc is in
   * the implementation file.
   *
   * As per definition a this class implements the @ref ipoint_ts interface,
   * and the time-axis of type @ref gta_t is currently computed in the constructor.
   * This could take some cpu if the time-axis is of type point_dt, so we could
   * consider working some more on the internal algorithms to avoid this.
   *
   * The @ref ts_point_fx is computed based on rhs,lhs. But can be overridden
   * by the user.
   *
   */
  struct abin_op_ts : ipoint_ts {

    apoint_ts lhs;
    iop_t op = iop_t::OP_NONE;
    apoint_ts rhs;
    gta_t ta;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;
    bool bound = false;

    ts_point_fx point_interpretation() const override {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx x) override {
      fx_policy = x;
    }

    void local_do_bind() {
      if (!bound) {
        fx_policy = result_policy(lhs.point_interpretation(), rhs.point_interpretation());
        ta = time_axis::combine(lhs.time_axis(), rhs.time_axis());
        bound = true;
      }
    }

    void local_do_unbind() {
      if (bound) {
        bound = false;
      }
    }

    abin_op_ts() = default;

    abin_op_ts(apoint_ts const & lhs, iop_t op, apoint_ts const & rhs)
      : lhs(lhs)
      , op(op)
      , rhs(rhs) {
      if (!needs_bind())
        local_do_bind();
    }

    void bind_check() const {
      if (!bound)
        throw runtime_error("attempting to use unbound timeseries, context abin_op_ts");
    }

    utcperiod total_period() const override {
      return time_axis().total_period();
    }

    gta_t const & time_axis() const override {
      bind_check();
      return ta;
    } // combine lhs,rhs

    size_t index_of(utctime t) const override {
      return time_axis().index_of(t);
    }

    size_t size() const override {
      return time_axis().size();
    } // use the combined ta.size();

    utctime time(size_t i) const override {
      return time_axis().time(i);
    } // return combined ta.time(i)

    double value_at(utctime t) const override;
    double value(size_t i) const override; // return op( lhs(t), rhs(t)) ..
    vector<double> values() const override;

    bool needs_bind() const override {
      return lhs.needs_bind() || rhs.needs_bind();
    }

    void do_bind() override {
      lhs.do_bind();
      rhs.do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      lhs.do_unbind();
      rhs.do_unbind();
      local_do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

  /** @brief  binary operation for type ts op double
   *
   * The resulting time-axis and point interpretation policy is equal to the ts.
   */
  struct abin_op_scalar_ts : ipoint_ts {
    double lhs;
    iop_t op = iop_t::OP_NONE;
    apoint_ts rhs;
    gta_t ta;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;
    bool bound = false;

    ts_point_fx point_interpretation() const override {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx x) override {
      fx_policy = x;
    }

    void local_do_bind() {
      if (!bound) {
        ta = rhs.time_axis();
        fx_policy = rhs.point_interpretation();
        bound = true;
      }
    }

    void local_do_unbind() {
      if (bound) {
        bound = false;
      }
    }

    void bind_check() const {
      if (!bound)
        throw runtime_error("attempting to use unbound timeseries, context abin_op_scalar");
    }

    abin_op_scalar_ts() = default;

    abin_op_scalar_ts(double lhs, iop_t op, apoint_ts const & rhs)
      : lhs(lhs)
      , op(op)
      , rhs(rhs) {
      if (!needs_bind())
        local_do_bind();
    }

    utcperiod total_period() const override {
      return time_axis().total_period();
    }

    gta_t const & time_axis() const override {
      bind_check();
      return ta;
    }; // combine lhs,rhs

    size_t index_of(utctime t) const override {
      return time_axis().index_of(t);
    };

    size_t size() const override {
      return time_axis().size();
    };

    utctime time(size_t i) const override {
      return time_axis().time(i);
    };

    double value_at(utctime t) const override;
    double value(size_t i) const override;
    vector<double> values() const override;

    bool needs_bind() const override {
      return rhs.needs_bind();
    }

    void do_bind() override {
      rhs.do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      rhs.do_unbind();
      local_do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

  /** @brief  binary operation for type ts op double
   *
   * The resulting time-axis and point interpretation policy is equal to the ts.
   */
  struct abin_op_ts_scalar : ipoint_ts {
    apoint_ts lhs;
    iop_t op = iop_t::OP_NONE;
    double rhs;
    gta_t ta;
    bool bound = false;
    ts_point_fx fx_policy = POINT_AVERAGE_VALUE;

    ts_point_fx point_interpretation() const override {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx x) override {
      fx_policy = x;
    }

    void local_do_bind() {
      if (!bound) {
        ta = lhs.time_axis();
        fx_policy = lhs.point_interpretation();
        bound = true;
      }
    }

    void local_do_unbind() {
      bound = false;
    }

    void bind_check() const {
      if (!bound)
        throw runtime_error("attempting to use unbound timeseries, context abin_op_ts_scalar");
    }

    abin_op_ts_scalar() = default;

    abin_op_ts_scalar(apoint_ts const & lhs, iop_t op, double rhs)
      : lhs(lhs)
      , op(op)
      , rhs(rhs) {
      if (!needs_bind())
        local_do_bind();
    }

    utcperiod total_period() const override {
      return time_axis().total_period();
    }

    gta_t const & time_axis() const override {
      bind_check();
      return ta;
    };

    size_t index_of(utctime t) const override {
      return time_axis().index_of(t);
    };

    size_t size() const override {
      return time_axis().size();
    };

    utctime time(size_t i) const override {
      return time_axis().time(i);
    };

    double value_at(utctime t) const override;
    double value(size_t i) const override;
    vector<double> values() const override;

    bool needs_bind() const override {
      return lhs.needs_bind();
    }

    void do_bind() override {
      lhs.do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      lhs.do_unbind();
      local_do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::time_series::dd::abin_op_scalar_ts);
x_serialize_export_key(shyft::time_series::dd::abin_op_ts);
x_serialize_export_key(shyft::time_series::dd::abin_op_ts_scalar);
