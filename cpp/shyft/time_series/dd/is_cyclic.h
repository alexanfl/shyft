#pragma once
#include <memory>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/container/flat_set.hpp>

#include <shyft/core/reflection.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/functional.h>
#include <shyft/time_series/dd/resolve_ts.h>

namespace shyft::time_series::dd {
  namespace detail {
    bool is_cyclic_subgraph(
      resolver auto &&resolver,
      std::shared_ptr<ipoint_ts const> const &root_ts,
      boost::container::flat_set<void *> &seen) {
      if (!root_ts) {
        // empty, is not cyclical
        return false;
      }
      auto *addr = (void *) root_ts.get();
      auto [itr, emplaced] = seen.emplace(addr);
      if (!emplaced) {
        // we have seen this ts before!!
        return true;
      }
      auto is_c_flattened = [&](bool is_cyclic, std::shared_ptr<ipoint_ts const> const &ts) {
        // short return if is_cyclic is true already
        return is_cyclic || is_cyclic_subgraph(resolver, ts, seen);
      };
      auto go_through_operands = [&]<typename T>(T &&ts) {
        if constexpr (needs_resolve_v<T>) {
          auto &&resolved_ts = resolver(root_ts, std::string_view(ts.id));
          if (resolved_ts == root_ts) {
            // direct cycle
            return true;
          }
          return is_c_flattened(false, resolved_ts);
        }
        return fold_operands(SHYFT_FWD(ts), false, is_c_flattened);
      };

      bool is_cyclic = with_operation(root_ts, go_through_operands, [] {
        return false;
      });
      seen.erase(addr);
      return is_cyclic;
    }
  }

  /**
   * @brief check if a timeseries expression contains a cycle
   *
   * @param resolver function to resolve symbolic references.
   * @param root_ts root expression
   */
  bool is_cyclic(resolver auto &&resolver, apoint_ts const &root_ts) {
    boost::container::flat_set<void *> seen;
    return detail::is_cyclic_subgraph(SHYFT_FWD(resolver), root_ts.ts, seen);
  }

}
