/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <stdexcept>
#include <unordered_map>
#include <fmt/format.h>
#include <shyft/time_series/dd/resolve_tsv.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::time_series::dd {
  // for doc, read header file
  void resolve_symbols(ats_vector &atsv, resolve_fx resolve, size_t recurse_level, size_t recurse_level_max) {
    if (recurse_level++ >= recurse_level_max)
      throw std::runtime_error(
        fmt::format("circular reference? resolve symbols recurse level max reached {}", recurse_level));

    std::unordered_map<std::string, std::vector<ts_bind_info>>
      ts_bind_map;                                 // ensure we only resolve one ref one time, regardless multiple refs
    std::vector<std::string> ts_id_list;           // what we need to apparantly resolve at this level
    std::vector<std::vector<ts_bind_info>> ats_bi; // the ats_bi[i] correspond to atsv[i] find bind-info
    // step 1: bind not yet bound time-series (ts with only symbol, needs to be resolved using bind_cb)
    for (auto &ats : atsv) {
      if (!ats.needs_bind()) { // this could break bw.compat, but is reasonable!(could cause inconsistencies if multi
                               // refs,bound/unboud)
        ats_bi.emplace_back(std::vector<ts_bind_info>{}); // push empty entry
        continue; // notice: it will not attempt to re-read/re-resolve timeseries that are bound (have refs, but they
                  // are bound)
      }
      ats_bi.emplace_back(
        ats.find_ts_bind_info()); // stash away the this entry, we need it later when completing the bind cycle
      // prune out those entries, that !.needs_bind(), the find_ts_bind_info currently gives all refs, regardless bind
      // status
      std::erase_if(ats_bi.back(), [](ts_bind_info const &x) {
        return !x.ts.needs_bind();
      });
      for (auto const &bi : ats_bi.back()) {                       // then for the remaining entries..:
        if (ts_bind_map.find(bi.reference) == ts_bind_map.end()) { // maintain unique set
          ts_id_list.push_back(bi.reference);
          ts_bind_map[bi.reference] = std::vector<ts_bind_info>();
        }
        ts_bind_map[bi.reference].push_back(bi);
      }
    }

    // step 2: (optional) bind_ts callback should resolve symbol time-series with content
    if (ts_bind_map.empty())
      return; // no more binds needed, we are done.
    // step 2 required:

    auto bts = resolve(ts_id_list); // resolve the one missing, either dtss, or dstm
    if (bts.size() != ts_id_list.size())
      throw std::runtime_error(fmt::format("failed to bind all of {} ts", bts.size()));
    ats_vector
      nested_expr; // check for nested expressions(e.g. dstm) that needs to be resolved recursively, but in bulk
    for (size_t i = 0; i < bts.size(); ++i) {
      if (bts[i].ts == nullptr) // sanity check goes here: we require resolve to actually do resolve all syms.
        throw std::runtime_error(fmt::format(
          "resolve_symbols: at recurse level {} got back nullptr from resolving '{}' ", recurse_level, ts_id_list[i]));
      if (bts[i].needs_bind())
        nested_expr.emplace_back(bts[i]);
    }
    if (!nested_expr.empty()) { // notice that we collect *all* next-level expr. for resolve, to allow bulk read in
                                // levels instead of 1 by 1.
      resolve_symbols(nested_expr, resolve, recurse_level, recurse_level_max); // recursively resolve nested expressions
      // notice that nested_expr have shared ptr. to the atsv, so it is inplace resolved into whatever expressions of
      // that original atsv
    }
    for (std::size_t i = 0; i < ts_id_list.size(); ++i) {
      for (auto &bi : ts_bind_map[ts_id_list[i]]) {
        bts[i].do_bind(); /// in case its half bound.. then finish it here, because next line require it
        bi.ts.bind(bts[i]);
      }
    }
    // step 3. (given there was work to do), finish off the bind phase for the completed deps
    for (size_t i = 0; i < atsv.size(); ++i) {
      if (ats_bi[i].empty())
        continue;
      atsv[i].do_bind();
    }
  };
}
