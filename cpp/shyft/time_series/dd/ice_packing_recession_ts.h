/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** @brief  controls a simple ice_packing recession
   *
   * This class keep the two minimal parameters for
   * the recession shape to be used while ice_packing is active.
   *
   * The recession formula is given by
   *
   *  f(t) = qbf + (qs - qbf) * std::exp(-alpha * (t - ts))
   *
   *  where
   *    qbf -> recession_minium [m**3/s]
   *    qs  -> flow observed just before start of ice-packing
   *    ts  -> time when ice-packing starts (in seconds,epoch)
   *    t   -> t time (in seconds, epoch, >= ts)
   *    alpha -> the alpha parameter (unit 1/s)
   *
   * so
   *   when time t =  ts,  then f(t) = qs
   *   when time t -> +oo, then f(t) -> qbf
   *
   */
  struct ice_packing_recession_parameters {
    double alpha{0.0};             ///< recession factor, unit [1/s], default flat(no recession)
    double recession_minimum{0.0}; ///< unit [m**3/s] minimum floor value for recession, default 0.0 m/s

    ice_packing_recession_parameters() = default;

    // -----
    ice_packing_recession_parameters(double alpha, double recession_minimum)
      : alpha{alpha}
      , recession_minimum{recession_minimum} {
    }

    // -----
    ~ice_packing_recession_parameters() = default;
    // -----
    ice_packing_recession_parameters(ice_packing_recession_parameters const &) = default;
    ice_packing_recession_parameters &operator=(ice_packing_recession_parameters const &) = default;
    // -----
    ice_packing_recession_parameters(ice_packing_recession_parameters &&) = default;
    ice_packing_recession_parameters &operator=(ice_packing_recession_parameters &&) = default;

    bool operator==(ice_packing_recession_parameters const &other) const noexcept {
      return alpha == other.alpha && epsilon_difference(recession_minimum, other.recession_minimum) < 2.0;
    }

    x_serialize_decl();
  };

  /** @brief ice-packing recession time-series
   *
   * The purpose of this time-series is to provide an all in one
   * package taking an ice-packing signal time-series
   * probably of type ice_packing_ts (but not necessary),
   * and providing recession values according to
   * specified parameters whenever the ice-packing ts
   * provide a signal that there is ongoing ice-packing.
   *
   */
  struct ice_packing_recession_ts : ipoint_ts {

    using iprp_t = ice_packing_recession_parameters;

    apoint_ts flow_ts;        ///< flow in [m**3/s] units
    apoint_ts ice_packing_ts; ///< ice-packing indicator, in 0..1 units (1.0->ice-packing).
    // -----
    iprp_t ipr_param; ///< recession parameters to control the shape during ice-packing
    // -----
    ts_point_fx fx_policy =
      ts_point_fx::POINT_INSTANT_VALUE; ///< ts-policy, questionable it should/could reflect underlying ts
    // -----
    bool bound = true; ///< internal to keep bound/unbound state, def. true since null apoint_ts is terminals

    ice_packing_recession_ts() = default; // all variables above get defaults, then bound must be true

    // -----
    template < class TS_A, class TS_B, class IPRP >
    ice_packing_recession_ts(
      TS_A &&flow_ts,
      TS_B &&ice_packing_ts,
      IPRP &&iprp,
      ts_point_fx fx_policy = ts_point_fx::POINT_INSTANT_VALUE)
      : flow_ts{std::forward<TS_A>(flow_ts)}
      , ice_packing_ts{std::forward<TS_B>(ice_packing_ts)}
      , ipr_param{std::forward<IPRP>(iprp)}
      , fx_policy{fx_policy}
      , bound{false} // bound false here because we need to check below

    {
      if (!flow_ts.needs_bind() && !ice_packing_ts.needs_bind())
        local_do_bind();
    }

    // -----
    virtual ~ice_packing_recession_ts() = default;
    // -----
    ice_packing_recession_ts(ice_packing_recession_ts const &) = default;
    ice_packing_recession_ts &operator=(ice_packing_recession_ts const &) = default;
    // -----
    ice_packing_recession_ts(ice_packing_recession_ts &&) = default;
    ice_packing_recession_ts &operator=(ice_packing_recession_ts &&) = default;

    // dispatch
    bool needs_bind() const override {
      return !bound;
    }

    void do_bind() override {
      if (!bound) {
        flow_ts.do_bind();
        ice_packing_ts.do_bind();
        local_do_bind();
      }
    }

    void do_unbind() override {
      if (bound) {
        flow_ts.do_unbind();
        ice_packing_ts.do_unbind();
        local_do_unbind();
      }
    }
   private: // dispatch impl
    void local_do_bind() {
      fx_policy = d_ref(flow_ts).point_interpretation();
      bound = true;
    }

    void local_do_unbind() {
      bound = false;
    }

    void ensure_bound() const {
      if (!bound) {
        throw runtime_error("ice_packing_recession_ts: access to not yet bound ts attempted");
      }
    }

   public: // api
    ts_point_fx point_interpretation() const override {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx policy) override {
      fx_policy = policy;
    }

    // -----
    size_t size() const override {
      return flow_ts.size();
    }

    utcperiod total_period() const override {
      return flow_ts.total_period();
    }

    gta_t const &time_axis() const override {
      return flow_ts.time_axis();
    }

    // -----
    size_t index_of(utctime t) const override {
      return flow_ts.index_of(t);
    }

    double value_at(utctime t) const override {
      return evaluate(t);
    }

    // -----
    utctime time(size_t i) const override {
      return flow_ts.time(i);
    }

    double value(size_t i) const override {
      return evaluate(time(i));
    }

    /** @note this is terribly inefficient..*/
    vector<double> values() const override {
      size_t dim = size();
      vector<double> ret;
      ret.reserve(dim);
      for (size_t i = 0; i < dim; ++i) {
        ret.emplace_back(value(i));
      }
      return ret;
    }

    ipoint_ts_ref evaluate(eval_ctx &ctx, ipoint_ts_ref const &shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx &ctx) const override;

   private:
    double evaluate(utctime t) const {
      ensure_bound();
      ensure_overlap();
      double ice = ice_packing_ts(t);
      if (!isfinite(ice)) { // should be policy driven! if we know it's summer...
        return shyft::nan;
      }

      double const indicator_limit = 0.5; // the indicator time-series is usually 0.0, or 1.0(packing), or nan (error).
      if (ice > indicator_limit) {        // Ok, ice is packing up, we need to get the flow before it started.
        // We *must* use the flow ts
        // and regardless flow shape (linear /staircase)
        // find the last flow point where there is no ice
        size_t f_ix = flow_ts.index_of(t);            // locate left flow value
        assert(f_ix != string::npos);                 // there should be one
        while (f_ix > 0 && ice > indicator_limit) {   // find where there is no ice
          ice = ice_packing_ts(flow_ts.time(--f_ix)); // this is rather high cost search..
          if (!isfinite(ice)) {                       // hmm.. policy driven.. and why give up here,?
            return shyft::nan;                        // ... we could just search back to next point..
          }
        }
        double const qs = flow_ts.value(f_ix); // last flow value with no ice, or start value
        const utctime ts = flow_ts.time(f_ix);
        // Compute recession from packing_start_time until now
        double const qbf = ipr_param.recession_minimum;
        double const alpha = ipr_param.alpha;

        return qbf + (qs - qbf) * std::exp(-alpha * to_seconds(t - ts));
      } else {
        return flow_ts(t);
      }
    }

    /// Ensure that the ice packing ts and the flow ts overlaps correctly.
    /// In short the flow series should either equal or be contained in
    /// the ice packing series.
    void ensure_overlap() const {
      if (!ice_packing_ts.total_period().contains(flow_ts.total_period())) {
        throw runtime_error(
          "ice_packing_recession_ts: total period of flow ts should equal or be contained in ice packing ts total "
          "period");
      }
    }

    string stringify() const override;

    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::time_series::dd::ice_packing_recession_parameters);
x_serialize_export_key(shyft::time_series::dd::ice_packing_recession_ts);
