/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/fx_average.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;                // fwd

  /** @brief The average_ts is used for providing ts average values over a time-axis
   *
   * Given a any ts, concrete, or an expression, provide the true average values on the
   * intervals as provided by the specified time-axis.
   *
   * true average for each period in the time-axis is defined as:
   *
   *   integral of f(t) dt from t0 to t1 / (t1-t0)
   *
   * using the f(t) interpretation of the supplied ts (linear or stair case).
   *
   * The @ref ts_point_fx is always POINT_AVERAGE_VALUE for the result ts.
   *
   * @note if a nan-value intervals are excluded from the integral and time-computations.
   *       E.g. let's say half the interval is nan, then the true average is computed for
   *       the other half of the interval.
   *
   */
  struct average_ts : ipoint_ts {
    gta_t ta;
    ipoint_ts_ref ts;

    // useful constructors
    average_ts(gta_t&& ta, apoint_ts const & ats)
      : ta(std::move(ta))
      , ts(ats.ts) {
    }

    average_ts(gta_t&& ta, apoint_ts&& ats)
      : ta(std::move(ta))
      , ts(std::move(ats.ts)) {
    }

    average_ts(gta_t const & ta, apoint_ts&& ats)
      : ta(ta)
      , ts(std::move(ats.ts)) {
    }

    average_ts(gta_t const & ta, apoint_ts const & ats)
      : ta(ta)
      , ts(ats.ts) {
    }

    average_ts(gta_t const & ta, ipoint_ts_ref const & ts)
      : ta(ta)
      , ts(ts) {
    }

    average_ts(gta_t&& ta, ipoint_ts_ref const & ts)
      : ta(std::move(ta))
      , ts(ts) {
    }

    // std copy ct and assign
    average_ts() = default;

    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override {
      return ts_point_fx::POINT_AVERAGE_VALUE;
    }

    void set_point_interpretation(ts_point_fx /*point_interpretation*/) override {
      ;
    }

    gta_t const & time_axis() const override {
      return ta;
    }

    utcperiod total_period() const override {
      return ta.total_period();
    }

    size_t index_of(utctime t) const override {
      return ta.index_of(t);
    }

    size_t size() const override {
      return ta.size();
    }

    utctime time(size_t i) const override {
      return ta.time(i);
    };

    double value(size_t i) const override {
      size_t ix_hint = (i * ts->size()) / ta.size(); // assume almost fixed delta-t.
      return average_value(*ts, ta.period(i), ix_hint, ts->point_interpretation() == ts_point_fx::POINT_INSTANT_VALUE);
    }

    double value_at(utctime t) const override {
      // return true average at t
      if (!ta.total_period().contains(t))
        return nan;
      return value(index_of(t));
    }

    vector<double> values() const override;

    bool needs_bind() const override {
      return ts->needs_bind();
    }

    void do_bind() override {
      dref(ts).do_bind();
    }

    void do_unbind() override {
      dref(ts).do_unbind();
    }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr< shyft::time_series::dd::ipoint_ts > clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::time_series::dd::average_ts);
