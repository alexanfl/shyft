/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** @brief bucket parameter
   *
   * To support global use (in case there are precip. buckets around the world,
   *  with the same defects as in the nordic countries (!)),
   * we keep the hour_offset that helps resolving the daily temperature variation pattern.
   * The bucket_empty_limit is the largest negative drop we allow in the algorithm to remove
   * the points where there has been an emptying of the measurement bucket.
   */
  struct bucket_parameter {
    utctime hour_offset{0ul};
    double bucket_empty_limit{-10};
    bucket_parameter() = default;

    bucket_parameter(utctime t, double bel)
      : hour_offset{t}
      , bucket_empty_limit{bel} {
    }

    bool operator==(bucket_parameter const & o) const {
      return hour_offset == o.hour_offset && (fabs(bucket_empty_limit - o.bucket_empty_limit) < 1e-8);
    }

    bool operator!=(bucket_parameter const & o) const {
      return !operator==(o);
    }
  };

  /** @brief precipitation bucket handling
   *
   * Precipitation bucket measurements have a lot of tweaks that needs to be resolved,
   * including negative variations over the day due to faulty temperature-dependent
   * volume/weight sensors attached.
   *
   * A precipitation bucket accumulates precipitation, so the readings should be strictly
   * increasing by time, until the bucket is emptied (full, or as part of maintenance).
   *
   * The goal for the bucket_ts is to provide *hourly* precipitation, based on some input signal
   * that usually is hourly(averaging is used if not hourly).
   *
   * The main strategy is to use 24 hour differences (typically at hours in a day where the
   * temperature is low, like early in the morning.), to adjust the hourly volume.
   *
   * Differences in periods of 24hour are distributed on all positive hourly evenets, the
   * negative derivatives are zeroed out, so that the hourly result for each 24 hour
   * is steady increasing, and equal to the difference of the 24hour area.
   *
   * The derivative is then usdo_local_binded to compute the hourly precipitation rate in mm/h
   *
   */
  struct bucket_ts : ipoint_ts {
    ipoint_ts_ref ts;   ///< the source ts
    bucket_parameter p; ///< the parameters that control how the inside is done
    gta_t ta;           ///< the hourly timeaxis for this ts,usually equal to source ts.
    bool bound{false};

    // useful constructors
    void do_early_bind() {
      if (ts && !ts->needs_bind())
        local_do_bind();
    }

    // bucket_ts(const apoint_ts& ats) :ts(ats.ts) { do_early_bind(); }
    // bucket_ts(apoint_ts&& ats) :ts(std::move(ats.ts)) { do_early_bind(); }
    bucket_ts(apoint_ts const & ats, bucket_parameter const & qp);

    // std copy ct and assign
    bucket_ts() = default;

    void assert_ts() const {
      if (!ts)
        throw runtime_error("bucket_ts:source ts is null");
    }

    void assert_bound() const {
      if (!bound)
        throw runtime_error("bucket_ts:attemt to use method on unbound ts");
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    ts_point_fx point_interpretation() const override {
      return ts_point_fx::POINT_AVERAGE_VALUE;
    }

    void set_point_interpretation(ts_point_fx) override {
    }

    gta_t const & time_axis() const override {
      assert_bound();
      return ta;
    }

    utcperiod total_period() const override {
      assert_bound();
      return ta.total_period();
    }

    size_t index_of(utctime t) const override {
      assert_bound();
      return ta.index_of(t);
    }

    size_t size() const override {
      assert_bound();
      return ta.size();
    }

    utctime time(size_t i) const override {
      assert_bound();
      return ta.time(i);
    }

    // methods that needs special implementation according to inside rules
    double value(size_t i) const override;
    double value_at(utctime t) const override;
    vector<double> values() const override;
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;

    // methods for binding and symbolic ts
    bool needs_bind() const override {
      return ts ? ts->needs_bind() : false;
    }

    void do_bind() override {
      if (ts)
        dref(ts).do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      if (ts)
        dref(ts).do_unbind();
      local_do_unbind();
    }

    bool hour_time_axis(time_axis::generic_dt const & ta) const {
      switch (ta.gt()) {
      case time_axis::generic_dt::POINT:
        return false;
      case time_axis::generic_dt::FIXED:
        return ta.dt() == seconds(3600);
      case time_axis::generic_dt::CALENDAR:
        return ta.dt() == seconds(3600);
      }
      return false;
    }

    void local_do_bind() {
      if (!bound && ts) {
        ta = ts->time_axis();
        if (!hour_time_axis(ta)) {
          auto dt = seconds(3600);
          auto t0 = floor(ta.total_period().start, dt);
          auto tn = floor(ta.total_period().end + dt - utctime(1ul), dt);
          auto n = static_cast<size_t>((tn - t0) / dt);
          ta = time_axis::generic_dt(t0, seconds(dt), n);
        }
        bound = true;
      }
    }

    void local_do_unbind() {
      if (bound) {
        bound = false;
      }
    }

    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_binary(shyft::time_series::dd::bucket_parameter);
x_serialize_export_key(shyft::time_series::dd::bucket_ts);
