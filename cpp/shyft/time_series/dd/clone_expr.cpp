#include <shyft/time_series/time_series_dd.h>

namespace shyft::time_series::dd {
  using std::shared_ptr;
  using std::runtime_error;
  using std::make_shared;

  shared_ptr<ipoint_ts> gpoint_ts::clone_expr() const {
    throw runtime_error("attempt to clone gpoint_ts");
  }

  shared_ptr<ipoint_ts> periodic_ts::clone_expr() const {
    throw runtime_error("attempt to clone periodic_ts");
  }

  shared_ptr<ipoint_ts> aref_ts::clone_expr() const {
    if (needs_bind())
      return make_shared<aref_ts>(*this);
    throw runtime_error("aref_ts: attempt to clone bound expr");
  }

  template <class T>
  static shared_ptr<ipoint_ts> mk_clone_lhs_rhs(T const &self, char const *msg) {
    if (self.needs_bind()) {
      auto r = make_shared<T>(self);
      if (r->lhs.needs_bind())
        r->lhs = self.lhs.clone_expr();
      if (r->rhs.needs_bind())
        r->rhs = self.rhs.clone_expr();
      return r;
    }
    throw runtime_error(msg);
  }

  shared_ptr<ipoint_ts> abin_op_ts::clone_expr() const {
    return mk_clone_lhs_rhs(*this, "abin_op_ts");
  }

  shared_ptr<ipoint_ts> use_time_axis_from_ts::clone_expr() const {
    return mk_clone_lhs_rhs(*this, "use_time_axis_from_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> extend_ts::clone_expr() const {
    return mk_clone_lhs_rhs(*this, "extend_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> abin_op_scalar_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<abin_op_scalar_ts>(*this);
      r->rhs = rhs.clone_expr();
      return r;
    }
    throw runtime_error("abin_op_scalar_ts: attempt to clone bound expr");
  }

  shared_ptr<ipoint_ts> abin_op_ts_scalar::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<abin_op_ts_scalar>(*this);
      r->lhs = lhs.clone_expr();
      return r;
    }
    throw runtime_error("abin_op_ts_scalar: attempt to clone bound expr");
  }

  shared_ptr<ipoint_ts> anary_op_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<anary_op_ts>(*this);
      for (auto &a : r->args) {
        if (a.needs_bind())
          a = a.clone_expr();
      }
      return r;
    }
    throw runtime_error("anary_op_ts: attempt to clone bound expr");
  }

  template <class T>
  static shared_ptr<ipoint_ts> mk_clone(T const &self, char const *msg) {
    if (self.needs_bind()) {
      auto r = make_shared<T>(self);
      r->ts = self.ts->clone_expr();
      return r;
    }
    throw runtime_error(msg);
  }

  shared_ptr<ipoint_ts> average_ts::clone_expr() const {
    return mk_clone(*this, "average_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> integral_ts::clone_expr() const {
    return mk_clone(*this, "integral_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> derivative_ts::clone_expr() const {
    return mk_clone(*this, "derivative_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> accumulate_ts::clone_expr() const {
    return mk_clone(*this, "accumulate_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> repeat_ts::clone_expr() const {
    return mk_clone(*this, "repeat_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> abs_ts::clone_expr() const {
    return mk_clone(*this, "abs_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> time_shift_ts::clone_expr() const {
    return mk_clone(*this, "time_shift_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> inside_ts::clone_expr() const {
    return mk_clone(*this, "inside_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> decode_ts::clone_expr() const {
    return mk_clone(*this, "decode_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> bucket_ts::clone_expr() const {
    return mk_clone(*this, "bucket_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> transform_spline_ts::clone_expr() const {
    return mk_clone(*this, "transform_spline_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> aglacier_melt_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<aglacier_melt_ts>(*this);
      if (r->gm.temperature->needs_bind())
        r->gm.temperature = gm.temperature->clone_expr();
      if (r->gm.sca_m2->needs_bind())
        r->gm.sca_m2 = gm.sca_m2->clone_expr();
      return r;
    }
    throw runtime_error("aglacier_melt_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> qac_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<qac_ts>(*this);
      if (ts && ts->needs_bind())
        r->ts = ts->clone_expr();
      if (cts && cts->needs_bind())
        r->cts = cts->clone_expr();
      return r;
    }
    throw runtime_error("qac_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> convolve_w_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<convolve_w_ts>(*this);
      r->ts_impl.ts = ts_impl.ts.clone_expr();
      return r;
    }
    throw runtime_error("convolve_w_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> ice_packing_recession_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<ice_packing_recession_ts>(*this);
      if (flow_ts.needs_bind())
        r->flow_ts = flow_ts.clone_expr();
      if (ice_packing_ts.needs_bind())
        r->ice_packing_ts = ice_packing_ts.clone_expr();
      return r;
    }
    throw runtime_error("ice_packing_recession_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> ice_packing_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<ice_packing_ts>(*this);
      r->ts.temp_ts = ts.temp_ts.clone_expr();
      return r;
    }
    throw runtime_error("ice_packing_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> rating_curve_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<rating_curve_ts>(*this);
      r->ts.level_ts = ts.level_ts.clone_expr();
      return r;
    }
    throw runtime_error("rating_curve_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> krls_interpolation_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<krls_interpolation_ts>(*this);
      r->ts = ts.clone_expr();
      return r;
    }
    throw runtime_error("krls_interpolation_ts: attempt to clone bound ts");
  }

  shared_ptr<ipoint_ts> statistics_ts::clone_expr() const {
    if (needs_bind()) {
      auto r = make_shared<statistics_ts>(*this);
      r->ts = ts->clone_expr();
      return r;
    }
    throw runtime_error("statistical_ts:: attempt to clone bound ts");
  }

}
