#include <shyft/time_series/time_series_dd.h>
#include <fmt/core.h>

namespace shyft::time_series::dd {

  static string to_string(gta_t const &ta) {
    switch (ta.gt()) {
    case gta_t::generic_type::FIXED:
      return fmt::format("TaF[{},{},{}]", calendar().to_string(ta.f().t), to_seconds(ta.f().dt), ta.f().n);
    case gta_t::generic_dt::CALENDAR:
      if (ta.c().cal) {
        return fmt::format(
          "TaC[{},{},{},{}]",
          ta.c().cal->tz_info->name(),
          ta.c().cal->to_string(ta.c().t),
          to_seconds(ta.c().dt),
          ta.c().n);
      }
      return fmt::format("TaC[empty]");
    case gta_t::generic_dt::POINT:
      return fmt::format("TaP[{},{}]", ta.p().total_period().to_string(), ta.p().size());
    }
    return "";
  }

  static string to_string(string const &lhs, iop_t op, string const &rhs) {
    switch (op) {
    case iop_t::OP_ADD:
      return "(" + lhs + " + " + rhs + ")";
    case iop_t::OP_SUB:
      return "(" + lhs + " - " + rhs + ")";
    case iop_t::OP_DIV:
      return "(" + lhs + "/" + rhs + ")";
    case iop_t::OP_MUL:
      return "(" + lhs + "*" + rhs + ")";
    case iop_t::OP_MAX:
      return "max(" + lhs + ", " + rhs + ")";
    case iop_t::OP_MIN:
      return "min(" + lhs + ", " + rhs + ")";
    case iop_t::OP_POW:
      return "pow(" + lhs + ", " + rhs + ")";
    case iop_t::OP_LOG:
      return "log(" + lhs + ")";
    case iop_t::OP_NONE:
      break; // just fall to exception
    }
    return "unsupported_op(" + lhs + "," + rhs + ")";
  }

  static string to_string(derivative_method dm) {
    switch (dm) {
    case derivative_method::default_diff:
      return "default";
    case derivative_method::forward_diff:
      return "forward";
    case derivative_method::backward_diff:
      return "backward";
    case derivative_method::center_diff:
      return "center";
    }
    return "unknown_method";
  }

  static string to_string(nary_op_t op) {
    switch (op) {
    case nary_op_t::OP_NONE:
      return "NONE";
    case nary_op_t::OP_ADD:
      return "ADD";
    case nary_op_t::OP_MERGE:
      return "MERGE";
    }
    return "unknown_method";
  }

  static string str(ipoint_ts_ref ts) {
    return ts ? ts->stringify() : string("null");
  }

  string gpoint_ts::stringify() const {
    return "Ts{" + to_string(rep.ta) + "v[..]}";
  }

  string aref_ts::stringify() const {
    return id + (rep ? (string("(") + rep->stringify() + string(")")) : string(""));
  }

  string abin_op_ts::stringify() const {
    return to_string(lhs.stringify(), op, rhs.stringify());
  }

  string abin_op_ts_scalar::stringify() const {
    return to_string(lhs.stringify(), op, std::to_string(rhs));
  }

  string abin_op_scalar_ts::stringify() const {
    return to_string(std::to_string(lhs), op, rhs.stringify());
  }

  string anary_op_ts::stringify() const {
    const size_t n_args = args.size();
    string s = "nary_op([";
    for (size_t i = 0; i < n_args; ++i)
      s += args[i].stringify() + (i == (n_args - 1) ? "" : ", ");
    s += "], op=" + to_string(op)
       + (op == nary_op_t::OP_MERGE
            ? +", fc_interval=" + std::to_string(to_seconds(fc_interval))
                + ", lead_time=" + std::to_string(to_seconds(lead_time))
            : string(""))
       + ")";
    return s;
  }

  string abs_ts::stringify() const {
    return "abs(" + str(ts) + ")";
  }

  string statistics_ts::stringify() const {
    return "statistics(" + str(ts) + "," + to_string(ta) + "," + std::to_string(p) + ")";
  }

  string average_ts::stringify() const {
    return "average(" + str(ts) + "," + to_string(ta) + ")";
  }

  string integral_ts::stringify() const {
    return "integral(" + str(ts) + "," + to_string(ta) + ")";
  }

  string accumulate_ts::stringify() const {
    return "accumulate(" + str(ts) + "," + to_string(ta) + ")";
  }

  string time_shift_ts::stringify() const {
    return "time_shift(" + str(ts) + "," + std::to_string(to_seconds(dt)) + ")";
  }

  string periodic_ts::stringify() const {
    return "periodic_ts(" + to_string(ts.ta) + ")";
  }

  string convolve_w_ts::stringify() const {
    return "convolve_w_ts(" + ts_impl.ts.stringify() + ",..)";
  }

  string extend_ts::stringify() const {
    return "extend_ts(" + lhs.stringify() + "," + rhs.stringify() + ",..)";
  }

  string use_time_axis_from_ts::stringify() const {
    return string("(") + lhs.stringify() + string(".use_time_axis_from(") + rhs.stringify() + string(",..))");
  }

  string rating_curve_ts::stringify() const {
    return "rating_curve_ts(" + ts.level_ts.stringify() + ",..)";
  }

  string ice_packing_ts::stringify() const {
    return "ice_packing_ts(" + ts.temp_ts.stringify() + ",..)";
  }

  string ice_packing_recession_ts::stringify() const {
    return "ice_packing_recession_ts(" + flow_ts.stringify() + "," + ice_packing_ts.stringify() + ",..)";
  }

  string krls_interpolation_ts::stringify() const {
    return "krls(" + ts.stringify() + ",..)";
  }

  string qac_ts::stringify() const {
    return "qac_ts(" + str(ts) + ", " + str(cts) + "..)";
  }

  string inside_ts::stringify() const {
    return "inside_ts(" + str(ts) + ", " + std::to_string(p.min_x) + ", " + std::to_string(p.max_x) + ", ..)";
  }

  string transform_spline_ts::stringify() const {
    return "transform_spline_ts(" + str(ts) + ",..params..)";
  }

  string decode_ts::stringify() const {
    return "decode_ts(" + str(ts) + ",start_bit=" + std::to_string(p.start_bit)
         + ",n_bits=" + std::to_string(p.n_bits()) + ")";
  }

  string bucket_ts::stringify() const {
    return "bucket_ts(" + str(ts)
         + ",start_hour_utc=" + std::to_string(static_cast<int>(to_seconds64(p.hour_offset) / 3600))
         + ",bucket_emptying_limit=" + std::to_string(p.bucket_empty_limit) + ")";
  }

  string derivative_ts::stringify() const {
    return "derivative(" + str(ts) + ",dm=" + to_string(dm) + ")";
  }

  string repeat_ts::stringify() const {
    return "repeat(" + str(ts) + ",rta=" + to_string(rta) + ")";
  }

  // gm(temp.ts, sca_m2.ts, glacier_area_m2, dtf)
  string aglacier_melt_ts::stringify() const {
    return "glacier_melt( temp=" + str(gm.temperature) + ", sca_m2=" + str(gm.sca_m2)
         + ",area_m2=" + std::to_string(gm.glacier_area_m2) + ")";
  }

  string apoint_ts::stringify() const {
    return str(ts);
  }

}
