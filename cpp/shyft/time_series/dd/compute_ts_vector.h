#pragma once
#include <vector>
#include <future>
#include <thread>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::time_series::dd {
  template <typename TsV>
  concept deflatable =
    std::ranges::input_range<TsV>
    && std::is_same_v<shyft::time_series::dd::apoint_ts, std::remove_cvref_t<std::ranges::range_value_t<TsV>>>;

  /** @brief Compute a ts-vector multi-threaded
   *
   *@detail Given a random_access sized range of expressions, deflate(evaluate) the expressions and return the
   * equivalent concrete point-time-series of the expressions in the
   * preferred destination type Ts
   * Useful for the dtss,
   * evaluates the expressions in parallel
   */
  template <class Ts, deflatable TsV>
  requires std::ranges::random_access_range<TsV> && std::ranges::sized_range<TsV>
  auto deflate_ts_vector(TsV &&tsv1) {
    std::vector<Ts> tsv2(tsv1.size());

    auto deflate_range = [&tsv1, &tsv2](std::size_t i0, std::size_t n) {
      eval_ctx c;
      // first register ref.count for all expressions
      for (std::size_t i = i0; i < i0 + n; ++i)
        if (tsv1[i].ts)
          tsv1[i].ts->prepare(c);
      // secondly, do the work
      for (std::size_t i = i0; i < i0 + n; ++i) {
        if (auto gts = std::dynamic_pointer_cast<const gpoint_ts>(c.evaluate(tsv1[i].ts)))
          tsv2[i] = Ts(gts->time_axis(), gts->rep.v, gts->point_interpretation());
      }
    };
    auto n_threads = std::thread::hardware_concurrency();
    if (n_threads < 2)
      n_threads = 2; // hard coded minimum
    std::vector<std::future<void>> calcs;
    size_t ps = 1 + tsv1.size() / n_threads;
    for (size_t p = 0; p < tsv1.size();) {
      size_t np = p + ps <= tsv1.size() ? ps : tsv1.size() - p;
      calcs.push_back(std::async(std::launch::async, deflate_range, p, np));
      p += np;
    }
    for (auto &f : calcs)
      f.get();
    return tsv2;
  }

  template <class Ts>
  auto deflate_ts_vector(deflatable auto &&tsv1) {
    std::vector<shyft::time_series::dd::apoint_ts> v;
    std::ranges::copy(tsv1, std::back_inserter(v));
    return deflate_ts_vector<Ts>(v);
  }
}
