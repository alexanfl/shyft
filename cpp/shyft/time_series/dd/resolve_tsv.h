/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <functional>

#include <shyft/time_series/dd/ats_vector.h>

namespace shyft::time_series::dd {

  using resolve_fx = std::function<ats_vector(std::vector<std::string> const &)>;
  /**
   * @brief resolve symbolic refs for a tsvector
   * @details
   * Given a ts-vector with expressions, fill in-place not yet bound symbolic refs.
   * The the .needs_bind() method is used to identify expressions that needs bind,
   * and the .find_ts_bind_info() is used to get the detail sym-ref/ts pairs that needs resolution.
   *
   * The resolution is done using the passed in `resolve` callable function.
   * The algorithm works similar to this:
   *
   * (1) Gather a unique list of refs that needs to be read (ensure only one read op pr symbol ref is read, even with
   * multiple same refs) (2) bulk read and bind those missing (verifies read/resolve succeeds, or throws) (3) - if the
   * above step gives an expression that is completely bound, the `.do_bind()` method is called to finish the expression
   * tree
   *
   * Important aspects of the algorithm:
   *
   * (1) Only invoke resolve for once pr. unique symbol
   * (2) expressions that are resolved, will reply false on `ts.needs_bind()`
   * (3) It will resolve the supplied ts-vector inplace(modifying it), or throw if not possible
   *
   * @note We aim to replace/rebuild this function resolve_ts.h engine later.
   *
   * @param atsv a vector of expressions to be resolved, mutable, the expression tree is modified
   * @param resolve  `vector<apoint_ts>fx(vector<string>const& ts_urls)` like function to bulk read symbols
   *                 typically, you can pack this callable with extra contextual arguments, bind_period etc. if needed.
   *                 note that each return entry must be non-null apoint_ts.ts, otherwise exception is thrown.
   * @param recurse_level current recursion level, default 1, used internally during invocations
   * @param recurse_level_max max levels before throwing runtime-error, default 1000 (very high..)
   *
   */
  void resolve_symbols(ats_vector &atsv, resolve_fx resolve, size_t recurse_level = 0, size_t recurse_level_max = 1000);

}
