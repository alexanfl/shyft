/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** @brief repeat ts  repeats the first period of ts over a larger time-axis
   *
   * Given a source time-series ts, and a time-axis ta,
   * The resulting ts have time_axis total period equal to ta,
   * where the time-points in each period of the time-axis ta,
   * is equal to replica of the time-points of the ts.time_axis()
   * The values of the new time-series is equal to the corresponding
   * points in the source ts.
   *
   * Typical use: Repeat a metered temperature  season -max mask over several years.
   *
   */
  struct repeat_ts : ipoint_ts {
    ipoint_ts_ref ts;  ///< the time-series to repeat
    gta_t rta;         ///< the repeat time-axis, with periods like year etc.
    gta_t ta;          ///< the resulting time-axis from using ts.time_axis repeated over rta-periods
    bool bound{false}; ///< flag to keep track of

    repeat_ts() = default;
    //-- useful ct goes here
    repeat_ts(apoint_ts const & ats, gta_t const & rta);
    repeat_ts(apoint_ts&& ats, gta_t&& rta);

    repeat_ts(ipoint_ts_ref const & ts, gta_t const & rta)
      : ts(ts)
      , rta(rta) {
      do_early_bind();
    }

    repeat_ts(ipoint_ts_ref const & ts, gta_t const & rta, gta_t const & ta, bool bound) // support expr. serialization
      : ts(ts)
      , rta(rta)
      , ta(ta)
      , bound(bound) {
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    ts_point_fx point_interpretation() const override {
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx pfx) override {
      if (ts)
        dref(ts).set_point_interpretation(pfx);
    }

    gta_t const & time_axis() const override {
      assert_bound();
      return ta;
    }

    utcperiod total_period() const override {
      assert_bound();
      return ta.total_period();
    }

    size_t index_of(utctime t) const override {
      assert_bound();
      return ta.index_of(t);
    }

    size_t size() const override {
      assert_bound();
      return ta.size();
    }

    utctime time(size_t i) const override {
      assert_bound();
      return ta.time(i);
    };

    // methods that needs special implementation according to qac rules
    virtual double value(size_t i) const override;
    virtual double value_at(utctime t) const override;
    vector<double> values() const override;

    // methods for binding and symbolic ts
    bool needs_bind() const override;
    void do_bind() override;
    void do_unbind() override;
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    string stringify() const override;
    x_serialize_decl();
   protected:
    void assert_bound() const {
      if (!bound)
        throw runtime_error("repeat_ts:attemt to use method on unbound ts");
    }

    void do_early_bind() {
      if (ts && !ts->needs_bind())
        local_do_bind();
    }

    void local_do_bind();
    void local_do_unbind();
  };

}

x_serialize_export_key(shyft::time_series::dd::repeat_ts);
