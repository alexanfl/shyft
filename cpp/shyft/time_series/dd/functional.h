#pragma once
#include <concepts>
#include <memory>
#include <ranges>
#include <type_traits>
#include <utility>

#include <boost/preprocessor/tuple/to_list.hpp>
#include <boost/preprocessor/list/for_each.hpp>

#include <shyft/core/reflection.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/abin_op_ts.h>
#include <shyft/time_series/dd/abs_ts.h>
#include <shyft/time_series/dd/accumulate_ts.h>
#include <shyft/time_series/dd/aglacier_melt_ts.h>
#include <shyft/time_series/dd/anary_op_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/average_ts.h>
#include <shyft/time_series/dd/bucket_ts.h>
#include <shyft/time_series/dd/convolve_w_ts.h>
#include <shyft/time_series/dd/decode_ts.h>
#include <shyft/time_series/dd/derivative_ts.h>
#include <shyft/time_series/dd/extend_ts.h>
#include <shyft/time_series/dd/fast_krls_ts.h>
#include <shyft/time_series/dd/geo_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/ice_packing_recession_ts.h>
#include <shyft/time_series/dd/ice_packing_ts.h>
#include <shyft/time_series/dd/inside_ts.h>
#include <shyft/time_series/dd/integral_ts.h>
#include <shyft/time_series/dd/krls_interpolation_ts.h>
#include <shyft/time_series/dd/periodic_ts.h>
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/time_series/dd/rating_curve_ts.h>
#include <shyft/time_series/dd/repeat_ts.h>
#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/time_series/dd/statistics_ts.h>
#include <shyft/time_series/dd/time_shift_ts.h>
#include <shyft/time_series/dd/transform_spline_ts.h>
#include <shyft/time_series/dd/use_time_axis_from_ts.h>

namespace shyft::time_series::dd {

#define SHYFT_TIME_SERIES_OPERATIONS \
  (aref_ts, \
   average_ts, \
   integral_ts, \
   accumulate_ts, \
   time_shift_ts, \
   abin_op_ts, \
   abin_op_scalar_ts, \
   abin_op_ts_scalar, \
   anary_op_ts, \
   abs_ts, \
   extend_ts, \
   use_time_axis_from_ts, \
   ice_packing_ts, \
   ice_packing_recession_ts, \
   rating_curve_ts, \
   krls_interpolation_ts, \
   qac_ts, \
   inside_ts, \
   derivative_ts, \
   convolve_w_ts, \
   decode_ts, \
   transform_spline_ts, \
   aglacier_melt_ts, \
   statistics_ts, \
   repeat_ts, \
   bucket_ts, \
   gpoint_ts)
  // note:    periodic_ts, is a terminal so its not in the list above

// NOTE:
//   expands to a conjunction of constraints that T is invocable for each type of time-series
//   that is lsited in SHYFT_TIME_SERIES_OPERATIONS. it's done with macros instead of
//   a fold expression to ensure better diagnostics, as fold expressions are interpreted
//   as an atomic constraint.
//   - jeh
#define SHYFT_MACRO(r, _, ts_type) &&std::invocable<T, add_const_if_t<C, ts_type> &>
  template <typename T, bool C = false>
  concept operation_invocable =
    true BOOST_PP_LIST_FOR_EACH(SHYFT_MACRO, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_TIME_SERIES_OPERATIONS));
#undef SHYFT_MACRO

#define SHYFT_MACRO(r, _, ts_type) &&std::is_invocable_r_v<R, T, add_const_if_t<C, ts_type> &>
  template <typename T, bool C, typename R>
  concept operation_invocable_to =
    true BOOST_PP_LIST_FOR_EACH(SHYFT_MACRO, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_TIME_SERIES_OPERATIONS));
#undef SHYFT_MACRO

  namespace detail {

    template <typename T>
    struct is_ipoint_ref_impl : std::false_type { };

    template <typename T>
    struct is_ipoint_ref_impl<std::shared_ptr<T>>
      : std::integral_constant< bool, std::is_same_v<std::remove_const_t<T>, ipoint_ts>> { };

    template <typename T>
    constexpr bool is_ipoint_ts_ref_v = detail::is_ipoint_ref_impl<std::remove_cvref_t<T>>::value;

    template <typename T>
    requires(is_ipoint_ts_ref_v<T>)
    constexpr decltype(auto) ensure_ipoint_ts_ref(T &&ts) {
      return (SHYFT_FWD(ts));
    }

    template <typename T>
    requires(std::same_as<std::remove_cvref_t<T>, apoint_ts>)
    constexpr decltype(auto) ensure_ipoint_ts_ref(T &&ts) {
      return ensure_ipoint_ts_ref(SHYFT_FWD(ts).ts);
    }

    template <typename T>
    requires(is_ipoint_ts_ref_v<T>)
    constexpr auto cast_ipoint_ts_ptr(T &&ts) {
      return const_cast<std::add_pointer_t<const_like_t<ipoint_ts, T>>>(SHYFT_FWD(ts).get());
    }

    template <bool is_const, typename T>
    requires(std::same_as<std::remove_cvref_t<T>, apoint_ts>)
    constexpr auto cast_ipoint_ts_ptr(T &&ts) {
      return cast_ipoint_ts_ptr(ensure_ipoint_ts_ref(SHYFT_FWD(ts)));
    }

  }

  /** @brief Invoke a function with the concrete type of a time-series
   *  @details
   *  Successively tries to cast and invoke the abstract time-series with the types
   *  listed in SHYFT_TIME_SERIES_OPERATIONS. The function will be called with
   *  a concrete type const-qualified as `its_ref`, so the user needs to take car
   *  not changing a const object.
   *  @param its_ref the abstract time-series
   *  @param function the function to invoke with a concrete time-series
   *  @param fallback the function to invoke if no concrete type was found
   *  @returns the result of the invocation of either function or callback
   */
  template <typename qualified_ipoint_ts_ref>
  requires detail::is_ipoint_ts_ref_v<qualified_ipoint_ts_ref>
  constexpr auto with_operation(
    qualified_ipoint_ts_ref &&its_ref,
    operation_invocable<std::is_const_v<std::remove_reference_t<qualified_ipoint_ts_ref>>> auto &&function,
    std::invocable<> auto &&fallback) {
    auto its_ptr = detail::cast_ipoint_ts_ptr(SHYFT_FWD(its_ref));

    // NOTE:
    //   injects a list of conditional invocations that first casts the abstract time-series
    //   and then invokes function if the cast succeedes. all the types listed in the
    //   SHYFT_TIME_SERIES_OPERATIONS are tried.
    //   - jeh
#define SHYFT_MACRO(r, data, ts_type) \
  if (auto ts_ptr = dynamic_cast<const_like_t<ts_type, qualified_ipoint_ts_ref> *>(its_ptr)) \
    return std::invoke(SHYFT_FWD(function), *ts_ptr);
    BOOST_PP_LIST_FOR_EACH(SHYFT_MACRO, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_TIME_SERIES_OPERATIONS))
    return std::invoke(SHYFT_FWD(fallback));
#undef SHYFT_MACRO
  }

  template < typename qualified_apoint_ts, bool is_const = std::is_const_v<std::remove_reference_t<qualified_apoint_ts>>>
  requires std::same_as<std::remove_cvref_t<qualified_apoint_ts>, apoint_ts>
  constexpr auto with_operation(
    qualified_apoint_ts &&ts_ref,
    operation_invocable<is_const> auto &&function,
    std::invocable<> auto &&fallback) {
    return with_operation(SHYFT_FWD(ts_ref).ts, SHYFT_FWD(function), SHYFT_FWD(fallback));
  }

  template <typename W, typename T, typename U>
  concept operand_fold_op = std::invocable<W, U &&, qual_like_t<std::shared_ptr<ipoint_ts const>, T>>;

  /** @brief Fold a function over the operand time-series of some time-series.
   *  @param ts either a concrete time-series or an `apoint_ts`
   *  @param value the initial value of the fold
   *  @param op the folding function
   *  @returns the result of the fold
   */
  template <typename T, typename U>
  constexpr auto fold_operands(T &&ts, U value, operand_fold_op<T, U> auto &&op) {

    auto call = [&](auto &&oprn) {
      value = std::invoke(op, std::move(value), detail::ensure_ipoint_ts_ref(SHYFT_FWD(oprn)));
    };

    using V = std::remove_const_t<std::remove_reference_t<T>>;
    if constexpr (std::is_same_v<V, apoint_ts>)
      with_operation(
        SHYFT_FWD(ts),
        [&](auto &&ts) {
          value = fold_operands(SHYFT_FWD(ts), std::move(value), op);
        },
        [] {});
    else if constexpr (std::is_same_v<V, ice_packing_ts>)
      call(SHYFT_FWD(ts).ts.temp_ts.ts);
    else if constexpr (std::is_same_v<V, ice_packing_recession_ts>) {
      call(SHYFT_FWD(ts).flow_ts.ts);
      call(SHYFT_FWD(ts).ice_packing_ts.ts);
    } else if constexpr (std::is_same_v<V, rating_curve_ts>)
      call(SHYFT_FWD(ts).ts.level_ts);
    else if constexpr (std::is_same_v<V, krls_interpolation_ts>)
      call(SHYFT_FWD(ts).ts.ts);
    else if constexpr (std::is_same_v<V, qac_ts>) {
      call(SHYFT_FWD(ts).ts);
      call(SHYFT_FWD(ts).cts);
    } else if constexpr (std::is_same_v<V, convolve_w_ts>)
      call(SHYFT_FWD(ts).ts_impl.ts.ts);
    else if constexpr (std::is_same_v<V, aglacier_melt_ts>) {
      call(SHYFT_FWD(ts).gm.temperature);
      call(SHYFT_FWD(ts).gm.sca_m2);
    } else if constexpr (std::is_same_v<V, abin_op_scalar_ts>)
      call(SHYFT_FWD(ts).rhs);
    else if constexpr (std::is_same_v<V, abin_op_ts_scalar>)
      call(SHYFT_FWD(ts).lhs);
    else if constexpr (requires { ts.ts; })
      call(SHYFT_FWD(ts).ts);
    else if constexpr (requires {
                         ts.lhs;
                         ts.rhs;
                       }) {
      call(SHYFT_FWD(ts).lhs);
      call(SHYFT_FWD(ts).rhs);
    } else if constexpr (std::is_same_v<V, anary_op_ts>)
      for (auto &&arg : SHYFT_FWD(ts).args)
        call(SHYFT_FWD(arg));
    return value;
  }

  template <typename W, typename T>
  concept operand_for_each_op = std::invocable<W, qual_like_t<std::shared_ptr<ipoint_ts const>, T> &>;

  /** @brief Invoke a function for each operand time-series of some time-series.
   *  @param ts either a concrete time-series or an `apoint_ts`
   *  @param op the function to invoce
   *  @returns void
   */

  template <typename T>
  constexpr void for_each_operands(T &&ts, operand_for_each_op<T> auto &&op) {
    fold_operands(SHYFT_FWD(ts), std::monostate{}, [&](std::monostate, auto &&ts) {
      op(SHYFT_FWD(ts));
      return std::monostate{};
    });
  }

}
