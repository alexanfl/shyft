/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <boost/math/special_functions/relative_difference.hpp>
#include <shyft/time_series/common.h>
#include <shyft/time_series/ice_packing_policy.h>

namespace shyft::time_series {
  using namespace shyft::core;
  using boost::math::epsilon_difference;

  /**  ice packing/reduction parameters
   * keeps the minimal parameters needed to describe the
   * ice_packing detection process.
   */
  struct ice_packing_parameters {

    utctimespan window{0};      ///< the time-window in seconds for the temperature computation compared to threshold
    double threshold_temp{0.0}; ///< the threshold temperature where ice-packing occurs, ref. .window for length

    ice_packing_parameters() = default;

    ice_packing_parameters(utctimespan window, double threshold_temp)
      : window{window}
      , threshold_temp{threshold_temp} {
    }

    ice_packing_parameters(int64_t window, double threshold_temp)
      : window{seconds(window)}
      , threshold_temp{threshold_temp} {
    }

    bool operator==(ice_packing_parameters const & other) const noexcept {
      return window == other.window && epsilon_difference(threshold_temp, other.threshold_temp) < 2.0;
    }

    x_serialize_decl();
  };
}
