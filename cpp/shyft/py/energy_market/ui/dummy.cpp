
#include <shyft/version.h>
#include <shyft/py/api/bindings.h>

BOOST_PYTHON_MODULE(_ui) {
  shyft::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  shyft::py::scope().attr("__doc__") = "Shyft Energy Market user interface configuration api";
  shyft::py::scope().attr("__version__") = shyft::_version_string();
  shyft::py::scope().attr("shyft_with_stm") = false;
}
