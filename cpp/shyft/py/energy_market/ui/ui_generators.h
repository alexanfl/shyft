#pragma once
#include <cstdint>
#include <cmath>
#include <string>
#include <iterator>
#include <initializer_list>

#include <QtGlobal>
#include <QObject>
#include <QLayout>
#include <QGridLayout>
#include <QBoxLayout>
#include <QStackedLayout>
#include <QFormLayout>
#include <QLayoutItem>
#include <QSpacerItem>
#include <QWidgetItem>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QAbstractSpinBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QDateTimeEdit>
#include <QAbstractButton>
#include <QPushButton>
#include <QRadioButton>
#include <QCheckBox>
#include <QToolButton>
#include <QButtonGroup>
#include <QDialogButtonBox>
#include <QTreeWidget>
#include <QTableWidget>
#include <QFrame>
#include <QScrollArea>
#include <QChartView>
#include <QDateTimeAxis>
#include <QValueAxis>
#include <QBarCategoryAxis>
#include <QXYSeries>
#include <QAbstractBarSeries>
#include <QBarSet>
#include <QDateTime>
#include <QToolBar>
#include <QAction>
#include <QWidgetAction>
#include <QMenu>
#include <QUuid>
#include <QDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <shyft/web_api/energy_market/generators.h> // json object emitter (emit_object etc)

namespace shyft::web_api::generator {

  using std::string;
  using namespace std::string_literals;
  using namespace QtCharts;

  //
  // Basic type emitters and utilities
  //

  /**
   * Automatically dereference pointers for emit (will often just dereference before calling emit,
   * but usefull for generic callables like in emit_list_fx, def_optional_list_fx).
   */
  template <class OutputIterator, class T>
  struct emit<OutputIterator, T*> {
    emit(OutputIterator& oi, T const * o) {
      if (o) {
        emit<OutputIterator, T>(oi, *o);
      } else {
        emit_null(oi);
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QRect> {
    emit(OutputIterator& oi, QRect const & o) {
      emit_object<OutputIterator> oo(oi);
      oo.def("x", o.x()).def("y", o.y()).def("w", o.width()).def("h", o.height());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QRectF> {
    emit(OutputIterator& oi, QRectF const & o) {
      emit_object<OutputIterator> oo(oi);
      oo.def("x", o.x()).def("y", o.y()).def("w", o.width()).def("h", o.height());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QString> {
    emit(OutputIterator& oi, QString const & o) {
      emit<OutputIterator, string>(oi, o.toStdString());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QDate> {
    emit(OutputIterator& oi, QDate const & o) {
      emit<OutputIterator, QString>(oi, o.toString(Qt::ISODate));
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QTime> {
    emit(OutputIterator& oi, QTime const & o) {
      emit<OutputIterator, QString>(oi, o.toString(Qt::ISODate));
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QDateTime> {
    emit(OutputIterator& oi, QDateTime const & o) {
      emit<OutputIterator, QString>(oi, o.toString(Qt::ISODate));
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QPointF> {
    emit(OutputIterator& oi, QPointF const & o) {
      emit_object<OutputIterator> oo(oi);
      oo.def("x", o.x()).def("y", o.y());
    }
  };

  template <class OutputIterator, class T>
  struct emit<OutputIterator, QList<T*>> {
    emit(OutputIterator& oi, QList<T*> const & o) {
      emit_vector_fx(oi, o, [](OutputIterator& oi, T const * p) -> void {
        emit<OutputIterator, T>(oi, *p);
      });
    }
  };

  template <class OutputIterator, class T>
  struct emit<OutputIterator, QList<T>> {
    emit(OutputIterator& oi, QList<T> const & o) {
      emit_vector_fx(oi, o, [](OutputIterator& oi, T const & p) -> void {
        emit<OutputIterator, T>(oi, p);
      });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QStringList> { // QStringList class inherits QList<QString> (adds a few convenience
                                             // functions) so could probably re-use QList<T> emitter

    emit(OutputIterator& oi, QStringList const & o) {
      emit_vector_fx(oi, o, [](OutputIterator& oi, QString const & p) -> void {
        emit<OutputIterator, QString>(oi, p);
      });
    }
  };

  template <class OutputIterator, class T>
  struct emit<OutputIterator, QVector<T>> {
    emit(OutputIterator& oi, QVector<T> const & o) {
      emit_vector_fx(oi, o, [](OutputIterator& oi, T const & p) -> void {
        emit<OutputIterator, T>(oi, p);
      });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QVariantMap> { // QVariantMap is typedef for QMap<QString, QVariant>, and the C++ type for
                                             // Python dict

    emit(OutputIterator& oi, QVariantMap const & o) {
      emit_object<OutputIterator> oo(oi);
      for (auto it = o.begin(), end = o.end(); it != end; ++it) {
        oo.def(it.key().toStdString(), it.value());
      }
    }
  };

  /**
   * Get the value from a QVariant (only) for types known to be implemented by an emit.
   *
   * @note static_cast here, will be obsolete in Qt6
   */
  template <class OutputIterator>
  struct emit<OutputIterator, QVariant> {
    emit(OutputIterator& oi, QVariant const & o) {
      switch (static_cast<QMetaType::Type>(o.type())) {
      case QMetaType::Bool:
        emit<OutputIterator, bool>(oi, o.value<bool>());
        break;
      case QMetaType::Int:
        emit<OutputIterator, int>(oi, o.value<int>());
        break;
      case QMetaType::Double:
        emit<OutputIterator, double>(oi, o.value<double>());
        break;
      case QMetaType::QChar:
        emit<OutputIterator, char>(oi, o.value<QChar>().toLatin1());
        break;
      case QMetaType::QString:
        emit<OutputIterator, QString>(oi, o.value<QString>());
        break;
      case QMetaType::UInt:
      case QMetaType::LongLong:
        emit<OutputIterator, int64_t>(oi, o.value<int64_t>());
        break;
      // case QMetaType::ULongLong: emit<OutputIterator, uint64_t>(oi, o.value<uint64_t>()); break;
      case QMetaType::QDate:
        emit<OutputIterator, QDate>(oi, o.value<QDate>());
        break;
      case QMetaType::QTime:
        emit<OutputIterator, QTime>(oi, o.value<QTime>());
        break;
      case QMetaType::QDateTime:
        emit<OutputIterator, QDateTime>(oi, o.value<QDateTime>());
        break;
      case QMetaType::QRect: {
        emit<OutputIterator, QRect>(oi, o.value<QRect>());
        break;
      }
      case QMetaType::QRectF: {
        emit<OutputIterator, QRectF>(oi, o.value<QRectF>());
        break;
      }
      case QMetaType::QStringList: {
        emit<OutputIterator, QStringList>(oi, o.value<QStringList>());
        break;
      } // QStringList is subclass of QList<QString>
      case QMetaType::QVariantList: {
        emit<OutputIterator, QVariantList>(oi, o.value<QVariantList>());
        break;
      } // QVariantList is typedef for QList<QVariant>
      case QMetaType::QVariantMap: {
        emit<OutputIterator, QVariantMap>(oi, o.value<QVariantMap>());
        break;
      } // QVariantMap is typedef for QMap<QString, QVariant>, and the C++ type for Python dict
      default:
        if (o.canConvert<QString>())
          emit<OutputIterator, QString>(oi, o.toString());
        else
          emit_null(oi);
      }
    }
  };

  /**
   * Utility function to check if a variant, which can be emitted using emitter above, contains a valid value.
   * Optionally also checks if it contains a "truthy" value using some unscientific conditions such as integer 0, empty
   * string etc. Note: This is not used directly by the emitter function above, but might be used before calling it
   * (where we have more context).
   */
  bool check_variant(QVariant const & o, bool truthy) {
    switch (static_cast<QMetaType::Type>(
      o.type())) { // Get the value from the QVariant (only) for types known to be implemented by an emit
    case QMetaType::Bool:
      return !truthy || o.value<bool>();
    case QMetaType::Int:
      return !truthy || o.value<int>() != 0;
    case QMetaType::Double:
      return !std::isnan(o.value<double>()) && (!truthy || o.value<double>() != 0.0);
    case QMetaType::QChar:
      return !truthy || !o.value<QChar>().isNull();
    case QMetaType::QString:
      return !o.value<QString>().isNull() && (!truthy || !o.value<QString>().isEmpty());
    case QMetaType::UInt:
    case QMetaType::LongLong:
      return !truthy || o.value<int64_t>() != 0;
    // case QMetaType::ULongLong: !truthy || o.value<uint64_t>() != 0;
    case QMetaType::QDate:
      return o.value<QDate>().isValid(); // Note: A null date is also an invalid time.
    case QMetaType::QTime:
      return o.value<QTime>().isValid(); // Note: A null time, which is midnight 00:00:00.000, is also an invalid time.
    case QMetaType::QDateTime:
      return o.value<QDateTime>().isValid(); // Note: A null date/time is also an invalid time.
    case QMetaType::QRect:
      return o.value<QRect>().isValid(); // Note: A null rectangle is also empty, and hence is not valid.
    case QMetaType::QRectF:
      return o.value<QRectF>().isValid(); // Note: A null rectangle is also empty, and hence is not valid.
    case QMetaType::QStringList:
      return !o.value<QStringList>().isEmpty();
    case QMetaType::QVariantList:
      return !o.value<QVariantList>().isEmpty();
    case QMetaType::QVariantMap:
      return !o.value<QVariantMap>().isEmpty();
    default:
      if (o.canConvert<QString>())
        return !truthy || !o.value<QString>().isEmpty();
      else
        return false;
    }
  }


#if 0
/**
 * Emit enum value as enum key string.
 * Only works with enums declared as Q_ENUM, Q_ENUM_NS, Q_FLAG or Q_FLAG_NS.
 */
template<class OutputIterator, class Enum>
void emit_enum_value(OutputIterator& oi, const Enum v) {
#if 0
    emit<OutputIterator, const char*>(oi, QMetaEnum::fromType<Enum>().valueToKey(v));
#else
    emit<OutputIterator, QString>(oi, QVariant::fromValue(v).toString());
#endif
}
#endif

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::Orientation> {
    emit(OutputIterator& oi, Qt::Orientation const & o) {
      switch (o) {
      case Qt::Horizontal:
        emit<OutputIterator, string>(oi, "horizontal"s);
        break;
      case Qt::Vertical:
        emit<OutputIterator, string>(oi, "vertical"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  void def_alignment_for_orientation(
    emit_object<OutputIterator>& oo,
    std::string_view name,
    Qt::Orientation const & orientation,
    Qt::Alignment const & value) {
    // The alignment enum contains different set of flags for horizontal and vertical
    // orientation that in some cases can be combined, but at most one horizontal and
    // one vertical flag at a time is allowed.
    // Note that it is still possible to set more than one of same orientation,
    // here we use the testFlag method to pick the first (in order of if tests)
    // alignment flag set for the orientation, and just not comparing enum values
    // which would match no single enum entry.
    // This function defines the value for one specified orientation.
    // See also the generic emit class for Alignment.
    if (orientation == Qt::Horizontal) {
      Qt::Alignment o = value & Qt::AlignHorizontal_Mask;
      if (o.testFlag(Qt::AlignLeft))
        oo.def(name, "left"s);
      else if (o.testFlag(Qt::AlignRight))
        oo.def(name, "right"s);
      else if (o.testFlag(Qt::AlignHCenter))
        oo.def(name, "center"s);
      else if (o.testFlag(Qt::AlignJustify))
        oo.def(name, "justify"s);
      else if (o.testFlag(Qt::AlignAbsolute))
        oo.def(name, "absolute"s);
    } else {
      Qt::Alignment o = value & Qt::AlignVertical_Mask;
      if (o.testFlag(Qt::AlignTop))
        oo.def(name, "top"s);
      else if (o.testFlag(Qt::AlignBottom))
        oo.def(name, "bottom"s);
      else if (o.testFlag(Qt::AlignVCenter))
        oo.def(name, "center"s);
      else if (o.testFlag(Qt::AlignBaseline))
        oo.def(name, "baseline"s);
    }
  }

  template <class OutputIterator>
  void def_alignment(emit_object<OutputIterator>& oo, Qt::Alignment const & o) {
    // Define horizontal and vertical alignment as separate attributes if set)
    // See also: def_alignment_for_orientation.
    def_alignment_for_orientation(oo, "horizontalAlignment", Qt::Horizontal, o);
    def_alignment_for_orientation(oo, "verticalAlignment", Qt::Vertical, o);
  }

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::Alignment> {
    emit_object<OutputIterator> oo;

    emit(OutputIterator& oi, Qt::Alignment const & o)
      : oo{oi} {
      // Emit alignment as an object, with horizontal and vertical alignment
      // as attributes (if set). See also: def_alignment_for_orientation.
      def_alignment_for_orientation(oo, "horizontal", Qt::Horizontal, o);
      def_alignment_for_orientation(oo, "vertical", Qt::Vertical, o);
    }
  };

  /**
   * Enum for identifiers of custom data elements supported in this project.
   * The enum Qt::ItemDataRole contains integer identifiers (called roles) for pre-defined data elements.
   * Custom data elements may be added using identifiers starting with integer 256.
   */
  template <class OutputIterator>
  struct emit<OutputIterator, Qt::ItemDataRole> {
    emit(OutputIterator& oi, Qt::ItemDataRole const & o) {
      switch (o) {
      case Qt::DisplayRole:
        emit<OutputIterator, string>(oi, "display"s);
        break;
      case Qt::DecorationRole:
        emit<OutputIterator, string>(oi, "decoration"s);
        break;
      case Qt::EditRole:
        emit<OutputIterator, string>(oi, "edit"s);
        break;
      case Qt::ToolTipRole:
        emit<OutputIterator, string>(oi, "toolTip"s);
        break;
      case Qt::StatusTipRole:
        emit<OutputIterator, string>(oi, "statusTip"s);
        break;
      case Qt::WhatsThisRole:
        emit<OutputIterator, string>(oi, "whatsThis"s);
        break;
      case Qt::SizeHintRole:
        emit<OutputIterator, string>(oi, "sizeHint"s);
        break;
      case Qt::FontRole:
        emit<OutputIterator, string>(oi, "font"s);
        break;
      case Qt::TextAlignmentRole:
        emit<OutputIterator, string>(oi, "textAlignment"s);
        break;
      case Qt::BackgroundRole:
        // case Qt::BackgroundColorRole: // Obsolete, superseded by BackgroundRole
        emit<OutputIterator, string>(oi, "background"s);
        break;
      case Qt::ForegroundRole:
        // case Qt::TextColorRole: // Obsolete, superseded by ForegroundRole
        emit<OutputIterator, string>(oi, "foreground"s);
        break;
      case Qt::CheckStateRole:
        emit<OutputIterator, string>(oi, "checkState"s);
        break;
      case Qt::InitialSortOrderRole:
        emit<OutputIterator, string>(oi, "initialSortOrder"s);
        break;
      case Qt::AccessibleTextRole:
        emit<OutputIterator, string>(oi, "accessibleText"s);
        break;
      case Qt::AccessibleDescriptionRole:
        emit<OutputIterator, string>(oi, "accessibleDescription"s);
        break;
        // case Qt::UserRole:
        //     emit<OutputIterator, string>(oi, "user"s); break;
      }
    }
  };
  enum class ItemDataProperty {
    DataX = Qt::UserRole, // 0x0100, the first role that can be used for application-specific purposes (after predefined
                          // roles in Qt::ItemDataRole).
    DataY,
    Decimals,
    ScaleX,
    ScaleY,
    ValidationX,
    ValidationY,
    Tags
  };

  template <class OutputIterator>
  struct emit<OutputIterator, ItemDataProperty> {
    emit(OutputIterator& oi, ItemDataProperty const & o) {
      switch (o) {
      case ItemDataProperty::DataX:
        emit<OutputIterator, string>(oi, "dataX"s);
        break;
      case ItemDataProperty::DataY:
        emit<OutputIterator, string>(oi, "dataY"s);
        break;
      case ItemDataProperty::Decimals:
        emit<OutputIterator, string>(oi, "decimals"s);
        break;
      case ItemDataProperty::ScaleX:
        emit<OutputIterator, string>(oi, "scaleX"s);
        break;
      case ItemDataProperty::ScaleY:
        emit<OutputIterator, string>(oi, "scaleY"s);
        break;
      case ItemDataProperty::ValidationX:
        emit<OutputIterator, string>(oi, "validationX"s);
        break;
      case ItemDataProperty::ValidationY:
        emit<OutputIterator, string>(oi, "validationY"s);
        break;
      case ItemDataProperty::Tags:
        emit<OutputIterator, string>(oi, "tags"s);
        break;
      }
    }
  };

  /**
   * Emit typed content from the data container of various Qt items as properties similar to QObject.
   * These item types, such as QTreeWidgetItem and QTableWidgetItem, typically do not inherit from QObject,
   * hence do not have the generic property system. They have a data container, though, which can store
   * typed content, so therefore emulating a similar concept by emitting data entries in a "properties" object.
   * The data container is indexed by an integer value, so-called "role", and not a name string like properties,
   * but by relying on a pre-defined enum of all supported property-roles we are here able to emit as named properties.
   * Does conditional emit; only explicitely specified data roles, only if they contain a valid value,
   * and if there are none then the properties object will not be mitted at all.
   * Roles to emit are typically from the ItemDataProperty enum, which is exposed to Python.
   * Note: Could consider registering the enum with Qt meta object system, using Q_ENUM macro, and then
   * here, instead of emitting specific enum roles, loop through all enum entries and emit any that are set
   * data for, but for now the number of roles are so few that it is a bit overkill..
   */
  template <class OutputIterator, class T>
  void def_item_data_properties(
    emit_object<OutputIterator>& oo,
    T const & o,
    std::initializer_list<ItemDataProperty> properties) {
    int count = 0;
    for (auto const & role : properties) {
      const QVariant v = o.data((int) role);
      if (role == ItemDataProperty::Decimals || check_variant(v, true)) { // decimals can be 0, so emit it..!
        if (count == 0) {
          oo.sep();
          emit<OutputIterator, string>(oo.oi, "properties"s);
          *oo.oi++ = colon;
          *oo.oi++ = obj_begin;
        } else {
          *oo.oi++ = comma;
        }
        emit<OutputIterator, ItemDataProperty>(oo.oi, role);
        *oo.oi++ = colon;
        emit<OutputIterator, QVariant>(oo.oi, v);
        ++count;
      }
    }
    if (count > 0)
      *oo.oi++ = obj_end;
  }

  /**
   * Emit any item flags as boolean properties on the owning object.
   * Can optionally supply a set of flags that represent default, and only emit any differences.
   * Default for QTreeWidgetItem: selectable, dragenabled, dropenabled, usercheckable, enabled.
   * Default for QTableWidgetItem: selectable, editable, dragenabled, dropenabled, usercheckable, enabled.
   */
  template <class OutputIterator>
  void def_item_flags(
    emit_object<OutputIterator>& oo,
    Qt::ItemFlags const & value,
    Qt::ItemFlags const & defaults = Qt::ItemFlags{}) {
    Qt::ItemFlags o = value ^ defaults;
    if (o.testFlag(Qt::NoItemFlags))
      return;
    if (o.testFlag(Qt::ItemIsSelectable))
      oo.def("selectable", value.testFlag(Qt::ItemIsSelectable));
    if (o.testFlag(Qt::ItemIsEditable))
      oo.def("editable", value.testFlag(Qt::ItemIsEditable));
    if (o.testFlag(Qt::ItemIsDragEnabled))
      oo.def("dragEnabled", value.testFlag(Qt::ItemIsDragEnabled));
    if (o.testFlag(Qt::ItemIsDropEnabled))
      oo.def("dropEnabled", value.testFlag(Qt::ItemIsDropEnabled));
    if (o.testFlag(Qt::ItemIsUserCheckable))
      oo.def("userCheckable", value.testFlag(Qt::ItemIsUserCheckable));
    if (o.testFlag(Qt::ItemIsEnabled))
      oo.def("enabled", value.testFlag(Qt::ItemIsEnabled));
    if (o.testFlag(Qt::ItemIsAutoTristate))
      oo.def("autoTristate", value.testFlag(Qt::ItemIsAutoTristate));
    // if (o.testFlag(Qt::ItemIsTristate)) oo.def("tristate", value.testFlag(Qt::ItemIsTristate)); // This enum value is
    // deprecated. Use Qt::ItemIsAutoTristate instead. if (o.testFlag(Qt::ItemNeverHasChildren))
    // oo.def("neverHasChildren", value.testFlag(Qt::ItemNeverHasChildren)); // The item never has child items. This is
    // used for optimization purposes only.
    if (o.testFlag(Qt::ItemIsUserTristate))
      oo.def("userTristate", value.testFlag(Qt::ItemIsUserTristate));
  }

  template <class OutputIterator>
  struct emit<OutputIterator, QBoxLayout::Direction> {
    emit(OutputIterator& oi, QBoxLayout::Direction const & o) {
      switch (o) {
      case QBoxLayout::LeftToRight:
        emit<OutputIterator, string>(oi, "leftToRight"s);
        break;
      case QBoxLayout::RightToLeft:
        emit<OutputIterator, string>(oi, "rightToLeft"s);
        break;
      case QBoxLayout::TopToBottom:
        emit<OutputIterator, string>(oi, "topToBottom"s);
        break;
      case QBoxLayout::BottomToTop:
        emit<OutputIterator, string>(oi, "bottomToTop"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QTreeWidgetItem::ChildIndicatorPolicy> {
    emit(OutputIterator& oi, QTreeWidgetItem::ChildIndicatorPolicy const & o) {
      switch (o) {
      case QTreeWidgetItem::ShowIndicator:
        emit<OutputIterator, string>(oi, "showIndicator"s);
        break;
      case QTreeWidgetItem::DontShowIndicator:
        emit<OutputIterator, string>(oi, "dontShowIndicator"s);
        break;
      case QTreeWidgetItem::DontShowIndicatorWhenChildless:
        emit<OutputIterator, string>(oi, "dontShowIndicatorWhenChildless"s);
        break;
      }
    }
  };
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)) // TickType (Fixed|Dynamic) introduced in Qt 5.12
  template <class OutputIterator>
  struct emit<OutputIterator, QValueAxis::TickType> {
    emit(OutputIterator& oi, QValueAxis::TickType const & o) {
      switch (o) {
      case QValueAxis::TicksDynamic:
        emit<OutputIterator, string>(oi, "dynamic"s);
        break;
      case QValueAxis::TicksFixed:
        emit<OutputIterator, string>(oi, "fixed"s);
        break;
      }
    }
  };
#endif
  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractAxis::AxisType> {
    emit(OutputIterator& oi, QAbstractAxis::AxisType const & o) {
      switch (o) {
      case QAbstractAxis::AxisType::AxisTypeNoAxis:
        emit<OutputIterator, string>(oi, "none"s);
        break;
      case QAbstractAxis::AxisType::AxisTypeValue:
        emit<OutputIterator, string>(oi, "value"s);
        break;
      case QAbstractAxis::AxisType::AxisTypeBarCategory:
        emit<OutputIterator, string>(oi, "barCategory"s);
        break;
      case QAbstractAxis::AxisType::AxisTypeCategory:
        emit<OutputIterator, string>(oi, "category"s);
        break;
      case QAbstractAxis::AxisType::AxisTypeDateTime:
        emit<OutputIterator, string>(oi, "dateTime"s);
        break;
      case QAbstractAxis::AxisType::AxisTypeLogValue:
        emit<OutputIterator, string>(oi, "logValue"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractSeries::SeriesType> {
    emit(OutputIterator& oi, QAbstractSeries::SeriesType const & o) {
      switch (o) {
      case QAbstractSeries::SeriesTypeLine:
        emit<OutputIterator, string>(oi, "line"s);
        break;
      case QAbstractSeries::SeriesTypeArea:
        emit<OutputIterator, string>(oi, "area"s);
        break;
      case QAbstractSeries::SeriesTypeBar:
        emit<OutputIterator, string>(oi, "bar"s);
        break;
      case QAbstractSeries::SeriesTypeStackedBar:
        emit<OutputIterator, string>(oi, "stackedBar"s);
        break;
      case QAbstractSeries::SeriesTypePercentBar:
        emit<OutputIterator, string>(oi, "percentBar"s);
        break;
      case QAbstractSeries::SeriesTypePie:
        emit<OutputIterator, string>(oi, "pie"s);
        break;
      case QAbstractSeries::SeriesTypeScatter:
        emit<OutputIterator, string>(oi, "scatter"s);
        break;
      case QAbstractSeries::SeriesTypeSpline:
        emit<OutputIterator, string>(oi, "spline"s);
        break;
      case QAbstractSeries::SeriesTypeHorizontalBar:
        emit<OutputIterator, string>(oi, "horizontalBar"s);
        break;
      case QAbstractSeries::SeriesTypeHorizontalStackedBar:
        emit<OutputIterator, string>(oi, "horizontalStackedBar"s);
        break;
      case QAbstractSeries::SeriesTypeHorizontalPercentBar:
        emit<OutputIterator, string>(oi, "horizontalPercentBar"s);
        break;
      case QAbstractSeries::SeriesTypeBoxPlot:
        emit<OutputIterator, string>(oi, "boxPlot"s);
        break;
      case QAbstractSeries::SeriesTypeCandlestick:
        emit<OutputIterator, string>(oi, "candleStick"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QFrame::Shape> {
    emit(OutputIterator& oi, QFrame::Shape const & o) {
      switch (o) {
      case QFrame::NoFrame:
        emit<OutputIterator, string>(oi, "none"s);
        break;
      case QFrame::Box:
        emit<OutputIterator, string>(oi, "box"s);
        break;
      case QFrame::Panel:
        emit<OutputIterator, string>(oi, "panel"s);
        break;
      case QFrame::StyledPanel:
        emit<OutputIterator, string>(oi, "styledPanel"s);
        break;
      case QFrame::HLine:
        emit<OutputIterator, string>(oi, "horizontalLine"s);
        break;
      case QFrame::VLine:
        emit<OutputIterator, string>(oi, "verticalLine"s);
        break;
      case QFrame::WinPanel:
        emit<OutputIterator, string>(oi, "winPanel"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QFrame::Shadow> {
    emit(OutputIterator& oi, QFrame::Shadow const & o) {
      switch (o) {
      case QFrame::Plain:
        emit<OutputIterator, string>(oi, "plain"s);
        break;
      case QFrame::Raised:
        emit<OutputIterator, string>(oi, "raised"s);
        break;
      case QFrame::Sunken:
        emit<OutputIterator, string>(oi, "sunken"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::ScrollBarPolicy> {
    emit(OutputIterator& oi, Qt::ScrollBarPolicy const & o) {
      switch (o) {
      case Qt::ScrollBarAsNeeded:
        emit<OutputIterator, string>(oi, "auto"s);
        break;
      case Qt::ScrollBarAlwaysOff:
        emit<OutputIterator, string>(oi, "never"s);
        break;
      case Qt::ScrollBarAlwaysOn:
        emit<OutputIterator, string>(oi, "always"s);
        break;
      }
    }
  };

  /**
   * Emit list-like attribute.
   * Callable fg(int index) is used to fetch items.
   */
  template <class OutputIterator, class Fx>
  void emit_list_fx(OutputIterator& oi, int n, Fx&& fx) {
    *oi++ = arr_begin;
    for (int i = 0; i < n; ++i) {
      auto v = fx(i);
      if (i > 0)
        *oi++ = comma;
      emit<OutputIterator, decltype(v)>(oi, v);
    }
    *oi++ = arr_end;
  }

  /**
   * Emit list-like attribute with validation of values.
   * Callable fg(int index) is used to fetch items, and fv(T value, int index)
   * is used to decide if a given value should be emitted or not (e.g. can
   * check for nullptrs). If no values are considered valid an empty list will
   * be emitted.
   */
  template <class OutputIterator, class Fget, class Fvalid>
  void emit_list_fx(OutputIterator& oi, int n, Fget&& fg, Fvalid&& fv) {
    *oi++ = arr_begin;
    for (int i = 0; i < n; ++i) {
      auto v = fg(i);
      if (fv(v, i)) {
        if (i > 0)
          *oi++ = comma;
        emit<OutputIterator, decltype(v)>(oi, v);
      }
    }
    *oi++ = arr_end;
  }

  /**
   * Emit entries from the start of list-like attribute if any of them has a valid value.
   * Argument n specifies the size of the list, but unless argument all is true then only
   * values from the start and until the last valid value are emitted, skipping any non-valid
   * entries at the end. Intermediate non-valid values will be emitted using the same emitter
   * as valid values, meaning e.g. nullptr will be emitted by pointer-emitter as json null value.
   * Callable fg(int index) is used to fetch items, and fv(T value, int index)
   * is used to decide if a value is valid.
   */
  template <class OutputIterator, class Fget, class Fvalid>
  void
    def_optional_list_fx(emit_object<OutputIterator>& oo, string const & name, int n, bool all, Fget&& fg, Fvalid&& fv) {
    if (n < 1)
      return;
    int flag =
      0; // Not a counter since we restart on first valid, so 1 means found one valid, 2 means emitted first array entry
    int m = all ? n : 0;
    for (int i = n - 1; i >= 0 && m < 1 /* implied: flag == 0*/; --i) { // Find last valid value (when all==false)
      auto v = fg(i); // When this returns a valid value, this one call to Fget will be repeated later below (wasted
                      // time, but hopefully cheap).
      if (fv(v, i)) {
        oo.sep();
        emit<OutputIterator, string>(oo.oi, name);
        *oo.oi++ = colon;
        *oo.oi++ = arr_begin;
        ++flag;
        m = i + 1;
      }
    }
    for (int i = 0; i < m;) {
      auto v = fg(i);
      if (flag == 0 && fv(v, i)) { // Detect first valid value (meaning all==true, if false we have found valid value
                                   // and set the flag above)
        oo.sep();
        emit<OutputIterator, string>(oo.oi, name);
        *oo.oi++ = colon;
        *oo.oi++ = arr_begin;
        ++flag;
        if (i > 0) {
          i = 0;
          continue; // Restart with flag set
        }
      }
      if (flag > 0) {
        if (flag > 1)
          *oo.oi++ = comma;
        emit<OutputIterator, decltype(v)>(oo.oi, v);
        if (flag == 1)
          ++flag;
      }
      ++i;
    }
    if (flag)
      *oo.oi++ = arr_end;
  }

  QList<QWidget*> getChildWidgets(QWidget const & parent, bool includeInternal = false, bool recurse = false) {
    // Retrieve child widgets (owned objects of type QWidget).
    // By default excludes widgets with object name prefix "qt_", used by objects automatically created by Qt.
    // By default only immediate children.
    if (includeInternal)
      return parent.findChildren<QWidget*>(
        QString{}, recurse ? Qt::FindChildrenRecursively : Qt::FindDirectChildrenOnly);
    else
      return parent.findChildren<QWidget*>(
        QRegularExpression{"^(?!qt_).*$"}, recurse ? Qt::FindChildrenRecursively : Qt::FindDirectChildrenOnly);
  }

  //
  // Objects
  //

  /**
   * Emit basic Qt object (QObject).
   *
   * Normally (always?) the emitting will be performed on inherited types, such as a widget (QWidget),
   * and even multiple levels of inheritance like QTableWidget. Instead of nesting the emitted structure
   * correspondingly, it is preferred to emit a composite object including any inherited properties directly.
   * Therefore the object emitter instance created here will be kept as a member, so that emitters for
   * inherited types can keep using the same.
   */
  template <class OutputIterator>
  struct emit<OutputIterator, QObject> {
    emit_object<OutputIterator> oo;

    emit(OutputIterator& oi, QObject const & o)
      : oo{oi} {
      if (!o.objectName().isEmpty())
        oo.def("id", o.objectName()); // Calling this "id" as it more for internal purposes than a "name", and some
                                      // specific types (such as QAbstractSeries) have separate "name" attribute so
                                      // should not call it just "name".
      // Set any Qt dynamic properties as plain name-value pairs of a common object value.
      // Supported primitive types are emitted as the corresponding (json) type, any other
      // types are set as strings if they can be converted to one, rest is just skipped.
      // All emitted property values are therefore primitive values with no explicit type
      // information included, it is assumed that any producers and consumers of these
      // properties all know the value type to use for each named property.
      auto names = o.dynamicPropertyNames();
      for (auto i = 0; i < names.count(); ++i) {
        if (
          names[i] == "_PySideInvalidatePtr" // Remove dynamic property used for internal purposes in PySide2
          || names[i].startsWith(
            "_q_")) { // Remove dynamic properties starting with "_q_", reserved for internal purposes in Qt
          names.removeAt(i--);
        }
      }
      if (!names.empty()) {
        oo.def_fx("properties", [&o, &names](OutputIterator& oi) -> void {
          emit_object<OutputIterator> po(oi);
          for (const auto& p : names)
            po.def(p.data(), o.property(p.data()));
        });
      }
    }
  };

  //
  // Layouts
  //

  /**
   * Base class for specific layout emitters. Not an emitter itself.
   */
  template <class OutputIterator>
  struct base_emit_layout : emit<OutputIterator, QObject> {
    using emit<OutputIterator, QObject>::oo;

    base_emit_layout(OutputIterator& oi, QLayout const & o)
      : emit<OutputIterator, QObject>{oi, o} {
    }

    /**
     * Shared utility function for emitting layout spacing properties.
     * Value is initially -1, but when layout is used in some context this will return value inherited from
     * parent layout or style of parent widget. Also QGridLayout and GFormLayout can have separate horizontal/vertical
     * spacing and then this value returns -1 if any of them are set, unless both are set equal then that value is
     * returned.
     * TODO: Some non-zero automatially calculated value seems to be returned in many cases, so we do not know if
     * explicitely set or not?
     */
    void def_layout_spacing(QLayout const &) {
    }

    void def_layout_spacing(QBoxLayout const & o) {
      if (o.spacing() > 0)
        oo.def("spacing", o.spacing());
    }

    template <class LayoutType> // QGridLayout or QFormLayout, the only two with horizontal and vertical spacing
    void def_layout_spacing(LayoutType const & o) {
      if (o.spacing() > 0) {
        oo.def("spacing", o.spacing());
      } else if (o.horizontalSpacing() > 0 || o.verticalSpacing() > 0) {
        oo.def_fx("spacing", [&o](OutputIterator& oi) -> void {
          emit_object po(oi);
          po.def("horizontal", o.horizontalSpacing()).def("vertical", o.verticalSpacing());
        });
      }
    }

    /**
     * Shared utility function for emitting layout items (its content).
     */
    void def_layout_items(QLayout const & o) {
      oo.def_fx("items", [&o](OutputIterator& oi) -> void {
        *oi++ = arr_begin;
        for (auto i = 0; i < o.count(); ++i) {
          if (/*const*/ auto* p = o.itemAt(i)) {
            if (i > 0)
              *oi++ = comma;
            emit_object po(oi);
            if (const auto* q = p->widget()) { // If this QLayoutItem is actually a QWidgetItem, which manages a QWidget
                                               // (returns the QWidget directly)
              po.def("widget", *q);
            } else if (
              const auto* q = p->layout()) { // If this QLayoutItem is actually a QLayout (representing a child layout)
              po.def("layout", *q);
            } else if (const auto* q = p->spacerItem()) { // If this QLayoutItem is actually a QSpacerItem
              po.def("spacer", "true"s);
            }
          }
        }
        *oi++ = arr_end;
      });
    }

    void def_layout_items(QGridLayout const & o) {
      oo.def_fx("items", [&o](OutputIterator& oi) -> void {
        *oi++ = arr_begin;
        for (auto i = 0; i < o.count(); ++i) {
          if (/*const*/ auto* p = o.itemAt(i)) {
            if (i > 0)
              *oi++ = comma;
            emit_object po(oi);
            int x, y, h, w;
            o.getItemPosition(i, &y, &x, &h, &w);
            po.def("rect", QRect{x, y, w, h});
            if (const auto* q = p->widget()) { // If this QLayoutItem is actually a QWidgetItem, which manages a QWidget
                                               // (returns the QWidget directly)
              po.def("widget", *q);
            } else if (
              const auto* q = p->layout()) { // If this QLayoutItem is actually a QLayout (representing a child layout)
              po.def("layout", *q);
            } else if (const auto* q = p->spacerItem()) { // If this QLayoutItem is actually a QSpacerItem
              po.def("spacer", "true"s);
            }
          }
        }
        *oi++ = arr_end;
      });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QBoxLayout> : base_emit_layout<OutputIterator> {
    using base_emit_layout<OutputIterator>::oo;

    emit(OutputIterator& oi, QBoxLayout const & o)
      : base_emit_layout<OutputIterator>{oi, o} {
      oo.def("type", "box"s).def("direction", o.direction());
      // def_layout_spacing(oo, o);
      // def_layout_stretch(oo, o, true);
      base_emit_layout<OutputIterator>::def_layout_items(o);
    }

    void def_layout_stretch(QBoxLayout const & o) {
      // Stretch alternative 1: Array of values for all items, but only if at least one of them is explicitely set (not
      // default zero value).
      def_optional_list_fx(
        oo,
        "stretch"s,
        o.count(),
        false,
        [&o](int i) {
          return o.stretch(i);
        },
        [](int const & v, int) {
          return v != 0;
        });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QGridLayout> : base_emit_layout<OutputIterator> {
    using base_emit_layout<OutputIterator>::oo;

    emit(OutputIterator& oi, QGridLayout const & o)
      : base_emit_layout<OutputIterator>{oi, o} {
      oo.def("type", "grid"s);
      oo.def("rowCount", o.rowCount()).def("columnCount", o.columnCount());
      // def_layout_spacing(oo, o);
      // def_layout_stretch(oo, o);
      base_emit_layout<OutputIterator>::def_layout_items(o);
    }

    void def_layout_stretch(QGridLayout const & o) {
      // Stretch
      // Alternative 1: Array of values for all items, but only if at least one of them is explicitely set (not default
      // zero value).
      def_optional_list_fx(
        oo,
        "rowStretch"s,
        o.rowCount(),
        false,
        [&o](int i) {
          return o.rowStretch(i);
        },
        [](int const & v, int) {
          return v != 0;
        });
      def_optional_list_fx(
        oo,
        "columnStretch"s,
        o.count(),
        false,
        [&o](int i) {
          return o.columnStretch(i);
        },
        [](int const & v, int) {
          return v != 0;
        });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QStackedLayout> : base_emit_layout<OutputIterator> {
    using base_emit_layout<OutputIterator>::oo;

    emit(OutputIterator& oi, QStackedLayout const & o)
      : base_emit_layout<OutputIterator>{oi, o} {
      oo.def("type", "stacked"s);
      // def_layout_spacing(oo, o);
      // def_layout_stretch(oo, o, true);
      base_emit_layout<OutputIterator>::def_layout_items(o);
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QFormLayout> : base_emit_layout<OutputIterator> {
    using base_emit_layout<OutputIterator>::oo;

    emit(OutputIterator& oi, QFormLayout const & o)
      : base_emit_layout<OutputIterator>{oi, o} {
      oo.def("type", "form"s);
      // def_layout_spacing(oo, o);
      // def_layout_stretch(oo, o, true);
      base_emit_layout<OutputIterator>::def_layout_items(o);
    }
  };

  /**
   * Emit Qt layout objects (QLayout).
   *
   * Assuming the base layout class QLayout, as returned by QWidget::layout() etc,
   * is the "entry point" for emitting any layout type. It will always be emitted
   * as a single object, where properties from specialized layout types will be included.
   */
  template <class OutputIterator>
  struct emit<OutputIterator, QLayout> {
    emit(OutputIterator& oi, QLayout const & o) {
      if (auto const * p = dynamic_cast< QBoxLayout const *>(&o))
        emit<OutputIterator, QBoxLayout>(oi, *p);
      else if (auto const * p = dynamic_cast< QGridLayout const *>(&o))
        emit<OutputIterator, QGridLayout>(oi, *p);
      else if (auto const * p = dynamic_cast< QStackedLayout const *>(&o))
        emit<OutputIterator, QStackedLayout>(oi, *p);
      else if (auto const * p = dynamic_cast< QFormLayout const *>(&o))
        emit<OutputIterator, QFormLayout>(oi, *p);
      else { // Not a known layout, emit just the properties known to a basic QLayout
        base_emit_layout<OutputIterator> base{oi, o};
        base.oo.def("type", "unknown"s);
      }
    }
  };

  //
  // Widgets
  //

  template <class OutputIterator>
  struct base_emit_widget : emit<OutputIterator, QObject> {
    // Base class for specific widget emitters. Not an emitter itself.
    using emit<OutputIterator, QObject>::oo;

    base_emit_widget(OutputIterator& oi, QWidget const & o, bool recurse)
      : emit<OutputIterator, QObject>{oi, o} {
      if (!o.isEnabled())
        oo.def("enabled", o.isEnabled());
      // if (!o.isVisible()) oo.def("visible", o.isVisible());
      if (!o.windowTitle().isEmpty())
        oo.def(
          "caption",
          o.windowTitle()); // The caption, or window title. Only considered by Qt for top-level widgets (windows), but
                            // allowed to be set for any widget. Specific types (such as QChartView and QAbstractAxis)
                            // have separate "title" attribute so should not call it that.
      if (!o.windowRole().isEmpty())
        oo.def("role", o.windowRole()); // The window role. Only considered by Qt for top-level widgets (windows), and
                                        // only on X11, but allowed to be set for any widget.
      if (!o.toolTip().isEmpty())
        oo.def("toolTip", o.toolTip());
      if (!o.statusTip().isEmpty())
        oo.def("statusTip", o.statusTip());
      if (!o.whatsThis().isEmpty())
        oo.def("whatsThis", o.whatsThis());
      if (!o.accessibleName().isEmpty())
        oo.def("accessibleName", o.accessibleName());
      if (!o.accessibleDescription().isEmpty())
        oo.def("accessibleDescription", o.accessibleDescription());
      if (!o.styleSheet().isEmpty())
        oo.def("styleSheet", o.styleSheet());
      if (recurse) {
        if (auto const * p = o.layout()) { // If widget has a layout then assume any children are managed by this! This
                                           // is not a restriction in Qt, but a limitation chosen here!
          oo.def("layout", *p);
        } else { // If widget does not have a layout then emit any child widgets added directly
          auto childs = o.findChildren<QWidget*>(
            QString{}, Qt::FindDirectChildrenOnly); // Find any child widgets, ignore any non-widget (QObject) children
          if (childs.count())
            oo.def("widgets", childs);
        }
      }
    }
  };

  template <class OutputIterator>
  struct base_emit_frame : base_emit_widget<OutputIterator> {
    // Base class for specific widget emitters that inherits QFrame, such as QLabel and QScrollArea. Not an emitter
    // itself.
    using base_emit_widget<OutputIterator>::oo;

    base_emit_frame(
      OutputIterator& oi,
      QFrame const & o,
      QFrame::Shape defaultFrameShape = QFrame::NoFrame,
      QFrame::Shadow defaultFrameShadow = QFrame::Plain,
      int defaultLineWidth = 1,
      int defaultMidLineWidth = 0)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      if (o.frameShape() != defaultFrameShape)
        oo.def("frameShape", o.frameShape());
      if (o.frameShadow() != defaultFrameShadow)
        oo.def("frameShadow", o.frameShadow());
      if (o.lineWidth() != defaultLineWidth)
        oo.def("lineWidth", o.lineWidth());
      if (o.midLineWidth() != defaultMidLineWidth)
        oo.def("midLineWidth", o.midLineWidth());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QLabel> : base_emit_frame<OutputIterator> {
    using base_emit_frame<OutputIterator>::oo;

    emit(OutputIterator& oi, QLabel const & o)
      : base_emit_frame<OutputIterator>{oi, o} {
      oo.def("type", "label"s).def("text", o.text());
      if (o.alignment() != (Qt::AlignLeft | Qt::AlignVCenter))
        def_alignment(oo, o.alignment());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QLineEdit::EchoMode> {
    emit(OutputIterator& oi, QLineEdit::EchoMode const & o) {
      switch (o) {
      case QLineEdit::Normal:
        emit<OutputIterator, string>(oi, "normal");
        break;
      case QLineEdit::NoEcho:
        emit<OutputIterator, string>(oi, "none");
        break;
      case QLineEdit::Password:
        emit<OutputIterator, string>(oi, "password");
        break;
      case QLineEdit::PasswordEchoOnEdit:
        emit<OutputIterator, string>(oi, "passwordEchoOnEdit");
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QLineEdit> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QLineEdit const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      oo.def("type", "lineEdit"s);
      if (!o.placeholderText().isEmpty())
        oo.def("placeholderText", o.placeholderText());
      if (!o.text().isEmpty())
        oo.def("text", o.text());
      if (o.cursorPosition() != o.text().length())
        oo.def("cursorPosition", o.cursorPosition());
      if (o.selectionLength() != 0) {
        oo.def_fx("selection", [&o](OutputIterator& oi) -> void {
          *oi++ = arr_begin;
          emit<OutputIterator, int>(oi, o.selectionStart());
          *oi++ = comma;
          emit<OutputIterator, int>(oi, o.selectionEnd());
          *oi++ = arr_end;
        });
      }
      if (o.maxLength() != 32767)
        oo.def("maxLength", o.maxLength());
      if (!o.hasFrame())
        oo.def("frame", o.hasFrame());
      if (o.isReadOnly())
        oo.def("readOnly", o.isReadOnly());
      if (o.echoMode() != QLineEdit::Normal)
        oo.def("echoMode", o.echoMode());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractSpinBox> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QAbstractSpinBox const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      if (auto const * p = dynamic_cast< QSpinBox const *>(&o)) {
        oo.def("type", "intSpinBox"s);
        if (p->value() != 0)
          oo.def("value", p->value());
        if (p->minimum() != 0)
          oo.def("minimum", p->minimum());
        if (p->maximum() != 99)
          oo.def("maximum", p->maximum());
        if (p->singleStep() != 1)
          oo.def("step", p->singleStep());
        if (!p->prefix().isEmpty())
          oo.def("prefix", p->prefix());
        if (!p->suffix().isEmpty())
          oo.def("suffix", p->suffix());
      } else if (auto const * p = dynamic_cast< QDoubleSpinBox const *>(&o)) {
        oo.def("type", "doubleSpinBox"s);
        if (p->value() != 0.0)
          oo.def("value", p->value());
        if (p->minimum() != 0.0)
          oo.def("minimum", p->minimum());
        if (p->maximum() != 99.99)
          oo.def("maximum", p->maximum());
        if (p->singleStep() != 1.0)
          oo.def("step", p->singleStep());
        if (p->decimals() != 2)
          oo.def("decimals", p->decimals());
        if (!p->prefix().isEmpty())
          oo.def("prefix", p->prefix());
        if (!p->suffix().isEmpty())
          oo.def("suffix", p->suffix());
      } else if (auto const * p = dynamic_cast< QDateTimeEdit const *>(&o)) {
        oo.def("type", "dateTimeSpinBox"s);
        // TODO
      }
      if (!o.specialValueText().isEmpty())
        oo.def("specialValueText", o.specialValueText());
      if (o.wrapping())
        oo.def("wrapping", o.wrapping());
      if (o.isReadOnly())
        oo.def("readOnly", o.isReadOnly());
      if (!o.alignment().testFlag(Qt::AlignLeft))
        def_alignment_for_orientation(oo, "alignment", Qt::Horizontal, o.alignment());
      switch (o.buttonSymbols()) {
      case QAbstractSpinBox::UpDownArrows:
        break; // Skip default
      case QAbstractSpinBox::PlusMinus:
        oo.def("symbols", "plusMinus"s);
        break;
      case QAbstractSpinBox::NoButtons:
        oo.def("symbols", "none"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractButton> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QAbstractButton const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      if (!o.text().isEmpty())
        oo.def("text", o.text());
      auto* group = o.group();
      if (group) {
        // If button is associated with a group we set the group as property directly
        // on the button and does not emit the group as an object on its own.
        if (group->objectName()
              .isEmpty()) { // We need a unique identifier to be able to refer to the group in emitted json
#if (QT_VERSION >= QT_VERSION_CHECK(5, 11, 0)) // QUuid::StringFormat introduced in Qt 5.12
          group->setObjectName(QUuid::createUuid().toString(QUuid::WithoutBraces));
#else
          group->setObjectName(QUuid::createUuid().toString());
#endif
        }
        oo.def("group", group->objectName());
        if (group->exclusive())
          oo.def(
            "exclusive",
            group->exclusive()); // Button groups are exclusive by default, but probably less confusing to alway emit
                                 // this property (unlike the autoExclusive properties, see below).
      }
      if (auto const * p = dynamic_cast< QPushButton const *>(&o)) {
        oo.def("type", "pushButton"s);
        if (p->isCheckable()) { // Push buttons can also be checkable, like radio buttons and checkbox, and will behave
                                // "sticky"with two states
          oo.def("checked", p->isChecked());
          if (!group && p->autoExclusive())
            oo.def(
              "exclusive",
              p->autoExclusive()); // Checkable pushbuttons are not autoExclusive by default. The autoExclusive property
                                   // has no effect on buttons that belong to a button group. Emitting it with same
                                   // property name as for exclusive group.
        }
        if (p->isDefault())
          oo.def("default", p->isDefault());
        if (o.actions().count())
          oo.def(
            "actions",
            o.actions()); // Push buttons can also have actions, like tool buttons, but only used for menu (push button
                          // will not inherit properties from some default action like a tool button will)
      } else if (auto const * p = dynamic_cast< QRadioButton const *>(&o)) {
        oo.def("type", "radioButton"s);
        if (!group && !p->autoExclusive())
          oo.def("exclusive", p->autoExclusive()); // Radio buttons are autoExclusive by default. See notes for
                                                   // pushbutton and group above.
        oo.def("checked", p->isChecked());
      } else if (auto const * p = dynamic_cast< QCheckBox const *>(&o)) {
        oo.def("type", "checkBox"s);
        if (p->isTristate())
          oo.def("tristate", p->isTristate());
        if (!group && p->autoExclusive())
          oo.def("exclusive", p->autoExclusive()); // Check boxes are not autoExclusive by default. See notes for
                                                   // pushbutton and group above.
        oo.def("checked", p->isChecked());
      } else if (auto const * p = dynamic_cast< QToolButton const *>(&o)) {
        oo.def("type", "toolButton"s);
        // Even though it inherits QAbstractButton, which again inherits QWidget, the tool buttons are
        // special since it usually gets most properties defined from a default action.
        // Note regarding defaultAction:
        // - The toolbutton may have one or many associated actions, returned by actions(),
        //   and may have one of them as the "default action", returned by defaultAction().
        //   Here we just emitt all actions, and does nothing special to identify the default.
        // - If the toolbutton actually has a default action, then the value of properties
        //   text and toolTip will default to those from the action. So if text/toolTip on the
        //   action are unique, and these have not been explicitely specified on the toolbutton,
        //   then these can be used by the consumer to identify which action is the default.
        // - The toolTip property has at this point already been emitted from base_emit_widget,
        //   so we always emit the text (unless empty) here, without any check if it has a default
        //   value or not.
        // Note regarding menu:
        // - A menu can be associated with a toolbutton, as well as with individual actions. It
        //   seems that setMenu on a toolbutton is merely a convenience function for creating
        //   an action with menu, so therefore not emitting menus from the toolbutton itself
        //   but only via the associated actions.
        if (o.actions().count())
          oo.def("actions", o.actions());
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QDialogButtonBox> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QDialogButtonBox const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      oo.def("type", "dialogButtonBox"s);
      if (o.orientation() != Qt::Horizontal)
        oo.def("orientation", o.orientation());
      if (o.centerButtons())
        def_alignment_for_orientation(
          oo, "alignment", Qt::Horizontal, Qt::AlignHCenter); // Re-use orientation with hardcoded "center" value!
      // Note regarding buttons:
      //  - There are standard buttons identified by name ("ok", "save" etc) and there are button objects.
      //    The button objects can be custom buttons, but the standard buttons are also represented here!
      //    There is a lookup function button that takes standard button enum value as argument and returns
      //    the corresponding button object if present, could use this to link them.
      //  - There is a role associated with each button, but this is kept on this buttonbox object
      //    in a separate member buttonRole that maps a button to an ButtonRole enum value, so its
      //    not part of the buttons itself that are emitted here!
      // def_standard_buttons(oo, o.standardButtons());
      if (o.buttons().count())
        oo.def("buttons", o.buttons());
    }

    void def_standard_buttons(emit_object<OutputIterator>& oo, QDialogButtonBox::StandardButtons const & o) {
      std::vector<string> buttonNames;
      if (o.testFlag(QDialogButtonBox::Ok))
        buttonNames.push_back("ok");
      if (o.testFlag(QDialogButtonBox::Open))
        buttonNames.push_back("open");
      if (o.testFlag(QDialogButtonBox::Save))
        buttonNames.push_back("save");
      if (o.testFlag(QDialogButtonBox::Cancel))
        buttonNames.push_back("cancel");
      if (o.testFlag(QDialogButtonBox::Close))
        buttonNames.push_back("close");
      if (o.testFlag(QDialogButtonBox::Discard))
        buttonNames.push_back("discard");
      if (o.testFlag(QDialogButtonBox::Apply))
        buttonNames.push_back("apply");
      if (o.testFlag(QDialogButtonBox::Reset))
        buttonNames.push_back("reset");
      if (o.testFlag(QDialogButtonBox::RestoreDefaults))
        buttonNames.push_back("restoreDefaults");
      if (o.testFlag(QDialogButtonBox::Help))
        buttonNames.push_back("help");
      if (o.testFlag(QDialogButtonBox::SaveAll))
        buttonNames.push_back("saveAll");
      if (o.testFlag(QDialogButtonBox::Yes))
        buttonNames.push_back("yes");
      if (o.testFlag(QDialogButtonBox::YesToAll))
        buttonNames.push_back("yesToAll");
      if (o.testFlag(QDialogButtonBox::No))
        buttonNames.push_back("no");
      if (o.testFlag(QDialogButtonBox::NoToAll))
        buttonNames.push_back("noToAll");
      if (o.testFlag(QDialogButtonBox::Abort))
        buttonNames.push_back("abort");
      if (o.testFlag(QDialogButtonBox::Retry))
        buttonNames.push_back("retry");
      if (o.testFlag(QDialogButtonBox::Ignore))
        buttonNames.push_back("ignore");
      def_optional_list_fx(
        oo,
        "standardButtons"s,
        buttonNames.size(),
        false,
        [&buttonNames](int i) {
          return buttonNames[i];
        },
        [](string const & v, int) {
          return true;
        });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QTreeWidget> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QTreeWidget const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      oo.def("type", "tree"s).def("columnCount", o.columnCount());
      //.def("indentation", o.indentation()) // By default, the value of this property is style dependent. Thus, when
      //the style changes, this property updates from it. .def("isHeaderHidden", o.isHeaderHidden());
      if (!o.isHeaderHidden())
        if (auto const * p = o.headerItem())
          oo.def("header", *p);
      oo.def_fx("items", [&o](OutputIterator& oi) -> void {
        emit_list_fx(
          oi,
          o.topLevelItemCount(),
          [&o](int i) {
            return o.topLevelItem(i);
          },
          [](const auto* v, int) {
            return v != nullptr;
          });
      });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QToolBar> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QToolBar const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      oo.def("type", "toolbar"s).def("orientation", o.orientation());
      // if (o.actions().count()) oo.def("actions", o.actions()); // Calling QWidget::actions, any widget can have
      // action but currently only considered for QToolBar
      auto childs = getChildWidgets(o);
      if (childs.count())
        oo.def("widgets", childs);
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QMenu> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QMenu const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      if (o.actions().count())
        oo.def("actions", o.actions());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QTableWidget> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QTableWidget const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      oo.def("type", "table"s).def("columnCount", o.columnCount()).def("rowCount", o.rowCount());
      // Emit headers. These are QTableWidgetItem object same as cells, so re-using the same emitter.
      // Note that column headers are created by default, with values "A", "B", "C" etc, and with the flag
      // Qt::ItemNeverHasChildren set.
      def_optional_list_fx(
        oo,
        "columnHeaders"s,
        o.columnCount(),
        false,
        [&o](int i) {
          return o.horizontalHeaderItem(i);
        },
        [](QTableWidgetItem const * v, int) {
          return v != nullptr;
        });
      def_optional_list_fx(
        oo,
        "rowHeaders"s,
        o.rowCount(),
        false,
        [&o](int i) {
          return o.verticalHeaderItem(i);
        },
        [](QTableWidgetItem const * v, int /*i*/) {
          return v != nullptr;
        });
      // Emit rows with an object ("cell") for each column, but only as long as there are values - so figure out how
      // many rows with values first. Note: This is basically doing the same as def_optional_list_fx, but with two
      // dimensions (rows and columns) so can not just call that.
      int rowsWithContent = 0;
      for (int i = o.rowCount() - 1; i >= 0 && rowsWithContent < 1; --i) {
        for (int j = 0; j < o.columnCount(); ++j) {
          if (auto const * p = o.item(i, j)) {
            rowsWithContent = i + 1; // This is the last row containing data
            break;
          }
        }
      }
      if (rowsWithContent > 0) {
        oo.def_fx("rows", [&rowsWithContent, &o](OutputIterator& oi) -> void {
          *oi++ = arr_begin;
          for (int i = 0; i < rowsWithContent; ++i) {
            if (i > 0)
              *oi++ = comma;
            *oi++ = arr_begin;
            for (int j = 0; j < o.columnCount(); ++j) {
              if (j > 0)
                *oi++ = comma;
#if 1 // Emit the nullptr for empty items, leading to json null value
              emit<OutputIterator, QTableWidgetItem*>(oi, o.item(i, j));
#else // Emit empty items as empty object instead of just emitting the nullptr
                        if (const auto* p = o.item(i, j)) {
                            emit<OutputIterator, QTableWidgetItem>(oi, *p);
                        } else {
                            emit_object po(oi);
                        }
#endif
            }
            *oi++ = arr_end;
          }
          *oi++ = arr_end;
        });
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QChartView> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QChartView const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      oo.def("type", "chart"s);
      if (auto const * p = o.chart()) {
        oo.def("title", p->title());
        oo.def("axes", p->axes());
        oo.def("series", p->series());
        if (p->legend()->isVisible()) // Default true
          oo.def_fx("legend", [&p](OutputIterator& oi) -> void {
            emit_object po(oi);
            po.def("visible", p->legend()->isVisible());
          });
        if (
          p->isBackgroundVisible()                                                                // Default true
          && (p->backgroundPen().style() != Qt::NoPen || emittableBrush(p->backgroundBrush()))) { // NOTE: Not just
                                                                                                  // checking
                                                                                                  // !=Qt::NoBrush for
                                                                                                  // now, since only
                                                                                                  // color based brushes
                                                                                                  // are currently
                                                                                                  // supported and
                                                                                                  // LinearGradient is
                                                                                                  // default so we avoid
                                                                                                  // emitting that
                                                                                                  // unnecessary.
          oo.def_fx("background", [this, &p](OutputIterator& oi) -> void {
            emit_object po(oi);
            // po.def("visible", p->isBackgroundVisible()); // Default: true
            if (p->backgroundPen().style() != Qt::NoPen)
              po.def("pen", p->backgroundPen());      // Default: NoPen style
            if (emittableBrush(p->backgroundBrush())) // NOTE: See comment for use of emittableBrush above.
              po.def("brush", p->backgroundBrush());  // Default: LinearGradient style
          });
        }
        if (
          p->isPlotAreaBackgroundVisible() // Default false, meaning the plot area uses the general chart background
                                           // (but can be set visible even when background is not)
          && (p->plotAreaBackgroundPen().style() != Qt::NoPen || emittableBrush(p->plotAreaBackgroundBrush()))) { // NOTE:
                                                                                                                  // See
                                                                                                                  // comment
                                                                                                                  // for
                                                                                                                  // use
                                                                                                                  // of
                                                                                                                  // emittableBrush
                                                                                                                  // above.
          oo.def_fx("plotArea", [this, &p](OutputIterator& oi) -> void {
            emit_object po(oi);
            // po.def("visible", p->isPlotAreaBackgroundVisible()); // Default: false
            // po.def("rect", p->plotArea()); // QRectF
            if (p->plotAreaBackgroundPen().style() != Qt::NoPen)
              po.def("pen", p->plotAreaBackgroundPen());      // Default: Solid style, 100% transparent white color
            if (emittableBrush(p->plotAreaBackgroundBrush())) // NOTE: See comment for use of emittableBrush above.
              po.def("brush", p->plotAreaBackgroundBrush());  // Default: NoBrush style
          });
        }
      }
    }

    bool emittableBrush(QBrush const & o) {
      // Small (temporary?) helper function to decide if an optional brush is worth emitting.
      // Just checking !=Qt::NoBrush is not enough, since only color based brushes are currently supported
      // and e.g. for chart background LinearGradient is default so we want to avoid emitting that unnecessary.
      // SEE: emit<OutputIterator, QBrush>
      switch (o.style()) {
      case Qt::NoBrush:
        return false; // No brush
      case Qt::LinearGradientPattern:
      case Qt::RadialGradientPattern:
      case Qt::ConicalGradientPattern:
        return false; // Gradient based brush: Not implemented
      case Qt::TexturePattern:
        return false; // Texture (image) based brush: Not implemented
      default:
        // Color based brush (Qt::SolidPattern, or one of several structure patterns).
        return o.color().isValid();
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QScrollArea> : base_emit_frame<OutputIterator> {
    using base_emit_frame<OutputIterator>::oo;

    emit(OutputIterator& oi, QScrollArea const & o)
      : base_emit_frame<OutputIterator>{oi, o, QFrame::StyledPanel, QFrame::Sunken} {
      oo.def("type", "scrollArea"s);
      if (o.horizontalScrollBarPolicy() != Qt::ScrollBarAsNeeded)
        oo.def("horizontalScrollBar", o.horizontalScrollBarPolicy());
      if (o.verticalScrollBarPolicy() != Qt::ScrollBarAsNeeded)
        oo.def("verticalScrollBar", o.verticalScrollBarPolicy());
      if (o.alignment() != (Qt::AlignLeft | Qt::AlignTop))
        def_alignment(oo, o.alignment());
      if (o.widgetResizable())
        oo.def("widgetResizable", o.widgetResizable());
      if (auto const * p = o.widget())
        oo.def("widget", o.widget());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QDialog> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QDialog const & o)
      : base_emit_widget<OutputIterator>{oi, o, true} {
      oo.def("type", "dialog"s);
      if (o.isModal())
        oo.def("modal", o.isModal());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QMessageBox> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;

    emit(OutputIterator& oi, QMessageBox const & o)
      : base_emit_widget<OutputIterator>{oi, o, false} {
      // Built-in convenience dialog, which is modal and contains a label, icon and buttons.
      // Could emit it using the general QDialog (or enable recursion in base_emit_widget)
      // but it is unnecessarily verbose assuming the receiver knows how to show a generic
      // message box. In addition the predefined icons are represented by a enum on the
      // QMessageBox while on the QDialog it will be a QLabel containing a QPixmap image
      // object that the receiver must know how to render.
      oo.def("type", "messageBox"s);
      switch (o.icon()) {
      case QMessageBox::NoIcon:
        oo.def("icon", "none"s);
        break;
      case QMessageBox::Question:
        oo.def("icon", "question"s);
        break;
      case QMessageBox::Information:
        oo.def("icon", "information"s);
        break;
      case QMessageBox::Warning:
        oo.def("icon", "warning"s);
        break;
      case QMessageBox::Critical:
        oo.def("icon", "critical"s);
        break;
      }
      if (!o.text().isEmpty())
        oo.def("label", o.text());
      if (!o.detailedText().isEmpty())
        oo.def("details", o.detailedText());
      if (o.buttons().count())
        oo.def("buttons", o.buttons());
    }
  };
#if 0
// NB: Removed custom emitter for QInputDialog, trying to just emit as base class QDialog instead!
//     One reason is that the QInputDialog does not have a method to retrieve buttons, like
//     QMessageBox does, and if the dialog callbacks should be hooked on to these we must
//     find them via the QDialogButtonBox widget contained in the layout. QDialog emitter already
//     does this, and using its recursive emit is not that verbose for QInputDialog, so will
//     se if that works out.
template<class OutputIterator>
struct emit<OutputIterator, QInputDialog> : base_emit_widget<OutputIterator> {
    using base_emit_widget<OutputIterator>::oo;
    emit(OutputIterator& oi, const QInputDialog& o) : base_emit_widget<OutputIterator>{ oi, o, true } {
        // Built-in convenience dialog, to get a single value from the user.
        // Shows SpinBox for numerical type input, LineEdit for single string type input,
        // and ComboBox or ListView for string type input with a predefined set of choices.
        // Could emit it using the general QDialog (or enable recursion in base_emit_widget)
        // but it is unnecessarily verbose assuming the receiver knows how to show a generic
        // input dialog.
        oo.def("type", "inputDialog"s);
        if (o.labelText() != "Enter a value :") oo.def("label", o.labelText());
        switch (o.inputMode()) {
        case QInputDialog::TextInput:
            oo.def("valueType", "string"s);
            if (!o.textValue().isEmpty()) oo.def("value", o.textValue());
            if (!o.comboBoxItems().isEmpty()) {
                oo.def("valueChoices", o.comboBoxItems());
                oo.def("valueChoicesPresentation", o.testOption(QInputDialog::UseListViewForComboBoxItems) ? "listView"s : "comboBox"s);
                oo.def("valueEditable", o.isComboBoxEditable());
            } else if (o.textEchoMode() != QLineEdit::Normal) {
                oo.def("valueEchoMode", o.textEchoMode());
            }
            break;
        case QInputDialog::IntInput:
            oo.def("valueType", "int"s);
            if (o.intValue() != 0) oo.def("value", o.intValue());
            if (o.intMinimum() != 0) oo.def("valueMinimum", o.intMinimum());
            if (o.intMaximum() != 99) oo.def("valueMaximum", o.intMaximum());
            if (o.intStep() != 1) oo.def("valueStep", o.intStep());
            break;
        case QInputDialog::DoubleInput:
            oo.def("valueType", "double"s);
            if (o.doubleValue() != 0.0) oo.def("value", o.doubleValue());
            if (o.doubleMinimum() != 0.0) oo.def("valueMinimum", o.doubleMinimum());
            if (o.doubleMaximum() != 99.99) oo.def("valueMaximum", o.doubleMaximum());
            if (o.doubleStep() != 1.0) oo.def("valueStep", o.doubleStep());
            if (o.doubleDecimals() != 2) oo.def("valueDecimals", o.doubleDecimals());
            break;
        }
        if (o.okButtonText() != "OK") oo.def("okButtonText", o.okButtonText());
        if (o.cancelButtonText() != "Cancel") oo.def("cancelButtonText", o.cancelButtonText());
    }
};
#endif
  /**
   * Emit Qt widget objects (QWidget).
   *
   * Assuming the base widget class QWidget is the "entry point" for emitting any
   * widget type. It will always be emitted as a single object, where properties
   * from specialized widget types will be included.
   * Assumptions/limitations:
   *  - Assuming specific widget types such as QLabel does not have custom child widgets/layouts.
   *  - Assuming generic widgets may have children via layout or direct children managed without
   *    layout, but not both at the same time - if there is a layout only child widgets included
   *    in it will be emitted!
   */
  template <class OutputIterator>
  struct emit<OutputIterator, QWidget> {
    emit(OutputIterator& oi, QWidget const & o) {
      if (auto const * p = dynamic_cast< QLabel const *>(&o))
        emit<OutputIterator, QLabel>(oi, *p);
      else if (auto const * p = dynamic_cast< QLineEdit const *>(&o))
        emit<OutputIterator, QLineEdit>(oi, *p);
      else if (auto const * p = dynamic_cast< QAbstractSpinBox const *>(&o))
        emit<OutputIterator, QAbstractSpinBox>(oi, *p);
      else if (auto const * p = dynamic_cast< QAbstractButton const *>(&o))
        emit<OutputIterator, QAbstractButton>(oi, *p);
      else if (auto const * p = dynamic_cast< QDialogButtonBox const *>(&o))
        emit<OutputIterator, QDialogButtonBox>(oi, *p);
      else if (auto const * p = dynamic_cast< QTreeWidget const *>(&o))
        emit<OutputIterator, QTreeWidget>(oi, *p);
      else if (auto const * p = dynamic_cast< QToolBar const *>(&o))
        emit<OutputIterator, QToolBar>(oi, *p);
      else if (auto const * p = dynamic_cast< QTableWidget const *>(&o))
        emit<OutputIterator, QTableWidget>(oi, *p);
      else if (auto const * p = dynamic_cast< QChartView const *>(&o))
        emit<OutputIterator, QChartView>(oi, *p);
      else if (auto const * p = dynamic_cast< QScrollArea const *>(&o))
        emit<OutputIterator, QScrollArea>(oi, *p);
      else if (
        auto const * p = dynamic_cast< QMessageBox const *>(
          &o)) // Subclass of QDialog, but so special it is handled separately!
        emit<OutputIterator, QMessageBox>(oi, *p);
#if 0
        else if (const auto* p = dynamic_cast<const QInputDialog*>(&o)) // Subclass of QDialog, but so special it is handled separately!
            emit<OutputIterator, QInputDialog>(oi, *p);
#endif
      else if (auto const * p = dynamic_cast< QDialog const *>(&o))
        emit<OutputIterator, QDialog>(oi, *p);
      else {
        // Type could be just a QWidget, or it could be some inherited type that is not
        // explicitely handled here. The top level widget will typically be just a QWidget,
        // it could be identified by not having a parent (o.parent()==nullptr), in which case
        // it logically is a window (but still just a QWidget).
        base_emit_widget<OutputIterator> base{oi, o, true};
      }
    }
  };

  //
  // Tree specifics
  //

  template <class OutputIterator>
  struct emit<OutputIterator, QTreeWidgetItem> {
    emit(OutputIterator& oi, QTreeWidgetItem const & o) {
      emit_object<OutputIterator> oo(oi); // Note: QTreeWidgetItem  does not inherited QObject (nor QWidget)
      oo.def("type", o.type()); // 1000 and above are values for custom types, values below are reserved by Qt but only
                                // 0 currently used.
      if (o.childIndicatorPolicy() != QTreeWidgetItem::DontShowIndicatorWhenChildless)
        oo.def("childIndicatorPolicy", o.childIndicatorPolicy());
      oo.def("columnCount", o.columnCount());
      // Typed content from data attribute role Qt::DisplayRole (and Qt::EditRole, as it is referring to the same data).
      // The text() method returns a stringification of the same value.
      def_optional_list_fx(
        oo,
        "value"s,
        o.columnCount(),
        false,
        [&o](int i) {
          return o.data(i, Qt::DisplayRole);
        },
        [/*&o*/](QVariant const & /*v*/, int /*i*/) {
          return true;
        }); // Always write it, even if empty?

      def_optional_list_fx(
        oo,
        "toolTip"s,
        o.columnCount(),
        false,
        [&o](int i) {
          return o.toolTip(i);
        },
        [](QString const & v, int /*i*/) {
          return !v.isEmpty();
        }); // Stringification of the same value as in data attribute role Qt::ToolTipRole.
      def_optional_list_fx(
        oo,
        "statusTip"s,
        o.columnCount(),
        false,
        [&o](int i) {
          return o.statusTip(i);
        },
        [](QString const & v, int /*i*/) {
          return !v.isEmpty();
        }); // Stringification of the same value as in data attribute role Qt::StatusTipRole.
      def_optional_list_fx(
        oo,
        "whatsThis"s,
        o.columnCount(),
        false,
        [&o](int i) {
          return o.whatsThis(i);
        },
        [](QString const & v, int /*i*/) {
          return !v.isEmpty();
        }); // Stringification of the same value as in data attribute role Qt::WhatsThisRole.
      def_item_flags(
        oo,
        o.flags(),
        Qt::ItemFlags{
          Qt::ItemIsSelectable,
          Qt::ItemIsDragEnabled,
          Qt::ItemIsDropEnabled,
          Qt::ItemIsUserCheckable,
          Qt::ItemIsEnabled});
      def_optional_list_fx(
        oo,
        "items"s,
        o.columnCount(),
        false,
        [&o](int i) {
          return o.child(i);
        },
        [](QTreeWidgetItem const * v, int /*i*/) {
          return v != nullptr;
        }); // Recurse children, which are also QTreeWidgetItem
    }
  };

  //
  // ToolBar specifics
  //

  template <class OutputIterator>
  struct emit<OutputIterator, QAction>
    : emit<OutputIterator, QObject> { // QAction does inherit QObject, but not QWidget
    using emit<OutputIterator, QObject>::oo;

    emit(OutputIterator& oi, QAction const & o)
      : emit<OutputIterator, QObject>{oi, o} {
      if (o.isSeparator()) {
        oo.def("separator", o.isSeparator());
      } else {
        if (!o.isEnabled())
          oo.def("enabled", o.isEnabled());
        auto const text = o.text();
        if (!text.isEmpty())
          oo.def(
            "text",
            text); // If no text is specified, the action's icon text will be returned (QAction specific behaviour)
        auto const iconText = o.iconText();
        if (!iconText.isEmpty() && iconText != text)
          oo.def("icon", iconText); // If no icon text is specified, the action's normal text will be returned (QAction
                                    // specific behaviour)
        auto const toolTip = o.toolTip();
        if (!toolTip.isEmpty() && toolTip != text)
          oo.def(
            "toolTip",
            toolTip); // If no tooltip is specified, the action's text will be returned (QAction specific behaviour)
        auto const statusTip = o.statusTip();
        if (!statusTip.isEmpty())
          oo.def("statusTip", statusTip);
        if (!o.whatsThis().isEmpty())
          oo.def("whatsThis", o.whatsThis());
        if (o.isCheckable())
          oo.def("checked", o.isChecked());
        auto const v = o.data();
        if (check_variant(v, true))
          oo.def("data", v); // Unlike the data on QTableWidgetItem and QTreeWidgetItem, this is only a single QVariant
                             // value, and is not linked to text or any other properties.
        if (auto const * m = o.menu())
          oo.def("menu", *m);
      }
    }
  };

  //
  // Table specifics
  //

  template <class OutputIterator>
  struct emit<OutputIterator, QTableWidgetItem> { // QTableWidgetItem does not inherit QObject (hence not QWidget).

    emit(OutputIterator& oi, QTableWidgetItem const & o) {
      emit_object<OutputIterator> oo(oi); // Note: QTableWidgetItem does not inherited QObject (nor QWidget)
      // oo.def("type", o.type()); // Integer value passed to the QTableWidgetItem constructor. Value 1000 and above are
      // values for custom types, values below are reserved by Qt but only 0 currently used.
      if (!o.toolTip().isEmpty())
        oo.def("toolTip", o.toolTip()); // Stringification of the same value as in data attribute role Qt::ToolTipRole.
      if (!o.statusTip().isEmpty())
        oo.def(
          "statusTip", o.statusTip()); // Stringification of the same value as in data attribute role Qt::StatusTipRole.
      if (!o.whatsThis().isEmpty())
        oo.def(
          "whatsThis", o.whatsThis()); // Stringification of the same value as in data attribute role Qt::WhatsThisRole.
      def_item_flags(
        oo,
        o.flags(),
        Qt::ItemFlags{
          Qt::ItemIsSelectable,
          Qt::ItemIsEditable,
          Qt::ItemIsDragEnabled,
          Qt::ItemIsDropEnabled,
          Qt::ItemIsUserCheckable,
          Qt::ItemIsEnabled});
      auto v = o.data(Qt::ItemDataRole::DisplayRole);
      if (check_variant(v, true))
        oo.def(
          "value", v); // Typed content from data attribute role Qt::DisplayRole (and Qt::EditRole, as it is referring
                       // to the same data). The text() method returns a stringification of the same value.
      def_item_data_properties(
        oo,
        o,
        {// Emit typed content from data attribute user roles, as "properties" object to emulate the export of
         // QObject::property().
         ItemDataProperty::DataX,
         ItemDataProperty::DataY,
         ItemDataProperty::Decimals,
         ItemDataProperty::ScaleX,
         ItemDataProperty::ScaleY,
         ItemDataProperty::ValidationX,
         ItemDataProperty::ValidationY,
         ItemDataProperty::Tags});
    }
  };

  //
  // Chart specifics
  //

  template <class OutputIterator>
  struct base_emit_axis : emit<OutputIterator, QObject> { // QAbstractAxis does inherit QObject, but not QWidget
    using emit<OutputIterator, QObject>::oo;

    base_emit_axis(OutputIterator& oi, QAbstractAxis const & o)
      : emit<OutputIterator, QObject>{oi, o} {
      oo.def("type", o.type()).def("orientation", o.orientation());

      // Although alignment enum contains horizontal and vertical flags that can be combined,
      // for chart axis it is limited by the orientation attribute to just one: If orientation
      // is horizontal, alignment must be vertical, and opposite. Possible values are also limited,
      // either top or bottom for vertical alignment and either left or right for horizontal alignment.
      // Attempting to set anything not legal will fail ("No alignment specified !"), so we can just
      // emit whatever is found without further verification.
      if (o.alignment())
        def_alignment(oo, o.alignment());
      // if (o.alignment()) oo.def("alignment", o.alignment());
      // if (o.alignment()) def_alignment_for_orientation(oo, "alignment3",
      // static_cast<Qt::Orientation>(static_cast<Qt::Orientations::Int>(~Qt::Orientations(o.orientation()))),
      // o.alignment());

      if (!o.isVisible())
        oo.def("visible", o.isVisible());
      // if (!o.isTitleVisible()) oo.def("isTitleVisible", o.isTitleVisible());
      if (!o.titleText().isEmpty())
        oo.def("title", o.titleText());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QValueAxis> : base_emit_axis<OutputIterator> {
    using base_emit_axis<OutputIterator>::oo;

    emit(OutputIterator& oi, QValueAxis const & o)
      : base_emit_axis<OutputIterator>{oi, o} {
      oo.def("min", o.min()).def("max", o.max());
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)) // TickType (Fixed|Dynamic) introduced in Qt 5.12
      if (
        o.tickType() != QValueAxis::TicksFixed
        || o.tickCount() != 5) { // Only emit if different from Qt default of fixed 5 ticks
        oo.def("tickType", o.tickType());
        if (o.tickType() == QValueAxis::TicksDynamic) {
          oo.def("tickAnchor", o.tickAnchor())      // Qt default: 0.0
            .def("tickInterval", o.tickInterval()); // Qt default: 0.0
        } else {
          oo.def("tickCount", o.tickCount()); // Qt default: 5
        }
      }
#else
      if (o.tickCount() != 5)
        oo.def("tickCount", o.tickCount()); // Qt default: 5. Only emit if different
#endif
      if (o.minorTickCount() > 0)
        oo.def("minorTickCount", o.minorTickCount());
      if (!o.labelFormat().isEmpty())
        oo.def("format", o.labelFormat());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QDateTimeAxis> : base_emit_axis<OutputIterator> {
    using base_emit_axis<OutputIterator>::oo;

    emit(OutputIterator& oi, QDateTimeAxis const & o)
      : base_emit_axis<OutputIterator>{oi, o} {
      oo.def("min", o.min()).def("max", o.max()).def("tickCount", o.tickCount());
      if (!o.format().isEmpty())
        oo.def("format", o.format()); // Qt default: "dd-MM-yyyy\nh:mm" (but maybe locale dependent?)
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QBarCategoryAxis> : base_emit_axis<OutputIterator> {
    using base_emit_axis<OutputIterator>::oo;

    emit(OutputIterator& oi, QBarCategoryAxis const & o)
      : base_emit_axis<OutputIterator>{oi, o} {
      if (!o.min().isEmpty())
        oo.def("min", o.min());
      if (!o.max().isEmpty())
        oo.def("max", o.max());
      oo.def(
        "categories",
        (QList<QString>) const_cast<QBarCategoryAxis&>(o)
          .categories()); // Note: Const cast because categories() is not const
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractAxis> {
    emit(OutputIterator& oi, QAbstractAxis const & o) {
      if (auto const * p = dynamic_cast< QValueAxis const *>(&o))
        emit<OutputIterator, QValueAxis>(oi, *p);
      else if (auto const * p = dynamic_cast< QDateTimeAxis const *>(&o))
        emit<OutputIterator, QDateTimeAxis>(oi, *p);
      else if (auto const * p = dynamic_cast< QBarCategoryAxis const *>(&o))
        emit<OutputIterator, QBarCategoryAxis>(oi, *p);
      else
        base_emit_axis<OutputIterator>(oi, o);
    }
  };

  template <class OutputIterator>
  struct base_emit_series : emit<OutputIterator, QObject> { // QAbstractSeries does inherit QObject, but not QWidget
    using emit<OutputIterator, QObject>::oo;

    base_emit_series(OutputIterator& oi, QAbstractSeries const & o)
      : emit<OutputIterator, QObject>{oi, o} {
      if (auto const * chart = o.chart()) { // Should always be true, but need the chart ptr anyway..
        oo.def("type", o.type());
        if (!o.name().isEmpty())
          oo.def("name", o.name());
        if (!o.isVisible())
          oo.def("visible", o.isVisible());
        auto const axes = chart->axes();
        oo.def_fx("attachedAxes", [&o, &axes](OutputIterator& oi) -> void {
          emit_vector_fx(
            oi,
            const_cast<QAbstractSeries&>(o).attachedAxes(), // Note: Const cast because attachedAxes() is not const
            [&axes](OutputIterator& oi, /*const*/ auto* p) -> void {
              auto ix = axes.indexOf(p);
              emit<OutputIterator, int>(oi, ix); // Note: Referencing axis by the index it has in the chart
            });
        });
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QXYSeries> : base_emit_series<OutputIterator> {
    // QXYSeries is base class of QLineSeries, with subclass QSplineSeries, and QScatterSeries.
    using base_emit_series<OutputIterator>::oo;

    emit(OutputIterator& oi, QXYSeries const & o)
      : base_emit_series<OutputIterator>{oi, o} {
      if (o.pen().style() != Qt::NoPen)
        oo.def("pen", o.pen()); // Pen for lines and outlines of shapes (with stroke brush)
      if (o.brush().style() != Qt::NoBrush && o.type() == QAbstractSeries::SeriesTypeScatter)
        oo.def("brush", o.brush()); // Fill brush for pattern of shapes - only relevant for QScatterSeries, QLineSeries
                                    // (including its subclass QSplineSeries) only use the pen
      if (o.count() > 0)
        oo.def("points", o.pointsVector());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractBarSeries> : base_emit_series<OutputIterator> {
    using base_emit_series<OutputIterator>::oo;

    emit(OutputIterator& oi, QAbstractBarSeries const & o)
      : base_emit_series<OutputIterator>{oi, o} {
      if (o.count() > 0)
        oo.def("barSets", o.barSets());
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QAbstractSeries> {
    emit(OutputIterator& oi, QAbstractSeries const & o) {
      if (auto const * p = dynamic_cast< QXYSeries const *>(&o))
        emit<OutputIterator, QXYSeries>(oi, *p);
      else if (auto const * p = dynamic_cast< QAbstractBarSeries const *>(&o))
        emit<OutputIterator, QAbstractBarSeries>(oi, *p);
      else
        base_emit_series<OutputIterator>(oi, o);
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QBarSet>
    : emit<OutputIterator, QObject> { // QBarSet does inherit QObject, but not QWidget
    using emit<OutputIterator, QObject>::oo;

    emit(OutputIterator& oi, QBarSet const & o)
      : emit<OutputIterator, QObject>{oi, o} {
      if (!o.label().isEmpty())
        oo.def("label", o.label());
      if (o.pen().style() != Qt::NoPen)
        oo.def("pen", o.pen()); // Pen for lines and outlines of shapes (with stroke brush)
      if (o.brush().style() != Qt::NoBrush)
        oo.def("brush", o.brush()); // Fill brush for pattern of shapes
      oo.def_fx("values", [&o](OutputIterator& oi) -> void {
        emit_list_fx(oi, o.count(), [&o](int i) {
          return o[i];
        });
      });
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::PenStyle> {
    emit(OutputIterator& oi, Qt::PenStyle const & o) {
      switch (o) {
      case Qt::NoPen:
        emit<OutputIterator, string>(oi, "none"s);
        break;
      case Qt::SolidLine:
        emit<OutputIterator, string>(oi, "solid"s);
        break;
      case Qt::DashLine:
        emit<OutputIterator, string>(oi, "dash"s);
        break;
      case Qt::DotLine:
        emit<OutputIterator, string>(oi, "dot"s);
        break;
      case Qt::DashDotLine:
        emit<OutputIterator, string>(oi, "dashDot"s);
        break;
      case Qt::DashDotDotLine:
        emit<OutputIterator, string>(oi, "dashDotLine"s);
        break;
      case Qt::CustomDashLine:
        emit<OutputIterator, string>(oi, "custom"s);
        break;
      default:
        break; // Enum also contains MPenStyle (bit mask for joining PenStyle, PenCapStyle and PenJoinStyle into a
               // single value)
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::PenCapStyle> {
    emit(OutputIterator& oi, Qt::PenCapStyle const & o) {
      switch (o) {
      case Qt::FlatCap:
        emit<OutputIterator, string>(oi, "flat"s);
        break;
      case Qt::SquareCap:
        emit<OutputIterator, string>(oi, "square"s);
        break;
      case Qt::RoundCap:
        emit<OutputIterator, string>(oi, "round"s);
        break;
      default:
        break; // Enum also contains MPenCapStyle (bit mask for joining PenStyle, PenCapStyle and PenJoinStyle into a
               // single value)
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::PenJoinStyle> {
    emit(OutputIterator& oi, Qt::PenJoinStyle const & o) {
      switch (o) {
      case Qt::MiterJoin:
        emit<OutputIterator, string>(oi, "miter"s);
        break;
      case Qt::BevelJoin:
        emit<OutputIterator, string>(oi, "bevel"s);
        break;
      case Qt::RoundJoin:
        emit<OutputIterator, string>(oi, "round"s);
        break;
      case Qt::SvgMiterJoin:
        emit<OutputIterator, string>(oi, "svgMiter"s);
        break;
      default:
        break; // Enum also contains MPenJoinStyle (bit mask for joining PenStyle, PenCapStyle and PenJoinStyle into a
               // single value)
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, Qt::BrushStyle> {
    emit(OutputIterator& oi, Qt::BrushStyle const & o) {
      switch (o) {
      case Qt::NoBrush:
        emit<OutputIterator, string>(oi, "none"s);
        break;
      case Qt::SolidPattern:
        emit<OutputIterator, string>(oi, "solid"s);
        break;
      case Qt::Dense1Pattern:
        emit<OutputIterator, string>(oi, "dense1"s);
        break;
      case Qt::Dense2Pattern:
        emit<OutputIterator, string>(oi, "dense2"s);
        break;
      case Qt::Dense3Pattern:
        emit<OutputIterator, string>(oi, "dense3"s);
        break;
      case Qt::Dense4Pattern:
        emit<OutputIterator, string>(oi, "dense4"s);
        break;
      case Qt::Dense5Pattern:
        emit<OutputIterator, string>(oi, "dense5"s);
        break;
      case Qt::Dense6Pattern:
        emit<OutputIterator, string>(oi, "dense6"s);
        break;
      case Qt::Dense7Pattern:
        emit<OutputIterator, string>(oi, "dense7"s);
        break;
      case Qt::HorPattern:
        emit<OutputIterator, string>(oi, "horizontalLines"s);
        break;
      case Qt::VerPattern:
        emit<OutputIterator, string>(oi, "verticalLines"s);
        break;
      case Qt::CrossPattern:
        emit<OutputIterator, string>(oi, "crossingLines"s);
        break;
      case Qt::BDiagPattern:
        emit<OutputIterator, string>(oi, "backwardDiagonalLines"s);
        break;
      case Qt::FDiagPattern:
        emit<OutputIterator, string>(oi, "forwardDiagonalLines"s);
        break;
      case Qt::DiagCrossPattern:
        emit<OutputIterator, string>(oi, "diagonalCrossingLines"s);
        break;
      case Qt::LinearGradientPattern:
        emit<OutputIterator, string>(oi, "linearGradient"s);
        break;
      case Qt::ConicalGradientPattern:
        emit<OutputIterator, string>(oi, "conicalGradient"s);
        break;
      case Qt::RadialGradientPattern:
        emit<OutputIterator, string>(oi, "radialGradient"s);
        break;
      case Qt::TexturePattern:
        emit<OutputIterator, string>(oi, "texture"s);
        break;
      }
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, QColor> {
    emit(OutputIterator& oi, QColor const & o) {
      // Emit the color in the string format typically supported in web/css:
      // Hexadecimal RGB format "#RRGGBB", or RGBA format (including transparency) "#RRGGBBAA",
      // Qt can return the color as hex string in RGB format "#RRGGBB", but when including
      // transparency it uses ARGB "#AARRGGBB", which means we use our own method to place
      // the alpha value to the end of the string.
      if (o.isValid()) {
        if (o.alpha() == 255) {
          emit<OutputIterator, QString>(
            oi, o.name(QColor::HexRgb)); // Hexadecimal RGB color code (without transparency): "#RRGGBB"
        } else {
          emit<OutputIterator, QString>(
            oi,
            o.name(QColor::HexRgb)
              + QString("%1").arg(
                o.alpha(), 2, 16, QLatin1Char('0'))); // Hexadecimal RGBA color code (with transparency): "#RRGGBBAA"
        }
      } else { // Skip invalid color (default constructed, RGB range out of bounds, etc)
        emit<OutputIterator, char*>(oi, "");
      }
    }
  };

  //
  // Pen
  //
  // Pen is used to specify lines and outlines of shapes.
  // The basic default is a solid black brush with 1 width, square cap style (Qt::SquareCap),
  // and bevel join style (Qt::BevelJoin).
  // The default pens for XY series and bar sets are different and theme-dependent, mostly color
  // and width, with color assigned when added to chart.
  //
  template <class OutputIterator>
  struct emit<OutputIterator, QPen> {
    emit_object<OutputIterator> oo;

    emit(OutputIterator& oi, QPen const & o)
      : oo{oi} {
      // Emit style type unconditionally:
      // - Don't want to emit just an empty object (when everything is default or null)
      // - Don't want to check against a default value, because it often depends on parent and theme
      //   and is therefore hard to find, and also because we may want to always emit the actual
      //   style used by Qt (even if default).
      // - Don't want to check on null value (Qt::NoPen), because would then have to consider this
      //   being the default and in that case emit it anyway if explicit set.
      oo.def("style", o.style());
      if (o.style() != Qt::NoPen) {
        // When a valid pen is set, emit its stroke brush unconditionally for the same reasons as style
        // (especially to always emit the actual colors used by Qt, even if default).
        // Note that if the stroke brush is NoBrush the pen will effectively be invisible!
        oo.def("brush", o.brush()); // Stroke brush
        // Emit join style, cap style and width conditionally: If different than the basic default,
        // since this seems to actually be the effective defaults in most cases.
        static QPen defaultPen;
        if (o.joinStyle() != defaultPen.joinStyle() /*Qt::BevelJoin*/)
          oo.def("joinStyle", o.joinStyle());
        if (o.capStyle() != defaultPen.capStyle() /*Qt::SquareCap*/)
          oo.def("capStyle", o.capStyle());
        if (o.width() != defaultPen.width() /*1*/)
          oo.def("width", o.width());
      }
    }
  };

  //
  // Brush
  //
  // Brush is used to specify the fill pattern of a shape (fill brush), or the
  // fill strokes of lines and outlines of shapes (stroke brush, see QPen emitter).
  //
  // The basic default for fill brush is to not use it (style Qt::NoBrush),
  // for stroke brush (on pen) it is black Qt:SolidPattern.
  // The default fill brushes for XY series and bar sets are different and theme-dependent.
  //
  template <class OutputIterator>
  struct emit<OutputIterator, QBrush> {
    emit_object<OutputIterator> oo;

    emit(OutputIterator& oi, QBrush const & o)
      : oo{oi} {
      // Style type is emitted unconditionally, for same reasons as for QPen.
      oo.def("style", o.style());
      switch (o.style()) {
      case Qt::NoBrush:
        // No brush
        break;
      case Qt::LinearGradientPattern:
      case Qt::RadialGradientPattern:
      case Qt::ConicalGradientPattern:
        // Gradient based brush.
        // TODO: Emit QGradient object returned by o.gradient().
        break;
      case Qt::TexturePattern:
        // Texture (image) based brush.
        // TODO: Emit QPixmap object returned from o.texture() and/or QImage from o.textureImage().
        break;
      default:
        // Color based brush (Qt::SolidPattern, or one of several structure patterns).
        // When a valid brush is set, emit its any valid color property value unconditionally,
        // for similar reasons as style, and especially because we may want to always emit the
        // actual colors used by Qt (even if default), e.g. auto assigned colors of chart series,
        // which depends on the configured theme.
        if (o.color().isValid()) {
          oo.def("color", o.color());
        }
      }
    }
  };

}
