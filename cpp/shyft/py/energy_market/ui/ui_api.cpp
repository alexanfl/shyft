#include <shyft/version.h>

#include <cstdint>
#include <string>
#include <iterator>
#include <shyft/py/api/bindings.h>
#include <shyft/py/energy_market/ui/ui_generators.h>
// note: q needs to be after py bindings, due to name sym clash on 'slot'
#include <QObject>
#include <QWidget>
#include <QVersionNumber>

namespace shyft::web_api::generator {
  using output_iterator = std::back_insert_iterator<std::string>;
}

namespace expose { // Expose Python api for generating ui json configuration from Qt Widgets

  using std::size_t;
  using std::intptr_t;
  using std::string;
  using std::to_string;
  using namespace std::string_literals;
  using std::runtime_error;

  // Qt wrap cast from opaque handle to 'known' pointer.

  py::object export_qt(intptr_t const &handle) { // Caller should supply shiboken2.getCppPointer(widget)[0]
    auto const *o = reinterpret_cast<QObject *>(handle);
    if (auto const *p = dynamic_cast< QWidget const *>(o)) {
      std::string s;
      shyft::web_api::generator::output_iterator oi(s);
      shyft::web_api::generator::emit<decltype(oi), QWidget>(oi, *p);
      return py::object(s);
    }
    return py::object("ERROR: Not a widget, need a top-level widget to start with");
  }

  void ui_cfg() {
    expose::py::scope().attr("qt_version") = string{QT_VERSION_STR};

    py::enum_<shyft::web_api::generator::ItemDataProperty>("ItemDataProperty")
      .value("DataX", shyft::web_api::generator::ItemDataProperty::DataX)
      .value("DataY", shyft::web_api::generator::ItemDataProperty::DataY)
      .value("Decimals", shyft::web_api::generator::ItemDataProperty::Decimals)
      .value("ScaleX", shyft::web_api::generator::ItemDataProperty::ScaleX)
      .value("ScaleY", shyft::web_api::generator::ItemDataProperty::ScaleY)
      .value("ValidationX", shyft::web_api::generator::ItemDataProperty::ValidationX)
      .value("ValidationY", shyft::web_api::generator::ItemDataProperty::ValidationY)
      .value("Tags", shyft::web_api::generator::ItemDataProperty::Tags)
      .export_values();

    py::def(
      "export",
      &export_qt,
      (py::arg("window")),
      doc.intro("Export the window/QWidget layout to a json suitable for the front end framework.")
        .intro("Note that the routine harvest the essential properties recursively starting at the top level.")
        .intro("Ref. to the shyft/python/test_suites/energy_market/ui directory for examples.")
        .parameters()
        .parameter("window", "QWidgetHandle", "A Qt Widget handle, containing the layout and components")
        .returns("json string", "str", "A json string suitable for the front end.")());
  }

  extern void ex_client_server();
}

BOOST_PYTHON_MODULE(_ui) {
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::py::scope().attr("__doc__") = "Shyft Energy Market user interface configuration api";
  expose::py::scope().attr("__version__") = shyft::_version_string();
  expose::py::scope().attr("shyft_with_stm") = true;
  expose::ui_cfg();
  expose::ex_client_server();
}
