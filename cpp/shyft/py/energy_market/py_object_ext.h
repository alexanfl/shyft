#pragma once

#include <shyft/py/api/bindings.h>

namespace expose {


  /** py_object_ext is for extending c++ classes the model with a python object
   *
   * @details
   * The base object of the energy-market model contains an optional 'em-handle'
   * ref to the 'id_base' object.
   * This is the interface that lets the user assign/read this
   * property.
   *
   * @tparam T like reservoir, unit etc.
   *
   */
  template <class T>
  struct py_object_ext {
    static py::object get_obj(T const & o) {
      auto pyo = static_cast<py::object*>(o.h.obj);
      if (pyo) {
        return *pyo;
      } else {
        return py::object{};
      }
    }

    static void set_obj(T& o, py::object& h) {
      auto pyo = static_cast<py::object*>(o.h.obj);
      if (pyo) {
        *pyo = h;
      } else {
        pyo = new py::object(h);
        o.h.obj = static_cast<void*>(pyo);
      }
    }
  };

}
