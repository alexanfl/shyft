
add_library(ltm SHARED
    ltm_api.cpp
)
target_include_directories(ltm PRIVATE ${python_include} ${python_numpy_include})
target_link_libraries(
    ltm
    PRIVATE shyft_private_flags em_model_core shyft_py_api ${boost_py_link_libraries} ${python_lib}
    PUBLIC shyft_public_flags
)

set_target_properties(ltm PROPERTIES
    OUTPUT_NAME ltm
    VISIBILITY_INLINES_HIDDEN TRUE
    CXX_VISIBILITY_PRESET hidden
    PREFIX "_" # Python extensions do not use the 'lib' prefix
    SUFFIX ${py_ext_suffix}
    INSTALL_RPATH "$ORIGIN/../../lib"
)

install(TARGETS ltm ${shyft_runtime} DESTINATION ${SHYFT_PYTHON_DIR}/shyft/energy_market/ltm)
