#include <shyft/version.h>

#include <shyft/py/api/bindings.h>

namespace expose {

  void ltm() {
  }

}

BOOST_PYTHON_MODULE(_ltm) {
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::py::scope().attr("__doc__") = "Shyft Open Source Energy Market long term models";
  expose::py::scope().attr("__version__") = shyft::_version_string();
  expose::ltm();
}
