#include <mutex>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <stdexcept>

#include <boost/thread/locks.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/server_logger.h>
#include <shyft/srv/db.h>
#include <shyft/web_api/energy_market/request_handler.h>

#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_optional.h>
#include <shyft/py/api/reflection.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/scoped_gil.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::pyapi::scoped_gil_release;
  using shyft::pyapi::scoped_gil_aquire;

  struct py_client {
    std::mutex mx; ///< to enforce just one thread active on this client object at a time
    client impl;

    py_client(std::string const &host_port, int timeout_ms)
      : impl{host_port, timeout_ms} {
    }

    ~py_client() {
    }

    std::string get_host_port() {
      return impl.c.host_port;
    }

    int get_timeout_ms() {
      return impl.c.timeout_ms;
    }

    bool is_open() const {
      return impl.c.is_open;
    }

    size_t get_reconnect_count() const {
      return impl.c.reconnect_count;
    }

    void close() {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.close();
    }

    std::string get_server_version() {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_version_info();
    }

    bool create_model(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.create_model(mid);
    }

    bool add_model(std::string const &mid, stm_system_ mdl) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.add_model(mid, mdl);
    }

    bool remove_model(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.remove_model(mid);
    }

    bool clone_model(std::string const &old_mid, std::string const &new_mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.clone_model(old_mid, new_mid);
    }

    bool rename_model(std::string const &old_mid, std::string const &new_mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.rename_model(old_mid, new_mid);
    }

    std::vector<std::string> get_model_ids() {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_model_ids();
    }

    auto get_model_infos() {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_model_infos();
    }

    stm_system_ get_model(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_model(mid);
    }

    bool optimize(
      std::string const &mid,
      generic_dt const &ta,
      std::vector<shop::shop_command> const &cmd,
      bool opt_only) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.optimize(mid, ta, cmd, opt_only);
    }

    bool start_tune(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock<std::mutex> lck(mx);
      return impl.start_tune(mid);
    }

    bool tune(std::string const &mid, generic_dt const &ta, std::vector<shop::shop_command> const &cmd) {
      scoped_gil_release gil;
      std::unique_lock<std::mutex> lck(mx);
      return impl.tune(mid, ta, cmd);
    }

    bool stop_tune(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock<std::mutex> lck(mx);
      return impl.stop_tune(mid);
    }

    std::vector<log_entry> get_log(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_log(mid);
    }

    model_state get_state(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_state(mid);
    }

    auto evaluate_ts(
      time_series::dd::ats_vector const &tsv,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {
      auto result = [&] {
        scoped_gil_release gil;
        std::unique_lock lck(mx);
        return impl.evaluate_ts(tsv, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
      }();
      py::list py_result;
      for (auto const &attr : result) {
        py_result.append(attr);
      }
      return py_result;
    }

    bool evaluate_model(
      std::string const &mid,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.evaluate_model(mid, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
    }

    bool fx(std::string const &mid, std::string const &fx_arg) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.fx(mid, fx_arg);
    }

    ats_vector get_ts(std::string const &mid, std::vector<std::string> const &ts_urls) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_ts(mid, ts_urls);
    }

    void set_ts(std::string const &mid, ats_vector const &tsv) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.set_ts(mid, tsv);
    }

    auto add_compute_server(std::string const &host_port) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.add_compute_server(host_port);
    }

    optimization_summary_ get_optimization_summary(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_optimization_summary(mid);
    }

    bool kill_optimization(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.kill_optimization(mid);
    }

    // NOTE: just do manual conversions for now - jeh
    py::object set_attrs(py::list const &py_attrs) {
      std::vector<std::pair<std::string, any_attr>> attrs;
      for (auto i : std::views::iota(0l, py::len(py_attrs))) {
        py::extract<std::pair<std::string, any_attr>> py_attr_extract(py_attrs[i]);
        if (!py_attr_extract.check())
          throw std::runtime_error(fmt::format(
            "Invalid conversion from python object to pair of string and dstm attribute on {}'th element.", i));
        attrs.push_back(py_attr_extract());
      }
      auto result = [&] {
        scoped_gil_release gil;
        std::unique_lock lck(mx);
        return impl.set_attrs(attrs);
      }();
      py::list py_result;
      for (auto const &optErr : result) {
        if (optErr) {
          py_result.append(*optErr);
        } else {
          py_result.append(py::object());
        }
      }
      return py_result;
    }

    auto get_attrs(std::vector<std::string> const &attrs) {
      auto result = [&] {
        scoped_gil_release gil;
        std::unique_lock lck(mx);
        return impl.get_attrs(attrs);
      }();
      py::list py_result;
      for (auto const &attr : result) {
        py_result.append(attr);
      }
      return py_result;
    }

    bool reset_model(std::string const &mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.reset_model(mid);
    }

    bool patch(std::string const &mid, stm::stm_patch_op op, stm::stm_system_ const &p) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.patch(mid, op, p);
    }

    auto compute_server_status() {
      auto result = [&] {
        scoped_gil_release gil;
        std::unique_lock lck(mx);
        return impl.compute_server_status();
      }();
      py::list pyresult{};
      for (auto const &r : result)
        pyresult.append(py::object(r));
      return pyresult;
    }
  };

  /** @brief  Server implementation
   *
   *  TODO: consider use server as impl instead of inheritance, and use scoped_gil for all python exposed calls
   */
  struct py_server : server {
    using request_handler = shyft::web_api::energy_market::request_handler;

    request_handler bg_server; ///< this object handle the requests from the web-api
    std::future<int> web_srv;  ///< mutex
    py::object py_fx_cb;       ///< python callback that can be set by the py user

    std::string web_api_ip; ///< Listening IP for web_api
    int web_api_port;       ////< Listening port for web API

    void py_do_notify_change(std::vector<std::string> const &urls) {
      scoped_gil_release gil;
      if (urls.size() == 0)
        return;
      sm->notify_change(urls);
    }

    /** this is where we attempt to fire user specified callback, */
    bool py_do_fx(std::string const &mid, std::string const &fx_arg) {
      bool r{false};
      if (py_fx_cb.ptr() != Py_None) {
        scoped_gil_aquire gil; // we need to use the GIL here before trying to call python.
        try {
          r = py::call<bool>(py_fx_cb.ptr(), mid, fx_arg);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      }
      return r;
    }

    py_server(compute::janitor_config j_config = {})
      : server(j_config)
      , bg_server{models, sm, fx_cb, dtss.get()} {
      if (!PyEval_ThreadsInitialized()) {
        PyEval_InitThreads(); // ensure threads is enabled
      }
      // rig the c++ fx_cb to forward calls to this python-layer
      fx_cb = [&](std::string mid, std::string fx_args) -> bool {
        return this->py_do_fx(mid, fx_args);
      };
    }

    ~py_server() {
      if (web_srv.valid())
        web_srv.wait();
    }

    void
      start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads, bool tls_only) {
      scoped_gil_release gil;
      if (!web_srv.valid()) {
        web_api_port = port;
        web_api_ip = host_ip;
        web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads, tls_only]() {
          return shyft::web_api::run_web_server(
            bg_server,
            host_ip,
            static_cast<unsigned short>(port),
            std::make_shared<std::string>(doc_root),
            fg_threads,
            bg_threads,
            tls_only // accept_plain == !tls_only
          );
        });
      }
    }

    void stop_web_api() {
      scoped_gil_release gil;
      if (web_srv.valid()) {
        std::raise(SIGINT);
        (void) web_srv.get();
      }
    }

    int get_web_api_port() {
      if (web_srv.valid()) {
        return web_api_port;
      } else {
        return -1;
      }
    }

    std::string get_web_api_ip() {
      if (web_srv.valid()) {
        return web_api_ip;
      } else {
        return "";
      }
    }

    auto py_optimize(
      std::string const &mid,
      generic_dt const &ta,
      std::vector<shop::shop_command> const &cmd,
      bool opt_only) {
      scoped_gil_release gil;
      return do_optimize(mid, ta, cmd, opt_only);
    }

    auto py_evaluate_model(
      std::string const &mid,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {
      scoped_gil_release gil;
      return do_evaluate_model(mid, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
    }

    auto py_clone_model(std::string const &old_mid, std::string new_mid) {
      scoped_gil_release gil;
      return do_clone_model(old_mid, new_mid);
    }

    auto py_remove_model(std::string const &mid) {
      scoped_gil_release gil;
      return do_remove_model(mid);
    }

    auto py_rename_model(std::string old_mid, std::string new_mid) {
      scoped_gil_release gil;
      return do_rename_model(old_mid, new_mid);
    }

    auto py_get_state(std::string const &mid) {
      scoped_gil_release gil;
      return do_get_state(mid);
    }

    auto py_get_model_ids() {
      scoped_gil_release gil;
      return do_get_model_ids();
    }

    auto py_get_model_infos() {
      scoped_gil_release gil;
      return do_get_model_infos();
    }

    auto py_add_model(std::string const &mid, stm_system_ mdl) {
      scoped_gil_release gil;
      return do_add_model(mid, mdl);
    }

    auto py_create_model(std::string const &mid) {
      scoped_gil_release gil;
      return do_create_model(mid);
    }

    auto add_compute_server(std::string const &mid) {
      scoped_gil_release gil;
      return do_add_compute_server(mid);
    }

    void stop_server(int ms) {
      scoped_gil_release gil;
      if (ms > 0)
        server::set_graceful_close_timeout(ms);
      server::clear();
    }

    py::object py_apply(std::string mid, py::object action) {
      scoped_gil_release gil;
      return models
        .mutate_or_throw(
          mid,
          [&](auto &&view) {
            scoped_gil_aquire gil;
            try {
              return py::call<py::object>(action.ptr(), view.model);
            } catch (const py::error_already_set &) {
              handle_pyerror();
              return py::object();
            }
          })
        .get();
    }

    // provide fx to dtss internals to inspect/control cache size
    dtss::cache_stats get_cache_stats() const {
      scoped_gil_release gil;
      return dtss->get_cache_stats();
    }

    void clear_cache_stats() {
      scoped_gil_release gil;
      dtss->clear_cache_stats();
    }

    std::int64_t get_cache_size() const {
      scoped_gil_release gil;
      return dtss->get_cache_size();
    }

    void set_cache_size(std::int64_t v) {
      scoped_gil_release gil;
      return dtss->set_cache_size(v);
    }

    std::int64_t get_ts_size() const {
      scoped_gil_release gil;
      return dtss->get_ts_size();
    }

    void set_ts_size(std::int64_t v) {
      scoped_gil_release gil;
      return dtss->set_ts_size(v);
    }

    std::int64_t get_cache_memory_target_size() const {
      scoped_gil_release gil;
      return dtss->get_cache_memory_target_size();
    }

    void set_cache_memory_target_size(std::int64_t v) {
      scoped_gil_release gil;
      return dtss->set_cache_memory_target_size(v);
    }

    void remove_from_cache(std::vector<std::string> v) {
      scoped_gil_release gil;
      return dtss->remove_from_cache(v);
    }

    void flush_cache_all() {
      scoped_gil_release gil;
      return dtss->flush_cache();
    }
  };

  void pyexport() {

    auto stm_log_levels =
      py::class_<dlib::log_level, boost::noncopyable>(
        "LogLevel", doc.intro("Logging level for STM logging utility.")(), py::no_init)
        .def(py::init<int, char const *>((py::arg("priority"), py::arg("name"))))
        .def_readonly("priority", &dlib::log_level::priority)
        .add_property(
          "name", +[](dlib::log_level &ll) -> string {
            return string(ll.name);
          });

    // DLIB LOGGING LEVELS:
    // The naive approach of setting py::scope().attr("LALL") = dlib::LALL;
    // yields a SystemError with unexpected Exception on the python side. (Possibly due to my misunderstanding of the
    // lifetime of globals) Work around for now is to hard code log levels from
    // http://dlib.net/dlib/logger/logger_kernel_abstract.h.html
    py::scope().attr("LALL") = stm_log_levels(std::numeric_limits<int>::min(), "ALL");
    py::scope().attr("LNONE") = stm_log_levels(std::numeric_limits<int>::max(), "NONE");
    py::scope().attr("LTRACE") = stm_log_levels(-100, "TRACE");
    py::scope().attr("LDEBUG") = stm_log_levels(0, "DEBUG");
    py::scope().attr("LINFO") = stm_log_levels(100, "INFO");
    py::scope().attr("LWARN") = stm_log_levels(200, "WARN");
    py::scope().attr("LERROR") = stm_log_levels(300, "ERROR");
    py::scope().attr("LFATAL") = stm_log_levels(400, "FATAL");

    py::class_<server_log_hook, boost::noncopyable>(
      "LogConfig", doc.intro("Class for controlling logging configuration")())
      .def(py::init<string>((py::arg("self"), py::arg("file"))));


    py::def(
      "configure_logger",
      &configure_logger,
      (py::arg("config"), py::arg("log_level")),
      doc.intro("Configure logger with log level and what file to write to")
        .parameters()
        .parameter("config", "LogConfig", "Configuration for logger")
        .parameter(
          "log_level",
          "LogLevel",
          "What log level is the lowest to report. ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL")
        .returns("nothing", "None", "")());

    {
      auto t_model_state = py::enum_<model_state>(
        "ModelState", doc.intro("Describes the possible state of the STM model")());
      shyft::pyapi::expose_enumerators(t_model_state);
    }
    {
      auto t_patch_op = py::enum_<stm_patch_op>(
        "StmPatchOperation", doc.intro("Describes how a patch should be applied to the STM model")());
      shyft::pyapi::expose_enumerators(t_patch_op);
    }
    py::class_<std::map<std::string, model_info, std::less<>>>(
      "ModelInfoDict",
      doc.intro("A map from model keys to ModelInfo, containing skeleton information about a model.")(),
      py::init<>((py::arg("self"))))
      .def(py::map_indexing_suite<std::map<std::string, model_info, std::less<>>>());

    auto py_janitor_config = py::class_<compute::janitor_config>("JanitorConfig", doc.intro("A janitor config")());
    shyft::pyapi::expose_members(py_janitor_config);

    auto py_dstm =
      py::class_<py_server, boost::noncopyable>(
        "DStmServer",
        doc.intro("A server object serving distributed, 'live' STM systems.\n"
                  "The server contains an DTSS that handles time series for the models stored in the server.")())
        .def(py::init<compute::janitor_config const &>((py::arg("j_config") = compute::janitor_config{})))
        .def("start_server", &py_server::start_server)
        .def(
          "set_listening_port",
          &py_server::set_listening_port,
          (py::arg("self"), py::arg("port_no")),
          doc.intro("set the listening port for the service")
            .parameters()
            .parameter("port_no", "int", "a valid and available tcp-ip port number to listen on.")
            .paramcont("typically it could be 20000 (avoid using official reserved numbers)")
            .returns("nothing", "None", "")())
        .def(
          "add_container",
          &py_server::add_container,
          (py::arg("self"), py::arg("container_name"), py::arg("root_dir")),
          doc.intro("Add a container to the server's DTSS.")
            .parameters()
            .parameter("container_name", "str", "name of container to create.")
            .parameter("root_dir", "str", "Directory where container's time series are stored.")
            .returns("nothing", "None", "")())
        .def(
          "get_listening_port",
          &py_server::get_listening_port,
          (py::arg("self")),
          "returns the port number it's listening at for serving incoming request")
        .def(
          "get_listening_ip",
          &py_server::get_listening_ip,
          (py::arg("self")),
          doc.intro("return the ip adress the server is listening on for serving incoming requests")())
        .def(
          "set_listening_ip",
          &py_server::set_listening_ip,
          (py::arg("self"), py::arg("ip")),
          doc.intro("set the listening port for the service")
            .parameters()
            .parameter("ip", "str", "ip or host-name to start listening on")
            .returns("nothing", "None", "")())
        .def(
          "do_get_version_info",
          &py_server::do_get_version_info,
          (py::arg("self")),
          "returns the version number of the StsServer")
        .def(
          "set_master_slave_mode",
          &py_server::set_master,
          (py::arg("self"),
           py::arg("ip"),
           py::arg("port"),
           py::arg("master_poll_time"),
           py::arg("unsubscribe_threshold"),
           py::arg("unsubscribe_max_delay")),
          doc
            .intro("Set master-slave mode, redirecting all IO calls on this dtss to the master ip:port dtss.\n"
                   "This instance of the dtss is kept in sync with changes done on the master using subscription to "
                   "changes on the master\n"
                   "Calculations, and caches are still done locally unloading the computational efforts from the "
                   "master.")
            .parameters()
            .parameter("ip", "str", "The ip address where the master dtss is running")
            .parameter("port", "int", "The port number for the master dtss")
            .parameter("master_poll_time", "time", "[s] max time between each update from master, typicall 0.1 s is ok")
            .parameter(
              "unsubscribe_threshold",
              "int",
              "minimum number of unsubscribed time-series before also unsubscribing from the master")
            .parameter("unsubscribe_max_delay", "time", "maximum time to delay unsubscriptions, regardless number")())
        .def(
          "do_create_model",
          &py_server::py_create_model,
          (py::arg("self"), py::arg("mid")),
          doc.intro("Create a new model")
            .parameters()
            .parameter("mid", "str", "ID of new model")
            .returns("mdl", "StmSystem", "Empty model with ID set to 'mid'")
            .raises()
            .raise("RuntimeError", "If 'mid' already is ID of a model")())
        .def(
          "do_add_model",
          &py_server::py_add_model,
          (py::arg("self"), py::arg("mid"), py::arg("mdl")),
          doc.intro("Add model to server")
            .parameters()
            .parameter("mid", "str", "ID/key to store model as.")
            .parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
            .returns("success", "bool", "Returns True on success")
            .raises()
            .raise("RuntimeError", "If 'mid' is already an ID of a model.")())
        .def(
          "do_remove_model",
          &py_server::py_remove_model,
          (py::arg("self"), py::arg("mid")),
          doc.intro("Remove model by ID.")
            .parameters()
            .parameter("mid", "str", "ID of model to remove.")
            .returns("success", "bool", "Returns True on success.")
            .raises()
            .raise("RuntimeError", "If model with ID 'mid' does not exist.")())
        .def(
          "do_rename_model",
          &py_server::py_rename_model,
          (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
          doc.intro("Rename a model")
            .parameters()
            .parameter("old_mid", "str", "ID of model to rename")
            .parameter("new_mid", "str", "New ID of model")
            .returns("success", "bool", "Returns True on success.")
            .raises()
            .raise("RuntimeError", "'new_mid' already stores a model")
            .raise("RuntimeError", "'old_mid' doesn't store a model to rename")())
        .def(
          "do_clone_model",
          &py_server::py_clone_model,
          (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
          doc.intro("Clone existing model with ID.")
            .parameters()
            .parameter("old_mid", "str", "ID of model to clone")
            .parameter("new_mid", "str", "ID to store cloned model against")
            .returns("success", "bool", "Returns True on success")
            .raises()
            .raise("RuntimeError", "'new_mid' already stores a model")
            .raise("RuntimeError", "'old_mid' doesn't store a model to clone")())
        .def(
          "do_get_model_ids",
          &py_server::py_get_model_ids,
          (py::arg("self")),
          doc.intro("Get IDs of all models stored").returns("id_list", "List[str]", "List of model IDs")())
        .def(
          "do_get_model_infos",
          &py_server::py_get_model_infos,
          (py::arg("self")),
          doc.intro("Get model infos of all models stored")
            .returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
            .see_also("shyft.energy_market.core.ModelInfo")())
        .def(
          "do_get_state",
          &py_server::py_get_state,
          (py::arg("self"), py::arg("mid")),
          doc.intro("Get state of stored model by ID")
            .parameters()
            .parameter("mid", "str", "ID of model to get state of")
            .returns("state", "ModelState", "State of requested model")
            .raises()
            .raise("RuntimeError", "Unable to find model with ID 'mid'")())
        .def_readwrite(
          "fx",
          &py_server::py_fx_cb,
          doc.intro("Callable[[str,str],bool]: server-side callable function(lambda) that takes two parameters:")
            .intro("mid :  the model id")
            .intro("fx_arg: arbitrary string to pass to the server-side function")
            .intro("The server-side fx is called when the client (or web-api) invokea the c.fx(mid,fx_arg).")
            .intro("The signature of the callback function should be fx_cb(mid:str, fx_arg:str)->bool")
            .intro("This feature is simply enabling the users to tailor server-side functionality in python!")
            .intro("Examples:\n")
            .intro(">>> from shyft.energy_market.stm import DStmServer\n"
                   ">>> s=DStmServer()\n"
                   ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
                   ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
                   ">>>   # note we can use captured Server s here!"
                   ">>>     return True\n"
                   ">>> # and then bind the function to the callback\n"
                   ">>> s.fx=my_fx\n"
                   ">>> s.start_server()\n"
                   ">>> : # later using client from anywhere to invoke the call\n"
                   ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n")())
        .def(
          "do_optimize",
          &py_server::py_optimize,
          (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd"), py::arg("compute_node_mode") = false),
          doc.intro("Run SHOP optimization on a model")
            .parameters()
            .parameter("mid", "str", "ID of model to run optimization on")
            .parameter("ta", "TimeAxis", "Time span to run optimization over")
            .parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
            .parameter(
              "compute_node_mode",
              "bool",
              "default false, for compute nodes set to true to minimize work done after optimize")
            .see_also("ShopCommand")
            .returns("success", "bool", "Stating whether optimization was started successfully or not.")())
        .def(
          "do_evaluate_model",
          &py_server::py_evaluate_model,
          (py::arg("self"),
           py::arg("mid"),
           py::arg("bind_period"),
           py::arg("use_ts_cached_read") = true,
           py::arg("update_ts_cache") = false,
           py::arg("clip_period") = utcperiod{}),
          doc.intro("Evaluate any unbound time series attributes of a model")
            .parameters()
            .parameter("mid", "str", "ID of model to evaluate")
            .parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
            .parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
            .parameter(
              "update_ts_cache",
              "bool",
              "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
            .parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
            .see_also("UtcPeriod")
            .returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")())
        .def(
          "start_web_api",
          &py_server::start_web_api,
          (py::arg("self"),
           py::arg("host_ip"),
           py::arg("port"),
           py::arg("doc_root"),
           py::arg("fg_threads") = 2,
           py::arg("bg_threads") = 4,
           py::arg("tls_only") = false),
          doc.intro("Start a web API for communicating with server")
            .parameters()
            .parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only, etc.")
            .parameter("port", "int", "port number to serve the web API on. Ensure it's available!")
            .parameter("doc_root", "str", "directory from which we will serve http/https documents.")
            .parameter("fg_threads", "int", "number of web API foreground threads, typical 1-4 depending on load.")
            .parameter("bg_threads", "int", "number of long running background thread workers to serve requests etc.")
            .parameter("tls_only", "bool", "default false, set to true to enforce tls sessions only.")())
        .def(
          "stop_web_api", &py_server::stop_web_api, (py::arg("self")), doc.intro("Stops any ongoing web API service")())
        .def(
          "get_web_api_port",
          &py_server::get_web_api_port,
          (py::arg("self")),
          doc.intro("Get listening port for web API. Returns -1 if not running")())
        .def(
          "get_web_api_ip",
          &py_server::get_web_api_ip,
          (py::arg("self")),
          doc.intro("Get listening IP for web API. Returns empty string if not running.")())
        .def(
          "notify_change",
          &py_server::py_do_notify_change,
          (py::arg("self"), py::arg("urls")),
          doc.intro("Notify change on model urls, dstm://M..., so that changes are pushed to subscribers.")
            .intro("In case the urls are wrong, misformed etc, there is no exception raised for that.")
            .intro("It's the callers responsibility entirely to provide valid URLs")
            .parameters()
            .parameter("urls", "StringVector", "List of valid model-urls as kept by this server")())
        .def(
          "close",
          &py_server::clear,
          (py::arg("self")),
          doc.intro("close and stop serving requests on the hpc binary socket interface")())
        .def(
          "add_compute_server",
          &py_server::add_compute_server,
          (py::arg("self"), py::arg("host_port")),
          doc.intro("add a compute node specified by it's address string of form host:port")
            .parameters()
            .parameter("host_port", "str", "the address of the dstm compute node service in the form of host:port")())
        .def(
          "stop_server",
          &py_server::stop_server,
          (py::arg("self"), py::arg("timeout") = 1000),
          doc.intro("stop serving connections, gracefully.").see_also("start_server()")())
        .def(
          "is_running",
          &py_server::is_running,
          (py::arg("self")),
          doc.intro("true if server is listening and running").see_also("start_server()")())
        .def_readwrite(
          "shared_lock_timeout",
          &py_server::shared_lock_timeout,
          doc.intro("maximum time to wait for shared-lock on models when doing evaluate call(default 200 ms)")())
        .def(
          "apply",
          &py_server::py_apply,
          (py::arg("self"), py::arg("mid"), py::arg("action")),
          doc.intro("Apply an action to a model.")
            .intro("")
            .intro("The action gets exclusive access to the "
                   "model when applied.")
            .intro("Take care not to return something owned by the model itself,")
            .intro("as exclusive access will be lost on return")
            .parameters()
            .parameter("mid", "str", "ID of model")
            .parameter("action", "Callable[[StmSystem],object]", "function to apply to model")
            .returns("obj", "object", "Object returned by the action")())
        .add_property(
          "cache_stats",
          &py_server::get_cache_stats,
          doc.intro("CacheStats: the internal dtss current cache statistics")())
        .def(
          "clear_cache_stats",
          &py_server::clear_cache_stats,
          (py::arg("self")),
          doc.intro("clear accumulated cache_stats of the internal dtss server")())
        .add_property(
          "cache_max_items",
          &py_server::get_cache_size,
          &py_server::set_cache_size,
          doc.intro("int: internal dtss cache_max_items is the maximum number of time-series identities that are")
            .intro("kept in memory. Elements exceeding this capacity is elided using the least-recently-used")
            .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
            .intro("time-series from cache in the least recently used order.")())
        .add_property(
          "cache_ts_initial_size_estimate",
          &py_server::get_ts_size,
          &py_server::set_ts_size,
          doc.intro("int: The internal dtss initial time-series size estimate in bytes for the cache mechanism.")
            .intro("memory-target = cache_ts_initial_size_estimate * cache_max_items")
            .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
            .intro("time-series from cache in the least recently used order.")())
        .add_property(
          "cache_memory_target",
          &py_server::get_cache_memory_target_size,
          &py_server::set_cache_memory_target_size,
          doc.intro("int: The internal dtss servere memory max target in number of bytes.")
            .intro("If not set directly the following equation is  use:")
            .intro("cache_memory_target = cache_ts_initial_size_estimate * cache_max_items")
            .intro("When setting the target directly, number of items in the chache is ")
            .intro("set so that real memory usage is less than the specified target.")
            .intro("The setter could cause elements to be flushed out of cache.")())
        .def(
          "flush_cache",
          &py_server::remove_from_cache,
          (py::arg("self"), py::arg("ts_ids")),
          doc.intro("flushes the *specified* ts_ids from cache")
            .intro("Has only effect for ts-ids that are in cache, non-existing items are ignored")
            .parameters()
            .parameter("ts_ids", "StringVector", "a list of time-series ids to flush out")())
        .def(
          "flush_cache_all",
          &py_server::flush_cache_all,
          (py::arg("self")),
          doc.intro("flushes all items out of internal dtss cache (cache_stats remain un-touched)")());

    py::class_<py_client, boost::noncopyable>(
      "DStmClient",
      doc.intro("Client side for the DStmServer")
        .intro("")
        .intro("Takes care of message exchange to the remote server, using the supplied parameters.")
        .intro("It implements the message protocol of the server, sending message-prefix, ")
        .intro("arguments, waiting for the response, deserialize the response ")
        .intro("and handle it back to the user.")
        .see_also("DStmServer")(),
      py::init<string, int>((py::arg("self"), py::arg("host_port"), py::arg("timeout_ms"))))
      .def_readonly("host_port", &py_client::get_host_port, "str: Endpoint network address of the remote server.")
      .def_readonly(
        "timeout_ms", &py_client::get_timeout_ms, "int: Timout for remote server operations, in number milliseconds.")
      .def_readonly("is_open", &py_client::is_open, "bool: If the connection to the remote server is (still) open.")
      .def_readonly(
        "reconnect_count",
        &py_client::get_reconnect_count,
        "int: Number of reconnects to the remote server that have been performed.")
      .def(
        "close",
        &py_client::close,
        (py::arg("self")),
        doc.intro("Close the connection. It will automatically reopen if needed.")())
      .def(
        "get_server_version",
        &py_client::get_server_version,
        (py::arg("self")),
        doc.intro("Get version of remote server.").returns("version", "str", "Server version string")())
      .def(
        "create_model",
        &py_client::create_model,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Create an empty model and store it server side.")
          .parameters()
          .parameter("mid", "str", "ID of new model")
          .returns("success", "bool", "Returns true on success")())
      .def(
        "add_model",
        &py_client::add_model,
        (py::arg("self"), py::arg("mid"), py::arg("mdl")),
        doc.intro("Add model to server")
          .parameters()
          .parameter("mid", "str", "ID/key to store model as.")
          .parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
          .returns("success", "bool", "Returns True on success")())
      .def(
        "remove_model",
        &py_client::remove_model,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Remove model by ID.")
          .parameters()
          .parameter("mid", "str", "ID of model to remove.")
          .returns("success", "bool", "Returns True on success.")())
      .def(
        "rename_model",
        &py_client::rename_model,
        (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
        doc.intro("Rename a model")
          .parameters()
          .parameter("old_mid", "str", "ID of model to rename")
          .parameter("new_mid", "str", "New ID of model")
          .returns("success", "bool", "Returns True on success.")())
      .def(
        "clone_model",
        &py_client::clone_model,
        (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
        doc.intro("Clone existing model with ID.")
          .parameters()
          .parameter("old_mid", "str", "ID of model to clone")
          .parameter("new_mid", "str", "ID to store cloned model against")
          .returns("success", "bool", "Returns True on success")())
      .def(
        "get_model_ids",
        &py_client::get_model_ids,
        (py::arg("self")),
        doc.intro("Get IDs of all models stored").returns("id_list", "List[str]", "List of model IDs")())
      .def(
        "get_model_infos",
        &py_client::get_model_infos,
        (py::arg("self")),
        doc.intro("Get model infos of all models stored")
          .returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
          .see_also("shyft.energy_market.core.ModelInfo")())
      .def(
        "get_model",
        &py_client::get_model,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Get a stored model by ID")
          .parameters()
          .parameter("mid", "str", "ID of model to get")
          .returns("mdl", "StmSystem", "Requested model")())
      .def(
        "get_state",
        &py_client::get_state,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Get the state of a model by ID")
          .parameters()
          .parameter("mid", "str", "ID of model to get state of")
          .returns("state", "ModelState", "State of requested model")())
      .def(
        "optimize",
        &py_client::optimize,
        (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd"), py::arg("compute_node_mode") = false),
        doc.intro("Run optimization on a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .parameter("ta", "TimeAxis", "Time span to run optimization over")
          .parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
          .parameter(
            "compute_node_mode",
            "bool",
            "default false, for compute nodes set to true to minimize work done after optimize")
          .see_also("ShopCommand")
          .returns("success", "bool", "Stating whether optimization was started successfully or not.")())
      .def(
        "start_tune",
        &py_client::start_tune,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Start tuning a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .returns("success", "bool", "Stating tuning was started successfully or not.")())
      .def(
        "tune",
        &py_client::tune,
        (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd")),
        doc.intro("Run tuning optimization on a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .parameter("ta", "TimeAxis", "Time span to run optimization over")
          .parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
          .see_also("ShopCommand")
          .returns("success", "bool", "Stating whether tuning optimization was started successfully or not.")())
      .def(
        "stop_tune",
        &py_client::stop_tune,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Stop tuning a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .returns("success", "bool", "Stating tuning was stopped successfully or not.")())
      .def(
        "get_log",
        &py_client::get_log,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Get log for a model")
          .parameters()
          .parameter("mid", "str", "ID of model to get log for")
          .returns("entries", "ShopLogEntryList", "List of log entries")())
      .def(
        "get_optimization_summary",
        &py_client::get_optimization_summary,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Get the optimization summary of a model by ID")
          .parameters()
          .parameter("mid", "str", "ID of model to get state of")
          .returns("summary", "", "The summary of last successful optimization. Values will be nan if not available")())
      .def(
        "kill_optimization",
        &py_client::kill_optimization,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Request to kill a running optimization for a model by ID")
          .parameters()
          .parameter("mid", "str", "ID of model to get state of")
          .returns("success", "bool", "true if the server-side accepted the kill signal (optimization was running)")())
      .def(
        "evaluate_ts",
        &py_client::evaluate_ts,
        (py::arg("self"),
         py::arg("tsv"),
         py::arg("bind_period"),
         py::arg("use_ts_cached_read") = true,
         py::arg("update_ts_cache") = false,
         py::arg("clip_period") = utcperiod{}),
        doc.intro("Evaluate any time series expressions")
          .intro("If an expression can not be evaluated, TsEvaluationError is returned in place of the ts.")
          .parameters()
          .parameter("tsv", "TsVector", "Timeseries to evaluate")
          .parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
          .parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
          .parameter(
            "update_ts_cache",
            "bool",
            "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
          .parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
          .see_also("UtcPeriod")
          .returns("result", "list", "Returns the evaluated timeseries or an evaluation error.")())
      .def(
        "evaluate_model",
        &py_client::evaluate_model,
        (py::arg("self"),
         py::arg("mid"),
         py::arg("bind_period"),
         py::arg("use_ts_cached_read") = true,
         py::arg("update_ts_cache") = false,
         py::arg("clip_period") = utcperiod{}),
        doc.intro("Evaluate any unbound time series attributes of a model")
          .parameters()
          .parameter("mid", "str", "ID of model to evaluate")
          .parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
          .parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
          .parameter(
            "update_ts_cache",
            "bool",
            "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
          .parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
          .see_also("UtcPeriod")
          .returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")())
      .def(
        "fx",
        &py_client::fx,
        (py::arg("self"), py::arg("mid"), py::arg("fx_arg")),
        doc.intro("Execute the serverside fx, passing supplied arguments")
          .parameters()
          .parameter("mid", "str", "ID of model for the server-side fx")
          .parameter("fx_arg", "str", "any argument passed to the server-side fx")
          .returns("success", "bool", "true if call successfully done")())
      .def(
        "get_ts",
        &py_client::get_ts,
        (py::arg("self"), py::arg("mid"), py::arg("ts_urls")),
        doc.intro("Get the time-series from the model mid, as specified by ts_urls")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("ts_urls", "StringVector", "Strongly typed list of strings, urls, like dstm://Mmid/..")
          .returns("time-series", "TsVector", "list of time-series as specifed by the list of ts_urls, same order")())
      .def(
        "set_ts",
        &py_client::set_ts,
        (py::arg("self"), py::arg("mid"), py::arg("tsv")),
        doc.intro("Set the time-series in the model mid, as specified by ts_urls in the tsv.")
          .intro("If anyone is subscribers on time-series, or expressions affected, they are notified")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("tsv", "TsVector", "list of TimeSeries(ts_url,ts_with_values) ,ts_url, like dstm://Mmid/..")())
      .def(
        "add_compute_server",
        &py_client::add_compute_server,
        (py::arg("self"), py::arg("host_port")),
        doc.intro("add a compute node specified by it's address string of form host:port")
          .parameters()
          .parameter("host_port", "str", "the address of the dstm compute node service in the form of host:port")())
      .def(
        "set_attrs",
        &py_client::set_attrs,
        (py::arg("self"), py::arg("attrs")),
        doc.intro("Set a list of attributes specified by url.")
          .intro("If anyone is subscribed on the attribute they are notified")
          .parameters()
          .parameter("attrs", "list", "list of pairs of dstm url and attribute.")
          .returns(
            "attrs", "list", "list of None if successful or UrlResolveError if the attribute could not be set.")())
      .def(
        "get_attrs",
        &py_client::get_attrs,
        (py::arg("self"), py::arg("urls")),
        doc.intro("Get a list of attributes specified by url.")
          .intro("If an url is unable to be resolved UrlResolveError is returned in place of the attribute.")
          .parameters()
          .parameter("urls", "list", "list of dstm attribute urls.")
          .returns("attrs", "list", "list of dstm attributes.")())
      .def(
        "reset_model",
        &py_client::reset_model,
        (py::arg("self"), py::arg("mid")),
        doc.intro("Reset the model specified by mid.")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .returns("ok", "bool", "whether or not the model was reset.")())
      .def(
        "patch",
        &py_client::patch,
        (py::arg("self"), py::arg("mid"), py::arg("op"), py::arg("p")),
        doc.intro("Patch the model `mid` with patch `p` using operation `op`.")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("op", "", "Operation is one of ADD,REMOVE_RELATIONS,REMOVE_OBJECTS")
          .parameter("p", "", "The StmSystem describing the patch")
          .returns("ok", "bool", "whether or not the model was reset.")())
      .def(
        "compute_server_status",
        &py_client::compute_server_status,
        (py::arg("self")),
        doc.intro("Get status of managed compute servers")
          .returns("status", "List[ComputeServerStatus]", "status of managed compute servers.")());
  }

}
