/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/srv/db.h>
#include <shyft/energy_market/stm/stm_system.h>

#include <shyft/py/api/bindings.h>
#include <shyft/py/energy_market/py_model_client_server.h>

namespace shyft::srv {

  /**
   * @brief receive_patch
   * @details
   * The client provides customisation for receiving models.
   * Fx to be applied after client receives a stm_system,
   * in this case fix_uplinks, so they are pointing to stm_system.
   * This ensure that the model do have uplinks for all child objects.
   *
   */
  template <>
  struct receive_patch<shyft::energy_market::stm::stm_system> {
    static void apply(std::shared_ptr<shyft::energy_market::stm::stm_system> const & m) {
      shyft::energy_market::stm::stm_system::fix_uplinks(m);
    }
  };
}

namespace shyft::energy_market::stm::srv { namespace storage {
  void hps_client_server() {
    using model = shyft::energy_market::stm::stm_hps;
    using client_t = shyft::pyapi::energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = shyft::pyapi::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;

    shyft::pyapi::energy_market::expose_client<client_t>(
      "HpsClient", "The client api for the hydro-power-system repostory server.");
    shyft::pyapi::energy_market::expose_server<srv_t>(
      "HpsServer", "The server-side component for the hydro-power-system model repository.");
  }

  void stm_client_server() {
    using model = shyft::energy_market::stm::stm_system;
    using client_t = shyft::pyapi::energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = shyft::pyapi::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;

    shyft::pyapi::energy_market::expose_client<client_t>("StmClient", "The client api for the stm repository server.");
    shyft::pyapi::energy_market::expose_server<srv_t>(
      "StmServer", "The server-side component for the stm energy_market model repository.");
  }

  void pyexport() {
    hps_client_server();
    stm_client_server();
  }
}}
