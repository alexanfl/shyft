/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <cstring>
#include <memory>
#include <ranges>
#include <vector>

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/reflection.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  void pyexport_model_unit_group() {

    auto ug = py::class_<unit_group, py::bases<id_base>, std::shared_ptr<unit_group>, boost::noncopyable>(
      "UnitGroup",
      "A a group of Units, with constraints applicable to the sum of the unit-features (production, flow...), that the "
      "optimization can take into account.",
      py::no_init);

    ug
      .add_property(
        "tag",
        +[](unit_group const & self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readwrite(
        "group_type",
        &unit_group::group_type,
        "unit_group_type: Unit group-type, one of GroupType enum, fcr_n_up, fcr_n_down etc.")
      .def_readonly("members", &unit_group::members, "_MemberList: unit group members")
      .add_property("market_area", &unit_group::get_energy_market_area, "MarketArea: Getter market area.")
      .def(
        "add_unit",
        &unit_group::add_unit,
        (py::arg("self"), py::arg("unit"), py::arg("active")),
        doc.intro("Adds a unit to the group, maintaining any needed expressions.")
          .parameters()
          .parameter("unit", "Unit", "The unit to be added to the group (sum expressions automagically updated).")
          .parameter(
            "active", "TimeSeries", "Determine the temporal group-member-ship, if empty Ts, then always member.")())
      .def(
        "remove_unit",
        &unit_group::remove_unit,
        (py::arg("self"), py::arg("unit")),
        doc.intro("Remove a nunit from the group maintaining any needed expressions.")
          .parameters()
          .parameter(
            "unit", "Unit", "The unit to be removed from the group (sum expressions automagically updated).")())
      .def(
        "_update_sum_expressions",
        &unit_group::update_sum_expressions,
        (py::arg("self")),
        doc.intro("update the sum-expressions, in the case this has not been done, or is out of sync.")())
      .def_readonly(
        "obligation",
        &unit_group::obligation,
        doc.intro("_Obligation: Obligation schedule, cost, results and penalty.")())
      .def_readonly(
        "delivery",
        &unit_group::delivery,
        doc.intro("_Delivery: Sum of product/obligation delivery schedule,result and realised for unit-members.")())
      .def(py::self == py::self)
      .def(py::self != py::self)
      .add_property(
        "obj",
        &expose::py_object_ext<unit_group>::get_obj,
        &expose::py_object_ext<unit_group>::set_obj,
        "object: python object")
      .def(
        "flattened_attributes",
        +[](unit_group& self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.")

      ;
    shyft::pyapi::expose_format(ug);
    expose::expose_custom_maps(ug);
    add_proxy_property(
      ug,
      "production",
      unit_group,
      production,
      doc.intro("_ts: [W] the sum resulting production for this unit-group.")());
    add_proxy_property(
      ug, "flow", unit_group, flow, doc.intro("_ts: [m3/s] the sum resulting water flow for this unit-group.")());

    using UnitGroupList = std::vector<std::shared_ptr<unit_group>>;
    auto ul = py::class_<UnitGroupList>("UnitGroupList", "A strongly typed list of UnitsGroups.");
    ul.def(py::vector_indexing_suite<UnitGroupList, true>())
      .def(
        "__init__",
        expose::construct_from<UnitGroupList>(py::default_call_policies(), (py::arg("unit_groups"))),
        "Construct from list.")
      .def(
        "__eq__",
        +[](UnitGroupList const & a, UnitGroupList const & b) -> bool {
          return equal_attribute(a, b);
        })
      .def(
        "__ne__", +[](UnitGroupList const & a, UnitGroupList const & b) -> bool {
          return !equal_attribute(a, b);
        });

    shyft::pyapi::expose_format(ul);

    {
      auto t_ugt = py::enum_<unit_group_type>(
        "UnitGroupType",
        doc.intro(
          "The unit-group type specifies the purpose of the group, and thus also how\n"
          "it is mapped to the optimization as constraint. E.g. operational reserve fcr_n.up.\n"
          "Current mapping to optimizer/shop:\n\n"
          "    *        FCR* :  primary reserve, instant response, note, sensitivity set by droop settings on the unit "
          "\n"
          "    *       AFRR* : automatic frequency restoration reserve, ~ minute response\n"
          "    *       MFRR* : it is the manual frequency restoration reserve, ~ 15 minute response\n"
          "    *         FFR : NOT MAPPED, fast frequency restoration reserve, 49.5..49.7 Hz, ~ 1..2 sec response\n"
          "    *        RR*  : NOT MAPPED, replacement reserve, 40..60 min response\n"
          "    *      COMMIT : currently not mapped\n"
          "    *  PRODUCTION : used for energy market area unit groups\n\n")());
      shyft::pyapi::expose_enumerators(t_ugt);
      t_ugt.export_values();
    }


    /** scope unit_group_member like UnitGroup._Member*/ {
      py::scope up_scope = ug;

      auto ugo = py::class_<unit_group::obligation_, py::bases<>, boost::noncopyable>(
        "_Obligation",
        doc.intro("This describes the obligation aspects of the unit group that the members should satisfy.")(),
        py::no_init);
      shyft::pyapi::expose_format(ugo);
      _add_proxy_property(
        ugo, "schedule", unit_group::obligation_, schedule, "_ts: [W] schedule/target that should be met.");
      _add_proxy_property(
        ugo, "cost", unit_group::obligation_, cost, "_ts: [money/W] the cost of not satisfying the schedule.");
      _add_proxy_property(
        ugo, "result", unit_group::obligation_, result, "_ts: [W] the resulting target/slack met after optimization.");
      _add_proxy_property(
        ugo,
        "penalty",
        unit_group::obligation_,
        penalty,
        "_ts: [money] If target violated, the cost of the violation.");

      auto ugd = py::class_<unit_group::delivery_, py::bases<>, boost::noncopyable>(
        "_Delivery", doc.intro("This describes the sum of product delivery aspects for the units.")(), py::no_init);
      shyft::pyapi::expose_format(ugd);
      _add_proxy_property(ugd, "schedule", unit_group::delivery_, schedule, "_ts: [product-unit] sum schedule");
      _add_proxy_property(ugd, "result", unit_group::delivery_, result, "_ts: [product-unit] sum result");
      _add_proxy_property(
        ugd, "realised", unit_group::delivery_, realised, "_ts: [product-unit] sum realised, as historical fact");

      auto ugm = py::class_<unit_group_member, py::bases<>, std::shared_ptr<unit_group_member>, boost::noncopyable>(
        "_Member",
        "A unit group member, refers to a specific unit along with the time-series that takes care of the temporal "
        "membership/contribution to the unit_group sum.",
        py::no_init);

      ugm.def_readonly("unit", &unit_group_member::unit, doc.intro("Unit: The unit this member represents.")());
      add_proxy_property(
        ugm,
        "active",
        unit_group_member,
        active,
        doc.intro("_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
                  "unit.")());
      shyft::pyapi::expose_format(ugm);

      using UnitGroupMemberList = std::vector<std::shared_ptr<unit_group_member>>;
      auto ugml = py::class_<UnitGroupMemberList>("_MemberList", "A strongly typed list of UnitsGroup._Member.");
      ugml.def(py::vector_indexing_suite<UnitGroupMemberList, true>())
        .def(
          "__init__",
          expose::construct_from<UnitGroupMemberList>(py::default_call_policies(), (py::arg("unit_group_members."))),
          "Construct from list.");
      shyft::pyapi::expose_format(ugml);
    }
  }
}
