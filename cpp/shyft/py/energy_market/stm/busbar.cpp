/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  void pyexport_model_busbar() {

    auto c = py::class_<busbar, py::bases<>, std::shared_ptr<busbar>, boost::noncopyable>(
      "Busbar", doc.intro("A hub connected by transmission lines")(), py::no_init);
    c.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<network> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("net")),
            "Create busbar with unique id and name for a network."))
      .def_readwrite("id", &busbar::id, "int: Unique id for this object.")
      .def_readwrite("name", &busbar::name, "str: Name for this object.")
      .def_readwrite("json", &busbar::json, "str: Json keeping any extra data for this object.")
      .add_property(
        "network",
        +[](busbar const & self) {
          return self.net_();
        },
        "Network: The owning/parent network for this busbar.")
      .add_property(
        "tag",
        +[](busbar const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def("__eq__", &busbar::operator==)
      .def("__ne__", &busbar::operator!=)
      .def(
        "flattened_attributes",
        +[](busbar &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "dict: Flat dict containing all component attributes.")
      .def_readonly("flow", &busbar::flow, "_TsTriplet: Flow attributes.")
      .def_readonly("price", &busbar::price, "_TsTriplet: Price attributes.")
      .def_readonly(
        "power_modules", &busbar::power_modules, "_PowerModuleMemberList: modules associated with this busbar")
      .def_readonly("units", &busbar::units, "_UnitMemberList: Units associated with this busbar")
      .def_readonly("wind_farms", &busbar::wind_farms, "_WindFarmMemberList: Wind Farms associated with this busbar")
      .def(
        "get_transmission_lines_from_busbar",
        &busbar::get_transmission_lines_from_busbar,
        "Get any transmission lines connected from this busbar")
      .def(
        "get_transmission_lines_to_busbar",
        &busbar::get_transmission_lines_to_busbar,
        "Get any transmission lines connected to this busbar")
      .def("get_market_areas", &busbar::get_market_areas, "Get any market areas associated with this busbar")
      .def(
        "add_to_start_of_transmission_line",
        &busbar::add_to_start_of_transmission_line,
        "Add this busbar to the start of a transmission line")
      .def(
        "add_to_end_of_transmission_line",
        &busbar::add_to_end_of_transmission_line,
        "Add this busbar to the end of a transmission line")
      .def("add_power_module", &busbar::add_power_module, "Associate a (time-dependent) power module to this busbar")
      .def(
        "remove_power_module", &busbar::remove_power_module, "Remove a (time-dependent) power module from this busbar")
      .def("add_unit", &busbar::add_unit, "Associate a (time-dependent) unit to this busbar")
      .def("remove_unit", &busbar::remove_unit, "Remove a (time-dependent) unit from this busbar")
      .def("add_wind_farm", &busbar::add_wind_farm, "Associate a (time-dependent) wind farm to this busbar")
      .def("remove_wind_farm", &busbar::remove_wind_farm, "Remove a (time-dependent) wind farm from this busbar")
      .def("add_to_market_area", &busbar::add_to_market_area, "Associate a market area to this busbar")

      ;
    shyft::pyapi::expose_format(c);
    expose::expose_custom_maps(c);

    auto pmm = py::class_<power_module_member, py::bases<>, std::shared_ptr<power_module_member>, boost::noncopyable>(
      "_PowerModuleMember",
      "A power_mobule busbar member, refers to a specific power_module along with the time-series that takes care of "
      "the temporal membership/contribution to the busbar sum.",
      py::no_init);

    pmm.def_readonly(
      "power_module",
      &power_module_member::power_module,
      doc.intro("PowerModule: The power_module this member represents.")());
    add_proxy_property(
      pmm,
      "active",
      power_module_member,
      active,
      doc.intro("_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
                "power_module.")());
    shyft::pyapi::expose_format(pmm);

    auto um = py::class_<unit_member, py::bases<>, std::shared_ptr<unit_member>, boost::noncopyable>(
      "_UnitMember",
      "A unit busbar member, refers to a specific unit along with the time-series that takes care of the temporal "
      "membership/contribution to the busbar sum.",
      py::no_init);

    um.def_readonly("unit", &unit_member::unit, doc.intro("Unit: The unit this member represents.")());
    add_proxy_property(
      um,
      "active",
      unit_member,
      active,
      doc.intro("_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
                "unit.")());
    shyft::pyapi::expose_format(um);

    auto wtm = py::class_<wind_farm_member, py::bases<>, std::shared_ptr<wind_farm_member>, boost::noncopyable>(
      "_WindFarmMember",
      "A wind farm busbar member, refers to a specific wind farm along with the time-series that takes care of the "
      "temporal "
      "membership/contribution to the busbar sum.",
      py::no_init);

    wtm.def_readonly("farm", &wind_farm_member::farm, doc.intro("Farm: The wind farm this member represents.")());
    add_proxy_property(
      wtm,
      "active",
      unit_member,
      active,
      doc.intro("_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
                "wind farm.")());
    shyft::pyapi::expose_format(wtm);

    using UnitMemberList = std::vector<std::shared_ptr<unit_member>>;
    auto uml = py::class_<UnitMemberList>("_UnitMemberList", "A strongly typed list of Busbar._UnitMember.");
    uml.def(py::vector_indexing_suite<UnitMemberList, true>())
      .def(
        "__init__",
        expose::construct_from<UnitMemberList>(py::default_call_policies(), (py::arg("busbar_unit_members."))),
        "Construct from list.");
    shyft::pyapi::expose_format(uml);

    using PowerModuleMemberList = std::vector<std::shared_ptr<power_module_member>>;
    auto pmml = py::class_<PowerModuleMemberList>(
      "_PowerModuleMemberList", "A strongly typed list of Busbar._PowerModuleMember.");
    pmml.def(py::vector_indexing_suite<PowerModuleMemberList, true>())
      .def(
        "__init__",
        expose::construct_from<PowerModuleMemberList>(
          py::default_call_policies(), (py::arg("busbar_power_module_members."))),
        "Construct from list.");
    shyft::pyapi::expose_format(pmml);

    using WindFarmMemberList = std::vector<std::shared_ptr<wind_farm_member>>;
    auto wml = py::class_<WindFarmMemberList>(
      "_WindFarmMemberList", "A strongly typed list of Busbar._WindFarmMember.");
    wml.def(py::vector_indexing_suite<WindFarmMemberList, true>())
      .def(
        "__init__",
        expose::construct_from<WindFarmMemberList>(py::default_call_policies(), (py::arg("busbar_wind_farm_members."))),
        "Construct from list.");
    shyft::pyapi::expose_format(wml);

    expose::expose_vector_eq<std::shared_ptr<busbar>>(
      "BusbarList", "A strongly typed list of Busbar.", &equal_attribute<std::vector<std::shared_ptr<busbar>>>, false);

    {
      py::scope scope_unit = c;
      auto p = py::class_<busbar::ts_triplet, py::bases<>, boost::noncopyable>(
        "_TsTriplet", doc.intro("Describes .realised, .schedule and .result time-series")(), py::no_init);
      _add_proxy_property(
        p, "realised", busbar::ts_triplet, realised, "_ts: SI-units, realised time-series, as in historical fact");
      _add_proxy_property(
        p, "schedule", busbar::ts_triplet, schedule, "_ts: SI-units, schedule, as in current schedule");
      _add_proxy_property(
        p, "result", busbar::ts_triplet, result, "_ts: SI-units, result, as provided by optimisation");
      shyft::pyapi::expose_format(p);
    }
  }
}
