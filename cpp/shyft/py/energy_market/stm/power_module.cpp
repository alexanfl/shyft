/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>

namespace shyft::energy_market::stm {

  void pyexport_model_power_module() {
    auto c = py::class_<power_module, py::bases<id_base>, std::shared_ptr<power_module>, boost::noncopyable>(
      "PowerModule", doc.intro("A power module representing consumption.")(), py::no_init);
    c.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_system> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
            "Create power module with unique id and name for a stm system."))
      .add_property(
        "system",
        +[](power_module const & self) {
          return self.sys_();
        },
        "StmSystem: The owning/parent system that keeps this area.")
      .add_property(
        "tag",
        +[](power_module const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly("power", &power_module::power, "_Power: Power attributes.")
      .def("__eq__", &power_module::operator==)
      .def("__ne__", &power_module::operator!=)
      .def(
        "flattened_attributes",
        +[](power_module &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(c);
    expose::expose_custom_maps(c);

    expose::expose_vector_eq<std::shared_ptr<power_module>>(
      "PowerModuleList",
      "A strongly typed list of PowerModule.",
      &equal_attribute<std::vector<std::shared_ptr<power_module>>>,
      false);

    {
      py::scope scope_unit = c;
      auto p = py::class_<power_module::power_, py::bases<>, boost::noncopyable>(
        "_Power", doc.intro("Unit.Power attributes, consumption[W, J/s].")(), py::no_init);
      shyft::pyapi::expose_format(p);
      _add_proxy_property(
        p,
        "realised",
        power_module::power_,
        realised,
        "_ts: Historical fact, time series. W, J/s, if positive then production, if negative then consumption.");
      _add_proxy_property(
        p,
        "schedule",
        power_module::power_,
        schedule,
        "_ts: The current schedule, time series. W, J/s, if positive then production, if negative then consumption");
      _add_proxy_property(
        p,
        "result",
        power_module::power_,
        result,
        "_ts: The optimal/simulated/estimated result, time series. W, J/s, if positive then production, if negative "
        "then consumption");
    }
  }
}
