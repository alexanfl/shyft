/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <memory>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  void pyexport_model_transmission_line() {
    auto c = py::class_<transmission_line, py::bases<id_base>, std::shared_ptr<transmission_line>, boost::noncopyable>(
      "TransmissionLine", doc.intro("A transmission line connecting two busbars.")(), py::no_init);
    c.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<network> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("net")),
            "Create transmission line with unique id and name for a network."))
      .add_property(
        "network",
        +[](transmission_line const & self) {
          return self.net_();
        },
        "Network: The owning/parent system that keeps this area.")
      .add_property(
        "tag",
        +[](transmission_line const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly("from_bb", &transmission_line::from_bb, "Busbar: connected from this transmission line")
      .def_readonly("to_bb", &transmission_line::to_bb, "Busbar: connected to this transmission line")
      .def("__eq__", &transmission_line::operator==)
      .def("__ne__", &transmission_line::operator!=)
      .def(
        "flattened_attributes",
        +[](transmission_line &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(c);
    expose::expose_custom_maps(c);
    add_proxy_property(c, "capacity", transmission_line, capacity, "_ts: Transmission line capacity");
    expose::expose_vector_eq<std::shared_ptr<transmission_line>>(
      "TransmissionLineList",
      "A strongly typed list of TransmissionLine.",
      &equal_attribute<std::vector<std::shared_ptr<transmission_line>>>,
      false);
  }
}
