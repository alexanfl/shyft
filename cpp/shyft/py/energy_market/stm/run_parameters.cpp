/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/expose_optional.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>

namespace shyft::energy_market::stm {

  using TimestampedString = std::pair<utctime, std::string>;
  using MessageList = std::vector<TimestampedString>;

  void pyexport_model_run_parameters() {
    // later we need to relocate this
    expose::register_optional<std::int16_t>();
    expose::register_optional<bool>();

    auto ts = py::class_<TimestampedString>(
      "TimestampedString", "A string with a corresponding timestamp.", py::no_init);
    ts.def_readonly("time", &TimestampedString::first, "time: timestamp")
      .def_readonly("message", &TimestampedString::second, "str: message");
    shyft::pyapi::expose_format(ts);

    auto ml = py::class_<MessageList>("MessageList", "A strongly typed list of str.");
    ml.def(py::vector_indexing_suite<MessageList, true>())
      .def(
        "__init__",
        expose::construct_from<MessageList>(py::default_call_policies(), (py::arg("messages"))),
        "Construct from list of messages.")
      .add_property(
        "exists",
        +[](MessageList* l) {
          return l ? l->size() > 0 : false;
        },
        "bool: true if any messages");
    shyft::pyapi::expose_format(ml);

    auto rp = py::class_<run_parameters, py::bases<>, std::shared_ptr<run_parameters>, boost::noncopyable>(
      "RunParameters", "A set of parameters from a simulation- or optimization run.", py::no_init);
    rp.def_readwrite("n_inc_runs", &run_parameters::n_inc_runs, "int: Number of incremental runs.")
      .def_readwrite("n_full_runs", &run_parameters::n_full_runs, "int: Number of full runs.")
      .def_readwrite("head_opt", &run_parameters::head_opt, "bool: Whether head optimization is turned on.")
      .def(py::self == py::self)
      .def(py::self != py::self);

    shyft::pyapi::expose_format(rp);
    expose::expose_custom_maps(rp);

    add_proxy_property(rp, "fx_log", run_parameters, fx_log, "MessageList: from the fx-callback.");
    add_proxy_property(rp, "run_time_axis", run_parameters, run_time_axis, "TimeAxis: The time axis for the SHOP run.");

    expose::def_a_wrap<MessageList>("_message_list");
  }
}
