/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  void pyexport_model_reservoir() {
    auto r = py::class_<reservoir, py::bases<hydro_power::reservoir>, std::shared_ptr<reservoir>, boost::noncopyable>(
      "Reservoir", "Stm reservoir.", py::no_init);
    r.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_hps> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
            "Create reservoir with unique id and name for a hydro power system."))
      .add_property("level", &reservoir::level, "_Level: Level attributes.")
      .add_property(
        "tag",
        +[](reservoir const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly("volume", &reservoir::volume, "_Volume: Volume attributes.")
      .def_readonly("inflow", &reservoir::inflow, "_Inflow: Inflow attributes.")
      .def_readonly("ramping", &reservoir::ramping, "_Ramping: Ramping attributes.")
      .def_readonly("water_value", &reservoir::water_value, "_WaterValue: water-value attributes.")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "flattened_attributes",
        +[](reservoir &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(r);
    expose::expose_custom_maps(r);

    add_proxy_property(
      r,
      "volume_level_mapping",
      reservoir,
      volume_level_mapping,
      "_t_xy_: Volume capacity description, time-dependent x=[masl], y=[m3].");

    expose::expose_vector_eq<std::shared_ptr<reservoir>>(
      "ReservoirList",
      "A strongly typed list of Reservoirs.",
      &equal_attribute<std::vector<std::shared_ptr<reservoir>>>,
      false);

    {
      py::scope scope_r = r;
      auto rl = py::class_<reservoir::level_, py::bases<>, boost::noncopyable>("_Level", py::no_init);
      rl.def_readonly("constraint", &reservoir::level_::constraint, "_Constraints: Level constraint attributes.");
      shyft::pyapi::expose_format(rl);
      _add_proxy_property(
        rl,
        "regulation_min",
        reservoir::level_,
        regulation_min,
        "_ts: [masl] Lowest regulated water level, time-dependent attribute.");
      _add_proxy_property(
        rl,
        "regulation_max",
        reservoir::level_,
        regulation_max,
        "_ts: [masl] Highest regulated water level, time-dependent attribute.");
      _add_proxy_property(
        rl, "realised", reservoir::level_, realised, "_ts: [masl] Historical water level, time series.");
      _add_proxy_property(rl, "schedule", reservoir::level_, schedule, "_ts: [masl] Level schedule, time series.");
      _add_proxy_property(rl, "result", reservoir::level_, result, "_ts: [masl] Level results, time series.");

      auto rv = py::class_<reservoir::volume_, py::bases<>, boost::noncopyable>("_Volume", py::no_init);
      rv.def_readonly("constraint", &reservoir::volume_::constraint, "_Constraints: Volume constraint attributes.")
        .def_readonly("slack", &reservoir::volume_::slack, "_Slack: Slack attributes.");
      shyft::pyapi::expose_format(rv);
      _add_proxy_property(
        rv,
        "static_max",
        reservoir::volume_,
        static_max,
        "_ts: [m3] Maximum regulated volume, time-dependent attribute.");
      _add_proxy_property(rv, "realised", reservoir::volume_, realised, "_ts: [m3] Historical volume, time series.");
      _add_proxy_property(rv, "schedule", reservoir::volume_, schedule, "_ts: [m3] Volume schedule, time series.");
      _add_proxy_property(rv, "result", reservoir::volume_, result, "_ts: [m3] Volume results, time series.");
      _add_proxy_property(rv, "penalty", reservoir::volume_, penalty, "_ts: [m3] Volume penalty, time series.");

      auto rwv = py::class_<reservoir::water_value_, py::bases<>, boost::noncopyable>("_WaterValue", py::no_init);
      rwv.def_readonly("result", &reservoir::water_value_::result, "_Result: Optimizer detailed watervalue results.");
      shyft::pyapi::expose_format(rwv);

      _add_proxy_property(
        rwv, "endpoint_desc", reservoir::water_value_, endpoint_desc, "_ts: [money/joule] Fixed water-value endpoint.");
      {
        py::scope scope_rwv = rwv;
        auto rwvr = py::class_<reservoir::water_value_::result_, py::bases<>, boost::noncopyable>(
          "_Result", py::no_init);
        _add_proxy_property(
          rwvr,
          "local_volume",
          reservoir::water_value_::result_,
          local_volume,
          "_ts: [money/m3] Resulting water-value.");
        _add_proxy_property(
          rwvr,
          "global_volume",
          reservoir::water_value_::result_,
          global_volume,
          "_ts: [money/m3] Resulting water-value.");
        _add_proxy_property(
          rwvr,
          "local_energy",
          reservoir::water_value_::result_,
          local_energy,
          "_ts: [money/joule] Resulting water-value.");
        _add_proxy_property(
          rwvr,
          "end_value",
          reservoir::water_value_::result_,
          end_value,
          "_ts: [money] Resulting water-value at the endpoint.");
        shyft::pyapi::expose_format(rwvr);
      }
      auto ri = py::class_<reservoir::inflow_, py::bases<>, boost::noncopyable>("_Inflow", py::no_init);
      shyft::pyapi::expose_format(ri);
      _add_proxy_property(ri, "schedule", reservoir::inflow_, schedule, "_ts: [m3/s] Inflow input, time series.");
      _add_proxy_property(ri, "realised", reservoir::inflow_, realised, "_ts: [m3/s] Historical inflow, time series.");
      _add_proxy_property(ri, "result", reservoir::inflow_, result, "_ts: [m3/s]  Inflow result, time series.");

      auto rr = py::class_<reservoir::ramping_, py::bases<>, boost::noncopyable>("_Ramping", py::no_init);
      shyft::pyapi::expose_format(rr);
      _add_proxy_property(
        rr, "level_down", reservoir::ramping_, level_down, "_ts: [m/s] Max level change down, time series.");
      _add_proxy_property(
        rr, "level_up", reservoir::ramping_, level_up, "_ts: [m/s] Max level change up, time series.");
      {
        py::scope rl_scope = rl;
        auto rlc = py::class_<reservoir::level_::constraint_, py::bases<>, boost::noncopyable>(
          "_Constraints", py::no_init);
        shyft::pyapi::expose_format(rlc);
        _add_proxy_property(
          rlc, "min", reservoir::level_::constraint_, min, "_ts: [masl] Level constraint minimum, time series.");
        _add_proxy_property(
          rlc, "max", reservoir::level_::constraint_, max, "_ts: [masl] Level constraint maximum, time series.");
      }

      {
        py::scope rv_scope = rv;
        auto rvc = py::class_<reservoir::volume_::constraint_, py::bases<>, boost::noncopyable>(
          "_Constraints", py::no_init);
        rvc.def_readonly(
          "tactical", &reservoir::volume_::constraint_::tactical, "_Tactical: Tactical volume constraint attributes.");
        shyft::pyapi::expose_format(rvc);
        _add_proxy_property(
          rvc, "min", reservoir::volume_::constraint_, min, "_ts: [m3] Volume constraint minimum, time series.");
        _add_proxy_property(
          rvc, "max", reservoir::volume_::constraint_, max, "_ts: [m3] Volume constraint maximum, time series.");

        {
          py::scope rvc_scope = rvc;
          auto rvct = py::class_<reservoir::volume_::constraint_::tactical_, py::bases<>, boost::noncopyable>(
            "_Tactical", py::no_init);
          rvct
            .def_readonly(
              "min",
              &reservoir::volume_::constraint_::tactical_::min,
              "PenaltyConstraint: [masl],[money] Tactical minimum volume constraint.")
            .def_readonly(
              "max",
              &reservoir::volume_::constraint_::tactical_::max,
              "PenaltyConstraint: [masl],[money] Tactical maximum volume constraint.");
          shyft::pyapi::expose_format(rvct);
        }

        auto rvs = py::class_<reservoir::volume_::slack_, py::bases<>, boost::noncopyable>("_Slack", py::no_init);
        shyft::pyapi::expose_format(rvs);
        _add_proxy_property(rvs, "lower", reservoir::volume_::slack_, lower, "_ts: [m3] Lower volume slack");
        _add_proxy_property(rvs, "upper", reservoir::volume_::slack_, upper, "_ts: [m3] Upper volume slack");

        auto rvco = py::class_<reservoir::volume_::cost_, py::bases<>, boost::noncopyable>("_Cost", py::no_init);
        rvco.def_readonly("flood", &reservoir::volume_::cost_::flood, "_CostCurve: Cost applied to volume.")
          .def_readonly(
            "peak", &reservoir::volume_::cost_::peak, "_CostCurve: Cost applied to the highest volume reached.");
        shyft::pyapi::expose_format(rvco);

        {
          py::scope rvc_scope = rvco;
          auto rvcc = py::class_<reservoir::volume_::cost_::cost_curve_, py::bases<>, boost::noncopyable>(
            "_CostCurve", py::no_init);
          shyft::pyapi::expose_format(rvcc);
          _add_proxy_property(
            rvcc, "curve", reservoir::volume_::cost_::cost_curve_, curve, "_t_xy_: [m3],[money/m3] Volume cost curve.");
          _add_proxy_property(
            rvcc,
            "penalty",
            reservoir::volume_::cost_::cost_curve_,
            penalty,
            "_ts: [money] Volume penalty, time series.");
        }
      }
    }
  }
}
