/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <vector>
#include <memory>
#include <string>

#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_power_plant() {
    auto p =
      py::class_<power_plant, py::bases<hydro_power::power_plant>, std::shared_ptr<power_plant>, boost::noncopyable>(
        "PowerPlant",
        doc.intro("A power plant keeping stm units.")
          .details("After creating the units, create the power plant,\n"
                   "and add the units to the power plant.")(),
        py::no_init);
    p.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_hps>&>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
            "Create power plant with unique id and name for a hydro power system."))
      .def_readonly("discharge", &power_plant::discharge, "_Discharge: Discharge attributes.")
      .def_readonly("production", &power_plant::production, "_Production: Production attributes.")
      .def_readonly("best_profit", &power_plant::best_profit, "_BestProfit: BP curves.")
      .def_readonly("fees", &power_plant::fees, "_Fees: other costs of production.")
      .add_property(
        "tag",
        +[](power_plant const & self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def(
        "add_unit",
        +[](std::shared_ptr<power_plant>& pp, std::shared_ptr<unit>& u) {
          hydro_power::power_plant::add_unit(pp, u);
        },
        (py::arg("self"), py::arg("unit")),
        doc.intro("Add unit to plant.")())
      .def("__eq__", &power_plant::operator==)
      .def("__ne__", &power_plant::operator!=)
      .def(
        "flattened_attributes",
        +[](power_plant& self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(p);
    expose::expose_custom_maps(p);

    add_proxy_property(
      p, "outlet_level", power_plant, outlet_level, "_ts: [masl] Outlet level, time-dependent attribute.");
    add_proxy_property(p, "gross_head", power_plant, gross_head, "_ts: [masl]Gross head, time-dependent attribute.");
    add_proxy_property(p, "mip", power_plant, mip, "_ts: Mip flag.");
    add_proxy_property(p, "unavailability", power_plant, unavailability, "_ts: Unavailability, time series.");

    {
      py::scope pp_scope = p;
      auto pd = py::class_<power_plant::discharge_, boost::noncopyable>("_Discharge", py::no_init);
      shyft::pyapi::expose_format(pd);

      _add_proxy_property(
        pd,
        "constraint_min",
        power_plant::discharge_,
        constraint_min,
        "_ts: [m3/s] Discharge minimum restriction, time series.");
      _add_proxy_property(
        pd,
        "constraint_max",
        power_plant::discharge_,
        constraint_max,
        "_ts: [m3/s] Discharge maximum restriction, time series.");
      _add_proxy_property(
        pd, "schedule", power_plant::discharge_, schedule, "_ts: [m3/s] Discharge schedule, time series.");
      _add_proxy_property(pd, "result", power_plant::discharge_, result, "_ts: [m3/s] Discharge result, time series.");
      _add_proxy_property(
        pd,
        "realised",
        power_plant::discharge_,
        realised,
        "_ts: [m3/s] Discharge realised, usually related/or defined as unit.discharge.realised.");
      _add_proxy_property(
        pd,
        "upstream_level_constraint",
        power_plant::discharge_,
        upstream_level_constraint,
        "_t_xy_: Max discharge limited by upstream water level.");
      _add_proxy_property(
        pd,
        "downstream_level_constraint",
        power_plant::discharge_,
        downstream_level_constraint,
        "_t_xy_: Max discharge limited by downstream water level.");
      _add_proxy_property(
        pd,
        "intake_loss_from_bypass_flag",
        power_plant::discharge_,
        intake_loss_from_bypass_flag,
        "_ts: Headloss of intake affected by bypass discharge.");
      _add_proxy_property(
        pd,
        "ramping_up",
        power_plant::discharge_,
        ramping_up,
        "_ts: [m3/s] Constraint on increasing discharge, time-dependent attribute.");
      _add_proxy_property(
        pd,
        "ramping_down",
        power_plant::discharge_,
        ramping_down,
        "_ts: [m3/s] Constraint on decreasing discharge, time-dependent attribute.");

      auto pp = py::class_<power_plant::production_, boost::noncopyable>("_Production", py::no_init);
      shyft::pyapi::expose_format(pp);
      _add_proxy_property(
        pp,
        "constraint_min",
        power_plant::production_,
        constraint_min,
        "_ts: [W] Production minimum restriction, time series.");
      _add_proxy_property(
        pp,
        "constraint_max",
        power_plant::production_,
        constraint_max,
        "_ts: [W] Production maximum restriction, time series.");
      _add_proxy_property(
        pp, "schedule", power_plant::production_, schedule, "_ts: [W] Production schedule, time series.");
      _add_proxy_property(
        pp,
        "realised",
        power_plant::production_,
        realised,
        "_ts: [W] Production realised, usually related to sum of unit.production.realised.");
      _add_proxy_property(
        pp,
        "merge_tolerance",
        power_plant::production_,
        merge_tolerance,
        "_ts: [W] Max deviation in production, time-dependent attribute.");
      _add_proxy_property(
        pp,
        "ramping_up",
        power_plant::production_,
        ramping_up,
        "_ts: [W] Constraint on increasing production, time-dependent attribute.");
      _add_proxy_property(
        pp,
        "ramping_down",
        power_plant::production_,
        ramping_down,
        "_ts: [W] Constraint on decreasing production, time-dependent attribute.");
      _add_proxy_property(pp, "result", power_plant::production_, result, "_ts: [W] Production result, time series.");
      _add_proxy_property(
        pp,
        "instant_max",
        power_plant::production_,
        instant_max,
        "_ts: [W] Computed instant max production for the rotating units");

      auto pbp = py::class_<power_plant::best_profit_, py::bases<>, boost::noncopyable>(
        "_BestProfit", doc.intro("Describes the best profit (BP) curves of the power plant.")(), py::no_init);
      shyft::pyapi::expose_format(pbp);
      _add_proxy_property(
        pbp,
        "discharge",
        power_plant,
        best_profit_::discharge,
        "_t_xy_: Plant discharge after changing the production of the plant from the current operating point to a new "
        "point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "cost_average",
        power_plant,
        best_profit_::cost_average,
        "_t_xy_: Average production cost after changing the production of the plant from the current operating point "
        "to a new point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "cost_marginal",
        power_plant,
        best_profit_::cost_marginal,
        "_t_xy_: Marginal production cost after changing the production of the plant from the current operating "
        "point to a new point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "cost_commitment",
        power_plant,
        best_profit_::cost_commitment,
        "_t_xy_: Sum of startup costs or shutdown costs after changing the production of the plant from the "
        "current operating point to a new point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "dynamic_water_value",
        power_plant,
        best_profit_::dynamic_water_value,
        "_ts: Flag determining whether water values in the best profit calculation should be based on dynamic "
        "results per time step from the optimization or the static end value description.");
      auto fed = py::class_<power_plant::fees_, py::bases<>, boost::noncopyable>(
        "_Fees", doc.intro("Describes other costs when producing.")(), py::no_init);
      _add_proxy_property(
        fed,
        "feeding_fee",
        power_plant,
        fees_::feeding_fee,
        "_ts: Extra cost for feeding electricity into the grid. The feeding fee is added as an extra production cost "
        "in the objective function. Tip: Use nan for periods that should have no fee.");
    }
    expose::expose_vector_eq<std::shared_ptr<power_plant>>(
      "PowerPlantList",
      "A strongly typed list of PowerPlant.",
      &equal_attribute<std::vector<std::shared_ptr<power_plant>>>,
      false);
  }
}
