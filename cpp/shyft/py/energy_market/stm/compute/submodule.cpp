#include <algorithm>
#include <mutex>
#include <stdexcept>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <shyft/version.h>
#include <shyft/energy_market/stm/srv/compute/client.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/reflection.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/api/expose_variant.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/scoped_gil.h>

namespace shyft::energy_market::stm::srv::compute {

  namespace py = boost::python;
  using shyft::pyapi::scoped_gil_release;

  struct py_client {
    std::mutex mutex;
    client impl;

    py_client(std::string const &host_port, int timeout_ms)
      : impl{
        .connection{host_port, timeout_ms}
    } {
    }

    ~py_client() {
    }

    compute::any_reply send(compute::any_request const &request) {
      scoped_gil_release gil;
      std::unique_lock lock(mutex);
      return std::visit(
        [&](auto const &request) {
          return compute::any_reply{impl.send(request)};
        },
        request);
    }
  };

  void pyexport() {
    {
      auto t_state = py::enum_<compute::state>("State", "Describes the possible states of a compute server");
      shyft::pyapi::expose_enumerators(t_state);
    }
    {
      auto t_managed_server_state = py::enum_<managed_server_state>(
        "ManagedServerState", "Describes the possible states of a managed compute server");
      shyft::pyapi::expose_enumerators(t_managed_server_state);
    }
    {
      auto t_managed_server_state =
        py::class_<managed_server_status>("ServerStatus", "Status of a managed compute server")
          .def(
            "__str__", +[](managed_server_status const &s) {
              return fmt::format("{}", s);
            });
      shyft::pyapi::expose_members(t_managed_server_state);
      expose::expose_vector<managed_server_status>("ServerStatusVector");
    }
    {
      boost::mp11::mp_for_each<boost::describe::describe_enumerators<compute::message_tag>>([&](auto e) {
        {
          using request = compute::request<e.value>;
          auto t_name = shyft::pyapi::pep8_typename(fmt::format("{}_request", e.name));
          auto t_request = py::class_<request>(t_name.c_str(), "Compute request");
          shyft::pyapi::expose_members(t_request);
          shyft::pyapi::expose_format_str(t_request);
          shyft::pyapi::expose_format_repr(t_request);
        }
        {
          using reply = compute::reply<e.value>;
          auto t_name = shyft::pyapi::pep8_typename(fmt::format("{}_reply", e.name));
          auto t_reply = py::class_<reply>(t_name.c_str(), "Compute reply");
          shyft::pyapi::expose_members(t_reply);
          shyft::pyapi::expose_format_str(t_reply);
          shyft::pyapi::expose_format_repr(t_reply);
        }
      });
      expose::register_variant(std::type_identity<compute::any_request>{});
      expose::register_variant(std::type_identity<compute::any_reply>{});
    }
    {
      auto t_client =
        py::class_<py_client, boost::noncopyable>("Client", "A client to the Server.", py::no_init)
          .def(py::init<std::string, int>(
            (py::arg("host_port"), py::arg("timeout_ms")), "Create a client with the host port `host_port` "));

      t_client.def("send", &py_client::send, (py::arg("request")));

      py::class_<server, boost::noncopyable>("Server", "A server for running heavy STM computations.")
        .def("start_server", &compute::server::start_server, py::arg("self"))
        .def(
          "stop_server",
          +[](compute::server &server, int ms) {
            scoped_gil_release gil;
            if (ms > 0)
              server.set_graceful_close_timeout(ms);
            server.clear();
          },
          (py::arg("self"), py::arg("timeout") = 1000))
        .def("is_running", &compute::server::is_running, py::arg("self"))
        .def("set_listening_port", &compute::server::set_listening_port, (py::arg("self"), py::arg("port_no")))
        .def("get_listening_port", &compute::server::get_listening_port, py::arg("self"))
        .def("get_listening_ip", &compute::server::get_listening_ip, py::arg("self"))
        .def("set_listening_ip", &compute::server::set_listening_ip, (py::arg("self"), py::arg("ip")))
        .def("close", &compute::server::clear, py::arg("self"));
    }
  }
}

BOOST_PYTHON_MODULE(compute) {
  namespace py = boost::python;
  py::docstring_options doc_options(true, true, false); // all except c++ signatures
  py::scope().attr("__doc__") = "Shyft Energy Market compute server";
  py::scope().attr("__version__") = shyft::_version_string();
  shyft::energy_market::stm::srv::compute::pyexport();
}
