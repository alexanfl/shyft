/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <memory>
#include <vector>

#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_unit() {

    auto u = py::class_<unit, py::bases<hydro_power::unit>, std::shared_ptr<unit>, boost::noncopyable>(
      "Unit", "Stm unit (turbine and generator assembly).", py::no_init);
    u.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_hps> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
            "Create unit with unique id and name for a hydro power system."))
      .def_readonly("discharge", &unit::discharge, "_Discharge: Discharge attributes.")
      .def_readonly("pump_constraint", &unit::pump_constraint, "_Pump_Constraint: Pump constraint attributes.")
      .def_readonly("production", &unit::production, "_Production: Production attributes.")
      .def_readonly("cost", &unit::cost, "_Cost: Cost attributes.")
      .def_readonly("reserve", &unit::reserve, "_Reserve: Operational reserve attributes.")
      .def_readonly(
        "production_discharge_relation",
        &unit::production_discharge_relation,
        "_ProductionDischargeRelation: PQ curves.")
      .def_readonly("best_profit", &unit::best_profit, "_BestProfit: BP curves.")
      .add_property(
        "tag",
        +[](unit const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")

      .def("__eq__", &unit::operator==)
      .def("__ne__", &unit::operator!=)

      .def(
        "flattened_attributes",
        +[](unit &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(u);
    expose::expose_custom_maps(u);
    add_proxy_property(
      u,
      "effective_head",
      unit,
      effective_head,
      doc.intro("_ts: Effective head of the generator, time-dependent attribute.")());
    add_proxy_property(
      u,
      "generator_description",
      unit,
      generator_description,
      doc.intro("_t_xy_: Generator efficiency curve, time-dependent attribute.")());
    add_proxy_property(
      u,
      "turbine_description",
      unit,
      turbine_description,
      doc.intro("_turbine_description: Time-dependent description of turbine efficiency.")());
    add_proxy_property(
      u,
      "pump_description",
      unit,
      pump_description,
      doc.intro("_t_xy_z_list: Time-dependent description of pump efficiency.")());
    add_proxy_property(
      u,
      "unavailability",
      unit,
      unavailability,
      doc.intro("_ts: Time series where time steps the unit is unavailable are marked with value 1, while a "
                "value nan or 0 means it is available.")());
    add_proxy_property(
      u,
      "pump_unavailability",
      unit,
      pump_unavailability,
      doc.intro("_ts: Time series where time steps the unit is unavailable are marked with value 1, while a "
                "value nan or 0 means it is available.")());
    add_proxy_property(
      u, "priority", unit, priority, doc.intro("_ts: Priority value for determining uploading order.")());

    expose::expose_vector_eq<std::shared_ptr<unit>>(
      "UnitList", "A strongly typed list of Units.", &equal_attribute<std::vector<std::shared_ptr<unit>>>, false);

    {
      py::scope scope_unit = u;
      auto ud = py::class_<unit::discharge_, py::bases<>, boost::noncopyable>(
        "_Discharge", doc.intro("Unit.Discharge attributes, flow[m3/s].")(), py::no_init);
      ud.def_readonly("constraint", &unit::discharge_::constraint, "_Constraint: Constraint group.");
      shyft::pyapi::expose_format(ud);
      _add_proxy_property(ud, "schedule", unit::discharge_, schedule, "_ts: [m^3/s] Discharge schedule.");
      _add_proxy_property(ud, "result", unit::discharge_, result, "_ts: [m^3/s] Discharge result.");
      _add_proxy_property(
        ud,
        "realised",
        unit::discharge_,
        realised,
        "_ts: [m^3/s] Discharge realised, usually the result of non trivial computation based on the measured "
        "unit.production.realised.");

      auto up = py::class_<unit::production_, py::bases<>, boost::noncopyable>(
        "_Production", doc.intro("Unit.Production attributes, effect[W].")(), py::no_init);
      up.def_readonly("constraint", &unit::production_::constraint, "_Constraint: Constraint group.");
      shyft::pyapi::expose_format(up);
      _add_proxy_property(
        up, "result", unit::production_, result, doc.intro("_ts: [W] Production result, time series.")());
      _add_proxy_property(up, "schedule", unit::production_, schedule, "_ts: [W] Production schedule.");
      _add_proxy_property(up, "commitment", unit::production_, commitment, "_ts: Production commitment.");
      _add_proxy_property(up, "pump_commitment", unit::production_, pump_commitment, "_ts: Consumption commitment.");
      _add_proxy_property(
        up, "static_min", unit::production_, static_min, "_ts: [W] Production minimum, time-dependent attribute.");
      _add_proxy_property(
        up, "static_max", unit::production_, static_max, "_ts: [W] Production maximum, time-dependent attribute.");
      _add_proxy_property(
        up,
        "nominal",
        unit::production_,
        nominal,
        "_ts: [W] Nominal production, or installed/rated/nameplate capacity, time-dependent attribute.");
      _add_proxy_property(up, "realised", unit::production_, realised, "_ts: [W] Historical production.");

      auto uph = py::class_<unit::pump_constraint_, py::bases<>, boost::noncopyable>(
        "_Pump_Constraint", doc.intro("Contains the pump constraints.")(), py::no_init);
      shyft::pyapi::expose_format(uph);
      _add_proxy_property(
        uph,
        "min_downstream_level",
        unit::pump_constraint_,
        min_downstream_level,
        "_ts: [masl] Minimum level downstream pump.");

      auto uc = py::class_<unit::cost_, py::bases<>, boost::noncopyable>(
        "_Cost", doc.intro("Unit.Cost contains the start/stop costs.")(), py::no_init);
      shyft::pyapi::expose_format(uc);
      _add_proxy_property(uc, "start", unit::cost_, start, "_ts: [money] Start cost.");
      _add_proxy_property(uc, "stop", unit::cost_, stop, "_ts: [money] Stop cost.");
      _add_proxy_property(uc, "pump_start", unit::cost_, pump_start, "_ts: [money] Start pump cost.");
      _add_proxy_property(uc, "pump_stop", unit::cost_, pump_stop, "_ts: [money] Stop pump cost.");

      {
        py::scope ud_scope = ud;
        auto udc = py::class_<unit::discharge_::constraint_, py::bases<>, boost::noncopyable>(
          "_Constraint", doc.intro("Constraints and limitations to the unit-flow.")(), py::no_init);
        shyft::pyapi::expose_format(udc);
        _add_proxy_property(
          udc, "min", unit::discharge_, constraint_::min, "_ts: [m3/s] Discharge constraint minimum.");
        _add_proxy_property(
          udc, "max", unit::discharge_, constraint_::max, "_ts: [m3/s] Discharge constraint maximum.");
        _add_proxy_property(
          udc,
          "max_from_downstream_level",
          unit::discharge_,
          constraint_::max_from_downstream_level,
          "_t_xy_: Discharge maximum, as a function of downstream pressure/water level.");
      }

      {
        py::scope up_scope = up;
        auto upc = py::class_<unit::production_::constraint_, py::bases<>, boost::noncopyable>(
          "_Constraint", doc.intro("Contains the effect constraints to the unit.")(), py::no_init);
        shyft::pyapi::expose_format(upc);
        _add_proxy_property(upc, "min", unit::production_, constraint_::min, "_ts: [W] Production constraint minimum.");
        _add_proxy_property(upc, "max", unit::production_, constraint_::max, "_ts: [W] Production constraint maximum.");
      }

      auto r = py::class_<unit::reserve_, py::bases<>, boost::noncopyable>(
        "_Reserve", doc.intro("Unit._Reserve contains all operational reserve related attributes.")(), py::no_init);
      shyft::pyapi::expose_format(r);
      r.def_readonly("fcr_n", &unit::reserve_::fcr_n, "_Pair: FCR_n up, down attributes.")
        .def_readonly("fcr_d", &unit::reserve_::fcr_d, "_Pair: FCR_d up, down attributes.")
        .def_readonly("afrr", &unit::reserve_::afrr, "_Pair: aFRR up, down attributes.")
        .def_readonly("mfrr", &unit::reserve_::mfrr, "_Pair: mFRR up, down attributes.")
        .def_readonly("frr", &unit::reserve_::frr, "_Pair: FRR up, down attributes.")
        .def_readonly("rr", &unit::reserve_::rr, "_Pair: RR up, down attributes.")
        .def_readonly("droop", &unit::reserve_::droop, "_Droop: Droop attributes, related/common to fcr settings.");
      _add_proxy_property(
        r,
        "fcr_static_min",
        unit::reserve_,
        fcr_static_min,
        "_ts: [W] Unit min-limit valid for FCR calculations (otherwise long running min is used).");
      _add_proxy_property(
        r,
        "fcr_static_max",
        unit::reserve_,
        fcr_static_max,
        "_ts: [W] Unit max-limit valid for FCR calculations (otherwise long running max is used).");
      _add_proxy_property(r, "fcr_mip", unit::reserve_, fcr_mip, "_ts: FCR flag.")
        _add_proxy_property(r, "mfrr_static_min", unit::reserve_, mfrr_static_min, "_ts: mFRR minimum value.");
      _add_proxy_property(
        r,
        "droop_steps",
        unit::reserve_,
        droop_steps,
        "_t_xy_: General discrete droop steps, x=step number, y=droop settings value.");

      {
        py::scope r_scope = r;
        auto rs = py::class_<unit::reserve_::spec_, py::bases<>, boost::noncopyable>(
          "_Spec",
          doc.intro("Describes reserve specification, (.schedule, or min..result..max) SI-units is W.")(),
          py::no_init);
        _add_proxy_property(rs, "schedule", unit::reserve_::spec_, schedule, "_ts: Reserve schedule.");
        _add_proxy_property(rs, "min", unit::reserve_::spec_, min, "_ts: Reserve minimum of range if no schedule.");
        _add_proxy_property(rs, "max", unit::reserve_::spec_, max, "_ts: Reserve minimum of range if no schedule.");
        _add_proxy_property(rs, "cost", unit::reserve_::spec_, cost, "_ts: Reserve cost.");
        _add_proxy_property(rs, "result", unit::reserve_::spec_, result, "_ts: Reserve result.");
        _add_proxy_property(rs, "penalty", unit::reserve_::spec_, penalty, "_ts: Reserve penalty.");
        _add_proxy_property(rs, "realised", unit::reserve_::spec_, realised, "_ts: Reserve realised.");

        shyft::pyapi::expose_format(rs);
        auto rp = py::class_<unit::reserve_::pair_, py::bases<>, boost::noncopyable>(
          "_Pair", doc.intro("Describes the up and down pair of reserve specification.")(), py::no_init);
        rp.def_readonly("up", &unit::reserve_::pair_::up, "_Spec: Up reserve specification.");
        rp.def_readonly("down", &unit::reserve_::pair_::down, "_Spec: Down reserve specification.");
        shyft::pyapi::expose_format(rp);
      }

      {
        py::scope r_scope = r;
        auto rfd = py::class_<unit::reserve_::fcr_droop_, py::bases<>, boost::noncopyable>(
          "_FcrDroop", doc.intro("Per reserve type attributes")(), py::no_init);
        _add_proxy_property(rfd, "cost", unit::reserve_::fcr_droop_, cost, "_ts: Droop cost. test");
        _add_proxy_property(rfd, "result", unit::reserve_::fcr_droop_, result, "_ts: Droop result. test");
        _add_proxy_property(
          rfd,
          "steps",
          unit::reserve_::fcr_droop_,
          steps,
          "_t_xy_: General discrete droop steps, x=step number, y=droop settings value.");

        shyft::pyapi::expose_format(rfd);

        auto rd = py::class_<unit::reserve_::droop_, py::bases<>, boost::noncopyable>(
          "_Droop",
          doc.intro("Describes droop specification, (.schedule, or min..result..max) SI-units is (%).")(),
          py::no_init);
        _add_proxy_property(rd, "schedule", unit::reserve_::droop_, schedule, "_ts: Droop schedule.");
        _add_proxy_property(rd, "min", unit::reserve_::droop_, min, "_ts: Droop minimum of range if no schedule.");
        _add_proxy_property(rd, "max", unit::reserve_::droop_, max, "_ts: Droop minimum of range if no schedule.");
        _add_proxy_property(rd, "cost", unit::reserve_::droop_, cost, "_ts: Droop cost.");
        _add_proxy_property(rd, "result", unit::reserve_::droop_, result, "_ts: Droop result.");
        _add_proxy_property(rd, "penalty", unit::reserve_::droop_, penalty, "_ts: Droop penalty.");
        _add_proxy_property(rd, "realised", unit::reserve_::droop_, realised, "_ts: Droop realised.");
        rd.def_readonly("fcr_n", &unit::reserve_::droop_::fcr_n, "_FcrDroop: FCR-N droop");
        rd.def_readonly("fcr_d_up", &unit::reserve_::droop_::fcr_d_up, "_FcrDroop: FCR-D up droop.");
        rd.def_readonly("fcr_d_down", &unit::reserve_::droop_::fcr_d_down, "_FcrDroop: FCR-D down droop.");
        shyft::pyapi::expose_format(rd);
      }

      auto upq = py::class_<unit::production_discharge_relation_, py::bases<>, boost::noncopyable>(
        "_ProductionDischargeRelation",
        doc.intro("Describes the production discharge relation (PQ) curves of the unit.")(),
        py::no_init);
      shyft::pyapi::expose_format(upq);
      _add_proxy_property(
        upq,
        "original",
        unit,
        production_discharge_relation_::original,
        "_t_xy_: Original PQ curve that includes non-convex regions, x=flow[m3/s], y=effect[W].");
      _add_proxy_property(
        upq,
        "convex",
        unit,
        production_discharge_relation_::convex,
        "_t_xy_: Convexified PQ curve that includes all the time-dependent operating limits and removes all the "
        "nonconcave points of the original PQ curve, the slope of each segment is non-increasing, x=flow[m3/s], "
        "y=effect[W].");
      _add_proxy_property(
        upq,
        "final",
        unit,
        production_discharge_relation_::final,
        "_t_xy_: Final PQ curve that is the final form included into the MILP optimization problem, the first "
        "point of the convex PQ curve is extended to Q=0, x=flow[m3/s], y=effect[W].");

      auto ubp = py::class_<unit::best_profit_, py::bases<>, boost::noncopyable>(
        "_BestProfit", doc.intro("Describes the best profit (BP) curves of the unit.")(), py::no_init);
      shyft::pyapi::expose_format(ubp);
      _add_proxy_property(
        ubp,
        "production",
        unit,
        best_profit_::production,
        "_t_xy_: Optimal production for the unit given a certain production level of the plant.");
      _add_proxy_property(
        ubp,
        "discharge",
        unit,
        best_profit_::discharge,
        "_t_xy_: Optimal discharge for the generator given a certain production level of the plant.");
      _add_proxy_property(
        ubp,
        "discharge_production_ratio",
        unit,
        best_profit_::discharge_production_ratio,
        "_t_xy_: Ratio between discharge and production for the generator given a certain production level of the "
        "plant.");
      _add_proxy_property(
        ubp,
        "operating_zone",
        unit,
        best_profit_::operating_zone,
        "_t_xy_: Optimal operating zone or Pelton needle combination for the generator given a certain "
        "production level of the plant.");
    }
  }
}
