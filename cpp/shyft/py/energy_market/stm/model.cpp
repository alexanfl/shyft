/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <algorithm>
#include <string>
#include <type_traits>
#include <utility>

#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>
#include <shyft/energy_market/stm/unit_group_type.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_tuple.h>
#include <shyft/py/api/expose_variant.h>
#include <shyft/py/api/reflection.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/core/fmt_std_opt.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  template <class T>
  void pyexport_attribute_type(char const * tp_name) {

    auto c = py::class_<typename T::element_type, py::bases<>, T>(
      tp_name,
      "Time variable value-type.\n"
      "\n"
      "Implemented as a sorted map of tuple (time,value) items.\n"
      "\n"
      "You can assign/replace a new item using:\n"
      ">>> from shyft.energy_market.stm import t_xy\n"
      ">>> from shyft.energy_market.core import XyPointCurve,Point,PointList\n"
      ">>> m = t_xy() # create a time-dependent map-type\n"
      ">>> pts = XyPointCurve(PointList([Point(0.0, 1000.0), Point(1200.0, 2000.0)]))\n"
      ">>> m[time('2018-01-01T00:00:00Z')] = pts\n"
      "\n"
      "And iterate over the tuple like this:\n"
      ">>> for i in m:\n"
      ">>>    print(i.key(),i.data())\n"
      "\n"
      "To make a copy of a time-dependent variable, use:\n"
      ">>> m_clone=t_double_(m) # pass in the object to clone in the constructor"
      "\n");
    c.def(py::map_indexing_suite<typename T::element_type, true>())
      .def(py::init<const typename T::element_type&>((py::arg("clone")), "create a copy of the object to clone"))
      .def(
        "__call__",
        +[](T const & m, shyft::core::utctime const & t) -> typename T::element_type::mapped_type {
          auto it = std::find_if(m->rbegin(), m->rend(), [&t](const auto& v) -> bool {
            return v.first <= t;
          });
          return it != m->rend() ? it->second : nullptr;
        },
        (py::arg("self"), py::arg("time")),
        "Find value for a given time.\n");
    shyft::pyapi::expose_format(c);
  }

  void pyexport_attribute_types() {

    pyexport_attribute_type<t_xy_>("t_xy");
    pyexport_attribute_type<t_xyz_>("t_xyz");
    pyexport_attribute_type<t_xyz_list_>("t_xyz_list");
    pyexport_attribute_type<t_turbine_description_>("t_turbine_description");
    auto ac = py::class_<absolute_constraint, py::bases<>, boost::noncopyable>(
      "AbsoluteConstraint",
      "A grouping of time series related to an absolute constraint (i.e. infinite cost)\n",
      py::no_init);

    _add_proxy_property(ac, "limit", absolute_constraint, limit, "TimeSeries: The threshold related to the constraint");
    _add_proxy_property(
      ac, "flag", absolute_constraint, flag, "TimeSeries: Flag indicating whether the constraint is active or not");
    pyapi::expose_format(ac);

    auto pc = py::class_<penalty_constraint, py::bases<>, boost::noncopyable>(
      "PenaltyConstraint", "A grouping of time-series related to a constraint with a penalty cost\n", py::no_init);

    _add_proxy_property(pc, "limit", penalty_constraint, limit, "TimeSeries: The threshold related to the constraint");
    _add_proxy_property(
      pc, "flag", penalty_constraint, flag, "TimeSeries: Flag indicating whether the constraint is active or not");
    _add_proxy_property(pc, "cost", penalty_constraint, cost, "TimeSeries: The cost of violating the constraint");
    _add_proxy_property(
      pc, "penalty", penalty_constraint, penalty, "TimeSeries: Incurred cost of violating the constraint");

    pyapi::expose_format(pc);

    expose::def_a_wrap<std::int8_t>("_i8");
    expose::def_a_wrap<std::int16_t>("_i16");
    expose::def_a_wrap<std::uint16_t>("_u16");
    expose::def_a_wrap<std::int32_t>("_i32");
    expose::def_a_wrap<std::int64_t>("_i64");
    expose::def_a_wrap<double>("_double");
    expose::def_a_wrap<bool>("_bool");
    expose::def_a_wrap<std::string>("_string");

    expose::def_a_wrap<time_series::dd::apoint_ts>("_ts");
    expose::def_a_wrap<time_axis::generic_dt>("_time_axis");

    expose::def_a_wrap<t_turbine_description_>("_turbine_description");
    expose::def_a_wrap<t_xy_>("_t_xy_");
    expose::def_a_wrap<t_xyz_>("_t_xyz");
    expose::def_a_wrap<t_xyz_list_>("_t_xy_z_list");
    expose::def_a_wrap<unit_group_type>("_unit_group_type");
    py::def(
      "compute_effective_price",
      +[](time_series::dd::apoint_ts usage, t_xy_ bids, bool cheapest) {
        return effective_price(usage, bids, cheapest);
      },
      (py::arg("usage"), py::arg("bids"), py::arg("use_cheapest")),
      "Given usage, and bids, compute the effective price achieved consuming bids in the order as speficied with "
      "`use_cheapest`.\n"
      "If usage is 0, then first available price is computed.\n"
      "If bids are None, usage None, or empty, then empty is returned.\n"
      "If usage is more than available in the bids, the effective price for all the bids are computed.\n"
      "\nArgs:"
      "\n    usage: The usage in W\n"
      "\n    bids: The available bids, time-dependent xy, where x= price [Money/J], y= energy [W]\n"
      "\n    use_cheapest: Take cheapest bids first, act as buyer, if false, act as seller, and take highest bids "
      "first\n"
      "\nReturns:"
      "\n    effective price: The computed effective price result\n");
    {
      auto t_log_severity = py::enum_<log_severity>("LogSeverity", "Log severity\n");
      pyapi::expose_enumerators(t_log_severity);
    }
    {
      auto t_log_entry = py::class_<log_entry, py::bases<>>("LogEntry", "A log entry.\n");
      pyapi::expose_members(t_log_entry);
      pyapi::expose_format_str(t_log_entry);
      pyapi::expose_format_repr(t_log_entry);
      t_log_entry.def(py::self == py::self).def(py::self != py::self);
      expose::expose_vector<log_entry>("LogEntryList", "A strongly typed list of LogEntry.\n");
    }

    expose::register_variant(std::type_identity<std::variant<any_attr, stm::url_resolve_error>>{});
    expose::register_variant(std::type_identity<std::variant<time_series::dd::apoint_ts, evaluate_ts_error>>{});
    expose::register_tuple(std::type_identity<std::pair<std::string, any_attr>>{});
    expose::expose_vector<std::pair<std::string, any_attr>>(
      "SetAttrList", "A list of attribute urls and attrs to assign.\n");
    expose::expose_vector<std::variant<any_attr, stm::url_resolve_error>>(
      "GetAttrResultList", "A list of something that is either an attribute or an url resolve error.\n");
    expose::expose_vector<std::optional<stm::url_resolve_error>>(
      "SetAttrResultList", "A list of something that is either an attribute or an url resolve error.\n");
  }

  extern void pyexport_model_busbar();
  extern void pyexport_model_contract();
  extern void pyexport_model_gate();
  extern void pyexport_model_hps();
  extern void pyexport_model_network();
  extern void pyexport_model_optimization_summary();
  extern void pyexport_model_power_module();
  extern void pyexport_model_power_plant();
  extern void pyexport_model_reservoir_aggregate();
  extern void pyexport_model_reservoir();
  extern void pyexport_model_run_parameters();
  extern void pyexport_model_stm_system();
  extern void pyexport_model_transmission_line();
  extern void pyexport_model_unit();
  extern void pyexport_model_unit_group();
  extern void pyexport_model_waterway();
  extern void pyexport_model_wind_farm();
  extern void pyexport_url_error();
  extern void pyexport_evaluate_ts_error();

  void pyexport() {
    pyexport_attribute_types();
    pyexport_model_unit();
    pyexport_model_unit_group();
    pyexport_model_gate();
    pyexport_model_power_plant();
    pyexport_model_reservoir_aggregate();
    pyexport_model_reservoir();
    pyexport_model_run_parameters();
    pyexport_model_stm_system();
    pyexport_model_transmission_line();
    pyexport_model_waterway();
    pyexport_model_hps();
    pyexport_model_optimization_summary();
    pyexport_model_power_module();
    pyexport_model_busbar();
    pyexport_model_contract();
    pyexport_model_network();
    pyexport_model_wind_farm();
    pyexport_url_error();
    pyexport_evaluate_ts_error();
  }

}
