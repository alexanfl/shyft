/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/formatters.h>

namespace shyft::energy_market::stm {

  /** extensions to ease py expose */
  struct hps_ext {
    static auto to_blob(std::shared_ptr<stm_hps> const &m) {
      auto s = shyft::energy_market::stm::stm_hps::to_blob(m);
      return std::vector<char>(s.begin(), s.end());
    }

    static auto from_blob(std::vector<char> &blob) {
      std::string s(blob.begin(), blob.end());
      return shyft::energy_market::stm::stm_hps::from_blob(s);
    }

    // wrap all create calls via the stm_hps_builder to enforce build-rules
    static auto
      create_catchment(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return stm_hps_builder(s).create_catchment(id, name, json);
    }

    static auto
      create_reservoir(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return stm_hps_builder(s).create_reservoir(id, name, json);
    }

    static auto create_reservoir_aggregate(
      std::shared_ptr<stm_hps> &s,
      int id,
      std::string const &name,
      std::string const &json) {
      return stm_hps_builder(s).create_reservoir_aggregate(id, name, json);
    }

    static auto create_unit(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return stm_hps_builder(s).create_unit(id, name, json);
    }

    static auto create_waterway(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return stm_hps_builder(s).create_waterway(id, name, json);
    }

    static auto create_gate(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return stm_hps_builder(s).create_gate(id, name, json);
    }

    static auto
      create_power_plant(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return stm_hps_builder(s).create_power_plant(id, name, json);
    }

    static auto create_tunnel(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return create_waterway(s, id, name, json);
    }

    static auto create_river(std::shared_ptr<stm_hps> &s, int id, std::string const &name, std::string const &json) {
      return create_waterway(s, id, name, json);
    }
  };

  void pyexport_model_hps() {

    auto h =
      py::class_< stm_hps, py::bases<hydro_power::hydro_power_system>, std::shared_ptr<stm_hps>, boost::noncopyable>(
        "HydroPowerSystem",
        doc.intro("A hydro power system, with indataset.")
          .details("The hydro power system consists of reservoirs, waterway (river/tunnel)\n"
                   "and units. In addition, the power plant has the role of keeping\n"
                   "related units together into a group that resembles what most people\n"
                   "would think is a power plant in this context. The power plant has just\n"
                   "references to the units (generator/turbine parts), but can keep\n"
                   "sum-requirements/schedules and computations valid at power plant level.")(),
        py::no_init);
    h.def(py::init<int, std::string>((py::arg("uid"), py::arg("name")), "Create hydro power system with unique uid."))
      .add_property(
        "system",
        +[](stm_hps const &self) {
          return self.system_();
        },
        "StmSystem: The owning/parent system that keeps this hydro power system.")
      .def_readonly(
        "reservoir_aggregates", &stm_hps::reservoir_aggregates, "ReservoirAggregateList: all the reservoir aggregates")
      .def(
        "create_reservoir",
        &hps_ext::create_reservoir,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm reservoir with unique uid.").returns("reservoir", "Reservoir", "The new reservoir.")())
      .def(
        "create_reservoir_aggregate",
        &hps_ext::create_reservoir_aggregate,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm reservoir aggregate with unique uid.")
          .returns("reservoir_aggregate", "ReservoirAggregate", "The new ReservoirAggregate.")())
      .def(
        "create_unit",
        &hps_ext::create_unit,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm unit.").returns("unit", "Unit", "The new unit.")())
      .def(
        "create_power_plant",
        &hps_ext::create_power_plant,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm power plant that keeps units.")
          .returns("power_plant", "PowerPlant", "The new PowerPlant.")())
      .def(
        "create_waterway",
        &hps_ext::create_waterway,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm waterway (a tunnel or river).").returns("waterway", "Waterway", "The new waterway.")())
      .def(
        "create_gate",
        &hps_ext::create_gate,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm gate.").returns("gate", "Gate", "The new gate.")())
      .def(
        "create_tunnel",
        &hps_ext::create_waterway,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm waterway (a tunnel or river).").returns("waterway", "Waterway", "The new waterway.")())
      .def(
        "create_river",
        &hps_ext::create_waterway,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm waterway (a tunnel or river).").returns("waterway", "Waterway", "The new waterway.")())
      .def(
        "to_blob",
        &hps_ext::to_blob,
        (py::arg("self")),
        doc.intro("Serialize the model to a blob.").returns("blob", "ByteVector", "Blob form of the model.")())
      .def(
        "from_blob",
        &hps_ext::from_blob,
        (py::arg("blob")),
        doc.intro("Re-create a stm hps from a previously create blob.")
          .returns("hps", "HydroPowerSystem", "A stm hydro-power-system including it's attributes in the ids.")())
      .staticmethod("from_blob")
      .def(
        "find_waterway_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<waterway>(s->find_waterway_by_id(id));
        })
      .def(
        "find_gate_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<gate>(s->find_gate_by_id(id));
        })
      .def(
        "find_reservoir_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<reservoir>(s->find_reservoir_by_id(id));
        })
      .def(
        "find_reservoir_aggregate_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<reservoir_aggregate>(s->find_reservoir_aggregate_by_id(id));
        })
      .def(
        "find_unit_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<unit>(s->find_unit_by_id(id));
        })
      .def(
        "find_power_plant_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<power_plant>(s->find_power_plant_by_id(id));
        })
      .def(
        "find_catchment_by_id",
        +[](stm_hps *s, int64_t id) {
          return std::dynamic_pointer_cast<catchment>(s->find_catchment_by_id(id));
        })

      .def(
        "find_waterway_by_name",
        +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<waterway>(s->find_waterway_by_name(n));
        })
      .def(
        "find_gate_by_name",
        +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<gate>(s->find_gate_by_name(n));
        })
      .def(
        "find_reservoir_by_name",
        +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<reservoir>(s->find_reservoir_by_name(n));
        })
      .def(
        "find_reservoir_aggregate_by_name",
        +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<reservoir_aggregate>(s->find_reservoir_aggregate_by_name(n));
        })
      .def(
        "find_unit_by_name",
        +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<unit>(s->find_unit_by_name(n));
        })
      .def(
        "find_power_plant_by_name",
        +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<power_plant>(s->find_power_plant_by_name(n));
        })
      .def(
        "find_catchment_by_name", +[](stm_hps *s, std::string const &n) {
          return std::dynamic_pointer_cast<catchment>(s->find_catchment_by_name(n));
        });
    shyft::pyapi::expose_format(h);

    expose::expose_vector_eq<std::shared_ptr<stm_hps>>(
      "HydroPowerSystemList",
      "A strongly typed list of HydroPowerSystem.",
      &equal_attribute<std::vector<std::shared_ptr<stm_hps>>>,
      false);
  }
}
