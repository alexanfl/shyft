#include <shyft/version.h>
#include <shyft/config.h>
#include <shyft/py/api/bindings.h>

#if defined(SHYFT_WITH_STM)
namespace shyft::energy_market::stm {
  extern void pyexport();

  namespace srv {
    namespace dstm {
      extern void pyexport();
    }

    namespace task {
      extern void pyexport();
    }

    namespace storage {
      extern void pyexport();
    }
  }
}
#endif

BOOST_PYTHON_MODULE(_stm) {
  using namespace shyft;
  py::docstring_options doc_options(true, true, false);
  py::scope().attr("__doc__") = "Shyft Energy Market detailed model";
  py::scope().attr("__version__") = _version_string();

#if defined(SHYFT_WITH_STM)
  py::scope().attr("shyft_with_stm") = true;
  energy_market::stm::pyexport();
  energy_market::stm::srv::storage::pyexport();
  energy_market::stm::srv::dstm::pyexport();
  energy_market::stm::srv::task::pyexport();
#else
  py::scope().attr("shyft_with_stm") = false;
#endif
}
