/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <fmt/core.h>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  /** extensions to ease py expose */
  struct net_ext {
    // wrap all create calls via the network_builder to enforce build-rules
    static auto create_transmission_line(
      std::shared_ptr<network>& n,
      int id,
      std::string const & name,
      std::string const & json) {
      return network_builder(n).create_transmission_line(id, name, json);
    }

    static auto create_busbar(std::shared_ptr<network>& n, int id, std::string const & name, std::string const & json) {
      return network_builder(n).create_busbar(id, name, json);
    }
  };

  void pyexport_model_network() {
    auto c = py::class_<network, py::bases<id_base>, shared_ptr<network>, boost::noncopyable>(
      "Network", doc.intro("A network consisting of busbars and transmission lines.")(), py::no_init);
    c.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_system>&>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
            "Create network with unique id and name for a stm system."))
      .def(
        "create_transmission_line",
        &net_ext::create_transmission_line,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm transmission line with unique uid.")
          .returns("transmission_line", "TransmissionLine", "The new transmission line.")())
      .def(
        "create_busbar",
        &net_ext::create_busbar,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm busbar with unique uid.").returns("busbar", "Busbar", "The new busbar.")())
      .add_property(
        "system",
        +[](network const & self) {
          return self.sys_();
        },
        "StmSystem: The owning/parent system that keeps this network.")
      .add_property(
        "tag",
        +[](network const & self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly(
        "transmission_lines", &network::transmission_lines, "TransmissionLineList: List of transmission lines.")
      .def_readonly("busbars", &network::busbars, "BusbarList: List of busbars.")
      .def("__eq__", &network::operator==)
      .def("__ne__", &network::operator!=);
    shyft::pyapi::expose_format(c);
    expose::expose_custom_maps(c);
    expose::expose_vector_eq<std::shared_ptr<network>>(
      "NetworkList",
      "A strongly typed list of Network.",
      &equal_attribute<std::vector<std::shared_ptr<network>>>,
      false);
  }
}
