#include <shyft/config.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/version.h>

#include <shyft/energy_market/stm/shop/shop_command.h>
#if defined(SHYFT_WITH_SHOP)
#include <boost/preprocessor/stringize.hpp>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/time/utctime_utilities.h>
#endif

namespace shyft::energy_market::stm::shop {

  void expose_shop_command() {

    auto t_shop_command =
      py::class_<shop_command, py::bases<>>(
        "ShopCommand",
        "A shop command, which can later be sent to the shop core.\n"
        "\n"
        "In the shop core a command can be described as a string with syntax:\n"
        "\"<keyword> <specifier> [/<opt> [/<opt>...]] [[<obj>] [<obj>...]]\".\n"
        "The keyword is a general word that specifies the kind of action, for\n"
        "example 'set', 'save' or 'return'.\n"
        "The specifier identifies what data will be affected by the command,\n"
        "and it is unique for every command, for example 'method', or 'ramping'.\n"
        "Commands can accept one or several pre-set options to further specify\n"
        "the command. These always start with a forward slash, consist of one\n"
        "word only, and if more than one the order is important.\n"
        "Some commands also needs input objects, usually an integer, floating\n"
        "point, or string value."
        "\n",
        py::no_init)
        .def(py::init< std::string const &>(
          (py::arg("command")), "Create from a single string in shop command language syntax."))
        .def(py::init<
             std::string const &,
             std::string const &,
             std::vector<std::string> const &,
             std::vector<std::string> const &>(
          (py::arg("keyword"), py::arg("specifier"), py::arg("options"), py::arg("objects")),
          "Create from individual components."))
        .def(py::self == py::self)
        .def(py::self != py::self)

        .def_readwrite("keyword", &shop_command::keyword, "keyword of the command")
        .def_readwrite("specifier", &shop_command::specifier, "specifier of the command")
        .def_readwrite("options", &shop_command::options, "list of options")
        .def_readwrite("objects", &shop_command::objects, "list of objects")

        .def("set_method_primal", &shop_command::set_method_primal, "Shop command string \"set method /primal\".")
        .staticmethod("set_method_primal")
        .def("set_method_dual", &shop_command::set_method_dual, "Shop command string \"set method /dual\".")
        .staticmethod("set_method_dual")
        .def("set_method_baropt", &shop_command::set_method_baropt, "Shop command string \"set method /baropt\".")
        .staticmethod("set_method_baropt")
        .def(
          "set_method_hydbaropt", &shop_command::set_method_hydbaropt, "Shop command string \"set method /hydbaropt\".")
        .staticmethod("set_method_hydbaropt")
        .def(
          "set_method_netprimal", &shop_command::set_method_netprimal, "Shop command string \"set method /netprimal\".")
        .staticmethod("set_method_netprimal")
        .def("set_method_netdual", &shop_command::set_method_netdual, "Shop command string \"set method /netdual\".")
        .staticmethod("set_method_netdual")
        .def("set_code_full", &shop_command::set_code_full, "Shop command string \"set code /full\".")
        .staticmethod("set_code_full")
        .def(
          "set_code_incremental", &shop_command::set_code_incremental, "Shop command string \"set code /incremental\".")
        .staticmethod("set_code_incremental")
        .def("set_code_head", &shop_command::set_code_head, "Shop command string \"set code /head\".")
        .staticmethod("set_code_head")
        .def(
          "set_password",
          &shop_command::set_password,
          (py::arg("key"), py::arg("value")),
          "Shop command string \"set password <value>\".")
        .staticmethod("set_password")
        .def(
          "start_sim",
          &shop_command::start_sim,
          (py::arg("iterations")),
          "Shop command string \"start sim <iterations>\".")
        .staticmethod("start_sim")
        .def("start_shopsim", &shop_command::start_shopsim, "Shop command string \"start shopsim\".")
        .staticmethod("start_shopsim")
        .def<shop_command (*)()>("log_file", &shop_command::log_file, "Shop command string \"log file\".")
        .def<shop_command (*)(std::string)>(
          "log_file", &shop_command::log_file, (py::arg("filename")), "Shop command string \"log file <filename>\".")
        .staticmethod("log_file")
        .def(
          "log_file_lp",
          &shop_command::log_file_lp,
          (py::arg("filename")),
          "Shop command string \"log file /lp <filename>\".")
        .staticmethod("log_file_lp")
        .def("set_xmllog", &shop_command::set_xmllog, (py::arg("on")), "Shop command string \"set xmllog /on|/off\".")
        .staticmethod("set_xmllog")
        .def(
          "return_simres",
          &shop_command::return_simres,
          (py::arg("filename")),
          "Shop command string \"return simres <filename>\".")
        .staticmethod("return_simres")
        .def(
          "return_simres_gen",
          &shop_command::return_simres_gen,
          (py::arg("filename")),
          "Shop command string \"return simres /gen <filename>\".")
        .staticmethod("return_simres_gen")
        .def(
          "save_series",
          &shop_command::save_series,
          (py::arg("filename")),
          "Shop command string \"save series <filename>\".")
        .staticmethod("save_series")
        .def(
          "save_xmlseries",
          &shop_command::save_xmlseries,
          (py::arg("filename")),
          "Shop command string \"save xmlseries <filename>\".")
        .staticmethod("save_xmlseries")
        .def(
          "print_model",
          &shop_command::print_model,
          (py::arg("filename")),
          "Shop command string \"print model <filename>\".")
        .staticmethod("print_model")
        .def(
          "return_scenario_result_table",
          &shop_command::return_scenario_result_table,
          (py::arg("filename")),
          "Shop command string \"return scenario_result_table <filename>\".")
        .staticmethod("return_scenario_result_table")
        .def("save_tunnelloss", &shop_command::save_tunnelloss, "Shop command string \"save tunnelloss\".")
        .staticmethod("save_tunnelloss")
        .def(
          "save_shopsimseries",
          &shop_command::save_shopsimseries,
          (py::arg("filename")),
          "Shop command string \"save shopsimseries <filename>\".")
        .staticmethod("save_shopsimseries")
        .def(
          "return_shopsimres",
          &shop_command::return_shopsimres,
          (py::arg("filename")),
          "Shop command string \"return shopsimres <filename>\".")
        .staticmethod("return_shopsimres")
        .def(
          "return_shopsimres_gen",
          &shop_command::return_shopsimres_gen,
          (py::arg("filename")),
          "Shop command string \"return shopsimres /gen <filename>\".")
        .staticmethod("return_shopsimres_gen")
        .def(
          "save_xmlshopsimseries",
          &shop_command::save_xmlshopsimseries,
          (py::arg("filename")),
          "Shop command string \"save xmlshopsimseries <filename>\".")
        .staticmethod("save_xmlshopsimseries")
        .def(
          "save_pq_curves",
          &shop_command::save_pq_curves,
          (py::arg("on")),
          "Shop command string \"save pq_curves /on|/off\".")
        .staticmethod("save_pq_curves")
        .def<shop_command (*)()>(
          "print_pqcurves_all", &shop_command::print_pqcurves_all, "Shop command string \"print pqcurves /all\".")
        .def<shop_command (*)(std::string)>(
          "print_pqcurves_all",
          &shop_command::print_pqcurves_all,
          (py::arg("filename")),
          "Shop command string \"print pqcurves /all <filename>\".")
        .staticmethod("print_pqcurves_all")
        .def<shop_command (*)()>(
          "print_pqcurves_original",
          &shop_command::print_pqcurves_original,
          "Shop command string \"print pqcurves /original\".")
        .def<shop_command (*)(std::string)>(
          "print_pqcurves_original",
          &shop_command::print_pqcurves_original,
          (py::arg("filename")),
          "Shop command string \"print pqcurves /original <filename>\".")
        .staticmethod("print_pqcurves_original")
        .def<shop_command (*)()>(
          "print_pqcurves_convex",
          &shop_command::print_pqcurves_convex,
          "Shop command string \"print pqcurves /convex\".")
        .def<shop_command (*)(std::string)>(
          "print_pqcurves_convex",
          &shop_command::print_pqcurves_convex,
          (py::arg("filename")),
          "Shop command string \"print pqcurves /convex <filename>\".")
        .staticmethod("print_pqcurves_convex")
        .def<shop_command (*)()>(
          "print_pqcurves_final", &shop_command::print_pqcurves_final, "Shop command string \"print pqcurves /final\".")
        .def<shop_command (*)(std::string)>(
          "print_pqcurves_final",
          &shop_command::print_pqcurves_final,
          (py::arg("filename")),
          "Shop command string \"print pqcurves /final <filename>\".")
        .staticmethod("print_pqcurves_final")
        .def(
          "print_mc_curves",
          &shop_command::print_mc_curves,
          (py::arg("filename")),
          "Shop command string \"print mc_curves <filename>\".")
        .staticmethod("print_mc_curves")
        .def(
          "print_mc_curves_up",
          &shop_command::print_mc_curves_up,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up <filename>\".")
        .staticmethod("print_mc_curves_up")
        .def(
          "print_mc_curves_down",
          &shop_command::print_mc_curves_down,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /down <filename>\".")
        .staticmethod("print_mc_curves_down")
        .def(
          "print_mc_curves_pq",
          &shop_command::print_mc_curves_pq,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /pq <filename>\".")
        .staticmethod("print_mc_curves_pq")
        .def(
          "print_mc_curves_mod",
          &shop_command::print_mc_curves_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /mod <filename>\".")
        .staticmethod("print_mc_curves_mod")
        .def(
          "print_mc_curves_up_pq",
          &shop_command::print_mc_curves_up_pq,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /pq <filename>\".")
        .staticmethod("print_mc_curves_up_pq")
        .def(
          "print_mc_curves_up_mod",
          &shop_command::print_mc_curves_up_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /mod <filename>\".")
        .staticmethod("print_mc_curves_up_mod")
        .def(
          "print_mc_curves_up_pq_mod",
          &shop_command::print_mc_curves_up_pq_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /pq /mod <filename>\".")
        .staticmethod("print_mc_curves_up_pq_mod")
        .def(
          "print_mc_curves_down_pq",
          &shop_command::print_mc_curves_down_pq,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /down /pq <filename>\".")
        .staticmethod("print_mc_curves_down_pq")
        .def(
          "print_mc_curves_down_mod",
          &shop_command::print_mc_curves_down_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /down /mod <filename>\".")
        .staticmethod("print_mc_curves_down_mod")
        .def(
          "print_mc_curves_down_pq_mod",
          &shop_command::print_mc_curves_down_pq_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /down /pq /mod <filename>\".")
        .staticmethod("print_mc_curves_down_pq_mod")
        .def(
          "print_mc_curves_up_down",
          &shop_command::print_mc_curves_up_down,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /down <filename>\".")
        .staticmethod("print_mc_curves_up_down")
        .def(
          "print_mc_curves_up_down_pq",
          &shop_command::print_mc_curves_up_down_pq,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /down /pq <filename>\".")
        .staticmethod("print_mc_curves_up_down_pq")
        .def(
          "print_mc_curves_up_down_mod",
          &shop_command::print_mc_curves_up_down_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /down /mod <filename>\".")
        .staticmethod("print_mc_curves_up_down_mod")
        .def(
          "print_mc_curves_up_down_pq_mod",
          &shop_command::print_mc_curves_up_down_pq_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /up /down /pq /mod <filename>\".")
        .staticmethod("print_mc_curves_up_down_pq_mod")
        .def(
          "print_mc_curves_pq_mod",
          &shop_command::print_mc_curves_pq_mod,
          (py::arg("filename")),
          "Shop command string \"print mc_curves /pq /mod <filename>\".")
        .staticmethod("print_mc_curves_pq_mod")
        .def("print_bp_curves", &shop_command::print_bp_curves, "Shop command string \"print bp_curves\".")
        .staticmethod("print_bp_curves")
        .def(
          "print_bp_curves_all_combinations",
          &shop_command::print_bp_curves_all_combinations,
          "Shop command string \"print bp_curves /all_combinations\".")
        .staticmethod("print_bp_curves_all_combinations")
        .def(
          "print_bp_curves_current_combination",
          &shop_command::print_bp_curves_current_combination,
          "Shop command string \"print bp_curves /current_combination\".")
        .staticmethod("print_bp_curves_current_combination")
        .def(
          "print_bp_curves_from_zero",
          &shop_command::print_bp_curves_from_zero,
          "Shop command string \"print bp_curves /from_zero\".")
        .staticmethod("print_bp_curves_from_zero")
        .def(
          "print_bp_curves_mc_format",
          &shop_command::print_bp_curves_mc_format,
          "Shop command string \"print bp_curves /mc_format\".")
        .staticmethod("print_bp_curves_mc_format")
        .def(
          "print_bp_curves_operation",
          &shop_command::print_bp_curves_operation,
          "Shop command string \"print bp_curves /operation\".")
        .staticmethod("print_bp_curves_operation")
        .def(
          "print_bp_curves_discharge",
          &shop_command::print_bp_curves_discharge,
          "Shop command string \"print bp_curves /discharge\".")
        .staticmethod("print_bp_curves_discharge")
        .def(
          "print_bp_curves_production",
          &shop_command::print_bp_curves_production,
          "Shop command string \"print bp_curves /production\".")
        .staticmethod("print_bp_curves_production")
        .def(
          "print_bp_curves_dyn_points",
          &shop_command::print_bp_curves_dyn_points,
          "Shop command string \"print bp_curves /dyn_points\".")
        .staticmethod("print_bp_curves_dyn_points")
        .def(
          "print_bp_curves_old_points",
          &shop_command::print_bp_curves_old_points,
          "Shop command string \"print bp_curves /old_points\".")
        .staticmethod("print_bp_curves_old_points")
        .def(
          "print_bp_curves_market_ref_mc",
          &shop_command::print_bp_curves_market_ref_mc,
          "Shop command string \"print bp_curves /market_ref_mc\".")
        .staticmethod("print_bp_curves_market_ref_mc")
        .def(
          "print_bp_curves_no_vertical_step",
          &shop_command::print_bp_curves_no_vertical_step,
          "Shop command string \"print bp_curves /no_vertical_step\".")
        .staticmethod("print_bp_curves_no_vertical_step")
        .def(
          "penalty_flag_all",
          &shop_command::penalty_flag_all,
          (py::arg("on")),
          "Shop command string \"penalty flag /all /on|/off\".")
        .staticmethod("penalty_flag_all")
        .def(
          "penalty_flag_reservoir_ramping",
          &shop_command::penalty_flag_reservoir_ramping,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /reservoir /ramping\".")
        .staticmethod("penalty_flag_reservoir_ramping")
        .def(
          "penalty_flag_reservoir_endpoint",
          &shop_command::penalty_flag_reservoir_endpoint,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /reservoir /endpoint\".")
        .staticmethod("penalty_flag_reservoir_endpoint")
        .def(
          "penalty_flag_load",
          &shop_command::penalty_flag_load,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /load\".")
        .staticmethod("penalty_flag_load")
        .def(
          "penalty_flag_powerlimit",
          &shop_command::penalty_flag_powerlimit,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /powerlimit\".")
        .staticmethod("penalty_flag_powerlimit")
        .def(
          "penalty_flag_discharge",
          &shop_command::penalty_flag_discharge,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /discharge\".")
        .staticmethod("penalty_flag_discharge")
        .def(
          "penalty_flag_plant_min_p_con",
          &shop_command::penalty_flag_plant_min_p_con,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /plant /min_p_con\".")
        .staticmethod("penalty_flag_plant_min_p_con")
        .def(
          "penalty_flag_plant_max_p_con",
          &shop_command::penalty_flag_plant_max_p_con,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /plant /max_p_con\".")
        .staticmethod("penalty_flag_plant_max_p_con")
        .def(
          "penalty_flag_plant_min_q_con",
          &shop_command::penalty_flag_plant_min_q_con,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /plant /min_q_con\".")
        .staticmethod("penalty_flag_plant_min_q_con")
        .def(
          "penalty_flag_plant_max_q_con",
          &shop_command::penalty_flag_plant_max_q_con,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /plant /max_q_con\".")
        .staticmethod("penalty_flag_plant_max_q_con")
        .def(
          "penalty_flag_plant_schedule",
          &shop_command::penalty_flag_plant_schedule,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /plant /schedule\".")
        .staticmethod("penalty_flag_plant_schedule")
        .def(
          "penalty_flag_gate_min_q_con",
          &shop_command::penalty_flag_gate_min_q_con,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /gate /min_q_con\".")
        .staticmethod("penalty_flag_gate_min_q_con")
        .def(
          "penalty_flag_gate_max_q_con",
          &shop_command::penalty_flag_gate_max_q_con,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /gate /max_q_con\".")
        .staticmethod("penalty_flag_gate_max_q_con")
        .def(
          "penalty_flag_gate_ramping",
          &shop_command::penalty_flag_gate_ramping,
          (py::arg("on")),
          "Shop command string \"penalty flag /on|/off /gate /ramping\".")
        .staticmethod("penalty_flag_gate_ramping")
        .def(
          "penalty_cost_all",
          &shop_command::penalty_cost_all,
          (py::arg("value")),
          "Shop command string \"penalty cost /all <value>\".")
        .staticmethod("penalty_cost_all")
        .def(
          "penalty_cost_reservoir_ramping",
          &shop_command::penalty_cost_reservoir_ramping,
          (py::arg("value")),
          "Shop command string \"penalty cost /reservoir /ramping <value>\".")
        .staticmethod("penalty_cost_reservoir_ramping")
        .def(
          "penalty_cost_reservoir_endpoint",
          &shop_command::penalty_cost_reservoir_endpoint,
          (py::arg("value")),
          "Shop command string \"penalty cost /reservoir /endpoint <value>\".")
        .staticmethod("penalty_cost_reservoir_endpoint")
        .def(
          "penalty_cost_load",
          &shop_command::penalty_cost_load,
          (py::arg("value")),
          "Shop command string \"penalty cost /load <value>\".")
        .staticmethod("penalty_cost_load")
        .def(
          "penalty_cost_powerlimit",
          &shop_command::penalty_cost_powerlimit,
          (py::arg("value")),
          "Shop command string \"penalty cost /powerlimit <value>\".")
        .staticmethod("penalty_cost_powerlimit")
        .def(
          "penalty_cost_discharge",
          &shop_command::penalty_cost_discharge,
          (py::arg("value")),
          "Shop command string \"penalty cost /discharge <value>\".")
        .staticmethod("penalty_cost_discharge")
        .def(
          "penalty_cost_gate_ramping",
          &shop_command::penalty_cost_gate_ramping,
          (py::arg("value")),
          "Shop command string \"penalty cost /gate /ramping <value>\".")
        .staticmethod("penalty_cost_gate_ramping")
        .def(
          "penalty_cost_overflow",
          &shop_command::penalty_cost_overflow,
          (py::arg("value")),
          "Shop command string \"penalty cost /overflow <value>\".")
        .staticmethod("penalty_cost_overflow")
        .def(
          "penalty_cost_overflow_time_adjust",
          &shop_command::penalty_cost_overflow_time_adjust,
          (py::arg("value")),
          "Shop command string \"penalty cost /overflow_time_adjust <value>\".")
        .staticmethod("penalty_cost_overflow_time_adjust")
        .def(
          "penalty_cost_reserve",
          &shop_command::penalty_cost_reserve,
          (py::arg("value")),
          "Shop command string \"penalty cost /reserve <value>\".")
        .staticmethod("penalty_cost_reserve")
        .def(
          "penalty_cost_soft_p_penalty",
          &shop_command::penalty_cost_soft_p_penalty,
          (py::arg("value")),
          "Shop command string \"penalty cost /soft_p_penalty <value>\".")
        .staticmethod("penalty_cost_soft_p_penalty")
        .def(
          "penalty_cost_soft_q_penalty",
          &shop_command::penalty_cost_soft_q_penalty,
          (py::arg("value")),
          "Shop command string \"penalty cost /soft_q_penalty <value>\".")
        .staticmethod("penalty_cost_soft_q_penalty")
        .def(
          "set_newgate", &shop_command::set_newgate, (py::arg("on")), "Shop command string \"set newgate /on|/off\".")
        .staticmethod("set_newgate")
        .def(
          "set_ramping", &shop_command::set_ramping, (py::arg("mode")), "Shop command string \"set ramping <value>\".")
        .staticmethod("set_ramping")
        .def(
          "set_mipgap",
          &shop_command::set_mipgap,
          (py::arg("absolute"), py::arg("value")),
          "Shop command string \"set mipgap /absolute|/relative <value>\".")
        .staticmethod("set_mipgap")
        .def(
          "set_nseg_all",
          &shop_command::set_nseg_all,
          (py::arg("value")),
          "Shop command string \"set nseg /all <value>\".")
        .staticmethod("set_nseg_all")
        .def(
          "set_nseg_up",
          &shop_command::set_nseg_up,
          (py::arg("value")),
          "Shop command string \"set nseg /up <value>\".")
        .staticmethod("set_nseg_up")
        .def(
          "set_nseg_down",
          &shop_command::set_nseg_down,
          (py::arg("value")),
          "Shop command string \"set nseg /down <value>\".")
        .staticmethod("set_nseg_down")
        .def("set_dyn_seg_on", &shop_command::set_dyn_seg_on, "Shop command string \"set dyn_seg /on\".")
        .staticmethod("set_dyn_seg_on")
        .def("set_dyn_seg_incr", &shop_command::set_dyn_seg_incr, "Shop command string \"set dyn_seg /incr\".")
        .staticmethod("set_dyn_seg_incr")
        .def("set_dyn_seg_mip", &shop_command::set_dyn_seg_mip, "Shop command string \"set dyn_seg /mip\".")
        .staticmethod("set_dyn_seg_mip")
        .def(
          "set_max_num_threads",
          &shop_command::set_max_num_threads,
          (py::arg("value")),
          "Shop command string \"set max_num_threads <value>\".")
        .staticmethod("set_max_num_threads")
        .def(
          "set_parallel_mode_auto",
          &shop_command::set_parallel_mode_auto,
          "Shop command string \"set parallel_mode /auto\".")
        .staticmethod("set_parallel_mode_auto")
        .def(
          "set_parallel_mode_deterministic",
          &shop_command::set_parallel_mode_deterministic,
          "Shop command string \"set parallel_mode /deterministic\".")
        .staticmethod("set_parallel_mode_deterministic")
        .def(
          "set_parallel_mode_opportunistic",
          &shop_command::set_parallel_mode_opportunistic,
          "Shop command string \"set parallel_mode /opportunistic\".")
        .staticmethod("set_parallel_mode_opportunistic")
        .def(
          "set_capacity_all",
          &shop_command::set_capacity_all,
          (py::arg("value")),
          "Shop command string \"set capacity /all <value>\".")
        .staticmethod("set_capacity_all")
        .def(
          "set_capacity_gate",
          &shop_command::set_capacity_gate,
          (py::arg("value")),
          "Shop command string \"set capacity /gate <value>\".")
        .staticmethod("set_capacity_gate")
        .def(
          "set_capacity_bypass",
          &shop_command::set_capacity_bypass,
          (py::arg("value")),
          "Shop command string \"set capacity /bypass <value>\".")
        .staticmethod("set_capacity_bypass")
        .def(
          "set_capacity_spill",
          &shop_command::set_capacity_spill,
          (py::arg("value")),
          "Shop command string \"set capacity /spill <value>\".")
        .staticmethod("set_capacity_spill")
        .def(
          "set_headopt_feedback",
          &shop_command::set_headopt_feedback,
          (py::arg("value")),
          "Shop command string \"set headopt_feedback <value>\".")
        .staticmethod("set_headopt_feedback")
        .def(
          "set_timelimit",
          &shop_command::set_timelimit,
          (py::arg("value")),
          "Shop command string \"set timelimit <value>\".")
        .staticmethod("set_timelimit")
        .def(
          "set_dyn_flex_mip",
          &shop_command::set_dyn_flex_mip,
          (py::arg("value")),
          "Shop command string \"set dyn_flex_mip <value>\".")
        .staticmethod("set_dyn_flex_mip")
        .def(
          "set_universal_mip_on", &shop_command::set_universal_mip_on, "Shop command string \"set universal_mip /on\".")
        .staticmethod("set_universal_mip_on")
        .def(
          "set_universal_mip_off",
          &shop_command::set_universal_mip_off,
          "Shop command string \"set universal_mip /off\".")
        .staticmethod("set_universal_mip_off")
        .def(
          "set_universal_mip_not_set",
          &shop_command::set_universal_mip_not_set,
          "Shop command string \"set universal_mip /not_set\".")
        .staticmethod("set_universal_mip_not_set")
        .def("set_merge_on", &shop_command::set_merge_on, "Shop command string \"set merge /on\".")
        .staticmethod("set_merge_on")
        .def("set_merge_off", &shop_command::set_merge_off, "Shop command string \"set merge /off\".")
        .staticmethod("set_merge_off")
        .def("set_merge_stop", &shop_command::set_merge_stop, "Shop command string \"set merge /stop\".")
        .staticmethod("set_merge_stop")
        .def(
          "set_com_dec_period",
          &shop_command::set_com_dec_period,
          (py::arg("value")),
          "Shop command string \"set com_dec_period <value>\".")
        .staticmethod("set_com_dec_period")
        .def(
          "set_time_delay_unit_hour",
          &shop_command::set_time_delay_unit_hour,
          "Shop command string \"set time_delay_unit hour\".")
        .staticmethod("set_time_delay_unit_hour")
        .def(
          "set_time_delay_unit_minute",
          &shop_command::set_time_delay_unit_minute,
          "Shop command string \"set time_delay_unit minute\".")
        .staticmethod("set_time_delay_unit_minute")
        .def(
          "set_time_delay_unit_time_step_length",
          &shop_command::set_time_delay_unit_time_step_length,
          "Shop command string \"set time_delay_unit time_step_length\".")
        .staticmethod("set_time_delay_unit_time_step_length")
        .def(
          "set_power_head_optimization",
          &shop_command::set_power_head_optimization,
          (py::arg("on")),
          "Shop command string \"set power_head_optimization /on|/off\".")
        .staticmethod("set_power_head_optimization")
        .def(
          "set_bypass_loss",
          &shop_command::set_bypass_loss,
          (py::arg("on")),
          "Shop command string \"set bypass_loss /on|/off\".")
        .staticmethod("set_bypass_loss")
        .def(
          "set_reserve_slack_cost",
          &shop_command::set_reserve_slack_cost,
          (py::arg("value")),
          "Shop command string \"set reserve_slack_cost <value>\".")
        .staticmethod("set_reserve_slack_cost")
        .def(
          "set_fcr_n_band",
          &shop_command::set_fcr_n_band,
          (py::arg("value")),
          "Shop command string \"set fcr_n_band <value>\".")
        .staticmethod("set_fcr_n_band")
        .def(
          "set_fcr_d_band",
          &shop_command::set_fcr_d_band,
          (py::arg("value")),
          "Shop command string \"set fcr_d_band <value>\".")
        .staticmethod("set_fcr_d_band")
        .def(
          "set_fcr_n_equality",
          &shop_command::set_fcr_n_equality,
          (py::arg("value")),
          "Shop command string \"set fcr_n_equality <value>\".")
        .staticmethod("set_fcr_n_equality")
        .def(
          "set_droop_discretization_limit",
          &shop_command::set_droop_discretization_limit,
          (py::arg("value")),
          "Shop command string \"set droop_discretization_limit <value>\".")
        .staticmethod("set_droop_discretization_limit")
        .def(
          "set_reserve_ramping_cost",
          &shop_command::set_reserve_ramping_cost,
          (py::arg("value")),
          "Shop command string \"set reserve_ramping_cost <value>\".")
        .staticmethod("set_reserve_ramping_cost")
        .def(
          "set_gen_turn_off_limit",
          &shop_command::set_gen_turn_off_limit,
          (py::arg("value")),
          "Shop command string \"set gen_turn_off_limit <value>\".")
        .staticmethod("set_gen_turn_off_limit")
        .def(
          "set_prod_from_ref_prod",
          &shop_command::set_prod_from_ref_prod,
          "Shop command string \"set prod_from_ref_prod\".")
        .staticmethod("set_prod_from_ref_prod")

      ;
    t_shop_command.def
      ("__str__",
       +[](const shop_command &c){
         return c.operator std::string();
       });
    shyft::pyapi::expose_format_repr(t_shop_command);
    expose::expose_vector<shop_command>("ShopCommandList", "A strongly typed list of ShopCommand.");
  }

#if defined(SHYFT_WITH_SHOP)

  void expose_shop_commander() {

    py::class_<shop_commander, py::bases<>, std::shared_ptr<shop_commander>, boost::noncopyable>(
      "ShopCommander", "A shop commander, utility for sending individual commands to the shop core.", py::no_init)
      .def("execute", &shop_commander::execute, (py::arg("self"), py::arg("command")), "Execute command object.")
      .def(
        "execute_string",
        &shop_commander::execute_string,
        (py::arg("self"), py::arg("command_string")),
        "Execute command string.")
      .def("executed", &shop_commander::executed, (py::arg("self")), "Get executed commands as objects.")
      .def(
        "executed_strings", &shop_commander::executed_strings, (py::arg("self")), "Get executed commands as strings.")
      .def(
        "set_method_primal",
        &shop_commander::set_method_primal,
        (py::arg("self")),
        "Shop command string \"set method /primal\".")
      .def(
        "set_method_dual",
        &shop_commander::set_method_dual,
        (py::arg("self")),
        "Shop command string \"set method /dual\".")
      .def(
        "set_method_baropt",
        &shop_commander::set_method_baropt,
        (py::arg("self")),
        "Shop command string \"set method /baropt\".")
      .def(
        "set_method_hydbaropt",
        &shop_commander::set_method_hydbaropt,
        (py::arg("self")),
        "Shop command string \"set method /hydbaropt\".")
      .def(
        "set_method_netprimal",
        &shop_commander::set_method_netprimal,
        (py::arg("self")),
        "Shop command string \"set method /netprimal\".")
      .def(
        "set_method_netdual",
        &shop_commander::set_method_netdual,
        (py::arg("self")),
        "Shop command string \"set method /netdual\".")
      .def(
        "set_code_full", &shop_commander::set_code_full, (py::arg("self")), "Shop command string \"set code /full\".")
      .def(
        "set_code_incremental",
        &shop_commander::set_code_incremental,
        (py::arg("self")),
        "Shop command string \"set code /incremental\".")
      .def(
        "set_code_head", &shop_commander::set_code_head, (py::arg("self")), "Shop command string \"set code /head\".")
      .def(
        "set_password",
        &shop_commander::set_password,
        (py::arg("self"), py::arg("key"), py::arg("value")),
        "Shop command string \"set password <value>\".")
      .def(
        "start_sim",
        &shop_commander::start_sim,
        (py::arg("self"), py::arg("iterations")),
        "Shop command string \"start sim <iterations>\".")
      .def("start_shopsim", &shop_commander::start_shopsim, (py::arg("self")), "Shop command string \"start shopsim\".")
      .def<void (shop_commander::*)()>(
        "log_file", &shop_commander::log_file, (py::arg("self")), "Shop command string \"log file\".")
      .def<void (shop_commander::*)(std::string const &)>(
        "log_file",
        &shop_commander::log_file,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"log file <filename>\".")
      .def(
        "log_file_lp",
        &shop_commander::log_file_lp,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"log file /lp <filename>\".")
      .def(
        "set_xmllog",
        &shop_commander::set_xmllog,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"set xmllog /on|/off\".")
      .def(
        "return_simres",
        &shop_commander::return_simres,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"return simres <filename>\".")
      .def(
        "return_simres_gen",
        &shop_commander::return_simres_gen,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"return simres /gen <filename>\".")
      .def(
        "save_series",
        &shop_commander::save_series,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"save series <filename>\".")
      .def(
        "save_xmlseries",
        &shop_commander::save_xmlseries,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"save xmlseries <filename>\".")
      .def(
        "print_model",
        &shop_commander::print_model,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print model <filename>\".")
      .def(
        "return_scenario_result_table",
        &shop_commander::return_scenario_result_table,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"return scenario_result_table <filename>\".")
      .def(
        "save_tunnelloss",
        &shop_commander::save_tunnelloss,
        (py::arg("self")),
        "Shop command string \"save tunnelloss\".")
      .def(
        "save_shopsimseries",
        &shop_commander::save_shopsimseries,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"save shopsimseries <filename>\".")
      .def(
        "return_shopsimres",
        &shop_commander::return_shopsimres,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"return shopsimres <filename>\".")
      .def(
        "return_shopsimres_gen",
        &shop_commander::return_shopsimres_gen,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"return shopsimres /gen <filename>\".")
      .def(
        "save_xmlshopsimseries",
        &shop_commander::save_xmlshopsimseries,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"save xmlshopsimseries <filename>\".")
      .def(
        "save_pq_curves",
        &shop_commander::save_pq_curves,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"save pq_curves /on|/off\".")
      .def<void (shop_commander::*)()>(
        "print_pqcurves_all",
        &shop_commander::print_pqcurves_all,
        (py::arg("self")),
        "Shop command string \"print pqcurves /all\".")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_all",
        &shop_commander::print_pqcurves_all,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print pqcurves /all <filename>\".")
      .def<void (shop_commander::*)()>(
        "print_pqcurves_original",
        &shop_commander::print_pqcurves_original,
        (py::arg("self")),
        "Shop command string \"print pqcurves /original\".")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_original",
        &shop_commander::print_pqcurves_original,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print pqcurves /original <filename>\".")
      .def<void (shop_commander::*)()>(
        "print_pqcurves_convex",
        &shop_commander::print_pqcurves_convex,
        (py::arg("self")),
        "Shop command string \"print pqcurves /convex\".")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_convex",
        &shop_commander::print_pqcurves_convex,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print pqcurves /convex <filename>\".")
      .def<void (shop_commander::*)()>(
        "print_pqcurves_final",
        &shop_commander::print_pqcurves_final,
        (py::arg("self")),
        "Shop command string \"print pqcurves /final\".")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_final",
        &shop_commander::print_pqcurves_final,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print pqcurves /final <filename>\".")
      .def(
        "print_mc_curves",
        &shop_commander::print_mc_curves,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves <filename>\".")
      .def(
        "print_mc_curves_up",
        &shop_commander::print_mc_curves_up,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up <filename>\".")
      .def(
        "print_mc_curves_down",
        &shop_commander::print_mc_curves_down,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /down <filename>\".")
      .def(
        "print_mc_curves_pq",
        &shop_commander::print_mc_curves_pq,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /pq <filename>\".")
      .def(
        "print_mc_curves_mod",
        &shop_commander::print_mc_curves_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /mod <filename>\".")
      .def(
        "print_mc_curves_up_pq",
        &shop_commander::print_mc_curves_up_pq,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /pq <filename>\".")
      .def(
        "print_mc_curves_up_mod",
        &shop_commander::print_mc_curves_up_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /mod <filename>\".")
      .def(
        "print_mc_curves_up_pq_mod",
        &shop_commander::print_mc_curves_up_pq_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /pq /mod <filename>\".")
      .def(
        "print_mc_curves_down_pq",
        &shop_commander::print_mc_curves_down_pq,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /down /pq <filename>\".")
      .def(
        "print_mc_curves_down_mod",
        &shop_commander::print_mc_curves_down_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /down /mod <filename>\".")
      .def(
        "print_mc_curves_down_pq_mod",
        &shop_commander::print_mc_curves_down_pq_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /down /pq /mod <filename>\".")
      .def(
        "print_mc_curves_up_down",
        &shop_commander::print_mc_curves_up_down,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /down <filename>\".")
      .def(
        "print_mc_curves_up_down_pq",
        &shop_commander::print_mc_curves_up_down_pq,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /down /pq <filename>\".")
      .def(
        "print_mc_curves_up_down_mod",
        &shop_commander::print_mc_curves_up_down_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /down /mod <filename>\".")
      .def(
        "print_mc_curves_up_down_pq_mod",
        &shop_commander::print_mc_curves_up_down_pq_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /up /down /pq /mod <filename>\".")
      .def(
        "print_mc_curves_pq_mod",
        &shop_commander::print_mc_curves_pq_mod,
        (py::arg("self"), py::arg("filename")),
        "Shop command string \"print mc_curves /pq /mod <filename>\".")
      .def(
        "print_bp_curves",
        &shop_commander::print_bp_curves,
        (py::arg("self")),
        "Shop command string \"print bp_curves\".")
      .def(
        "print_bp_curves_all_combinations",
        &shop_commander::print_bp_curves_all_combinations,
        (py::arg("self")),
        "Shop command string \"print bp_curves /all_combinations\".")
      .def(
        "print_bp_curves_current_combination",
        &shop_commander::print_bp_curves_current_combination,
        (py::arg("self")),
        "Shop command string \"print bp_curves /current_combination\".")
      .def(
        "print_bp_curves_from_zero",
        &shop_commander::print_bp_curves_from_zero,
        (py::arg("self")),
        "Shop command string \"print bp_curves /from_zero\".")
      .def(
        "print_bp_curves_mc_format",
        &shop_commander::print_bp_curves_mc_format,
        (py::arg("self")),
        "Shop command string \"print bp_curves /mc_format\".")
      .def(
        "print_bp_curves_operation",
        &shop_commander::print_bp_curves_operation,
        (py::arg("self")),
        "Shop command string \"print bp_curves /operation\".")
      .def(
        "print_bp_curves_discharge",
        &shop_commander::print_bp_curves_discharge,
        (py::arg("self")),
        "Shop command string \"print bp_curves /discharge\".")
      .def(
        "print_bp_curves_production",
        &shop_commander::print_bp_curves_production,
        (py::arg("self")),
        "Shop command string \"print bp_curves /production\".")
      .def(
        "print_bp_curves_dyn_points",
        &shop_commander::print_bp_curves_dyn_points,
        (py::arg("self")),
        "Shop command string \"print bp_curves /dyn_points\".")
      .def(
        "print_bp_curves_old_points",
        &shop_commander::print_bp_curves_old_points,
        (py::arg("self")),
        "Shop command string \"print bp_curves /old_points\".")
      .def(
        "print_bp_curves_market_ref_mc",
        &shop_commander::print_bp_curves_market_ref_mc,
        (py::arg("self")),
        "Shop command string \"print bp_curves /market_ref_mc\".")
      .def(
        "print_bp_curves_no_vertical_step",
        &shop_commander::print_bp_curves_no_vertical_step,
        (py::arg("self")),
        "Shop command string \"print bp_curves /no_vertical_step\".")
      .def(
        "penalty_flag_all",
        &shop_commander::penalty_flag_all,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /all /on|/off\".")
      .def(
        "penalty_flag_reservoir_ramping",
        &shop_commander::penalty_flag_reservoir_ramping,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /reservoir /ramping\".")
      .def(
        "penalty_flag_reservoir_endpoint",
        &shop_commander::penalty_flag_reservoir_endpoint,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /reservoir /endpoint\".")
      .def(
        "penalty_flag_load",
        &shop_commander::penalty_flag_load,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /load\".")
      .def(
        "penalty_flag_powerlimit",
        &shop_commander::penalty_flag_powerlimit,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /powerlimit\".")
      .def(
        "penalty_flag_discharge",
        &shop_commander::penalty_flag_discharge,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /discharge\".")
      .def(
        "penalty_flag_plant_min_p_con",
        &shop_commander::penalty_flag_plant_min_p_con,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /plant /min_p_con\".")
      .def(
        "penalty_flag_plant_max_p_con",
        &shop_commander::penalty_flag_plant_max_p_con,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /plant /max_p_con\".")
      .def(
        "penalty_flag_plant_min_q_con",
        &shop_commander::penalty_flag_plant_min_q_con,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /plant /min_q_con\".")
      .def(
        "penalty_flag_plant_max_q_con",
        &shop_commander::penalty_flag_plant_max_q_con,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /plant /max_q_con\".")
      .def(
        "penalty_flag_plant_schedule",
        &shop_commander::penalty_flag_plant_schedule,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /plant /schedule\".")
      .def(
        "penalty_flag_gate_min_q_con",
        &shop_commander::penalty_flag_gate_min_q_con,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /gate /min_q_con\".")
      .def(
        "penalty_flag_gate_max_q_con",
        &shop_commander::penalty_flag_gate_max_q_con,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /gate /max_q_con\".")
      .def(
        "penalty_flag_gate_ramping",
        &shop_commander::penalty_flag_gate_ramping,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"penalty flag /on|/off /gate /ramping\".")
      .def(
        "penalty_cost_all",
        &shop_commander::penalty_cost_all,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /all <value>\".")
      .def(
        "penalty_cost_reservoir_ramping",
        &shop_commander::penalty_cost_reservoir_ramping,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /reservoir /ramping <value>\".")
      .def(
        "penalty_cost_reservoir_endpoint",
        &shop_commander::penalty_cost_reservoir_endpoint,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /reservoir /endpoint <value>\".")
      .def(
        "penalty_cost_load",
        &shop_commander::penalty_cost_load,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /load <value>\".")
      .def(
        "penalty_cost_powerlimit",
        &shop_commander::penalty_cost_powerlimit,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /powerlimit <value>\".")
      .def(
        "penalty_cost_discharge",
        &shop_commander::penalty_cost_discharge,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /discharge <value>\".")
      .def(
        "penalty_cost_gate_ramping",
        &shop_commander::penalty_cost_gate_ramping,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /gate /ramping <value>\".")
      .def(
        "penalty_cost_overflow",
        &shop_commander::penalty_cost_overflow,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /overflow <value>\".")
      .def(
        "penalty_cost_overflow_time_adjust",
        &shop_commander::penalty_cost_overflow_time_adjust,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /overflow_time_adjust <value>\".")
      .def(
        "penalty_cost_reserve",
        &shop_commander::penalty_cost_reserve,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /reserve <value>\".")
      .def(
        "penalty_cost_soft_p_penalty",
        &shop_commander::penalty_cost_soft_p_penalty,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /soft_p_penalty <value>\".")
      .def(
        "penalty_cost_soft_q_penalty",
        &shop_commander::penalty_cost_soft_q_penalty,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"penalty cost /soft_q_penalty <value>\".")
      .def(
        "set_newgate",
        &shop_commander::set_newgate,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"set newgate /on|/off\".")
      .def(
        "set_ramping",
        &shop_commander::set_ramping,
        (py::arg("self"), py::arg("mode")),
        "Shop command string \"set ramping <value>\".")
      .def(
        "set_mipgap",
        &shop_commander::set_mipgap,
        (py::arg("self"), py::arg("absolute"), py::arg("value")),
        "Shop command string \"set mipgap /absolute|/relative <value>\".")
      .def(
        "set_nseg_all",
        &shop_commander::set_nseg_all,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set nseg /all <value>\".")
      .def(
        "set_nseg_up",
        &shop_commander::set_nseg_up,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set nseg /up <value>\".")
      .def(
        "set_nseg_down",
        &shop_commander::set_nseg_down,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set nseg /down <value>\".")
      .def(
        "set_dyn_seg_on",
        &shop_commander::set_dyn_seg_on,
        (py::arg("self")),
        "Shop command string \"set dyn_seg /on\".")
      .def(
        "set_dyn_seg_incr",
        &shop_commander::set_dyn_seg_incr,
        (py::arg("self")),
        "Shop command string \"set dyn_seg /incr\".")
      .def(
        "set_dyn_seg_mip",
        &shop_commander::set_dyn_seg_mip,
        (py::arg("self")),
        "Shop command string \"set dyn_seg /mip\".")
      .def(
        "set_max_num_threads",
        &shop_commander::set_max_num_threads,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set max_num_threads <value>\".")
      .def(
        "set_parallel_mode_auto",
        &shop_commander::set_parallel_mode_auto,
        (py::arg("self")),
        "Shop command string \"set parallel_mode /auto\".")
      .def(
        "set_parallel_mode_deterministic",
        &shop_commander::set_parallel_mode_deterministic,
        (py::arg("self")),
        "Shop command string \"set parallel_mode /deterministic\".")
      .def(
        "set_parallel_mode_opportunistic",
        &shop_commander::set_parallel_mode_opportunistic,
        (py::arg("self")),
        "Shop command string \"set parallel_mode /opportunistic\".")
      .def(
        "set_capacity_all",
        &shop_commander::set_capacity_all,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set capacity /all <value>\".")
      .def(
        "set_capacity_gate",
        &shop_commander::set_capacity_gate,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set capacity /gate <value>\".")
      .def(
        "set_capacity_bypass",
        &shop_commander::set_capacity_bypass,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set capacity /bypass <value>\".")
      .def(
        "set_capacity_spill",
        &shop_commander::set_capacity_spill,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set capacity /spill <value>\".")
      .def(
        "set_headopt_feedback",
        &shop_commander::set_headopt_feedback,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set headopt_feedback <value>\".")
      .def(
        "set_timelimit",
        &shop_commander::set_timelimit,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set timelimit <value>\".")
      .def(
        "set_dyn_flex_mip",
        &shop_commander::set_dyn_flex_mip,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set dyn_flex_mip <value>\".")
      .def(
        "set_universal_mip_on",
        &shop_commander::set_universal_mip_on,
        (py::arg("self")),
        "Shop command string \"set universal_mip /on\".")
      .def(
        "set_universal_mip_off",
        &shop_commander::set_universal_mip_off,
        (py::arg("self")),
        "Shop command string \"set universal_mip /off\".")
      .def(
        "set_universal_mip_not_set",
        &shop_commander::set_universal_mip_not_set,
        (py::arg("self")),
        "Shop command string \"set universal_mip /not_set\".")
      .def("set_merge_on", &shop_commander::set_merge_on, (py::arg("self")), "Shop command string \"set merge /on\".")
      .def(
        "set_merge_off", &shop_commander::set_merge_off, (py::arg("self")), "Shop command string \"set merge /off\".")
      .def(
        "set_merge_stop",
        &shop_commander::set_merge_stop,
        (py::arg("self")),
        "Shop command string \"set merge /stop\".")
      .def(
        "set_com_dec_period",
        &shop_commander::set_com_dec_period,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set com_dec_period <value>\".")
      .def(
        "set_time_delay_unit_hour",
        &shop_commander::set_time_delay_unit_hour,
        (py::arg("self")),
        "Shop command string \"set time_delay_unit hour\".")
      .def(
        "set_time_delay_unit_minute",
        &shop_commander::set_time_delay_unit_minute,
        (py::arg("self")),
        "Shop command string \"set time_delay_unit minute\".")
      .def(
        "set_time_delay_unit_time_step_length",
        &shop_commander::set_time_delay_unit_time_step_length,
        (py::arg("self")),
        "Shop command string \"set time_delay_unit time_step_length\".")
      .def(
        "set_power_head_optimization",
        &shop_commander::set_power_head_optimization,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"set power_head_optimization /on|/off\".")
      .def(
        "set_bypass_loss",
        &shop_commander::set_bypass_loss,
        (py::arg("self"), py::arg("on")),
        "Shop command string \"set bypass_loss /on|/off\".")
      .def(
        "set_reserve_slack_cost",
        &shop_commander::set_reserve_slack_cost,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set reserve_slack_cost <value>\".")
      .def(
        "set_fcr_n_band",
        &shop_commander::set_fcr_n_band,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set fcr_n_band <value>\".")
      .def(
        "set_fcr_d_band",
        &shop_commander::set_fcr_d_band,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set fcr_d_band <value>\".")
      .def(
        "set_fcr_n_equality",
        &shop_commander::set_fcr_n_equality,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set fcr_n_equality <value>\".")
      .def(
        "set_droop_discretization_limit",
        &shop_commander::set_droop_discretization_limit,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set droop_discretization_limit <value>\".")
      .def(
        "set_reserve_ramping_cost",
        &shop_commander::set_reserve_ramping_cost,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set reserve_ramping_cost <value>\".")
      .def(
        "set_gen_turn_off_limit",
        &shop_commander::set_gen_turn_off_limit,
        (py::arg("self"), py::arg("value")),
        "Shop command string \"set gen_turn_off_limit <value>\".")
      .def(
        "set_prod_from_ref_prod",
        &shop_commander::set_prod_from_ref_prod,
        (py::arg("self")),
        "Shop command string \"set prod_from_ref_prod\".")

      ;
  }

  // Utility functions for Python wrapping of generic shop_system functions that streams
  // data into an std::ostream reference (by default std::cout) given as last argument,
  // to collect the data and return as a string, which is easier to work with from Python.
  template <class T, class Func, class... Args>
  auto string_wrap_streaming_function(T const &obj, Func func, Args... args) {
    std::ostringstream stream;
    (obj.*func)(args..., stream);
    return stream.str();
  }

  template <class Func, class... Args>
  auto string_wrap_streaming_function(Func func, Args... args) {
    std::ostringstream stream;
    func(args..., stream);
    return stream.str();
  }

  void expose_shop_optimize(
    stm_system &stm,
    shyft::core::utctime t0,
    shyft::core::utctime t_end,
    shyft::core::utctimespan dt,
    std::vector<shop_command> const &cmds,
    bool log_to_stdstream,
    bool log_to_files) {
    calendar utc;
    std::size_t n_steps = shyft::core::utcperiod(t0, t_end).diff_units(utc, dt);
    time_axis::generic_dt ta{t0, dt, n_steps};
    return shop_system::optimize(stm, ta, cmds, log_to_stdstream, log_to_files);
  }

  void expose_shop_optimize_ta(
    stm_system &stm,
    shyft::time_axis::generic_dt ta,
    std::vector<shop_command> const &cmds,
    bool log_stream,
    bool log_file) {
    return shop_system::optimize(stm, ta, cmds, log_stream, log_file);
  }

  struct shop_system_ext {
    static shop_system *create_from_period_and_dt(shyft::core::utcperiod p, shyft::core::utctimespan dt) {
      calendar utc;
      std::size_t n_steps = p.diff_units(utc, dt);
      time_axis::generic_dt ta{p.start, dt, n_steps};
      return new shop_system(ta);
    }
  };

  static void expose_shop_system() {

    py::class_<shop_system, py::bases<>, std::shared_ptr<shop_system>, boost::noncopyable>(
      "ShopSystem", "A shop system, managing a session to the shop core.", py::no_init)
      .def(
        "__init__",
        py::make_constructor(&shop_system_ext::create_from_period_and_dt, py::default_call_policies()),
        "Create shop system, initialize with fixed time resolution. note that you should provide valid period and "
        "reasonable dt\n")
      .def(py::init< generic_dt const &>((py::arg("time_axis")), "Create shop system, initialize with time axis."))
      .def_readonly("commander", &shop_system::commander, "Get shop commander object.\n")
      .def(
        "get_version_info",
        &shop_system::get_version_info,
        (py::arg("self")),
        "Get version information from the shop core.")
      .def(
        "set_logging_to_stdstreams", &shop_system::set_logging_to_stdstreams, (py::arg("self"), py::arg("on") = true))
      .def("set_logging_to_files", &shop_system::set_logging_to_files, (py::arg("self"), py::arg("on") = true))
      .def("get_log_buffer", &shop_system::get_log_buffer, (py::arg("self"), py::arg("limit")))
      .def(
        "get_log_buffer",
        +[](shop_system &o) {
          return o.get_log_buffer();
        },
        (py::arg("self")))
      .def(
        "emit", &shop_system::emit, (py::arg("self"), py::arg("stm_system")), "Emit a stm system into the shop core.")
      .def("collect", &shop_system::collect, (py::arg("self"), py::arg("stm_system")))
      .def("complete", &shop_system::complete, (py::arg("self"), py::arg("stm_system")))
      .def(
        "optimize2",
        &expose_shop_optimize,
        (py::arg("stm_system"),
         py::arg("time_begin"),
         py::arg("time_end"),
         py::arg("time_step"),
         py::arg("commands"),
         py::arg("logging_to_stdstreams"),
         py::arg("logging_to_files")))
      .def(
        "optimize",
        &expose_shop_optimize_ta,
        (py::arg("stm_system"),
         py::arg("time_axis"),
         py::arg("commands"),
         py::arg("logging_to_stdstreams"),
         py::arg("logging_to_files")))
      .def(
        "export_yaml_string",
        &shop_system::export_yaml,
        (py::arg("self"),
         py::arg("input_only") = false,
         py::arg("compress_txy") = false,
         py::arg("compress_connection") = false))
      .def(
        "export_topology_string",
        +[](shop_system const &o, bool all, bool raw) -> string {
          return string_wrap_streaming_function(o, &shop_system::export_topology, all, raw);
        },
        (py::arg("self"), py::arg("all") = false, py::arg("raw") = false))
      .def(
        "export_topology",
        +[](shop_system const &o, bool all, bool raw) {
          o.export_topology(all, raw, std::cout);
        },
        (py::arg("self"), py::arg("all") = false, py::arg("raw") = false))
      .def(
        "export_data_string",
        +[](shop_system const &o, bool all) -> string {
          return string_wrap_streaming_function(o, &shop_system::export_data, all);
        },
        (py::arg("self"), py::arg("all") = false))
      .def(
        "export_data",
        +[](shop_system const &o, bool all) {
          o.export_data(all, std::cout);
        },
        (py::arg("self"), py::arg("all") = false))
      .def("command_raw", &shop_system::command_raw, (py::arg("self"), py::arg("command")))
      .def("get_executed_commands_raw", &shop_system::get_executed_commands_raw, (py::arg("self")))
      .def(
        "environment_string",
        +[] {
          return string_wrap_streaming_function(&shop_system::environment);
        },
        "Get all environment variables as a newline delimited string value.")
      .staticmethod("environment_string")
      .def(
        "environment",
        +[] {
          shop_system::environment(std::cout);
        },
        "Print all environment variables to standard output.")
      .staticmethod("environment");
  }

#endif

  void pyexport() {
    py::docstring_options doc_options(true, true, false); // all except c++ signatures
    py::scope().attr("__doc__") = "Statkraft Energy Market short term model Shop adapter";
    py::scope().attr("__version__") = shyft::_version_string();
    expose_shop_command();
#if defined(SHYFT_WITH_SHOP)
    py::scope().attr("shyft_with_shop") = true;
    py::scope().attr("shop_api_version") = BOOST_PP_STRINGIZE(SHOP_API_VERSION);
    expose_shop_system();
    expose_shop_commander();
#else
    py::scope().attr("shyft_with_shop") = false;
    py::scope().attr("shop_api_version") = "";
#endif
  }

}

BOOST_PYTHON_MODULE(_shop) {
  shyft::energy_market::stm::shop::pyexport();
}
