/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/bindings.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/api/reflection.h>
#include <shyft/energy_market/stm/urls.h>

namespace shyft::energy_market::stm {
  void pyexport_url_error() {
    auto e = py::class_<url_resolve_error>("UrlResolveError", "Url resolve error");
    shyft::pyapi::expose_members(e);
    shyft::pyapi::expose_format(e);
  }
}