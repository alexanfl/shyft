/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <fmt/core.h>

#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  void pyexport_model_contract() {
    auto c = py::class_<contract, py::bases<id_base>, std::shared_ptr<contract>, boost::noncopyable>(
      "Contract",
      doc.intro("A contract between two parties, seller and buyer, for sale of a product at a given price.")
        .intro("The contract can refer to specific power-plant, unit group, or even other contracts.")(),
      py::no_init);
    c.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_system> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
            "Create contract with unique id and name for a stm system."))
      .add_property(
        "tag",
        +[](contract const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly("power_plants", &contract::power_plants, "PowerPlantList: List of associated power plants.")
      .def_readonly("relations", &contract::relations, "_ContractRelationList: List of related contracts.")
      .def_readonly("constraint", &contract::constraint, "_Constraint: Constrant of this contract.")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "flattened_attributes",
        +[](contract &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.")
      .def(
        "get_portfolios",
        &contract::get_portfolios,
        "Get any portfolios this contract is associated with. Convenience for search in ContractPortfolio.contracts.")
      .def(
        "add_to_portfolio",
        &contract::add_to_portfolio,
        "Add this contract to specified portfolio. Convenience for appending to ContractPortfolio.contracts.")
      .def(
        "get_market_areas",
        &contract::get_energy_market_areas,
        "Get any energy market areas this contract is associated with. Convenience for search in MarketArea.contracts.")
      .def(
        "add_to_market_area",
        &contract::add_to_energy_market_area,
        "Add this contract to specified energy market area. Convenience for appending to MarketArea.contracts.")
      .def(
        "add_relation",
        &contract::add_relation,
        (py::arg("self"), py::arg("id"), py::arg("contract"), py::arg("relation_type")),
        doc.intro("Add a contract as a relation from this contract")
          .parameters()
          .parameter("id", "Id", "The relation id (must be unique for this contract)")
          .parameter("contract", "Contract", "The contract to be added as a relation")
          .parameter("relation_type", "RelationType", "A free to use integer to describe relation type")())
      .def(
        "remove_relation",
        &contract::remove_relation,
        (py::arg("self"), py::arg("contract_relation")),
        doc.intro("Remove relation to contract.")
          .parameters()
          .parameter("contract_relation", "ContractRelation", "The relation to be removed")())
      .def(
        "find_related_to_this",
        &contract::find_related_to_this,
        (py::arg("self")),
        doc.intro("Find contracts that have a relation to self.")
          .returns("contracts", "ContractVector", "The contracts that have a relation to this contract")());
    shyft::pyapi::expose_format(c);
    expose::expose_custom_maps(c);
    add_proxy_property(
      c,
      "quantity",
      contract,
      quantity,
      "_ts: [J/s] Contract quantity, in rate units, so that integrated over contract period gives total volume.");
    add_proxy_property(c, "price", contract, price, "_ts: [money/J] Contract price.");
    add_proxy_property(
      c,
      "fee",
      contract,
      fee,
      "_ts: [money/s] Any contract fees, in rate units so that fee over the contract period gives total fee.");
    add_proxy_property(
      c,
      "revenue",
      contract,
      revenue,
      "_ts: [money/s] Calculated revenue, in rate units, so that integrated of the contract period gives total revenue "
      "volume");
    add_proxy_property(
      c,
      "parent_id",
      contract,
      parent_id,
      "_string: Optional reference to parent (contract). Typically for forwardes/futures, that is splitted into "
      "shorter terms as time for the delivery is approaching.");
    add_proxy_property(c, "active", contract, active, "_ts: Contract status (dead/alive).");
    add_proxy_property(
      c,
      "validation",
      contract,
      validation,
      "_ts: Validation status (and whether the contract has been validated or not).");
    add_proxy_property(c, "buyer", contract, buyer, "_string: The name of the buyer party of the contract.");
    add_proxy_property(c, "seller", contract, seller, "_string: The name of the seller party of the contract.");
    add_proxy_property(
      c, "options", contract, options, "_t_xy_: Defines optional price/volume curves for future trading.");

    {
      py::scope scope_contract = c;

      auto cc = py::class_<contract::constraint_, py::bases<>, boost::noncopyable>(
        "_Constraint", doc.intro("Contract.Constraint attributes")(), py::no_init);
      _add_proxy_property(
        cc,
        "min_trade",
        contract::constraint_,
        min_trade,
        "_ts: Minimum quantity (volume) that must be traded for this contract.");
      _add_proxy_property(
        cc,
        "max_trade",
        contract::constraint_,
        max_trade,
        "_ts: Maximum quantity (volume) that must be traded for this contract.");
      _add_proxy_property(
        cc,
        "ramping_up",
        contract::constraint_,
        ramping_up,
        "_ts: Max quantity (volume) to ramp up between timesteps for this contract.");
      _add_proxy_property(
        cc,
        "ramping_down",
        contract::constraint_,
        ramping_down,
        "_ts: Max quantity (volume) to ramp down between timesteps for this contract.");
      _add_proxy_property(
        cc,
        "ramping_up_penalty_cost",
        contract::constraint_,
        ramping_up_penalty_cost,
        "_ts: Penalty for violating ramping up limit.");
      _add_proxy_property(
        cc,
        "ramping_down_penalty_cost",
        contract::constraint_,
        ramping_down_penalty_cost,
        "_ts: Penalty for violating ramping down limit.");
    }

    expose::expose_vector_eq<std::shared_ptr<contract>>(
      "ContractList",
      "A strongly typed list of Contract.",
      &equal_attribute<std::vector<std::shared_ptr<contract>>>,
      false);

    auto cp = py::class_<contract_portfolio, py::bases<>, std::shared_ptr<contract_portfolio>, boost::noncopyable>(
      "ContractPortfolio",
      doc.intro("Stm contract portfolio represents a set of contracts, so that the sum of interesting contract "
                "properties can be evaluated and compared.")(),
      py::no_init);
    cp.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_system> &>(
             (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
             "Create contract portfolio with unique id and name for a stm system."))
      .def_readwrite("id", &contract_portfolio::id, "int: Unique id for this object.")
      .def_readwrite("name", &contract_portfolio::name, "str: Name for this object.")
      .def_readwrite("json", &contract_portfolio::json, "str: Json keeping any extra data for this object.")
      .add_property(
        "tag",
        +[](contract_portfolio const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly("contracts", &contract_portfolio::contracts, "ContractList: List of contracts.")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "flattened_attributes",
        +[](contract_portfolio &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(cp);
    expose::expose_custom_maps(cp);
    add_proxy_property(
      cp,
      "quantity",
      contract_portfolio,
      quantity,
      "_ts: [J/s] normally sum of contracts, unit depends on the contracts.");
    add_proxy_property(
      cp, "fee", contract_portfolio, fee, "_ts: [money/s] Fees of the portfolio, normally sum of contracts.");
    add_proxy_property(cp, "revenue", contract_portfolio, revenue, "_ts: [money/s] Calculated revenue.");

    expose::expose_vector_eq<std::shared_ptr<contract_portfolio>>(
      "ContractPortfolioList",
      "A strongly typed list of ContractPortfolio.",
      &equal_attribute<std::vector<std::shared_ptr<contract_portfolio>>>,
      false);
    {
      py::scope sc = c;
      auto cr = py::class_<contract_relation, py::bases<>, std::shared_ptr<contract_relation>, boost::noncopyable>(
        "_ContractRelation",
        doc.intro("A relation to another contract, where the relation-type is a user specified integer.")
          .intro("This allows building and maintaining contract system that have internal rules/constraints")
          .intro("There is a minimal set of rules, like avoiding circularities that are enforced")(),
        py::no_init);

      cr.def(py::self == py::self);
      cr.def(py::self != py::self);
      cr.def_readonly("related", &contract_relation::related, doc.intro("Contract: The related contract")());
      cr.def_readonly("id", &contract_relation::id, doc.intro("int: The id of the relation")());
      cr.add_property(
        "owner", &contract_relation::owner_, doc.intro("Contract: ref. to the contract that owns this relation")());

      add_proxy_property(
        cr,
        "relation_type",
        contract_relation,
        relation_type,
        doc.intro("_u16: Free to use integer to describe relation type")());
      shyft::pyapi::expose_format(cr);

      using ContractRelationList = std::vector<std::shared_ptr<contract_relation>>;
      auto crl = py::class_<ContractRelationList>(
        "_ContractRelationList", "A strongly typed list of Contracts._ContractRelation.");
      crl.def(py::vector_indexing_suite<ContractRelationList, true>())
        .def(
          "__init__",
          expose::construct_from<ContractRelationList>(py::default_call_policies(), (py::arg("contract_relation."))),
          "Construct from list.");
      shyft::pyapi::expose_format(crl);
    }
  }
}
