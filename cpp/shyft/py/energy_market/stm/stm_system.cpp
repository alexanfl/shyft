/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  using shyft::time_series::dd::apoint_ts;

  struct stm_sys_ext {
    static auto to_blob(std::shared_ptr<stm_system> const & m) {
      auto s = stm_system::to_blob(m);
      return std::vector<char>(s.begin(), s.end());
    }

    static auto from_blob(std::vector<char>& blob) {
      std::string s(blob.begin(), blob.end());
      return stm_system::from_blob(s);
    }

    static auto create_power_module(
      std::shared_ptr<stm_system>& sys,
      int id,
      std::string const & name,
      std::string const & json) {
      return stm_builder(sys).create_power_module(id, name, json);
    }

    static auto
      create_network(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_network(id, name, json);
    }

    static auto
      create_wind_farm(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_wind_farm(id, name, json);
    }

    static auto
      create_market_area(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_market_area(id, name, json);
    }
    static auto
      create_hps(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_hydro_power_system(id, name, json);
    }
    static bool patch(std::shared_ptr<stm_system>& sys, stm_patch_op op, std::shared_ptr<stm_system> const & p) {
      return p ? stm::patch(sys, op, *p) : false;
    }
    static bool fix_uplinks(std::shared_ptr<stm_system>&sys) {
      return stm_system::fix_uplinks(sys);
    }
  };

  void pyexport_model_stm_system() {
    auto ma = py::class_<energy_market_area, py::bases<id_base>, std::shared_ptr<energy_market_area>, boost::noncopyable >(
      "MarketArea",
      doc.intro("A market area, with load/price and other properties.")
        .details("Within the market area, there are zero or more energy-\n"
                 "producing/consuming units.")(),
      py::no_init);
    ma
      .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_system>&>(
        (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("stm_sys")), "Create market area for a stm system."))
      .add_property(
        "system",
        +[](energy_market_area const & self) {
          return self.sys_();
        },
        "StmSystem: The owning/parent system that keeps this area.")
      .add_property(
        "tag",
        +[](energy_market_area const & self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .add_property(
        "unit_group",
        &energy_market_area::get_unit_group,
        &energy_market_area::set_unit_group,
        "UnitGroup: Getter and setter for unit group.")
      .def_readonly("contracts", &energy_market_area::contracts, "ContractList: List of contracts.")
      .def_readonly("busbars", &energy_market_area::busbars, "BusbarList: List of busbars.")
      .def_readonly("demand", &energy_market_area::demand, "_Offering: The demand side describing the buyers of energy")
      .def_readonly(
        "supply", &energy_market_area::supply, "_Offering: The supply side describing the producers of energy")
      .def(
        "remove_unit_group",
        &energy_market_area::remove_unit_group,
        (py::arg("self")),
        doc.intro("Remove the unit group associated with this market if available.")
          .parameters()
          .returns("unit_group", "UnitGroup", "The newly removed unit group.")())
      .def(
        "transmission_lines_to",
        &energy_market_area::transmission_lines_to,
        (py::arg("self"), py::arg("to_market")),
        doc.intro("Get transmission lines (from this) to MarketArea")
          .parameters()
          .parameter("to", "MarketArea", "MarketArea to get transmission lines to")
          .returns("transmission_line_list", "TransmissionLineList", "List of transmission lines to MarketArea")())
      .def(
        "transmission_lines_from",
        &energy_market_area::transmission_lines_from,
        (py::arg("self"), py::arg("from_market")),
        doc.intro("Remove the unit group associated with this market if available.")
          .parameters()
          .parameter("from", "MarketArea", "MarketArea to get transmission lines from")
          .returns("transmission_line_list", "TransmissionLineList", "List of transmission lines from MarketArea")())
      .def(
        "create_busbar_derived_unit_group",
        &energy_market_area::create_busbar_derived_unit_group,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("Add a unit-group of type 'production' to the system and to the market area.")
          .intro("The unit group will derive its units from the associated network busbars.")
          .intro("Units are automatically added/removed if busbars are added/removed on the network.")
          .parameters()
          .parameter("id", "int", "Unique id of the group.")
          .parameter("name", "str", "Unique name of the group.")
          .parameter("json", "str", "Json payload for py customization.")
          .returns("unit_group", "UnitGroup", "The newly created unit-group.")())
      .def(
        "get_production",
        &energy_market_area::get_production,
        (py::arg("self")),
        doc.intro("Get the sum of production contribution for all busbars")
          .parameters()
          .returns(
            "production", "TsTripletResult", "Production contribution sum, as triplet: result,realised,schedule")())
      .def(
        "get_consumption",
        &energy_market_area::get_consumption,
        (py::arg("self")),
        doc.intro("Get the sum of consumption contribution for all busbars")
          .parameters()
          .returns(
            "consumption", "TsTripletResult", "Consumption contribution sum, as triplet: result,realised,schedule")())
      .def(
        "get_import",
        &energy_market_area::get_import,
        (py::arg("self")),
        doc.intro("Get the total import of the market area.")
          .intro("If the actual sum of busbar flow is negative, import is 0.")
          .parameters()
          .returns("import", "TsTripletResult", "Market area import, as triplet: result,realised,schedule")())
      .def(
        "get_export",
        &energy_market_area::get_export,
        (py::arg("self")),
        doc.intro("Get the total export of the market area. (converted to positive value)")
          .intro("If the actual sum of busbar flow is positive, export is 0.")
          .parameters()
          .returns("export", "TsTripletResult", "Market area export, as triplet: result,realised,schedule")())
      .def(
        "flattened_attributes",
        +[](energy_market_area& self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.")
      .def(py::self == py::self)
      .def(py::self != py::self);
    expose::expose_custom_maps(ma);
    shyft::pyapi::expose_format(ma);
    add_proxy_property(ma, "load", energy_market_area, load, "_ts: [W] Load.");
    add_proxy_property(ma, "price", energy_market_area, price, "_ts: [Money/J] Price.");
    add_proxy_property(ma, "max_buy", energy_market_area, max_buy, "_ts: [W] Maximum buy.");
    add_proxy_property(ma, "max_sale", energy_market_area, max_sale, "_ts: [W] Maximum sale.");
    add_proxy_property(ma, "buy", energy_market_area, buy, "_ts: [W] Buy result.");
    add_proxy_property(ma, "sale", energy_market_area, sale, "_ts: [W] Sale result.");
    add_proxy_property(ma, "production", energy_market_area, production, "_ts: [W] Production result.");
    add_proxy_property(
      ma,
      "reserve_obligation_penalty",
      energy_market_area,
      reserve_obligation_penalty,
      "_ts: [Money/x] Obligation penalty.");
    {
      py::scope offering{ma};
      auto ts3 = py::class_<energy_market_area::ts_triplet_, py::bases<>, boost::noncopyable>(
        "_TsTriplet", doc.intro("describes .realised, .schedule and .result time-series")(), py::no_init);
      _add_proxy_property(
        ts3,
        "realised",
        energy_market_area::ts_triplet_,
        realised,
        "_ts: SI-units, realised time-series, as in historical fact");
      _add_proxy_property(
        ts3, "schedule", energy_market_area::ts_triplet_, schedule, "_ts: SI-units, schedule, as in current schedule");
      _add_proxy_property(
        ts3, "result", energy_market_area::ts_triplet_, result, "_ts: SI-units, result, as provided by optimisation");
      shyft::pyapi::expose_format(ts3);

      auto offr = py::class_<energy_market_area::offering_, py::bases<>, boost::noncopyable>(
        "_Offering", doc.intro("describes actors (supply/demand) offering into the market")(), py::no_init);
      offr
        .def_readonly(
          "usage", &energy_market_area::offering_::usage, "_TsTriplet: The amount consumed/used of the offering")
        .def_readonly(
          "price",
          &energy_market_area::offering_::price,
          "_TsTriplet: Given the usage, the effective price based on bids");
      shyft::pyapi::expose_format(offr);
      _add_proxy_property(
        offr, "bids", energy_market_area::offering_, bids, "_t_xy_: time dep x,y, x=price[Money/J],y=amount[W]");

      py::class_<energy_market_area::ts_triplet_result, py::bases<>>(
        "TsTripletResult", doc.intro("describes .realised, .schedule and .result time-series")(), py::no_init)
        .def_readonly(
          "realised",
          &energy_market_area::ts_triplet_result::realised,
          "_ts: SI-units, realised time-series, as in historical fact")
        .def_readonly(
          "schedule",
          &energy_market_area::ts_triplet_result::schedule,
          "_ts: SI-units, schedule, as in current schedule")
        .def_readonly(
          "result",
          &energy_market_area::ts_triplet_result::result,
          "_ts: SI-units, result, as provided by optimisation");
    }

    expose::expose_vector_eq<std::shared_ptr<energy_market_area>>(
      "MarketAreaList",
      "A strongly typed list of MarketArea.",
      &equal_attribute<std::vector<std::shared_ptr<energy_market_area>>>,
      false);

    auto ss = py::class_<stm_system, py::bases<>, std::shared_ptr<stm_system>, boost::noncopyable>(
      "StmSystem", "A complete stm system, with market areas, and hydro power systems.", py::no_init);
    ss.def(py::init<int, std::string const &, std::string const &>(
             (py::arg("uid"), py::arg("name"), py::arg("json")), "Create stm system."))
      .def_readwrite("id", &stm_system::id, "int: Unique id for this object.")
      .def_readwrite("name", &stm_system::name, "str: Name for this object.")
      .def_readwrite("json", &stm_system::json, "str: Json keeping any extra data for this object.")
      .def_readonly("market_areas", &stm_system::market, "MarketAreaList: List of market areas.")
      .def_readonly("contracts", &stm_system::contracts, "ContractList: List of contracts.")
      .def_readonly(
        "contract_portfolios", &stm_system::contract_portfolios, "ContractPortfolioList: List of contract portfolios.")
      .def_readonly("hydro_power_systems", &stm_system::hps, "HydroPowerSystemList: List of hydro power systems.")
      .def_readonly("power_modules", &stm_system::power_modules, "PowerModuleList: List of power modules.")
      .def_readonly("networks", &stm_system::networks, "NetworkList: List of networks.")
      .def_readonly("wind_farms", &stm_system::wind_farms, "WindFarmList: List of wind parks.")
      .def_readonly("summary", &stm_system::summary, "_OptimizationSummary: Key result values from simulations.")
      .def_readonly("run_parameters", &stm_system::run_params, "RunParameters: Run parameters.")
      .def_readonly(
        "unit_groups",
        &stm_system::unit_groups,
        doc.intro("UnitGroupList: Unit groups with constraints.")
          .intro("These groups holds a list of units, plus the constraints that apply to them.")
          .intro("During optimisation, the groups along with their constraints,")
          .intro("so that the optimization can take it into account when finding the")
          .intro("optimal solution.")())
      .def(
        "create_power_module",
        &stm_sys_ext::create_power_module,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm power module with unique uid.")
          .returns("power_module", "PowerModule", "The new power module.")())
      .def(
        "create_network",
        &stm_sys_ext::create_network,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm network with unique uid.").returns("network", "Network", "The new network.")())
      .def(
        "create_wind_farm",
        &stm_sys_ext::create_wind_farm,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm network with unique uid.").returns("wind_farm", "WindFarm", "The new wind farm.")())
      .def(
        "create_market_area",
        &stm_sys_ext::create_market_area,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create stm energy market area with unique uid.")
          .returns("market_area", "MarketArea", "The new market area.")())
      .def(
        "create_hydro_power_system",
        &stm_sys_ext::create_hps,
        (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = std::string("")),
        doc.intro("Create hydro power system with unique uid.")
          .returns("hps", "HydroPowerSystem", "The new hydro power system.")())
      .def(
        "add_unit_group",
        &stm_system::add_unit_group,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json"), py::arg("group_type") = unit_group_type{}),
        doc.intro("Add an empty named unit-group to the system.")
          .parameters()
          .parameter("id", "int", "Unique id of the group.")
          .parameter("name", "str", "Unique name of the group.")
          .parameter("json", "str", "Json payload for py customization.")
          .parameter("group_type", "int", "One of UnitGroupType values, default 0.")
          .returns("unit_group", "UnitGroup", "The newly created unit-group.")())
      .def(
        "result_ts_urls",
        +[](stm_system const * s, std::string prefix) {
          return srv::dstm::ts_url_generator(prefix, *s);
        },
        (py::arg("self"), py::arg("prefix")),
        doc.intro("Generate and return all result time series urls for the specified model.")
          .intro("Useful in the context of a dstm client, that have a get_ts method that accepts this list.")
          .parameters()
          .parameter("prefix", "", "Url prefix, like dstm://Mmodel_id.")
          .returns("ts_urls", "StringVector", "A strongly typed list of strings with ready to use ts-urls.")())
      .def(
        "to_blob",
        &stm_sys_ext::to_blob,
        (py::arg("self")),
        doc.intro("Serialize the model to a blob.").returns("blob", "ByteVector", "Blob form of the model.")())
      .def(
        "from_blob",
        &stm_sys_ext::from_blob,
        (py::arg("blob")),
        doc.intro("Re-create a stm system from a previously create blob.")
          .returns("stm_sys", "StmSystem", "A stm system including hydro-power-systems and markets.")())
      .staticmethod("from_blob")
      .def(
        "patch",
        &stm_sys_ext::patch,
        (py::arg("self"), py::arg("op"), py::arg("p")),
        doc.intro("Patch self with patch `p` using operation `op`.")
          .intro("If operation is ADD, the new objects in p that have id==0, will have auto assigned unique `id`s")
          .intro("For the value of the auto assigned `id` is max(obj.id)+1, in the order of appearance. ")
          .intro("side-effects: The patch `p` is updated with these new object id values.")
          .parameters()
          .parameter("op", "", "Operation is one of ADD,REMOVE_RELATIONS,REMOVE_OBJECTS")
          .parameter("p", "", "The StmSystem describing the patch")
          .returns("result", "", "True if patch operation and type was applied, false if not supported operation")())
      .def(
        "fix_uplinks",
        &stm_sys_ext::fix_uplinks,
        (py::arg("self")),
        doc.intro("Ensure all members(hps,contracts etc.) refers to this system to enable easy py navigation.")
          .intro("Usually only needed in cases where the stm-system is built without proper system refrences for child objects.")
          .intro("side-effects: Missing uplinks are fixed.")
          .returns("result", "", "True if anything was changed in uplinks")())
      .def(py::self == py::self)
      .def(py::self != py::self);
    shyft::pyapi::expose_format(ss);

    expose::expose_vector_eq<std::shared_ptr<stm_system>>(
      "StmSystemList",
      "Strongly typed list of StmSystem",
      &equal_attribute<std::vector<std::shared_ptr<stm_system>>>,
      false);
  }
}
