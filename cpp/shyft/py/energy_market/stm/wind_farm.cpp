/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <fmt/core.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_wind_farm() {
    auto w = py::class_<wind_farm, py::bases<id_base>, std::shared_ptr<wind_farm>, boost::noncopyable>(
      "WindFarm", "Stm wind park", py::no_init);
    w.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_system> &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
            "Create wind farm with unique id and name for a stm system."))
      .add_property(
        "system",
        +[](wind_farm const &self) {
          return self.sys_();
        },
        "StmSystem: The owning/parent system that keeps this windfarm.")

      .def_readonly("production", &wind_farm::production, "_Production: Production attributes.")
      .add_property(
        "tag",
        +[](wind_farm const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def("__eq__", &wind_farm::operator==)
      .def("__ne__", &wind_farm::operator!=)
      .def(
        "flattened_attributes",
        +[](wind_farm &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(w);
    expose::expose_custom_maps(w);
    {
      py::scope scope_unit = w;
      auto prod = py::class_<wind_farm::production_, py::bases<>, boost::noncopyable>(
        "_Production", doc.intro("WindFarm.production attributes, amount power produced[W, J/s].")(), py::no_init);
      shyft::pyapi::expose_format(prod);
      _add_proxy_property(
        prod,
        "forecast",
        wind_farm::production_,
        forecast,
        "_ts: [W, J/s] The forecast amount of power produced, time series.");
      _add_proxy_property(
        prod, "realised", wind_farm::production_, realised, "_ts: The current schedule, time series. [W, J/s]");
      _add_proxy_property(
        prod,
        "result",
        wind_farm::production_,
        result,
        "_ts: [W, J/s] Power produced. As computed by optimization/simulation process.");
    }
    expose::expose_vector_eq<std::shared_ptr<wind_farm>>(
      "WindFarmList",
      "A strongly typed list of WindFarm.",
      &equal_attribute<std::vector<std::shared_ptr<wind_farm>>>,
      false);
  }
}
