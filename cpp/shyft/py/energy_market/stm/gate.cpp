/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/bindings.h>

#include <fmt/core.h>

#include <shyft/energy_market/stm/waterway.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_gate() {
    auto g = py::class_<gate, py::bases<hydro_power::gate>, std::shared_ptr<gate>, boost::noncopyable>(
      "Gate", "Stm gate.", py::no_init);
    g.def_readonly("opening", &gate::opening, "_Opening: Opening attributes.")
      .def_readonly("discharge", &gate::discharge, "_Discharge: Discharge attributes.")
      .add_property(
        "tag",
        +[](gate const & self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")

      .def(py::self == py::self)
      .def(py::self != py::self)

      .def(
        "flattened_attributes",
        +[](gate& self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(g);
    expose::expose_custom_maps(g);
    add_proxy_property(
      g,
      "flow_description",
      gate,
      flow_description,
      doc.intro("_t_xy_z_list: Gate flow description. Flow [m^3/s] as a function of water level [m] for relative gate "
                "opening [%].")());

    add_proxy_property(
      g,
      "flow_description_delta_h",
      gate,
      flow_description_delta_h,
      doc.intro("_t_xy_z_list: Gate flow description. Flow [m^3/s] as a function of water level difference [m] for "
                "relative gate opening [%].")());

    add_proxy_property(g, "cost", gate, cost, doc.intro("_ts: Gate adjustment cost, time series.")());

    {
      py::scope scope_gate = g;

      auto go = py::class_<gate::opening_, py::bases<>, boost::noncopyable>("_Opening", py::no_init);
      go.def_readonly("constraint", &gate::opening_::constraint, "_Constraints: Opening constraint attributes.");

      _add_proxy_property(
        go, "schedule", gate::opening_, schedule, "_ts: Planned opening schedule, value between 0.0 and 1.0.");
      _add_proxy_property(
        go, "realised", gate::opening_, realised, "_ts:Historical opening schedule, value between 0.0 and 1.0.");
      _add_proxy_property(
        go, "result", gate::opening_, result, "_ts:Result opening schedule, value between 0.0 and 1.0.");

      shyft::pyapi::expose_format(go);

      auto gd = py::class_<gate::discharge_, py::bases<>, boost::noncopyable>("_Discharge", py::no_init);
      gd.def_readonly("constraint", &gate::discharge_::constraint, "_Constraints: Discharge constraint attributes.");

      _add_proxy_property(gd, "schedule", gate::discharge_, schedule, "_ts: [m3/s] Discharge schedule restriction.");
      _add_proxy_property(gd, "realised", gate::discharge_, realised, "_ts: [m3/s] Historical discharge restriction.");
      _add_proxy_property(gd, "result", gate::discharge_, result, "_ts: [m3/s] Discharge result.");
      _add_proxy_property(gd, "static_max", gate::discharge_, static_max, "_ts: [m3/s] Maximum discharge.");
      _add_proxy_property(
        gd,
        "merge_tolerance",
        gate::discharge_,
        merge_tolerance,
        "_ts: [m3/s] Maximum deviation in discharge between two timesteps.");

      shyft::pyapi::expose_format(gd);

      {
        py::scope gdc = go;
        auto gdcc = py::class_<gate::opening_::constraint_, py::bases<>, boost::noncopyable>(
          "_Constraints", py::no_init);
        shyft::pyapi::expose_format(gdcc);
        _add_proxy_property(
          gdcc,
          "positions",
          gate::opening_::constraint_,
          positions,
          "_t_xy_: Predefined gate positions mapped to gate opening.");
        _add_proxy_property(
          gdcc,
          "continuous",
          gate::opening_::constraint_,
          continuous,
          "_ts: Flag determining whether the gate can be set to anything between predefined positions, time-dependent "
          "attribute.");
      }

      {
        py::scope gdc = gd;
        auto gdcc = py::class_<gate::discharge_::constraint_, py::bases<>, boost::noncopyable>(
          "_Constraints", py::no_init);
        shyft::pyapi::expose_format(gdcc);
        _add_proxy_property(
          gdcc, "min", gate::discharge_::constraint_, min, "_ts: [masl] Discharge constraint minimum, time series.");
        _add_proxy_property(
          gdcc, "max", gate::discharge_::constraint_, max, "_ts: [masl] Discharge constraint maximum, time series.");
      }
    }
  }
}
