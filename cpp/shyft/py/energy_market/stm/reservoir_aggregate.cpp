/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_url_tag.h>

namespace shyft::energy_market::stm {

  void pyexport_model_reservoir_aggregate() {
    auto ra = py::class_<reservoir_aggregate, py::bases<id_base>, std::shared_ptr<reservoir_aggregate>, boost::noncopyable>(
      "ReservoirAggregate",
      doc.intro("A reservoir_aggregate keeping stm reservoirs.")
        .details("After creating the reservoirs, create the reservoir aggregate,\n"
                 "and add the reservoirs to it.")(),
      py::no_init);
    ra
      .add_property(
        "tag",
        +[](reservoir_aggregate const &self) {
          return expose::url_tag(self);
        },
        "str: Url tag.")
      .def_readonly("reservoirs", &reservoir_aggregate::reservoirs)
      .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<stm_hps> &>(
        (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
        "Create reservoir aggregate with unique id and name for a hydro power system."))
      .def(
        "add_reservoir",
        &reservoir_aggregate::add_reservoir,
        (py::arg("self"), py::arg("reservoir")),
        doc.intro("Adds a reservoir to the reservoir aggregate.")
          .parameters()
          .parameter("reservoir", "Reservoir", "The reservoir to be added to the reservoir aggregate")())
      .def(
        "remove_reservoir",
        &reservoir_aggregate::remove_reservoir,
        (py::arg("self"), py::arg("reservoir")),
        doc.intro("Remove a reservoir from the reservoir aggregate")
          .parameters()
          .parameter("reservoir", "Reservoir", "The reservoir to be removed from the reservoir aggregate")())
      .def_readonly("inflow", &reservoir_aggregate::inflow, "_Inflow: Inflow attributes.")
      .def_readonly("volume", &reservoir_aggregate::volume, "_Volume: Volume attributes.")
      .def("__eq__", &reservoir_aggregate::operator==)
      .def("__ne__", &reservoir_aggregate::operator!=)
      .def(
        "flattened_attributes",
        +[](reservoir_aggregate &self) {
          return expose::make_flat_attribute_dict(self);
        },
        "Flat dict containing all component attributes.");
    shyft::pyapi::expose_format(ra);

    {
      py::scope ra_scope = ra;
      auto rai = py::class_<reservoir_aggregate::inflow_, boost::noncopyable>("_Inflow", py::no_init);
      shyft::pyapi::expose_format(rai);

      _add_proxy_property(
        rai, "schedule", reservoir_aggregate::inflow_, schedule, "_ts: [m3/s] Inflow schedule, time series.");
      _add_proxy_property(
        rai, "realised", reservoir_aggregate::inflow_, realised, "_ts: [m3/s] Inflow realised, time series.");
      _add_proxy_property(
        rai, "result", reservoir_aggregate::inflow_, result, "_ts: [m3/s] Inflow result, time series.");

      auto rav = py::class_<reservoir_aggregate::volume_, boost::noncopyable>("_Volume", py::no_init);
      shyft::pyapi::expose_format(rav);
      _add_proxy_property(
        rav, "static_max", reservoir_aggregate::volume_, static_max, "_ts: [m3] Production static max, time series.");
      _add_proxy_property(
        rav, "schedule", reservoir_aggregate::volume_, schedule, "_ts: [m3] Production schedule, time series.");
      _add_proxy_property(
        rav, "realised", reservoir_aggregate::volume_, realised, "_ts: [m3] Production realised, time series.");
      _add_proxy_property(
        rav, "result", reservoir_aggregate::volume_, result, "_ts: [m3] Production result, time series.");
    }

    expose::expose_vector_eq<std::shared_ptr<reservoir_aggregate>>(
      "ReservoirAggregateList",
      "A strongly typed list of ReservoirAggregates.",
      &equal_attribute<std::vector<std::shared_ptr<reservoir_aggregate>>>,
      false);
  }

}
