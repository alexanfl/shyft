#pragma once
#include <string>
#include <iterator>

namespace expose {

  /** url_tag for exposing the T::generate_url(...) function to python.
   *
   * @tparam T is the object type, that must have the .generate_url(...) signature typically  reservoir, unit etc.
   *
   * @param o the object that is called to generate the url on
   * @param prefix a prefix added to the string  prior to the url-generation
   * @param levels default -1, number of levels to include
   * @param template_levels default -1, number of levels that should emit template-holders instead of the object-ids
   * etc.
   * @return the resulting url.
   */
  template <class T>
  std::string url_tag(T const & o, std::string const & prefix = "", int levels = -1, int template_levels = -1) {
    std::string res;
    res.reserve(100);
    auto rbi = std::back_inserter(res);
    std::copy(prefix.begin(), prefix.end(), rbi);
    o.generate_url(rbi, levels, template_levels);
    return res;
  }
}
