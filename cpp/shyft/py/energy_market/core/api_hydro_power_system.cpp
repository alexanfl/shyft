
#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/graph/graph_utilities.h>
#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/hydro_operations.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_object_ext.h>

namespace expose {

  using namespace shyft::energy_market;
  using namespace shyft::energy_market::hydro_power;

  void catchment_stuff() {
    using namespace shyft::energy_market::hydro_power;
    auto c =
      py::class_<catchment, py::bases<shyft::energy_market::id_base>, std::shared_ptr<catchment>, boost::noncopyable>(
        "Catchment",
        doc.intro("Catchment descriptive component, suitable for energy market long-term and/or short term managment.\n"
                  "This component usually would contain usable view of the much more details shyft.hydrology region "
                  "model\n")())
        .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<hydro_power_system> const &>(
          (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json"), py::arg("hps")), doc.intro("tbd")()))
        .add_property(
          "hps",
          &catchment::hps_,
          doc.intro("HydroPowerSystem: returns the hydro power system this Catchment is a part of")())

        .def(py::self == py::self)
        .def(py::self != py::self);
    shyft::pyapi::expose_format(c);
    expose_vector_eq<std::shared_ptr<catchment>>(
      "CatchmentList",
      "Strongly typed list of catchments",
      +[](std::vector<std::shared_ptr<catchment>> const &a, std::vector<std::shared_ptr<catchment>> const &b) -> bool {
        return equal_vector_ptr_content<catchment>(a, b);
      },
      false);
  }

  void water_route_stuff() {
    using namespace shyft::energy_market::hydro_power;
    // note: we use pattern +[](shrd_ptr,..)->{} to fixup const issues with self (and maybe other args) passed as
    // shrd_ptr const&(symptoms:weakptrs end up zero after return to python)
    auto wr =
      py::class_<waterway, py::bases<hydro_component>, std::shared_ptr<waterway>, boost::noncopyable>(
        "Waterway",
        doc.intro("The waterway can be a river or a tunnel, and connects the reservoirs, units(turbine).")())
        .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<hydro_power_system> const &>(
          (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json"), py::arg("hps")), doc.intro("tbd")()))
        .def_readonly(
          "downstream", &waterway::downstream, doc.intro("HydroComponent: returns downstream object(if any)")())
        .def_readonly("upstream", &waterway::upstream, doc.intro("HydroComponent: returns upstream object(if any)")())
        .add_property(
          "upstream_role",
          &waterway::upstream_role,
          doc.intro("the role the water way has relative to the component above")())
        .def(
          "add_gate",
          +[](std::shared_ptr<waterway> &ww, std::shared_ptr<gate> &g) -> void {
            waterway::add_gate(ww, g);
          },
          (py::arg("self"), py::arg("gate")),
          doc.intro("add a gate to the waterway")())
        .def(
          "remove_gate",
          &waterway::remove_gate,
          (py::arg("self"), py::arg("gate")),
          doc.intro("remove a gate from the waterway")())
        .def_readonly(
          "gates", &waterway::gates, doc.intro("GateList: the gates attached to the inlet of the waterway")())

        .def(
          "input_from",
          +[](std::shared_ptr<waterway> s, std::shared_ptr<waterway> o) {
            return waterway::input_from(s, o);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the input of this waterway to the output of the other waterway.")())
        .def(
          "input_from",
          +[](std::shared_ptr<waterway> s, std::shared_ptr<unit> u) {
            return waterway::input_from(s, u);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the input of this waterway to the output of the unit.")())
        .def(
          "input_from",
          +[](std::shared_ptr<waterway> s, std::shared_ptr<reservoir> rsv, connection_role r) {
            return waterway::input_from(s, rsv, r);
          },
          (py::arg("reservoir"), py::arg("other"), py::arg("role") = connection_role::main),
          doc.intro("Connect the input of this waterway to the output of the reservoir, and assign the connection "
                    "role.")())
        .def(
          "output_to",
          +[](std::shared_ptr<waterway> s, std::shared_ptr<waterway> w) {
            return waterway::output_to(s, w);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the output of this waterway to the input of the other waterway.")())
        .def(
          "output_to",
          +[](std::shared_ptr<waterway> s, std::shared_ptr<reservoir> r) {
            return waterway::output_to(s, r);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the output of this waterway to the input of the reservoir.")())
        .def(
          "output_to",
          +[](std::shared_ptr<waterway> s, std::shared_ptr<unit> u) {
            return waterway::output_to(s, u);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the output of this waterway to the input of the unit.")())
        .def(py::self == py::self)
        .def(py::self != py::self);
    shyft::pyapi::expose_format(wr);
    expose_vector_eq<std::shared_ptr<waterway>>(
      "WaterwayList",
      "Strongly typed list of waterways",
      +[](std::vector<std::shared_ptr<waterway>> const &a, std::vector<std::shared_ptr<waterway>> const &b) -> bool {
        return equal_vector_ptr_content<waterway>(a, b);
      },
      false);

    auto gt =
      py::class_<gate, py::bases<shyft::energy_market::id_base>, std::shared_ptr<gate>, boost::noncopyable>(
        "Gate",
        doc.intro(
          "A gate controls the amount of flow into the waterway by the gate-opening.\n"
          "In the case of tunnels, it's usually either closed or open.\n"
          "For reservoir flood-routes, the gate should be used to model the volume-flood characteristics.\n"
          "The resulting flow through a waterway is a function of many factors, most imporant:\n    \n"
          "    * gate opening and gate-characteristics\n"
          "    * upstream water-level\n"
          "    * downstrem water-level(in some-cases)\n"
          "    * waterway properties(might be state dependent)\n\n")())
        .def(py::init<int, std::string const &, std::string const &>(
          (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")), doc.intro("construct a new gate")()))

        .add_property("water_route", &gate::wtr_, doc.intro("Waterway: deprecated:use waterway")())
        .add_property(
          "waterway", &gate::wtr_, doc.intro("Waterway: ref. to the waterway where this gate controls the flow")())

        .def(py::self == py::self)
        .def(py::self != py::self);
    shyft::pyapi::expose_format(gt);
    expose_vector_eq<std::shared_ptr<gate>>(
      "GateList",
      "Strongly typed list of gates",
      +[](std::vector<std::shared_ptr<gate>> const &a, std::vector<std::shared_ptr<gate>> const &b) -> bool {
        return equal_vector_ptr_content<gate>(a, b);
      },
      false);
  }

  void reservoir_stuff() {
    using namespace shyft::energy_market::hydro_power;
    using py::self;
    auto rsv =
      py::class_<reservoir, py::bases<hydro_component>, std::shared_ptr<reservoir>, boost::noncopyable>(
        "Reservoir", doc.intro("")(), py::no_init)
        .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<hydro_power_system> const &>(
          (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json"), py::arg("hps")), doc.intro("tbd")()))
        .def(
          "input_from",
          +[](std::shared_ptr<reservoir> s, std::shared_ptr<waterway> w) {
            return reservoir::input_from(s, w);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the input of the reservoir to the output of the waterway.")())
        .def(
          "output_to",
          +[](std::shared_ptr<reservoir> s, std::shared_ptr<waterway> w, connection_role r) {
            return reservoir::output_to(s, w, r);
          },
          (py::arg("self"), py::arg("other"), py::arg("role") = connection_role::main),
          doc.intro("Connect the output of this reservoir to the input of the waterway, and assign the connection "
                    "role")())
        .def(py::self == py::self)
        .def(py::self != py::self);
    shyft::pyapi::expose_format(rsv);
    expose_vector_eq<std::shared_ptr<reservoir>>(
      "ReservoirList",
      "Strongly typed list of reservoirs",
      +[](std::vector<std::shared_ptr<reservoir>> const &a, std::vector<std::shared_ptr<reservoir>> const &b) -> bool {
        return equal_vector_ptr_content<reservoir>(a, b);
      },
      false);
  }

  void power_plant_stuff() {
    using namespace shyft::energy_market::hydro_power;
    using shyft::energy_market::id_base;

    auto u =
      py::class_<unit, py::bases<hydro_component>, std::shared_ptr<unit>, boost::noncopyable>(
        "Unit",
        doc.intro(
          "An Unit consist of a turbine and a connected generator.\n"
          "The turbine is hydrologically connected to upstream tunnel and downstream tunell/river.\n"
          "The generator part is connected to the electrical grid through a busbar.\n"
          ""
          "In the long term models, the entire power plant is represented by a virtual unit that represents\n"
          "the total capability of the power-plant.\n"
          "\n"
          "The short-term detailed models, usually describes every aggratate up to a granularity that is\n"
          "relevant for the short-term optimization/simulation horizont.\n"
          "\n"
          "A power plant is a collection of one or more units that are natural to group into one power plant.")())
        .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<hydro_power_system> const &>(
          (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json"), py::arg("hps")), doc.intro("tbd")()))
        .add_property(
          "downstream",
          &unit::downstream,
          doc.intro("Waterway: returns downstream waterway(river/tunnel) object(if any)")())
        .add_property(
          "upstream", &unit::upstream, doc.intro("Waterway: returns upstream tunnel(water-route) object(if any)")())
        .add_property("power_station", &unit::pwr_station_, doc.intro("PowerPlant: deprecated: use power_plant")())
        .add_property(
          "power_plant",
          &unit::pwr_station_,
          doc.intro("PowerPlant: return the hydro power plant associated with this unit")())
        .add_property(
          "is_pump", &unit::is_pump, doc.intro("bool: Returns true if the unit is a pump, otherwise, returns false")())
        .def(
          "input_from",
          +[](std::shared_ptr<unit> s, std::shared_ptr<waterway> w) {
            return unit::input_from(s, w);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the input of this unit to the output of the waterway.")())
        .def(
          "output_to",
          +[](std::shared_ptr<unit> s, std::shared_ptr<waterway> w) {
            return unit::output_to(s, w);
          },
          (py::arg("self"), py::arg("other")),
          doc.intro("Connect the output of this unit to the input of the waterway.")())
        .def(py::self == py::self)
        .def(py::self != py::self);
    shyft::pyapi::expose_format(u);
    expose_vector_eq<std::shared_ptr<unit>>(
      "UnitList",
      "Strongly typed list of units",
      +[](std::vector<std::shared_ptr<unit>> const &a, std::vector<std::shared_ptr<unit>> const &b) -> bool {
        return equal_vector_ptr_content<unit>(a, b);
      },
      false);

    auto pp =
      py::class_<power_plant, py::bases<shyft::energy_market::id_base>, std::shared_ptr<power_plant>, boost::noncopyable>(
        "PowerPlant",
        doc.intro("A hydro power plant is the site/building that contains a number of units.\n"
                  "The attributes of the power plant, are typically sum-requirement and/or operations that applies\n"
                  "all of the units."
                  "\n")())
        .def(py::init<int, std::string const &, std::string const &, std::shared_ptr<hydro_power_system> const &>(
          (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json"), py::arg("hps")), doc.intro("tbd")()))
        .add_property(
          "hps",
          &power_plant::hps_,
          doc.intro("HydroPowerSystem: returns the hydro-power-system this component is a part of")())
        .def_readwrite("units", &power_plant::units, doc.intro("UnitList: associated units")())
        .def(
          "add_unit",
          +[](std::shared_ptr<power_plant> pp, std::shared_ptr<unit> u) -> void {
            power_plant::add_unit(pp, u);
          }, // fixup const shrd ptr ref to python.
          (py::arg("self"), py::arg("unit")),
          doc.intro("add unit to plant")())
        .def(
          "remove_unit",
          &power_plant::remove_unit,
          (py::arg("self"), py::arg("unit")),
          doc.intro("remove unit from plant")())
        // bw compat
        .def_readwrite("aggregates", &power_plant::units, doc.intro("UnitList: deprecated! use units")())
        .def(
          "add_aggregate",
          +[](std::shared_ptr<power_plant> pp, std::shared_ptr<unit> u) -> void {
            power_plant::add_unit(pp, u);
          }, // fixup const shrd ptr ref to python.
          (py::arg("self"), py::arg("aggregate")),
          doc.intro("deprecated:use add_unit")())
        .def(
          "remove_aggregate",
          &power_plant::remove_unit,
          (py::arg("self"), py::arg("aggregate")),
          doc.intro("deprecated:use remove_unit")())
        .def(py::self == py::self)
        .def(py::self != py::self);
    shyft::pyapi::expose_format(pp);
    expose_vector_eq<std::shared_ptr<power_plant>>(
      "PowerPlantList",
      "Strongly typed list of PowerPlants",
      +[](std::vector<std::shared_ptr<power_plant>> const &a, std::vector<std::shared_ptr<power_plant>> const &b)
        -> bool {
        return equal_vector_ptr_content<power_plant>(a, b);
      },
      false);
  }

  void hydro_component_stuff() {
    using namespace shyft::energy_market::hydro_power;
    py::enum_<connection_role>("ConnectionRole")
      .value("main", connection_role::main)
      .value("bypass", connection_role::bypass)
      .value("flood", connection_role::flood)
      .value("input", connection_role::input)
      .export_values();
    typedef hydro_connection HydroConnection;
    auto hc =
      py::class_<HydroConnection, py::bases<>, HydroConnection, boost::noncopyable>(
        "HydroConnection",
        doc.intro("A hydro connection is the connection object that relate one hydro component to another.\n"
                  "A hydro component have zero or more hydro connections, contained in upstream and downstream lists.\n"
                  "If you are using the hydro system builder, there will always be a mutual/two way connection.\n"
                  "That is, if a reservoir connects downstream to a tunell (in role main), then the tunell will have\n"
                  "a upstream connection pointing to the reservoir (as in role input)\n")(),
        py::no_init)
        .def_readwrite("role", &HydroConnection::role, doc.intro("ConnectionRole: role like main,bypass,flood,input")())
        .add_property(
          "target",
          +[](HydroConnection const &h) {
            return h.target;
          },
          doc.intro("HydroComponent: target of the hydro-connection, Reservoir|Unit|Waterway")())
        .add_property("has_target", &HydroConnection::has_target, doc.intro("bool: true if valid/available target")());
    shyft::pyapi::expose_format(hc);
    typedef std::vector<HydroConnection> HydroConnectionList;
    py::class_<HydroConnectionList>("HydroConnectionList").def(py::vector_indexing_suite<HydroConnectionList>());


    typedef hydro_component HydroComponent;
    auto hyc =
      py::class_<
        HydroComponent,
        py::bases<shyft::energy_market::id_base>,
        std::shared_ptr<HydroComponent>,
        boost::noncopyable>(
        "HydroComponent",
        doc.intro("A hydro component keeps the common attributes and relational properties common for all components "
                  "that can contain water")(),
        py::no_init)
        .add_property(
          "hps",
          &HydroComponent::hps_,
          doc.intro("HydroPowerSystem: returns the hydro-power-system this component is a part of")())
        .def_readonly(
          "upstreams",
          &HydroComponent::upstreams,
          doc.intro("HydroComponentList: list of hydro-components that are conceptually upstreams")())
        .def_readonly(
          "downstreams",
          &HydroComponent::downstreams,
          doc.intro("HydroComponentList: list of hydro-components that are conceptually downstreams")())
        .def(
          "disconnect_from",
          &HydroComponent::disconnect_from,
          (py::arg("self"), py::arg("other")),
          doc.intro("disconnect from another component")())
        .def(
          "equal_structure",
          &HydroComponent::equal_structure,
          (py::arg("self"), py::arg("other")),
          doc.intro("Returns true if the `other` object have the same interconnections to the close neighbors as self.")
            .intro("The neighbors are identified by their `.id` attribute, and they must appear in the same role to "
                   "be considered equal.")
            .intro("E.g. if for a reservoir, a waterway is in role flood for "
                   "self, and in role bypass for other, they are different "
                   "connections.")
            .parameters()
            .parameter("other", "", "the other object, of same type, hydro component, to compare.")
            .returns("bool", "", "True if the other have same interconnections as self")());
    shyft::pyapi::expose_format(hyc);
    expose_vector_eq<std::shared_ptr<hydro_component>>(
      "HydroComponentList",
      "Strongly typed list of HydroComponents",
      +[](
         std::vector<std::shared_ptr<hydro_component>> const &a,
         std::vector<std::shared_ptr<hydro_component>> const &b) -> bool {
        return equal_vector_ptr_content<hydro_component>(a, b);
      },
      false);
  }

  struct hps_ext {
    static std::vector<char>
      to_blob(std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system> const &hps) {
      auto s = shyft::energy_market::hydro_power::hydro_power_system::to_blob(hps);
      return std::vector<char>(s.begin(), s.end());
    }

    static std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system> from_blob(std::vector<char> &blob) {
      std::string s(blob.begin(), blob.end());
      return shyft::energy_market::hydro_power::hydro_power_system::from_blob(s);
    }
  };

  void hydro_power_system_stuff() {
    using namespace shyft::energy_market::hydro_power;
    typedef hydro_power_system HydroPowerSystem;
    auto hps =
      py::class_<HydroPowerSystem, py::bases<shyft::energy_market::id_base>, std::shared_ptr<HydroPowerSystem>, boost::noncopyable>(
        "HydroPowerSystem", doc.intro("")(), py::no_init)
        .def(py::init<std::string>(
          (py::arg("name")), doc.intro("creates an empty hydro power system with the specified name")()))
        .def(py::init<int, std::string,py::optional<std::string>>(
          (py::arg("id"), py::arg("name"),py::arg("json")),
          doc.intro("creates a an empty new hydro power system with specified id and name and json str info")()))
        .def_readwrite(
          "created",
          &HydroPowerSystem::created,
          doc.intro("time: The time when this system was created(you should specify it when you create it)")())
        .def_readonly("water_routes", &HydroPowerSystem::waterways, "WaterwayList: deprecated:use waterways")
        .def_readonly(
          "waterways", &HydroPowerSystem::waterways, "WaterwayList: all the waterways(tunells,rivers) of the system")
        .add_property(
          "gates",
          +[](HydroPowerSystem const &h) {
            std::vector<std::shared_ptr<gate>> g;
            std::ranges::copy(h.gates(), std::back_inserter(g));
            return g;
          },
          "GateList: all the gates of the system")
        .def_readonly("aggregates", &HydroPowerSystem::units, "UnitList: deprecated: use units")
        .def_readonly("units", &HydroPowerSystem::units, "UnitList: all the hydropower units in this system")
        .def_readonly("reservoirs", &HydroPowerSystem::reservoirs, "ReservoirList: all the reservoirs")
        .def_readonly("catchments", &HydroPowerSystem::catchments, "CatchmentList: all the catchments for the system")
        .def_readonly("power_stations", &HydroPowerSystem::power_plants, "PowerPlantList: deprecated: use power_plant")
        .def_readonly(
          "power_plants",
          &HydroPowerSystem::power_plants,
          "PowerPlantList: all power plants, each with references to its units")
        .add_property(
          "model_area",
          &HydroPowerSystem::mdl_area_,
          +[](HydroPowerSystem &hps, std::shared_ptr<model_area> &ma) {
            hps.set_mdl_area(ma);
          }, // Boost.Python can't handle pass by const-reference, so we make a thin wrapper.
          doc.intro("ModelArea: returns the model area this hydro-power-system is a part of").see_also("ModelArea")())

        .def(
          "find_aggregate_by_name",
          &HydroPowerSystem::find_unit_by_name,
          (py::arg("self"), py::arg("name")),
          "deprecated: use find_unit_by_name")
        .def(
          "find_unit_by_name",
          &HydroPowerSystem::find_unit_by_name,
          (py::arg("self"), py::arg("name")),
          "returns object that exactly  matches name")
        .def(
          "find_reservoir_by_name",
          &HydroPowerSystem::find_reservoir_by_name,
          (py::arg("self"), py::arg("name")),
          "returns object that exactly  matches name")
        .def(
          "find_water_route_by_name",
          &HydroPowerSystem::find_waterway_by_name,
          (py::arg("self"), py::arg("name")),
          "deprecated:use find_waterway_by_name")
        .def(
          "find_waterway_by_name",
          &HydroPowerSystem::find_waterway_by_name,
          (py::arg("self"), py::arg("name")),
          "returns object that exactly  matches name")
        .def(
          "find_gate_by_name",
          &HydroPowerSystem::find_gate_by_name,
          (py::arg("self"), py::arg("name")),
          "returns object that exactly  matches name")
        .def(
          "find_power_station_by_name",
          &HydroPowerSystem::find_power_plant_by_name,
          (py::arg("self"), py::arg("name")),
          "deprecated:use find_power_plant_by_name")
        .def(
          "find_power_plant_by_name",
          &HydroPowerSystem::find_power_plant_by_name,
          (py::arg("self"), py::arg("name")),
          "returns object that exactly  matches name")
        .def(
          "find_aggregate_by_id",
          &HydroPowerSystem::find_unit_by_id,
          (py::arg("self"), py::arg("id")),
          "deprecated:use find_unit_by_id")
        .def(
          "find_unit_by_id",
          &HydroPowerSystem::find_unit_by_id,
          (py::arg("self"), py::arg("id")),
          "returns object with specified id")
        .def(
          "find_reservoir_by_id",
          &HydroPowerSystem::find_reservoir_by_id,
          (py::arg("self"), py::arg("id")),
          "returns object with specified id")
        .def(
          "find_water_route_by_id",
          &HydroPowerSystem::find_waterway_by_id,
          (py::arg("self"), py::arg("id")),
          "deprecated:use find_waterway_by_id")
        .def(
          "find_waterway_by_id",
          &HydroPowerSystem::find_waterway_by_id,
          (py::arg("self"), py::arg("id")),
          "returns object with specified id")
        .def(
          "find_gate_by_id",
          &HydroPowerSystem::find_gate_by_id,
          (py::arg("self"), py::arg("id")),
          "returns object with specified id")
        .def(
          "find_power_station_by_id",
          &HydroPowerSystem::find_power_plant_by_id,
          (py::arg("self"), py::arg("id")),
          "deprecated:use find_power_plant_by_id")
        .def(
          "find_power_plant_by_id",
          &HydroPowerSystem::find_power_plant_by_id,
          (py::arg("self"), py::arg("id")),
          "returns object with specified id")
        .def(
          "equal_structure",
          &HydroPowerSystem::equal_structure,
          (py::arg("self"), py::arg("other_hps")),
          doc.intro("returns true if equal structure of identified objects, using the .id, but not comparing .name, "
                    ".attributes etc., to the other")())
        .def(
          "equal_content",
          &HydroPowerSystem::equal_content,
          (py::arg("self"), py::arg("other_hps")),
          doc.intro("returns true if alle the content of the hps are equal, same as the equal == operator, except that "
                    ".id, .name .created at the top level is not compared")())
        .def(py::self == py::self)
        .def(py::self != py::self)
        .def(
          "to_blob_ref",
          &hps_ext::to_blob,
          (py::arg("me")),
          doc.intro("serialize the model into an blob")
            .returns("blob", "string", "blob-serialized version of the model")
            .see_also("from_blob")())
        .staticmethod("to_blob_ref")
        .def(
          "from_blob",
          &hps_ext::from_blob,
          (py::arg("blob_string")),
          doc.intro("constructs a model from a blob_string previously created by the to_blob method")
            .parameters()
            .parameter(
              "blob_string", "string", "blob-formatted representation of the model, as create by the to_blob method")())
        .staticmethod("from_blob");
    shyft::pyapi::expose_format(hps);

    py::class_<hydro_power_system_builder>(
      "HydroPowerSystemBuilder",
      doc.intro("class to support building hydro-power-systems save and easy")(),
      py::no_init)
      .def(py::init<std::shared_ptr<hydro_power_system> &>((py::arg("hydro_power_system"))))
      .def(
        "create_river",
        &hydro_power_system_builder::create_river,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("create and add river to the system")())
      .def(
        "create_tunnel",
        &hydro_power_system_builder::create_tunnel,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("create and add river to the system")())
      .def(
        "create_water_route",
        &hydro_power_system_builder::create_waterway,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("deprecated:use create_waterway")())
      .def(
        "create_waterway",
        &hydro_power_system_builder::create_waterway,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("create and add river to the system")())
      .def(
        "create_gate",
        &hydro_power_system_builder::create_gate,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("create and add a gate to the system")())
      .def(
        "create_aggregate",
        &hydro_power_system_builder::create_unit,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("deprecated:use create_unit")())
      .def(
        "create_unit",
        &hydro_power_system_builder::create_unit,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("creates a new unit with the specified parameters")())
      .def(
        "create_reservoir",
        &hydro_power_system_builder::create_reservoir,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("creates and adds a reservoir to the system")())
      .def(
        "create_power_station",
        &hydro_power_system_builder::create_power_plant,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("deprecated: use create_power_plant")())
      .def(
        "create_power_plant",
        &hydro_power_system_builder::create_power_plant,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("creates and adds a power plant to the system")())
      .def(
        "create_catchment",
        &hydro_power_system_builder::create_catchment,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("create and add catchmment to the system")());

    expose_map<std::string, std::shared_ptr<hydro_power_system>>(
      "HydroPowerSystemDict", doc.intro("A dict of HydroPowerSystem, the key-value is the watercourse-name")());

    expose_vector_eq<std::shared_ptr<hydro_power_system>>(
      "HydroPowerSystemList",
      "Strongly typed list of HydroPowerSystems",
      +[](
         std::vector<std::shared_ptr<hydro_power_system>> const &a,
         std::vector<std::shared_ptr<hydro_power_system>> const &b) -> bool {
        return equal_vector_ptr_content<hydro_power_system>(a, b);
      },
      false);
  }

  void hydro_graph_stuff() {
    using namespace shyft::energy_market::hydro_power;
    using namespace shyft::energy_market::graph;

    py::def(
      "upstream_reservoirs",
      upstream_reservoirs,
      (py::arg("component"), py::arg("max_dist") = 0),
      doc.intro("Find all reservoirs upstream from component, stopping at `max_dist` traversals.")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("reservoirs", "ReservoirList", "The reservoirs within the specified distance")());
    py::def(
      "downstream_reservoirs",
      downstream_reservoirs,
      (py::arg("component"), py::arg("max_dist") = 0),
      doc.intro("Find all reservoirs upstream from component, stopping at `max_dist` traversals")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("reservoirs", "ReservoirList", "The reservoirs within the specified distance")());
    py::def(
      "upstream_units",
      upstream_units,
      (py::arg("component"), py::arg("max_dist") = 0),
      doc.intro("Find units upstream from component, stopping at `max_dist` traversals")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("units", "UnitList", "The units within the specified distance")());
    py::def(
      "downstream_units",
      downstream_units,
      (py::arg("component"), py::arg("max_dist") = 0),
      doc.intro("Find all units downstream from component, stopping at `max_dist` traversals")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("units", "UnitList", "The units within the specified distance")());
  }

  void hydro_operations_stuff() {
    using namespace shyft::energy_market::hydro_power;
    using hgt = hydro_operations; // alias hgt = HydroGraphTraversal
    using chc = const std::shared_ptr<hydro_component>;
    using hc_vec = std::vector<std::shared_ptr<hydro_component>>;

    py::class_<hgt, py::bases<>, std::shared_ptr<hgt>, boost::noncopyable>(
      "HydroGraphTraversal", doc.intro("A collection of hydro operations")(), py::no_init)
      .def<void (*)(hc_vec &, connection_role)>(
        "path_to_ocean", &hgt::path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .def<void (*)(hc_vec &)>(
        "path_to_ocean", &hgt::path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .staticmethod("path_to_ocean")
      .def<hc_vec (*)(chc &, connection_role)>(
        "get_path_to_ocean", &hgt::get_path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .def<hc_vec (*)(chc &)>(
        "get_path_to_ocean", &hgt::get_path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .staticmethod("get_path_to_ocean")
      .def<hc_vec (*)(chc &, chc &, connection_role)>(
        "get_path_between", &hgt::get_path_between, doc.intro("finds path between two hydro components")())
      .def<hc_vec (*)(chc &, chc &)>(
        "get_path_between", &hgt::get_path_between, doc.intro("finds path between two hydro components")())
      .staticmethod("get_path_between")
      .def<bool (*)(chc &, chc &, connection_role)>(
        "is_connected", &hgt::is_connected, doc.intro("finds whether two hydro components are connected")())
      .def<bool (*)(chc &, chc &)>(
        "is_connected", &hgt::is_connected, doc.intro("finds whether two hydro components are connected")())
      .staticmethod("is_connected")
      .def(
        "extract_water_courses",
        &hgt::extract_water_courses,
        (py::arg("hps")),
        doc.intro("extracts the sub-hydro system from a given hydro system")())
      .staticmethod("extract_water_courses");
  }

  void all_of_hydro_power_system() {
    // call each expose f here
    catchment_stuff();
    hydro_component_stuff();
    water_route_stuff();
    reservoir_stuff();
    power_plant_stuff();
    hydro_power_system_stuff();
    hydro_operations_stuff();
    hydro_graph_stuff();
  }
}
