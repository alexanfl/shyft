#include <memory>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <set>

#include <shyft/version.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/energy_market/py_custom_maps_expose.h>
#include <shyft/py/api/py_convertible.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_variant.h>

namespace expose {
  using std::shared_ptr;
  using std::string;
  using std::vector;
  using std::map;

  void std_containers() {
    register_variant(std::type_identity<shyft::energy_market::stm::any_attr>{});
    expose_map<int, std::string>(
      "IntStringDict", "A strongly typed dictionary with key type int and value type string.");
    expose_map<std::string, shyft::energy_market::hydro_power::apoint_ts, std::less<>>(
      "StringTimeSeriesDict", "A strongly typed dictionary with key type string and value type TimeSeries.");
    expose_map<std::string, shyft::energy_market::stm::any_attr, std::less<>>("StringAnyAttrDict", "desc", true, false);
  }

  void xy_point_curves_etc() {

    using namespace boost::python;
    using Point = shyft::energy_market::hydro_power::point;
    using shyft::energy_market::hydro_power::x_min;
    using shyft::energy_market::hydro_power::x_max;
    using shyft::energy_market::hydro_power::y_min;
    using shyft::energy_market::hydro_power::y_max;
    using shyft::energy_market::hydro_power::z_min;
    using shyft::energy_market::hydro_power::z_max;
    using shyft::energy_market::hydro_power::apoint_ts;
    using shyft::energy_market::hydro_power::interpolation_scheme;

    shyft::pyapi::expose_format(
      py::class_<Point>("Point", doc.intro("Simply a point (x,y)")())
        .def(py::init<double, double>((py::arg("x"), py::arg("y")), doc.intro("construct a point with x and y")()))
        .def(py::init< Point const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def_readwrite("x", &Point::x, "float: ")
        .def_readwrite("y", &Point::y, "float: ")
        .def(py::self == py::self)
        .def(py::self != py::self));

    typedef vector<Point> PointList;
    shyft::pyapi::expose_format(
      py::class_<PointList>("PointList", doc.intro("A strongly typed list of Point.")())
        .def(vector_indexing_suite<PointList>())
        .def(py::init< PointList const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def(py::self == py::self)
        .def(py::self != py::self));
    py_api::iterable_converter().from_python<PointList>();

    using XyPointCurve = shyft::energy_market::hydro_power::xy_point_curve;
    shyft::pyapi::expose_format(
          py::class_<XyPointCurve,py::bases<>,shared_ptr<XyPointCurve>>(
            "XyPointCurve", doc.intro("A curve described using points, piecewise linear.")())
          .def(py::init< PointList const &>((py::arg("points"))))
          .def("__init__",
               py::make_constructor(
                 +[]( std::vector<double>  const &x, std::vector<double>  const &y){
                   return new XyPointCurve(XyPointCurve::make(x,y));
                 },
                 py::default_call_policies(),
                 (py::arg("x_vector"),py::arg("y_vector"))))
          .def(py::init< XyPointCurve const &>((py::arg("clone")), doc.intro("Create a clone.")()))
          .def_readwrite("points",&XyPointCurve::points,"PointList: describing the curve")
          .def("calculate_y", static_cast<double (XyPointCurve::*) (double) const>(&XyPointCurve::calculate_y),
               (py::arg("self"),py::arg("x")), doc.intro("interpolating and extending")())
          .def("calculate_y", static_cast<apoint_ts (XyPointCurve::*) ( apoint_ts const &, interpolation_scheme) const>(&XyPointCurve::calculate_y),
               (py::arg("self"),py::arg("x"),py::arg("method")="linear"), doc.intro("interpolating and extending")())
          .def("calculate_x", static_cast<double (XyPointCurve::*) (double) const>(&XyPointCurve::calculate_x),
               (py::arg("self"),py::arg("x")), doc.intro("interpolating and extending")())
          .def("calculate_x", static_cast<apoint_ts (XyPointCurve::*) ( apoint_ts const &, interpolation_scheme) const>(&XyPointCurve::calculate_x),
               (py::arg("self"),py::arg("x"),py::arg("method")="linear"), doc.intro("interpolating and extending")())
          .def("is_mono_increasing",&XyPointCurve::is_xy_mono_increasing,doc.intro("true if y=f(x) is monotone and increasing")())
          .def("is_convex",&XyPointCurve::is_xy_convex,doc.intro("true if y=f(x) is convex")())
          .def("x_min", +[]( XyPointCurve const & c) -> double { return x_min(c); }, doc.intro("returns smallest value of x")())
          .def("x_max", +[]( XyPointCurve const & c) -> double { return x_max(c); }, doc.intro("returns largest value of x")())
          .def("y_min", +[]( XyPointCurve const & c) -> double { return y_min(c); }, doc.intro("returns smallest value of y")())
          .def("y_max", +[]( XyPointCurve const & c) -> double { return y_max(c); }, doc.intro("returns largest value of y")())
          .def(py::self == py::self)
          .def(py::self != py::self))
        ;

    using XyPointCurveList = vector<XyPointCurve>;
    shyft::pyapi::expose_format(
      py::class_<XyPointCurveList>("XyPointCurveList", doc.intro("A strongly typed list of XyPointCurve.")())
        .def(vector_indexing_suite<XyPointCurveList>())
        .def(py::init< XyPointCurveList const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def(py::self == py::self)
        .def(py::self != py::self));
    py_api::iterable_converter().from_python<XyPointCurveList>();

    using XyPointCurveWithZ = shyft::energy_market::hydro_power::xy_point_curve_with_z;
    shyft::pyapi::expose_format(
      py::class_<XyPointCurveWithZ, py::bases<>, shared_ptr<XyPointCurveWithZ>>(
        "XyPointCurveWithZ", doc.intro("A XyPointCurve with a reference value z.")())
        .def(py::init< XyPointCurve const &, double>((py::arg("xy_point_curve"), py::arg("z"))))
        .def(py::init< XyPointCurveWithZ const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def_readwrite("xy_point_curve", &XyPointCurveWithZ::xy_curve, "XyPointCurve: describes the function at z")
        .def_readwrite("z", &XyPointCurveWithZ::z, "float: z value")
        .def(py::self == py::self)
        .def(py::self != py::self));

    using XyPointCurveWithZList = vector<XyPointCurveWithZ>;
    shyft::pyapi::expose_format(
      py::class_<XyPointCurveWithZList, py::bases<>, shared_ptr<XyPointCurveWithZList>>(
        "XyPointCurveWithZList", doc.intro("A strongly typed list of XyPointCurveWithZ.")())
        .def(vector_indexing_suite<XyPointCurveWithZList>())
        .def(py::init< XyPointCurveWithZList const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def(
          "x_min",
          +[](XyPointCurveWithZList const &c) -> double {
            return x_min(c);
          },
          doc.intro("returns smallest value of x")())
        .def(
          "x_max",
          +[](XyPointCurveWithZList const &c) -> double {
            return x_max(c);
          },
          doc.intro("returns largest value of x")())
        .def(
          "y_min",
          +[](XyPointCurveWithZList const &c) -> double {
            return y_min(c);
          },
          doc.intro("returns smallest value of y")())
        .def(
          "y_max",
          +[](XyPointCurveWithZList const &c) -> double {
            return y_max(c);
          },
          doc.intro("returns largest value of y")())
        .def(
          "z_min",
          +[](XyPointCurveWithZList const &c) -> double {
            return z_min(c);
          },
          doc.intro("returns smallest value of z")())
        .def(
          "z_max",
          +[](XyPointCurveWithZList const &c) -> double {
            return z_max(c);
          },
          doc.intro("returns largest value of z")())
        .def(
          "evaluate",
          +[](XyPointCurveWithZList const &self, double x, double z) {
            return shyft::energy_market::hydro_power::xyz_point_curve(self).evaluate(x, z);
          },
          (py::arg("x"), py::arg("z")),
          doc.intro("Evaluate the curve at the point (x, z)")())

        .def(py::self == py::self)
        .def(py::self != py::self));
    py_api::iterable_converter().from_python<XyPointCurveWithZList>();

    using XyzPointCurve = shyft::energy_market::hydro_power::xyz_point_curve;
    shyft::pyapi::expose_format(
      py::class_<XyzPointCurve, py::bases<>, shared_ptr<XyzPointCurve>>(
        "XyzPointCurve",
        doc.intro("A 3D curve consisting of one or more 2D curves parametrised over a third variable.")())
        .def(py::init< XyPointCurveWithZList const &>((py::arg("curves")), doc.intro("Create from a list.")()))
        .def(
          "set_curve",
          (void(XyzPointCurve::*)(double, XyPointCurve const &)) & XyzPointCurve::set_curve,
          (py::arg("z"), py::arg("xy")),
          doc.intro("Assign an XyzPointCurve to a z-value")())
        .def("get_curve", &XyzPointCurve::get_curve, py::arg("z"), doc.intro("get the curve assigned to the value")())
        .def(
          "evaluate",
          &XyzPointCurve::evaluate,
          (py::arg("x"), py::arg("z")),
          doc.intro("Evaluate the curve at the point (x, z)")())
        .def("gradient", &XyzPointCurve::gradient)
        .add_property(
          "curves",
          +[](XyzPointCurve const &self) {
            return static_cast<XyPointCurveWithZList>(self);
          },
          doc.intro("XyPointCurveWithZList: list the contained curves with z values")())
        .def(py::self == py::self)
        .def(py::self != py::self));

    using TurbineOperatingZone = shyft::energy_market::hydro_power::turbine_operating_zone;
    shyft::pyapi::expose_format(
      py::class_<TurbineOperatingZone, py::bases<>, shared_ptr<TurbineOperatingZone>>(
        "TurbineOperatingZone",
        doc.intro("A turbine efficiency.")
          .details("Defined by a set of efficiency curves, one for each net head, with optional production limits.\n"
                   "Part of the turbine description, to describe the efficiency of an entire turbine, or an isolated\n"
                   "operating zone or a Pelton needle combination. Production limits are only relevant when "
                   "representing\n"
                   "an isolated operating zone or a Pelton needle combination.")())
        .def(py::init< XyPointCurveWithZList const &>((py::arg("efficiency_curves"))))
        .def(py::init< XyPointCurveWithZList const &, double, double>(
          (py::arg("efficiency_curves"), py::arg("production_min"), py::arg("production_max"))))
        .def(py::init< XyPointCurveWithZList const &, double, double, double, double, double>(
          (py::arg("efficiency_curves"),
           py::arg("production_min"),
           py::arg("production_max"),
           py::arg("production_nominal"),
           py::arg("fcr_min"),
           py::arg("fcr_max"))))
        .def(py::init< TurbineOperatingZone const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def_readwrite(
          "efficiency_curves",
          &TurbineOperatingZone::efficiency_curves,
          doc.intro("XyPointCurveWithZList: A list of XyPointCurveWithZ efficiency curves for the net head range of "
                    "the entire turbine, or an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "production_min",
          &TurbineOperatingZone::production_min,
          doc.intro("float: The minimum production for which the efficiency curves are valid.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "production_max",
          &TurbineOperatingZone::production_max,
          doc.intro("float: The maximum production for which the efficiency curves are valid.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "production_nominal",
          &TurbineOperatingZone::production_nominal,
          doc
            .intro("float: The nominal production, or installed/rated/nameplate capacity, for which the efficiency "
                   "curves are valid.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "fcr_min",
          &TurbineOperatingZone::fcr_min,
          doc
            .intro("float: The temporary minimum production allowed for this set of efficiency curves when delivering "
                   "FCR.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "fcr_max",
          &TurbineOperatingZone::fcr_max,
          doc
            .intro("float: The temporary maximum production allowed for this set of efficiency curves when delivering "
                   "FCR.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def(
          "evaluate",
          &TurbineOperatingZone::evaluate,
          (py::arg("x"), py::arg("z")),
          doc.intro("Evaluate the efficiency curves at a point (x, z)")())
        .def(py::self == py::self)
        .def(py::self != py::self));

    using TurbineOperatingZoneList = vector<TurbineOperatingZone>;
    shyft::pyapi::expose_format(
      py::class_<TurbineOperatingZoneList>(
        "TurbineOperatingZoneList", doc.intro("A strongly typed list of TurbineOperatingZone.")())
        .def(vector_indexing_suite<TurbineOperatingZoneList>())
        .def(py::init< TurbineOperatingZoneList const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def(py::self == py::self)
        .def(py::self != py::self));
    py_api::iterable_converter().from_python<TurbineOperatingZoneList>();

    using TurbineDescription = shyft::energy_market::hydro_power::turbine_description;

    {
      using shyft::energy_market::hydro_power::turbine_capability;
      py::enum_<turbine_capability>("TurbineCapability", doc.intro("Describes the capabilities of a turbine.")())
        .value("turbine_none", turbine_capability::turbine_none)
        .value("turbine_forward", turbine_capability::turbine_forward)
        .value("turbine_backward", turbine_capability::turbine_backward)
        .value("turbine_reversible", turbine_capability::turbine_reversible)
        .export_values();

      py::def(
        "has_forward_capability",
        +[](turbine_capability const &t) {
          return has_forward_capability(t);
        },
        doc.intro("Checks if a turbine can support generating")());
      py::def(
        "has_backward_capability",
        +[](turbine_capability const &t) {
          return has_backward_capability(t);
        },
        doc.intro("Checks if a turbine can support pumping")());
      py::def(
        "has_reversible_capability",
        +[](turbine_capability const &t) {
          return has_reversible_capability(t);
        },
        doc.intro("Checks if a turbine can support both generating and pumping")());
    }

    shyft::pyapi::expose_format(
      py::class_<TurbineDescription, py::bases<>, shared_ptr<TurbineDescription>>(
        "TurbineDescription",
        doc.intro("Complete description of efficiencies a turbine for all operating zones.")
          .details("Pelton turbines typically have multiple operating zones; one for each needle combination.\n"
                   "Other turbines normally have only a single operating zone describing the entire turbine,\n"
                   "but may have more than one to model different isolated operating zones.\n"
                   "Each operating zone is described with a turbine efficiency object, which in turn may\n"
                   "contain multiple efficiency curves; one for each net head.")())
        .def(py::init< TurbineOperatingZoneList const &>((py::arg("operating_zones"))))
        .def(py::init< TurbineDescription const &>((py::arg("clone")), doc.intro("Create a clone.")()))
        .def_readwrite(
          "operating_zones",
          &TurbineDescription::operating_zones,
          doc.intro("TurbineOperatingZoneList: list of TurbineOperatingZone.")
            .details("Containing a single entry describing the entire turbine, or one entry for each isolated "
                     "operating zone or Pelton needle combinations.")())

        .def(
          "get_operating_zone",
          &TurbineDescription::get_operating_zone,
          py::arg("p"),
          doc.intro("Find operating zone for given production value p")
            .notes()
            .note("If operating zones are overlapping then the zone with lowest value of production_min will be "
                  "selected.")())

        .def(py::self == py::self)
        .def(py::self != py::self)
        .def(
          "capability",
          +[](TurbineDescription const &p) {
            return capability(p);
          },
          doc.intro("Return the capability of the turbine")()));
    ;
  }

  vector<shyft::energy_market::hydro_power::point>
    create_from_vectors(vector<double> const &x, vector<double> const &y) {
    if (x.size() != y.size())
      throw std::runtime_error("diff size");
    vector<shyft::energy_market::hydro_power::point> r;
    for (size_t i = 0; i < x.size(); ++i) {
      r.push_back(shyft::energy_market::hydro_power::point(x[i], y[i]));
    }
    return r;
  }

  void point_vector_from_x_y() {
    py::def("points_from_x_y", create_from_vectors, (py::arg("x"), py::arg("y")));
  }

  struct mod_ext {
    static vector<char> to_blob(shared_ptr<shyft::energy_market::market::model> const &m) {
      auto s = m->to_blob();
      return vector<char>(s.begin(), s.end());
    }

    static shared_ptr<shyft::energy_market::market::model> from_blob(vector<char> &blob) {
      string s(blob.begin(), blob.end());
      return shyft::energy_market::market::model::from_blob(s);
    }
  };

  void model() {
    using namespace boost::python;
    using Model = shyft::energy_market::market::model;
    using ModelBuilder = shyft::energy_market::market::model_builder;

    py::class_<ModelBuilder>(
      "ModelBuilder", doc.intro("This class helps building an EMPS model, step by step")(), no_init)
      .def(py::init<shared_ptr<Model> &>(
        (py::arg("model")),
        doc.intro("Make a model-builder for the model")
          .intro("The model can be modified/built using the methods")
          .intro("available in this class")
          .parameters()
          .parameter("model", "Model", "the model to be built/modified")()))
      .def(
        "create_power_line",
        &ModelBuilder::create_power_line,
        (py::arg("self"), py::arg("a"), py::arg("b"), py::arg("id"), py::arg("name"), py::arg("json") = ""),
        doc.intro("create and add a power line with capacity_MW between area a and b to the model")
          .parameters()
          .parameter("a", "ModelArea", "from existing model-area, that is part of the current model")
          .parameter("b", "ModelArea", "to existing model-area, that is part of the current model")
          .parameter("id", "int", "unique ID of the power-line")
          .parameter("name", "string", "unique name of the power-line")
          .parameter("json", "string", "json for the power-line")
          .returns("pl", "PowerLine", "the newly created power-line, that is now a part of the model")())
      .def(
        "create_model_area",
        &ModelBuilder::create_model_area,
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("json") = ""),
        doc.intro("create and add an area to the model.")
          .intro("ensures that area_name, and that area_id is unique.")
          .parameters()
          .parameter("id", "int", "unique identifier for the area, must be unique within model")
          .parameter("name", "string", "any valid area-name, must be unique within model")
          .parameter("json", "string", "json for the area")
          .returns("area", "ModelArea", "a reference to the newly added area")
          .see_also("add_area")())
      .def(
        "create_power_module",
        &ModelBuilder::create_power_module,
        (py::arg("self"), py::arg("model_area"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("create and add power-module to the area, doing validity checks")
          .parameters()
          .parameter("model_area", "ModelArea", "the model-area for which we create a power-module")
          .parameter("id", "string", "encoded power_type/load/wind module id")
          .parameter("module_name", "string", "unique module-name for each area")
          .parameter("json", "string", "json for the pm")
          .returns("pm", "PowerModule", "a reference to the created and added power-module")());

    py::class_<Model, bases<shyft::energy_market::id_base>, shared_ptr<Model>, boost::noncopyable>(
      "Model",
      doc.intro("The Model class describes the  LTM (persisted) model")
        .intro("A model consists of model_areas and power-lines interconnecting them.")
        .intro("To buid a model use the .add_area() and .add_power_line() methods")
        .see_also("ModelArea,PowerLine,PowerModule")())
      .def(py::init<int, string const &, py::optional< string const &>>(
        (py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("constructs a Model object with the specified parameters")
          .parameters()
          .parameter("id", "int", "a global unique identifier of the mode")
          .parameter("name", "string", "the name of the model")
          .parameter("json", "string", "extra info as json for the model")()))
      .def_readwrite(
        "created", &Model::created, doc.intro("time: The timestamp when the model was created, utc seconds 1970")())
      .def_readonly(
        "area",
        &Model::area,
        doc.intro("ModelAreaDict: a dict(area-name,area) for the model-areas")()) // return_internal_reference<>()
      .def_readonly(
        "power_lines",
        &Model::power_lines,
        doc.intro("PowerLineList: list of power-lines,each with connection to the areas they interconnect")())
      .def(
        "equal_structure",
        &Model::equal_structure,
        (py::arg("self"), py::arg("other")),
        doc.intro("Compare this model with other_model for equality in topology and interconnections.")
          .intro("The comparison is using each object`.id` member to identify the same objects.")
          .intro("Notice that the attributes of the objects are not considered, only the topology.")
          .parameters()
          .parameter("other", "Model", "The model to compare with")
          .returns("equal", "bool", "true if other_model has structure and objects as self")())
      .def(
        "equal_content",
        &Model::equal_content,
        (py::arg("self"), py::arg("other")),
        doc
          .intro("Compare this model with other_model for equality, except for the `.id`, `.name`,`.created`, "
                 "attributes of the model it self.")
          .intro("This is the same as the equal,==, operation, except that the self model local attributes are not "
                 "compared.")
          .intro("This method can be used to determine that two models have the same content, even if they model.id "
                 "etc. are different.")
          .parameters()
          .parameter("other", "Model", "The model to compare with")
          .returns(
            "equal",
            "bool",
            "true if other have exactly the same content as self(disregarding the model .id,.name,.created,.json "
            "attributes)")())
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "to_blob",
        &mod_ext::to_blob,
        doc.intro("serialize the model into a blob")
          .returns("blob", "ByteVector", "serialized version of the model")
          .see_also("from_blob")())
      .def(
        "from_blob",
        &mod_ext::from_blob,
        (py::arg("blob")),
        doc.intro("constructs a model from a blob previously created by the to_blob method")
          .parameters()
          .parameter("blob", "ByteVector", "blob representation of the model, as create by the to_blob method")())
      .staticmethod("from_blob");
    using ModelList = vector<shared_ptr<Model>>;
    py::class_<ModelList>("ModelList", "A strongly typed list, vector, of models")
      .def(py::vector_indexing_suite<ModelList, true>());
  }

  void model_area() {
    using namespace boost::python;
    using ModelArea = shyft::energy_market::market::model_area;
    py::class_<ModelArea, bases<shyft::energy_market::id_base>, shared_ptr<ModelArea>, boost::noncopyable> m(
      "ModelArea",
      doc.intro("The ModelArea class describes the EMPS LTM (persisted) model-area")
        .intro("A model-area consists of power modules and hydro-power-system.")
        .intro("To buid a model-are use the .add_power_module() and the hydro-power-system builder")
        .see_also("Model,PowerLine,PowerModule,HydroPowerSystem")());
    m
      .def(py::init<
           shared_ptr<shyft::energy_market::market::model> const &,
           int,
           string const &,
           py::optional< string const &>>(
        (py::arg("model"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("constructs a ModelArea object with the specified parameters")
          .parameters()
          .parameter("model", "Model", "the model owning the created model-area")
          .parameter("id", "int", "a global unique identifier of the model-area")
          .parameter("name", "string", "the name of the model-area")
          .parameter("json", "string", "extra info as json")()))
      .def_readonly(
        "power_modules",
        &ModelArea::power_modules,
        doc.intro("PowerModuleDict: power-modules in this area, a dictionary using power-module unique id")())
      .add_property(
        "detailed_hydro",
        &ModelArea::get_detailed_hydro,
        &ModelArea::set_detailed_hydro,
        doc.intro("HydroPowerSystem:  detailed hydro description.").see_also("HydroPowerSystem")())
      .add_property("model", &ModelArea::get_model, "Model: the model for the area")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "equal_structure",
        &ModelArea::equal_structure,
        (py::arg("self"), py::arg("other")),
        doc.intro("Compare this model-area with other_model-area for equality in topology and interconnections.")
          .intro("The comparison is using each object`.id` member to identify the same objects.")
          .intro("Notice that the attributes of the objects are not considered, only the topology.")
          .parameters()
          .parameter("other", "ModelArea", "The model-area to compare with")
          .returns("equal", "bool", "true if other_model has structure and objects as self")());

    typedef map<int, shared_ptr<ModelArea>> ModelAreaDict;
    py::class_<ModelAreaDict>("ModelAreaDict", doc.intro("A dict of ModelArea, the key-value is the area-name")())
      .def(map_indexing_suite<ModelAreaDict, true>()) // true since the shared_ptr is already a proxy
      .def(py::init< ModelAreaDict const &>((py::arg("clone_me"))));
  }

  void id_base_stuff() {
    using shyft::energy_market::id_base;
    py::class_< id_base, py::bases<>, shared_ptr<id_base>, boost::noncopyable> c(
      "IdBase",
      doc.intro("The IdBase class provides common properties of the energy_market objects.")
        .intro("This includes, the identifier, name, json type properties,")
        .intro("and also the python object handle 'obj', as well as custom")
        .intro("time-series and any type attributes")
        .intro("This class can not be constructed, but serves as base class for most objects.")
        .see_also("Model,ModelArea,PowerModule,HydroPowerSystem")(),
      py::no_init);
    c.def_readwrite("id", &id_base::id, doc.intro("int: The unique id of the component")())
      .def_readwrite("name", &id_base::name, doc.intro("str: The name of the component")())
      .def_readwrite("json", &id_base::json, doc.intro("str: json string with info")())
      .add_property(
        "obj",
        &py_object_ext<id_base>::get_obj,
        &py_object_ext<id_base>::set_obj,
        doc.intro("object: a python object")())
      .def(py::self == py::self)
      .def(py::self != py::self);
    expose::expose_custom_maps(c);
  }

  void power_line() {
    using PowerLine = shyft::energy_market::market::power_line;
    py::class_<PowerLine, py::bases<shyft::energy_market::id_base>, shared_ptr<PowerLine>, boost::noncopyable>(
      "PowerLine",
      doc.intro("The PowerLine class describes the LTM (persisted) power-line")
        .intro("A power-line represents the transmission capacity between two model-areas.")
        .intro("Use the ModelArea.create_power_line(a1,a2,id) to build a power line")
        .see_also("Model,ModelArea,PowerModule,HydroPowerSystem")(),
      py::no_init)
      .def(py::init<
           shared_ptr<shyft::energy_market::market::model> const &,
           shared_ptr<shyft::energy_market::market::model_area> &,
           shared_ptr<shyft::energy_market::market::model_area> &,
           int,
           string const &,
           py::optional< string const &>>(
        (py::arg("model"), py::arg("area_1"), py::arg("area_2"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("constructs a PowerLine object between area 1 and 2 with the specified id")
          .parameters()
          .parameter("model", "Model", "the model for the power-line")
          .parameter("area_1", "ModelArea", "a reference to an existing area in the model")
          .parameter("area_2", "ModelArea", "a reference to an existing area in the model")
          .parameter("id", "int", "a global unique identifier for the power-line")
          .parameter("name", "string", "a global unique name for the power-line")
          .parameter("json", "string", "extra json for the power-line")()))
      .add_property("model", &PowerLine::get_model, doc.intro("Model: the model for this power-line")())
      .add_property(
        "area_1", &PowerLine::get_area_1, &PowerLine::set_area_1, doc.intro("ModelArea: reference to area-from")())
      .add_property(
        "area_2", &PowerLine::get_area_2, &PowerLine::set_area_2, doc.intro("ModelArea: reference to area-to")())
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "equal_structure",
        &PowerLine::equal_structure,
        (py::arg("self"), py::arg("other")),
        doc.intro("Compare this power-line with the other for equality in topology and interconnections.")
          .intro("The comparison is using each object`.id` member to identify the same objects.")
          .intro("Notice that the attributes of the objects are not considered, only the topology.")
          .parameters()
          .parameter("other", "", "The model-area to compare with")
          .returns("equal", "bool", "true if other has structure equal to self")());
    typedef vector<shared_ptr<PowerLine>> PowerLineList;
    py::class_<PowerLineList>("PowerLineList", doc.intro("A dict of ModelArea, the key-value is the area-name")())
      .def(py::vector_indexing_suite<PowerLineList, true>()) // true since the shared_ptr is already a proxy
      .def(py::init< PowerLineList const &>((py::arg("clone_me"))));
  }

  void power_module() {
    using namespace boost::python;
    using PowerModule = shyft::energy_market::market::power_module;

    py::class_<PowerModule, bases<shyft::energy_market::id_base>, shared_ptr<PowerModule>, boost::noncopyable>(
      "PowerModule",
      doc.intro("The PowerModule class describes the LTM (persisted) power-module")
        .intro("A power-module represents an actor that consume/produces power for given price/volume")
        .intro("characteristics. The user can influence this characteristics giving")
        .intro("specific semantic load_type/power_type and extra data and/or relations to")
        .intro("other power-modules within the same area.")
        .see_also("Model,ModelArea,PowerLine,HydroPowerSystem")())
      .def(py::init<shared_ptr<shyft::energy_market::market::model_area> const &, int, string, py::optional<string>>(
        (py::arg("area"), py::arg("id"), py::arg("name"), py::arg("json")),
        doc.intro("constructs a PowerModule with specified mandatory name and module-id")
          .parameters()
          .parameter("area", "ModelArea", "the area for this power-module")
          .parameter("id", "int", "unique pm-id for area")
          .parameter("name", "string", "the name of the power-module")
          .parameter("json", "string", "optional json ")()))
      .add_property("area", &PowerModule::get_area, "ModelArea: the model-area for this power-module")
      .def(py::self == py::self)
      .def(py::self != py::self);

    typedef map<int, shared_ptr<PowerModule>> PowerModuleDict;
    py::class_<PowerModuleDict>(
      "PowerModuleDict", doc.intro("A dict of PowerModule, the key-value is the power module id")())
      .def(map_indexing_suite<PowerModuleDict, true>()) // true since the shared_ptr is already a proxy
      .def(py::init< PowerModuleDict const &>((py::arg("clone_me"))));
  }

  void py_destroy(void *o) {
    auto pyo = static_cast<py::object *>(o);
    if (pyo) {
      delete pyo;
    }
  };

  void register_py_destroy() {
    shyft::energy_market::em_handle::destroy = py_destroy;
  }

  extern void all_of_hydro_power_system();
  extern void all_time_series_support();
  extern void ex_client_server();
}

BOOST_PYTHON_MODULE(_core) {
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::py::scope().attr("__doc__") = "Shyft Open Source Energy Market model core";
  expose::py::scope().attr("__version__") = shyft::_version_string();
  expose::id_base_stuff();
  expose::model_area();
  expose::model();
  expose::std_containers();
  expose::power_line();
  expose::power_module();
  expose::xy_point_curves_etc();
  expose::all_of_hydro_power_system();
  expose::all_time_series_support();
  expose::point_vector_from_x_y();
  expose::ex_client_server();
  expose::register_py_destroy();
}
