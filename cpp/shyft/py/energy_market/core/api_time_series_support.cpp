#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/ts_compress.h>
#include <shyft/py/api/bindings.h>

namespace expose {

  static size_t compressed_size_float(std::vector<float> const & v, float accuracy) {
    return shyft::time_series::ts_compress_size(v, accuracy);
  }

  static size_t compressed_size_double(std::vector<double> const & v, double accuracy) {
    return shyft::time_series::ts_compress_size(v, accuracy);
  }

  void all_time_series_support() {
    py::def("compressed_size", compressed_size_double, (py::arg("double_vector"), py::arg("accuracy")));
    py::def("compressed_size", compressed_size_float, (py::arg("float_vector"), py::arg("accuracy")));
  }

}
