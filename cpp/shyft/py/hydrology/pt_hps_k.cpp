/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/bindings.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/hbv_physical_snow.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/stacks/pt_hps_k.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/stacks/pt_hps_k_cell_model.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>


#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/api/expose_container.h>

static char const *version() {
  return "v1.0";
}

namespace expose { namespace pt_hps_k {
    using namespace shyft::core;
    using namespace shyft::core::pt_hps_k;

    static void parameter_state_response() {
      py::class_<parameter, py::bases<>, std::shared_ptr<parameter>> pp(
        "PTHPSKParameter",
        "Contains the parameters to the methods used in the PTHPSK assembly\n"
        "priestley_taylor,hbv_physical_snow,actual_evapotranspiration,precipitation_correction,kirchner\n");
      pp
        .def(py::init<
             priestley_taylor::parameter const &,
             hbv_physical_snow::parameter const &,
             actual_evapotranspiration::parameter const &,
             kirchner::parameter const &,
             precipitation_correction::parameter const &,
             py::optional<glacier_melt::parameter, routing::uhg_parameter, mstack_parameter>>(
          (py::arg("pt"),
           py::arg("hps"),
           py::arg("ae"),
           py::arg("k"),
           py::arg("p_corr"),
           py::arg("gm"),
           py::arg("routing"),
           py::arg("msp")),
          "create object with specified parameters"))
        .def(py::init< parameter const &>((py::arg("p")), "clone a parameter"))
        .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
        .def_readwrite("ae", &parameter::ae, "ActualEvapotranspirationParameter: actual evapotranspiration parameter")
        .def_readwrite("hps", &parameter::hps, "HbvPhysicalSnowParameter: hbv-physical-snow parameter")
        .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
        .def_readwrite("kirchner", &parameter::kirchner, "KirchnerParameter: kirchner parameter")
        .def_readwrite(
          "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
        .def_readwrite(
          "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters")
        .def_readwrite("msp", &parameter::msp, "MethodStackParameter: contains the method stack parameters")
        .def("size", &parameter::size, "returns total number of calibration parameters")
        .def(
          "set",
          &parameter::set,
          (py::arg("self"), py::arg("p")),
          "set parameters from vector/list of float, ordered as by get_name(i)")
        .def(
          "get",
          &parameter::get,
          (py::arg("self"), py::arg("i")),
          "return the value of the i'th parameter, name given by .get_name(i)")
        .def(
          "get_name",
          &parameter::get_name,
          (py::arg("self"), py::arg("i")),
          "returns the i'th parameter name, see also .get()/.set() and .size()")
        .def(
          "serialize",
          &serialize_to_bytes<parameter>,
          (py::arg("self")),
          "serializes the parameters to a blob, that later can be passed in to .deserialize()")
        .def("deserialize", &deserialize_from_bytes<parameter>, (py::arg("blob")))
        .staticmethod("deserialize")
        .def(py::self == py::self)
        .def(py::self != py::self);
      shyft::pyapi::expose_format(pp);
      expose_map<std::int64_t, parameter_t_>(
        "PTHPSKParameterMap",
        "dict (int,parameter)  where the int is the catchment_id",
        true, // comparable
        false // shared_ptr, so no proxy
      );


      py::class_<state>("PTHPSKState")
        .def(py::init<hbv_physical_snow::state, kirchner::state>(
          (py::arg("hps"), py::arg("k")), "initializes state with hbv-physical-snow gs and kirchner k"))
        .def_readwrite("hps", &state::hps, "HbvPhysicalSnowState: physical-snow state")
        .def_readwrite("kirchner", &state::kirchner, "KircherState: kirchner state");

      typedef std::vector<state> PTHPSKStateVector;
      py::class_<PTHPSKStateVector, py::bases<>, std::shared_ptr<PTHPSKStateVector> >("PTHPSKStateVector")
        .def(py::vector_indexing_suite<PTHPSKStateVector>());
      py::class_<response>(
        "PTHPSKResponse", "This struct contains the responses of the methods used in the PTHPSK assembly")
        .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
        .def_readwrite("hps", &response::hps, "HbvPhysicalSnowResponse: physical-snow response")
        .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
        .def_readwrite("ae", &response::ae, "ActualEvapotranspirationResponse: actual evapotranspiration response")
        .def_readwrite("kirchner", &response::kirchner, "KirchnerResponse: kirchner response")
        .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
    }

    static void collectors() {
      typedef shyft::core::pt_hps_k::all_response_collector PTHPSKAllCollector;
      py::class_<PTHPSKAllCollector>("PTHPSKAllCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTHPSKAllCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTHPSKAllCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m3/s] for the timestep")
        .def_readonly(
          "hps_sca",
          &PTHPSKAllCollector::snow_sca,
          "TsFixed: hbv snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "hps_swe",
          &PTHPSKAllCollector::snow_swe,
          "TsFixed: hbv snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly(
          "hps_outflow", &PTHPSKAllCollector::hps_outflow, "TsFixed: hbv snow output [m^3/s] for the timestep")
        .def_readonly(
          "glacier_melt", &PTHPSKAllCollector::glacier_melt, "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
        .def_readonly("ae_output", &PTHPSKAllCollector::ae_output, "TsFixed: actual evap mm/h")
        .def_readonly("pe_output", &PTHPSKAllCollector::pe_output, "TsFixed: pot evap mm/h")
        .def_readonly(
          "end_reponse", &PTHPSKAllCollector::end_reponse, "PTHPSKResponse: end_response, at the end of collected")
        .def_readonly("avg_charge", &PTHPSKAllCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

      typedef shyft::core::pt_hps_k::discharge_collector PTHPSKDischargeCollector;
      py::class_<PTHPSKDischargeCollector>("PTHPSKDischargeCollector", "collect all cell response from a run")
        .def_readonly(
          "destination_area", &PTHPSKDischargeCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTHPSKDischargeCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
        .def_readonly(
          "hps_sca",
          &PTHPSKDischargeCollector::snow_sca,
          "TsFixed: hbv snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "hps_swe",
          &PTHPSKDischargeCollector::snow_swe,
          "TsFixed: hbv snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly(
          "end_reponse",
          &PTHPSKDischargeCollector::end_response,
          "PTHPSKResponse: end_response, at the end of collected")
        .def_readwrite(
          "collect_snow", &PTHPSKDischargeCollector::collect_snow, "bool: controls collection of snow routine")
        .def_readonly(
          "avg_charge", &PTHPSKDischargeCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep")

        ;
      typedef shyft::core::pt_hps_k::null_collector PTHPSKNullCollector;
      py::class_<PTHPSKNullCollector>(
        "PTHPSKNullCollector",
        "collector that does not collect anything, useful during calibration to minimize memory&maximize speed");

      typedef shyft::core::pt_hps_k::state_collector PTHPSKStateCollector;
      py::class_<PTHPSKStateCollector>("PTHPSKStateCollector", "collects state, if collect_state flag is set to true")
        .def_readwrite(
          "collect_state",
          &PTHPSKStateCollector::collect_state,
          "if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
        .def_readonly(
          "kirchner_discharge",
          &PTHPSKStateCollector::kirchner_discharge,
          "TsFixed: Kirchner state instant Discharge given in m^3/s")
        .def_readonly("hps_swe", &PTHPSKStateCollector::hps_swe, "TsFixed: swe")
        .def_readonly("hps_sca", &PTHPSKStateCollector::hps_sca, "TsFixed: sca")
        .def_readonly("hps_surface_heat", &PTHPSKStateCollector::hps_surface_heat, "TsFixed: surface heat")
        .def_readonly("snow_sp", &PTHPSKStateCollector::sp, "CoreTsVector: sp")
        .def_readonly("snow_sw", &PTHPSKStateCollector::sw, "CoreTsVector: sw")
        .def_readonly("albedo", &PTHPSKStateCollector::albedo, "CoreTsVector: albedo")
        .def_readonly("iso_pot_energy", &PTHPSKStateCollector::iso_pot_energy, "CoreTsVector: iso_pot_energy")

        ;
    }

    static void cells() {
      typedef shyft::core::cell<parameter, state, state_collector, all_response_collector> PTHPSKCellAll;
      typedef shyft::core::cell<parameter, state, null_collector, discharge_collector> PTHPSKCellOpt;
      expose::cell<PTHPSKCellAll>("PTHPSKCellAll", "tbd: PTHPSKCellAll doc");
      expose::cell<PTHPSKCellOpt>("PTHPSKCellOpt", "tbd: PTHPSKCellOpt doc");
      expose::statistics::hbv_physical_snow<PTHPSKCellAll>(
        "PTHPSKCell"); // it only gives meaning to expose the *All collect cell-type
      expose::statistics::actual_evapotranspiration<PTHPSKCellAll>("PTHPSKCell");
      expose::statistics::priestley_taylor<PTHPSKCellAll>("PTHPSKCell");
      expose::statistics::kirchner<PTHPSKCellAll>("PTHPSKCell");
      expose::cell_state_etc<PTHPSKCellAll>("PTHPSK"); // just one expose of state
    }

    static void models() {
      typedef shyft::core::region_model<pt_hps_k::cell_discharge_response_t, shyft::api::a_region_environment>
        PTHPSKOptModel;
      typedef shyft::core::region_model<pt_hps_k::cell_complete_response_t, shyft::api::a_region_environment>
        PTHPSKModel;
      expose::model<PTHPSKModel>("PTHPSKModel", "PTHPSK");
      expose::model<PTHPSKOptModel>("PTHPSKOptModel", "PTHPSK");
      def_clone_to_similar_model<PTHPSKModel, PTHPSKOptModel>("create_opt_model_clone");
      def_clone_to_similar_model<PTHPSKOptModel, PTHPSKModel>("create_full_model_clone");
    }

    static void model_calibrator() {
      expose::model_calibrator<
        shyft::core::region_model<pt_hps_k::cell_discharge_response_t, shyft::api::a_region_environment>>(
        "PTHPSKOptimizer");
    }
}}

BOOST_PYTHON_MODULE(_pt_hps_k) {

  expose::py::scope().attr("__doc__") = "Shyft python api for the pt_hps_k model";
  expose::py::def("version", version);
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::pt_hps_k::parameter_state_response();
  expose::pt_hps_k::cells();
  expose::pt_hps_k::models();
  expose::pt_hps_k::collectors();
  expose::pt_hps_k::model_calibrator();
}
