/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/hydrology/cell_model.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/py_convertible.h>

namespace expose {
  void cell_environment() {
    using CellEnvironment = shyft::core::environment_t;
    py::class_<CellEnvironment>(
      "CellEnvironment", "Contains all ts projected to a certain cell-model using interpolation step (if needed)")
      .def_readwrite("temperature", &CellEnvironment::temperature, "TsFixed: temperature")
      .def_readwrite("precipitation", &CellEnvironment::precipitation, "TsFixed: precipitation")
      .def_readwrite("radiation", &CellEnvironment::radiation, "TsFixed: radiation")
      .def_readwrite("wind_speed", &CellEnvironment::wind_speed, "TsFixed: wind speed")
      .def_readwrite("rel_hum", &CellEnvironment::rel_hum, "TsFixed: relhum")
      .def("init", &CellEnvironment::init, (py::arg("self"), py::arg("ta")), "zero all series, set time-axis ta")
      .def(
        "has_nan_values",
        &CellEnvironment::has_nan_values,
        (py::arg("self")),
        doc.intro("scans all time-series for nan-values")
          .returns("has_nan", "bool", "true if any nan is encounted, otherwise false")());

    py::enum_<shyft::core::stat_scope>(
      "stat_scope",
      doc.intro("Defines the scope of the indexes passed to statistics functions")
        .intro(".cell      : the indexes are considered as the i'th cell")
        .intro(".catchment : the indexes are considered catchment identifiers")
        .intro("")
        .intro("The statistics is then covering the cells that matches the selected criteria")())
      .value("cell", shyft::core::stat_scope::cell_ix)
      .value("catchment", shyft::core::stat_scope::catchment_ix)
      .export_values();
  }
}
