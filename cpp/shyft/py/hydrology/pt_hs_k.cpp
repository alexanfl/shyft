/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/bindings.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/hbv_snow.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/stacks/pt_hs_k.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/stacks/pt_hs_k_cell_model.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>


#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/api/expose_container.h>

static char const *version() {
  return "v1.0";
}

namespace expose { namespace pt_hs_k {
    using namespace shyft::core;
    using namespace shyft::core::pt_hs_k;

    static void parameter_state_response() {
      py::class_<parameter, py::bases<>, std::shared_ptr<parameter>> pp(
        "PTHSKParameter",
        "Contains the parameters to the methods used in the PTHSK assembly\n"
        "priestley_taylor,hbv_snow,actual_evapotranspiration,precipitation_correction,kirchner\n");
      pp
        .def(py::init<
             priestley_taylor::parameter const &,
             hbv_snow::parameter const &,
             actual_evapotranspiration::parameter const &,
             kirchner::parameter const &,
             precipitation_correction::parameter const &,
             py::optional<glacier_melt::parameter, routing::uhg_parameter, mstack_parameter>>(
          (py::arg("pt"),
           py::arg("snow"),
           py::arg("ae"),
           py::arg("k"),
           py::arg("p_corr"),
           py::arg("gm"),
           py::arg("routing"),
           py::arg("msp")),
          "create object with specified parameters"))
        .def(py::init< parameter const &>((py::arg("p")), "clone a parameter"))
        .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
        .def_readwrite("ae", &parameter::ae, "ActualEvapotranspirationParameter: actual evapotranspiration parameter")
        .def_readwrite("hs", &parameter::hs, "HbvSnowParameter: hbv-snow parameter")
        .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
        .def_readwrite("kirchner", &parameter::kirchner, "KirchnerParameter: kirchner parameter")
        .def_readwrite(
          "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
        .def_readwrite(
          "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters")
        .def_readwrite("msp", &parameter::msp, "MethodStackParameter: contains the method stack parameters")
        .def("size", &parameter::size, "returns total number of calibration parameters")
        .def(
          "set",
          &parameter::set,
          (py::arg("self"), py::arg("p")),
          "set parameters from vector/list of float, ordered as by get_name(i)")
        .def(
          "get",
          &parameter::get,
          (py::arg("self"), py::arg("i")),
          "return the value of the i'th parameter, name given by .get_name(i)")
        .def(
          "get_name",
          &parameter::get_name,
          (py::arg("self"), py::arg("i")),
          "returns the i'th parameter name, see also .get()/.set() and .size()")
        .def(
          "serialize",
          &serialize_to_bytes<parameter>,
          (py::arg("self")),
          "serializes the parameters to a blob, that later can be passed in to .deserialize()")
        .def("deserialize", &deserialize_from_bytes<parameter>, (py::arg("blob")))
        .staticmethod("deserialize")
        .def(py::self == py::self)
        .def(py::self != py::self);
      shyft::pyapi::expose_format(pp);
      expose_map<std::int64_t, parameter_t_>(
        "PTHSKParameterMap",
        "dict (int,parameter)  where the int is the catchment_id",
        true, // comparable
        false // shared_ptr, so no proxy
      );


      py::class_<state>("PTHSKState")
        .def(py::init<hbv_snow::state, kirchner::state>(
          (py::arg("snow"), py::arg("k")), "initializes state with hbv-snow gs and kirchner k"))
        .def_readwrite("snow", &state::snow, "HbvSnowState: hbv-snow state")
        .def_readwrite("kirchner", &state::kirchner, "KirchnerState: kirchner state");

      typedef std::vector<state> PTHSKStateVector;
      py::class_<PTHSKStateVector, py::bases<>, std::shared_ptr<PTHSKStateVector> >("PTHSKStateVector")
        .def(py::vector_indexing_suite<PTHSKStateVector>());
      py::class_<response>(
        "PTHSKResponse", "This struct contains the responses of the methods used in the PTHSK assembly")
        .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
        .def_readwrite("snow", &response::snow, "HbvSnowResponse: hbv-snow response")
        .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
        .def_readwrite("ae", &response::ae, "ActualEvapotranspirationResponse: actual evapotranspiration response")
        .def_readwrite("kirchner", &response::kirchner, "KirchnerResponse: kirchner response")
        .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
    }

    static void collectors() {
      typedef shyft::core::pt_hs_k::all_response_collector PTHSKAllCollector;
      py::class_<PTHSKAllCollector>("PTHSKAllCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTHSKAllCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTHSKAllCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m3/s] for the timestep")
        .def_readonly(
          "snow_sca",
          &PTHSKAllCollector::snow_sca,
          "TsFixed: hbv snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTHSKAllCollector::snow_swe,
          "TsFixed: hbv snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly(
          "snow_outflow", &PTHSKAllCollector::snow_outflow, "TsFixed: hbv snow output [m^3/s] for the timestep")
        .def_readonly(
          "glacier_melt", &PTHSKAllCollector::glacier_melt, "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
        .def_readonly("ae_output", &PTHSKAllCollector::ae_output, "TsFixed: actual evap mm/h")
        .def_readonly("pe_output", &PTHSKAllCollector::pe_output, "TsFixed: pot evap mm/h")
        .def_readonly(
          "end_reponse", &PTHSKAllCollector::end_reponse, "PTHSKResponse: end_response, at the end of collected")
        .def_readonly("avg_charge", &PTHSKAllCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

      typedef shyft::core::pt_hs_k::discharge_collector PTHSKDischargeCollector;
      py::class_<PTHSKDischargeCollector>("PTHSKDischargeCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTHSKDischargeCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTHSKDischargeCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
        .def_readonly(
          "snow_sca",
          &PTHSKDischargeCollector::snow_sca,
          "TsFixed: hbv snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTHSKDischargeCollector::snow_swe,
          "TsFixed: hbv snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly(
          "end_reponse", &PTHSKDischargeCollector::end_response, "PTHSKResponse: end_response, at the end of collected")
        .def_readwrite(
          "collect_snow", &PTHSKDischargeCollector::collect_snow, "bool: controls collection of snow routine")
        .def_readonly(
          "avg_charge", &PTHSKDischargeCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");
      typedef shyft::core::pt_hs_k::null_collector PTHSKNullCollector;
      py::class_<PTHSKNullCollector>(
        "PTHSKNullCollector",
        "collector that does not collect anything, useful during calibration to minimize memory&maximize speed");

      typedef shyft::core::pt_hs_k::state_collector PTHSKStateCollector;
      py::class_<PTHSKStateCollector>("PTHSKStateCollector", "collects state, if collect_state flag is set to true")
        .def_readwrite(
          "collect_state",
          &PTHSKStateCollector::collect_state,
          "if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
        .def_readonly(
          "kirchner_discharge",
          &PTHSKStateCollector::kirchner_discharge,
          "TsFixed: Kirchner state instant Discharge given in m^3/s")
        .def_readonly("snow_swe", &PTHSKStateCollector::snow_swe, "TsFixed: swe")
        .def_readonly("snow_sca", &PTHSKStateCollector::snow_sca, "TsFixed: sca")
        .def_readonly("snow_sp", &PTHSKStateCollector::sp, "CoreTsVector: sp")
        .def_readonly("snow_sw", &PTHSKStateCollector::sw, "CoreTsVector: sw")

        ;
    }

    static void cells() {
      typedef shyft::core::cell<parameter, state, state_collector, all_response_collector> PTHSKCellAll;
      typedef shyft::core::cell<parameter, state, null_collector, discharge_collector> PTHSKCellOpt;
      expose::cell<PTHSKCellAll>("PTHSKCellAll", "tbd: PTHSKCellAll doc");
      expose::cell<PTHSKCellOpt>("PTHSKCellOpt", "tbd: PTHSKCellOpt doc");
      expose::statistics::hbv_snow<PTHSKCellAll>(
        "PTHSKCell"); // it only gives meaning to expose the *All collect cell-type
      expose::statistics::actual_evapotranspiration<PTHSKCellAll>("PTHSKCell");
      expose::statistics::priestley_taylor<PTHSKCellAll>("PTHSKCell");
      expose::statistics::kirchner<PTHSKCellAll>("PTHSKCell");
      expose::cell_state_etc<PTHSKCellAll>("PTHSK"); // just one expose of state
    }

    static void models() {
      typedef shyft::core::region_model<pt_hs_k::cell_discharge_response_t, shyft::api::a_region_environment>
        PTHSKOptModel;
      typedef shyft::core::region_model<pt_hs_k::cell_complete_response_t, shyft::api::a_region_environment> PTHSKModel;
      expose::model<PTHSKModel>("PTHSKModel", "PTHSK");
      expose::model<PTHSKOptModel>("PTHSKOptModel", "PTHSK");
      def_clone_to_similar_model<PTHSKModel, PTHSKOptModel>("create_opt_model_clone");
      def_clone_to_similar_model<PTHSKOptModel, PTHSKModel>("create_full_model_clone");
    }

    static void model_calibrator() {
      expose::model_calibrator<
        shyft::core::region_model<pt_hs_k::cell_discharge_response_t, shyft::api::a_region_environment>>(
        "PTHSKOptimizer");
    }
}}

BOOST_PYTHON_MODULE(_pt_hs_k) {

  expose::py::scope().attr("__doc__") = "Shyft python api for the pt_hs_k model";
  expose::py::def("version", version);
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::pt_hs_k::parameter_state_response();
  expose::pt_hs_k::cells();
  expose::pt_hs_k::models();
  expose::pt_hs_k::collectors();
  expose::pt_hs_k::model_calibrator();
}
