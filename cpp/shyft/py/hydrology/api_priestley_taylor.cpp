/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/priestley_taylor.h>

#include <shyft/py/api/bindings.h>

namespace expose {

  void priestley_taylor() {
    using namespace shyft::core::priestley_taylor;

    py::class_<parameter>("PriestleyTaylorParameter")
      .def(py::init<py::optional<double, double>>(
        (py::arg("albedo"), py::arg("alpha")), "a new object with specified parameters"))
      .def_readwrite("albedo", &parameter::albedo, "float: typical value 0.2")
      .def_readwrite("alpha", &parameter::alpha, "float: typical value 1.26");

    py::class_<response>("PriestleyTaylorResponse")
      .def_readwrite("pot_evapotranspiration", &response::pot_evapotranspiration, "float: response");

    py::class_<calculator>(
      "PriestleyTaylorCalculator",
      "PriestleyTaylor,PT, (google for PriestleyTaylor)\n"
      "primitive implementation for calculating the potential evaporation.\n"
      "This function is plain and simple, taking land_albedo and PT.alpha\n"
      "into the constructor and provides a function that calculates potential evapotransporation\n"
      "[mm/s] units.\n",
      py::no_init)
      .def(py::init<double, double>((py::arg("land_albedo"), py::arg("alpha"))))
      .def(
        "potential_evapotranspiration",
        &calculator::potential_evapotranspiration,
        (py::arg("temperature"), py::arg("global_radiation"), py::arg("rhumidity")),
        "Calculate PotentialEvapotranspiration, given specified parameters\n"
        "\n"
        "   * param temperature in [degC]\n"
        "   * param global_radiation [W/m2]\n"
        "   * param rhumidity in interval [0,1]\n"
        "   * return PotentialEvapotranspiration in [mm/s] units\n"
        "   *\n");
  }
}
