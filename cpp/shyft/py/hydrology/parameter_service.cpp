/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <future>
#include <mutex>
#include <atomic>
#include <shyft/py/api/bindings.h>
#include <shyft/py/scoped_gil.h>
#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_boost_variant.h>

namespace expose {
  using std::mutex;
  using std::unique_lock;
  using std::string;
  using std::vector;
  using std::shared_ptr;

  using shyft::hydrology::srv::parameter_map_variant_t;

  using namespace shyft::pyapi;
  using shyft::time_series::dd::apoint_ts;
  using shyft::hydrology::srv::parameter_model;
  using parameter_model_ = std::shared_ptr<parameter_model>;

  namespace {
    /// helper to convert c++variant to its real type that is understood by python
    struct x_model {
      static py::object parameters(py::tuple const & args, py::dict /*kwargs*/) {
        // consider py::len(args)>0 check
        auto me = py::extract<parameter_model*>(args[0])();
        if (!me)
          return py::object(nullptr);
        return boost::apply_visitor(
          [](auto const & x) -> py::object {
            return py::object(x);
          },
          me->parameters);
      }
    };
  }

  void def_parameter_model() {
    py::class_<parameter_model, py::bases<>, parameter_model_> m(
      "ParameterModel",
      "A model identifier and a strongly typed dict, cpp-map,  of stack parameters, suitable for use with a "
      "region-model matching the type.");

    m.def_readwrite("id", &parameter_model::id, "int: unique model id")
      .add_property(
        "parameters",
        py::raw_function(
          x_model::parameters),       // getter returns ready converted type so that user get a known type back.
        &parameter_model::parameters, // the setter, accepts the property, that automagically assigns the user supplied
                                      // type.
        "Any: kind of parameter with id ")
      .def(py::self == py::self);

    shyft::pyapi::expose_format(m);
    using parameter_model_vector = std::vector<parameter_model_>;
    py::class_<parameter_model_vector> mv("ParameterModelVector", "A strongly typed list of ParameterModels");
    mv.def(py::vector_indexing_suite<parameter_model_vector, true>());
    shyft::pyapi::expose_format(mv);
    detail::expose_eq_ne(mv);
    detail::expose_clone(mv);
    register_boost_variant(std::type_identity<parameter_map_variant_t>{});
  }

  void parameter_client_server() {
    using shyft::hydrology::srv::parameter_model;
    using model = parameter_model;
    using client_t = shyft::pyapi::energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = shyft::pyapi::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;
    def_parameter_model();
    shyft::pyapi::energy_market::expose_client<client_t>(
      "ParameterClient", "The client api for the shyft.hydrology parameter model service.");
    shyft::pyapi::energy_market::expose_server<srv_t>(
      "ParameterServer", "The server-side component for the shyft.hydrology parameter model repository.");
  }
}
