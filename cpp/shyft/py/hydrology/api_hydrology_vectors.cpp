/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include <shyft/py/api/bindings.h>
#include <numpy/arrayobject.h>
#include <shyft/py/api/numpy_boost_python.hpp>
#include <shyft/py/api/py_convertible.h>

namespace expose {
  using namespace shyft::core;

  using std::vector;
  using std::string;

  namespace sa = shyft::api;
  namespace sc = shyft::core;
  namespace ts = shyft::time_series;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::gta_t;

  static void* np_import() {
    import_array();
    return nullptr;
  }

  template <class T>
  static T x_arg(py::tuple const & args, size_t i) {
    if (py::len(args) + 1 < (int) i)
      throw std::runtime_error("missing arg #" + std::to_string(i) + std::string(" in time"));
    py::object o = args[i];
    py::extract<T> xtract_arg(o);
    return xtract_arg();
  }

  /* helper to expose serialize and deserialize */
  struct geo_cell_data_vector_ext {
    static py::object serialize_to_string(py::tuple const & args, py::dict const & /* kwpy::args*/) {
      auto const & gcd = x_arg< vector<shyft::core::geo_cell_data> const &>(args, 0);
      // serialize to binary string
      //
      std::ostringstream bs;
      core_oarchive oa(bs, core_arch_flags);
      oa << core_nvp("geo_cell_data_vector", gcd);
      bs.flush();
      auto bss = bs.str();
      return py::object(vector<char>(begin(bss), end(bss)));
    }

    static py::object deserialize_from_string(py::tuple const & args, py::dict const & /*kwpy::args*/) {
      auto bss = x_arg<vector<char>>(args, 0);
      //-- de-serialize from
      std::istringstream xmli(string(bss.data(), bss.size()));
      core_iarchive ia(xmli, core_arch_flags);
      vector<geo_cell_data> gcd;
      ia >> core_nvp("geo_cell_data_vector", gcd);
      return py::object(gcd);
    }
  };

  template <class S>
  vector<S> create_from_geo_tsv_from_np(
    gta_t const & ta,
    vector<sc::geo_point> const & gpv,
    numpy_boost<double, 2> const & a,
    ts::ts_point_fx point_fx) {
    vector<S> r;
    size_t n_ts = a.shape()[0];
    size_t n_pts = a.shape()[1];
    if (ta.size() != n_pts)
      throw std::runtime_error("time-axis should have same length as second dim in numpy array");
    if (n_ts != gpv.size())
      throw std::runtime_error("geo-point vector should have same size as first dim (n_ts) in numpy array");
    r.reserve(n_ts);
    for (size_t i = 0; i < n_ts; ++i) {
      std::vector<double> v;
      v.reserve(n_pts);
      for (size_t j = 0; j < n_pts; ++j)
        v.emplace_back(a[i][j]);
      r.emplace_back(gpv[i], sa::apoint_ts(ta, v, point_fx));
    }
    return r;
  }

  template <class T>
  static vector<T> FromNdArray(numpy_boost<T, 1> const & npv) {
    vector<T> r;
    r.reserve(npv.shape()[0]);
    for (size_t i = 0; i < npv.shape()[0]; ++i) {
      r.push_back(npv[i]);
    }
    return r;
  }

  template <class T>
  static numpy_boost<T, 1> ToNpArray(vector<T> const & v) {
    int dims[] = {int(v.size())};
    numpy_boost<T, 1> r(dims);
    for (size_t i = 0; i < r.size(); ++i) {
      r[i] = v[i];
    }
    return r;
  }

  static void expose_geo_cell_data_vector() {
    typedef std::vector<shyft::core::geo_cell_data> GeoCellDataVector;
    py::class_<GeoCellDataVector>("GeoCellDataVector", "A vector, list, of GeoCellData")
      .def(py::vector_indexing_suite<GeoCellDataVector>())
      .def(py::init< GeoCellDataVector const &>((py::arg("const_ref_v"))))
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "serialize",
        py::raw_function(&geo_cell_data_vector_ext::serialize_to_string, 1),
        doc.intro("serialize to a binary byte vector")())
      .def(
        "deserialize",
        py::raw_function(&geo_cell_data_vector_ext::deserialize_from_string, 1),
        doc.intro("deserialize from a binary to GeoCellDataVector")())
      .staticmethod("deserialize");
    py_api::iterable_converter().from_python<GeoCellDataVector>();
  }

  static void expose_geo_ts_vector_create() {
    py::def(
      "create_temperature_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::TemperatureSource>,
      (py::arg("time_axis"), py::arg("geo_points"), py::arg("np_array"), py::arg("point_fx")),
      doc.intro("Create a TemperatureSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "TemperatureSourceVector",
          "a TemperatureSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")());

    py::def(
      "create_precipitation_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::PrecipitationSource>,
      (py::arg("time_axis"), py::arg("geo_points"), py::arg("np_array"), py::arg("point_fx")),
      doc.intro("Create a PrecipitationSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "PrecipitationSourceVector",
          "a PrecipitationSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")());

    py::def(
      "create_wind_speed_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::WindSpeedSource>,
      (py::arg("time_axis"), py::arg("geo_points"), py::arg("np_array"), py::arg("point_fx")),
      doc.intro("Create a WindSpeedSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "WindSpeedSourceVector",
          "a WindSpeedSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")());

    py::def(
      "create_rel_hum_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::RelHumSource>,
      (py::arg("time_axis"), py::arg("geo_points"), py::arg("np_array"), py::arg("point_fx")),
      doc.intro("Create a RelHumSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "RelHumSourceVector",
          "a RelHumSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")());

    py::def(
      "create_radiation_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::RadiationSource>,
      (py::arg("time_axis"), py::arg("geo_points"), py::arg("np_array"), py::arg("point_fx")),
      doc.intro("Create a RadiationSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "RadiationSourceVector",
          "a RadiationSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")());
    numpy_boost_python_register_type<double, 2>();
  }

  void hydrology_vectors() {
    np_import();
    // may be numpy_boost_python_register_type<int, 1>();
    expose_geo_cell_data_vector();
    expose_geo_ts_vector_create();
  }
}
