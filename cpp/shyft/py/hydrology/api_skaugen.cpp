/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/skaugen.h>

#include <shyft/py/api/bindings.h>

namespace expose {

  void skaugen_snow() {
    using namespace shyft::core::skaugen;

    py::class_<parameter>("SkaugenParameter")
      .def(py::init<double, double, double, double, double, double, double, double>(
        (py::arg("alpha_0"),
         py::arg("d_range"),
         py::arg("unit_size"),
         py::arg("max_water_fraction"),
         py::arg("tx"),
         py::arg("cx"),
         py::arg("ts"),
         py::arg("cfr")),
        "create parameter object with specifed values"))
      .def(py::init< parameter const &>((py::arg("p")), "create a clone of p"))
      .def_readwrite("alpha_0", &parameter::alpha_0, "float: default = 40.77")
      .def_readwrite("d_range", &parameter::d_range, "float: default = 113.0")
      .def_readwrite("unit_size", &parameter::unit_size, "float: default = 0.1")
      .def_readwrite("max_water_fraction", &parameter::max_water_fraction, "float: default = 0.1")
      .def_readwrite("tx", &parameter::tx, "float: default = 0.16")
      .def_readwrite("cx", &parameter::cx, "float: default = 2.5")
      .def_readwrite("ts", &parameter::ts, "float: default = 0.14")
      .def_readwrite("cfr", &parameter::cfr, "float: default = 0.01");

    py::class_<state>("SkaugenState")
      .def(py::init<double, py::optional<double, double, double, double, double, double>>(
        (py::arg("nu"),
         py::arg("alpha"),
         py::arg("sca"),
         py::arg("swe"),
         py::arg("free_water"),
         py::arg("residual"),
         py::arg("num_units")),
        "create a state with specified values"))
      .def(py::init< state const &>((py::arg("s")), "create a clone of s"))
      .def_readwrite("nu", &state::nu, "float: ..")
      .def_readwrite("alpha", &state::alpha, "float: ..")
      .def_readwrite("sca", &state::sca, "float: ..")
      .def_readwrite("swe", &state::swe, "float: ..")
      .def_readwrite("free_water", &state::free_water, "float: ..")
      .def_readwrite("residual", &state::residual, "float: ..")
      .def_readwrite("num_units", &state::num_units, "int: ..");

    py::class_<response>("SkaugenResponse")
      .def_readwrite("sca", &response::sca, "float: snow-covered area in fraction")
      .def_readwrite("swe", &response::swe, "float: snow water equivalient [mm] over cell-area")
      .def_readwrite("outflow", &response::outflow, "float: from snow-routine in [mm/h] over cell-area")
      .def_readwrite("total_stored_water", &response::swe, "float: same as swe(deprecated)");

    typedef calculator SkaugenCalculator;
    py::class_<SkaugenCalculator>(
      "SkaugenCalculator",
      "Skaugen snow model method\n"
      "\n"
      "This algorithm uses theory from Skaugen \n"
      "\n")
      .def(
        "step",
        &SkaugenCalculator::step,
        (py::arg("delta_t"),
         py::arg("parameter"),
         py::arg("temperature"),
         py::arg("precipitation"),
         py::arg("radiation"),
         py::arg("wind_speed"),
         py::arg("state"),
         py::arg("response")),
        "steps the model forward delta_t seconds, using specified input, updating state and response");
  }
}
