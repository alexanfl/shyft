/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/bindings.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/hbv_soil.h>
#include <shyft/hydrology/methods/hbv_tank.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/stacks/pt_st_hbv.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/stacks/pt_st_hbv_cell_model.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>


#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/api/expose_container.h>

static char const *version() {
  return "v1.0";
}

namespace expose { namespace pt_st_hbv {
    using namespace shyft::core;
    using namespace shyft::core::pt_st_hbv;

    static void parameter_state_response() {
      py::class_<parameter, py::bases<>, std::shared_ptr<parameter>> pp(
        "PTSTHBVParameter",
        "Contains the parameters to the methods used in the PTSTHBV assembly\n"
        "priestley_taylor, snow_tiles, precipitation_correction, hbv_soil, hbv_tank\n");
      pp
        .def(py::init<
             priestley_taylor::parameter const &,
             snow_tiles::parameter const &,
             hbv_soil::parameter const &,
             hbv_tank::parameter const &,
             precipitation_correction::parameter const &,
             py::optional<glacier_melt::parameter, routing::uhg_parameter, mstack_parameter>>(
          (py::arg("pt"),
           py::arg("snow"),
           py::arg("hbv_soil"),
           py::arg("hbv_tank"),
           py::arg("p_corr"),
           py::arg("gm"),
           py::arg("routing"),
           py::arg("msp")),
          "create object with specified parameters"))
        .def(py::init< parameter const &>((py::arg("p")), "clone a parameter"))
        .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
        .def_readwrite("st", &parameter::st, "SnowTilesParameter: snow_tiles parameter")
        .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
        .def_readwrite("soil", &parameter::soil, "HbvSoilParameter: hbv soil parameter")
        .def_readwrite("tank", &parameter::tank, "HbvTankParameter: hbv tank parameter")
        .def_readwrite(
          "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
        .def_readwrite(
          "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters")
        .def_readwrite("msp", &parameter::msp, "MethodStackParameter: contains the method stack parameters")
        .def("size", &parameter::size, "returns total number of calibration parameters")
        .def(
          "set",
          &parameter::set,
          (py::arg("self"), py::arg("p")),
          "set parameters from vector/list of float, ordered as by get_name(i)")
        .def(
          "get",
          &parameter::get,
          (py::arg("self"), py::arg("i")),
          "return the value of the i'th parameter, name given by .get_name(i)")
        .def(
          "get_name",
          &parameter::get_name,
          (py::arg("self"), py::arg("i")),
          "returns the i'th parameter name, see also .get()/.set() and .size()")
        .def(
          "serialize",
          &serialize_to_bytes<parameter>,
          (py::arg("self")),
          "serializes the parameters to a blob, that later can be passed in to .deserialize()")
        .def("deserialize", &deserialize_from_bytes<parameter>, (py::arg("blob")))
        .staticmethod("deserialize")
        .def(py::self == py::self)
        .def(py::self != py::self);
      shyft::pyapi::expose_format(pp);

      expose_map<std::int64_t, parameter_t_>(
        "PTSTHBVParameterMap",
        "dict (int,parameter)  where the int is the catchment_id",
        true, // comparable
        false // shared_ptr, so no proxy
      );

      py::class_<state>("PTSTHBVState")
        .def(py::init<snow_tiles::state, hbv_soil::state, hbv_tank::state>(
          (py::arg("snow"), py::arg("hbv_soil"), py::arg("hbv_tank")),
          "initializes state with snow_tiles snow, and hbv_soil soil and hbv_tank tank"))
        .def_readwrite("snow", &state::snow, "SnowTilesState: snow_tiles state")
        .def_readwrite("soil", &state::soil, "HbvSoilState: hbv_soil state")
        .def_readwrite("tank", &state::tank, "HbvTankState: hbv_tank state");

      typedef std::vector<state> PTSTHBVStateVector;
      py::class_<PTSTHBVStateVector, py::bases<>, std::shared_ptr<PTSTHBVStateVector> >("PTSTHBVStateVector")
        .def(py::vector_indexing_suite<PTSTHBVStateVector>());
      py::class_<response>(
        "PTSTHBVResponse", "This struct contains the responses of the methods used in the PTSTHBV assembly")
        .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
        .def_readwrite("snow", &response::snow, "SnowTilesResponse: snow-tiles method response")
        .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
        .def_readwrite("soil", &response::soil, "ActualEvapotranspirationResponse: hbv_soil response")
        .def_readwrite("tank", &response::tank, "HbvTankResponse: hbv_tank response")
        .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
    }

    static void collectors() {
      typedef shyft::core::pt_st_hbv::all_response_collector PTSTHBVAllCollector;
      py::class_<PTSTHBVAllCollector>("PTSTHBVAllCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTSTHBVAllCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTSTHBVAllCollector::avg_discharge,
          "TsFixed: HBV Discharge given in [m3/s] for the timestep")

        .def_readonly(
          "soil_ae",
          &PTSTHBVAllCollector::soil_ae,
          "TsFixed: HBV soil actual evaporation given in [mm/h] for the timestep")
        .def_readonly(
          "inuz",
          &PTSTHBVAllCollector::inuz,
          "TsFixed: HBV soil perculation to upper zone given in [mm/h] for the timestep")
        .def_readonly(
          "elake", &PTSTHBVAllCollector::elake, "TsFixed: HBV tank lake evaporation given in [mm/h] for the timestep")
        .def_readonly("qlz", &PTSTHBVAllCollector::qlz, "TsFixed: HBV tank lower zone in [m^3/s] for the timestep")
        .def_readonly(
          "quz0", &PTSTHBVAllCollector::quz0, "TsFixed: HBV tank upper zone slow response in [m^3/s] for the timestep")
        .def_readonly(
          "quz1", &PTSTHBVAllCollector::quz1, "TsFixed: HBV tank upper zone mid response in [m^3/s] for the timestep")
        .def_readonly(
          "quz2", &PTSTHBVAllCollector::quz2, "TsFixed: HBV tank upper zone fast response in [m^3/s] for the timestep")

        .def_readonly(
          "snow_sca",
          &PTSTHBVAllCollector::snow_sca,
          "TsFixed: snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTSTHBVAllCollector::snow_swe,
          "TsFixed: snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly(
          "snow_outflow", &PTSTHBVAllCollector::snow_outflow, "TsFixed: snow output [m^3/s] for the timestep")
        .def_readonly(
          "glacier_melt", &PTSTHBVAllCollector::glacier_melt, "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
        .def_readonly("pe_output", &PTSTHBVAllCollector::pe_output, "TsFixed: pot evap mm/h")
        .def_readonly(
          "end_reponse", &PTSTHBVAllCollector::end_reponse, "PTSTHBVResponse: end_response, at the end of collected")
        .def_readonly("avg_charge", &PTSTHBVAllCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

      typedef shyft::core::pt_st_hbv::discharge_collector PTSTHBVDischargeCollector;
      py::class_<PTSTHBVDischargeCollector>("PTSTHBVDischargeCollector", "collect all cell response from a run")
        .def_readonly(
          "destination_area", &PTSTHBVDischargeCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTSTHBVDischargeCollector::avg_discharge,
          "TsFixed: HBV Discharge given in [m^3/s] for the timestep")
        .def_readonly(
          "snow_sca",
          &PTSTHBVDischargeCollector::snow_sca,
          "TsFixed: snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTSTHBVDischargeCollector::snow_swe,
          "TsFixed: snow swe, [mm] over the cell area, - at the end of timestep")
        .def_readonly(
          "end_reponse",
          &PTSTHBVDischargeCollector::end_response,
          "PTSTHBVResponse: end_response, at the end of collected")
        .def_readwrite(
          "collect_snow", &PTSTHBVDischargeCollector::collect_snow, "bool: controls collection of snow routine")
        .def_readonly(
          "avg_charge", &PTSTHBVDischargeCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");
      typedef shyft::core::pt_st_hbv::null_collector PTSTHBVNullCollector;
      py::class_<PTSTHBVNullCollector>(
        "PTSTHBVNullCollector",
        "collector that does not collect anything, useful during calibration to minimize memory&maximize speed");

      typedef shyft::core::pt_st_hbv::state_collector PTSTHBVStateCollector;
      py::class_<PTSTHBVStateCollector>("PTSTHBVStateCollector", "collects state, if collect_state flag is set to true")
        .def_readwrite(
          "collect_state",
          &PTSTHBVStateCollector::collect_state,
          "bool: if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
        .def_readonly("soil_sm", &PTSTHBVStateCollector::soil_sm, "TsFixed: HBV soil moisture state [mm]")
        .def_readonly("tank_uz", &PTSTHBVStateCollector::tank_uz, "TsFixed: HBV tank upper zone state [mm]")
        .def_readonly("tank_lz", &PTSTHBVStateCollector::tank_lz, "TsFixed: HBV tank lower zone state [mm]")
        .def_readonly("snow_sp", &PTSTHBVStateCollector::fw, "CoreTsVector: raw snow-tiles state-data for 'fw'")
        .def_readonly("snow_sw", &PTSTHBVStateCollector::lw, "CoreTsVector: raw snow-tiles state-data for 'lw'")
        .add_property(
          "snow_sca",
          +[](PTSTHBVStateCollector const &a) {
            return shyft::time_series::dd::apoint_ts(a.sca_());
          },
          "TimeSeries: Snow covered area, derived from snow_sp, snow_sw, snow-tiles parameter and snow_fraction")
        .add_property(
          "snow_swe",
          +[](PTSTHBVStateCollector const &a) {
            return shyft::time_series::dd::apoint_ts(a.swe_());
          },
          "TimeSeries: Snow water-equivalent, derived from snow_sp, snow_sw, snow-tiles parameter and snow_fraction")

        ;
    }

    static void cells() {
      typedef shyft::core::cell<parameter, state, state_collector, all_response_collector> PTSTHBVCellAll;
      typedef shyft::core::cell<parameter, state, null_collector, discharge_collector> PTSTHBVCellOpt;
      expose::cell<PTSTHBVCellAll>("PTSTHBVCellAll", "tbd: PTSTHBVCellAll doc");
      expose::cell<PTSTHBVCellOpt>("PTSTHBVCellOpt", "tbd: PTSTHBVCellOpt doc");
      expose::statistics::priestley_taylor<PTSTHBVCellAll>("PTSTHBVCell");
      expose::statistics::hbv_soil<PTSTHBVCellAll>("PTSTHBVCell");
      expose::statistics::hbv_tank<PTSTHBVCellAll>("PTSTHBVCell");
      expose::statistics::snow_tiles<PTSTHBVCellAll>("PTSTHBVCell");
      expose::cell_state_etc<PTSTHBVCellAll>("PTSTHBV"); // just one expose of state
    }

    static void models() {
      typedef shyft::core::region_model<pt_st_hbv::cell_discharge_response_t, shyft::api::a_region_environment>
        PTSTHBVOptModel;
      typedef shyft::core::region_model<pt_st_hbv::cell_complete_response_t, shyft::api::a_region_environment>
        PTSTHBVModel;
      expose::model<PTSTHBVModel>("PTSTHBVModel", "PTSTHBV");
      expose::model<PTSTHBVOptModel>("PTSTHBVOptModel", "PTSTHBV");
      def_clone_to_similar_model<PTSTHBVModel, PTSTHBVOptModel>("create_opt_model_clone");
      def_clone_to_similar_model<PTSTHBVOptModel, PTSTHBVModel>("create_full_model_clone");
    }

    static void model_calibrator() {
      expose::model_calibrator<
        shyft::core::region_model<pt_st_hbv::cell_discharge_response_t, shyft::api::a_region_environment>>(
        "PTSTHBVOptimizer");
    }
}}

BOOST_PYTHON_MODULE(_pt_st_hbv) {

  expose::py::scope().attr("__doc__") = "Shyft python api for the pt_st_hbv model";
  expose::py::def("version", version);
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::pt_st_hbv::parameter_state_response();
  expose::pt_st_hbv::cells();
  expose::pt_st_hbv::models();
  expose::pt_st_hbv::collectors();
  expose::pt_st_hbv::model_calibrator();
}
