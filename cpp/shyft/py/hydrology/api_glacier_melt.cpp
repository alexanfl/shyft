/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/py/api/bindings.h>

namespace expose {

  using namespace shyft::core::glacier_melt;

  void glacier_melt() {
    py::class_<parameter>("GlacierMeltParameter")
      .def(py::init<double, py::optional<double>>(
        (py::arg("dtf"), py::arg("direct_response") = 0.0), "create parameter object with specified values"))
      .def_readwrite("dtf", &parameter::dtf, "float: degree timestep factor, default=6.0 [mm/day/degC]")
      .def_readwrite(
        "direct_response",
        &parameter::direct_response,
        "float: fraction that goes as direct response, (1-fraction) is routed through soil/kirchner "
        "routine,default=0.0");
    py::def(
      "glacier_melt_step",
      step,
      (py::arg("dtf"), py::arg("temperature"), py::arg("sca"), py::arg("glacier_fraction")),
      doc.intro("Calculates outflow from glacier melt rate [mm/h].")
        .parameters()
        .parameter(
          "dtf",
          "float",
          "degree timestep factor [mm/day/deg.C]; lit. values for Norway: 5.5 - 6.4 in Hock, R. (2003), J. Hydrol., "
          "282, 104-115.")
        .parameter("temperature", "float", "degC, considered constant over timestep dt")
        .parameter(
          "sca",
          "float",
          "fraction of snow cover in cell [0..1], glacier melt occurs only if glacier fraction > snow fraction")
        .parameter("glacier_fraction", "float", "glacier fraction [0..1] in the total area")
        .returns("glacier_melt", "float", "output from glacier, melt rate [mm/h]")());
  }
}
