/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/hbv_soil.h>
#include <shyft/py/api/bindings.h>

namespace expose {

  void hbv_soil() {
    using namespace shyft::core::hbv_soil;

    py::class_<parameter>("HbvSoilParameter")
      .def(py::init<py::optional<double, double>>(
        (py::arg("fc"), py::arg("beta")), "create parameter object with specifed values"))
      .def_readwrite(
        "fc", &parameter::fc, "float: (mm) maximum water content of the soil (Field Capacity), default=32.2755689")
      .def_readwrite(
        "lpdel",
        &parameter::lpdel,
        "float: (unitless) dimensionless parameter (<1), from which level evapotranspiration is potential. Some "
        "publications use fcdel instead. default= 0.848557794")
      .def_readwrite(
        "beta",
        &parameter::beta,
        "float: (unitless) exponent in the nonlinear relationship between soil moisture and field capacity. "
        "default=1.50738409")
      .def_readwrite(
        "infmax",
        &parameter::infmax,
        "float: (mm/h) maximum input rate to the soil moisture zone. Excess water will go directly to upper ground "
        "water zone. default=2.0");

    py::class_<state>("HbvSoilState")
      .def(py::init<py::optional<double>>((py::arg("sm")), "create a state with specified values"))
      .def_readwrite("sm", &state::sm, "float: Soil  moisture [mm]. default=10");

    py::class_<response>("HbvSoilResponse")
      .def(py::init<py::optional<double, double>>(
        (py::arg("inuz"), py::arg("ae")), "create response object with specifed values"))
      .def_readwrite(
        "inuz",
        &response::inuz,
        "float: (mm/h) Perculation to upper ground water zone. Called `cuz` by Nils Roar. default=0")
      .def_readwrite("ae", &response::ae, "float: (mm/h) Actual evapotranspiration. default=0");

    typedef calculator HbvSoilCalculator;
    py::class_<HbvSoilCalculator>(
      "HbvSoilCalculator",
      doc.intro("Computing water through the soil moisture zone of the HBV model.")
        .intro("\n")
        .intro("Reference:\n")
        .intro(" * Nils Roar Sæhlthun: The Nordic HBV model 1996 "
               "https://publikasjoner.nve.no/publication/1996/publication1996_07.pdf\n")
        .intro("\n")
        .notes()
        .note("Lake and glacier are treated in the stack")(),
      py::no_init)
      .def(py::init< parameter const &, double>(
        (py::arg("parameter"), py::arg("land_fraction")), "creates a calculator with given parameter"))
      .def(
        "step",
        &HbvSoilCalculator::step,
        (py::arg("self"), py::arg("state"), py::arg("response"), py::arg("insoil"), py::arg("pe"), py::arg("sca")),
        doc.intro("One step of the model, given state, parameters and input.\n")
          .intro("Updates the state and response.\n")
          .parameters()
          .parameter("state", "HbvSoilState", " param of type S, in/out, ref template parameters")
          .parameter("response", "HbvSoilResponse", " param of type R, in/out, ref template parameters")
          .parameter("insoil", "float", "inflow to soil")
          .parameter("pe", "float", "potential evaporation")
          .parameter("sca", "float", "the response , out parameter.")
          .parameter(
            "land_fraction",
            "float",
            "Land-fraction of the cell. Not directly used/desribed in ref. 'Nils Roar', but introduced here to adopt "
            "to the stack.")());
  }
}
