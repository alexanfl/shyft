/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <future>
#include <mutex>
#include <atomic>
#include <shyft/py/api/bindings.h>
#include <shyft/py/scoped_gil.h>
#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/api/expose_container.h>

namespace expose {
  using std::mutex;
  using std::unique_lock;
  using std::string;
  using std::vector;
  using std::shared_ptr;

  using shyft::hydrology::srv::state_variant_t;

  using namespace shyft::pyapi;
  using shyft::time_series::dd::apoint_ts;

  namespace {
    /// helper to convert c++variant to its real type that is understood by python
    struct x_state_model {
      static py::object states(py::tuple const & args, py::dict /*kwargs*/) {
        // consider py::len(args)>0 check
        auto me = py::extract<shyft::hydrology::srv::state_model*>(args[0])();
        if (!me)
          return py::object(nullptr);
        return boost::apply_visitor(
          [](auto const & x) -> py::object {
            return py::object(x);
          },
          me->states);
      }
    };
  }

  void def_state_model() {
    using shyft::hydrology::srv::state_model;
    using state_model_ = std::shared_ptr<state_model>;
    py::class_<state_model, py::bases<>, state_model_> m(
      "StateModel",
      "A model identifier and a StateVector, where the vector can keep an uniform set of StateId of specified type");

    m.def_readwrite("id", &state_model::id, "int: unique model id")
      .add_property(
        "states",
        py::raw_function(
          x_state_model::states), // getter returns ready converted type so that user get a known type back.
        &state_model::states, // the setter, accepts the property, that automagically assigns the user supplied type.
        "Any: kind of state vector with id ")
      .def(py::self == py::self);

    shyft::pyapi::expose_format(m);
    using state_model_vector = std::vector<state_model_>;
    py::class_<state_model_vector> mv("StateModelVector", "A strongly typed list of StateModel's");
    mv.def(py::vector_indexing_suite<state_model_vector, true>());
    shyft::pyapi::expose_format(mv);
    detail::expose_eq_ne(mv);
    detail::expose_clone(mv);
  }

  void state_client_server() {
    using shyft::hydrology::srv::state_model;
    using model = state_model;
    using client_t = shyft::pyapi::energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = shyft::pyapi::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;
    def_state_model();
    shyft::pyapi::energy_market::expose_client<client_t>(
      "StateClient", "The client api for the shyft.hydrology state model service.");
    shyft::pyapi::energy_market::expose_server<srv_t>(
      "StateServer", "The server-side component for the shyft.hydrology state model repository.");
  }

}
