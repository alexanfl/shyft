/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/hbv_physical_snow.h>
#include <shyft/py/api/bindings.h>

namespace expose {

  void hbv_physical_snow() {
    using namespace shyft::core::hbv_physical_snow;

    py::class_<parameter>("HbvPhysicalSnowParameter")
      .def(py::init<
           py::optional<double, double, double, double, double, double, double, double, double, double, double, bool>>(
        (py::arg("tx"),
         py::arg("lw"),
         py::arg("cfr"),
         py::arg("wind_scale"),
         py::arg("wind_const"),
         py::arg("surface_magnitude"),
         py::arg("max_albedo"),
         py::arg("min_albedo"),
         py::arg("fast_albedo_decay_rate"),
         py::arg("slow_albedo_decay_rate"),
         py::arg("snowfall_reset_depth"),
         py::arg("calculate_iso_pot_energy")),
        "create parameter object with specifed values"))
      .def(py::init<
           vector<double> const &,
           vector<double> const &,
           py::optional<double, double, double, double, double, double, double, double, double, double, double, bool>>(
        (py::arg("snow_redist_factors"),
         py::arg("quantiles"),
         py::arg("tx"),
         py::arg("lw"),
         py::arg("cfr"),
         py::arg("wind_scale"),
         py::arg("wind_const"),
         py::arg("surface_magnitude"),
         py::arg("max_albedo"),
         py::arg("min_albedo"),
         py::arg("fast_albedo_decay_rate"),
         py::arg("slow_albedo_decay_rate"),
         py::arg("snowfall_reset_depth"),
         py::arg("calculate_iso_pot_energy")),
        "create a parameter with snow re-distribution factors, quartiles and optionally the other parameters"))
      .def(
        "set_snow_redistribution_factors",
        &parameter::set_snow_redistribution_factors,
        (py::arg("self"), py::arg("snow_redist_factors")))
      .def("set_snow_quantiles", &parameter::set_snow_quantiles, (py::arg("self"), py::arg("quantiles")))
      .def_readwrite("tx", &parameter::tx, "float: threshold temperature determining if precipitation is rain or snow")
      .def_readwrite("lw", &parameter::lw, "float: max liquid water content of the snow")
      .def_readwrite("cfr", &parameter::cfr, "float: cfr")
      .def_readwrite("wind_scale", &parameter::wind_scale, "float: slope in turbulent wind function [m/s]")
      .def_readwrite("wind_const", &parameter::wind_const, "float: intercept in turbulent wind function")
      .def_readwrite("surface_magnitude", &parameter::surface_magnitude, "float: surface layer magnitude")
      .def_readwrite("max_albedo", &parameter::max_albedo, "float: maximum albedo value")
      .def_readwrite("min_albedo", &parameter::min_albedo, "float: minimum albedo value")
      .def_readwrite(
        "fast_albedo_decay_rate", &parameter::fast_albedo_decay_rate, "float: albedo decay rate during melt [days]")
      .def_readwrite(
        "slow_albedo_decay_rate",
        &parameter::slow_albedo_decay_rate,
        "float: albedo decay rate in cold conditions [days]")
      .def_readwrite(
        "snowfall_reset_depth", &parameter::snowfall_reset_depth, "float: snowfall required to reset albedo [mm]")
      .def_readwrite(
        "calculate_iso_pot_energy",
        &parameter::calculate_iso_pot_energy,
        "float: whether or not to calculate the potential energy flux")
      .def_readwrite("s", &parameter::s, "DoubleVector: snow redistribution factors,default =1.0..")
      .def_readwrite("intervals", &parameter::intervals, "DoubleVector: snow quantiles list default 0, 0.25 0.5 1.0");

    py::class_<state>("HbvPhysicalSnowState")
      .def(py::init< vector<double> const &, vector<double> const &, py::optional<double, double, double>>(
        (py::arg("albedo"), py::arg("iso_pot_energy"), py::arg("surface_heat"), py::arg("swe"), py::arg("sca")),
        "create a state with specified values"))
      .def_readwrite("albedo", &state::albedo, "float: albedo (Broadband snow reflectivity fraction)")
      .def_readwrite(
        "iso_pot_energy",
        &state::iso_pot_energy,
        "float: iso_pot_energy (Accumulated energy assuming isothermal snow surface) [J/m2]")
      .def_readwrite("surface_heat", &state::surface_heat, "float: surface_heat (Snow surface cold content) [J/m2]")
      .def_readwrite("sw", &state::sw, "DoubleVector: snow water[mm]")
      .def_readwrite("sp", &state::sp, "DoubleVector: snow dry[mm]")

      .def_readwrite("swe", &state::swe, "float: snow water equivalent[mm]")
      .def_readwrite("sca", &state::sca, "float: snow covered area [0..1]")
      .def(
        "distribute",
        &state::distribute,
        (py::arg("self"), py::arg("p"), py::arg("force") = true),
        doc.intro("Distribute state according to parameter settings.")
          .parameters()
          .parameter("p", "HbvPhysicalSnowParameter", "descr")
          .parameter(
            "force",
            "bool",
            "default true, if false then only distribute if state vectors are of different size than parameters passed")
          .returns("", "None", "")());

    py::class_<response>("HbvPhysicalSnowResponse")
      .def_readwrite("outflow", &response::outflow, "float: from snow-routine in [mm]")
      .def_readwrite("hps_state", &response::hps_state, "HbvPhysicalSnowState: current state instance")
      .def_readwrite("sca", &response::sca, "float: snow-covered area")
      .def_readwrite("storage", &response::storage, "float: snow storage [mm]");

    typedef calculator HbvPhysicalSnowCalculator;
    py::class_<HbvPhysicalSnowCalculator>(
      "HbvPhysicalSnowCalculator",
      "Generalized quantile based HBV Physical Snow model method\n"
      "\n"
      "This algorithm uses arbitrary quartiles to model snow. No checks are performed to assert valid input.\n"
      "The starting points of the quantiles have to partition the unity, \n"
      "include the end points 0 and 1 and must be given in ascending order.\n"
      "\n",
      py::no_init)
      .def(py::init< parameter const &>(
        (py::arg("parameter")),
        "creates a calculator with given parameter and initial state, notice that state is updated in this call(hmm)"))
      .def(
        "step",
        &HbvPhysicalSnowCalculator::step,
        (py::arg("self"),
         py::arg("state"),
         py::arg("response"),
         py::arg("t"),
         py::arg("dt"),
         py::arg("temperature"),
         py::arg("rad"),
         py::arg("prec_mm_h"),
         py::arg("wind_speed"),
         py::arg("rel_hum")),
        "steps the model forward from t to t+dt, updating state and response")

      ;
  }
}
