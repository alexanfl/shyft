/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <fmt/core.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/srv/db.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/formatters.h>
#include <shyft/py/energy_market/py_model_client_server.h>

namespace expose {
  using namespace shyft::core;

  void def_gcd_model() {
    using gcd_model_ = std::shared_ptr<gcd_model>;
    py::class_<gcd_model, py::bases<>, gcd_model_> m(
      "GeoCellDataModel", "A model identifier and a geo-cell-data vector");
    m.def_readwrite("id", &gcd_model::id, "int: unique model identifier")
      .def_readwrite(
        "geo_cell_data",
        &gcd_model::gcd,
        "GeoCellDataVector: a strongly typed list of geo-cell-data, the geo-personality of Shyft cells ")
      .def(py::self == py::self)
      .def(py::self != py::self);
    shyft::pyapi::expose_format(m);
    using gcd_model_vector = std::vector<gcd_model_>;
    py::class_<gcd_model_vector> mv("GeoCellDataModelVector", "A strongly typed list of GeoCellDataModel's");
    mv.def(py::vector_indexing_suite<gcd_model_vector, true>());
    shyft::pyapi::expose_format(mv);
    detail::expose_eq_ne(mv);
    detail::expose_clone(mv);
  }

  void gcd_client_server() {
    using model = gcd_model;
    using client_t = shyft::pyapi::energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = shyft::pyapi::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;
    def_gcd_model();
    shyft::pyapi::energy_market::expose_client<client_t>(
      "GeoCellDataClient", "The client api for the shyft.hydrology geo_cell_data model service.");
    shyft::pyapi::energy_market::expose_server<srv_t>(
      "GeoCellDataServer", "The server-side component for the shyft.hydrology geo_cell_data model repository.");
  }
}
