/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/penman_monteith.h>
#include <shyft/py/api/bindings.h>

namespace expose {

  void penman_monteith() {
    using namespace shyft::core::penman_monteith;

    py::class_<parameter>("PenmanMonteithParameter")
      .def(py::init<double, double, double, double, bool>(
        (py::arg("height_veg"), py::arg("height_ws"), py::arg("height_t"), py::arg("rl"), py::arg("full_model")),
        "a new object with specified parameters"))
      .def_readwrite("height_veg", &parameter::height_t, "float: grass 0.15")
      .def_readwrite("height_ws", &parameter::height_ws, "float: typical value 2.0")
      .def_readwrite("height_t", &parameter::height_t, "float: typical value 2.0")
      .def_readwrite("rl", &parameter::rl, "float: stomatal resistance, for full PM only, 140")
      .def_readwrite(
        "full_model",
        &parameter::full_model,
        "bool: boolean to specify if full penman-monteith required, set to false by default, so it is asce-ewri "
        "standard");

    py::class_<response>("PenmanMonteithResponse")
      .def_readwrite("et_ref", &response::et_ref, "float: ..")
      .def_readwrite("soil_heat", &response::soil_heat, "float: ..");

    typedef calculator PenmanMonteithCalculator;
    py::class_<PenmanMonteithCalculator>(
      "PenmanMonteithCalculator",
      doc.intro(
        "Evapotranspiration model, Penman-Monteith equation, PM\n"
        "reference ::\n\n"
        "  ASCE-EWRI The ASCE Standardized Reference Evapotranspiration Equation Environmental and\n"
        "     * Water Resources Institute (EWRI) of the American Society of Civil Engineers Task Com- mittee on "
        "Standardization of\n"
        "     * Reference Evapotranspiration Calculation, ASCE, Washington, DC, Environmental and Water Resources "
        "Institute (EWRI) of\n"
        "     * the American Society of Civil Engineers Task Com- mittee on Standardization of Reference "
        "Evapotranspiration Calculation,\n"
        "     * ASCE, Washington, DC, 2005\n\n"
        "calculating reference evapotranspiration\n"
        "This function is plain and simple, taking albedo and turbidity\n"
        "into the constructor and provides function:\n"
        "reference_evapotranspiration;\n"
        "[mm/s] units.")()

        ,
      py::no_init

      )
      .def(py::init< parameter const &>((py::arg("param")), "create a calculator using supplied parameter"))
      .def(
        "reference_evapotranspiration",
        &PenmanMonteithCalculator::reference_evapotranspiration,
        (py::arg("self"),
         py::arg("response"),
         py::arg("dt"),
         py::arg("net_radiation"),
         py::arg("tempmax"),
         py::arg("tempmin"),
         py::arg("rhumidity"),
         py::arg("elevation"),
         py::arg("windspeed")),
        doc.intro("calculates reference evapotranspiration (standard or full based on full_model param), updating "
                  "response")());
  }
}
