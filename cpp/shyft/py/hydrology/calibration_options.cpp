#include <shyft/py/api/bindings.h>
#include <shyft/py/scoped_gil.h>
#include <fmt/core.h>
#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/hydrology/calibration_algorithms.h>

namespace expose {
  using std::shared_ptr;
  using std::string;
  using std::vector;
  using co = shyft::hydrology::srv::calibration_options;
  using om = shyft::hydrology::srv::optimizer_method;
  using shyft::core::utctime;

  struct optimizer_model {

    vector<double> p_min;
    vector<double> p_max;
    py::object fx; ///< callback for notify

    optimizer_model(vector<double> const & p_min, vector<double> const & p_max, py::object fx)
      : p_min{p_min}
      , p_max{p_max}
      , fx{fx} {
      if (p_min.size() < 1 || p_min.size() != p_max.size()) {
        throw std::runtime_error(
          "The dimension of lower and upper parameter bounds must be equal, and greater than 0.");
      }
      rig_fx();
    }

    void rig_fx() {
      if (!PyEval_ThreadsInitialized()) {
        PyEval_InitThreads(); // ensure threads-is enabled
      }
    }

    vector<double> to_vector(dlib::matrix<double, 0, 1> const & params) const {
      vector<double> x;
      x.reserve(params.nr());
      for (auto i = 0; i < params.nr(); ++i) {
        x.emplace_back(params(i));
      }
      return x;
    }

    double operator()(dlib::matrix<double, 0, 1> const & params) {
      auto x = to_vector(params);
      return py_goal_function(from_scaled(x));
    }

    double operator()(vector<double> const & x) {
      return py_goal_function(from_scaled(x));
    }

    double py_goal_function(vector<double> const & x) {
      double r = shyft::nan;
      if (fx.ptr() != Py_None) {
        shyft::pyapi::scoped_gil_aquire gil;
        try {
          r = py::call<double, vector<double>>(fx.ptr(), x);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else {
        // Consider old fashioned printouts..
        // but leave that to py callback instead.
      }
      return r;
    }

    double goal_function(vector<double> const & x) {
      shyft::pyapi::scoped_gil_release gil;
      return py_goal_function(x);
    }

    vector<double> to_scaled(vector<double>& p) {
      vector<double> p_s;
      p_s.reserve(p.size());
      for (size_t i = 0; i < p.size(); ++i)
        p_s.emplace_back((p[i] - p_min[i]) / (p_max[i] - p_min[i]));
      return p_s;
    }

    vector<double> from_scaled(const dlib::matrix<double, 0, 1> scaled_params) {
      return from_scaled(to_vector(scaled_params));
    }

    vector<double> from_scaled(vector<double> const & p_s) const {
      std::vector<double> p;
      p.reserve(p_s.size());
      for (size_t i = 0; i < p_s.size(); ++i)
        p.emplace_back((p_max[i] - p_min[i]) * p_s[i] + p_min[i]);
      return p;
    }

    vector<double> min_bobyqa(vector<double>& x, int max_n_evaluations, double tr_start, double tr_stop) {
      shyft::pyapi::scoped_gil_release gil;
      shyft::core::model_calibration::min_bobyqa(*this, x, max_n_evaluations, tr_start, tr_stop);
      return x;
    }

    vector<double> min_global(vector<double>& x, int max_n_evaluations, double max_seconds, double solver_epsilon) {
      shyft::pyapi::scoped_gil_release gil;
      shyft::core::model_calibration::min_global(*this, x, max_n_evaluations, max_seconds, solver_epsilon);
      return x;
    }

    vector<double> min_dream(vector<double>& x, int max_n_evaluations) {
      shyft::pyapi::scoped_gil_release gil;
      shyft::core::model_calibration::min_dream(*this, x, max_n_evaluations);
      return x;
    }

    vector<double>
      min_sceua(vector<double>& x, size_t max_n_evaluations, double x_eps = 0.0001, double y_eps = 0.0001) {
      shyft::pyapi::scoped_gil_release gil;
      shyft::core::model_calibration::min_sceua(*this, x, max_n_evaluations, x_eps, y_eps);
      return x;
    }

    vector<double> optimize(vector<double> x, co cal_opt) {
      if (x.size() < 1) {
        throw std::runtime_error("The dimension of x must be greater than 0.");
      }
      if (cal_opt.method == om::BOBYQA) {
        return min_bobyqa(x, cal_opt.max_n_iterations, cal_opt.tr_start, cal_opt.tr_stop);
      }
      if (cal_opt.method == om::GLOBAL) {
        return min_global(
          x, cal_opt.max_n_iterations, shyft::core::to_seconds(cal_opt.time_limit), cal_opt.solver_epsilon);
      }
      if (cal_opt.method == om::DREAM) {
        if (x.size() < 3) {
          throw std::runtime_error(fmt::format("The dimension of x must be greater than 2 for DREAM method."));
        }
        return min_dream(x, cal_opt.max_n_iterations);
      }
      if (cal_opt.method == om::SCEUA) {
        return min_sceua(x, cal_opt.max_n_iterations, cal_opt.x_epsilon, cal_opt.y_epsilon);
      }
      throw std::invalid_argument("received invalid optimizer method");
    }

    string to_str() const {
      return "ParameterOptimizer(p_min, p_max)";
    }
  };

  void api_parameter_optimizer() {
    auto pyco =
      py::class_<optimizer_model>(
        "ParameterOptimizer",
        doc.intro("The ParameterOptimizer allows for testing and comparing optimizer methods for the same inital "
                  "parameters and goal function.")(),
        py::no_init)
        .def(py::init<vector<double> const &, vector<double> const &, py::object>(
          (py::arg("p_min"), py::arg("p_max"), py::arg("fx")),
          doc.intro("Initialises a ParameterOptimizer instance.")
            .parameter(
              "p_min",
              "Union[DoubleVector[float], DoubleVector[int]]",
              "the lower parameter bound in at least one-dimensional space.")
            .parameter(
              "p_max",
              "Union[DoubleVector[float], DoubleVector[int]]",
              "the upper parameter bound in at least one-dimensional space.")
            .parameter("fx", "Callable", "the goal function to be used in the parameter optimization problem.")()))
        .def_readonly(
          "p_min", &optimizer_model::p_min, "DoubleVector: lower parameter bound in at least one-dimensional space")
        .def_readonly(
          "p_max", &optimizer_model::p_max, "DoubleVector: upper parameter bound in at least one-dimensional space")
        .def_readwrite(
          "fx", &optimizer_model::fx, "Callable[[],float]: goal function for which parameters are optimized")

        .def(
          "goal_function",
          &optimizer_model::goal_function,
          (py::arg("self"), py::arg("x")),
          doc.intro("The goal function to be used in the optimization problem.")
            .parameter(
              "x",
              "Union[DoubleVector[float], DoubleVector[int]]",
              "the point in at least one-dimensional space at which the goal function is evaluated.")
            .returns("y", "DoubleVector[float]", "The function value f(x) at point x.")())
        .def(
          "optimize",
          &optimizer_model::optimize,
          (py::arg("self"), py::arg("x"), py::arg("calibration_option")),
          doc.intro("Run the optimizer for given parameters and configuration.")
            .parameter(
              "x",
              "Union[DoubleVector[float], DoubleVector[int]]",
              "the initial point in at least one-dimensional space for the optimization problem.")
            .parameter(
              "calibration_option", "CalibrationOption", "the calibration for the various optimization methods.")
            .returns("x_opt", "DoubleVector[float]", "The optimal parameters for which the optimum of f(x) is found.")
            .see_also("CalibrationOption")())
        .def("__str__", &optimizer_model::to_str, (py::arg("self")))
        .def("__repr__", &optimizer_model::to_str, (py::arg("self")));
  }

  void api_calibration_options() {
    auto pyco =
      py::class_<co>(
        "CalibrationOption",
        doc.intro("The CalibrationOption controls how the calibration optmizer is run regarding method and termination "
                  "criterias.\n"
                  "Notice that the different optmizers do have variations in the termination criterias.\n"
                  "All optimization algorithms expect at least one-dimensional paramteter space.")())
        .def(py::init<om, py::optional<int, utctime, double, double, double, double, double>>(
          (py::arg("method"),
           py::arg("max_iterations"),
           py::arg("time_limit"),
           py::arg("solver_epsilon"),
           py::arg("x_epsilon"),
           py::arg("y_epsilon"),
           py::arg("tr_start"),
           py::arg("tr_stop")),
          doc.intro("Initialises a ParameterOptimizer instance.")
            .parameter("method", "OptimizerMethod", "select which parameter optimizer to use.")
            .parameter("max_iterations", "int", "stop after this number of iterations (default 1500).")
            .parameter("time_limit", "float", "GLOBAL: don't run the global search longer than this (default 0, off).")
            .parameter("solver_epsilon", "float", "GLOBAL: select which parameter optimizer to use (default 0.001).")
            .parameter(
              "x_epsilon", "float", "SCEUA: stop when normalized parameters x is within this range in variation.")
            .parameter(
              "y_epsilon",
              "float",
              "SCEUA: stop when goal function is stable within this range (not improving any further).")
            .parameter("tr_start", "float", "BOBYQA: trust region start (default 0.1).")
            .parameter("tr_stop", "float", "BOBYQA: trust region stop/end (default 1e-5).")()))
        .def_readwrite("method", &co::method, "int: OptimizerMethod,  which parameter optimizer to use")
        .def_readwrite(
          "max_iterations", &co::max_n_iterations, "int: stop after this number of iterations (default 1500)")
        .def_readwrite(
          "time_limit", &co::time_limit, "time: GLOBAL: don't run the global search longer than this (default 0, off)")
        .def_readwrite(
          "solver_epsilon",
          &co::solver_epsilon,
          "float: GLOBAL: select which parameter optimizer to use (default 0.001)")
        .def_readwrite(
          "x_epsilon",
          &co::x_epsilon,
          "float: SCEUA: stop when normalized parameters x is within this range in variation")
        .def_readwrite(
          "y_epsilon",
          &co::y_epsilon,
          "float: SCEUA: stop when goal function is stable within this range (not improving any further)")
        .def_readwrite("tr_start", &co::tr_start, "float: BOBYQA: trust region start (default 0.1)")
        .def_readwrite("tr_stop", &co::tr_stop, "float: BOBYQA: trust region stop/end (default 1e-5)")

        .def(
          "bobyqa",
          +[](int max_iterations, double tr_start, double tr_stop) {
            co r{om::BOBYQA, size_t(max_iterations)};
            r.tr_start = tr_start;
            r.tr_stop = tr_stop;
            return r;
          },
          (py::arg("max_iterations"), py::arg("tr_start"), py::arg("tr_stop")),
          doc.intro("Construct bobyqa options.")
            .parameter("max_iterations", "int", "stop after this number of iterations (default 1500).")
            .parameter("tr_start", "float", "BOBYQA: trust region start (default 0.1).")
            .parameter("tr_stop", "float", "trust region stop/end (default 1e-5).")
            .returns(
              "co", "CalibrationOption", "An instance of CalibrationOption for the relevant optmization algorithm.")())
        .staticmethod("bobyqa")

        .def(
          "global_search",
          +[](int max_iterations, utctime time_limit, double solver_epsilon) {
            co r{om::GLOBAL, size_t(max_iterations), time_limit, solver_epsilon};
            return r;
          },
          (py::arg("max_iterations"), py::arg("time_limit"), py::arg("solver_epsilon")),
          doc.intro("Construct global search options")
            .parameter("max_iterations", "int", "stop after this number of iterations (default 1500).")
            .parameter("time_limit", "float", "don't run the global search longer than this (default 0, off).")
            .parameter("solver_epsilon", "float", "select which parameter optimizer to use (default 0.001).")
            .returns(
              "co", "CalibrationOption", "An instance of CalibrationOption for the relevant optmization algorithm.")())
        .staticmethod("global_search")

        .def(
          "sceua",
          +[](int max_iterations, double x_epsilon, double y_epsilon) {
            co r{om::SCEUA, size_t(max_iterations)};
            r.x_epsilon = x_epsilon;
            r.y_epsilon = y_epsilon;
            return r;
          },
          (py::arg("max_iterations"), py::arg("x_epsilon"), py::arg("y_epsilon")),
          doc.intro("Construct sceua options")
            .parameter("max_iterations", "int", "stop after this number of iterations (default 1500).")
            .parameter("x_epsilon", "float", "stop when normalized parameters x is within this range in variation.")
            .parameter(
              "y_epsilon", "float", "stop when goal function is stable within this range (not improving any further).")
            .returns(
              "co", "CalibrationOption", "An instance of CalibrationOption for the relevant optmization algorithm.")())
        .staticmethod("sceua")

        .def(
          "dream",
          +[](int max_iterations) {
            co r{om::DREAM, size_t(max_iterations)};
            return r;
          },
          (py::arg("max_iterations")),
          doc.intro("Construct dream options")
            .parameter("max_iterations", "int", "stop after this number of iterations (default 1500).")
            .returns(
              "co", "CalibrationOption", "An instance of CalibrationOption for the relevant optmization algorithm.")())
        .staticmethod("dream");
    //{
    //  py::scope _x=pyco;
    py::enum_<om>(
      "OptimizerMethod",
      doc.intro("Shyft calibration allow the user to select between one of these parameter optimizer methods.")())
      .value("BOBYQA", om::BOBYQA)
      .value("GLOBAL", om::GLOBAL)
      .value("DREAM", om::DREAM)
      .value("SCEUA", om::SCEUA)
      .export_values();
    //}
  }
}
