/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/bindings.h>

#include <shyft/hydrology/methods/kirchner.h>

namespace expose {

  void kirchner() {
    using namespace shyft::core::kirchner;


    py::class_<parameter>("KirchnerParameter")
      .def(py::init<double, py::optional<double, double>>(
        (py::arg("c1"), py::arg("c2"), py::arg("c3")), "creates parameter object according to parameters"))
      .def_readwrite("c1", &parameter::c1, "float: default =2.439")
      .def_readwrite("c2", &parameter::c2, "float: default= 0.966")
      .def_readwrite("c3", &parameter::c3, "float: default = -0.10");

    py::class_<state>("KirchnerState")
      .def(py::init<double>((py::arg("q")), "create a state specifying initial content q"))
      .def_readwrite(
        "q",
        &state::q,
        "float: state water 'content' in [mm/h], it defaults to 0.0001 mm, zero is not a reasonable valid value");

    py::class_<response>("KirchnerResponse")
      .def_readwrite("q_avg", &response::q_avg, "float: average discharge over time-step in [mm/h]");

    typedef calculator KirchnerCalculator;
    py::class_<KirchnerCalculator>(
      "KirchnerCalculator",
      "Kirchner model for computing the discharge based on precipitation and evapotranspiration data.\n"
      "\n"
      "This algorithm is based on the log transform of the ode formulation of the time change in discharge as a "
      "function\n"
      "of measured precipitation, evapo-transpiration and discharge, i.e. equation 19 in the publication\n"
      "Catchments as simple dynamic systems: Catchment characterization, rainfall-runoff modeling, and doing\n"
      "'hydrology backward' by James W. Kirchner, published in Water Resources Research, vol. 45, W02429,\n"
      "doi: 10.1029/2008WR006912, 2009.\n"
      "\n",
      py::no_init)
      .def(py::init< parameter const &>((py::arg("param")), "create a calculator using supplied parameter"))
      .def(py::init<double, double, parameter const &>(
        (py::arg("abs_err"), py::arg("rel_err"), py::arg("param")),
        "create a calculator using supplied parameter, also specifying the ODE error parameters"))
      .def(
        "step",
        &KirchnerCalculator::step_fx,
        (py::arg("self"),
         py::arg("state"),
         py::arg("response"),
         py::arg("t0"),
         py::arg("t1"),
         py::arg("precipitation"),
         py::arg("evapotranspiration")),
        doc.intro("step Kirchner model forward from time t0 to time t1\n")
          .intro("note: If the supplied q (state) is less than min_q(0.00001, it represents mm water..),"
                 " it is forced to min_q to ensure numerical stability\n")
          .parameters()
          .parameter("state", "KirchnerState", " current state, updated on return")
          .parameter("response", "KirchnerResponse", "the response , out parameter.")
          .parameter("t0", "time", "start time")
          .parameter("t1", "time", "end time")
          .parameter("precipitation", "float", "in mm/h")
          .parameter("evapotranspiration", "float", "in mm/h")());
  }
}
