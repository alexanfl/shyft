/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/bindings.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/stacks/pt_st_k.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/stacks/pt_st_k_cell_model.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>


#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/api/expose_container.h>

static char const *version() {
  return "v1.0";
}

namespace expose { namespace pt_st_k {
    using namespace shyft::core;
    using namespace shyft::core::pt_st_k;

    static void parameter_state_response() {
      py::class_<parameter, py::bases<>, std::shared_ptr<parameter>> pp(
        "PTSTKParameter",
        "Contains the parameters to the methods used in the PTSTK assembly\n"
        "priestley_taylor,snow_tiles,actual_evapotranspiration,precipitation_correction,kirchner\n");
      pp
        .def(py::init<
             priestley_taylor::parameter const &,
             snow_tiles::parameter const &,
             actual_evapotranspiration::parameter const &,
             kirchner::parameter const &,
             precipitation_correction::parameter const &,
             py::optional<glacier_melt::parameter, routing::uhg_parameter, mstack_parameter>>(
          (py::arg("pt"),
           py::arg("snow"),
           py::arg("ae"),
           py::arg("k"),
           py::arg("p_corr"),
           py::arg("gm"),
           py::arg("routing"),
           py::arg("msp")),
          "create object with specified parameters"))
        .def(py::init< parameter const &>((py::arg("p")), "clone a parameter"))
        .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
        .def_readwrite("ae", &parameter::ae, "ActualEvapotranspirationParameter: actual evapotranspiration parameter")
        .def_readwrite("st", &parameter::st, "SnowTilesParameter: snow_tiles parameter")
        .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
        .def_readwrite("kirchner", &parameter::kirchner, "KirchnerParameter: kirchner parameter")
        .def_readwrite(
          "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
        .def_readwrite(
          "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters")
        .def_readwrite("msp", &parameter::msp, "MethodStackParameter: contains the method stack parameters")
        .def("size", &parameter::size, "returns total number of calibration parameters")
        .def(
          "set",
          &parameter::set,
          (py::arg("self"), py::arg("p")),
          "set parameters from vector/list of float, ordered as by get_name(i)")
        .def(
          "get",
          &parameter::get,
          (py::arg("self"), py::arg("i")),
          "return the value of the i'th parameter, name given by .get_name(i)")
        .def(
          "get_name",
          &parameter::get_name,
          (py::arg("self"), py::arg("i")),
          "returns the i'th parameter name, see also .get()/.set() and .size()")
        .def(
          "serialize",
          &serialize_to_bytes<parameter>,
          (py::arg("self")),
          "serializes the parameters to a blob, that later can be passed in to .deserialize()")
        .def("deserialize", &deserialize_from_bytes<parameter>, (py::arg("blob")))
        .staticmethod("deserialize")
        .def(py::self == py::self)
        .def(py::self != py::self);
      shyft::pyapi::expose_format(pp);
      expose_map<std::int64_t, parameter_t_>(
        "PTSTKParameterMap",
        "dict (int,parameter)  where the int is the catchment_id",
        true, // comparable
        false // shared_ptr, so no proxy
      );

      py::class_<state>("PTSTKState")
        .def(py::init<snow_tiles::state, kirchner::state>(
          (py::arg("snow"), py::arg("k")), "initializes state with snow_tiles gs and kirchner k"))
        .def_readwrite("snow", &state::snow, "SnowTilesState: snow_tiles state")
        .def_readwrite("kirchner", &state::kirchner, "KirchnerState: kirchner state");

      typedef std::vector<state> PTSTKStateVector;
      py::class_<PTSTKStateVector, py::bases<>, std::shared_ptr<PTSTKStateVector> >("PTSTKStateVector")
        .def(py::vector_indexing_suite<PTSTKStateVector>());
      py::class_<response>(
        "PTSTKResponse", "This struct contains the responses of the methods used in the PTSTK assembly")
        .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
        .def_readwrite("snow", &response::snow, "SnowTilesResponse: snow-tiles method response")
        .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
        .def_readwrite("ae", &response::ae, "ActualEvapotranspirationResponse: actual evapotranspiration response")
        .def_readwrite("kirchner", &response::kirchner, "KirchnerResponse: kirchner response")
        .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
    }

    static void collectors() {
      typedef shyft::core::pt_st_k::all_response_collector PTSTKAllCollector;
      py::class_<PTSTKAllCollector>("PTSTKAllCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTSTKAllCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTSTKAllCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m3/s] for the timestep")
        .def_readonly(
          "snow_sca",
          &PTSTKAllCollector::snow_sca,
          "TsFixed: snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTSTKAllCollector::snow_swe,
          "TsFixed: snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly("snow_outflow", &PTSTKAllCollector::snow_outflow, "TsFixed: snow output [m^3/s] for the timestep")
        .def_readonly(
          "glacier_melt", &PTSTKAllCollector::glacier_melt, "TsFixed:  glacier melt (outflow) [m3/s] for the timestep")
        .def_readonly("ae_output", &PTSTKAllCollector::ae_output, "TsFixed: actual evap mm/h")
        .def_readonly("pe_output", &PTSTKAllCollector::pe_output, "TsFixed: pot evap mm/h")
        .def_readonly(
          "end_reponse", &PTSTKAllCollector::end_reponse, "PTSTKResponse: end_response, at the end of collected")
        .def_readonly("avg_charge", &PTSTKAllCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

      typedef shyft::core::pt_st_k::discharge_collector PTSTKDischargeCollector;
      py::class_<PTSTKDischargeCollector>("PTSTKDischargeCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTSTKDischargeCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTSTKDischargeCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
        .def_readonly(
          "snow_sca",
          &PTSTKDischargeCollector::snow_sca,
          "TsFixed: snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTSTKDischargeCollector::snow_swe,
          "TsFixed: snow swe, [mm] over the cell area, - at the end of timestep")
        .def_readonly(
          "end_reponse", &PTSTKDischargeCollector::end_response, "PTSTKResponse: end_response, at the end of collected")
        .def_readwrite(
          "collect_snow", &PTSTKDischargeCollector::collect_snow, "bool: controls collection of snow routine")
        .def_readonly(
          "avg_charge", &PTSTKDischargeCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");
      typedef shyft::core::pt_st_k::null_collector PTSTKNullCollector;
      py::class_<PTSTKNullCollector>(
        "PTSTKNullCollector",
        "collector that does not collect anything, useful during calibration to minimize memory&maximize speed");

      typedef shyft::core::pt_st_k::state_collector PTSTKStateCollector;
      py::class_<PTSTKStateCollector>("PTSTKStateCollector", "collects state, if collect_state flag is set to true")
        .def_readwrite(
          "collect_state",
          &PTSTKStateCollector::collect_state,
          "bool: if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
        .def_readonly(
          "kirchner_discharge",
          &PTSTKStateCollector::kirchner_discharge,
          "TsFixed: Kirchner state instant Discharge given in m^3/s")
        .def_readonly("snow_sp", &PTSTKStateCollector::fw, "CoreTsVector: raw snow-tiles state-data for 'fw'")
        .def_readonly("snow_sw", &PTSTKStateCollector::lw, "CoreTsVector: raw snow-tiles state-data for 'lw'")
        .add_property(
          "snow_sca",
          +[](PTSTKStateCollector const &a) {
            return shyft::time_series::dd::apoint_ts(a.sca_());
          },
          "TimeSeries: Snow covered area, derived from snow_sp, snow_sw, snow-tiles parameter and snow_fraction")
        .add_property(
          "snow_swe",
          +[](PTSTKStateCollector const &a) {
            return shyft::time_series::dd::apoint_ts(a.swe_());
          },
          "TimeSeries: Snow water-equivalent, derived from snow_sp, snow_sw, snow-tiles parameter and snow_fraction")

        ;
    }

    static void cells() {
      typedef shyft::core::cell<parameter, state, state_collector, all_response_collector> PTSTKCellAll;
      typedef shyft::core::cell<parameter, state, null_collector, discharge_collector> PTSTKCellOpt;
      expose::cell<PTSTKCellAll>("PTSTKCellAll", "tbd: PTSTKCellAll doc");
      expose::cell<PTSTKCellOpt>("PTSTKCellOpt", "tbd: PTSTKCellOpt doc");
      // expose::statistics::hbv_snow<PTSTKCellAll>("PTSTKCell");//it only gives meaning to expose the *All collect
      // cell-type
      expose::statistics::actual_evapotranspiration<PTSTKCellAll>("PTSTKCell");
      expose::statistics::priestley_taylor<PTSTKCellAll>("PTSTKCell");
      expose::statistics::kirchner<PTSTKCellAll>("PTSTKCell");
      expose::statistics::snow_tiles<PTSTKCellAll>("PTSTKCell");
      expose::cell_state_etc<PTSTKCellAll>("PTSTK"); // just one expose of state
    }

    static void models() {
      typedef shyft::core::region_model<pt_st_k::cell_discharge_response_t, shyft::api::a_region_environment>
        PTSTKOptModel;
      typedef shyft::core::region_model<pt_st_k::cell_complete_response_t, shyft::api::a_region_environment> PTSTKModel;
      expose::model<PTSTKModel>("PTSTKModel", "PTSTK");
      expose::model<PTSTKOptModel>("PTSTKOptModel", "PTSTK");
      def_clone_to_similar_model<PTSTKModel, PTSTKOptModel>("create_opt_model_clone");
      def_clone_to_similar_model<PTSTKOptModel, PTSTKModel>("create_full_model_clone");
    }

    static void model_calibrator() {
      expose::model_calibrator<
        shyft::core::region_model<pt_st_k::cell_discharge_response_t, shyft::api::a_region_environment>>(
        "PTSTKOptimizer");
    }
}}

BOOST_PYTHON_MODULE(_pt_st_k) {

  expose::py::scope().attr("__doc__") = "Shyft python api for the pt_st_k model";
  expose::py::def("version", version);
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::pt_st_k::parameter_state_response();
  expose::pt_st_k::cells();
  expose::pt_st_k::models();
  expose::pt_st_k::collectors();
  expose::pt_st_k::model_calibrator();
}
