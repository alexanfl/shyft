/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/bindings.h>
#include <shyft/hydrology/api/api_state.h>

namespace expose {
  using namespace shyft::api;

  void api_cell_state_id() {
    py::class_<cell_state_id>(
      "CellStateId",
      "Unique cell pseudo identifier of  a state\n"
      "\n"
      "A lot of Shyft models can appear in different geometries and\n"
      "resolutions.In a few rare cases we would like to store the *state* of\n"
      "a model.This would be typical for example for hydrological new - year 1 - sept in Norway.\n"
      "To ensure that each cell - state have a unique identifier so that we never risk\n"
      "mixing state from different cells or different geometries, we create a pseudo unique\n"
      "id that helps identifying unique cell - states given this usage and context.\n"
      "\n"
      "The primary usage is to identify which cell a specific identified state belongs to.\n"

      )
      .def(py::init<int, int, int, int>(
        (py::arg("cid"), py::arg("x"), py::arg("y"), py::arg("area")),
        "construct CellStateId with specified parameters"))
      .def_readwrite("cid", &cell_state_id::cid, "int: catchment identifier")
      .def_readwrite("x", &cell_state_id::x, "int: x position in [m]")
      .def_readwrite("y", &cell_state_id::y, "int: y position in [m]")
      .def_readwrite("area", &cell_state_id::area, "int: area in [m^2]")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }
}
