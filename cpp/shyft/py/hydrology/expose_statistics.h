/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/hydrology/api/api_statistics.h>

namespace expose::statistics {
  typedef shyft::time_series::dd::apoint_ts rts_;
  typedef std::vector<double> vd_;
  typedef std::vector<int64_t> const &cids_;
  typedef size_t ix_;
  using shyft::core::stat_scope;

  template <class cell>
  static void kirchner(char const *cell_name) {
    auto state_name=fmt::format("{}KirchnerStateStatistics",cell_name);
    typedef typename shyft::api::kirchner_cell_state_statistics<cell> sc_stat;

    rts_ (sc_stat::*discharge_ts)(cids_, stat_scope) const = &sc_stat::discharge;
    vd_ (sc_stat::*discharge_vd)(cids_, ix_, stat_scope) const = &sc_stat::discharge;
    py::class_<sc_stat>(state_name.c_str(), "Kirchner response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct Kirchner cell response statistics object"))
      .def(
        "discharge",
        discharge_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "discharge",
        discharge_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "discharge_value",
        &sc_stat::discharge_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum discharge[m3/s]  for cells matching catchments_ids at the i'th timestep");
  }

  template <class cell>
  static void hbv_soil(char const *cell_name) {
    auto state_name=fmt::format("{}HBVSoilStateStatistics",cell_name);
    auto response_name=fmt::format("{}HBVSoilResponseStatistics",cell_name);
    typedef typename shyft::api::hbv_soil_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_soil_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*sm_ts)(cids_, stat_scope) const = &sc_stat::sm;
    vd_ (sc_stat::*sm_vd)(cids_, ix_, stat_scope) const = &sc_stat::sm;

    py::class_<sc_stat>(state_name.c_str(), "HBV Soil state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HBV soil cell state statistics object"))
      .def(
        "sm",
        sm_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids")
      .def(
        "sm",
        sm_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep")
      .def(
        "sm_value",
        &sc_stat::sm_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep");


    rts_ (rc_stat::*soil_ae_ts)(cids_, stat_scope) const = &rc_stat::soil_ae;
    vd_ (rc_stat::*soil_ae_vd)(cids_, ix_, stat_scope) const = &rc_stat::soil_ae;

    rts_ (rc_stat::*inuz_ts)(cids_, stat_scope) const = &rc_stat::inuz;
    vd_ (rc_stat::*inuz_vd)(cids_, ix_, stat_scope) const = &rc_stat::inuz;

    py::class_<rc_stat>(response_name.c_str(), "HBV soil response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HBV soil cell response statistics object"))
      .def(
        "soil_ae",
        soil_ae_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids [mm/h]")
      .def(
        "soil_ae",
        soil_ae_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]")
      .def(
        "soil_ae_value",
        &rc_stat::soil_ae_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]")
      .def(
        "inuz",
        inuz_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids[mm/h]")
      .def(
        "inuz",
        inuz_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]")
      .def(
        "inuz_value",
        &rc_stat::inuz_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]");
  }

  template <class cell>
  static void hbv_tank(char const *cell_name) {
    auto state_name=fmt::format("{}HBVTankStateStatistics",cell_name);
    auto response_name=fmt::format("{}HBVTankResponseStatistics",cell_name);
    typedef typename shyft::api::hbv_tank_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_tank_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*uz_ts)(cids_, stat_scope) const = &sc_stat::uz;
    vd_ (sc_stat::*uz_vd)(cids_, ix_, stat_scope) const = &sc_stat::uz;

    rts_ (sc_stat::*lz_ts)(cids_, stat_scope) const = &sc_stat::lz;
    vd_ (sc_stat::*lz_vd)(cids_, ix_, stat_scope) const = &sc_stat::lz;


    py::class_<sc_stat>(state_name.c_str(), "hbv tank state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct hbv tank cell state statistics object"))
      .def(
        "uz",
        uz_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids")
      .def(
        "uz",
        uz_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep")
      .def(
        "uz_value",
        &sc_stat::uz_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep")
      .def(
        "lz",
        lz_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids")
      .def(
        "lz",
        lz_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep")
      .def(
        "lz_value",
        &sc_stat::lz_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep");

    rts_ (rc_stat::*elake_ts)(cids_, stat_scope) const = &rc_stat::elake;
    vd_ (rc_stat::*elake_vd)(cids_, ix_, stat_scope) const = &rc_stat::elake;

    rts_ (rc_stat::*qlz_ts)(cids_, stat_scope) const = &rc_stat::qlz;
    vd_ (rc_stat::*qlz_vd)(cids_, ix_, stat_scope) const = &rc_stat::qlz;

    rts_ (rc_stat::*quz0_ts)(cids_, stat_scope) const = &rc_stat::quz0;
    vd_ (rc_stat::*quz0_vd)(cids_, ix_, stat_scope) const = &rc_stat::quz0;

    rts_ (rc_stat::*quz1_ts)(cids_, stat_scope) const = &rc_stat::quz1;
    vd_ (rc_stat::*quz1_vd)(cids_, ix_, stat_scope) const = &rc_stat::quz1;

    rts_ (rc_stat::*quz2_ts)(cids_, stat_scope) const = &rc_stat::quz2;
    vd_ (rc_stat::*quz2_vd)(cids_, ix_, stat_scope) const = &rc_stat::quz2;

    py::class_<rc_stat>(response_name.c_str(), "hbv tank response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct hbv tank cell response statistics object"))
      .def(
        "elake",
        elake_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids [mm/h]")
      .def(
        "elake",
        elake_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]")
      .def(
        "elake_value",
        &rc_stat::elake_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]")
      .def(
        "qlz",
        qlz_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids[m3/s]")
      .def(
        "qlz",
        qlz_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "qlz_value",
        &rc_stat::qlz_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "quz0",
        quz0_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids[m3/s]")
      .def(
        "quz0",
        quz0_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "quz0_value",
        &rc_stat::quz0_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "quz1",
        quz1_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids[m3/s]")
      .def(
        "quz1",
        quz1_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "quz1_value",
        &rc_stat::quz1_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "quz2",
        quz2_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum for catcment_ids[m3/s]")
      .def(
        "quz2",
        quz2_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "quz2_value",
        &rc_stat::quz2_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]");
  }

  template <class cell>
  static void priestley_taylor(char const *cell_name) {
    auto response_name=fmt::format("{}PriestleyTaylorResponseStatistics",cell_name);
    typedef typename shyft::api::priestley_taylor_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(response_name.c_str(), "PriestleyTaylor response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct PriestleyTaylor cell response statistics object"))
      .def(
        "output",
        output_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "output",
        output_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "output_value",
        &rc_stat::output_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns for cells matching catchments_ids at the i'th timestep");
  }

  template <class cell>
  static void radiation(char const *cell_name) {
    auto response_name=fmt::format("{}RadiationResponseStatistics",cell_name);
    typedef typename shyft::api::radiation_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(response_name.c_str(), "Radiation response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct Radiation cell response statistics object"))
      .def(
        "output",
        output_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "output",
        output_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "output_value",
        &rc_stat::output_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns for cells matching catchments_ids at the i'th timestep");
  }

  template <class cell>
  static void penman_monteith(char const *cell_name) {
    auto response_name=fmt::format("{}PenmanMonteithResponseStatistics",cell_name);
    typedef typename shyft::api::penman_monteith_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(response_name.c_str(), "PenmanMonteith response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct PenmanMonteith cell response statistics object"))
      .def(
        "output",
        output_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "output",
        output_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "output_value",
        &rc_stat::output_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns for cells matching catchments_ids at the i'th timestep");
  }

  template <class cell>
  static void actual_evapotranspiration(char const *cell_name) {
    auto response_name=fmt::format("{}ActualEvapotranspirationResponseStatistics",cell_name);
    typedef typename shyft::api::actual_evapotranspiration_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    rts_ (rc_stat::*pot_ratio_ts)(cids_, stat_scope) const = &rc_stat::pot_ratio;
    vd_ (rc_stat::*pot_ratio_vd)(cids_, ix_, stat_scope) const = &rc_stat::pot_ratio;
    py::class_<rc_stat>(response_name.c_str(), "ActualEvapotranspiration response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct ActualEvapotranspiration cell response statistics object"))
      .def(
        "output",
        output_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "output",
        output_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "output_value",
        &rc_stat::output_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns for cells matching catchments_ids at the i'th timestep")
      .def(
        "pot_ratio",
        pot_ratio_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns the avg ratio (1-exp(-water_level*3/scale_factor)) for catcment_ids")
      .def(
        "pot_ratio",
        pot_ratio_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns the ratio the ratio (1-exp(-water_level*3/scale_factor)) for cells matching catchments_ids at the "
        "i'th timestep")
      .def(
        "pot_ratio_value",
        &rc_stat::pot_ratio_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns the ratio avg (1-exp(-water_level*3/scale_factor)) value for cells matching catchments_ids at the "
        "i'th timestep");
  }

  template <class cell>
  static void hbv_actual_evapotranspiration(char const *cell_name) {
    auto response_name=fmt::format("{}HbvActualEvapotranspirationResponseStatistics",cell_name);
    typedef typename shyft::api::hbv_actual_evapotranspiration_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(response_name.c_str(), "HbvActualEvapotranspiration response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HbvActualEvapotranspiration cell response statistics object"))
      .def(
        "output",
        output_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "output",
        output_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "output_value",
        &rc_stat::output_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns for cells matching catchments_ids at the i'th timestep");
  }

  template <class cell>
  static void gamma_snow(char const *cell_name) {
    auto state_name=fmt::format("{}GammaSnowStateStatistics",cell_name);
    auto response_name=fmt::format("{}GammaSnowResponseStatistics",cell_name);
    typedef typename shyft::api::gamma_snow_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::gamma_snow_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*albedo_ts)(cids_, stat_scope) const = &sc_stat::albedo;
    vd_ (sc_stat::*albedo_vd)(cids_, ix_, stat_scope) const = &sc_stat::albedo;

    rts_ (sc_stat::*lwc_ts)(cids_, stat_scope) const = &sc_stat::lwc;
    vd_ (sc_stat::*lwc_vd)(cids_, ix_, stat_scope) const = &sc_stat::lwc;

    rts_ (sc_stat::*surface_heat_ts)(cids_, stat_scope) const = &sc_stat::surface_heat;
    vd_ (sc_stat::*surface_heat_vd)(cids_, ix_, stat_scope) const = &sc_stat::surface_heat;

    rts_ (sc_stat::*alpha_ts)(cids_, stat_scope) const = &sc_stat::alpha;
    vd_ (sc_stat::*alpha_vd)(cids_, ix_, stat_scope) const = &sc_stat::alpha;

    rts_ (sc_stat::*sdc_melt_mean_ts)(cids_, stat_scope) const = &sc_stat::sdc_melt_mean;
    vd_ (sc_stat::*sdc_melt_mean_vd)(cids_, ix_, stat_scope) const = &sc_stat::sdc_melt_mean;

    rts_ (sc_stat::*acc_melt_ts)(cids_, stat_scope) const = &sc_stat::acc_melt;
    vd_ (sc_stat::*acc_melt_vd)(cids_, ix_, stat_scope) const = &sc_stat::acc_melt;

    rts_ (sc_stat::*iso_pot_energy_ts)(cids_, stat_scope) const = &sc_stat::iso_pot_energy;
    vd_ (sc_stat::*iso_pot_energy_vd)(cids_, ix_, stat_scope) const = &sc_stat::iso_pot_energy;

    rts_ (sc_stat::*temp_swe_ts)(cids_, stat_scope) const = &sc_stat::temp_swe;
    vd_ (sc_stat::*temp_swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::temp_swe;

    py::class_<sc_stat>(state_name.c_str(), "GammaSnow state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct GammaSnow cell state statistics object"))
      .def(
        "albedo",
        albedo_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "albedo",
        albedo_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "albedo_value",
        &sc_stat::albedo_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "lwc",
        lwc_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "lwc",
        lwc_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "lwc_value",
        &sc_stat::lwc_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "surface_heat",
        surface_heat_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "surface_heat",
        surface_heat_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "surface_heat_value",
        &sc_stat::surface_heat_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "alpha",
        alpha_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "alpha",
        alpha_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "alpha_value",
        &sc_stat::alpha_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sdc_melt_mean",
        sdc_melt_mean_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "sdc_melt_mean",
        sdc_melt_mean_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sdc_melt_mean_value",
        &sc_stat::sdc_melt_mean_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "acc_melt",
        acc_melt_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "acc_melt",
        acc_melt_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "acc_melt_value",
        &sc_stat::acc_melt_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "iso_pot_energy",
        iso_pot_energy_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "iso_pot_energy",
        iso_pot_energy_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "iso_pot_energy_value",
        &sc_stat::iso_pot_energy_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "temp_swe",
        temp_swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "temp_swe",
        temp_swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "temp_swe_value",
        &sc_stat::temp_swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep");


    rts_ (rc_stat::*sca_ts)(cids_, stat_scope) const = &rc_stat::sca;
    vd_ (rc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &rc_stat::sca;

    rts_ (rc_stat::*swe_ts)(cids_, stat_scope) const = &rc_stat::swe;
    vd_ (rc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &rc_stat::swe;

    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;

    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(response_name.c_str(), "GammaSnow response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct GammaSnow cell response statistics object"))
      .def(
        "outflow",
        outflow_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "outflow",
        outflow_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe",
        swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "swe",
        swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe_value",
        &rc_stat::swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca",
        sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "sca",
        sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca_value",
        &rc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "glacier_melt",
        glacier_melt_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids[m3/s]")
      .def(
        "glacier_melt",
        glacier_melt_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]");
  }

  template <class cell>
  static void hbv_snow(char const *cell_name) {
    auto state_name=fmt::format("{}HBVSnowStateStatistics",cell_name);
    auto response_name=fmt::format("{}HBVSnowResponseStatistics",cell_name);
    typedef typename shyft::api::hbv_snow_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_snow_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;

    py::class_<sc_stat>(state_name.c_str(), "HBVSnow state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HBVSnow cell state statistics object"))
      .def(
        "swe",
        swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "swe",
        swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe_value",
        &sc_stat::swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca",
        sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "sca",
        sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca_value",
        &sc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep");


    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(response_name.c_str(), "HBVSnow response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HBVSnow cell response statistics object"))
      .def(
        "outflow",
        outflow_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "outflow",
        outflow_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "glacier_melt",
        glacier_melt_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids[m3/s]")
      .def(
        "glacier_melt",
        glacier_melt_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]");
  }

  template <class cell>
  static void snow_tiles(char const *cell_name) {
    auto state_name=fmt::format("{}SnowTilesStateStatistics",cell_name);
    auto response_name=fmt::format("{}SnowTilesResponseStatistics",cell_name);
    using sc_stat = shyft::api::snow_tiles_cell_state_statistics<cell>;
    using rc_stat = shyft::api::snow_tiles_cell_response_statistics<cell>;

    rts_ (sc_stat::*s_swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*s_swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*s_sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*s_sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;

    py::class_<sc_stat>(state_name.c_str(), "Snow tiles state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct Snow tiles cell state statistics object"))
      .def(
        "swe",
        s_swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "swe",
        s_swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe_value",
        &sc_stat::swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca",
        s_sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "sca",
        s_sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca_value",
        &sc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep");


    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*swe_ts)(cids_, stat_scope) const = &rc_stat::swe;
    vd_ (rc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &rc_stat::swe;
    rts_ (rc_stat::*sca_ts)(cids_, stat_scope) const = &rc_stat::sca;
    vd_ (rc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &rc_stat::sca;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(response_name.c_str(), "SnowTiles response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct SnowTiles cell response statistics object"))
      .def(
        "outflow",
        outflow_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum of outflow for catcment_ids [m3 s-1]")
      .def(
        "outflow",
        outflow_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns outflow for cells matching catchments_ids at the i'th timestep [m3 s-1]")
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum of outfow for cells matching catchments_ids at the i'th timestep [m3 s-1]")
      .def(
        "swe",
        swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns average snow-water equivalent for catcment_ids [mm]")
      .def(
        "swe",
        swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow-water equivalent for cells matching catchments_ids at the i'th timestep [mm]")
      .def(
        "swe_value",
        &rc_stat::swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns average snow-water equivalent for cells matching catchments_ids at the i'th timestep [mm]")
      .def(
        "sca",
        sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns average snow cover fraction for catcment_ids [0...1]")
      .def(
        "sca",
        sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow cover fraction for cells matching catchments_ids at the i'th timestep [0...1]")
      .def(
        "sca_value",
        &rc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns average snow cover fraction for cells matching catchments_ids at the i'th timestep [0...1]")
      .def(
        "glacier_melt",
        glacier_melt_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum of glacier melt for catcment_ids [m3/s]")
      .def(
        "glacier_melt",
        glacier_melt_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns glacier melt for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum of glacier melt for cells matching catchments_ids at the i'th timestep[m3/s]");
  }

  template <class cell>
  static void hbv_physical_snow(char const *cell_name) {
    auto state_name=fmt::format("{}HBVPhysicalSnowStateStatistics",cell_name);
    auto response_name=fmt::format("{}HBVPhysicalSnowResponseStatistics",cell_name);
    typedef typename shyft::api::hbv_physical_snow_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_physical_snow_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;
    rts_ (sc_stat::*surface_heat_ts)(cids_, stat_scope) const = &sc_stat::surface_heat;
    vd_ (sc_stat::*surface_heat_vd)(cids_, ix_, stat_scope) const = &sc_stat::surface_heat;

    py::class_<sc_stat>(state_name.c_str(), "HBVPhysicalSnow state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HBVPhysicalSnow cell state statistics object"))
      .def(
        "swe",
        swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "swe",
        swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe_value",
        &sc_stat::swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca",
        sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "sca",
        sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca_value",
        &sc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "surface_heat",
        surface_heat_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "surface_heat",
        surface_heat_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "surface_heat_value",
        &sc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")

      ;


    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(response_name.c_str(), "HBVSnow response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct HBVSnow cell response statistics object"))
      .def(
        "outflow",
        outflow_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "outflow",
        outflow_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "glacier_melt",
        glacier_melt_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids[m3/s]")
      .def(
        "glacier_melt",
        glacier_melt_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]");
  }

  template <class cell>
  static void skaugen(char const *cell_name) {
    auto state_name=fmt::format("{}SkaugenStateStatistics",cell_name);
    auto response_name=fmt::format("{}SkaugenResponseStatistics",cell_name);
    typedef typename shyft::api::skaugen_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::skaugen_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*alpha_ts)(cids_, stat_scope) const = &sc_stat::alpha;
    vd_ (sc_stat::*alpha_vd)(cids_, ix_, stat_scope) const = &sc_stat::alpha;
    rts_ (sc_stat::*nu_ts)(cids_, stat_scope) const = &sc_stat::nu;
    vd_ (sc_stat::*nu_vd)(cids_, ix_, stat_scope) const = &sc_stat::nu;
    rts_ (sc_stat::*lwc_ts)(cids_, stat_scope) const = &sc_stat::lwc;
    vd_ (sc_stat::*lwc_vd)(cids_, ix_, stat_scope) const = &sc_stat::lwc;
    rts_ (sc_stat::*residual_ts)(cids_, stat_scope) const = &sc_stat::residual;
    vd_ (sc_stat::*residual_vd)(cids_, ix_, stat_scope) const = &sc_stat::residual;
    rts_ (sc_stat::*swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;

    py::class_<sc_stat>(state_name.c_str(), "Skaugen snow state statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct Skaugen snow cell state statistics object"))
      .def(
        "alpha",
        alpha_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "alpha",
        alpha_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "alpha_value",
        &sc_stat::alpha_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "nu",
        nu_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "nu",
        nu_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "nu_value",
        &sc_stat::nu_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "lwc",
        lwc_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "lwc",
        lwc_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "lwc_value",
        &sc_stat::lwc_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "residual",
        residual_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "residual",
        residual_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "residual_value",
        &sc_stat::residual_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe",
        swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "swe",
        swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe_value",
        &sc_stat::swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca",
        sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "sca",
        sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca_value",
        &sc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep");


    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*total_stored_water_ts)(cids_, stat_scope) const = &rc_stat::total_stored_water;
    vd_ (rc_stat::*total_stored_water_vd)(cids_, ix_, stat_scope) const = &rc_stat::total_stored_water;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;
    rts_ (rc_stat::*r_sca_ts)(cids_, stat_scope) const = &rc_stat::sca;
    vd_ (rc_stat::*r_sca_vd)(cids_, ix_, stat_scope) const = &rc_stat::sca;

    py::class_<rc_stat>(response_name.c_str(), "Skaugen snow response statistics", py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("cells")), "construct Skaugen snow cell response statistics object"))
      .def(
        "outflow",
        outflow_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "outflow",
        outflow_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "total_stored_water",
        total_stored_water_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "total_stored_water",
        total_stored_water_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "total_stored_water_value",
        &rc_stat::total_stored_water_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe",
        total_stored_water_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "swe",
        total_stored_water_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "swe_value",
        &rc_stat::total_stored_water_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "glacier_melt",
        glacier_melt_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids[m3/s]")
      .def(
        "glacier_melt",
        glacier_melt_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]")
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]")
      .def(
        "sca",
        r_sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns avg for catcment_ids")
      .def(
        "sca",
        r_sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "sca_value",
        &rc_stat::sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep");
  }

  template <class cell>
  static void basic_cell(char const *cell_name) {
    auto base_name=fmt::format("{}Statistics",cell_name);
    typedef typename shyft::api::basic_cell_statistics<cell> bc_stat;

    rts_ (bc_stat::*discharge_ts)(cids_, stat_scope) const = &bc_stat::discharge;
    vd_ (bc_stat::*discharge_vd)(cids_, ix_, stat_scope) const = &bc_stat::discharge;

    rts_ (bc_stat::*charge_ts)(cids_, stat_scope) const = &bc_stat::charge;
    vd_ (bc_stat::*charge_vd)(cids_, ix_, stat_scope) const = &bc_stat::charge;

    rts_ (bc_stat::*temperature_ts)(cids_, stat_scope) const = &bc_stat::temperature;
    vd_ (bc_stat::*temperature_vd)(cids_, ix_, stat_scope) const = &bc_stat::temperature;

    rts_ (bc_stat::*radiation_ts)(cids_, stat_scope) const = &bc_stat::radiation;
    vd_ (bc_stat::*radiation_vd)(cids_, ix_, stat_scope) const = &bc_stat::radiation;

    rts_ (bc_stat::*wind_speed_ts)(cids_, stat_scope) const = &bc_stat::wind_speed;
    vd_ (bc_stat::*wind_speed_vd)(cids_, ix_, stat_scope) const = &bc_stat::wind_speed;

    rts_ (bc_stat::*rel_hum_ts)(cids_, stat_scope) const = &bc_stat::rel_hum;
    vd_ (bc_stat::*rel_hum_vd)(cids_, ix_, stat_scope) const = &bc_stat::rel_hum;

    rts_ (bc_stat::*precipitation_ts)(cids_, stat_scope) const = &bc_stat::precipitation;
    vd_ (bc_stat::*precipitation_vd)(cids_, ix_, stat_scope) const = &bc_stat::precipitation;

    rts_ (bc_stat::*snow_swe_ts)(cids_, stat_scope) const = &bc_stat::snow_swe;
    vd_ (bc_stat::*snow_swe_vd)(cids_, ix_, stat_scope) const = &bc_stat::snow_swe;

    rts_ (bc_stat::*snow_sca_ts)(cids_, stat_scope) const = &bc_stat::snow_sca;
    vd_ (bc_stat::*snow_sca_vd)(cids_, ix_, stat_scope) const = &bc_stat::snow_sca;


    py::class_<bc_stat>(
      base_name.c_str(),
      doc.intro("This class provides statistics for group of cells, as specified")
        .intro("by the list of catchment identifiers, or list of cell-indexes passed to the methods.")
        .intro("It is provided both as a separate class, but is also provided")
        .intro("automagically through the region_model.statistics property."),
      py::no_init)
      .def(py::init<std::shared_ptr<std::vector<cell>> >(
        (py::arg("self"), py::arg("cells")),
        "construct basic cell statistics object for the list of cells, usually from the region-model"))
      .def(
        "discharge",
        discharge_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "discharge",
        discharge_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "discharge_value",
        &bc_stat::discharge_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "charge",
        charge_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum charge[m^3/s] for catcment_ids")
      .def(
        "charge",
        charge_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns charge[m^3/s]  for cells matching catchments_ids at the i'th timestep")
      .def(
        "charge_value",
        &bc_stat::charge_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns charge[m^3/s] for cells matching catchments_ids at the i'th timestep")
      .def(
        "snow_swe",
        snow_swe_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_swe [mm] for catcment_ids")
      .def(
        "snow_swe",
        snow_swe_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_swe [mm]  for cells matching catchments_ids at the i'th timestep")
      .def(
        "snow_swe_value",
        &bc_stat::snow_swe_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_swe [mm] for cells matching catchments_ids at the i'th timestep")
      .def(
        "snow_sca",
        snow_sca_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_sca [] for catcment_ids")
      .def(
        "snow_sca",
        snow_sca_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_sca []  for cells matching catchments_ids at the i'th timestep")
      .def(
        "snow_sca_value",
        &bc_stat::snow_sca_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_sca [] for cells matching catchments_ids at the i'th timestep")
      .def(
        "temperature",
        temperature_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "temperature",
        temperature_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "temperature_value",
        &bc_stat::temperature_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "precipitation",
        precipitation_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "precipitation",
        precipitation_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "precipitation_value",
        &bc_stat::precipitation_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "radiation",
        radiation_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "radiation",
        radiation_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "radiation_value",
        &bc_stat::radiation_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "wind_speed",
        wind_speed_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "wind_speed",
        wind_speed_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "wind_speed_value",
        &bc_stat::wind_speed_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "rel_hum",
        rel_hum_ts,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns sum  for catcment_ids")
      .def(
        "rel_hum",
        rel_hum_vd,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "rel_hum_value",
        &bc_stat::rel_hum_value,
        (py::arg("self"), py::arg("indexes"), py::arg("i"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns  for cells matching catchments_ids at the i'th timestep")
      .def(
        "total_area",
        &bc_stat::total_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns total area[m2] for cells matching catchments_ids")
      .def(
        "forest_area",
        &bc_stat::forest_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns forest area[m2] for cells matching catchments_ids")
      .def(
        "glacier_area",
        &bc_stat::glacier_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns glacier area[m2] for cells matching catchments_ids")
      .def(
        "lake_area",
        &bc_stat::lake_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns lake area[m2] for cells matching catchments_ids")
      .def(
        "reservoir_area",
        &bc_stat::reservoir_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns reservoir area[m2] for cells matching catchments_ids")
      .def(
        "unspecified_area",
        &bc_stat::unspecified_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns unspecified area[m2] for cells matching catchments_ids")
      .def(
        "snow_storage_area",
        &bc_stat::snow_storage_area,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns snow_storage area where snow can build up[m2], eg total_area - lake and reservoir")
      .def(
        "elevation",
        &bc_stat::elevation,
        (py::arg("self"), py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix),
        "returns area-average elevation[m.a.s.l] for cells matching catchments_ids");
  }

}
