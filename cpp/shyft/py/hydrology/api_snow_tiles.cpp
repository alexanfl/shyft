/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/py/api/bindings.h>

namespace expose {

  void snow_tiles() {
    using namespace shyft::core::snow_tiles;
    using std::vector;

    py::class_<parameter>("SnowTilesParameter")

      .def(py::init<double, py::optional< double, double, double, double, double, vector<double>>>(
        (py::arg("shape"),
         py::arg("tx"),
         py::arg("cx"),
         py::arg("ts"),
         py::arg("lwmax"),
         py::arg("cfr"),
         py::arg("area_fractions"))))

      .def_readwrite(
        "tx", &parameter::tx, "float: threshold temperature determining precipitation phase, unit (C), range [-4, 4]")
      .def_readwrite("cx", &parameter::cx, "float: degree-day melt factor, unit (mm/C/day), range [0, 30]")
      .def_readwrite("ts", &parameter::ts, "float: threshold temperature for melt onset, unit (C), range [-4, 4]")
      .def_readwrite(
        "lwmax",
        &parameter::lwmax,
        "float: max liquid water content given as a fraction of ice in the snowpack, unit (-), range [0, 1]")
      .def_readwrite("cfr", &parameter::cfr, "float: refreeze coefficient, unit (-), range [0, 1]")

      .add_property(
        "area_fractions",
        py::make_function(&parameter::get_area_fractions, py::return_value_policy<py::copy_const_reference>()),
        &parameter::set_area_fractions,
        "DoubleVector: area fractions of the individual tiles, unit (-), range per tile [0.01, 1], the sum of the "
        "elements in the vector must equal 1")
      .add_property(
        "shape",
        &parameter::get_shape,
        &parameter::set_shape,
        "float: shape parameter of the Gamma distribution defining the multiplication factors for snowfall, unit (-), "
        "range [0.1, inf]")
      .add_property(
        "_multiply",
        py::make_function(&parameter::get_multiply, py::return_value_policy<py::copy_const_reference>()),
        "DoubleVector: multiplication factors for distributing solid precipitation between the tiles (mean of elements "
        "= 1)")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::class_<state>("SnowTilesState")
      .def(py::init<vector<double>, vector<double>>((py::arg("fw"), py::arg("lw"))))
      .def_readwrite("fw", &state::fw, "DoubleVector: frozen water in the snowpack, unit (mm)")
      .def_readwrite("lw", &state::lw, "DoubleVector: liquid water in the snowpack, unit (mm)")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::class_<response>("SnowTilesResponse")
      .def_readwrite("outflow", &response::outflow, "average outflow, rain and snowmelt, for the cell, unit (mm/hour)")
      .def_readwrite("swe", &response::swe, "float: average swe for the cell, unit (mm)")
      .def_readwrite("sca", &response::sca, "float: average sca as a fraction of total cell area, unit (-)");

    py::class_<calculator>(
      "SnowTilesCalculator",
      "Tile based snow model method\n"
      "\n"
      "This algorithm uses ...\n"
      "\n",
      py::no_init)
      .def(py::init< parameter const &>((py::arg("parameter")), "creates a calculator with given parameter"))
      .def(
        "step",
        &calculator::step,
        (py::arg("self"),
         py::arg("state"),
         py::arg("response"),
         py::arg("t0"),
         py::arg("t1"),
         py::arg("precipitation"),
         py::arg("temperature")),
        doc.intro("steps the model forward from t0 to t1, updating state and response")());
  }
}
