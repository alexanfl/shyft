/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <fstream>
#include <shyft/py/api/bindings.h>

char const * version() {
  return "v5.x";
}

namespace expose {
  extern void api_geo_cell_data();
  extern void hydrology_vectors();
  extern void target_specification();
  extern void region_environment();
  extern void priestley_taylor();
  extern void actual_evapotranspiration();
  extern void gamma_snow();
  extern void kirchner();
  extern void precipitation_correction();
  extern void hbv_snow();
  extern void snow_tiles();
  extern void hbv_physical_snow();
  extern void cell_environment();
  extern void interpolation();
  extern void skaugen_snow();
  extern void kalman();
  extern void hbv_soil();
  extern void hbv_tank();
  extern void hbv_actual_evapotranspiration();
  extern void glacier_melt();
  extern void routing();
  extern void api_cell_state_id();
  extern void radiation();
  extern void penman_monteith();
  extern void drms();
  extern void api_calibration_status();
  extern void api_calibration_options();
  extern void api_parameter_optimizer();
  extern void cf_time();
  extern void gcd_client_server();
  extern void state_client_server();
  extern void parameter_client_server();

  void api() {
    api_geo_cell_data();
    hydrology_vectors();
    target_specification();
    region_environment();
    precipitation_correction();
    priestley_taylor();
    actual_evapotranspiration();
    gamma_snow();
    skaugen_snow();
    hbv_snow();
    snow_tiles();
    hbv_physical_snow();
    kirchner();
    cell_environment();
    interpolation();
    kalman();
    hbv_soil();
    hbv_tank();
    hbv_actual_evapotranspiration();
    glacier_melt();
    routing();
    api_cell_state_id();
    radiation();
    penman_monteith();
    drms();
    api_calibration_status();
    api_calibration_options();
    api_parameter_optimizer();
    cf_time();
    gcd_client_server();
    state_client_server();
    parameter_client_server();
  }
}

BOOST_PYTHON_MODULE(_api) {
  expose::py::scope().attr("__doc__") = "Shyft hydrology python api providing basic types";
  expose::py::def("version", version);
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::api();
}
