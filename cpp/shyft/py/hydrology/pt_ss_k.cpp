/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/bindings.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/skaugen.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/stacks/pt_ss_k.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/stacks/pt_ss_k_cell_model.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/model_calibration.h>


#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/api/expose_container.h>

static char const * version() {
  return "v1.0";
}

namespace expose { namespace pt_ss_k {
    using namespace shyft::core;
    using namespace shyft::core::pt_ss_k;

    static void parameter_state_response() {

      py::class_<parameter, py::bases<>, std::shared_ptr<parameter>> pp(
        "PTSSKParameter",
        "Contains the parameters to the methods used in the PTSSK assembly\n"
        "priestley_taylor,skaugen,actual_evapotranspiration,precipitation_correction,kirchner\n");
      pp
        .def(py::init<
             priestley_taylor::parameter,
             skaugen::parameter,
             actual_evapotranspiration::parameter,
             kirchner::parameter,
             precipitation_correction::parameter,
             py::optional<glacier_melt::parameter, routing::uhg_parameter, mstack_parameter>>(
          (py::arg("pt"),
           py::arg("gs"),
           py::arg("ae"),
           py::arg("k"),
           py::arg("p_corr"),
           py::arg("gm"),
           py::arg("routing"),
           py::arg("msp")),
          "create object with specified parameters"))
        .def(py::init< parameter const &>((py::arg("p")), "clone a parameter"))
        .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
        .def_readwrite("ae", &parameter::ae, "ActualEvapotranspirationParameter: actual evapotranspiration parameter")
        .def_readwrite("ss", &parameter::ss, "SkaugenParameter: skaugen-snow parameter")
        .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
        .def_readwrite("kirchner", &parameter::kirchner, "kirchner parameter")
        .def_readwrite("p_corr", &parameter::p_corr, "KirchnerParameter: precipitation correction parameter")
        .def_readwrite(
          "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters")
        .def_readwrite("msp", &parameter::msp, "MethodStackParameter: contains the method stack parameters")
        .def("size", &parameter::size, "returns total number of calibration parameters")
        .def(
          "set",
          &parameter::set,
          (py::arg("self"), py::arg("p")),
          "set parameters from vector/list of float, ordered as by get_name(i)")
        .def(
          "get",
          &parameter::get,
          (py::arg("self"), py::arg("i")),
          "return the value of the i'th parameter, name given by .get_name(i)")
        .def(
          "get_name",
          &parameter::get_name,
          (py::arg("self"), py::arg("i")),
          "returns the i'th parameter name, see also .get()/.set() and .size()")
        .def(
          "serialize",
          &serialize_to_bytes<parameter>,
          (py::arg("self")),
          "serializes the parameters to a blob, that later can be passed in to .deserialize()")
        .def("deserialize", &deserialize_from_bytes<parameter>, (py::arg("blob")))
        .staticmethod("deserialize")
        .def(py::self == py::self)
        .def(py::self != py::self);
      shyft::pyapi::expose_format(pp);

      expose_map<std::int64_t, parameter_t_>(
        "PTSSKParameterMap",
        "dict (int,parameter)  where the int is the catchment_id",
        true, // comparable
        false // shared_ptr, so no proxy
      );

      py::class_<state>("PTSSKState")
        .def(py::init<skaugen::state, kirchner::state>(
          (py::arg("snow"), py::arg("kirchner")), "initializes state with skaugen-snow gs and kirchner k"))
        .def_readwrite("snow", &state::snow, "skaugen-snow state")
        .def_readwrite("kirchner", &state::kirchner, "kirchner state");

      typedef std::vector<state> PTSSKStateVector;
      py::class_<PTSSKStateVector, py::bases<>, std::shared_ptr<PTSSKStateVector> >("PTSSKStateVector")
        .def(py::vector_indexing_suite<PTSSKStateVector>());


      py::class_<response>(
        "PTSSKResponse", "This struct contains the responses of the methods used in the PTSSK assembly")
        .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
        .def_readwrite("snow", &response::snow, "SkaugenResponse: skaugen-snow response")
        .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
        .def_readwrite("ae", &response::ae, "ActualEvapotranspirationResponse: actual evapotranspiration response")
        .def_readwrite("kirchner", &response::kirchner, "KirchnerResponse: kirchner response")
        .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
    }

    static void collectors() {
      typedef shyft::core::pt_ss_k::all_response_collector PTSSKAllCollector;
      py::class_<PTSSKAllCollector>("PTSSKAllCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTSSKAllCollector::destination_area, "a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTSSKAllCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
        .def_readonly(
          "snow_total_stored_water", &PTSSKAllCollector::snow_swe, "TsFixed: skaugen aka sca*(swe + lwc) in [mm]")
        .def_readonly(
          "snow_outflow", &PTSSKAllCollector::snow_outflow, "TsFixed: skaugen snow output [m^3/s] for the timestep")
        .def_readonly(
          "glacier_melt", &PTSSKAllCollector::glacier_melt, "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
        .def_readonly("ae_output", &PTSSKAllCollector::ae_output, "TsFixed: actual evap mm/h")
        .def_readonly("pe_output", &PTSSKAllCollector::pe_output, "TsFixed: pot evap mm/h")
        .def_readonly(
          "end_reponse", &PTSSKAllCollector::end_reponse, "PTSSKResponse: end_response, at the end of collected")
        .def_readonly("avg_charge", &PTSSKAllCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

      typedef shyft::core::pt_ss_k::discharge_collector PTSSKDischargeCollector;
      py::class_<PTSSKDischargeCollector>("PTSSKDischargeCollector", "collect all cell response from a run")
        .def_readonly("destination_area", &PTSSKDischargeCollector::destination_area, "float: a copy of cell area [m2]")
        .def_readonly(
          "avg_discharge",
          &PTSSKDischargeCollector::avg_discharge,
          "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
        .def_readonly(
          "snow_sca",
          &PTSSKDischargeCollector::snow_sca,
          "TsFixed: skaugen snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
        .def_readonly(
          "snow_swe",
          &PTSSKDischargeCollector::snow_swe,
          "TsFixed: skaugen snow swe, [mm] over the cell sca.. area, - at the end of timestep")
        .def_readonly(
          "end_reponse", &PTSSKDischargeCollector::end_response, "PTSSKResponse: end_response, at the end of collected")
        .def_readwrite(
          "collect_snow", &PTSSKDischargeCollector::collect_snow, "bool: controls collection of snow routine")
        .def_readonly(
          "avg_charge", &PTSSKDischargeCollector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");
      typedef shyft::core::pt_ss_k::null_collector PTSSKNullCollector;
      py::class_<PTSSKNullCollector>(
        "PTSSKNullCollector",
        "collector that does not collect anything, useful during calibration to minimize memory&maximize speed");

      typedef shyft::core::pt_ss_k::state_collector PTSSKStateCollector;
      py::class_<PTSSKStateCollector>("PTSSKStateCollector", "collects state, if collect_state flag is set to true")
        .def_readwrite(
          "collect_state",
          &PTSSKStateCollector::collect_state,
          "bool: if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
        .def_readonly(
          "kirchner_discharge",
          &PTSSKStateCollector::kirchner_discharge,
          "TsFixed: Kirchner state instant Discharge given in m^3/s")
        .def_readonly("snow_swe", &PTSSKStateCollector::snow_swe, "TsFixed: snow swe")
        .def_readonly("snow_sca", &PTSSKStateCollector::snow_sca, "TsFixed: snow sca")
        .def_readonly("snow_alpha", &PTSSKStateCollector::snow_alpha, "TsFixed: snow alpha")
        .def_readonly("snow_nu", &PTSSKStateCollector::snow_nu, "TsFixed: snow nu ")
        .def_readonly("snow_lwc", &PTSSKStateCollector::snow_lwc, "TsFixed: snow lwc")
        .def_readonly("snow_residual", &PTSSKStateCollector::snow_residual, "TsFixed: snow residual");
    }

    static void cells() {
      typedef shyft::core::cell<parameter, state, state_collector, all_response_collector> PTSSKCellAll;
      typedef shyft::core::cell<parameter, state, null_collector, discharge_collector> PTSSKCellOpt;
      expose::cell<PTSSKCellAll>("PTSSKCellAll", "tbd: PTSSKCellAll doc");
      expose::cell<PTSSKCellOpt>("PTSSKCellOpt", "tbd: PTSSKCellOpt doc");
      expose::statistics::skaugen<PTSSKCellAll>(
        "PTSSKCell"); // it only gives meaning to expose the *All collect cell-type
      expose::statistics::actual_evapotranspiration<PTSSKCellAll>("PTSSKCell");
      expose::statistics::priestley_taylor<PTSSKCellAll>("PTSSKCell");
      expose::statistics::kirchner<PTSSKCellAll>("PTSSKCell");
      expose::cell_state_etc<PTSSKCellAll>("PTSSK"); // just one expose of state
    }

    static void models() {
      typedef shyft::core::region_model<pt_ss_k::cell_discharge_response_t, shyft::api::a_region_environment>
        PTSSKOptModel;
      typedef shyft::core::region_model<pt_ss_k::cell_complete_response_t, shyft::api::a_region_environment> PTSSKModel;
      expose::model<PTSSKModel>("PTSSKModel", "PTSSK");
      expose::model<PTSSKOptModel>("PTSSKOptModel", "PTSSK");
      def_clone_to_similar_model<PTSSKModel, PTSSKOptModel>("create_opt_model_clone");
      def_clone_to_similar_model<PTSSKOptModel, PTSSKModel>("create_full_model_clone");
    }

    static void model_calibrator() {
      expose::model_calibrator<
        shyft::core::region_model<pt_ss_k::cell_discharge_response_t, shyft::api::a_region_environment>>(
        "PTSSKOptimizer");
    }

}}

BOOST_PYTHON_MODULE(_pt_ss_k) {

  expose::py::scope().attr("__doc__") = "Shyft python api for the pt_ss_k model";
  expose::py::def("version", version);
  expose::py::docstring_options doc_options(true, true, false); // all except c++ signatures
  expose::pt_ss_k::parameter_state_response();
  expose::pt_ss_k::cells();
  expose::pt_ss_k::models();
  expose::pt_ss_k::collectors();
  expose::pt_ss_k::model_calibrator();
}
