/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/radiation.h>

#include <shyft/py/api/bindings.h>

namespace expose {

  void radiation() {
    using namespace shyft::core::radiation;

    py::class_<parameter>("RadiationParameter")
      .def(
        py::init<double, double>((py::arg("albedo"), py::arg("turbidity")), "a new object with specified parameters"))
      .def_readwrite("albedo", &parameter::albedo, "float: typical value 0.2")
      .def_readwrite("turbidity", &parameter::turbidity, "float: typical value 1.0");

    py::class_<response>("RadiationResponse")
      .def_readwrite("sw_cs_p", &response::sw_cs_p, "float: ..")
      .def_readwrite("sw_t", &response::sw_t, "float: ..")
      .def_readwrite("net_sw", &response::net_sw, "float: ..")
      .def_readwrite("net_lw", &response::net_lw, "float: ..")
      .def_readwrite("net", &response::net, "float: ..")
      .def_readwrite("ra", &response::ra, "float: ..");

    typedef calculator RadiationCalculator;
    py::class_<RadiationCalculator>(
      "RadiationCalculator",
      doc.intro(
        "Radiation,R,\n"
        "\n"
        "reference ::\n\n"
        "  Allen, R. G.; Trezza, R. & Tasumi, M. Analytical integrated functions for daily solar radiation on slopes "
        "Agricultural and Forest Meteorology, 2006, 139, 55-73)\n"
        "  primitive implementation for calculating predicted clear-sky short-wave solar radiation for inclined "
        "surfaces\n"
        "\n\n"
        "This function is plain and simple, taking albedo and turbidity\n"
        "into the constructor and provides 2 functions:\n\n"
        " * net_radiation calculates predicted solar radiation (if no measured data available) or translates measured "
        "data into the slope plus adds the longwave radiation: instantaneously;\n"
        " * net_radiation_step calculates predicted solar radiation or/and translates measured horizontal radiation "
        "into sloped surface for the time period between tstart and tend plus adds the lw radiation\n"
        "\n"
        "Recommended usage is the net_radiation_step for 24h-steps; it was also tested with 1h and 3h steps")(),
      py::no_init)
      .def(py::init< parameter const &>((py::arg("param")), "create a calculator using supplied parameter"))
      .def(
        "net_radiation_inst",
        &RadiationCalculator::net_radiation_inst,
        (py::arg("self"),
         py::arg("response"),
         py::arg("latitude"),
         py::arg("t"),
         py::arg("slope"),
         py::arg("aspect"),
         py::arg("temperature"),
         py::arg("rhumidity"),
         py::arg("elevation"),
         py::arg("rsm")),
        doc.intro("calculates net radiation using instantaneous method, updating response")())
      .def(
        "net_radiation_step",
        &RadiationCalculator::net_radiation_step,
        (py::arg("self"),
         py::arg("response"),
         py::arg("latitude"),
         py::arg("t1"),
         py::arg("dt"),
         py::arg("slope"),
         py::arg("aspect"),
         py::arg("temperature"),
         py::arg("rhumidity"),
         py::arg("elevation"),
         py::arg("rsm")),
        doc.intro("calculates net radiation, updating response")())
      .def(
        "net_radiation",
        &RadiationCalculator::net_radiation,
        (py::arg("self"),
         py::arg("response"),
         py::arg("latitude"),
         py::arg("t1"),
         py::arg("dt"),
         py::arg("slope"),
         py::arg("aspect"),
         py::arg("temperature"),
         py::arg("rhumidity"),
         py::arg("elevation"),
         py::arg("rsm")),
        doc.intro("calculates net radiation using either step or instantaneous method, updating response")())
      .def(
        "net_radiation_step_asce_st",
        &RadiationCalculator::net_radiation_step_asce_st,
        (py::arg("self"),
         py::arg("response"),
         py::arg("latitude"),
         py::arg("t1"),
         py::arg("dt"),
         py::arg("slope"),
         py::arg("aspect"),
         py::arg("temperature"),
         py::arg("rhumidity"),
         py::arg("elevation"),
         py::arg("rsm")),
        doc.intro("calculates net radiation, updating response")());
  }
}
