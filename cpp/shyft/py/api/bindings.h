#pragma once

#include <shyft/py/api/boostpython.h>

namespace shyft {

  // NOTE:
  //   temporary alias to set up migration path from boost::python
  //   preferrably it was called api, but already taken. not in
  //   py namespace since this is defined multiple places and could
  //   collide.
  //    - jeh
  namespace py = boost::python;
  using expose::handle_pyerror;
}
