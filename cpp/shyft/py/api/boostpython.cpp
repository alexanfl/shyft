/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython.h>
#include <boost/python/import.hpp>

namespace expose {
  void handle_pyerror() {
    // from SO: https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
    using namespace boost;
    std::string msg{"unspecified error"};
    if (PyErr_Occurred()) {
      PyObject *exc, *val, *tb;
      py::object formatted_list, formatted;
      PyErr_Fetch(&exc, &val, &tb);
      py::handle<> hexc(exc), hval(py::allow_null(val)), htb(py::allow_null(tb));
      py::object traceback(py::import("traceback"));
      if (!tb) {
        py::object format_exception_only{traceback.attr("format_exception_only")};
        formatted_list = format_exception_only(hexc, hval);
      } else {
        py::object format_exception{traceback.attr("format_exception")};
        if (format_exception) {
          try {
            formatted_list = format_exception(hexc, hval, htb);
          } catch (...) { // any error here, and we bail out, no crash please
            msg = "not able to extract exception info";
          }
        } else
          msg = "not able to extract exception info";
      }
      if (formatted_list) {
        formatted = py::str("\n").join(formatted_list);
        msg = py::extract<std::string>(formatted);
      }
    }
    py::handle_exception();
    PyErr_Clear();
    throw std::runtime_error(msg);
  }
}
