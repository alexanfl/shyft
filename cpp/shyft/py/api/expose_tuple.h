#pragma once

#include <functional>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

#include <shyft/py/api/bindings.h>

namespace expose {

  namespace detail {

    /** to python converter for std::tuple<T...> support
     *
     * @tparam T the optinal type, like
     */
    template <typename T>
    struct to_python_tuple {
      static PyObject* convert(T const & obj) {
        return std::apply(
          [](auto const &... u) {
            return py::make_tuple(py::object(u)...).ptr();
          },
          obj);
      }
    };

    /** helper to enable python pass value or none as setter to std::tuple args/members
     */
    template <typename U, typename I = std::make_index_sequence<std::tuple_size_v<U>>>
    struct from_python_tuple;

    template <typename U, std::size_t... I>
    struct from_python_tuple<U, std::integer_sequence<std::size_t, I...>> {

      from_python_tuple() {
        py::converter::registry::push_back(&convertible, &construct, py::type_id< U >());
      }

      static bool _is_none(PyObject* obj_ptr) {
        return obj_ptr ? obj_ptr == py::object().ptr() : false;
      }

      static void* convertible(PyObject* obj_ptr) {
        if (_is_none(obj_ptr)) // ensure to passthrough None, to clear the tuple
          return obj_ptr;

        py::extract<py::tuple> tup_ex(obj_ptr);
        if (!tup_ex.check())
          return nullptr;

        return obj_ptr;
      }

      static void construct(PyObject* obj_ptr, py::converter::rvalue_from_python_stage1_data* data) {
        void* storage = ((py::converter::rvalue_from_python_storage<U >*) data)->storage.bytes;
        auto ok = [&] {
          if (_is_none(obj_ptr))
            return false;

          auto tuple = py::extract<py::tuple>(obj_ptr)();

          if (py::len(tuple) != sizeof...(I))
            return false;

          if (!(py::extract<std::tuple_element_t<I, U>>(tuple[I]).check() && ...))
            return false;
          new (storage) U(py::extract<std::tuple_element_t<I, U>>(tuple[I])()...);
          return true;
        }();
        if (!ok)
          new (storage) U();
        data->convertible = storage;
      }
    };
  }

  /** @brief register_tuple<T>() provides tuple-like converters
   * @param a T tag
   */
  template <class T>
  void register_tuple(std::type_identity<T>) {
    py::to_python_converter<T, detail::to_python_tuple<T>>();
    detail::from_python_tuple<T>();
  }

}
