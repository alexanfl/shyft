/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <utility>
#include <optional>

#include <shyft/py/api/bindings.h>

namespace expose {

  namespace detail {

    /** to python converter for std::optional<T> support
     *
     * If the std::optional is set, provide the value
     * otherwise, return None
     *
     * @tparam T the optinal type, like
     */
    template <typename T>
    struct to_python_optional {
      static PyObject* convert(std::optional<T> const & obj) {
        if (obj)
          return py::incref(py::object(*obj).ptr()); // incref, and return object
        else
          return py::object().ptr(); // less practical alternative throw std::runtime_error("attempt to read unset
                                     // attribute");attribute_error();
      }
    };

    /** helper to enable python pass value or none as setter to std::optional args/members
     */
    template <typename T>
    struct from_python_optional {
      using extract_type = typename py::extract<
        T>; //  this is the boost::python:extract<T> that know how/if to convert a given python-type

      from_python_optional() {
        py::converter::registry::push_back(&convertible, &construct, py::type_id< typename std::optional<T> >());
      }

      static bool _is_none(PyObject* obj_ptr) {
        return obj_ptr ? obj_ptr == py::object().ptr() : false;
      }

      static void* convertible(PyObject* obj_ptr) {
        if (_is_none(obj_ptr)) // ensure to passthrough None, to clear the optional
          return obj_ptr;
        return extract_type{obj_ptr}.check() ? obj_ptr : nullptr;
      }

      static void construct(PyObject* obj_ptr, py::converter::rvalue_from_python_stage1_data* data) {
        void* storage = ((py::converter::rvalue_from_python_storage<std::optional<T> >*) data)->storage.bytes;
        if (_is_none(obj_ptr)) {
          new (storage) std::optional<T>();
        } else {
          extract_type xo{obj_ptr};
          new (storage) std::optional<T>(xo());
        }
        data->convertible = storage;
      }
    };
  }

  /** @brief register_optional<T>() provides std::optional<T> adapters
   *
   * @details Registers std::optional<T> converters, so that we can assign
   * None, or the value-type to the value. Evaluating a property yields None
   * if the optional is not set.
   * Somewhere in your python module, register the optional converters(once pr. system, including dependent extensions!)
   * @code{.cpp}
   * expose::register_optional<int>()
   * @endcode
   *
   * @tparam T the std::optional<T>
   */
  template <class T>
  void register_optional() {
    py::to_python_converter< std::optional<T>, detail::to_python_optional<T>>();
    detail::from_python_optional<T>();
  }

  /** @brief opt_readwrite(py_cls,"name",&wrapped::optional_attr,"doc str)
   *
   * @details Performs c.add_property(name,getter,setter,doc) for an optional attribute.
   *
   * @code{.cpp}
   * struct something {
   *    std::optional<int> a;
   * };
   * auto c=py::class_<something...>("Something");
   * opt_readwrite(c,"a",&something::a,"doc for a");
   *
   * @endcode
   * @note require that register_optional<T>() is done.
   *
   * @tparam PyCls boost::python::class_<T...> type
   * @tparam A the type member-pointer to the attribute, e.g. &C::a, where type of a is std::optional<something>
   * @param c class_ instance
   * @param name  the name of the attribute, passed on to c.add_property(name
   * @return c to support chaining
   */
  template <class PyCls, typename A>
  PyCls& opt_readwrite(PyCls& c, char const * name, A const & a, char const * doc) {
    c.template add_property(
      name, py::make_getter(a, py::return_value_policy<py::return_by_value>()), py::make_setter(a), doc);

    return c;
  }

  /** @brief opt_readwrite(py_cls,"name",&wrapped::optional_attr,"doc str)
   *
   * @details Performs c.add_property(name,getter,doc) for an optional attribute
   *
   * @note require that register_optional<T>() is done.
   *
   * @tparam PyCls boost::python::class_<T...> type
   * @tparam A the type member-pointer to the attribute, e.g. &C::a, where type of a is std::optional<something>
   * @param c class_ instance
   * @param name  the name of the attribute, passed on to c.add_property(name
   * @return c to support chaining
   */
  template <class PyCls, typename A>
  PyCls& opt_readonly(PyCls& c, char const * name, A a, char const * doc) {
    c.template add_property(name, py::make_getter(a, py::return_value_policy<py::return_by_value>()), doc);

    return c;
  }
}
