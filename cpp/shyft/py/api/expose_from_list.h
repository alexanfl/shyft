/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <stdexcept>
#include <string>
#include <utility>

#include <shyft/py/api/bindings.h>

namespace expose {
  namespace detail {
    /** create_from_list to create a vector<T> from py::list
     *
     * Iterates over the  list, and type-extracts each element to the VT::value_type,
     * if it fails, raises runtime_error indicating the position and repr(l[i]) that failed.
     *
     * @tparam VT vector type, like vector<float>, or vector<some_obj>
     * @param l python list
     * @return a pointer to a fresh VT, to be managed by the  caller
     */
    template <class VT>
    VT* create_from_list(py::list l) {
      using T = typename VT::value_type;
      const size_t n = py::len(l);
      auto r = new VT{};
      if (n > 0)
        r->reserve(n); // only reserve specifics if we n >0
      for (size_t i = 0; i < n; ++i) {
        py::object o = l[i];  // get the object
        py::extract<T> xo(o); // and test if it can be extracted as type T
        if (xo.check()) {
          r->emplace_back(xo());
        } else {
          throw std::runtime_error(
            "Error: construct from python list on " + std::to_string(i)
            + "'th element, :" + py::call_method<std::string>(o.ptr(), "__repr__"));
        }
      }
      return r;
    }
  }

  /** construct_from<vector<type>>(...) generates a make_constructor
   *
   * @detail
   * In your class_...
   * @code{.cpp}
   * struct athing {};
   * using namespace py=boost::python
   * void expose_athing() {
   *    using athing_list=std::vector<athing>; // assume you have expose athing, wants to expose a strongly typed list
   *    cls<athing_list>("AThingList")
   *    .def(py::vector_indexing_suite<athing_list,true>())
   *    .def("__init__",
   *            construct_from<athinglist>(py::default_call_policies(),(py::arg("athings"))),
   *           "Construct from list."
   *       )
   *     ;
   * @endcode
   *
   * @tparam VT The vector-type, like vector<reservoir_> etc., forwared to create_from_list<VT>..
   * @tparam ...Args parameter pack types of  arguments forwared to py::make_constructor
   * @return the same thing as py::make_construct, (py::object).
   */
  template <class VT, typename... Args>
  auto construct_from(Args&&... args) {
    return py::make_constructor(&detail::create_from_list<VT>, std::forward<Args>(args)...);
  }
}
