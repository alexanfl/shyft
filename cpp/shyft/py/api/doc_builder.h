#pragma once

#include <string>
#include <string_view>
#include <fmt/core.h>

//
// struct to build up google-style multi-line docstring
//
constexpr inline struct doc_ {
  auto intro(std::string_view intro) const {
    return doc_{fmt::format("{}{}\n", docstring, intro)};
  }

  auto details(std::string_view dt) const {
    return doc_{fmt::format("{}\n{}\n", docstring, dt)};
  }

  auto attributes() const {
    return doc_{fmt::format("{}\nAttributes:", docstring)};
  }

  auto attribute(std::string_view name, std::string_view type, std::string_view descr) const {
    return doc_{fmt::format("{}\n    {} ({}): {}\n", docstring, name, type, descr)};
  }

  auto parameters() const {
    return doc_{fmt::format("{}\nArgs:", docstring)};
  }

  auto parameter(std::string_view name, std::string_view type, std::string_view descr) const {
    return doc_{fmt::format("{}\n    {} ({}): {}\n", docstring, name, type, descr)};
  }

  auto paramcont(std::string_view doc) const {
    return doc_{fmt::format("{}    {}\n", docstring, doc)};
  }

  auto returns(std::string_view name, std::string_view type, std::string_view descr) const {
    return doc_{fmt::format("{}\nReturns:\n    {}: {}. {}\n", docstring, type, name, descr)};
  }

  auto notes() const {
    return doc_{fmt::format("{}\nNotes:", docstring)};
  }

  auto note(std::string_view note) const {
    return doc_{fmt::format("{}\n    {}\n", docstring, note)};
  }

  auto see_also(std::string_view ref) const {
    return doc_{fmt::format("{}\nSee also:\n    {}\n", docstring, ref)};
  }

  auto reference(std::string_view ref_name, std::string_view ref) const {
    return doc_{fmt::format("{}\n.. _{}:\n    {}\n", docstring, ref_name, ref)};
  }

  auto ind(std::string_view doc) const {
    return doc_{fmt::format("{}    {}", docstring, doc)};
  }

  auto raises() const {
    return doc_{fmt::format("{}\nRaises:", docstring)};
  }

  auto raise(std::string_view type, std::string_view descr) const {
    return doc_{fmt::format("{}\n    {}: {}", docstring, type, descr)};
  }

  auto retcont(std::string_view descr) const {
    return doc_{fmt::format("{}{}\n", docstring, descr)};
  }

  auto pure(std::string_view descr) const {
    return doc_{fmt::format("{}{}", docstring, descr)};
  }

  // refs to py syms
  auto ref_meth(std::string_view m) const {
    return doc_{fmt::format("{} :meth:`{}` ", docstring, m)};
  }

  auto ref_func(std::string_view m) const {
    return doc_{fmt::format("{} :func:`{}` ", docstring, m)};
  }

  auto ref_class(std::string_view m) const {
    return doc_{fmt::format("{} :class:`{}` ", docstring, m)};
  }

  auto ref_mod(std::string_view m) const {
    return doc_{fmt::format("{} :mod:`{}` ", docstring, m)};
  }

  auto ref_attr(std::string_view m) const {
    return doc_{fmt::format("{} :attr:`{}` ", docstring, m)};
  }

  auto operator()() const {
    return docstring.c_str();
  }

  operator char const *() const {
    return docstring.c_str();
  }

  auto str() const {
    return docstring;
  }

  std::string docstring = "";
} doc{};
