#pragma once

#include <algorithm>
#include <cctype>
#include <iterator>
#include <ranges>
#include <string>
#include <vector>

#include <boost/preprocessor/iteration/iterate.hpp>
#include <boost/python/class.hpp>
#include <fmt/core.h>
#include <fmt/format.h>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>

namespace shyft::pyapi {

  template <std::ranges::forward_range R>
  requires std::is_same_v<std::ranges::range_value_t<R>, char>
  std::string pep8_typename(R &&typename_) {
    auto parts = typename_ | std::views::split('_');
    auto pepify = [](auto part) {
      std::vector<char> pepped_part{(char) std::toupper(*std::ranges::begin(part))};
      std::ranges::copy(std::views::drop(part, 1), std::back_inserter(pepped_part));
      return pepped_part;
    };

    std::vector<std::vector<char>> pepped_parts(std::ranges::distance(parts));
    std::ranges::transform(parts, std::begin(pepped_parts), pepify);
    auto joined_pepped_parts = std::views::join(pepped_parts);

    return std::string{std::ranges::begin(joined_pepped_parts), std::ranges::end(joined_pepped_parts)};
  }

  template <reflected_struct T>
  std::string pep8_typename() {
    return pep8_typename(reflected_typename<T>);
  }

  template <typename T, typename... O>
  requires fmt::is_formattable<T>::value
  void expose_format_str(boost::python::class_<T, O...> &py_type) {
    py_type.def(
      "__str__", +[](T const &t) {
        if constexpr (reflected_struct<T>)
          return fmt::format("{}({})", pep8_typename<T>(), t);
        else
          return fmt::format("{}", t);
      });
  }

  template <typename T, typename... O>
  requires fmt::is_formattable<T>::value
  void expose_format_repr(boost::python::class_<T, O...> &py_type) {
    py_type.def(
      "__repr__", +[](T const &t) {
        if constexpr (reflected_struct<T>)
          return fmt::format("{}({})", pep8_typename<T>(), t);
        else
          return fmt::format("{}", t);
      });
  }

  template <typename T, typename... O>
  requires fmt::is_formattable<T>::value
  void expose_format(boost::python::class_<T, O...> &py_type) {
    expose_format_str(py_type);
    expose_format_repr(py_type);
  }

}
