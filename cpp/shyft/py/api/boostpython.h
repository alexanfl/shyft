/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#if defined(_WINDOWS)
#ifndef STRICT
#define STRICT
#endif
#ifndef NOMINMAX
#define NOMINMAX
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Winsock2.h> // WSACleanup
#include <Windows.h>
#endif

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/args.hpp>
#include <boost/python/class.hpp>
#include <boost/python/scope.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/return_internal_reference.hpp>
#include <boost/python/return_arg.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/handle.hpp>
#include <boost/python/tuple.hpp>
#include <boost/python/enum.hpp>
#include <boost/python/list.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/make_constructor.hpp>
#include <boost/python/default_call_policies.hpp>
#include <boost/python/raw_function.hpp>
#include <boost/python/exception_translator.hpp>
#include <boost/python/raw_function.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/str.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/multi_array.hpp>
#include <shyft/core/core_serialization.h>
#include <shyft/py/api/doc_builder.h>

//--extra to help us make implicit convertible, supplying a function to do the inplace conversion
namespace boost::python {
  namespace converter {

    template <class Source, class Target, class Fx>
    struct fx_implicit {
      static void* convertible(PyObject* obj) {
        // Find a converter which can produce a Source instance from
        // obj. The user has told us that Source can be converted to
        // Target, and instantiating construct() below, ensures that
        // at compile-time.
        return implicit_rvalue_convertible_from_python(obj, registered<Source>::converters) ? obj : 0;
      }

      static void construct(PyObject* obj, rvalue_from_python_stage1_data* data) {
        void* storage = ((rvalue_from_python_storage<Target>*) data)->storage.bytes;
        arg_from_python<Source> get_source(obj);
        bool convertible = get_source.convertible();
        BOOST_VERIFY(convertible);
        Fx::construct(get_source(), (Target*) storage);
        // record successful construction
        data->convertible = storage;
      }
    };

  } // namespace converter

  template <class Source, class Target, class Fx>
  void fx_implicitly_convertible(boost::type<Source>* = 0, boost::type<Target>* = 0) {
    using functions = converter::fx_implicit<Source, Target, Fx>;

    converter::registry::push_back(
      &functions::convertible,
      &functions::construct,
      type_id<Target>()
#ifndef BOOST_PYTHON_NO_PY_SIGNATURES
        ,
      &converter::expected_from_python_type_direct<Source>::get_pytype
#endif
    );
  }

} // namespace boost::python

//-- extra to help us survive boost python std::shared_ptr //weak_ptr problems
namespace boost::python::converter {

  /** @brief shared_ptr_from_python specialization for std:shared_ptr
   *
   * @details
   * The standard boost::python::shared_ptr_from_python have some special trick
   * trying to preserve the python id(obj) for some simple use-patterns.
   *
   * This is done using a shared_ptr<void> with customer deleter with reference to the python object,
   * allowing the same python object to be revealed to the python user.
   *
   * Unfortunately, that implementation breaks the weak-pointer mechanism of shared-ptr,
   * invalidating any reasonable use inside the the c++ library.
   *
   * Thus, we choose to undo this approach, using standard robust c++ mechanisms,
   * and sacrifice the 'id(obj)' requirement.
   *
   * Notice that the implementation is a copy of the original, with the exception
   * of the construct method.
   */
  template <class T>
  struct shared_ptr_from_python<T, std::shared_ptr> {

    shared_ptr_from_python() {
      converter::registry::insert(
        &convertible,
        &construct,
        type_id<std::shared_ptr<T> >()
#ifndef BOOST_PYTHON_NO_PY_SIGNATURES
          ,
        &converter::expected_from_python_type_direct<T>::get_pytype
#endif
      );
    }

   private:
    static void* convertible(PyObject* p) {
      if (p == Py_None)
        return p;
      return converter::get_lvalue_from_python(p, registered<T>::converters);
    }

    static void construct(PyObject* source, rvalue_from_python_stage1_data* data) {
      void* const storage = ((converter::rvalue_from_python_storage<std::shared_ptr<T> >*) data)->storage.bytes;
      if (data->convertible == source) { // Deal with the "None" case.
        new (storage) std::shared_ptr<T>();
      } else {
        reference_arg_from_python<std::shared_ptr<T>&> sp(
          source);                                // holder = shared_ptr<reservoir>, source ===> shared_ptr<..>
        if (sp.convertible()) {                   // in this case, we succeded retrieving the shared-ptr ref
          new (storage) std::shared_ptr<T>(sp()); // use standard shared-ptr handling, proven to work.
        } else {
          // no luck with digging out the shared ptr from the source,
          // - despite convertible(source) and get_lvalue_from_python was successful ??
          // Ok, I resign, and ..:
          // use original terrible hack, with aliasing constructor reff to the py object, and deleter
          // that breaks any usage of weak-pointers, silently!
          std::shared_ptr<void> hold_convertible_ref_count((void*) 0, shared_ptr_deleter(handle<>(borrowed(source))));
          new (storage) std::shared_ptr<T>(hold_convertible_ref_count, static_cast<T*>(data->convertible));
        }
      }
      data->convertible = storage;
    }
  };
}


namespace expose {
  namespace py = boost::python;
  extern void handle_pyerror();
}
