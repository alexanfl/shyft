/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include <shyft/py/api/bindings.h>

// #define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include <shyft/py/api/numpy_boost_python.hpp>
#include <shyft/py/api/expose_container.h>

namespace expose {
  using namespace shyft::core;
  using namespace std;
  namespace sa = shyft::api;
  namespace sc = shyft::core;
  namespace ts = shyft::time_series;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::gta_t;

  static void* np_import() {
    import_array();
    return nullptr;
  }

  template <class T>
  static T x_arg(py::tuple const & args, size_t i) {
    if (py::len(args) + 1 < (int) i)
      throw std::runtime_error("missing arg #" + std::to_string(i) + std::string(" in time"));
    py::object o = args[i];
    py::extract<T> xtract_arg(o);
    return xtract_arg();
  }

  ats_vector create_tsv_from_np(gta_t const & ta, numpy_boost<double, 2> const & a, ts::ts_point_fx point_fx) {
    ats_vector r;
    size_t n_ts = a.shape()[0];
    size_t n_pts = a.shape()[1];
    if (ta.size() != n_pts)
      throw std::runtime_error("time-axis should have same length as second dim in numpy array");
    r.reserve(n_ts);
    for (size_t i = 0; i < n_ts; ++i) {
      std::vector<double> v;
      v.reserve(n_pts);
      for (size_t j = 0; j < n_pts; ++j)
        v.emplace_back(a[i][j]);
      r.emplace_back(ta, v, point_fx);
    }
    return r;
  }

  template <class T>
  static vector<T> FromNdArray(numpy_boost<T, 1> const & npv) {
    vector<T> r;
    r.reserve(npv.shape()[0]);
    for (size_t i = 0; i < npv.shape()[0]; ++i) {
      r.push_back(npv[i]);
    }
    return r;
  }

  template <class T>
  static numpy_boost<T, 1> ToNpArray(vector<T> const & v) {
    int dims[] = {int(v.size())};
    numpy_boost<T, 1> r(dims);
    for (size_t i = 0; i < r.size(); ++i) {
      r[i] = v[i];
    }
    return r;
  }

  template <class T>
  static void expose_numpy_vector(char const * name) {
    typedef std::vector<T> XVector;

    py::class_<XVector>(name)
      .def(py::vector_indexing_suite<XVector>()) // meaning it get all it needs to appear as python list
      .def(py::init< XVector const &>(
        (py::arg("const_ref_v")),
        doc.intro("copy construct a new strongly typed list")()

          )) // so we can copy construct
      .def("FromNdArray", FromNdArray<T>)
      .staticmethod("FromNdArray") // BW compatible
      .def("from_numpy", FromNdArray<T>)
      .staticmethod("from_numpy")                        // static construct from numpy TODO: fix __init__
      .def("to_numpy", ToNpArray<T>, "convert to numpy") // Ok, to make numpy 1-d arrays
      .def(py::self == py::self);
    numpy_boost_python_register_type<T, 1>(); // register the numpy object so we can access it in C++
    py_api::iterable_converter().from_python<XVector>();
  }

  extern int64_t utctime_range_check(int64_t sec);
  extern double utctime_range_check(double sec);

  static void expose_ts_vector_create() {
    py::def(
      "create_ts_vector_from_np_array",
      &create_tsv_from_np,
      (py::arg("time_axis"), py::arg("np_array"), py::arg("point_fx")),
      doc.intro("Create a TsVector from specified time_axis, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "TsVector",
          "a TsVector of length first np_array dim, n_ts, each with time-axis, values and point_fx")());
    numpy_boost_python_register_type<double, 2>();
  }

  typedef std::vector<utctime> UtcTimeVector;
  extern utctime x_kwarg_utctime(py::tuple const & args, py::dict const & kwargs, size_t i, char const * kw);

  struct utc_ext {
    static UtcTimeVector* create_default() {
      return new UtcTimeVector();
    }

    static UtcTimeVector* create_from_intv(std::vector<int64_t> const & v) {
      auto r = new UtcTimeVector();
      r->reserve(v.size());
      for (auto s : v)
        r->emplace_back(utctime{std::chrono::seconds(utctime_range_check(int64_t(s)))});
      return r;
    }

    static UtcTimeVector* create_from_doublev(std::vector<double> const & v) {
      auto r = new UtcTimeVector();
      r->reserve(v.size());
      for (auto s : v)
        r->emplace_back(from_seconds(utctime_range_check(s)));
      return r;
    }

    template <class tp>
    static UtcTimeVector* create_from_np_tp(numpy_boost<tp, 1> const & npv) {
      auto r = new UtcTimeVector();
      size_t n = npv.shape()[0];
      r->reserve(n);
      for (size_t i = 0; i < n; ++i) {
        r->emplace_back(from_seconds(utctime_range_check(npv[i])));
      }
      return r;
    }

    static UtcTimeVector* create_from_clone(std::vector<utctime> const & v) {
      return new UtcTimeVector(v);
    }

    static UtcTimeVector* create_from_list(py::list times) {
      if (py::len(times) == 0)
        return new UtcTimeVector();
      auto r = new UtcTimeVector();
      size_t n = py::len(times);
      r->reserve(py::len(times));
      for (size_t i = 0; i < n; ++i) {
        py::object oi = times[i];
        py::extract<utctime> xtract_utctime(oi);
        if (xtract_utctime.check()) {
          r->push_back(xtract_utctime());
        } else {
          py::extract<int64_t> xtract_int(oi);
          if (xtract_int.check()) {
            r->push_back(utctime{std::chrono::seconds(utctime_range_check(xtract_int()))});
          } else {
            py::extract<double> xtract_double(oi);
            if (xtract_double.check()) {
              r->push_back(utctime{from_seconds(utctime_range_check(xtract_double()))});
            } else {
              py::extract<string> xtract_string(oi);
              if (xtract_string.check()) {
                r->push_back(create_from_iso8601_string(xtract_string()));
              } else {
                throw std::runtime_error(std::string("failed to convert ") + std::to_string(i) + " element to utctime");
              }
            }
          }
        }
      }
      return r;
    }

    static py::object push_back(py::tuple const & args, py::dict const & kwargs) {
      py::extract<vector<utctime>&> s(args[0]);
      if (!s.check())
        throw std::runtime_error("UtcTimeVector: invalid self to push_back");
      auto& v = s();
      auto t = x_kwarg_utctime(args, kwargs, 1, "t");
      v.push_back(t);
      return py::object();
    }
  };

  static numpy_boost<int64_t, 1> utctime_to_numpy(vector<utctime> const & v) {
    int dims[] = {int(v.size())};
    numpy_boost<int64_t, 1> r(dims);
    for (size_t i = 0; i < r.size(); ++i) {
      r[i] = to_seconds64(v[i]);
    }
    return r;
  }

  static numpy_boost<double, 1> utctime_to_numpy_double(vector<utctime> const & v) {
    int dims[] = {int(v.size())};
    numpy_boost<double, 1> r(dims);
    for (size_t i = 0; i < r.size(); ++i) {
      r[i] = to_seconds(v[i]);
    }
    return r;
  }

  static vector<utctime> utctime_from_numpy(numpy_boost<int64_t, 1> const & npv) {
    size_t n = npv.shape()[0];
    vector<utctime> r;
    r.reserve(n);
    for (size_t i = 0; i < npv.shape()[0]; ++i) {
      r.emplace_back(from_seconds(utctime_range_check(npv[i])));
    }
    return r;
  }

  static void expose_utctime_vector() {

    py::class_<UtcTimeVector>("UtcTimeVector", py::no_init)
      .def(py::vector_indexing_suite<UtcTimeVector>()) // meaning it get all it needs to appear as python list
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_default, py::default_call_policies()),
        doc.intro("construct empty UtcTimeVecor")())
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_from_clone, py::default_call_policies(), (py::arg("clone_me"))),
        doc.intro("construct a copy of supplied  UtcTimeVecor")
          .parameters()
          .parameter("clone_me", "UtcTimeVector", "to be cloned")())
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_from_intv, py::default_call_policies(), (py::arg("seconds_vector"))),
        doc.intro("construct a from seconds epoch utc as integer")
          .parameters()
          .parameter("seconds", "IntVector", "seconds")())
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_from_doublev, py::default_call_policies(), (py::arg("seconds_vector"))),
        doc.intro("construct a from seconds epoch utc as float")
          .parameters()
          .parameter("seconds", "DoubleVector", "seconds, up to us resolution epoch utc")())
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_from_list, py::default_call_policies(), (py::arg("times"))),
        doc.intro("construct a from a list of something that is convertible to UtcTime")
          .parameters()
          .parameter("times", "list", "a list with convertible times")())
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_from_np_tp<int64_t>, py::default_call_policies(), (py::arg("np_times"))),
        doc.intro("construct a from a numpy array of int64 s epoch")
          .parameters()
          .parameter("np_times", "list", "a list with convertible times")())
      .def(
        "__init__",
        py::make_constructor(&utc_ext::create_from_np_tp<double>, py::default_call_policies(), (py::arg("np_times"))),
        doc.intro("construct a from a numpy array of float s epoch")
          .parameters()
          .parameter("np_times", "list", "a list with float convertible times")())
      .def(
        "push_back",
        py::raw_function(utc_ext::push_back, 2),
        doc.intro("appends a utctime like value to the vector")
          .parameters()
          .parameter("t", "utctime", "an int (seconds), or utctime")())
      .def("from_numpy", utctime_from_numpy)
      .staticmethod("from_numpy") // bw.compatible
      .def(
        "to_numpy",
        utctime_to_numpy,
        (py::arg("self")),
        doc.intro("convert to numpy array of type np.int64, seconds since epoch")())
      .def(
        "to_numpy_double",
        utctime_to_numpy_double,
        (py::arg("self")),
        doc.intro("convert to numpy array of type np.float64, seconds since epoch")())
      .def(py::self == py::self)
      .def(py::self != py::self);

    numpy_boost_python_register_type<utctime, 1>(); // register the numpy object so we can access it in C++
    numpy_boost_python_register_type<int64_t, 1>();
    py_api::iterable_converter().from_python<UtcTimeVector>();
  }

  using GeoPointVector = std::vector<shyft::core::geo_point>;

  static GeoPointVector create_from_x_y_z_vectors(
    std::vector<double> const & x,
    std::vector<double> const & y,
    std::vector<double> const & z) {
    if (!(x.size() == y.size() && y.size() == z.size()))
      throw std::runtime_error("x,y,z vectors need to have same number of elements");
    GeoPointVector r;
    r.reserve(x.size());
    for (size_t i = 0; i < x.size(); ++i)
      r.emplace_back(x[i], y[i], z[i]);
    return r;
  }

  static void expose_geo_point_vector() {

    py::class_<GeoPointVector>("GeoPointVector", "A vector, list, of GeoPoints")
      .def(py::vector_indexing_suite<GeoPointVector>())
      .def(py::init< GeoPointVector const &>(
        (py::arg("const_ref_v")), doc.intro("copy construct a new GeoPointVector")()

          ))
      .def(
        "create_from_x_y_z",
        create_from_x_y_z_vectors,
        (py::arg("x"), py::arg("y"), py::arg("z")),
        "Create a GeoPointVector from x,y and z DoubleVectors of equal length")
      .staticmethod("create_from_x_y_z")
      .def(py::self == py::self)
      .def(py::self != py::self);
    py_api::iterable_converter().from_python<GeoPointVector>();
  }

  void vectors() {
    np_import();
    expose_geo_point_vector();
    expose_vector<std::string>("StringVector");
    expose_numpy_vector<double>("DoubleVector");
    expose_numpy_vector<vector<double>>("DoubleVectorVector");
    expose_numpy_vector<int64_t>("IntVector");
    numpy_boost_python_register_type<int, 1>();
    expose_numpy_vector<char>("ByteVector");
    expose_utctime_vector();
    expose_ts_vector_create();
  }
}
