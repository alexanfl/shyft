#include <vector>
#include <string>
#include <stdexcept>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/geo.h>

#include <shyft/py/api/bindings.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

#include <numpy/arrayobject.h>
#include <shyft/py/api/numpy_boost_python.hpp>

namespace expose {
  using shyft::core::utcperiod;
  using shyft::core::utctimespan;
  using shyft::core::utctime;
  using shyft::time_series::ts_point_fx;
  using shyft::core::geo_point;
  using shyft::time_series::dd::gta_t;
  using shyft::time_series::dd::apoint_ts;
  /** extension class do deal with vector constructs etc. */
  using geo_tsv = shyft::time_series::dd::geo_ts_vector;
  using std::runtime_error;
  using std::to_string;
  using std::vector;
  using std::string;

  using shyft::core::geo_point;
  using shyft::time_series::dd::apoint_ts;
  using std::vector;

  struct geo_tsv_ext {
    static geo_tsv *create_default() {
      return new geo_tsv{};
    }

    static geo_tsv *create_from_list(py::list tsl) {
      size_t n = py::len(tsl);
      if (n == 0)
        return new geo_tsv{};
      auto r = new geo_tsv{};
      r->reserve(n);
      for (size_t i = 0; i < n; ++i) {
        py::object ts = tsl[i];
        py::extract<geo_tsv::value_type> xts(ts);
        if (xts.check()) {
          r->push_back(xts());
        } else {
          throw runtime_error("Failed to convert " + to_string(i) + " element to GeoTimeSeries");
        }
      }
      return r;
    }

    static geo_tsv *create_from_geo_tsv_from_np(
      gta_t const &ta,
      vector<geo_point> const &gpv,
      numpy_boost<double, 2> const &a,
      ts_point_fx point_fx) {
      size_t n_ts = a.shape()[0];
      size_t n_pts = a.shape()[1];
      if (ta.size() != n_pts)
        throw runtime_error("time-axis should have same length as second dim in numpy array");
      if (n_ts != gpv.size())
        throw runtime_error("geo-point vector should have same size as first dim (n_ts) in numpy array");
      geo_tsv *r = new geo_tsv{};
      r->reserve(n_ts);
      for (size_t i = 0; i < n_ts; ++i) {
        std::vector<double> v;
        v.reserve(n_pts);
        for (size_t j = 0; j < n_pts; ++j)
          v.emplace_back(a[i][j]);
        r->emplace_back(gpv[i], apoint_ts(ta, v, point_fx));
      }
      return r;
    }

    static vector<double> values_at_time(geo_tsv const &gtsv, utctime t) {
      std::vector<double> r;
      if (gtsv.size()) {
        r.reserve(gtsv.size());
        for (auto const &gts : gtsv)
          r.push_back(gts.ts(t));
      }
      return r;
    }
  };

  static void geo_ts() {
    using shyft::time_series::dd::geo_ts;
    py::class_<geo_ts>(
      "GeoTimeSeries",
      doc.intro("A minimal geo-located time-series, a time-series plus a representative 3d mid_point")())
      .def(py::init<geo_point, apoint_ts>(
        (py::arg("mid_point"), py::arg("ts")),
        doc.intro("Construct a GeoTimeSeries")
          .parameters()
          .parameter("mid_point", "GeoPoint", "The 3d location representative for ts")
          .parameter("ts", "TimeSeries", "Any kind of TimeSeries")()))
      .def_readwrite("ts", &geo_ts::ts, "TimeSeries: the assigned time-series")
      .def_readwrite(
        "mid_point",
        &geo_ts::mid_point,
        "GeoPoint: the mid-point(of an area) for which the assigned time-series is valid")
      .def(py::self == py::self)
      .def(py::self != py::self);
    // consider holder object shared_ptr to geo_tsv
    py::class_<geo_tsv>("GeoTimeSeriesVector", py::no_init)
      .def(py::vector_indexing_suite<geo_tsv>())
      .def(
        "__init__",
        py::make_constructor(&geo_tsv_ext::create_default, py::default_call_policies()),
        doc.intro("Create an empty TsVector")())

      .def(
        "__init__",
        make_constructor(&geo_tsv_ext::create_from_list, py::default_call_policies(), (py::arg("geo_ts_list"))),
        doc.intro("Create a GeoTimeSeriesVector from a python list of GeoTimeSeries")
          .parameters()
          .parameter("geo_ts_list", "List[GeoTimeSeries]", "A list of GeoTimeSeries")())
      .def(
        "__init__",
        make_constructor(
          &geo_tsv_ext::create_from_geo_tsv_from_np,
          py::default_call_policies(),
          (py::arg("time_axis"), py::arg("geo_points"), py::arg("np_array"), py::arg("point_fx"))),
        doc.intro("Create a GeoTimeSeriesVector from time-axis,geo-points,2d-numpy-array and point-interpretation")
          .parameters()
          .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
          .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
          .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
          .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
          .returns(
            "GeoTimeSeriesVector",
            "GeoTimeSeriesVector",
            "a GeoTimeSeriesVector of length first np_array dim, n_ts, each with geo-point and time-series with "
            "time-axis, values and point_fx")())
      .def(
        "values_at_time",
        &geo_tsv_ext::values_at_time,
        (py::arg("self"), py::arg("t")),
        doc.intro("The values at specified time as a DoubleVector, that you can use .to_numpy() to get np array from")
          .intro("This function can be suitable if you are doing area-animated (birds-view) presentations")
          .parameters()
          .parameter("t", "time", "the time that should be used for getting each value")
          .returns("values", "DoubleVector", "The evaluated geo.ts(t) for all items in the vector")())
      .def(py::self == py::self);
    numpy_boost_python_register_type<double, 2>(); // should only be done once.
    // TODO: convertible functions from GeoTimeSeriesVector -> TemperatureSourceVector etc..
  }

  static void geo_query() {
    using geo_query = shyft::dtss::geo::query;
    py::class_<geo_query>("GeoQuery", doc.intro("A query as a polygon with specified geo epsg coordinate system")())
      .def(py::init<int64_t, vector<geo_point>>(
        (py::arg("epsg"), py::arg("points")),
        doc.intro("Construct a GeoQuery from specified parameterrs")
          .parameters()
          .parameter("epsg", "int", "A valid epsg for the polygon, and also wanted coordinate system")
          .parameter("points", "GeoPointVector", "3 or more points forming a polygon that is the spatial scope")()))
      .def_readonly("epsg", &geo_query::epsg, "int: the epsg coordinate system")
      .def_readonly("polygon", &geo_query::polygon, "GeoPointVector: the polygon giving the spatial scope")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static void geo_grid_spec() {
    using geo_grid_spec = shyft::dtss::geo::grid_spec;

    py::class_<geo_grid_spec>(
      "GeoGridSpec",
      doc.intro("A point set for a geo-grid, but does not have to be a regular grid.")
        .intro("It serves the role of defining the spatial representative points for")
        .intro("a typical spatial  grid, e.g as for arome, or ec forecasts.")())
      .def(py::init<int64_t, vector<geo_point>>(
        (py::arg("epsg"), py::arg("points")),
        doc.intro("Construct a GeoQuery from specified parameterrs")
          .parameters()
          .parameter("epsg", "int", "A valid epsg for the spatial points")
          .parameter(
            "points", "GeoPointVector", "0 or more representative points for the spatial properties of the grid")()))
      .def_readonly("epsg", &geo_grid_spec::epsg, "int: the epsg coordinate system")
      .def_readonly("points", &geo_grid_spec::points, "GeoPoinVector: the representative points of the spatial grid")
      .def(
        "find_geo_match",
        &geo_grid_spec::find_geo_match_ix,
        (py::arg("self"), py::arg("geo_query")),
        doc.intro("finds the points int the grid that is covered by the polygon of the geo_query")
          .intro("note: that currently we only consider the horizontal dimension when matching points")
          .parameters()
          .parameter("geo_query", "GeoQuery", "A polygon giving an area to capture")
          .returns(
            "matches",
            "IntVector",
            "a list of all points that is inside, or on the border of the specified polygon, in guaranteed ascending "
            "point index order")())
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static void geo_slice() {
    using geo_slice = shyft::dtss::geo::slice;
    using shyft::dtss::geo::ix_vector;

    py::class_<geo_slice>(
      "GeoSlice",
      doc.intro("Keeps data that describes as slice into the t0-variable-ensemble-geo, (t,v,e,g), space.")
        .intro("It is the result-type of GeoTimeSeriesConfiguration.compute(GeoEvalArgs)")
        .intro("and is passed to the geo-db-read callback to specify wanted time-series to read.")
        .intro("Note that the content of a GeoSlice can only be interpreteded in terms of the")
        .intro("GeoTimeSeriesConfiguration it is derived from.")
        .intro("The indicies and values of the slice, strongly relates to the definition of it's geo-tsdb.")())
      .def(py::init<ix_vector const &, ix_vector const &, ix_vector const &, vector<utctime> const &, utctime>(
        (py::arg("v"), py::arg("g"), py::arg("e"), py::arg("t"), py::arg("ts_dt")),
        doc.intro("Construct a GeoSlice from supplied vectors.")
          .parameters()
          .parameter("v", "IntVector", "list of variables idx, each defined by GeoTimeSeriesConfiguration.variables[i]")
          .parameter("e", "IntVector", "list of ensembles, each in range 0..GeoTimeSeriesConfiguration.n_ensembles-1")
          .parameter(
            "g", "IntVector", "list of geo-point idx, each defined by GeoTimeSeriesConfiguration.grid.points[i]")
          .parameter(
            "t",
            "UtcTimeVector",
            "list of t0-time points, each of them should exist in GeoTimeSeriesConfiguration.t0_times")
          .parameter("ts_dt", "time", "time-length to read from each time-series, we read from [t0 .. t0+ts_dt>")()))
      .def_readwrite(
        "v", &geo_slice::v, "IntVector: list of variables idx, each defined by GeoTimeSeriesConfiguration.variables[i]")
      .def_readwrite(
        "e", &geo_slice::e, "IntVector: list of ensembles, each in range 0..GeoTimeSeriesConfiguration.n_ensembles-1")
      .def_readwrite(
        "g",
        &geo_slice::g,
        "IntVector: list of geo-point idx, each defined by GeoTimeSeriesConfiguration.grid.points[i]")
      .def_readwrite(
        "t",
        &geo_slice::t,
        "UtcTimeVector: list of t0-time points, each of them should exist in GeoTimeSeriesConfiguration.t0_times")
      .def_readwrite("ts_dt", &geo_slice::ts_dt, "time: time length to read from each time-series, [t0 .. t0+ts_dt>")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static void geo_ts_config() {
    using geo_ts_db_config = shyft::dtss::geo::ts_db_config;
    using geo_grid_spec = shyft::dtss::geo::grid_spec;

    py::class_<geo_ts_db_config, py::bases<>, std::shared_ptr<geo_ts_db_config> >(
      "GeoTimeSeriesConfiguration",
      doc.intro("Contain minimal  description to efficiently work with arome/ec  forecast data")
        .intro("It defines the spatial, temporal and ensemble dimensions available, and")
        .intro("provides means of mapping a GeoQuery to a set of ts_urls that ")
        .intro("serves as keys for manipulating and assembling forcing input data")
        .intro("for example to the shyft hydrology region-models.")())
      .def(py::init<
           string const &,
           string const &,
           string const &,
           geo_grid_spec const &,
           vector<utctime> const &,
           utctime,
           int64_t,
           vector<string> const &>(
        (py::arg("prefix"),
         py::arg("name"),
         py::arg("description"),
         py::arg("grid"),
         py::arg("t0_times"),
         py::arg("dt"),
         py::arg("n_ensembles"),
         py::arg("variables")),
        doc.intro("Construct a GeoQuery from specified parameterrs")
          .parameters()
          .parameter(
            "prefix",
            "str",
            "ts-url prefix, like shyft:// for internally stored ts, or geo:// for externally stored parts")
          .parameter("name", "str", "A shortest possible unique name of the configuration")
          .parameter("description", "str", "a human readable description of the configuration")
          .parameter("grid", "GeoGridSpec", "specification of the spatial grid")
          .parameter(
            "t0_times",
            "UtcTimeVector",
            "List of time where we have register time-series,e.g forecast times, first timepoint")
          .parameter("dt", "time", "the (max) length of each geo-ts, so geo_ts total_period is [t0..t0+dt>")
          .parameter("n_ensembles", "int", "number of ensembles available, must be >0, 1 if no ensemples")
          .parameter("variables", "string", "list of minimal keys, representing temperature, preciptiation etc")()))
      .def_readonly(
        "prefix",
        &geo_ts_db_config::prefix,
        "str: ts-url prefix, like shyft:// for internally stored ts, or geo:// for externally stored parts")
      .def_readwrite("name", &geo_ts_db_config::name, "str: the name for the config (keep it minimal)")
      .def_readwrite("description", &geo_ts_db_config::descr, "str: the human readable description of this geo ts db")
      .def_readonly("grid", &geo_ts_db_config::grid, "GeoGridSpec: the spatial grid definition")
      .def_readwrite(
        "t0_times",
        &geo_ts_db_config::t0_times,
        "UtcTimeVector: list of time-points, where there are registred/available time-series")
      .def_readonly(
        "dt", &geo_ts_db_config::dt, "time: the (max) length of each geo-ts, so geo_ts total_period is [t0..t0+dt>")
      .def_readonly("n_ensembles", &geo_ts_db_config::n_ensembles, "int: number of ensembles available, range 1..n")
      .def_readonly(
        "variables",
        &geo_ts_db_config::variables,
        "StringList: the list of available properties, like short keys for preciptation,temperature etc")
      .def_readonly("t0_time_axis", &geo_ts_db_config::t0_time_axis, "TimeAxis: t0 time-points as time-axis")
      //-- methods

      .def(
        "compute",
        &geo_ts_db_config::compute,
        (py::arg("self"), py::arg("eval_args")),
        doc.intro("Compute the GeoSlice from evaluation arguments")
          .parameters()
          .parameter("eval_args", "GeoEvalArgs", "Specification to evaluate")
          .returns("geo_slice", "GeoSlice", "A geo-slice describing (t0,v,e,g) computed")())
      .def(
        "find_geo_match_ix",
        &geo_ts_db_config::find_geo_match_ix,
        (py::arg("self"), py::arg("geo_query")),
        doc.intro("Returns the indicies to the points that matches the geo_query (polygon)")
          .parameters()
          .parameter("geo_query", "GeoQuery", "The query, polygon that matches the spatial scope")
          .returns("point_indexes", "IntVector", "The list of indicies that  matches the geo_query")())
      .def(
        "create_geo_ts_matrix",
        &geo_ts_db_config::create_geo_ts_matrix,
        (py::arg("self"), py::arg("slice")),
        doc
          .intro("Creates a GeoTsMatrix(element type is GeoTimeSeries) to hold the values according to dimensionality "
                 "of GeoSlice")
          .parameters()
          .parameter(
            "slice",
            "GeoSlice",
            "a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
          .returns("geo_ts_matrix", "GeoTsMatrix", "ready to be filled in with points and time-series")())
      .def(
        "create_ts_matrix",
        &geo_ts_db_config::create_ts_matrix,
        (py::arg("self"), py::arg("slice")),
        doc
          .intro("Creates a GeoMatrix (element type is TimeSeries only) to hold the values according to dimensionality "
                 "of GeoSlice")
          .parameters()
          .parameter(
            "slice",
            "GeoSlice",
            "a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
          .returns("ts_matrix", "GeoMatrix", "ready to be filled in time-series(they are all empty/null)")())
      .def(
        "bounding_box",
        &geo_ts_db_config::bounding_box,
        (py::arg("self"), py::arg("slice")),
        doc
          .intro("Compute the 3D bounding_box, as two GeoPoints containing the min-max of x,y,z of points in the "
                 "GeoSlice")
          .intro("Could be handy when generating queries to externally  stored geo-ts databases like netcdf etc.")
          .intro("See also convex_hull().")
          .parameters()
          .parameter(
            "slice",
            "GeoSlice",
            "a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
          .returns(
            "bbox", "GeoPointVector", "with two GeoPoints, [0] keeping the minimum x,y,z, and [1] the maximum x,y,z")())
      .def(
        "convex_hull",
        &geo_ts_db_config::convex_hull,
        (py::arg("self"), py::arg("slice")),
        doc.intro("Compute the 2D convex hull, as a list of GeoPoints describing the smallest convex planar polygon ")
          .intro("containing all points in the slice wrt. x,y. ")
          .intro("The returned point sequence is 'closed', i.e the first and last point in the sequence are equal. ")
          .intro("See also bounding_box().")
          .parameters()
          .parameter(
            "slice",
            "GeoSlice",
            "a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
          .returns("hull", "GeoPointVector", "containing the sequence of points of the convex hull polygon.")())
      //-- handy stuff
      .def(py::self == py::self)
      .def(py::self != py::self);
    using GDBVector = vector<std::shared_ptr<geo_ts_db_config>>;
    py::class_<GDBVector>(
      "GeoTimeSeriesConfigurationVector", doc.intro("A strongly typed list of GeoTimeSeriesConfigurations")())
      .def(py::vector_indexing_suite<GDBVector, true>()) // important! do not wrap shared ptr in vectors
      ;
  }

  static void geo_eval_args() {
    using geo_args = shyft::dtss::geo::eval_args;
    using geo_query = shyft::dtss::geo::query;
    using shyft::time_series::dd::ats_vector;

    py::class_<geo_args>(
      "GeoEvalArgs",
      doc.intro(
        "GeoEvalArgs is used for the geo-evaluate functions.\n\n"
        "It describes scope for the geo-evaluate function, in terms of:\n"
        "  \n"
        "   - the geo-ts database identifier\n"
        "   - variables to extract, by names\n"
        "   - ensemble members (list of ints)\n"
        "   - temporal, using `t0` from specified time-axis, `+ ts_dt` for time-range\n"
        "   - spatial, using `points` for a polygon\n"
        "\n"
        "and optionally:\n"
        "  \n"
        "   - the concat postprocessing with parameters\n"
        "\n")())
      .def(py::init<
           string const &,
           vector<string> const &,
           vector<int64_t> const &,
           gta_t const &,
           utctime,
           geo_query const &,
           bool,
           utctime >(
        (py::arg("geo_ts_db_id"),
         py::arg("variables"),
         py::arg("ensembles"),
         py::arg("time_axis"),
         py::arg("ts_dt"),
         py::arg("geo_range"),
         py::arg("concat"),
         py::arg("cc_dt0")),
        doc.intro("Construct GeoEvalArgs from specified parameters")
          .parameters()
          .parameter(
            "geo_ts_db_id",
            "str",
            "identifies the geo-ts-db, short, as 'arome', 'ec', as specified with server.add_geo_ts_db(cfg)")
          .parameter(
            "variables", "StringVector", "names of the wanted variables, if empty, return all variables configured")
          .parameter("ensembles", "IntVector", "List of ensembles, if empty, return all ensembles configured")
          .parameter(
            "time_axis",
            "TimeAxis",
            "specifies the t0, and  .total_period().end is used as concact open-end fill-in length")
          .parameter(
            "ts_dt",
            "time",
            "specifies the time-length to read from  each time-series,t0.. t0+ts_dt, and  .total_period().end is used "
            "as concact open-end fill-in length")
          .parameter("geo_range", "GeoQuery", "the spatial scope of the query, if empty, return all configured")
          .parameter(
            "concat", "bool", "postprocess using concatenated forecast, returns 'one' concatenated forecast from many.")
          .parameter(
            "cc_dt0", "time", "concat lead-time, skip cc_dt0 of each forecast (offsets the slice you selects)")()))
      .def(py::init<
           string const &,
           vector<int64_t> const &,
           gta_t const &,
           utctime,
           geo_query const &,
           bool,
           utctime,
           ats_vector const & >(
        (py::arg("geo_ts_db_id"),
         py::arg("ensembles"),
         py::arg("time_axis"),
         py::arg("ts_dt"),
         py::arg("geo_range"),
         py::arg("concat"),
         py::arg("cc_dt0"),
         py::arg("ts_expressions")),
        doc.intro("Construct GeoEvalArgs from specified parameters")
          .parameters()
          .parameter(
            "geo_ts_db_id",
            "str",
            "identifies the geo-ts-db, short, as 'arome', 'ec', as specified with server.add_geo_ts_db(cfg)")
          .parameter("ensembles", "IntVector", "List of ensembles, if empty, return all ensembles configured")
          .parameter(
            "time_axis",
            "TimeAxis",
            "specifies the t0, and  .total_period().end is used as concact open-end fill-in length")
          .parameter(
            "ts_dt",
            "time",
            "specifies the time-length to read from  each time-series,t0.. t0+ts_dt, and  .total_period().end is used "
            "as concact open-end fill-in length")
          .parameter("geo_range", "GeoQuery", "the spatial scope of the query, if empty, return all configured")
          .parameter(
            "concat", "bool", "postprocess using concatenated forecast, returns 'one' concatenated forecast from many.")
          .parameter("cc_dt0", "time", "concat lead-time, skip cc_dt0 of each forecast (offsets the slice you selects)")
          .parameter(
            "ts_expressions",
            "TsVector",
            "expressions to evaluate, where the existing variables are referred to by index number as a string, ex. "
            "TimeSeries('0') ")()))
      .def_readwrite("geo_ts_db_id", &geo_args::geo_ts_db_id, "str: the name for the config (keep it minimal)")
      .def_readwrite(
        "variables", &geo_args::variables, "StringVector: the human readable description of this geo ts db")
      .def_readwrite("ens", &geo_args::ens, "IntVector: list of ensembles to return, empty=all, if specified >0")
      .def_readwrite(
        "t0_time_axis",
        &geo_args::ta,
        "TimeAxis: specifies the t0, and  .total_period().end is used as concact open-end fill-in length")
      .def_readwrite(
        "ts_dt", &geo_args::ts_dt, "time: specifies the time-length to read from  each time-series,t0.. t0+ts_dt,")
      .def_readwrite("geo_range", &geo_args::geo_range, "GeoQuery: the spatial scope, as simple polygon")
      .def_readwrite(
        "concat",
        &geo_args::concat,
        "bool: postprocess using concatenated forecast, returns 'one' concatenated forecast from many")
      .def_readwrite("cc_dt0", &geo_args::cc_dt0, "time: concat lead-time")
      .def_readwrite(
        "ts_expressions",
        &geo_args::ts_expressions,
        "TsVector: time series expressions to evaluate instead of variables")

      //-- handy stuff
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  using shyft::time_series::dd::ats_vector;
  using shyft::dtss::geo::detail::ix_calc;

  void def_geo_matrix_shape() {
    using shyft::dtss::geo::detail::ix_calc;
    py::class_<ix_calc>("GeoMatrixShape", py::no_init)
      .def(py::init<int, int, int, int>(
        (py::arg("n_t0"), py::arg("n_v"), py::arg("n_e"), py::arg("n_g")),
        doc.intro("Create with specified dimensionality")()))
      .def_readonly("n_t0", &ix_calc::n_t0, "int: number of t0, e.g forecasts")
      .def_readonly("n_v", &ix_calc::n_v, "int: number of variables")
      .def_readonly("n_e", &ix_calc::n_e, "int: number of ensembles")
      .def_readonly("n_g", &ix_calc::n_g, "int: number of geo points")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  void def_ts_matrix() {
    using shyft::dtss::geo::ts_matrix;
    py::class_<ts_matrix>(
      "GeoMatrix",
      doc.intro("GeoMatrix is 4d matrix,index dimensions (t0,variable,ensemble,geo_point)")
        .intro("to be understood as a slice of a geo-ts-db (slice could be the entire db)")
        .intro("The element type of the matrix is TimeSeries")(),
      py::no_init)
      .def(py::init<int, int, int, int>(
        (py::arg("n_t0"), py::arg("n_v"), py::arg("n_e"), py::arg("n_g")),
        doc.intro("create GeoMatrix with specified t0,variables,ensemble and geo-point dimensions")()))
      .def(
        "set_ts",
        &ts_matrix::set_ts,
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e"), py::arg("g"), py::arg("ts")),
        doc.intro("performs self[t,v,e,g]= ts")())
      .def(
        "get_ts",
        &ts_matrix::ts,
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e"), py::arg("g")),
        doc.intro("return self[t,v,e,g] of type TimeSeries")(),
        py::return_value_policy<py::copy_const_reference>())
      .def_readonly("shape", &ts_matrix::shape, "GeoMatrixShape: the shape of the GeoMatrix")
      .def(
        "concatenate",
        &ts_matrix::concatenate,
        (py::arg("self"), py::arg("cc_dt0"), py::arg("concat_interval")),
        doc.intro("Concatenate all the forecasts in the GeoMatrix using supplied parameters")
          .parameters()
          .parameter("cc_dt0", "time", "skip first period of length cc_dt0 each forecast")
          .parameter("concat_interval", "time", "the nominal length between each ts.time(0) of all time-series")
          .returns("tsm", "GeoMatrix", "A new concatenated geo-ts-matix")())
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  void def_geo_ts_matrix() {
    using shyft::dtss::geo::geo_ts_matrix;
    py::class_<geo_ts_matrix>(
      "GeoTsMatrix",
      doc.intro("GeoTsMatrix is 4d matrix,index dimensions `(t0,variable,ensemble,geo_point)`")
        .intro("to be understood as a slice of a geo-ts-db (slice could be the entire db)")
        .pure("The element types of the matrix is ")
        .ref_class("GeoTimeSeries")
        .intro("")(),
      py::no_init)
      .def(py::init<int, int, int, int>(
        (py::arg("n_t0"), py::arg("n_v"), py::arg("n_e"), py::arg("n_g")),
        doc.intro("create GeoTsMatrix with specified t0,variables,ensemble and geo-point dimensions")()))
      .def_readonly(
        "shape",
        &geo_ts_matrix::shape,
        "GeoMatrixShape: The shape of a GeoMatrix in terms of forecasts(n_t0),variables(n_v),ensembles(n_e) and "
        "geopoints(n_g)")
      .def(
        "set_ts",
        &geo_ts_matrix::set_ts,
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e"), py::arg("g"), py::arg("ts")),
        doc.intro("performs self[t,v,e,g].ts= ts")())
      .def(
        "get_ts",
        &geo_ts_matrix::ts,
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e"), py::arg("g")),
        doc.intro("return self[t,v,e,g] of type TimeSeries")(),
        py::return_value_policy<py::copy_const_reference>())
      .def(
        "set_geo_point",
        &geo_ts_matrix::set_geo_point,
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e"), py::arg("g"), py::arg("point")),
        doc.intro("performs self[t,v,e,g].mid_point= point")())
      .def(
        "get_geo_point",
        &geo_ts_matrix::get_geo_point,
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e"), py::arg("g")),
        doc.intro("return self[t,v,e,g].mid_point of type GeoPoint")(),
        py::return_value_policy<py::copy_const_reference>())
      .def(
        "concatenate",
        &geo_ts_matrix::concatenate,
        (py::arg("self"), py::arg("cc_dt0"), py::arg("concat_interval")),
        doc.intro("Concatenate all the forecasts in the GeoTsMatrix using supplied parameters")
          .parameters()
          .parameter("cc_dt0", "time", "skip first period of length cc_dt0 each forecast")
          .parameter("concat_interval", "time", "the nominal length between each ts.time(0) of all time-series")
          .returns("tsm", "GeoTsMatrix", "A new concatenated geo-ts-matix")())
      .def<geo_ts_matrix (geo_ts_matrix::*)(int v_ix, apoint_ts expr) const>(
        "transform",
        &geo_ts_matrix::transform,
        (py::arg("self"), py::arg("variable"), py::arg("expression")),
        doc.intro("Apply the expression to each time-series of the specified variable.")
          .parameters()
          .parameter("variable", "", "the variable index to select the specific variable, use -1 to apply to all")
          .parameter(
            "expr",
            "",
            "ts expresssion, like 2.0*TimeSeries('x'), where x will be substituted with the variable, notice that its "
            "a required to be just one unbound time-series with the reference name 'x'.")
          .returns(
            "GeoTsMatrix",
            "GeoTsMatrix",
            "A new GeoTsMatrix, where the time-series for the specified varable is transformed")())
      .def<geo_ts_matrix (geo_ts_matrix::*)(ats_vector const &expr_vector) const>(
        "transform",
        &geo_ts_matrix::transform,
        (py::arg("self"), py::arg("expr_vector")),
        doc.intro("Apply the expression to each time-series of the specified variable.")
          .parameters()
          .parameter(
            "expr_vector",
            "",
            "ts expresssions, like 2.0*TimeSeries('0'), where 0 will be substituted with the corresponding variable, "
            "notice that its a required to be just one unbound time-series with the reference name '0'.")
          .returns(
            "GeoTsMatrix",
            "GeoTsMatrix",
            "A new GeoTsMatrix, where the time-series for the specified varable is transformed")())
      .def(
        "evaluate",
        &geo_ts_matrix::evaluate,
        (py::arg("self")),
        doc.intro("Apply the expression to each time-series of the specified variable.")
          .parameters()
          .returns("GeoTsMatrix", "GeoTsMatrix", "A new GeoTsMatrix, where all time-series is evaluated")())
      .def(
        "extract_geo_ts_vector",
        +[](geo_ts_matrix const *me, int tix, int vix, int eix) {
          me->shape.validate(tix, vix, eix, 0);
          shyft::dtss::geo::geo_ts_vector r;
          r.reserve(me->shape.n_g);
          for (size_t g = 0; g < me->shape.n_g; ++g)
            r.emplace_back(me->tsv[me->shape.flat(tix, vix, eix, g)]);
          return r;
        },
        (py::arg("self"), py::arg("t"), py::arg("v"), py::arg("e")),
        doc
          .intro("Given given arguments, return the GeoTimeSeriesVector suitable for constructing GeoPointSource for "
                 "hydrology region-environments forcing data")
          .parameters()
          .parameter("t", "int", "the forecast index, e.g. selects specific forecast, in case of several (t0)")
          .parameter("v", "int", "the variable index, e.g. selects temperature,precipitation etc.")
          .parameter("e", "int", "the ensemble index, in case of many ensemples, select specific ensemble")
          .returns(
            "GeoTimeSeriesVector",
            "GeoTimeSeriesVector",
            "The GeoTsVector for selected forcast time t, variable and ensemble")())
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  void all_geo_stuff() {
    geo_ts();
    geo_query();
    geo_slice();
    geo_grid_spec();
    geo_eval_args();
    geo_ts_config();
    def_ts_matrix();
    def_geo_ts_matrix();
    def_geo_matrix_shape();
  }

}
