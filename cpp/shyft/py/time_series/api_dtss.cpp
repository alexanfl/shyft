/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <mutex>
#include <csignal>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_url.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/queue_msg.h>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/py/api/bindings.h>
#include <shyft/py/scoped_gil.h>

// also consider policy: from
// https://www.codevate.com/blog/7-concurrency-with-embedded-python-in-a-multi-threaded-c-application

namespace shyft::dtss {
  using time_series::dd::gta_t;
  using time_series::dd::apoint_ts;
  using time_series::dd::ats_vector;
  using pyapi::scoped_gil_aquire;
  using pyapi::scoped_gil_release;
  using core::utcperiod;
  using std::mutex;
  using std::unique_lock;

  struct py_server : server {
    py::object cb;                      ///< callback for the read function
    py::object fcb;                     ///< callback for the find function
    py::object scb;                     ///< callback for the store function
    py::object rcb;                     ///< callback for the remove container function
    py::object grcb;                    ///< callback for the geo read function
    py::object gscb;                    ///< callback for the geo store function
    web_api::request_handler bg_server; ///< handle web-api requests
    std::future<int> web_srv;           ///<

    py_server()
      : server(
        [&](id_vector_t const &ts_ids, utcperiod p) {
          return this->fire_cb(ts_ids, p);
        },
        [&](std::string search_expression) {
          return this->find_cb(search_expression);
        },
        [&](ts_vector_t const &tsv) {
          this->store_cb(tsv);
        },
        [&](std::string const &url, bool remove_from_disk) {
          this->remove_container_cb(url, remove_from_disk);
        },
        [&](geo::ts_db_config_ const &cfg, geo::slice const &gs) {
          return this->geo_read_cb(cfg, gs);
        },
        [&](geo::ts_db_config_ cfg, geo::ts_matrix const &vget, bool replace) {
          this->geo_store_cb(cfg, vget, replace);
        }) {
      if (!PyEval_ThreadsInitialized()) {
        PyEval_InitThreads(); // ensure threads-is enabled
      }
      bg_server.srv = this;
    }

    ~py_server() {
      cb = py::object();
      fcb = py::object();
      scb = py::object();
      rcb = py::object();
      grcb = py::object();
      gscb = py::object();
    }

    size_t get_alive_connections() {
      return alive_connections.load();
    }

    bool auth_needed() const {
      return bg_server.auth.needed();
    }

    vector<string> auth_tokens() const {
      return bg_server.auth.tokens();
    }

    void add_auth_tokens(vector<string> const &tokens) {
      bg_server.auth.add(tokens);
    }

    void remove_auth_tokens(vector<string> const &tokens) {
      bg_server.auth.remove(tokens);
    }

    void no_callback_error(string cb_message) {
      throw runtime_error("No python callback provided for : " + cb_message);
    }

    geo::ts_matrix geo_read_cb(geo::ts_db_config_ const &cfg, geo::slice const &g) {
      geo::ts_matrix r;
      if (grcb.ptr() != Py_None) {
        scoped_gil_aquire gil;
        try {
          r = py::call<geo::ts_matrix>(grcb.ptr(), cfg, g);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else
        no_callback_error("geo_read");
      return r;
    }

    void geo_store_cb(geo::ts_db_config_ cfg, geo::ts_matrix const &tsm, bool replace) {
      if (gscb.ptr() != Py_None) {
        scoped_gil_aquire gil;
        try {
          py::call<void>(gscb.ptr(), cfg, tsm, replace);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else
        no_callback_error("geo_store");
    }

    ts_info_vector_t find_cb(std::string search_expression) {
      ts_info_vector_t r;
      if (fcb.ptr() != Py_None) {
        scoped_gil_aquire gil;
        try {
          r = py::call<ts_info_vector_t>(fcb.ptr(), search_expression);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else
        no_callback_error("find_ts");
      return r;
    }

    int store_cb(ts_vector_t const &tsv) {
      int r{0};
      if (scb.ptr() != Py_None) {
        scoped_gil_aquire gil;
        try {
          py::call<void>(scb.ptr(), tsv);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else
        no_callback_error("store_cb");
      return r;
    }

    int remove_container_cb(std::string const &url, bool remove_from_disk) {
      int r{0};
      if (rcb.ptr() != Py_None) {
        scoped_gil_aquire gil;
        try {
          py::call<void>(rcb.ptr(), url, remove_from_disk);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else
        no_callback_error("remove_container_cb");
      return r;
    }

    ts_vector_t fire_cb(id_vector_t const &ts_ids, utcperiod p) {
      ats_vector r;
      if (cb.ptr() != Py_None) {
        scoped_gil_aquire gil;
        try {
          r = py::call<ts_vector_t>(cb.ptr(), ts_ids, p);
        } catch (py::error_already_set const &) {
          handle_pyerror();
        }
      } else
        no_callback_error("read_cb");
      return r;
    }

    void process_messages(int msec) {
      scoped_gil_release gil;
      if (!is_running())
        start_async();
      std::this_thread::sleep_for(std::chrono::milliseconds(msec));
    }

    void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads, bool tls_only) {
      if (!web_srv.valid()) {
        bg_server.running = false;
        web_srv = std::async(
          std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads, tls_only]() -> int {
            return web_api::start_web_server(
              bg_server, host_ip, port, make_shared<string>(doc_root), fg_threads, bg_threads, tls_only);
          });
        size_t x = 0;
        do { // wait until it has started the port before returning
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
        } while (!bg_server.running && ++x < 500);
        if (!bg_server.running) {
          throw std::runtime_error("Failed web-api thread did not signal ready to take sockets");
        }
      }
    }

    void stop_web_api() {
      if (web_srv.valid()) {
        std::raise(SIGINT);
        (void) web_srv.get();
      }
    }

    void stop_server(int timeout_ms) {
      set_graceful_close_timeout(timeout_ms);
      clear();
    }

    vector<geo::ts_db_config_> get_geo_ts_db_info() {
      return do_get_geo_info();
    }
  };

  struct py_srv_connection { // simple struct for exposing information from shyft::core::srv_connection
    string host_port;
    int timeout_ms;
    bool is_open;
    size_t reconnect_count;

    bool operator==(py_srv_connection const &other)
      const { // to be able to expose a vector of py_srv_connection to python with indexing capabilities (making it
              // appear as a list) comparison operator is required
      return host_port == other.host_port && timeout_ms == other.timeout_ms && is_open == other.is_open
          && reconnect_count == other.reconnect_count;
    }

    bool operator!=(py_srv_connection const &other) const {
      return !operator==(other);
    }
  };

  static std::atomic_size_t py_client_count{
    0}; // just to help python get an indicator of number of connections pr. process

  // need to wrap core client to unlock gil during processing
  struct py_client {
    mutex mx; ///< to enforce just one thread active on this client object at a time
    client impl;

    py_client(std::string const &host_port, bool ac, int timeout_ms)
      : impl(host_port, ac, timeout_ms) {
      ++py_client_count; // add active clients
    }

    py_client(vector<string> const &host_ports, bool ac, int timeout_ms)
      : impl(host_ports, ac, timeout_ms) {
      ++py_client_count; // add active clients
    }

    ~py_client() {
      --py_client_count;
    }

    py_client(py_client const &) = delete;
    py_client(py_client &&) = delete;
    py_client &operator=(py_client const &o) = delete;

    static size_t get_client_count() {
      return py_client_count.load();
    }

    bool get_auto_connect() const {
      return impl.auto_connect;
    }

    bool get_compress_expressions() const {
      return impl.compress_expressions;
    }

    void set_compress_expressions(bool v) {
      impl.compress_expressions = v;
    }

    vector<py_srv_connection> get_connections() const {
      vector<py_srv_connection> result;
      std::transform(impl.srv_con.cbegin(), impl.srv_con.cend(), std::back_inserter(result), [](auto const &c) {
        return py_srv_connection{c.host_port, c.timeout_ms, c.is_open, c.reconnect_count};
      });
      return result;
    }


    void close(int timeout_ms = 1000) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      impl.close(timeout_ms);
    }

    void reopen(int timeout_ms = 1000) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      impl.reopen(timeout_ms);
    }

    auto get_container_names() {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.get_container_names();
    }

    string get_server_version() {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.get_server_version();
    }

    ts_vector_t percentiles(
      ts_vector_t const &tsv,
      utcperiod p,
      gta_t const &ta,
      std::vector<int64_t> const &percentile_spec,
      bool use_ts_cached_read,
      bool update_ts_cache) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.percentiles(tsv, p, ta, percentile_spec, use_ts_cached_read, update_ts_cache);
    }

    ts_vector_t evaluate(
      ts_vector_t const &tsv,
      utcperiod p,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_result) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return ts_vector_t(impl.evaluate(tsv, p, use_ts_cached_read, update_ts_cache, clip_result));
    }

    ts_info_vector_t find(std::string const &search_expression) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.find(search_expression);
    }

    void set_container(
      std::string const &name,
      std::string const &rel_path,
      std::string const &type = "ts_db",
      db_cfg dc = {}) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.set_container(name, rel_path, type, dc);
    }

    void remove_container(std::string const &container_url, bool remove_from_disk) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.remove_container(container_url, remove_from_disk);
    }

    void swap_container(std::string const &a, std::string const &b) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.swap_container(a, b);
    }

    ts_info get_ts_info(std::string const &ts_url) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.get_ts_info(ts_url);
    }

    void store_ts(ts_vector_t const &tsv, bool overwrite_on_write, bool cache_on_write) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      impl.store_ts(tsv, overwrite_on_write, cache_on_write);
    }

    void merge_store_ts(ts_vector_t const &tsv, bool cache_on_write) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      impl.merge_store_ts(tsv, cache_on_write);
    }

    void remove(std::string const &ts_url) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      impl.remove(ts_url);
    }

    void cache_flush() {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.cache_flush();
    }

    cache_stats get_cache_stats() {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.get_cache_stats();
    }

    geo::geo_ts_matrix geo_evaluate(
      string const &geo_ts_db_id,
      vector<string> const &variables,
      vector<int64_t> const &ens,
      gta_t const &ta,
      utctime ts_dt,
      geo::query const &geo_range,
      bool concat,
      utctimespan cc_dt0,
      bool use_cache,
      bool update_cache) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.geo_evaluate(
        geo::eval_args{geo_ts_db_id, variables, ens, ta, ts_dt, geo_range, concat, cc_dt0}, use_cache, update_cache);
    }

    geo::geo_ts_matrix geo_evaluate2(geo::eval_args const &ea, bool use_cache, bool update_cache) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.geo_evaluate(ea, use_cache, update_cache);
    }

    vector<geo::ts_db_config_> get_geo_ts_db_info() {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.get_geo_ts_db_info();
    }

    void geo_store(string const &geo_db_name, geo::ts_matrix const &tsm, bool replace, bool cache) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.geo_store(geo_db_name, tsm, replace, cache);
    }

    void add_geo_ts_db(geo::ts_db_config_ const &gdb) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.add_geo_ts_db(gdb);
    }

    void remove_geo_ts_db(string const &geo_db_name) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.remove_geo_ts_db(geo_db_name);
    }

    vector<string> q_list() {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_list();
    }

    queue::msg_info q_msg_info(string const &q_name, string const &msg_id) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_msg_info(q_name, msg_id);
    }

    vector<queue::msg_info> q_msg_infos(string const &q_name) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_msg_infos(q_name);
    }

    //
    void
      q_put(string const &q_name, string const &msg_id, string const &descript, utctime ttl, ts_vector_t const &tsv) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_put(q_name, msg_id, descript, ttl, tsv);
    }

    queue::tsv_msg_ q_get(string const &q_name, utctime max_wait) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_get(q_name, max_wait);
    }

    void q_ack(string const &q_name, string const &msg_id, string const &diag) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_ack(q_name, msg_id, diag);
    }

    size_t q_size(string const &q_name) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_size(q_name);
    }

    void q_add(string const &q_name) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_add(q_name);
    }

    void q_remove(string const &q_name) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_remove(q_name);
    }

    void q_maintain(string const &q_name, bool keep_ttl_items, bool flush_all) {
      scoped_gil_release gil;
      unique_lock<mutex> lck(mx);
      return impl.q_maintain(q_name, keep_ttl_items, flush_all);
    }
  };
}

namespace expose {
  // using namespace boost::python;
  namespace py = boost::python;
  using shyft::core::utcperiod;
  using shyft::core::utctimespan;
  using shyft::core::utctime;
  using shyft::time_series::ts_point_fx;

  void dtss_finalize() {
#ifdef _MSC_VER
    //FIXME: might be needed for mingw as well
    WSACleanup();
#endif
  }

  static void dtss_messages() {
    py::def(
      "dtss_finalize",
      dtss_finalize,
      "dlib socket and timer cleanup before exit python(automatically called once at module exit)");

    typedef shyft::dtss::ts_info TsInfo;
    py::class_<TsInfo>(
      "TsInfo",
      doc.intro("Gives some information from the backend ts data-store")
        .intro("about the stored time-series, that could be useful in some contexts")(),
      py::init<>(py::arg("self")))
      .def(py::init<std::string, ts_point_fx, utctimespan, std::string, utcperiod, utctime, utctime>(
        (py::arg("self"),
         py::arg("name"),
         py::arg("point_fx"),
         py::arg("delta_t"),
         py::arg("olson_tz_id"),
         py::arg("data_period"),
         py::arg("created"),
         py::arg("modified")),
        doc.intro("construct a TsInfo with all values specified")()))
      .def_readwrite("name", &TsInfo::name, doc.intro("the unique name")())
      .def_readwrite(
        "point_fx",
        &TsInfo::point_fx,
        doc.intro("how to interpret the points, instant value, or average over period")())
      .def_readwrite("delta_t", &TsInfo::delta_t, doc.intro("time-axis steps, in seconds, 0 if irregular time-steps")())
      .def_readwrite(
        "olson_tz_id",
        &TsInfo::olson_tz_id,

        doc.intro("empty or time-axis calendar for calendar,t0,delta_t type time-axis")())
      .def_readwrite("data_period", &TsInfo::data_period, doc.intro("the period for data-stored, if applicable")())
      .def_readwrite("created", &TsInfo::created, doc.intro("when time-series was created, seconds 1970s utc")())
      .def_readwrite("modified", &TsInfo::modified, doc.intro("when time-series was last modified, seconds 1970 utc")())
      .def(py::self == py::self)
      .def(py::self != py::self);

    typedef std::vector<TsInfo> TsInfoVector;
    py::class_<TsInfoVector>(
      "TsInfoVector", doc.intro("A strongly typed list of TsInfo")(), py::init<>(py::arg("self")))
      .def(py::vector_indexing_suite<TsInfoVector>())
      .def(py::init< TsInfoVector const &>((py::arg("self"), py::arg("clone_me"))))
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static void dtss_q_messages() {

    using shyft::dtss::queue::msg_info;
    py::class_<msg_info>(
      "QueueMessageInfo",
      doc.intro("Information about the queue item,")
        .intro("such as the state of the item, in-queue,fetched, done.")
        .intro("This element is never to be created by the python user, but is a return type")
        .intro("from the dtss queue message info related calls.")(),
      py::init<>(py::arg("self")))
      .def_readonly("msg_id", &msg_info::msg_id, doc.intro("str: The unique id for this message in the live-queue")())
      .def_readonly(
        "description",
        &msg_info::description,
        doc.intro("str: A user specified description, we recommend json format")())
      .def_readonly(
        "ttl", &msg_info::ttl, doc.intro("time: Time to live set for this message, used to prune out old messages")())
      .def_readonly("created", &msg_info::created, doc.intro("time: Time when the message was put into the queue")())
      .def_readonly(
        "fetched", &msg_info::fetched, doc.intro("time: Time when the message was fetched from the queue")())
      .def_readonly(
        "done",
        &msg_info::done,
        doc.intro("time: Time when the message acknowledged done from the receiver(end-to-end ack)")())
      .def_readonly(
        "diagnostics",
        &msg_info::diagnostics,
        doc.intro("str: Time when the message acknowledged done from the receiver(end-to-end ack)")())
      .def(py::self == py::self)
      .def(py::self != py::self);

    using msg_infos = std::vector<msg_info>;
    py::class_<msg_infos>(
      "QueueMessageInfoVector",
      doc.intro("A strongly typed list of QueueMessageInfo, as returned by the dtss queue info calls")(),
      py::init<>(py::arg("self")))
      .def(py::vector_indexing_suite<msg_infos>())
      .def(py::init< msg_infos const &>((py::arg("self"), py::arg("clone_me"))))
      .def(py::self == py::self)
      .def(py::self != py::self);
    using shyft::dtss::queue::tsv_msg;
    py::class_<tsv_msg, py::bases<>, std::shared_ptr<tsv_msg>>(
      "QueueMessage",
      doc.intro("A QueueMessage as returned from the DtsClient.q_get(..) consist of the .info part and the payload "
                "time-series vector .tsv")())
      .def_readonly("info", &tsv_msg::info, "The information about the message")
      .def_readonly("tsv", &tsv_msg::tsv, "The time-series vector payload part of the message")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static void dtss_server() {

    using DtsServer = shyft::dtss::py_server;


    py::class_<DtsServer, boost::noncopyable >(
      "DtsServer",
      doc
        .pure("A distributed time-series server.\n\n"
              "The server part of the Shyft Distributed TimeSeries System(DTSS).\n"
              "Capable of processing time-series messages and responding accordingly.\n\n"
              "It has dual service interfaces:\n"
              "    \n"
              "    1. raw-socket boost serialized binary, use ")
        .ref_class("DtsClient")
        .intro(
          "\n"
          "    2. web-api, web-socket(https/wss w. auth supported) using boost.beast, boost.spirit to process/emit "
          "messages. This also supports ts-change subscriptions.\n"
          "\n"
          "**python customization and extension capability**\n"
          "\n"
          "The user can setup callback to python to handle unbound symbolic time-series references, `ts-urls`.\n"
          "This means that you can use your own ts database backend if you have one that can beat the shyft-internal "
          "ts-db.\n"
          "\n"
          "The DtsServer then resolves symbolic references reading time-series from a service or storage for the "
          "specified period.\n"
          "The server object will then compute the resulting time-series vector,\n"
          "and respond back to clients with the results\n"
          "multi-node considerations:\n"
          "    \n"
          "    1. firewall/routing: ensure that the port you are using are open for ip-traffic(use ssh-tunnel if you "
          "need ssl/tls)\n"
          "    2. we strongly recommend using linux for performance and longterm stability\n"
          "\n"
          "The Dts also support master-slave mode, that allows scaling out computations to several Dtss instances, see "
          "set_master_slave_mode\n"
          "\n"
          "**backend storage**\n"
          "\n"
          "There are 3 internal backends, and customization for external storage as well.\n"
          "Internal storage containers:\n"
          "    \n"
          "    1. **rocksdb** - by facebook, configurable specifying `ts_rdb` in the `set_container` method\n"
          "    2. **leveldb** - (deprecated, replaced by rocksdb) by google, configurable specifying `ts_ldb` in the `set_container` method\n"
          "    3. **filedb** - fast zero overhead, and simple internal binary formats, configurable specifying `ts_db` "
          "in the `set_container` method\n"
          "\n"
          "The kind of backend storage for the backing store ts-containers is specifed in the `set_container` method, "
          "for explicit creation of ts-containers.\n"
          "Notice that for remotely client created containers for geo time-series storage, the `default_geo_db_type` "
          "applies, set to `ts_rdb`."
          "**External storage** can be setup by suppling python callbacks for the `find`,`read`,`store` and "
          "`remove_container` hooks.\n"
          "To ensure that containers are (remotely)  found and configured after reboot/restart, ensure to provide\n"
          "a dtss configuration file where this information is stored.\n"
          "Then specifying something else than the `shyft://` pre-fix for the ts-urls, allows any external storage to "
          "be used."
          "\n"
          "**HPC setup, configure linux os user limits**\n"
          "For high performance envirments, the `ulimit`, especially memory, number of files open, needs to be set to\n"
          "higher values than the defaults, usually nofiles is 1024, which is to low for HPC apps.\n"
          "We recommend 4096, or 8192 or even higher\n"
          "for demanding databases.\n"
          "For tuning rocksdb, or leveldb, read tuning guides for those libraries, -we provide some basic parameters "
          "for tuning,\n"
          "but more can be added if needed.\n")
        .see_also("DtsClient")(),
      py::init<>((py::arg("self"))))
      .add_property("configuration_file",
                    &DtsServer::cfg_file,
                    &DtsServer::set_container_cfg,
                    doc.intro("str: configuration file to enable persistent container configurations over coldstarts")())
#if 0
      .def("set_configuration_file",
           ,
           (py::arg("self"),py::arg("configuration_file")),
           doc.intro("Set the persistent storage file for the servers container configuration.")
           .intro("The server will then read and reconfigure, merge if needed that information to the current mappings.")
           .intro("After that the configuration will be keept updated, and it will be restored after cold starts.")
           .parameters()
           .parameter("configuration_file","str","The full path to a writable location where configuration can be persisted")
       ())
#endif
      .def(
        "set_can_remove",
        &DtsServer::set_can_remove,
        (py::arg("self"), py::arg("can_remove")),
        doc.intro("Set whether the DtsServer support removing time-series")
          .intro("The default setting is false, su unless this method is called with true")
          .intro("as argument the server will not allow removing data using `DtsClient.remove`.")
          .parameters()
          .parameter("can_remove", "bool", "``true`` if the server should allow removing data. ``false`` otherwise")())
      .def(
        "set_listening_port",
        &DtsServer::set_listening_port,
        (py::arg("self"), py::arg("port_no")),
        doc.intro("set the listening port for the service")
          .parameters()
          .parameter("port_no", "int", "a valid and available tcp-ip port number to listen on.")
          .paramcont("typically it could be 20000 (avoid using official reserved numbers)")
          .returns("nothing", "None", "")())
      .def(
        "start_async",
        &DtsServer::start_server,
        (py::arg("self")),
        doc.intro("(deprecated, use start_server) start server listening in background, and processing messages")
          .see_also("set_listening_port(port_no),set_listening_ip,is_running,cb,process_messages(msec)")
          .returns(
            "port_no",
            "in",
            "the port used for listening operations, either the value as by set_listening_port, or if it was "
            "unspecified, a new available port")
          .notes()
          .note("you should have setup up the callback, cb before calling start_async")
          .note("Also notice that processing will acquire the GIL\n -so you need to release the GIL to allow for "
                "processing messages")
          .see_also("process_messages(msec)")())
      .def(
        "start_server",
        &DtsServer::start_server,
        (py::arg("self")),
        doc.intro("start server listening in background, and processing messages")
          .see_also("set_listening_port(port_no),set_listening_ip,is_running,cb,process_messages(msec)")
          .returns(
            "port_no",
            "in",
            "the port used for listening operations, either the value as by set_listening_port, or if it was "
            "unspecified, a new available port")
          .notes()
          .note("you should have setup up the callback, cb before calling start_server")
          .note("Also notice that processing will acquire the GIL\n -so you need to release the GIL to allow for "
                "processing messages")
          .see_also("process_messages(msec)")())
      .def(
        "stop_server",
        &DtsServer::stop_server,
        (py::arg("self"), py::arg("timeout") = 1000),
        doc.intro("stop serving connections, gracefully.").see_also("start_server()")())
      .def(
        "set_master_slave_mode",
        &DtsServer::set_master,
        (py::arg("self"),
         py::arg("ip"),
         py::arg("port"),
         py::arg("master_poll_time"),
         py::arg("unsubscribe_threshold"),
         py::arg("unsubscribe_max_delay")),
        doc
          .intro("Set master-slave mode, redirecting all IO calls on this dtss to the master ip:port dtss.\n"
                 "This instance of the dtss is kept in sync with changes done on the master using subscription to "
                 "changes on the master\n"
                 "Calculations, and caches are still done locally unloading the computational efforts from the master.")
          .parameters()
          .parameter("ip", "str", "The ip address where the master dtss is running")
          .parameter("port", "int", "The port number for the master dtss")
          .parameter("master_poll_time", "time", "[s] max time between each update from master, typicall 0.1 s is ok")
          .parameter(
            "unsubscribe_threshold",
            "int",
            "minimum number of unsubscribed time-series before also unsubscribing from the master")
          .parameter("unsubscribe_max_delay", "int", "maximum time to delay unsubscriptions, regardless number")())

      .def(
        "set_max_connections",
        &DtsServer::set_max_connections,
        (py::arg("self"), py::arg("max_connect")),
        doc.intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
          .parameters()
          .parameter("max_connect", "int", "maximum number of connections before denying more connections")
          .see_also("get_max_connections()")())
      .def(
        "get_max_connections",
        &DtsServer::get_max_connections,
        (py::arg("self")),
        doc.intro("returns the maximum number of connections to be served concurrently")())
      .def(
        "clear",
        &DtsServer::clear,
        (py::arg("self")),
        doc.intro("stop serving connections, gracefully.").see_also("cb, process_messages(msec),start_server()")())
      .def(
        "close",
        &DtsServer::clear,
        (py::arg("self")),
        doc.intro("stop serving connections, gracefully.").see_also("cb, process_messages(msec),start_server()")())
      .def(
        "is_running",
        &DtsServer::is_running,
        (py::arg("self")),
        doc.intro("true if server is listening and running").see_also("start_server(),process_messages(msec)")())
      .def(
        "get_listening_port",
        &DtsServer::get_listening_port,
        (py::arg("self")),
        "returns the port number it's listening at for serving incoming request")
      .def(
        "set_listening_ip",
        &DtsServer::set_listening_ip,
        (py::arg("self"), py::arg("ip")),
        doc.intro("Set the ip address to specific interface ip. Must be called prior to the start server method")
          .parameters()
          .parameter("ip", "", "ip address, like 127.0.0.1 for local host only interface")())
      .def(
        "get_listening_ip",
        &DtsServer::get_listening_ip,
        (py::arg("self")),
        doc.intro("Get the current ip listen address")
          .returns("listening ip", "", "note that 0.0.0.0 means listening for all interfaces")())


      .def_readwrite(
        "cb",
        &DtsServer::cb,
        doc
          .intro("Callable[[StringVector,UtcPeriod],TsVector]: callback for binding unresolved time-series references "
                 "to concrete time-series.")
          .intro("Called *if* the incoming messages contains unbound time-series.")
          .intro("The signature of the callback function should be TsVector cb(StringVector,utcperiod)")
          .intro("\nExamples:\n")
          .intro(
            ">>> from shyft import time_series as sa\n\n"
            ">>> def resolve_and_read_ts(ts_ids,read_period):\n"
            ">>>     print('ts_ids:', len(ts_ids), ', read period=', str(read_period))\n"
            ">>>     ta = sa.TimeAxis(read_period.start, sa.deltahours(1), read_period.timespan()//sa.deltahours(1))\n"
            ">>>     x_value = 1.0\n"
            ">>>     r = sa.TsVector()\n"
            ">>>     for ts_id in ts_ids :\n"
            ">>>         r.append(sa.TimeSeries(ta, fill_value = x_value))\n"
            ">>>         x_value = x_value + 1\n"
            ">>>     return r\n"
            ">>> # and then bind the function to the callback\n"
            ">>> dtss=sa.DtsServer()\n"
            ">>> dtss.cb=resolve_and_read_ts\n"
            ">>> dtss.set_listening_port(20000)\n"
            ">>> dtss.process_messages(60000)\n")())
      .def_readwrite(
        "find_cb",
        &DtsServer::fcb,
        doc.intro("Callable[[str],TsInfoVector]: callback for finding time-series using a search-expression.")
          .intro("Called everytime the .find() method is called.")
          .intro("The signature of the callback function should be fcb(search_expr: str)->TsInfoVector")
          .intro("\nExamples:\n")
          .intro(
            ">>> from shyft import time_series as sa\n\n"
            ">>> def find_ts(search_expr: str)->sa.TsInfoVector:\n"
            ">>>     print('find:',search_expr)\n"
            ">>>     r = sa.TsInfoVector()\n"
            ">>>     tsi = sa.TsInfo()\n"
            ">>>     tsi.name = 'some_test'\n"
            ">>>     r.append(tsi)\n"
            ">>>     return r\n"
            ">>> # and then bind the function to the callback\n"
            ">>> dtss=sa.DtsServer()\n"
            ">>> dtss.find_cb=find_ts\n"
            ">>> dtss.set_listening_port(20000)\n"
            ">>> # more code to invoce .find etc.\n")())
      .def_readwrite(
        "store_ts_cb",
        &DtsServer::scb,
        doc.intro("Callable[[TsVector],None]: callback for storing time-series.")
          .intro("Called everytime the .store_ts() method is called and non-shyft urls are passed.")
          .intro("The signature of the callback function should be scb(tsv: TsVector)->None")
          .intro("\nExamples:\n")
          .intro(
            ">>> from shyft import time_series as sa\n\n"
            ">>> def store_ts(tsv:sa.TsVector)->None:\n"
            ">>>     print('store:',len(tsv))\n"
            ">>>     # each member is a bound ref_ts with an url\n"
            ">>>     # extract the url, decode and store\n"
            ">>>     #\n"
            ">>>     #\n"
            ">>>     return\n"
            ">>> # and then bind the function to the callback\n"
            ">>> dtss=sa.DtsServer()\n"
            ">>> dtss.store_ts_cb=store_ts\n"
            ">>> dtss.set_listening_port(20000)\n"
            ">>> # more code to invoce .store_ts etc.\n")())
      .def_readwrite(
        "remove_container_cb",
        &DtsServer::rcb,
        doc.intro("Callable[[str, bool],None]: callback for removing external containers.")
          .intro("Called when the .remove_container() method is called with a non-shyft container url.")
          .intro("The signature of the callback function should be rcb(container_url: string, remove_from_disk: "
                 "bool)->None")())
      .def_readwrite(
        "geo_ts_read_cb",
        &DtsServer::grcb,
        doc.intro("Callable[[GeoTimeSeriesConfiguration,GeoSlice],GeoMatrix]: Callback for reading  geo_ts db.")
          .intro("Called everytime there is a need for geo_ts not stored in cached.")
          .intro("The signature of the callback function should be grcb(cfg:GeoTimeSeriesConfiguration, "
                 "slice:GeoSlice)->GeoMatrix")())
      .def_readwrite(
        "geo_ts_store_cb",
        &DtsServer::gscb,
        doc.intro("Callable[[GeoTimeSeriesConfiguration,GeoMatrix,bool],None]: callback for storing to geo_ts db.")
          .intro("Called everytime the client.store_geo_ts() method is called.")
          .intro("The signature of the callback function should be gscb(cfg:GeoTimeSeriesConfiguration, tsm:GeoMatrix, "
                 "replace:bool)->None")())

      .def("fire_cb", &DtsServer::fire_cb, (py::arg("self"), py::arg("msg"), py::arg("rp")), "testing fire cb from c++")
      .def(
        "process_messages",
        &DtsServer::process_messages,
        (py::arg("self"), py::arg("msec")),
        doc.intro("wait and process messages for specified number of msec before returning")
          .intro("the dtss-server is started if not already running")
          .parameters()
          .parameter("msec", "int", "number of millisecond to process messages")
          .notes()
          .note("this method releases GIL so that callbacks are not blocked when the\n"
                "dtss-threads perform the callback ")
          .see_also("cb,start_server(),is_running,clear()")())
      .def(
        "set_container",
        &DtsServer::add_container,
        (py::arg("self"),
         py::arg("name"),
         py::arg("root_dir"),
         py::arg("container_type") = std::string{},
         py::arg("cfg") = shyft::dtss::db_cfg{}),
        doc.intro("set ( or replaces) an internal shyft store container to the dtss-server.")
          .intro("All ts-urls with shyft://<container>/ will resolve")
          .intro("to this internal time-series storage for find/read/store operations")
          .parameters()
          .parameter("name", "str", "Name of the container as pr. url definition above")
          .parameter("root_dir", "str", "A valid directory root for the container")
          .parameter(
            "container_type",
            "str",
            "one of ('ts_rdb', 'ts_ldb','ts_db','krls'), container type to add.") // TODO: document properly
          .notes()
          .note("currently this call should only be used when the server is not processing messages, "
                "- before starting, or after stopping listening operations\n")()

          )
      .def(
        "remove_container",
        &DtsServer::remove_container,
        (py::arg("self"), py::arg("container_url"), py::arg("delete_from_disk") = false),
        doc.intro("remove an internal shyft store container or an external container from the dtss-server.")
          .intro("container_url on the form shyft://<container>/ will remove internal containers")
          .intro("all other urls with be forwarded to the remove_external_cb callback on the server")
          .intro("removal of containers can take a long time to finish")
          .parameters()
          .parameter("container_url", "str", "url of the container as pr. url definition above")
          .parameter("delete_from_disk", "bool", "Flag to indicate if the container should be deleted from disk")())
      .def(
        "swap_container",
        &DtsServer::swap_container,
        (py::arg("self"), py::arg("container_name_a"), py::arg("container_name_b") = false),
        doc.intro("Swap the backend storage for container `a` and `b`.")
          .intro("The content of a and b should be equal prior to the call to ensure wanted semantics,")
          .intro(" as well as cache correctness.")
          .intro("This is the case if `a` is immutable, and copied to `b` prior to the operation.")
          .intro("If `a` is not permanently immutable, it has to be ensured at least for")
          .intro("the time where the copy/swap operation is done.")
          .intro("The intended purpose is to support migration and moving ts-db backends.")
          .intro("When swap is done, the remove_container can be used for the container that is redunant.")
          .intro("A typical operation is copy `a`->`a_tmp`, then swap(`a`,`a_tmp`), then remove(`shyft://a_tmp`,True)")
          .parameters()
          .parameter("container_name_a", "str", "Name of container a")
          .parameter("container_name_b", "str", "Name of container b")())
      .def(
        "set_geo_ts_db",
        &DtsServer::add_geo_ts_db,
        (py::arg("self"), py::arg("geo_ts_cfg")),
        doc.intro("This add/replace a geo-ts database to the server, so that geo-related requests")
          .intro("can be resolved by means of this configuation and the geo-related callbacks.")
          .parameters()
          .parameter("geo_ts_cfg", "GeoTimeseriesConfiguration", "The configuration for the new geo-ts data-base")())
      .def(
        "set_auto_cache",
        &DtsServer::set_auto_cache,
        (py::arg("self"), py::arg("active")),
        doc.intro("set auto caching all reads active or passive.")
          .intro("Default is off, and caching must be done through")
          .intro("explicit calls to .cache(ts_ids,ts_vector)")
          .parameters()
          .parameter("active", "bool", "if set True, all reads will be put into cache")())
      .def(
        "cache",
        &DtsServer::add_to_cache,
        (py::arg("self"), py::arg("ts_ids"), py::arg("ts_vector")),
        doc.intro("add/update specified ts_ids with corresponding ts to cache")
          .intro("please notice that there is no validation of the tds_ids, they")
          .intro("are threated identifiers,not verified against any existing containers etc.")
          .intro("Requests that follows, will use the cached item as long as it satisfies")
          .intro("the identifier and the coverage period requested")
          .parameters()
          .parameter("ts_ids", "StringVector", "a list of time-series ids")
          .parameter("ts_vector", "TsVector", "a list of corresponding time-series")())
      .def(
        "flush_cache",
        &DtsServer::remove_from_cache,
        (py::arg("self"), py::arg("ts_ids")),
        doc.intro("flushes the *specified* ts_ids from cache")
          .intro("Has only effect for ts-ids that are in cache, non-existing items are ignored")
          .parameters()
          .parameter("ts_ids", "StringVector", "a list of time-series ids to flush out")())
      .def(
        "flush_cache_all",
        &DtsServer::flush_cache,
        (py::arg("self")),
        doc.intro("flushes all items out of cache (cache_stats remain un-touched)")())
      .add_property("cache_stats", &DtsServer::get_cache_stats, doc.intro("CacheStats: the current cache statistics")())
      .def(
        "clear_cache_stats",
        &DtsServer::clear_cache_stats,
        (py::arg("self")),
        doc.intro("clear accumulated cache_stats")())
      .add_property(
        "cache_max_items",
        &DtsServer::get_cache_size,
        &DtsServer::set_cache_size,
        doc.intro("int: cache_max_items is the maximum number of time-series identities that are")
          .intro("kept in memory. Elements exceeding this capacity is elided using the least-recently-used")
          .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
          .intro("time-series from cache in the least recently used order.")())
      .add_property(
        "cache_ts_initial_size_estimate",
        &DtsServer::get_ts_size,
        &DtsServer::set_ts_size,
        doc.intro("int: The initial time-series size estimate in bytes for the cache mechanism.")
          .intro("memory-target = cache_ts_initial_size_estimate * cache_max_items")
          .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
          .intro("time-series from cache in the least recently used order.")())
      .add_property(
        "cache_memory_target",
        &DtsServer::get_cache_memory_target_size,
        &DtsServer::set_cache_memory_target_size,
        doc.intro("int: The memory max target in number of bytes.")
          .intro("If not set directly the following equation is  use:")
          .intro("cache_memory_target = cache_ts_initial_size_estimate * cache_max_items")
          .intro("When setting the target directly, number of items in the chache is ")
          .intro("set so that real memory usage is less than the specified target.")
          .intro("The setter could cause elements to be flushed out of cache.")())


      .add_property(
        "graceful_close_timeout_ms",
        &DtsServer::get_graceful_close_timeout,
        &DtsServer::set_graceful_close_timeout,
        doc.intro("int: how long to let a connection linger after message is processed to allow for")
          .intro("flushing out reply to client.")
          .intro("Ref to dlib.net dlib.net/dlib/server/server_kernel_abstract.h.html ")())
      .def(
        "start_web_api",
        &DtsServer::start_web_api,
        (py::arg("self"),
         py::arg("host_ip"),
         py::arg("port"),
         py::arg("doc_root"),
         py::arg("fg_threads") = 2,
         py::arg("bg_threads") = 4,
         py::arg("tls_only") = false),
        doc.intro("starts the dtss web-api on the specified host_ip, port, doc_root and number of threads")
          .parameters()
          .parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only etc.")
          .parameter("port", "int", "port number to serve the web_api on, ensure it's available!")
          .parameter("doc_root", "str", "directory from which we will serve http/https documents, like index.html etc.")
          .parameter("fg_threads", "int", "number of web-api foreground threads, typical 1-4 depending on load")
          .parameter(
            "bg_threads", "int", "number of long running  background threads workers to serve dtss-request etc.")
          .parameter("tls_only", "bool", "default false, set to true to enforce tls sessions only.")())
      .def(
        "stop_web_api", &DtsServer::stop_web_api, (py::arg("self")), doc.intro("Stops any ongoing web-api service")())
      .add_property(
        "alive_connections",
        &DtsServer::get_alive_connections,
        doc.intro("int: returns currently alive connections to the server")())
      .add_property(
        "auth_needed",
        &DtsServer::auth_needed,
        doc.intro("bool: returns true if the server is setup with auth-tokens, requires web-api clients to pass a "
                  "valid token")())
      .def(
        "auth_tokens",
        &DtsServer::auth_tokens,
        (py::arg("self")),
        doc.intro("returns the registered authentication tokens.")())
      .def(
        "add_auth_tokens",
        &DtsServer::add_auth_tokens,
        (py::arg("self"), py::arg("tokens")),
        doc.intro("Adds auth tokens, and activate authentication.")
          .intro("The tokens is compared exactly to the autorization token passed in the request.")
          .intro("Authorization should onlye be used for the https/wss, unless other measures(vpn/ssh tunnels etc.) "
                 "are used to protect auth tokens on the wire")
          .intro("Important! Ensure to start_web_api with tls_only=True when using auth!")
          .parameters()
          .parameter(
            "tokens", "", "list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")())
      .def(
        "remove_auth_tokens",
        &DtsServer::remove_auth_tokens,
        (py::arg("self"), py::arg("tokens")),
        doc
          .intro("removes auth tokens, if it matches all available tokens, then deactivate auth requirement for "
                 "clients")
          .parameters()
          .parameter(
            "tokens", "", "list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")())
      .def_readwrite(
        "default_geo_db_config",
        &DtsServer::default_geo_db_cfg,
        doc.intro("GeoTimeSeriesConfiguration: Default parameters for geo db created by clients")())
      .def_readwrite(
        "default_geo_db_type",
        &DtsServer::default_geo_db_type,
        doc.intro("str: default container type for geo db created by clients,(`ts_rdb`,`ts_ldb`,`ts_db`), defaults set "
                  "to `ts_rdb`")())
      .def(
        "get_geo_db_ts_info",
        &DtsServer::do_get_geo_info,
        (py::arg("self")),
        doc.intro("Returns the configured geo-ts data-bases on the server, so queries can be specified and formulated")
          .returns("r", "GeoTimeseriesConfigurationVector", "A strongly typed list of GeoTimeseriesConfiguration")
          .see_also(".geo_evaluate()")())
      .def(
        "get_container_names",
        &DtsServer::do_get_container_names,
        (py::arg("self")),
        doc.intro("Return a list of the names of containers available on the server")())

      ;
  }

  static void dtss_client() {

    typedef shyft::dtss::py_srv_connection DtsConnection;
    py::class_<DtsConnection, boost::noncopyable>("DtsConnection", "", py::no_init)
      .def_readonly(
        "host_port", &DtsConnection::host_port, "str: Endpoint network address of the remote server, like `host:port`.")
      .def_readonly(
        "timeout_ms", &DtsConnection::timeout_ms, "int: Timout for remote server operations, in number milliseconds.")
      .def_readonly("is_open", &DtsConnection::is_open, "bool: If the connection to the remote server is (still) open.")
      .def_readonly(
        "reconnect_count",
        &DtsConnection::reconnect_count,
        "int: Number of reconnects to the remote server that have been performed.");

    typedef std::vector<DtsConnection> DtsConnectionVector;
    py::class_<DtsConnectionVector>(
      "DtsConnectionVector", doc.intro("A strongly typed list of DtsConnection")(), py::init<>(py::arg("self")))
      .def(py::vector_indexing_suite<DtsConnectionVector>())
      //.def(py::init<const DtsConnectionVector&>((py::arg("self"),py::arg("clone_me")))) // don't have to be clonable
      // since it's only exposed read-only to python
      .def(py::self == py::self)
      .def(py::self != py::self);

    typedef shyft::dtss::py_client DtsClient;
    py::class_<DtsClient, boost::noncopyable>(
      "DtsClient",
      doc.intro("The client side part of the distributed time series system(DTSS).")
        .intro("")
        .pure("The DtsClient communicate with the DtsServer using an efficient raw socket\n"
              "protocol using boost binary serialization. A typical operation would be that\n"
              "the DtsClient forwards TsVector that represents lists and structures of\n"
              "time-series expressions) to the DtsServer(s), that takes care of binding\n"
              "unbound symbolic time-series, evaluate and return the results back to the DtsClient.\n"
              "This class is closely related to the")
        .ref_class("DtsServer")
        .pure("and useful reference\n"
              "is also")
        .ref_class("TsVector")
        .intro(".\n\n")
        .pure("Best practice for client/server is to use cache following two simple rules(the default):\n    \n"
              "    1. Always caching writes (because then consumers get it fresh and fast).\n"
              "    2. Always use caching reads(utilize and maintain the adaptive cache).\n\n"
              "The only two known very rare and special scenarios where *uncached writes* can be useful\n"
              "are when loading large initial content of time-series db.\n"
              "Another special scenario, where caching reads should be turned off is when using the\n"
              "3rd party dtss backend extension, where the 3rd party db is written/modified\n"
              "*outside the control of dtss*\n"
              "Also note that the caching works with the ts-terminals, not the result of the expressions.\n"
              "When reading time-series expressions, such as `ts = ts1 - ts2`, implementation of the cache is such\n"
              "that it contains the ts-terminals (here, `ts1` and `ts2`), not the expression itself (`ts`).\n"
              "The `.cache_stats`, provides cache statistics for the server.\n"
              "The cache can be flushed, useful for some special cases of loading data outside cache.")(),
      py::no_init)
      .def(py::init< std::string const &, bool, int>(
        (py::arg("self"), py::arg("host_port"), py::arg("auto_connect") = true, py::arg("timeout_ms") = 1000),
        doc.intro("Constructs a dts-client with the specifed host_port parameter.")
          .intro("A connection is immediately done to the server at specified port.")
          .intro("If no such connection can be made, it raises a RuntimeError.")
          .parameter("host_port", "string", "a string of the format 'host:portnumber', e.g. 'localhost:20000'")
          .parameter(
            "auto_connect",
            "bool",
            "default True, connection pr. call. if false, connection last lifetime of object unless explicitely "
            "closed/reopened")
          .parameter("timeout_ms", "int", "defalt 1000ms, used for timeout connect/reconnect/close operations")()))
      .def(py::init< std::vector<std::string> const &, bool, int>(
        (py::arg("self"), py::arg("host_ports"), py::arg("auto_connect"), py::arg("timeout_ms")),
        doc.intro("Constructs a dts-client with the specifed host_ports parameters.")
          .intro("A connection is immediately done to the server at specified port.")
          .intro("If no such connection can be made, it raises a RuntimeError.")
          .intro("If several servers are passed, the .evaluate and .percentile function will partition the ts-vector "
                 "between the")
          .intro("provided servers and scale out the computation")
          .parameter(
            "host_ports", "StringVector", "a a list of string of the format 'host:portnumber', e.g. 'localhost:20000'")
          .parameter(
            "auto_connect",
            "bool",
            "default True, connection pr. call. if false, connection last lifetime of object unless explicitely "
            "closed/reopened")
          .parameter("timeout_ms", "int", "defalt 1000ms, used for timeout connect/reconnect/close operations")()))
      .add_static_property(
        "total_clients", &DtsClient::get_client_count) // docstring not supported for add_static_property
      .def_readonly(
        "auto_connect",
        &DtsClient::get_auto_connect,
        "bool: If connections are made as needed, and kept short, otherwise externally managed.")
      .add_property(
        "compress_expressions",
        &DtsClient::get_compress_expressions,
        &DtsClient::set_compress_expressions,
        doc.intro("bool: If True, the expressions are compressed before sending to the server.")
          .intro("For expressions of any size, like 100 elements, with expression")
          .intro("depth 100 (e.g. nested sums), this can speed up")
          .intro("the transmission by a factor or 3.")())
      .def_readonly("connections", &DtsClient::get_connections, doc.intro("int: Get remote server connections.")())

      .def(
        "close",
        &DtsClient::close,
        (py::arg("self"), py::arg("timeout_ms") = 1000),
        doc.intro("Close the connection. If auto_connect is enabled it will automatically reopen if needed.")())
      .def(
        "reopen",
        &DtsClient::reopen,
        (py::arg("self"), py::arg("timeout_ms") = 1000),
        doc.intro("(Re)open a connection after close or server restart.")())
      .def(
        "get_server_version",
        &DtsClient::get_server_version,
        doc.intro("Returns the server version major.minor.patch string, if multiple servers, the version of the first "
                  "is returned")())
      .def(
        "percentiles",
        &DtsClient::percentiles,
        (py::arg("self"),
         py::arg("ts_vector"),
         py::arg("utcperiod"),
         py::arg("time_axis"),
         py::arg("percentile_list"),
         py::arg("use_ts_cached_read") = true,
         py::arg("update_ts_cache") = true),
        doc.intro("Evaluates the expressions in the ts_vector for the specified utcperiod.")
          .intro("If the expression includes unbound symbolic references to time-series,")
          .intro("these time-series will be passed to the binding service callback")
          .intro("on the serverside.")
          .parameters()
          .parameter(
            "ts_vector", "TsVector", "a list of time-series (expressions), including unresolved symbolic references")
          .parameter(
            "utcperiod",
            "UtcPeriod",
            "the valid non-zero length period that the binding service should read from the backing "
            "ts-store/ts-service")
          .parameter("time_axis", "TimeAxis", "the time_axis for the percentiles, e.g. a weekly time_axis")
          .parameter(
            "percentile_list", "IntVector", "a list of percentiles, where -1 means true average, 25=25percentile etc")
          .parameter("use_ts_cached_read", "bool", "utilize server-side cached results")
          .parameter("update_ts_cache", "bool", "when reading time-series, also update the server-side cache")
          .returns(
            "tsvector",
            "TsVector",
            "an evaluated list of percentile time-series in the same order as the percentile input list")
          .see_also(".evaluate(), DtsServer")())
      .def(
        "evaluate",
        &DtsClient::evaluate,
        (py::arg("self"),
         py::arg("ts_vector"),
         py::arg("utcperiod"),
         py::arg("use_ts_cached_read") = true,
         py::arg("update_ts_cache") = true,
         py::arg("clip_result") = utcperiod{}),
        doc.intro("Evaluates the expressions in the ts_vector.")
          .intro("If the expression includes unbound symbolic references to time-series,")
          .intro("these time-series will be passed to the binding service callback")
          .intro("on the serverside, passing on the specifed utcperiod.")
          .intro("")
          .intro("NOTE: That the ts-backing-store, either cached or by read, will return data for:")
          .intro("    * at least the period needed to evaluate the utcperiod")
          .intro("    * In case of cached result, this will currently involve the entire matching cached time-series "
                 "segment.")
          .intro("In particular, this means that the returned result **could be larger** than the specified utcperiod, "
                 "unless you specify `clip_result`")
          .intro("      Other available methods, such as the expression (x.average(ta)), including time-axis,")
          .intro("      can be used to exactly control the returned result size.")
          .intro("      Also note that the semantics of utcperiod is ")
          .intro("      to ensure that enough data is read from the backend, so that it can evaluate the expressions.")
          .intro("      Use clip_result argument to clip the time-range of the resulting time-series to fit your need "
                 "if needed")
          .intro("      - this will typically be in scenarios where you have not supplied time-axis operations "
                 "(unbounded eval),")
          .intro("      and you also are using caching.")
          .intro("      ")
          .intro("\nSee also")
          .ref_meth("DtsClient.percentiles")
          .intro("if you want to evaluate percentiles of an expression.")

          .parameters()
          .parameter(
            "ts_vector", "TsVector", "a list of time-series (expressions), including unresolved symbolic references")
          .parameter(
            "utcperiod",
            "UtcPeriod",
            "the valid non-zero length period that the binding service should read from the backing "
            "ts-store/ts-service")
          .parameter("use_ts_cached_read", "bool", "use of server-side ts-cache")
          .parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data")
          .parameter(
            "clip_result",
            "UtcPeriod",
            "If supplied, clip the time-range of the resulting time-series to cover evaluation f(t) over this period "
            "only")
          .returns("tsvector", "TsVector", "an evaluated list of point time-series in the same order as the input list")
          .see_also("DtsServer")())
      .def(
        "set_container",
        &DtsClient::set_container,
        (py::arg("self"),
         py::arg("name"),
         py::arg("relative_path"),
         py::arg("container_type") = std::string{"ts_db"},
         py::arg("cfg") = shyft::dtss::db_cfg{}),
        doc.intro("create an internal shyft store container to the dtss-server with a root relative path.")
          .intro("All ts-urls with shyft://<container>/ will resolve")
          .intro("to this internal time-series storage for find/read/store operations")
          .intro("will not replace existing containers that have the same name")
          .parameters()
          .parameter("name", "str", "Name of the container as pr. url definition above")
          .parameter(
            "relative_path", "str", "A valid directory for the container relative to the root path of the server.")
          .parameter("container_type", "str", "one of ('ts_rdb', 'ts_ldb','ts_db'), container type to add.")())
      .def(
        "remove_container",
        &DtsClient::remove_container,
        (py::arg("self"), py::arg("container_url"), py::arg("delete_from_disk") = false),
        doc.intro("remove an internal shyft store container or an external container from the dtss-server.")
          .intro("container_url on the form shyft://<container>/ will remove internal containers")
          .intro("all other urls with be forwarded to the remove_external_cb callback on the server")
          .intro("removal of containers can take a long time to finish")
          .parameters()
          .parameter("container_url", "str", "url of the container as pr. url definition above")
          .parameter("delete_from_disk", "bool", "Flag to indicate if the container should be deleted from disk")())
      .def(
        "swap_container",
        &DtsClient::swap_container,
        (py::arg("self"), py::arg("container_name_a"), py::arg("container_name_b") = false),
        doc.intro("Swap the backend storage for container `a` and `b`.")
          .intro("The content of a and b should be equal prior to the call to ensure wanted semantics,")
          .intro(" as well as cache correctness.")
          .intro("This is the case if `a` is immutable, and copied to `b` prior to the operation.")
          .intro("If `a` is not permanently immutable, it has to be ensured at least for")
          .intro("the time where the copy/swap operation is done.")
          .intro("The intended purpose is to support migration and moving ts-db backends.")
          .intro("When swap is done, the remove_container can be used for the container that is redunant.")
          .intro("A typical operation is copy `a`->`a_tmp`, then swap(`a`,`a_tmp`), then remove(`shyft://a_tmp`,True)")
          .parameters()
          .parameter("container_name_a", "str", "Name of container a")
          .parameter("container_name_b", "str", "Name of container b")())
      .def(
        "find",
        &DtsClient::find,
        (py::arg("self"), py::arg("search_expression")),
        doc.intro("Find ts information that fully matches the regular search-expression.")
          .intro("For the shyft file based backend, take care to specify path elements precisely,")
          .intro("so that the directories visited is minimised.")
          .intro("e.g:`a\\/.*\\/my\\.ts`")
          .intro("Will prune out  any top level directory not starting with `a`,")
          .intro("but will match any subdirectories below that level.")
          .intro("Refer to python test-suites for a wide range of examples using find.")
          .intro("Notice that the regexp search algoritm uses ignore case.")
          .intro("Please be aware that custom backend by python extension might have different rules.")
          .parameters()
          .parameter(
            "search_expression", "str", "regular search-expression, to be interpreted by the back-end tss server")
          .returns("ts_info_vector", "TsInfoVector", "The search result, as vector of TsInfo objects")
          .see_also("TsInfo,TsInfoVector")())
      .def(
        "get_ts_info",
        &DtsClient::get_ts_info,
        (py::arg("self"), py::arg("ts_url")),
        doc.intro("Get ts information for a time-series from the backend")
          .parameters()
          .parameter("ts_url", "str", "Time-series url to lookup ts info for")
          .returns("ts_info", "TsInfo", "A TsInfo object")
          .see_also("TsInfo")())
      .def(
        "store_ts",
        &DtsClient::store_ts,
        (py::arg("self"), py::arg("tsv"), py::arg("overwrite_on_write") = false, py::arg("cache_on_write") = true),
        doc.intro("Store the time-series in the ts-vector in the dtss backend.")
          .pure(
            "Stores the time-series fragments data passed to the backend.\n"
            "It is semantically stored as if \n\n"
            "first erasing the existing stored points in the range of ts.time_axis().total_period()\n\n"
            "then inserting the points of the ts.\n\n"
            "Thus, only modifying the parts of time-series that are covered by the ts-fragment passed.\n"
            "If there is no previously existing time-series, its merely stores the ts-fragment as the initial\n"
            "content and definition of the time-series.\n\n"
            "When creating time-series 1st time, pay attention to the time-axis, and point-interpretation as this\n"
            "remains the properties of the newly created time-series.\n"
            "Storing 15min data to a time-series defined initially as hourly series will raise exception.\n"
            "On the otherhand, the variable interval time-series are generic and will accept any\n"
            "time-resolution to be stored\n\n"
            "The `overwrite_on_write` = True,  is used to *replace* the entire definition of any previously stored "
            "time-series.\n"
            "This is semantically as if erasing the previously stored time-series and replacing its entire content\n"
            "and definition, starting fresh with the newly passed time-series.\n"
            "The time-series should be created like this, with url and a concrete point-ts:\n"
            "\n"
            ">>>   a=sa.TimeSeries(ts_url,ts_points)\n"
            ">>>   tsv.append(a)\n")
          .parameters()
          .parameter(
            "tsv", "TsVector", "ts-vector with time-series, url-reference and values to be stored at dtss server")
          .parameter(
            "overwrite_on_write",
            "bool",
            "When True the backend replaces the entire content and definition of any existing time-series with the "
            "passed time-series")
          .parameter(
            "cache_on_write",
            "bool",
            "defaults True, if set to False, the cache is not updated, and should only be considered used in very "
            "special use-cases.")
          .returns("None", "", "")
          .see_also("TsVector")())
      .def(
        "merge_store_ts_points",
        &DtsClient::merge_store_ts,
        (py::arg("self"), py::arg("tsv"), py::arg("cache_on_write") = true),
        doc.intro("Merge the ts-points supplied in the tsv into the existing time-series on the server side.")
          .intro("The effect of each ts is similar to **as if**:")
          .intro("")
          .intro("    1. **read** ts.total_period() from ts point store")
          .intro("    2. **in memory appy** the TimeSeries.merge_points(ts) on the read-ts")
          .intro("    3. **write** the resulting merge-result back to the ts-store")
          .intro("")
          .intro("This function is suitable for typical data-collection tasks")
          .intro("where the points collected is from an external source, appears as batches,")
          .intro("that should just be added to the existing point-set")
          .parameters()
          .parameter(
            "tsv", "TsVector", "ts-vector with time-series, url-reference and values to be stored at dtss server")
          .parameter(
            "cache_on_write",
            "bool",
            "updates the cache with the result of the merge operation, if set to False, this is skipped, notice that "
            "this is only useful for very special use-cases.")
          .returns("None", "", "")
          .see_also("TsVector")())
      .def(
        "remove",
        &DtsClient::remove,
        py::arg("ts_url"),
        doc.intro("Remove a time-series from the dtss backend")
          .intro("The time-series referenced by ``ts_url`` is removed from the backend DtsServer.")
          .intro("Note that the DtsServer may prohibit removing time-series.")
          .parameters()
          .parameter("ts_url", "str", "shyft url referencing a time series")())
      .def(
        "cache_flush",
        &DtsClient::cache_flush,
        (py::arg("self")),
        doc.intro("Flush the cache (including statistics) on the server. This can be useful in scenario when "
                  "cache_on_write=False in the store operations.")())
      .add_property(
        "cache_stats",
        &DtsClient::get_cache_stats,
        doc.intro("CacheStats: Get the cache_stats (including statistics) on the server.")())
      .def(
        "geo_evaluate",
        &DtsClient::geo_evaluate,
        (py::arg("self"),
         py::arg("geo_ts_db_name"),
         py::arg("variables"),
         py::arg("ensembles"),
         py::arg("time_axis"),
         py::arg("ts_dt"),
         py::arg("geo_range"),
         py::arg("concat"),
         py::arg("cc_dt0"),
         py::arg("use_cache") = true,
         py::arg("update_cache") = true),
        doc.intro("Evaluates a geo-temporal query on the server, and return the results")
          .parameters()
          .parameter("geo_ts_db_name", "string", "The name of the geo_ts_db, e.g. arome, ec, arome_cc ec_cc etc.")
          .parameter(
            "variables",
            "StringVector",
            "list  of variables, like 'temperature','precipitation'. If empty, return data for all available variables")
          .parameter("ensembles", "IntVector", "list of ensembles to read, if empty return all available")
          .parameter(
            "time_axis",
            "TimeAxis",
            "return geo_ts where t0 matches time-points of this time-axis. If concat, the ta.total_period().end "
            "determines how long to extend latest forecast")
          .parameter("ts_dt", "time", "specifies the length of the time-slice to read from each time-series")
          .parameter("geo_range", "GeoQuery", "Specify polygon to include, empty means all")
          .parameter(
            "concat",
            "bool",
            "If true, the geo_ts for each ensemble/point is joined together to form one singe time-series, "
            "concatenating a slice from each of the forecasts")
          .parameter(
            "cc_dt0",
            "time",
            "concat delta time to skip from beginning of each geo_ts, so you can specify 3h, then select +3h.. "
            "slice-end from each forecast")
          .parameter("use_cache", "bool", "use cache if available(speedup)")
          .parameter(
            "update_cache", "bool", "if reading data from backend, also stash it to the cache for faster evaluations")
          .returns(
            "r",
            "GeoMatrix",
            "A matrix where the elements are GeoTimeSeries, accessible using indicies time,variable, ensemble, t0")
          .see_also(".get_geo_ts_db_info()")())
      .def(
        "geo_evaluate",
        &DtsClient::geo_evaluate2,
        (py::arg("self"), py::arg("eval_args"), py::arg("use_cache") = true, py::arg("update_cache") = true),
        doc.intro("Evaluates a geo-temporal query on the server, and return the results")
          .parameters()
          .parameter(
            "eval_args",
            "GeoEvalArgs",
            "complete set of arguments for geo-evaluation, including geo-db, scope for variables, ensembles, time and "
            "geo-range")
          .parameter("use_cache", "bool", "use cache if available(speedup)")
          .parameter(
            "update_cache", "bool", "if reading data from backend, also stash it to the cache for faster evaluations")
          .returns(
            "r",
            "GeoMatrix",
            "A matrix where the elements are GeoTimeSeries, accessible using indicies time,variable, ensemble, t0")
          .see_also(".get_geo_ts_db_info()")())
      .def(
        "get_geo_db_ts_info",
        &DtsClient::get_geo_ts_db_info,
        (py::arg("self")),
        doc.intro("Returns the configured geo-ts data-bases on the server, so queries can be specified and formulated")
          .returns("r", "GeoTimeseriesConfigurationVector", "A strongly typed list of GeoTimeseriesConfiguration")
          .see_also(".geo_evaluate()")())
      .def(
        "geo_store",
        &DtsClient::geo_store,
        (py::arg("self"), py::arg("geo_ts_db_name"), py::arg("tsm"), py::arg("replace"), py::arg("cache") = true),
        doc.intro("Store a ts-matrix with needed dimensions and data to the specified geo-ts-db")
          .parameters()
          .parameter("geo_ts_db_name", "string", "The name of the geo_ts_db, e.g. arome, ec, arome_cc ec_cc etc.")
          .parameter(
            "tsm",
            "TsMatrix",
            "A dense matrix with dimensionality complete for variables, ensembles and geo-points,flexible "
            "time-dimension 1..n")
          .parameter(
            "replace",
            "bool",
            "Replace existing geo time-series with the new ones, does not extend existing ts, replaces them!")
          .parameter("cache", "bool", "Also put values to the cache")
          .see_also(".get_geo_ts_db_info(),.geo_evaluate")())
      .def(
        "add_geo_ts_db",
        &DtsClient::add_geo_ts_db,
        (py::arg("self"), py::arg("geo_cfg")),
        doc.intro("Adds a new geo time-series database to the dtss-server with the given specifications")
          .parameter(
            "geo_cfg",
            "GeoTimeSeriesConfiguration",
            "the configuration to be added to the server specifying the dimensionality etc.")
          .see_also(".get_geo_db_ts_info()")())
      .def(
        "remove_geo_ts_db",
        &DtsClient::remove_geo_ts_db,
        (py::arg("self"), py::arg("geo_ts_db_name")),
        doc.intro("Remove the specified geo time-series database from dtss-server")
          .parameter("geo_ts_db_name", "string", "the name of the geo-ts-database to be removed")
          .see_also(".get_geo_db_ts_info(),add_geo_ts_db()")())
      .def(
        "get_container_names",
        &DtsClient::get_container_names,
        doc.intro("Return a list of the names of containers available on the server")())
      .def(
        "q_add",
        &DtsClient::q_add,
        (py::arg("self"), py::arg("name")),
        doc.intro("Add a a named queue to the dtss server")
          .parameters()
          .parameter("name", "", "the name of the new queue, required to be unique")())
      .def(
        "q_remove",
        &DtsClient::q_remove,
        (py::arg("self"), py::arg("name")),
        doc.intro("Removes a named queue from dtss server, including all data in flight on the queue")
          .parameters()
          .parameter("name", "", "the name of the queue")())
      .def(
        "q_maintain",
        &DtsClient::q_maintain,
        (py::arg("self"), py::arg("name"), py::arg("keep_ttl_items"), py::arg("flush_all") = false),
        doc.intro("Maintains, removes items that has passed through the queue, and are marked as done.")
          .intro("To flush absolutely all items, pass flush_all=True.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter(
            "keep_ttl_items",
            "",
            "If true, the ttl set for the done messages are respected, and they are not removed until the create+ttl "
            "has expired")
          .parameter("flush_all", "", "removes all items in the queue and kept by the queue, the queue is emptied")())
      .def(
        "q_list",
        &DtsClient::q_list,
        (py::arg("self")),
        doc.intro("returns a list of defined queues on the dtss server")())
      .def(
        "q_msg_infos",
        &DtsClient::q_msg_infos,
        (py::arg("self"), py::arg("name")),
        doc.intro("Returns all message informations from a queue, including not yet pruned fetched/done messages")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .returns("msg_infos", "", "the list of information keept in the named queue")())
      .def(
        "q_msg_info",
        &DtsClient::q_msg_info,
        (py::arg("self"), py::arg("name"), py::arg("msg_id")),
        doc.intro("From the specified queue, fetch info about specified msg_id.")
          .intro("By inspecting the provided information, one can see when the messaage is created, fetched, and done "
                 "with.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the msg_id")
          .returns("msg_info", "", "the information/state of the identified message")())
      .def(
        "q_put",
        &DtsClient::q_put,
        (py::arg("self"), py::arg("name"), py::arg("msg_id"), py::arg("description"), py::arg("ttl"), py::arg("tsv")),
        doc.intro("Put a message, as specified with the supplied parameters, into the specified named queue.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the msg_id, required to be unique within current messages keept by the queue")
          .parameter(
            "description", "", "the freetext description to but along with the message, we recommend json formatted")
          .parameter(
            "ttl",
            "",
            "time-to-live for the message after done, if specified, the q_maintain process can be asked to keep done "
            "messages that have ttl")
          .parameter("tsv", "", "time-series vector, with the wanted payload of time-series")())
      .def(
        "q_get",
        &DtsClient::q_get,
        (py::arg("self"), py::arg("name"), py::arg("max_wait")),
        doc.intro("Get a message out from the named queue, waiting max_wait time for it if it's not already there.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("max_wait", "", "max_time to wait for message to arrive")
          .returns(
            "q_msg",
            "",
            "A queue message consisting of .info describing the message, and the time-series vector .tsv")())
      .def(
        "q_ack",
        &DtsClient::q_ack,
        (py::arg("self"), py::arg("name"), py::arg("msg_id"), py::arg("diagnostics")),
        doc.intro("After q_get, q_ack confirms that the message is ok/handled back to the process that called q_put.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the msg_id, required to be unique within current messages keept by the queue")
          .parameter(
            "diagnostics", "", "the freetext diagnostics to but along with the message, we recommend json formatted")())
      .def(
        "q_size",
        &DtsClient::q_size,
        (py::arg("self"), py::arg("name")),
        doc.intro("Returns number of queue messages waiting to be read by q_get.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .returns("unread count", "", "number of elements queued up")());
  }

  void dtss_cache_stats() {
    using CacheStats = shyft::dtss::cache_stats;
    py::class_<CacheStats>("CacheStats", doc.intro("Cache statistics for the DtsServer."), py::init<>(py::arg("self")))
      .def_readwrite("hits", &CacheStats::hits, doc.intro("int: number of hits by time-series id")())
      .def_readwrite("misses", &CacheStats::misses, doc.intro("int: number of misses by time-series id")())
      .def_readwrite(
        "coverage_misses",
        &CacheStats::coverage_misses,
        doc.intro("int: number of misses where we did find the time-series id, but the period coverage was "
                  "insufficient")())
      .def_readwrite(
        "id_count",
        &CacheStats::id_count,

        doc.intro("int: number of unique time-series identities in cache")())
      .def_readwrite(
        "point_count", &CacheStats::point_count, doc.intro("int: total number of time-series points in the cache")())
      .def_readwrite(
        "fragment_count",
        &CacheStats::fragment_count,
        doc.intro("int: number of time-series fragments in the cache, (greater or equal to id_count)")())
      .def(py::self == py::self)
      .def(py::self == py::self);
  }

  /** @brief Helper function to expose `shyft_url` accepting a Python dictionary with queries.
   */
  std::string shyft_url_fn(std::string const &container, std::string const &ts_path, py::dict const &py_queries) {

    std::map<std::string, std::string> queries{};

    // create a iterator over the list of python objects (actually tuples) in the py::dict
    py::stl_input_iterator<py::object> begin(py_queries.items()), end;
    std::for_each(begin, end, [&queries](py::object const &obj) {
      // extract the two tuple elements into our std::map
      queries[py::extract<std::string>(obj[0])] = py::extract<std::string>(obj[1]);
    });
    return shyft::dtss::shyft_url(container, ts_path, queries);
  }

  inline std::string
    ext_query_url(std::string const &prefix, std::string const &container, std::string const &ts_name) {
    return prefix + container + "?" + shyft::dtss::urlencode(ts_name);
  }

  inline std::string ext_path_url(std::string const &prefix, std::string const &container, std::string const &ts_name) {
    return prefix + container + "/" + shyft::dtss::urlencode(ts_name);
  }

  /** @brief Helper function to expose `extract_shyft_url_query_parameters` returning a Python dictionary with queries.
   */
  py::dict extract_shyft_url_query_parameters_fn(std::string const &url) {
    py::dict py_queries{};
    for (auto const &[key, value] : shyft::dtss::extract_shyft_url_query_parameters(url))
      py_queries[key] = value;
    return py_queries;
  }

  void url_utils() {
    py::def(
      "shyft_url",
      shyft_url_fn,
      (py::arg("container"), py::arg("ts_path"), py::arg("queries") = py::dict{}),
      doc.intro("Construct a Shyft URL from a container, a TS-path, and an optional collection of query flags.")
        .intro("")
        .intro("Query keys and values are always urlencoded. The separating `?`, `&`, and `=` are not encoded.")
        .parameters()
        .parameter("container", "str", "Shyft TS container.")
        .parameter("ts_path", "str", "Time-series path.")
        .parameter(
          "queries", "Dict[str,str]", "Optional mapping from query keys to values. Defaults to an empty dictionary.")
        .returns("url", "str", "Constructed Shyft URL.")
        .see_also("shyft_url, urlencode, extract_shyft_url_container, extract_shyft_url_path, "
                  "extract_shyft_url_query_parameters")());
    py::def(
      "ext_path_url",
      ext_path_url,
      (py::arg("prefix"), py::arg("container"), py::arg("ts_path")),
      doc.intro("Construct a prefix container / urlencode(ts_name) fast")
        .intro("")
        .parameters()
        .parameter("prefix", "str", "like fame:// mydb:// or similar")
        .parameter("container", "str", "TS container.")
        .parameter("ts_path", "str", "Time-series path.")
        .returns("url", "str", "Constructed url with url-encoded ts_path")
        .see_also("shyft_url, ext_query_url, urlencode, extract_shyft_url_container, extract_shyft_url_path, "
                  "extract_shyft_url_query_parameters")());
    py::def(
      "ext_query_url",
      ext_query_url,
      (py::arg("prefix"), py::arg("container"), py::arg("ts_path")),
      doc.intro("Construct a prefix container?urlencode(ts_name) fast")
        .intro("")
        .parameters()
        .parameter("prefix", "str", "like fame:// mydb:// or similar")
        .parameter("container", "str", "TS container.")
        .parameter("ts_path", "str", "Time-series path.")
        .returns("url", "str", "Constructed url with url-encoded ts_path")
        .see_also("shyft_url,ext_path_url, urlencode, extract_shyft_url_container, extract_shyft_url_path, "
                  "extract_shyft_url_query_parameters")());


    py::def(
      "extract_shyft_url_container",
      shyft::dtss::extract_shyft_url_container,
      (py::arg("url")),
      doc.intro("Extract the container part from a Shyft URL.")
        .parameters()
        .parameter("url", "str", "Shyft URL to extract container from.")
        .returns(
          "container",
          "str",
          "Container part of `url`, if the string is invalid as a Shyft URL\n\t"
          "an empty string is retuned instead.")
        .see_also("shyft_url")());
    py::def(
      "extract_shyft_url_path",
      shyft::dtss::extract_shyft_url_path_after_container,
      (py::arg("url")),
      doc.intro("Extract the time-series path part from a Shyft URL.")
        .parameters()
        .parameter("url", "str", "Shyft URL to extract the TS-path from.")
        .returns(
          "ts_path",
          "str",
          "TS-path part of `url`, if the string is invalid as a Shyft URL\n\t"
          "an empty string is retuned instead.")
        .see_also("shyft_url")());
    py::def(
      "extract_shyft_url_query_parameters",
      extract_shyft_url_query_parameters_fn,
      (py::arg("url")),
      doc.intro("Extract query parameters from a Shyft URL.")
        .intro("")
        .intro("The query string is assumed to be on the format `?key1=value1&key2=value2&key3=&key4=value4`.")
        .intro("This will be parsed into a map with four keys: `key1` through `key4`, where `key3` have a")
        .intro("empty string value, while the rest have respectivly values `value1`, `value2`, and `value4`.")
        .intro("")
        .intro("Both query keys and values are assumed to be urlencoded, thus urlencode is called on every")
        .intro("key and every value.")
        .parameters()
        .parameter("url", "str", "Shyft URL to extract the TS-path from.")
        .returns(
          "queries",
          "Dict[str,str]",
          "A dict with all queries defined in `url`. The dictionary is "
          "empty if `url` is invalid as a Shyft URL.")
        .see_also("shyft_url, urldecode")());

    py::def(
      "urlencode",
      shyft::dtss::urlencode,
      (py::arg("text"), py::arg("space_pluss") = true),
      doc.intro("Percent-encode a string for use in URLs.")
        .intro("")
        .intro("All characters designated as reserved in RFC3986 (Jan. 2005), sec. 2.2 are always percent")
        .intro("encoded, while all character explitily stated as unreserved (RFC3986, Jan. 2005, sec. 2.3)")
        .intro("are never percent encoded. Space characters are encoded as `+` characters, while all other")
        .intro("characters are precent-encoded.")
        .intro("")
        .intro("The implementation only handles 8-bit character values. The behavior for multibyte characters")
        .intro("is unspecified.")
        .intro("")
        .intro("The reverse operation of this is `urldecode`.")
        .parameters()
        .parameter("text", "str", "Text string to encode.")
        .parameter(
          "percent_plus",
          "bool",
          "When true the `SP` character (ASCII 0x20) is encoded as `+` instead of its "
          "percent encoding `%20`. Defaults to true.")
        .returns("encoded", "str", "Percent-encoded representation if the input.")
        .see_also("urldecode")());
    py::def(
      "urldecode",
      shyft::dtss::urldecode,
      (py::arg("encoded"), py::arg("space_pluss") = true),
      doc.intro("Decode a percent-encoded string to its original representation.")
        .intro("")
        .intro("All characters designated as unreserved in RFC3986 (Jan. 2005), sec. 2.3 are always passed")
        .intro("through unmodified, except where they are encountered while parsing a percent-encoded value.")
        .intro("")
        .intro("The implementation only handles 8-bit character values. The behavior for multibyte characters")
        .intro("is unspecified.")
        .intro("Additionally it is undefined if characters outside the range `0-9A-Fa-f` are encountered as")
        .intro("one of the two characters immidiatly succeeding a percent-sign.")
        .intro("")
        .intro("This is the reverse operation of `urlencode`.")
        .parameters()
        .parameter("encoded", "str", "Text string to decode.")
        .parameter(
          "percent_plus",
          "bool",
          "When true `+` characters are decoded to `SP` characters (ASCII 0x20). When this "
          "is true and a `+` is encountered and exception is thrown. The default value is true.")
        .returns("text", "str", "Original representation of the encoded input.")
        .intro("")
        .raises()
        .raise(
          "RuntimeError",
          "Thrown if unencoded characters outside the unreserved range is encountered,"
          " includes `+` when the `percent_plus` argument is false. The exception message"
          " contains the character and its location in the string.")
        .see_also("urlencode")());
  }

  void level_db_stuff() {
    using shyft::dtss::db_cfg;
    py::class_<db_cfg>(
      "DtssCfg",
      doc.intro("Configuration for google level db specific parameters.")
        .intro("")
        .intro(
          "Each parameter have reasonable defaults, have a look at google level db documentation for the effect\n"
          "of max_file_size, write_buffer_size and compression.\n"
          "The ppf remains constant once db is created (any changes will be ignored).\n"
          "The other can be changed on persisted/existing databases.\n"
          "\n"
          "About compression:\n"
          "Turns out although very effective for a lot of time-series, it have a single thread performance cost of "
          "2..3x\n"
          "native read/write performance due to compression/decompression.\n"
          "\n"
          "However, for geo dtss we are using multithreaded writes, so performance is limited to  the io-capacity,\n"
          "so it might be set to true for those kind of scenarios.\n")(),
      py::init<>(py::arg("self")))
      .def(py::init<int64_t, bool, int64_t, int64_t, int64_t, int64_t, int64_t, int64_t>(
        (py::arg("self"),
         py::arg("ppf"),
         py::arg("compress"),
         py::arg("max_file_size"),
         py::arg("write_buffer_size"),
         py::arg("log_level") = 200,
         py::arg("test_mode") = 0,
         py::arg("ix_cache") = 0,
         py::arg("ts_cache") = 0),
        doc.intro("construct a DtssCfg  with all values specified")()))
      .def_readwrite(
        "ppf",
        &db_cfg::ppf,
        "int: (default 1024) ts-points per fragment(e.g.key/value), how large ts is chunked into fragments, read/write "
        "operations to key-value storage are in fragment sizes.")
      .def_readwrite(
        "compression",
        &db_cfg::compression,
        "bool: (default False), using snappy compression, could reduce storage 1::3 at similar cost of performance")
      .def_readwrite(
        "max_file_size",
        &db_cfg::max_file_size,
        "int: (default 100Mega), choose to make a reasonable number of files for storing time-series")
      .def_readwrite(
        "write_buffer_size", &db_cfg::write_buffer_size, "int: (default 10Mega), to balance write io-activity.")
      .def_readwrite(
        "log_level",
        &db_cfg::log_level,
        "int: default warn(200), trace(-1000),debug(0),info(100),error(300),fatal(400)")
      .def_readwrite(
        "test_mode", &db_cfg::test_mode, "int: for internal use only, should always be set to 0(the default)")
      .def_readwrite(
        "ix_cache",
        &db_cfg::ix_cache,
        "int: low-level index-cache, could be useful when working with large compressed databases")
      .def_readwrite(
        "ts_cache",
        &db_cfg::ts_cache,
        "int: low-level data-cache, could be useful in case of very large compressed databases")
      .def(py::self == py::self)
      .def(py::self == py::self);
  }

  void dtss() {
    dtss_messages();
    level_db_stuff();
    dtss_q_messages();
    dtss_server();
    dtss_client();
    dtss_cache_stats();
    url_utils();
  }
}
