/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

#include <shyft/py/api/bindings.h>

namespace expose {
  using gta_t = shyft::time_axis::generic_dt;
  using shyft::core::utcperiod;

  gta_t time_axis_merge(gta_t const &a, gta_t const &b) {
    if (a.size() == 0)
      return b;
    if (b.size() == 0)
      return a;
    auto ap = a.total_period();
    auto bp = b.total_period();
    if (ap.end < bp.start || ap.start > bp.end) { // a gap, that we need to fix with a temporary axis w. 1 step between
      utcperiod gp = ap.end < bp.start ? utcperiod(ap.end, bp.start) : utcperiod(bp.end, ap.start);
      gta_t g{gp.start, gp.timespan(), 1};
      return shyft::time_axis::merge(shyft::time_axis::merge(a, g), b);
    }
    if (a == b) // easy& efficient compared to alternative: just return the one
      return a;
    if (ap.end == bp.start || ap.start == bp.end) { // exact joint
      return shyft::time_axis::merge(a, b);
    }
    // ok some overlapping,
    // we use a.merge(combine(a,b)).merge(b)
    auto c = shyft::time_axis::combine(a, b); // c combined overlap-zone
    auto a_c = shyft::time_axis::merge(c, a); // a || c
    return shyft::time_axis::merge(a_c, b);   // a||c||b
  }

  namespace time_axis {
    using namespace shyft::core;

    template <class C_, class PyC>
    PyC &e_time_axis_std(PyC &c) {

      size_t (C_::*index_of_t)(utctime) const = &C_::index_of;
      size_t (C_::*open_range_index_of_t)(utctime, size_t) const = &C_::open_range_index_of;

      return c
        .def(
          "total_period",
          &C_::total_period,
          doc.returns("total_period", "UtcPeriod", "the period that covers the entire time-axis")())
        .def("size", &C_::size, doc.returns("n", "int", "number of periods in time-axis")())
        .def(
          "time",
          &C_::time,
          (py::arg("self"), py::arg("i")),
          doc.parameters()
            .parameter("i", "int", "the i'th period, 0..n-1")
            .returns("utctime", "int", "the start(utctime) of the i'th period of the time-axis")())
        .def(
          "period",
          &C_::period,
          (py::arg("self"), py::arg("i")),
          doc.parameters()
            .parameter("i", "int", "the i'th period, 0..n-1")
            .returns("period", "UtcPeriod", "the i'th period of the time-axis")())
        .def(
          "index_of",
          index_of_t,
          (py::arg("self"), py::arg("t")),
          doc.parameters()
            .parameter("t", "utcime", "utctime in seconds 1970.01.01")
            .returns("index", "int", "the index of the time-axis period that contains t, npos if outside range")())
        .def(
          "open_range_index_of",
          open_range_index_of_t,
          (py::arg("self"), py::arg("t"), py::arg("ix_hint") = std::string::npos),
          doc.intro("returns the index that contains t, or is before t")
            .parameters()
            .parameter("t", "int", "utctime in seconds 1970.01.01")
            .parameter("ix_hint", "int", "index-hint to make search in point-time-axis faster")
            .returns(
              "index",
              "int",
              "the index the time-axis period that contains t, npos if before first period n-1, if t is after last "
              "period")())
        .def(
          "slice",
          &C_::slice,
          (py::arg("self"), py::arg("start"), py::arg("n")),
          doc.intro("returns slice of time-axis as a new time-axis")
            .parameters()
            .parameter("start", "int", "first interval to include")
            .parameter("n", "int", "number of intervals to include")
            .returns("time-axis", "TimeAxis", "A new time-axis with the specified slice")())
        .def(
          "empty",
          &C_::empty,
          (py::arg("self")),
          doc.intro("true if empty time-axis").returns("empty", "bool", "true if empty time-axis")())
        .def(py::self == py::self)
        .def(py::self != py::self);
    }

    static void e_fixed_dt() {
      using namespace shyft::time_axis;

      auto f_dt =
        py::class_<fixed_dt>(
          "TimeAxisFixedDeltaT",
          doc.intro("A time-axis is a set of ordered non-overlapping periods,")
            .intro("and this class implements a fixed delta-t time-axis by")
            .intro("specifying the minimal t-start, delta-t and number of consecutive periods.")
            .intro("This class is a wrapper of the Shyft core-library high performance time-axis")
            .see_also("TimeAxisCalendarDeltaT,TimeAxisByPoints,TimeAxis")())
          .def(py::init<utctime, utctimespan, int64_t>(
            (py::arg("start"), py::arg("delta_t"), py::arg("n")),
            doc.intro("creates a time-axis with n intervals, fixed delta_t, starting at start")
              .parameters()
              .parameter("start", "int", "utc-time 1970 utc based")
              .parameter("delta_t", "int", "number of seconds delta-t, length of periods in the time-axis")
              .parameter("n", "int", "number of periods in the time-axis")()))
          .def_readonly("n", &fixed_dt::n, "int: number of periods")
          .def_readonly("start", &fixed_dt::t, "time: start of the time-axis, in seconds since 1970.01.01 UTC")
          .def_readonly("delta_t", &fixed_dt::dt, "time: time-span of each interval in seconds")
          .def("full_range", &fixed_dt::full_range, "returns a timeaxis that covers [-oo..+oo> ")
          .staticmethod("full_range")
          .def("null_range", &fixed_dt::null_range, "returns a null timeaxis")
          .staticmethod("null_range");
      ;
      e_time_axis_std<fixed_dt>(f_dt);
    }

    static void e_calendar_dt() {
      using namespace shyft::time_axis;
      auto c_dt =
        py::class_<calendar_dt>(
          "TimeAxisCalendarDeltaT",
          doc.intro("A time-axis is a set of ordered non-overlapping periods,")
            .intro("and this class implements a calendar-unit fixed delta-t time-axis by")
            .intro("specifying the minimal calendar, t-start, delta-t and number of consecutive periods.")
            .intro("This class is particularly useful if you need to work with calendar and daylight-saving time,")
            .intro("or calendar-specific periods like day,week,month,quarters or years.")
            .intro("\n")
            .notes()
            .note("For performance reasons, dt < DAY results in a TimeAxisFixedDeltaT.")
            .see_also("TimeAxisFixedDeltaT,TimeAxisByPoints,TimeAxis")())
          .def(py::init<shared_ptr<calendar> const &, utctime, utctimespan, int64_t>(
            (py::arg("calendar"), py::arg("start"), py::arg("delta_t"), py::arg("n")),
            doc.intro("creates a calendar time-axis")
              .parameters()
              .parameter(
                "calendar",
                "Calendar",
                "specifies the calendar to be used, keeps the time-zone and dst-arithmetic rules")
              .parameter("start", "int", "utc-time 1970 utc based")
              .parameter(
                "delta_t",
                "int",
                "number of seconds delta-t, length of periods in the time-axis."
                "For performance reasons, dt < DAY results in a TimeAxisFixedDeltaT.")
              .parameter("n", "int", "number of periods in the time-axis")()))
          .def_readonly("n", &calendar_dt::n, "int: number of periods")
          .def_readonly("start", &calendar_dt::t, "time: start of the time-axis, in seconds since 1970.01.01 UTC")
          .def_readonly(
            "delta_t",
            &calendar_dt::dt,
            "time: timespan of each interval,use Calendar.DAY|.WEEK|.MONTH|.QUARTER|.YEAR, or seconds")
          .add_property("calendar", &calendar_dt::get_calendar, "Calendar: the calendar of the time-axis");
      e_time_axis_std<calendar_dt>(c_dt);
    }

    static void e_point_dt() {
      using namespace shyft::time_axis;
      auto p_dt =
        py::class_<point_dt>(
          "TimeAxisByPoints",
          doc.intro("A time-axis is a set of ordered non-overlapping periods,")
            .intro("and this class implements this by a set of ")
            .intro("ordered unique time-points. This is the most flexible time-axis representation,")
            .intro("that allows every period in the time-axis to have different length.")
            .intro("It comes at the cost of space&performance in certain cases, so ")
            .intro("avoid use in scenarios where high-performance is important.")
            .see_also("TimeAxisCalendarDeltaT,TimeAxisFixedDeltaT,TimeAxis")())
          .def(py::init< vector<utctime> const &, utctime>(
            (py::arg("time_points"), py::arg("t_end")),
            doc.intro("creates a time-axis by specifying the time_points and t-end of the last interval")
              .parameters()
              .parameter(
                "time_points",
                "UtcTimeVector",
                "ordered set of unique utc-time points, the start of each consecutive period")
              .parameter(
                "t_end",
                "int",
                "the end of the last period in time-axis, utc-time 1970 utc based, must be > time_points[-1]")()))
          .def(py::init< vector<utctime> const & >(
            (py::arg("time_points")),
            doc.intro("create a time-axis supplying n+1 points to define n intervals")
              .parameters()
              .parameter(
                "time_points",
                "UtcTimeVector",
                "ordered set of unique utc-time points, 0..n-2:the start of each consecutive period,n-1: end of last "
                "period")()))
          .def_readonly("t", &point_dt::t, "UtcTimeVector: time_points except end of last period, see t_end")
          .def_readonly("t_end", &point_dt::t_end, "time: end of time-axis");
      e_time_axis_std<point_dt>(p_dt);
    }

    static std::vector<int64_t> time_axis_extract_time_points(shyft::time_axis::generic_dt const &ta) {
      std::vector<int64_t> r;
      r.reserve(ta.size() + 1);
      for (size_t i = 0; i < ta.size(); ++i) {
        r.emplace_back(to_seconds64(ta.time(i)));
      }
      if (ta.size())
        r.emplace_back(to_seconds64(ta.total_period().end));
      return r;
    }

    static std::vector<utctime> time_axis_extract_time_points_as_utctime(shyft::time_axis::generic_dt const &ta) {
      std::vector<utctime> r;
      r.reserve(ta.size() + 1);
      for (size_t i = 0; i < ta.size(); ++i) {
        r.emplace_back(ta.time(i));
      }
      if (ta.size())
        r.emplace_back(ta.total_period().end);
      return r;
    }

    static std::vector<utctime>
      time_axis_extract_time_points_as_utctime_tz(shyft::time_axis::generic_dt const &ta, calendar const &c) {
      std::vector<utctime> r;
      r.reserve(ta.size() + 1);
      auto tz = c.get_tz_info();
      for (size_t i = 0; i < ta.size(); ++i) {
        auto t = ta.time(i);
        r.emplace_back(t + tz->utc_offset(t));
      }
      if (ta.size())
        r.emplace_back(ta.total_period().end + tz->utc_offset(ta.total_period().end));
      return r;
    }

    static void e_generic_dt() {
      using namespace shyft::time_axis;
      namespace py = boost::python;
      py::enum_<generic_dt::generic_type>(
        "TimeAxisType", doc.intro("enumeration for the internal time-axis representations")())
        .value("FIXED", generic_dt::generic_type::FIXED)
        .value("CALENDAR", generic_dt::generic_type::CALENDAR)
        .value("POINT", generic_dt::generic_type::POINT)
        // avoid this: .export_values()
        ;

      size_t (generic_dt::*index_of_t)(utctime, size_t) const = &generic_dt::index_of;
      size_t (generic_dt::*open_range_index_of_t)(utctime, size_t) const = &generic_dt::open_range_index_of;

      auto g_dt=py::class_<generic_dt>("TimeAxis",
          doc.intro("A time-axis is a set of ordered non-overlapping periods,")
          .intro("and TimeAxis provides the most generic implementation of this.")
          .intro("The internal representation is selected based on provided parameters")
          .intro("to the constructor.")
          .intro("The internal representation is one of TimeAxis FixedDeltaT CalendarDelataT or ByPoints.")
          .intro("The internal representation type and corresponding realizations are available as properties.")
          .notes()
          .note("The internal representation can be one of TimeAxisCalendarDeltaT,TimeAxisFixedDeltaT,TimeAxisByPoints")()
        )
        .def(py::init<utctime,utctimespan,int64_t>((py::arg("start"),py::arg("delta_t"),py::arg("n")),
            doc.intro("creates a time-axis with n intervals, fixed delta_t, starting at start")
            .parameters()
            .parameter("start","utctime","utc-time 1970 utc based")
            .parameter("delta_t","utctime","number of seconds delta-t, length of periods in the time-axis")
            .parameter("n","int","number of periods in the time-axis")()
          )
        )
        .def(py::init<shared_ptr<calendar>,utctime,utctimespan,int64_t>((py::arg("calendar"),py::arg("start"),py::arg("delta_t"),py::arg("n")),
            doc.intro("creates a calendar time-axis")
            .parameters()
            .parameter("calendar","Calendar","specifies the calendar to be used, keeps the time-zone and dst-arithmetic rules")
            .parameter("start","utctime","utc-time 1970 utc based")
            .parameter("delta_t","utctime","number of seconds delta-t, length of periods in the time-axis")
            .parameter("n","int","number of periods in the time-axis")()
          )
        )
        .def(py::init< vector<utctime> const &,utctime>((py::arg("time_points"),py::arg("t_end")),
            doc.intro("creates a time-axis by specifying the time_points and t-end of the last interval")
            .parameters()
            .parameter("time_points","UtcTimeVector","ordered set of unique utc-time points, the start of each consecutive period")
            .parameter("t_end","time","the end of the last period in time-axis, utc-time 1970 utc based, must be > time_points[-1]")()
          )
        )
        .def(py::init< vector<utctime> const & >(
            (py::arg("time_points")),
            doc.intro("create a time-axis supplying n+1 points to define n intervals")
            .parameters()
            .parameter("time_points","UtcTimeVector","ordered set of unique utc-time points, 0..n-2:the start of each consecutive period,n-1: end of last period")()
           )
        )
        .def(py::init< calendar_dt const &>(
            (py::arg("calendar_dt")),
            doc.intro("create a time-axis from a calendar time-axis")
            .parameters()
            .parameter("calendar_dt","TimeAxisCalendarDeltaT","existing calendar time-axis")()
           )
        )
        .def(py::init< fixed_dt const &>(
            (py::arg("fixed_dt")),
            doc.intro("create a time-axis from a a fixed delta-t time-axis")
            .parameters()
            .parameter("fixed_dt","TimeAxisFixedDeltaT","existing fixed delta-t time-axis")()
           )
        )
        .def(py::init< point_dt const &>(
            (py::arg("point_dt")),
            doc.intro("create a time-axis from a a by points  time-axis")
            .parameters()
            .parameter("point_dt","TimeAxisByPoints","existing by points time-axis")()
           )
        )
        .add_property("timeaxis_type",&generic_dt::gt,"TimeAxisType: describes what time-axis representation type this is,e.g (fixed|calendar|point)_dt ")
        .add_property("fixed_dt",+[](generic_dt const&me){return me.f();},"TimeAxisFixedDeltaT: The fixed dt representation (if active)")
        .add_property("calendar_dt",+[](generic_dt const& me){return me.c();},"TimeAxisCalendarDeltaT: The calendar dt representation(if active)")
        .add_property("point_dt",+[](generic_dt const& me){return me.p();},"TimeAxisByPoints:  point_dt representation(if active)")
        .def("total_period", &generic_dt::total_period,
          doc.returns("total_period", "UtcPeriod", "the period that covers the entire time-axis")()
        )
        .def("size", &generic_dt::size,
          doc.returns("n", "int", "number of periods in time-axis")()
        )
        .def("time", &generic_dt::time, (py::arg("self"),py::arg("i")),
          doc.parameters()
          .parameter("i", "int", "the i'th period, 0..n-1")
          .returns("utctime", "int", "the start(utctime) of the i'th period of the time-axis")()
        )
        .def("period", &generic_dt::period, (py::arg("self"),py::arg("i")),
          doc.parameters()
          .parameter("i", "int", "the i'th period, 0..n-1")
          .returns("period", "UtcPeriod", "the i'th period of the time-axis")()
        )
        .def("index_of", index_of_t, (py::arg("self"),py::arg("t"),py::arg("ix_hint")=string::npos),
          doc.parameters()
          .parameter("t", "int", "utctime in seconds 1970.01.01")
          .parameter("ix_hint","int","index-hint to make search in point-time-axis faster")
          .returns("index", "int", "the index of the time-axis period that contains t, npos if outside range")()
        )
        .def("open_range_index_of", open_range_index_of_t, (py::arg("self"),py::arg("t"), py::arg("ix_hint") = string::npos),
          doc.intro("returns the index that contains t, or is before t")
          .parameters()
          .parameter("t", "int", "utctime in seconds 1970.01.01")
          .parameter("ix_hint", "int", "index-hint to make search in point-time-axis faster")
          .returns("index", "int", "the index the time-axis period that contains t, npos if before first period n-1, if t is after last period")()
        )
        .def("slice",&generic_dt::slice,(py::arg("self"),py::arg("start"),py::arg("n")),
          doc.intro("returns slice of time-axis as a new time-axis")
          .parameters()
          .parameter("start","int","first interval to include")
          .parameter("n","int","number of intervals to include")
          .returns("time-axis","TimeAxis","A new time-axis with the specified slice")()
        )
        .def("merge",&time_axis_merge,(py::arg("self"),py::arg("other")),
           doc.intro("Returns a new time-axis that contains the union of time-points/periods of the two time-axis.")
           .intro("If there is a gap between, it is filled")
           .intro("merge with empty time-axis results into the other time-axis")
           .parameters()
           .parameter("other","TimeAxis","The other time-axis to merge with")
           .returns("merge_result","TimeAxis","the resulting merged time-axis")()
        )
        .def("__call__",&generic_dt::period,(py::arg("self"),py::arg("i")),
          doc.intro("Returns the i-th period of the time-axis")
          .parameters()
          .parameter("i","int","index to lookup")
          .returns("period","UtcPeriod","The period for the supplied index")()
        )
        .def("__len__",&generic_dt::size,(py::arg("self")),
          doc.intro("Returns the number of periods in the time-axis")()
        )
        .def(
          "empty",
          &generic_dt::empty,
          (py::arg("self")),
          doc.intro("true if empty time-axis").returns("empty", "bool", "true if empty time-axis")())
        .def(py::self == py::self)
        .def(py::self != py::self)
        ;//.def("full_range",&point_dt::full_range,"returns a timeaxis that covers [-oo..+oo> ").staticmethod("full_range")
      //.def("null_range",&point_dt::null_range,"returns a null timeaxis").staticmethod("null_range");
      // e_time_axis_std<generic_dt>(g_dt);
      py::def(
        "time_axis_extract_time_points",
        time_axis_extract_time_points,
        (py::arg("time_axis")),
        doc.intro("Extract all time_axis.period(i).start plus time_axis.total_period().end into a UtcTimeVector")
          .parameters()
          .parameter("time_axis", "TimeAxis", "time-axis to extract all time-points from")
          .returns("time_points", "Int64Vector", "all time_axis.period(i).start plus time_axis.total_period().end")());
      py::def(
        "time_axis_extract_time_points_as_utctime",
        time_axis_extract_time_points_as_utctime,
        (py::arg("time_axis")),
        doc.intro("Extract all time_axis.period(i).start plus time_axis.total_period().end into a UtcTimeVector")
          .parameters()
          .parameter("time_axis", "TimeAxis", "time-axis to extract all time-points from")
          .returns(
            "time_points", "UtcTimeVector", "all time_axis.period(i).start plus time_axis.total_period().end")());
      // time_axis_extract_time_points_as_utctime_tz
      py::def(
        "time_axis_extract_time_points_as_utctime_tz",
        time_axis_extract_time_points_as_utctime_tz,
        (py::arg("time_axis"), py::arg("calendar")),
        doc.intro("Extract all time_axis.period(i).start plus time_axis.total_period().end into a UtcTimeVector")
          .intro(" - adding the calendar.tz_info.utc_offset(t) for all t in UtcTimeVector ")
          .intro("Effectively you get out 'tz' version of the time-points, that might not be strictly ascending")
          .intro("Intended usage is within the shyft.dashboard time-series rendering where we currently ")
          .intro("need to pass tz-version of the time for rendering due to lack of proper view-handling of")
          .intro("time-zones in bokeh")
          .intro("NOT recommended for other usage!")
          .parameters()
          .parameter("time_axis", "TimeAxis", "time-axis to extract all time-points from")
          .parameter("calendar", "Calendar", "calendar with tz-info to use for adding tz_info.utc_offset(t)")
          .returns(
            "time_points", "UtcTimeVector", "all time_axis.period(i).start plus time_axis.total_period().end")());
    }

  }

  void api_time_axis() {
    time_axis::e_fixed_dt();
    time_axis::e_point_dt();
    time_axis::e_calendar_dt();
    time_axis::e_generic_dt();
  }

}
