/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <functional>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/api/bindings.h>

namespace expose {
  using namespace shyft::core;
  using namespace std;

  template <class T>
  static inline T range_check(T sec) {
    if (abs(double(sec)) > to_seconds(max_utctime))
      throw runtime_error(
        string("time(s) construct:  specified seconds, ") + std::to_string(sec)
        + ", is outside range min_utctime .. max_utctime");
    return sec;
  }

  // expose for external use in utc-vector
  int64_t utctime_range_check(int64_t sec) {
    return range_check(sec);
  }

  double utctime_range_check(double sec) {
    return range_check(sec);
  }

  struct ct_from_int64 {
    static void construct(int64_t src_seconds, utctime* dst) {
      *dst = utctime_from_seconds64(range_check(src_seconds));
    }
  };

  struct ct_from_utctime {
    static void construct(utctime src, int64_t* dst) {
      *dst = to_seconds64(src);
    }
  };

  struct ct_utctime_from_double {
    static void construct(double src_seconds, utctime* dst) {
      *dst = from_seconds(range_check(src_seconds));
    }
  };

  struct ct_double_from_utctime {
    static void construct(utctime src, double* dst) {
      *dst = to_seconds(src);
    }
  };

  static bool is_npos(size_t n) {
    return n == string::npos;
  }

  template <class T>
  static T x_arg(py::tuple const & args, size_t i) {
    if (py::len(args) + 1 < (int) i)
      throw std::runtime_error("missing arg #" + std::to_string(i) + std::string(" in time"));
    py::object o = args[i];
    py::extract<T> xtract_arg(o);
    return xtract_arg();
  }

  template <class T>
  static T x_kwarg(py::tuple const & args, py::dict const & kwargs, size_t i, char const * kw) {
    if (py::len(args) > (int) i) {
      return x_arg<T>(args, i);
    }
    if (kwargs.has_key(kw)) {
      py::object o = kwargs[kw];
      py::extract<T> xtract_arg(o);
      return xtract_arg();
    }
    throw std::runtime_error("missing kw arg #" + std::string(kw));
  }

  static void args_check(py::tuple const & args) {
    if (py::len(args) < 2)
      throw std::runtime_error("compare needs two args");
  }

  struct utctime_ext {
    static utctime* create_default() {
      return new utctime{0};
    }

    static utctime* create_from_args(py::object const & args) {
      return new utctime{as_utctime(args)};
    }

    static utctime x_self(py::tuple const & args) {
      if (py::len(args) == 0)
        throw std::runtime_error("self is null in time");
      py::object self = args[0];
      py::extract<utctime> xtract_self(self);
      return xtract_self();
    }

    static py::object get_seconds(py::tuple args, py::dict /*kwargs*/) {
      utctime dt = x_self(args);
      auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
      if (dt_s == dt)
        return py::object(int64_t(dt_s.count()));
      return py::object(to_seconds(dt));
    }

    static py::object get_int_seconds(py::tuple args, py::dict /*kwargs*/) {
      utctime dt = x_self(args);
      auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
      if (dt_s == dt)
        return py::object(int64_t(dt_s.count()));
      return py::object(int64_t(to_seconds(dt)));
    }

    static py::object str(py::tuple args, py::dict /*kwargs*/) {
      // note: this is somewhat fuzzy logic to make nice- strings in the debugger...
      // presenting it as a iso 8601 if > 1 year from 1970, otherwise just report seconds
      utctime dt = x_self(args);
      auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
      static calendar utc;
      auto max_year = utc.time(YMDhms::YEAR_MAX, 1, 1);
      auto min_year = utc.time(YMDhms::YEAR_MIN, 1, 1);

      if (
        (dt > calendar::YEAR || dt < -calendar::YEAR)
        && (dt < max_year && dt > min_year)) { // assume user want to see year..
        return py::str(utc.to_string(dt));
      } else {
        char s[100];
        if (dt_s == dt)
#if defined(_WIN32)
          sprintf(s, "%llds", int64_t(dt_s.count()));
#elif defined(__APPLE__)
          sprintf(s, "%llds", int64_t(dt_s.count()));
#else
          sprintf(s, "%lds", int64_t(dt_s.count()));
#endif
        else {
          if (dt == no_utctime)
            return py::str(string("<undefined>"));
          else if (dt == max_utctime)
            return py::str(string("+oo"));
          else if (dt == min_utctime)
            return py::str(string("-oo"));
          sprintf(s, "%0.6lfs", to_seconds(dt));
        }
        return py::str(std::string(s));
      }
    }

    static py::object repr(py::tuple args, py::dict /*kwargs*/) {
      utctime dt = x_self(args);
      auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
      if (dt == no_utctime)
        return py::str(string("time.undefined"));
      else if (dt == max_utctime)
        return py::str(string("time.max"));
      else if (dt == min_utctime)
        return py::str(string("time.min"));
      else {
        char s[100];
        if (dt_s == dt)
#if defined(_WIN32)
          sprintf(s, "time(%lld)", dt_s.count());
#elif defined(__APPLE__)
          sprintf(s, "time(%lld)", dt_s.count());
#else
          sprintf(s, "time(%ld)", dt_s.count());
#endif
        else
          sprintf(s, "time(%0.6lf)", to_seconds(dt));
        return py::str(std::string(s));
      }
    }

    static utctime abs_timespan(utctime x) {
      return x >= x.zero() ? x : -x;
    }

    static int64_t _hash_(utctime x) {
      return std::hash<int64_t>{}(int64_t(x.count()));
    }

    static double _float_(utctime x) {
      return to_seconds(x);
    }

    static utctime _round_(utctime x) {
      return from_seconds(std::round(to_seconds(x)));
    }

    /// best effort convert from any number or string to utctime
    /// as long as it is well defined
    static utctime as_utctime(py::object const & po) {
      py::extract<utctime> o(po);
      if (o.check())
        return o();
      py::extract<int64_t> i(po);
      if (i.check())
        return from_seconds(range_check(i()));
      py::extract<double> d(po);
      if (d.check())
        return from_seconds(range_check(d()));
      py::extract<string> s(po);
      if (s.check())
        return create_from_iso8601_string(s());
      throw std::runtime_error("supplied argument not convertible to time");
    }

    // rel ops
    static py::object _lt_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) < as_utctime(args[1]));
    }

    static py::object _le_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) <= as_utctime(args[1]));
    }

    static py::object _gt_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) > as_utctime(args[1]));
    }

    static py::object _ge_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) >= as_utctime(args[1]));
    }

    static py::object _eq_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) == as_utctime(args[1]));
    }

    static py::object _ne_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) != as_utctime(args[1]));
    }

    // math
    static py::object _add_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) + as_utctime(args[1]));
    }

    static py::object _sub_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) - as_utctime(args[1]));
    }

    static py::object _rsub_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(-(x_arg<utctime>(args, 0) - as_utctime(args[1])));
    }

    static py::object _mult_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(from_seconds(to_seconds(x_arg<utctime>(args, 0)) * to_seconds(as_utctime(args[1]))));
    }

    static py::object _div_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(from_seconds(to_seconds(x_arg<utctime>(args, 0)) / to_seconds(as_utctime(args[1]))));
    }

    static py::object _mod_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(x_arg<utctime>(args, 0) % as_utctime(args[1]));
    }

    static py::object _floordiv_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(from_seconds(truncf(to_seconds(x_arg<utctime>(args, 0)) / to_seconds(as_utctime(args[1])))));
    }

    static py::object _rfloordiv_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(from_seconds(truncf(to_seconds(as_utctime(args[1])) / to_seconds(x_arg<utctime>(args, 0)))));
    }

    static py::object _rdiv_(py::tuple args, py::dict /*kwargs*/) {
      args_check(args);
      return py::object(from_seconds(to_seconds(as_utctime(args[1])) / to_seconds(x_arg<utctime>(args, 0))));
    }

    // just for backward(consider change code)
    static py::object _sqrt_(py::tuple args, py::dict /*kwargs*/) {
      return py::object(from_seconds(sqrt(to_seconds(as_utctime(args[0])))));
    }
  };

  utctime x_kwarg_utctime(py::tuple const & args, py::dict const & kwargs, size_t i, char const * kw) {
    if (py::len(args) > (int) i) {
      return utctime_ext::as_utctime(args[i]);
    }
    if (kwargs.has_key(kw)) {
      return utctime_ext::as_utctime(kwargs[kw]);
    }
    throw std::runtime_error("missing kw arg #" + std::string(kw));
  }

  struct utctime_picklers : py::pickle_suite {
    static auto getstate(utctime const & t) -> py::tuple {
      return py::make_tuple(t.count());
    }

    static auto setstate(utctime& t, py::tuple state) -> void {
      t += utctime(py::extract<int64_t>(state[0]));
    }
  };

  struct utcperiod_picklers : py::pickle_suite {
    static auto getinitargs(utcperiod const & p) -> py::tuple {
      return py::make_tuple(p.start, p.end);
    }
  };

  static int64_t utcperiod_hash_(utcperiod x) {
    utcperiod_hasher h{};
    return h(x);
  }

  struct calendar_ext {
    static py::object trim(py::tuple const & args, py::dict const & kwargs) {
      auto const & c = x_arg< calendar const &>(args, 0);
      auto t = x_kwarg_utctime(args, kwargs, 1, "t");
      auto dt = x_kwarg_utctime(args, kwargs, 2, "delta_t");
      return py::object(c.trim(t, dt));
    }

    static py::object add(py::tuple const & args, py::dict const & kwargs) {
      auto const & c = x_arg< calendar const &>(args, 0);
      auto t = x_kwarg_utctime(args, kwargs, 1, "t");
      auto dt = x_kwarg_utctime(args, kwargs, 2, "delta_t");
      auto n = x_kwarg<int>(args, kwargs, 3, "n");
      return py::object(c.add(t, dt, n));
    }

    static py::object diff_units(py::tuple const & args, py::dict const & kwargs) {
      auto const & c = x_arg< calendar const &>(args, 0);
      auto t1 = x_kwarg_utctime(args, kwargs, 1, "t1");
      auto t2 = x_kwarg_utctime(args, kwargs, 2, "t2");
      auto dt = x_kwarg_utctime(args, kwargs, 3, "delta_t");
      return py::object(c.diff_units(t1, t2, dt));
    }

    static py::object to_string(py::tuple const & args, py::dict const & kwargs) {
      auto const & c = x_arg< calendar const &>(args, 0);
      return py::object(c.to_string(x_kwarg_utctime(args, kwargs, 1, "t")));
    }

    static py::object calendar_units(py::tuple const & args, py::dict const & kwargs) {
      auto const & c = x_arg< calendar const &>(args, 0);
      return py::object(c.calendar_units(x_kwarg_utctime(args, kwargs, 1, "t")));
    }

    static py::object calendar_week_units(py::tuple const & args, py::dict const & kwargs) {
      auto const & c = x_arg< calendar const &>(args, 0);
      return py::object(c.calendar_week_units(x_kwarg_utctime(args, kwargs, 1, "t")));
    }
  };

  static void e_utctime() {
    py::class_<utctime>(
      "time",
      doc.intro("**time** is represented as a number, in SI-unit seconds.\n")
        .intro("For accuracy and performance, it's internally represented as 64bit integer, at micro-second resolution")
        .intro("It is usually used in two roles:\n\n")
        .intro(" 1) time measured in seconds since epoch (1970.01.01UTC)")
        .intro("      often constructed using the Calendar class that takes calendar-coordinates (YMDhms etc) and ")
        .intro("      returns the corresponding time-point, taking time-zone and dst etc. into account.\n")
        .intro(">>>      utc = Calendar()")
        .intro(">>>      t1 = utc.time(2018,10,15,16,30,15)")
        .intro(">>>      t2 = time('2018-10-15T16:30:15Z')")
        .intro(">>>      t3 = time(1539621015)\n\n")
        .intro(" 2) time measure, in unit of seconds (resolution up to 1us)")
        .intro("      often constructed from numbers, always use SI-unit of seconds\n")
        .intro(">>>      dt1 = time(3600)  # 3600 seconds")
        .intro(">>>      dt2 = time(0.123456)  #  0.123456 seconds\n")
        .intro("\n")
        .intro("It can be constructed supplying number of seconds, or a well defined iso8601 string\n")
        .intro("To convert it to a python number, use float() or int() to cast operations")
        .intro("If dealing with time-zones/calendars conversion, use the calendar.time(..)/.calendar_units functions.")
        .intro("If you want to use time-zone/calendar semantic add/diff/trim, use the corresponding Calendar methods.")
        .intro("")
        .see_also("Calendar,deltahours,deltaminutes")(),
      py::no_init)
      .def(
        "__init__",
        py::make_constructor(&utctime_ext::create_default, py::default_call_policies()),
        doc.intro("construct a 0s")())
      .def(
        "__init__",
        py::make_constructor(&utctime_ext::create_from_args, py::default_call_policies(), (py::arg("seconds"))),
        doc.intro("construct a from a precise time like item, e.g. time, float,int and iso8601 string")
          .parameters()
          .parameter(
            "seconds", "", "A time like item, time, float,int or iso8601 YYYY-MM-DDThh:mm:ss[.xxxxxx]Z string")())

      .def_pickle(utctime_picklers())
      .add_property(
        "seconds", py::raw_function(utctime_ext::get_seconds, 1), doc.intro("float: returns time in seconds")())
      .def("__abs__", &utctime_ext::abs_timespan, (py::arg("self")))
      .def("__round__", &utctime_ext::_round_, (py::arg("self")))
      .def("__float__", &utctime_ext::_float_, (py::arg("self")))
      .def("__int__", raw_function(utctime_ext::get_int_seconds, 1), doc.intro("time as int seconds")())
      .def("__long__", raw_function(utctime_ext::get_int_seconds, 1), doc.intro("time as int seconds")())
      .def("__repr__", raw_function(utctime_ext::repr, 1), doc.intro("repr of time")())
      .def("__str__", raw_function(utctime_ext::str, 1), doc.intro("str of time")())
      // rel operations
      .def("__lt__", raw_function(utctime_ext::_lt_, 1))
      .def("__le__", raw_function(utctime_ext::_le_, 1))
      .def("__gt__", raw_function(utctime_ext::_gt_, 1))
      .def("__ge__", raw_function(utctime_ext::_ge_, 1))
      .def("__eq__", raw_function(utctime_ext::_eq_, 1))
      .def("__ne__", raw_function(utctime_ext::_ne_, 1))
      .def(
        "__neg__",
        +[](utctime x) {
          return -x;
        })

      // math
      .def("__add__", raw_function(utctime_ext::_add_, 1))
      .def("__radd__", raw_function(utctime_ext::_add_, 1))
      .def("__sub__", raw_function(utctime_ext::_sub_, 1))
      .def("__rsub__", raw_function(utctime_ext::_rsub_, 1))
      .def("__truediv__", raw_function(utctime_ext::_div_, 1))
      .def("__rtruediv__", raw_function(utctime_ext::_rdiv_, 1))
      .def("__floordiv__", raw_function(utctime_ext::_floordiv_, 1))
      .def("__rfloordiv__", raw_function(utctime_ext::_rfloordiv_, 1))
      .def("__mod__", raw_function(utctime_ext::_mod_, 1))

      .def("__mul__", raw_function(utctime_ext::_mult_, 1))
      .def("__rmul__", raw_function(utctime_ext::_mult_, 1))

      .def("sqrt", raw_function(utctime_ext::_sqrt_, 1))
      //.def(self % self)
      .def(-py::self)
      // hash is needed!
      .def("__hash__", &utctime_ext::_hash_, (py::arg("self")))
      .add_static_property(
        "min",
        +[]() {
          return min_utctime;
        })
      .add_static_property(
        "max",
        +[]() {
          return max_utctime;
        })
      .add_static_property(
        "undefined",
        +[]() {
          return no_utctime;
        })
      .add_static_property(
        "epoch",
        +[]() {
          return utctime_0;
        })
      .def(
        "now",
        +[]() {
          return utctime_now();
        })
      .staticmethod("now");
    py::def("utctime_now", utctime_now, "returns time now as seconds since 1970s");
    py::def("deltahours", deltahours, (py::arg("n")), "returns time equal to specified n hours");
    py::def("deltaminutes", deltaminutes, (py::arg("n")), "returns time equal to specified n minutes");
    py::def("is_npos", is_npos, (py::arg("n")), "returns true if n is npos, - meaning no position");

    py::scope current;
    current.attr("max_utctime") = max_utctime;
    current.attr("min_utctime") = min_utctime;
    current.attr("no_utctime") = no_utctime;
    current.attr("npos") = string::npos;
    // TODO: require own type for utctime:
    // implicitly_convertible<utctime,int64_t>();
    // implicitly_convertible<int64_t,utctime>();
    // fx_implicitly_convertible<int64_t,utctime,ct_from_int64>();
    // fx_implicitly_convertible<utctime,int64_t,ct_from_utctime>();
    py::fx_implicitly_convertible<double, utctime, ct_utctime_from_double>();
    // fx_implicitly_convertible<utctime,double,ct_double_from_utctime>();
  }

  typedef std::vector<utcperiod> UtcPeriodVector;

  static void e_calendar() {

    // std::string (shyft::core::calendar::*to_string_t)(shyft::core::utctime) const= &calendar::to_string;//selects
    // correct ptr.
    std::string (calendar::*to_string_p)(utcperiod) const = &calendar::to_string;
    // int64_t (calendar::*diff_units)(utctime,utctime,utctimespan) const=&calendar::diff_units;
    utctime (calendar::*time_YMDhms)(YMDhms) const = &calendar::time;
    utctime (calendar::*time_YWdhms)(YWdhms) const = &calendar::time;
    utctime (calendar::*time_6)(int, int, int, int, int, int, int) const = &calendar::time;
    utctime (calendar::*time_from_week_6)(int, int, int, int, int, int, int) const = &calendar::time_from_week;
    py::class_<calendar, shared_ptr<calendar>>(
      "Calendar",
      doc.intro("Calendar deals with the concept of a human calendar\n")
        .intro("In Shyft we practice the 'utc-time perimeter' principle,\n")
        .intro("  * the core is utc-time exclusively ")
        .intro("  * we deal with time-zone and calendars at the interfaces/perimeters\n")
        .intro("In python, this corresponds to timestamp[64], or as the integer version of the time package "
               "representation")
        .intro("e.g. the difference between time.time()- utctime_now() is in split-seconds")
        .intro("\n")
        .intro("Calendar functionality:\n")
        .intro(" * Conversion between the calendar coordinates YMDhms or iso week YWdhms and utctime, taking  any "
               "timezone and DST into account\n")
        .intro(" * Calendar constants, utctimespan like values for Year,Month,Week,Day,Hour,Minute,Second\n")
        .intro(" * Calendar arithmetic, like adding calendar units, e.g. day,month,year etc.\n")
        .intro(" * Calendar arithmetic, like trim/truncate a utctime down to nearest timespan/calendar unit. eg. day\n")
        .intro(" * Calendar arithmetic, like calculate difference in calendar units(e.g days) between two utctime "
               "points\n")
        .intro(" * Calendar Timezone and DST handling\n")
        .intro(" * Converting time to string and vice-versa\n")
        .intro("\n")
        .notes()
        .note("Please notice that although the calendar concept is complete:\n")
        .note(" * We only implement features as needed in the core and interfaces")
        .note("   Currently this includes most options, including olson time-zone handling")
        .note("   The time-zone support is currently a snapshot of rules ~2023")
        .note("   but we plan to use standard packages like Howard Hinnant's online approach for this later.")
        .note(" * Working range for DST is 1905..2105 (dst is considered 0 outside)")
        .note(" * Working range,validity of the calendar computations is limited to gregorian as of boost::date.")
        .note(" * Working range avoiding overflows is -4000 .. + 4000 Years")())
      .def(py::init<utctime>(
        (py::arg("tz_offset")),
        doc.intro("creates a calendar with constant tz-offset")
          .parameters()
          .parameter("tz_offset", "time", "specifies utc offset, time(3600) gives UTC+01 zone")()))
      .def(py::init<int>(
        (py::arg("tz_offset")),
        doc.intro("creates a calendar with constant tz-offset")
          .parameters()
          .parameter("tz_offset", "int", "seconds utc offset, 3600 gives UTC+01 zone")()))
      .def(py::init<string>(
        (py::arg("olson_tz_id")),
        doc.intro("create a Calendar from Olson timezone id, eg. 'Europe/Oslo'")
          .parameters()
          .parameter("olson_tz_id", "str", "Olson time-zone id, e.g. 'Europe/Oslo'")()))
      .def(
        "to_string",
        py::raw_function(calendar_ext::to_string, 2), //) to_string_t, (py::arg("self"),py::arg("t")),
        doc.intro("convert time t to readable iso standard string taking ")
          .intro(" the current calendar properties, including timezone into account")
          .parameters()
          .parameter("utctime", "time", "seconds utc since epoch")
          .returns("iso time string", "str", "iso standard formatted string,including tz info")())
      .def(
        "to_string",
        to_string_p,
        (py::arg("self"), py::arg("utcperiod")),
        doc
          .intro("convert utcperiod p to readable string taking current calendar properties, including timezone into "
                 "account")
          .parameters()
          .parameter("utcperiod", "UtcPeriod", "An UtcPeriod object")
          .returns("period-string", "str", "[start..end>, iso standard formatted string,including tz info")())
      .def(
        "region_id_list",
        &calendar::region_id_list,
        doc.intro("Returns a list over predefined Olson time-zone identifiers")
          .notes()
          .note("the list is currently static and reflects tz-rules approx as of 2014")())
      .staticmethod("region_id_list")
      .def(
        "calendar_units",
        py::raw_function(calendar_ext::calendar_units, 2),
        doc
          .intro("returns YMDhms (.year,.month,.day,.hour,.minute..)  for specified t, in the time-zone as given by "
                 "the calendar")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .returns("calendar_units", "YMDhms", "calendar units as in year-month-day hour-minute-second")())
      .def(
        "calendar_week_units",
        py::raw_function(calendar_ext::calendar_week_units, 2),
        doc
          .intro("returns iso YWdhms, with properties (.iso_year,.iso_week,.week_day,.hour,..)\n"
                 "for specified t, in the time-zone as given by the calendar\n")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .returns("calendar_week_units", "YWdms", "calendar units as in iso year-week-week_day hour-minute-second")())
      .def(
        "time",
        time_YMDhms,
        (py::arg("self"), py::arg("YMDhms")),
        doc.intro("convert calendar coordinates into time using the calendar time-zone")
          .parameters()
          .parameter("YMDhms", "YMDhms", "calendar cooordinate structure containg year,month,day, hour,minute,second")
          .returns("timestamp", "int", "timestamp as in seconds utc since epoch")())
      .def(
        "time",
        time_YWdhms,
        (py::arg("self"), py::arg("YWdhms")),
        doc.intro("convert calendar iso week coordinates structure into time using the calendar time-zone")
          .parameters()
          .parameter("YWdhms", "YWdhms", "structure containg iso specification calendar coordinates")
          .returns("timestamp", "int", "timestamp as in seconds utc since epoch")())

      .def(
        "time",
        time_6,
        (py::arg("self"),
         py::arg("Y"),
         py::arg("M") = 1,
         py::arg("D") = 1,
         py::arg("h") = 0,
         py::arg("m") = 0,
         py::arg("s") = 0,
         py::arg("us") = 0),
        doc.intro("convert calendar coordinates into time using the calendar time-zone")
          .parameters()
          .parameter("Y", "int", "Year")
          .parameter("M", "int", "Month  [1..12], default=1")
          .parameter("D", "int", "Day    [1..31], default=1")
          .parameter("h", "int", "hour   [0..23], default=0")
          .parameter("m", "int", "minute [0..59], default=0")
          .parameter("s", "int", "second [0..59], default=0")
          .parameter("us", "int", "micro second[0..999999], default=0")
          .returns("timestamp", "time", "timestamp as in seconds utc since epoch")())
      .def(
        "time_from_week",
        time_from_week_6,
        (py::arg("self"),
         py::arg("Y"),
         py::arg("W") = 1,
         py::arg("wd") = 1,
         py::arg("h") = 0,
         py::arg("m") = 0,
         py::arg("s") = 0,
         py::arg("us") = 0),
        doc.intro("convert calendar iso week coordinates into time using the calendar time-zone")
          .parameters()
          .parameter("Y", "int", "ISO Year")
          .parameter("W", "int", "ISO Week  [1..54], default=1")
          .parameter("wd", "int", "week_day  [1..7]=[mo..su], default=1")
          .parameter("h", "int", "hour   [0..23], default=0")
          .parameter("m", "int", "minute [0..59], default=0")
          .parameter("s", "int", "second [0..59], default=0")
          .parameter("us", "int", "micro second[0..999999], default=0")
          .returns("timestamp", "time", "timestamp as in seconds utc since epoch")())

      .def(
        "add",
        py::raw_function(calendar_ext::add, 4), // ("t", "delta_t", "n"),
        doc.intro("This function does a calendar semantic add.\n")
          .intro("Conceptually this is similar to t + deltaT*n")
          .intro("but with deltaT equal to calendar::DAY,WEEK,MONTH,YEAR")
          .intro("and/or with dst enabled time-zone the variation of length due to dst")
          .intro("as well as month/year length is taken into account.")
          .intro("E.g. add one day, and calendar have dst, could give 23,24 or 25 hours due to dst.")
          .intro("Similar for week or any other time steps.")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .parameter("delta_t", "time", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .parameter("n", "int", "number of timesteps to add")
          .returns("t", "time", "new timestamp with the added time-steps, seconds utc since epoch")
          .notes()
          .note("ref. to related functions .diff_units(...) and .trim(..)")())
      .def(
        "diff_units",
        py::raw_function(calendar_ext::diff_units, 4), // diff_units,("t1","t2","delta_t"),
        doc.intro("calculate the distance t1..t2 in specified units, taking dst into account if observed")
          .intro("The function takes calendar semantics when delta_t is calendar::DAY,WEEK,MONTH,YEAR,")
          .intro("and in addition also dst if observed.")
          .intro("e.g. the diff_units of calendar::DAY over summer->winter shift is 1,")
          .intro("even if the number of hours during those days are 23 and 25 summer and winter transition "
                 "respectively.")
          .intro("It computes the calendar semantics of (t2-t1)/deltaT, where deltaT could be calendar units "
                 "DAY,WEEK,MONTH,YEAR")
          .parameters()
          .parameter("t1", "time", "timestamp utc seconds since epoch")
          .parameter("t2", "time", "timestamp utc seconds since epoch")
          .parameter("delta_t", "time", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .returns("n_units", "int", "number of units, so that t2 = c.add(t1,delta_t,n) + remainder(discarded)")
          .notes()
          .note("ref. to related functions .add(...) and .trim(...)")())
      .def(
        "trim",
        py::raw_function(calendar_ext::trim, 3),
        doc.intro("Round time t down to the nearest calendar time-unit delta_t,")
          .intro("taking the calendar time-zone and dst into account.\n")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .parameter(
            "delta_t", "time", "timestep in seconds, with semantic interpretation of Calendar.{DAY|WEEK|MONTH|YEAR}")
          .returns("t", "time", "new trimmed timestamp, seconds utc since epoch")
          .notes()
          .note("ref to related functions .add(t,delta_t,n),.diff_units(t1,t2,delta_t)")())

      .def(
        "quarter",
        &calendar::quarter,
        (py::arg("t")),
        doc.intro("returns the quarter of the specified t, -1 if invalid t")
          .parameters()
          .parameter("t", "int", "timestamp utc seconds since epoch")
          .returns("quarter", "int", "in range[1..4], -1 if invalid time")())
      .def_readonly("YEAR", &calendar::YEAR, "time: defines a semantic year")
      .def_readonly("MONTH", &calendar::MONTH, "time: defines a semantic calendar month")
      .def_readonly("QUARTER", &calendar::QUARTER, "time: defines a semantic calendar quarter (3 months)")
      .def_readonly("DAY", &calendar::DAY, "time: defines a semantic calendar day")
      .def_readonly("WEEK", &calendar::WEEK, "time: defines a semantic calendar week")
      .def_readonly("HOUR", &calendar::HOUR, "time: hour, 3600 seconds")
      .def_readonly("MINUTE", &calendar::MINUTE, "time: minute, 60 seconds")
      .def_readonly("SECOND", &calendar::SECOND, "time: second, 1 second")
      .add_property(
        "tz_info",
        &calendar::get_tz_info,
        "TzInfo: keeping the time-zone name, utc-offset and DST rules (if any)") //,return_value_policy<return_internal_reference>())
      .def(
        "day_of_year",
        &calendar::day_of_year,
        (py::arg("self"), py::arg("t")),
        doc.intro("returns the day of year for the specified time")
          .parameters()
          .parameter("t", "time", "time to use for computation")
          .returns("day_of_year", "int", "in range 1..366")())
      .def_readonly("TZ_RANGE", &calendar::tz_range, "UtcPeriod: Working range of DST and time-zone rules")
      .def_readonly(
        "RANGE",
        &calendar::range,
        "UtcPeriod: Working range of calendar arithmetic, to avoid overflow, the validity is furhter limited by "
        "gregorian calendar")
      .def(py::self == py::self)
      .def(py::self != py::self)

      ;

    py::class_<YMDhms>(
      "YMDhms",
      doc.intro("Defines calendar coordinates as Year Month Day hour minute second and micro_second")
        .intro("The intended usage is ONLY as result from the Calendar.calendar_units(t),")
        .intro("to ensure a type-safe return of these entities for a given time.")
        .note("Please use this as a read-only return type from the Calendar.calendar_units(t)")())
      .def(py::init<int, py::optional<int, int, int, int, int, int>>(
        (py::arg("Y"), py::arg("M"), py::arg("D"), py::arg("h"), py::arg("m"), py::arg("s"), py::arg("us")),
        "Creates calendar coordinates specifying Y,M,D,h,m,s,us"))
      .def("is_valid", &YMDhms::is_valid, "returns true if YMDhms values are reasonable")
      .def("is_null", &YMDhms::is_null, "returns true if all values are 0, - the null definition")
      .def_readwrite("year", &YMDhms::year, "int: ")
      .def_readwrite("month", &YMDhms::month, "int: ")
      .def_readwrite("day", &YMDhms::day, "int: ")
      .def_readwrite("hour", &YMDhms::hour, "int: ")
      .def_readwrite("minute", &YMDhms::minute, "int: ")
      .def_readwrite("second", &YMDhms::second, "int: ")
      .def_readwrite("micro_second", &YMDhms::micro_second, "int: ")
      .def("max", &YMDhms::max, "returns the maximum representation")
      .staticmethod("max")
      .def("min", &YMDhms::min, "returns the minimum representation")
      .staticmethod("min")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::class_<YWdhms>(
      "YWdhms",
      doc.intro("Defines calendar coordinates as iso Year Week week-day hour minute second and micro_second")
        .intro("The intended usage is ONLY as result from the Calendar.calendar_week_units(t),")
        .intro("to ensure a type-safe return of these entities for a given time.")
        .notes()
        .note("Please use this as a read-only return type from the Calendar.calendar_week_units(t)")()

        )
      .def(py::init<int, py::optional<int, int, int, int, int, int>>(
        (py::arg("Y"), py::arg("W"), py::arg("wd"), py::arg("h"), py::arg("m"), py::arg("s"), py::arg("us")),
        doc.intro("Creates calendar coordinates specifying iso Y,W,wd,h,m,s")
          .parameters()
          .parameter("Y", "int", "iso-year")
          .parameter("W", "int", "iso week [1..53]")
          .parameter("wd", "int", "week_day [1..7]=[mo..sun]")
          .parameter("h", "int", "hour [0..23]")
          .parameter("m", "int", "minute [0..59]")
          .parameter("s", "int", "second [0..59]")
          .parameter("us", "int", "micro_second [0..999999]")()))
      .def("is_valid", &YWdhms::is_valid, "returns true if YWdhms values are reasonable")
      .def("is_null", &YWdhms::is_null, "returns true if all values are 0, - the null definition")
      .def_readwrite("iso_year", &YWdhms::iso_year, "int: ")
      .def_readwrite("iso_week", &YWdhms::iso_week, "int: ")
      .def_readwrite("week_day", &YWdhms::week_day, doc.intro("int: week_day,[1..7]=[mo..sun]"))
      .def_readwrite("hour", &YWdhms::hour, "int: ")
      .def_readwrite("minute", &YWdhms::minute, "int: ")
      .def_readwrite("second", &YWdhms::second, "int: ")
      .def_readwrite("micro_second", &YWdhms::micro_second, "int: ")
      .def("max", &YWdhms::max, "returns the maximum representation")
      .staticmethod("max")
      .def("min", &YWdhms::min, "returns the minimum representation")
      .staticmethod("min")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::class_<time_zone::tz_info_t, py::bases<>, time_zone::tz_info_t_, boost::noncopyable>(
      "TzInfo",
      doc.intro("The TzInfo class is responsible for providing information about the\n"
                "time-zone of the calendar.\n"
                "This includes:\n\n"
                " * name (olson identifier)\n"
                " * base_offset\n"
                " * utc_offset(t) time-dependent\n\n"
                "The Calendar class provides a shared pointer to it's TzInfo object")(),
      py::no_init)
      .def(py::init<utctimespan>((py::arg("base_tz")), "creates a TzInfo with a fixed utc-offset(no dst-rules)"))
      .def("name", &time_zone::tz_info_t::name, "returns the olson time-zone identifier or name for the TzInfo")
      .def("base_offset", &time_zone::tz_info_t::base_offset, "returnes the time-invariant part of the utc-offset")
      .def(
        "utc_offset",
        &time_zone::tz_info_t::utc_offset,
        (py::arg("t")),
        "returns the utc_offset at specified utc-time, takes DST into account if applicable")
      .def(
        "is_dst", &time_zone::tz_info_t::is_dst, (py::arg("t")), "returns true if DST is observed at given utc-time t");
  }

  static void e_utcperiod() {
    py::enum_<shyft::core::trim_policy>(
      "trim_policy", doc.intro("Enum to decide if to trim inwards or outwards where TRIM_IN means inwards")())
      .value("TRIM_IN", shyft::core::trim_policy::TRIM_IN)
      .value("TRIM_OUT", shyft::core::trim_policy::TRIM_OUT)
      .export_values();
    bool (utcperiod::*contains_t)(utctime) const = &utcperiod::contains;
    bool (utcperiod::*contains_i)(int64_t) const = &utcperiod::contains;
    bool (utcperiod::*contains_p)(utcperiod const &) const = &utcperiod::contains;
    utcperiod (utcperiod::*trim_i)(calendar const &, int64_t, trim_policy) const = &utcperiod::trim;
    utcperiod (utcperiod::*trim_t)(calendar const &, utctimespan, trim_policy) const = &utcperiod::trim;
    int64_t (utcperiod::*diff_units_t)(calendar const &, utctimespan) const = &utcperiod::diff_units;
    int64_t (utcperiod::*diff_units_i)(calendar const &, int64_t) const = &utcperiod::diff_units;

    py::class_<utcperiod>(
      "UtcPeriod",
      doc.intro("UtcPeriod defines the open utc-time range [start..end>")
        .intro("where end is required to be equal or greater than start")())
      .def_pickle(utcperiod_picklers())
      .def(py::init<utctime, utctime>((py::arg("start"), py::arg("end")), "Create utcperiod given start and end"))
      .def("valid", &utcperiod::valid, "returns true if start<=end otherwise false")
      .def(
        "contains",
        contains_t,
        (py::arg("self"), py::arg("t")),
        "returns true if time t is contained in this utcperiod")
      .def(
        "contains",
        contains_i,
        (py::arg("self"), py::arg("t")),
        "returns true if time t is contained in this utcperiod")
      .def(
        "contains",
        contains_p,
        (py::arg("self"), py::arg("p")),
        "returns true if utcperiod p is contained in this utcperiod")
      .def(
        "overlaps",
        &utcperiod::overlaps,
        (py::arg("self"), py::arg("p")),
        "returns true if period p overlaps this utcperiod")
      .def(
        "trim",
        trim_t,
        (py::arg("self"),
         py::arg("calendar"),
         py::arg("delta_t"),
         py::arg("trim_policy") = shyft::core::trim_policy::TRIM_IN),
        doc.intro("Round UtcPeriod up or down to the nearest calendar time-unit delta_t")
          .intro("taking the calendar time-zone and dst into account")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter(
            "delta_t", "time", "timestep in seconds, with semantic interpretation of Calendar.(DAY,WEEK,MONTH,YEAR)")
          .parameter("trim_policy", "trim_policy", "TRIM_IN if rounding period inwards, else rounding outwards")
          .returns("trimmed_UtcPeriod", "UtcPeriod", "new trimmed UtcPeriod")())
      .def(
        "trim",
        trim_i,
        (py::arg("self"),
         py::arg("calendar"),
         py::arg("delta_t"),
         py::arg("trim_policy") = shyft::core::trim_policy::TRIM_IN),
        doc.intro("Round UtcPeriod up or down to the nearest calendar time-unit delta_t")
          .intro("taking the calendar time-zone and dst into account")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter(
            "delta_t", "int", "timestep in seconds, with semantic interpretation of Calendar.(DAY,WEEK,MONTH,YEAR)")
          .parameter("trim_policy", "trim_policy", "TRIM_IN if rounding period inwards, else rounding outwards")
          .returns("trimmed_UtcPeriod", "UtcPeriod", "new trimmed UtcPeriod")())
      .def(
        "diff_units",
        diff_units_t,
        (py::arg("self"), py::arg("calendar"), py::arg("delta_t")),
        doc
          .intro("Calculate the distance from start to end of UtcPeriod in specified units, taking dst into account if "
                 "observed")
          .intro("The function takes calendar semantics when delta_t is calendar::DAY,WEEK,MONTH,YEAR,")
          .intro("and in addition also dst if observed.")
          .intro("e.g. the diff_units of calendar::DAY over summer->winter shift is 1,")
          .intro("even if the number of hours during those days are 23 and 25 summer and winter transition "
                 "respectively")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter("delta_t", "time", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .returns("n_units", "int", "number of units in UtcPeriod")())
      .def(
        "diff_units",
        diff_units_i,
        (py::arg("self"), py::arg("calendar"), py::arg("delta_t")),
        doc
          .intro("Calculate the distance from start to end of UtcPeriod in specified units, taking dst into account if "
                 "observed")
          .intro("The function takes calendar semantics when delta_t is calendar::DAY,WEEK,MONTH,YEAR,")
          .intro("and in addition also dst if observed.")
          .intro("e.g. the diff_units of calendar::DAY over summer->winter shift is 1,")
          .intro("even if the number of hours during those days are 23 and 25 summer and winter transition "
                 "respectively")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter("delta_t", "int", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .returns("n_units", "int", "number of units in UtcPeriod")())
      .def("__str__", &utcperiod::to_string, "returns the str using time-zone utc to convert to readable time")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def("__hash__", &utcperiod_hash_, (py::arg("self")))

      .def("timespan", &utcperiod::timespan, "returns end-start, the timespan of the period")
      .def_readwrite("start", &utcperiod::start, "time: Defines the start of the period, inclusive")
      .def_readwrite("end", &utcperiod::end, "time: Defines the end of the period, not inclusive")
      .def(
        "intersection",
        +[](utcperiod a, utcperiod b) {
          return intersection(a, b);
        },
        (py::arg("a"), py::arg("b")),
        doc.intro("Returns the intersection of two periods")
          .intro("if there is an intersection, the resulting period will be .valid() and .timespan()>0")
          .intro("If there is no intersection, an empty not .valid() period is returned")
          .parameters()
          .parameter("a", "UtcPeriod", "1st UtcPeriod argument")
          .parameter("b", "UtcPeriod", "2nd UtcPeriod argument")
          .returns("intersection", "UtcPeriod", "The computed intersection, or an empty not .valid() UtcPeriod")())
      .def("__str__", &utcperiod::to_string, "the str representation")
      .def("__repr__", &utcperiod::to_string, "the str representation")
      .def("to_string", &utcperiod::to_string, "A readable representation in UTC");
    py::def(
      "intersection",
      &intersection,
      (py::arg("a"), py::arg("b")),
      doc.intro("Returns the intersection of two periods")
        .intro("if there is an intersection, the resulting period will be .valid() and .timespan()>0")
        .intro("If there is no intersection, an empty not .valid() period is returned")
        .parameters()
        .parameter("a", "UtcPeriod", "1st UtcPeriod argument")
        .parameter("b", "UtcPeriod", "2nd UtcPeriod argument")
        .returns("intersection", "UtcPeriod", "The computed intersection, or an empty not .valid() UtcPeriod")());
  }

  void calendar_and_time() {
    e_utctime();
    e_utcperiod();
    e_calendar();
  }
}
