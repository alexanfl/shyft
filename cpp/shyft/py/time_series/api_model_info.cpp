/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/srv/model_info.h>

#include <shyft/py/api/bindings.h>
#include <shyft/py/api/expose_container.h>

namespace expose {
  using shyft::srv::model_info;
  using namespace shyft::core;
  using std::string;
  using std::vector;

  void ex_model_info() {
    using py::self;
    py::class_<model_info>("ModelInfo", doc.intro("Provides model-information useful for selection and filtering")())
      .def(py::init<int64_t, string const &, utctime, string>(
        (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("created"), py::arg("json") = string{""}),
        doc.intro("Constructs ModelInfo with the supplied parameters")
          .parameters()
          .parameter("id", "int", "the unique model id")
          .parameter("name", "str", "any useful name or description")
          .parameter("created", "time", "time of creation")
          .parameter("json", "str", "extra information, preferably a valid json")()))
      .def_readwrite("id", &model_info::id, "int: the unique model id, can be used to retrieve the real model")
      .def_readwrite("name", &model_info::name, "str: any useful name or description")
      .def_readwrite("created", &model_info::created, "time: the time of creation, or last modification of the model")
      .def_readwrite(
        "json",
        &model_info::json,
        "str: a json formatted string to enable scripting and python to store more information")
      .def(self == self)
      .def(self != self);
    // using ModelInfoVector=vector<model_info>;
    expose_vector<model_info>("ModelInfoVector", "A strongly typed list of ModelInfo");
    // py::class_<ModelInfoVector>("ModelInfoVector", "A strongly typed list, vector, of ModelInfo")
    //.def(py::vector_indexing_suite<ModelInfoVector, true>())
    //;
  }
}
