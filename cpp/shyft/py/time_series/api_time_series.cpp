/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/predictions.h>
#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/transform_spline_ts.h>
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/time_series/dd/statistics_ts.h>
#include <shyft/time_series/ice_packing_parameters.h>
#include <shyft/time_series/ice_packing_policy.h>
#include <shyft/time_series/dd/ice_packing_recession_ts.h>

#include <shyft/time_series/dd/compute_ts_vector.h>

#include <shyft/py/api/bindings.h>
#include <numpy/arrayobject.h>
#include <shyft/py/api/numpy_boost_python.hpp>
#include <shyft/py/api/py_convertible.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/scoped_gil.h>

namespace shyft::api {
  using namespace shyft::time_series;

  /**
   * @brief TsFactor provides time-series creation function
   * @details
   * using supplied primitives like vector of double, start,
   * delta-t, n etc.
   * @note: deprecated, replace by generic ts library
   */
  struct TsFactory {

    apoint_ts create_point_ts(
      int n,
      utctime tStart,
      utctimespan dt,
      std::vector<double> const &values,
      ts_point_fx interpretation = POINT_INSTANT_VALUE) {
      return apoint_ts(time_axis::fixed_dt(tStart, dt, n), values, interpretation);
    }

    apoint_ts create_time_point_ts(
      utcperiod period,
      std::vector<utctime> const &times,
      std::vector<double> const &values,
      ts_point_fx interpretation = POINT_INSTANT_VALUE) {
      if (times.size() == values.size() + 1) {
        return apoint_ts(time_axis::point_dt(times), values, interpretation);
      } else if (times.size() == values.size()) {
        auto tx(times);
        tx.push_back(period.end > times.back() ? period.end : times.back() + utctimespan(1));
        return apoint_ts(time_axis::point_dt(tx), values, interpretation);
      } else {
        throw std::runtime_error("create_time_point_ts times and values arrays must have corresponding count");
      }
    }
  };
}

namespace expose {
  using std::shared_ptr;
  using std::string;
  using std::runtime_error;
  using std::vector;
  using std::map;
  using std::make_shared;
  using std::int64_t;

  using namespace shyft;
  using namespace shyft::core;

  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::quantile_map_forecast;
  using shyft::time_series::dd::rts_t;
  using shyft::time_series::dd::nash_sutcliffe;
  using namespace shyft::time_series::dd;
  using py::self; // considered ok
  using gta_t = shyft::time_axis::generic_dt;
  extern gta_t time_axis_merge(gta_t const &a, gta_t const &b);

  ats_vector quantile_map_forecast_5(
    vector<ats_vector> const &forecast_set,
    vector<double> const &set_weights,
    ats_vector const &historical_data,
    shyft::time_series::dd::gta_t const &time_axis,
    utctime interpolation_start) {
    return quantile_map_forecast(forecast_set, set_weights, historical_data, time_axis, interpolation_start);
  }

  ats_vector quantile_map_forecast_6(
    vector<ats_vector> const &forecast_set,
    vector<double> const &set_weights,
    ats_vector const &historical_data,
    shyft::time_series::dd::gta_t const &time_axis,
    utctime interpolation_start,
    utctime interpolation_end) {
    return quantile_map_forecast(
      forecast_set, set_weights, historical_data, time_axis, interpolation_start, interpolation_end);
  }

  ats_vector quantile_map_forecast_7(
    vector<ats_vector> const &forecast_set,
    vector<double> const &set_weights,
    ats_vector const &historical_data,
    shyft::time_series::dd::gta_t const &time_axis,
    utctime interpolation_start,
    utctime interpolation_end,
    bool interpolated_quantiles) {
    return quantile_map_forecast(
      forecast_set,
      set_weights,
      historical_data,
      time_axis,
      interpolation_start,
      interpolation_end,
      interpolated_quantiles);
  }

  static string ts_stringify(apoint_ts const &ats) {
    return ats.stringify();
  }

  static void expose_core_ts_vector() {
    typedef vector<rts_t> core_ts_vector;
    py::class_<core_ts_vector>("CoreTsVector", doc.intro("A raw vector of core time-series.").intro("")())
      .def(py::vector_indexing_suite<core_ts_vector>());
  }

  /** ats_vector python expose helper for constructor variants so that
   * it's easy for to use. We can't use py_convertible since
   * python get confused by 3*tsv etc. (trying to convert 3 -> tsvector)
   */
  struct ats_vector_ext {
    static ats_vector *create_default() {
      return new ats_vector{};
    }

    static ats_vector *create_from_clone(ats_vector const &c) {
      return new ats_vector(c);
    }

    static ats_vector *create_from_ts_list(py::list tsl) {
      size_t n = py::len(tsl);
      if (n == 0)
        return new ats_vector{};
      auto r = new ats_vector();
      r->reserve(n);
      for (size_t i = 0; i < n; ++i) {
        py::object ts = tsl[i];
        py::extract<apoint_ts> xts(ts);
        if (xts.check()) {
          r->push_back(xts());
        } else {
          throw runtime_error("Failed to convert " + to_string(i) + " element to TimeSeries");
        }
      }
      return r;
    }

    static ats_vector compute(ats_vector const &c) {
      shyft::pyapi::scoped_gil_release gil;
      return ats_vector{shyft::time_series::dd::deflate_ts_vector<apoint_ts>(c)};
    }

    /** extract as table, using supplied time-axis, suitable for bokeh table apps etc.
     * returns vector of vector<double>
     * where:
     *  r[0] is the timepoints, time-zoned to specified cal, and multiplied with time_scale
     *  r[1..n] is the values of time-series that have it's value exactly at specified time of ta.
     */
    static vector<vector<double>> extract_as_table(ats_vector const &c, calendar const &cal, double time_scale) {
      shyft::pyapi::scoped_gil_release gil;
      vector<vector<double>> r;
      if (c.size() == 0)
        return r;
      gta_t ta;
      for (auto const &ts : c)
        ta = time_axis_merge(ta, ts.time_axis());

      r.reserve(c.size() + 1); // time-axis + each of the values
      vector<double> t;
      t.reserve(ta.size());
      auto tz = cal.tz_info;
      auto utz = [&tz](utctime t) {
        return tz ? tz->utc_offset(t) : utctime_0;
      };

      for (size_t i = 0; i < ta.size(); ++i)
        t.push_back(time_scale * (to_seconds(ta.time(i) + utz(ta.time(i)))));
      r.emplace_back(std::move(t));

      for (size_t i = 0; i < c.size(); ++i) {
        vector<double> vv;
        vv.reserve(ta.size());
        size_t ix = 0;
        auto const &ts = c[i];
        for (size_t j = 0; j < ta.size(); ++j) {
          auto tj = ta.time(j);
          ix = ts.index_of(tj, ix);
          if (ix != std::string::npos && ts.time(ix) == tj) {
            vv.push_back(ts.value(ix));
          } else
            vv.push_back(shyft::nan);
        }
        r.emplace_back(std::move(vv));
      }

      return r;
    }
  };

  static void expose_ats_vector() {
    using namespace shyft::api;
    using shyft::time_series::dd::pow;
    typedef ats_vector (ats_vector::*m_double)(double) const;
    typedef ats_vector (ats_vector::*m_ts)(apoint_ts const &) const;
    typedef ats_vector (ats_vector::*m_tsv)(ats_vector const &) const;
    typedef ats_vector (ats_vector::*m_na)() const;

    py::class_<ats_vector> cv(
      "TsVector",
      doc.intro("A vector, as in strongly typed list, array, of time-series that supports ts-math operations.")
        .intro("You can create a TsVector from a list, or list generator of type TimeSeries.")
        .pure("TsVector is to TimeSeries that a numpy array is to numbers, see also")
        .ref_class("TimeSeries")
        .intro("\n\n")
        .intro("Math operations and their types transformations:")
        .intro("")
        .intro("    * `number bin_op ts_vector -> ts_vector`")
        .intro("    * `ts_vector bin_op ts_vector -> ts_vector`")
        .intro("    * `ts bin_op ts_vector -> ts_vector`")
        .intro("\nwhere bin_op is any of `(*,/,+,-)` and explicit forms of  binary functions like pow,log,min,max.")
        .intro("\n")
        .pure("In addition these are also available:")
        .ref_meth("average")
        .ref_meth("integral")
        .ref_meth("accumulate")
        .ref_meth("time_shift")
        .ref_meth("percentiles")
        .intro("")
        .intro("")
        .intro("All operation return a **new** object, usually a ts-vector, containing the resulting expressions")
        .intro("\n")
        .intro("Examples:\n")
        .intro(">>> import numpy as np")
        .intro(">>> from shyft.time_series import TsVector,Calendar,deltahours,TimeAxis,TimeSeries,POINT_AVERAGE_VALUE "
               "as fx_avg")
        .intro(">>>")
        .intro(">>> utc = Calendar()  # ensure easy consistent explicit handling of calendar and time")
        .intro(">>> ta1 = TimeAxis(utc.time(2016, 9, 1, 8, 0, 0), deltahours(1), 10)  # create a time-axis for ts1")
        .intro(">>> ts1 = TimeSeries(ta1, np.linspace(0, 10, num=len(ta)), fx_avg)")
        .intro(">>> ta2 = TimeAxis(utc.time(2016, 9, 1, 8, 30, 0), deltahours(1), 5)  # create a time-axis to ts2")
        .intro(">>> ts2 = TimeSeries(ta2, np.linspace(0,  1, num=len(ta)), fx_avg)")
        .intro(">>> tsv = TsVector([ts1, ts2]) # create ts vector from list of time-series")
        .intro(">>> c = tsv + tsv*3.0  # c is now an expression, time-axis is the overlap of a and b, lazy evaluation")
        .intro(">>> c_values = c[0].values.to_numpy()  # compute and extract the values of the ith (here: 0) "
               "time-series, as numpy array")
        .intro(">>>")
        .intro(">>> # Calculate data for new time-points")
        .intro(">>> value_1 = tsv(utc.time(2016, 9, 1, 8, 30)) # calculates value at a given time")
        .intro(">>> ta_target = TimeAxis(utc.time(2016, 9, 1, 7, 30), deltahours(1), 12)  # create a target time_axis")
        .intro(">>> tsv_new = tsv.average(ta_target) # new ts-vector with values on target time_axis")
        .intro(">>> ts0_val = tsv_new[0].values.to_numpy() # access values of the ith (here: 0) time-series as a numpy "
               "array")
        .intro(">>>\n\n")(),
      py::no_init);
    expose::detail::expose_container_methods(cv, true, true);
    cv.def(py::vector_indexing_suite<ats_vector>())
      .def(
        "__init__",
        py::make_constructor(&ats_vector_ext::create_default, py::default_call_policies()),
        doc.intro("Create an empty TsVector")())
      .def(
        "__init__",
        py::make_constructor(&ats_vector_ext::create_from_clone, py::default_call_policies(), (py::arg("cloneme"))),
        doc.intro("Create a shallow clone of  the TsVector")
          .parameters()
          .parameter("cloneme", "TsVector", "The TsVector to be cloned")())
      .def(
        "__init__",
        py::make_constructor(&ats_vector_ext::create_from_ts_list, py::default_call_policies(), (py::arg("ts_list"))),
        doc.intro("Create a TsVector from a python list of TimeSeries")
          .parameters()
          .parameter("ts_list", "List[TimeSeries]", "A list of TimeSeries")())
      .def(
        "values_at",
        &ats_vector::values_at_time,
        (py::arg("self"), py::arg("t")),
        doc.intro("Computes the value at specified time t for all time-series")
          .parameters()
          .parameter("t", "utctime", "seconds since epoch 1970 UTC")())
      .def(
        "values_at",
        &ats_vector::values_at_time_i,
        (py::arg("self"), py::arg("t")),
        doc.intro("Computes the value at specified time t for all time-series")
          .parameters()
          .parameter("t", "int", "seconds since epoch 1970 UTC")())
      .def(
        "percentiles",
        &ats_vector::percentiles,
        (py::arg("self"), py::arg("time_axis"), py::arg("percentiles")),
        doc.intro("\nCalculate the percentiles of all timeseries.")
          .intro("over the specified time-axis. The definition is equal to e.g. NIST R7, excel, and in R.")
          .intro("The time-series point_fx interpretation is used when performing")
          .intro("the true-average over the time_axis periods.")
          .intro("This functions works on bound expressions, for unbound expressions, use the `DtsClient.percentiles`.")
          .pure("\nSee also")
          .ref_meth("DtsClient.percentiles")
          .intro("if you want to evaluate percentiles of an *unbound* expression.")
          .parameters()
          .parameter(
            "percentiles",
            "IntVector",
            "A list of numbers,like `[ 0, 25,50,-1,75,100]` will return 6 time-series. "
            "Number with special sematics are: `-1 -> arithmetic average`,"
            " `-1000 -> min extreme value`"
            " `+1000 -> max extreme value`")
          .parameter("time_axis", "TimeAxis", "The time-axis used when applying true-average to the time-series")
          .returns(
            "calculated_percentiles",
            "TsVector",
            "Time-series list with evaluated percentile results, same length as input")())
      .def(
        "percentiles",
        &ats_vector::percentiles_f,
        (py::arg("self"), py::arg("time_axis"), py::arg("percentiles")),
        doc.intro("Calculate the percentiles of the timeseries. ")
          .intro("over the specified time-axis. The definition is equal to e.g. NIST R7, excel, and in R.")
          .intro("The time-series point_fx interpretation is used when performing")
          .intro("the true-average over the time_axis periods.")
          .intro("This functions works on bound expressions, for unbound expressions, use the `DtsClient.percentiles`.")
          .pure("\nSee also")
          .ref_meth("DtsClient.percentiles")
          .intro("if you want to evaluate percentiles of an *unbound* expression.")
          .parameters()
          .parameter(
            "percentiles",
            "IntVector",
            "A list of numbers,[ 0, 25,50,-1,75,100] will return 6 time-series,`-1 -> arithmetic average`, `-1000 -> "
            "min extreme value`, ` +1000 max extreme value`")
          .parameter(
            "time_axis", "TimeAxisFixedDeltaT", "The time-axis used when applying true-average to the time-series")
          .returns(
            "calculated_percentiles",
            "TsVector",
            "Time-series list with evaluated percentile results, same length as input")())
      .def(
        "value_range",
        &ats_vector::value_range,
        (py::arg("self"), py::arg("p")),
        doc.intro("Computes min and max of all non-nan values in the period for bound expressions.")
          .parameters()
          .parameter("p", "UtcPeriod", "")
          .returns(
            "values",
            "DoubleVectorVector",
            "Resulting [min_value, max_value]. If all values are equal, min = max = the_value")())
      .def(
        "slice",
        &ats_vector::slice,
        (py::arg("self"), py::arg("indexes")),
        doc.intro("returns a slice of self, specified by indexes")
          .parameters()
          .parameter(
            "indexes", "IntVector", "the indicies to pick out from self, if indexes is empty, then all is returned")
          .returns("slice", "TsVector", "a new TsVector, with content according to indexes specified")())
      .def(
        "abs",
        &ats_vector::abs,
        (py::arg("self")),
        doc.intro("create a new ts-vector, with all members equal to abs(py::self")
          .returns("tsv", "TsVector", "a new TsVector expression, that will provide the abs-values of self.values")())

      .def(
        "average",
        &ats_vector::average,
        (py::arg("self"), py::arg("ta")),
        doc.intro("create a new vector of ts that is the true average of self")
          .intro("over the specified time-axis ta.")
          .parameters()
          .parameter("ta", "TimeAxis", "time-axis that specifies the periods where true-average is applied")
          .returns("tsv", "TsVector", "a new time-series expression, that will provide the true-average when requested")
          .notes()
          .note("the self point interpretation policy is used when calculating the true average")())
      .def(
        "integral",
        &ats_vector::integral,
        (py::arg("self"), py::arg("ta")),
        doc.intro("create a new vector of ts that is the true integral of self")
          .intro("over the specified time-axis ta.")
          .intro("defined as integral of the non-nan part of each time-axis interval")
          .parameters()
          .parameter("ta", "TimeAxis", "time-axis that specifies the periods where true-integral is applied")
          .returns(
            "tsv", "TsVector", "a new time-series expression, that will provide the true-integral when requested")
          .notes()
          .note("the self point interpretation policy is used when calculating the true average")())
      .def(
        "accumulate",
        &ats_vector::accumulate,
        (py::arg("self"), py::arg("ta")),
        doc.intro("create a new  vector of time-series where the vaue of each i-th element is computed as:")
          .intro("`integral f(t) *dt, from t0..ti`")
          .intro("given the specified time-axis ta and point interpretation.")
          .intro("")
          .parameters()
          .parameter("ta", "TimeAxis", "time-axis that specifies the periods where accumulated integral is applied")
          .returns(
            "tsv", "TsVector", "a new time-series expression, that will provide the accumulated values when requested")
          .notes()
          .pure("\n    Has a point-instant interpretation, see also note in")
          .ref_meth("TimeSeries.accumulate")
          .pure("for possible consequences")())
      .def(
        "derivative",
        &ats_vector::derivative,
        (py::arg("self"), py::arg("method") = derivative_method::default_diff),
        doc.intro("create a new  vector of ts where each i'th element is the")
          .intro("`derivative of f(t)`")
          .parameters()
          .parameter("method", "derivative_method", "what derivative_method variant to use")
          .returns("tsv", "TsVector", "where each member is the derivative of the source")())

      .def(
        "time_shift",
        &ats_vector::time_shift,
        (py::arg("self"), py::arg("delta_t")),
        doc.intro("create a new vector of ts that is a the time-shifted  version of self")
          .parameters()
          .parameter("delta_t", "time", "number of seconds to time-shift, positive values moves forward")
          .returns("tsv", "TsVector", "a new time-series, that appears as time-shifted version of self")())
      .def(
        "use_time_axis_from",
        &ats_vector::use_time_axis_from,
        (py::arg("self"), py::arg("other")),
        doc.pure("Create a new ts-vector applying")
          .ref_meth("TimeSeries.use_time_axis_from")
          .intro(" on each member")
          .intro("")
          .parameters()
          .parameter("other", "TimeSeries", "time-series that provides the wanted time-axis")
          .returns("tsv", "TsVector", "time-series vector, where each element have time-axis from other")())
      .def(
        "repeat",
        &ats_vector::repeat,
        (py::arg("self"), py::arg("repeat_time_axis")),
        doc.intro("Repeat all time-series over the given repeat_time_axis periods")
          .parameters()
          .parameter(
            "repeat_time_axis", "TimeAxis", "A time-axis that have the coarse repeat interval, like YEAR or similar")
          .returns("tsv", "TsVector", "time-series vector, where each element is repeated according to parameter")())
      .def(
        "extend_ts",
        &ats_vector::extend_ts,
        (py::arg("ts"),
         py::arg("split_policy") = extend_ts_split_policy::EPS_LHS_LAST,
         py::arg("fill_policy") = extend_ts_fill_policy::EPF_NAN,
         py::arg("split_at") = utctime(seconds(0)),
         py::arg("fill_value") = shyft::nan),
        doc.intro("create a new ats_vector where all time-series are extended by ts")
          .parameters()
          .parameter("ts", "TimeSeries", "time-series to extend each time-series in self with")
          .parameter("split_policy", "extend_ts_split_policy", "policy determining where to split between self and ts")
          .parameter(
            "fill_policy", "extend_ts_fill_policy", "policy determining how to fill any gap between self and ts")
          .parameter("split_at", "utctime", "time at which to split if split_policy == EPS_VALUE")
          .parameter("fill_value", "float", "value to fill any gap with if fill_policy == EPF_FILL")
          .returns(
            "new_ts_vec",
            "TsVector",
            "a new time-series vector where all time-series in self have been extended by ts")())
      .def(
        "extend_ts",
        &ats_vector::extend_vec,
        (py::arg("ts"),
         py::arg("split_policy") = extend_ts_split_policy::EPS_LHS_LAST,
         py::arg("fill_policy") = extend_ts_fill_policy::EPF_NAN,
         py::arg("split_at") = utctime(seconds(0)),
         py::arg("fill_value") = shyft::nan),
        doc.intro("create a new ats_vector where all ts' are extended by the matching ts from ts_vec")
          .parameters()
          .parameter("ts_vec", "TsVector", "time-series vector to extend time-series in self with")
          .parameter("split_policy", "extend_ts_split_policy", "policy determining where to split between self and ts")
          .parameter(
            "fill_policy", "extend_ts_fill_policy", "policy determining how to fill any gap between self and ts")
          .parameter("split_at", "utctime", "time at which to split if split_policy == EPS_VALUE")
          .parameter("fill_value", "float", "value to fill any gap with if fill_policy == EPF_FILL")
          .returns(
            "new_ts_vec",
            "TsVector",
            "a new time-series vector where all time-series in self have been extended by the corresponding "
            "time-series in ts_vec")())
      .def(
        "min", (m_double) &ats_vector::min, (py::arg("self"), py::arg("number")), "returns min of vector and a number")
      .def("min", (m_ts) &ats_vector::min, (py::arg("self"), py::arg("ts")), "returns min of ts-vector and a ts")
      .def(
        "min",
        (m_tsv) &ats_vector::min,
        (py::arg("self"), py::arg("tsv")),
        "returns min of ts-vector and another ts-vector")
      .def(
        "max", (m_double) &ats_vector::max, (py::arg("self"), py::arg("number")), "returns max of vector and a number")
      .def("max", (m_ts) &ats_vector::max, (py::arg("self"), py::arg("ts")), "returns max of ts-vector and a ts")
      .def(
        "max",
        (m_tsv) &ats_vector::max,
        (py::arg("self"), py::arg("tsv")),
        "returns max of ts-vector and another ts-vector")
      .def(
        "pow",
        (m_double) &ats_vector::pow,
        (py::arg("self"), py::arg("number")),
        "returns TsVector pow(py::self,number)")
      .def("pow", (m_ts) &ats_vector::pow, (py::arg("self"), py::arg("ts")), "returns TsVector pow(py::self,ts)")
      .def("pow", (m_tsv) &ats_vector::pow, (py::arg("self"), py::arg("tsv")), "returns TsVector pow(py::self,tsv)")
      .def("log", (m_na) &ats_vector::log, (py::arg("self")), "returns TsVector log(py::self)")
      .def("sum", &ats_vector::sum, (py::arg("self")), "returns sum of all ts in TsVector as ts as in reduce(add,..))")
      .def(
        "forecast_merge",
        &ats_vector::forecast_merge,
        (py::arg("self"), py::arg("lead_time"), py::arg("fc_interval")),
        doc.intro("merge the forecasts in this vector into a time-series that is constructed")
          .intro("taking a slice of length fc_interval starting lead_time into each of the forecasts")
          .intro("of this time-series vector.")
          .intro("The content of the vector should be ordered in forecast-time, each entry at least")
          .intro("fc_interval separated from the previous.")
          .intro("If there is missing forecasts (larger than fc_interval between two forecasts) this is")
          .intro("automagically repaired using extended slices from the existing forecasts")
          .parameters()
          .parameter("lead_time", "int", "start slice number of seconds from t0 of each forecast")
          .parameter(
            "fc_interval",
            "int",
            "length of each slice in seconds, and thus also gives the forecast-interval separation")
          .returns("merged time-series", "TimeSeries", "A merged forecast time-series")())
      .def(
        "nash_sutcliffe",
        &ats_vector::nash_sutcliffe,
        (py::arg("self"), py::arg("observation_ts"), py::arg("lead_time"), py::arg("delta_t"), py::arg("n")),
        doc.intro("Computes the nash-sutcliffe (wiki nash-sutcliffe) criteria between the")
          .intro("observation_ts over the slice of each time-series in the vector.")
          .intro("The slice for each ts is specified by the lead_time, delta_t and n")
          .intro("parameters. The function is provided to ease evaluation of forecast performance")
          .intro("for different lead-time periods into each forecast.")
          .intro("The returned value range is 1.0 for perfect match -oo for no match, or nan if observations is "
                 "constant or data missing.")
          .intro("See also nash_sutcliffe_goal_function")
          .parameters()
          .parameter("observation_ts", "TimeSeries", "the observation time-series")
          .parameter("lead_time", "int", "number of seconds lead-time offset from each ts .time(0)")
          .parameter(
            "delta_t", "int", "delta-time seconds to average as basis for n.s. simulation and observation values")
          .parameter("n", "int", "number of time-steps of length delta_t to slice out of each forecast/simulation ts")
          .returns(
            "nash-sutcliffe value",
            "double",
            "the nash-sutcliffe criteria evaluated over all time-series in the TsVector for the specified lead-time, "
            "delta_t and number of elements")())
      .def(
        "average_slice",
        &ats_vector::average_slice,
        (py::arg("self"), py::arg("lead_time"), py::arg("delta_t"), py::arg("n")),
        doc.intro("Returns a ts-vector with the average time-series of the specified slice")
          .intro("The slice for each ts is specified by the lead_time, delta_t and n")
          .intro("parameters. ")
          .intro("See also nash_sutcliffe,forecast_merge")
          .parameters()
          .parameter("lead_time", "int", "number of seconds lead-time offset from each ts .time(0)")
          .parameter(
            "delta_t", "int", "delta-time seconds to average as basis for n.s. simulation and observation values")
          .parameter("n", "int", "number of time-steps of length delta_t to slice out of each forecast/simulation ts")
          .returns("ts_vector_sliced", "TsVector", "a ts-vector with average ts of each slice specified.")()

          )
      .def(
        "inside",
        &ats_vector::inside,
        (py::arg("self"),
         py::arg("min_v"),
         py::arg("max_v"),
         py::arg("nan_v") = shyft::nan,
         py::arg("inside_v") = 1.0,
         py::arg("outside_v") = 0.0),
        doc
          .intro("Create an inside min-max range ts-vector, that transforms the point-values\n"
                 "that falls into the half open range [min_v .. max_v > to \n"
                 "the value of inside_v(default=1.0), or outside_v(default=0.0),\n"
                 "and if the value considered is nan, then that value is represented as nan_v(default=nan)\n"
                 "You would typically use this function to form a true/false series (inside=true, outside=false)\n")
          .parameters()
          .parameter("min_v", "float", "minimum range, values <  min_v are not inside min_v==NaN means no lower limit")
          .parameter("max_v", "float", "maximum range, values >= max_v are not inside. max_v==NaN means no upper limit")
          .parameter("nan_v", "float", "value to return if the value is nan")
          .parameter("inside_v", "float", "value to return if the ts value is inside the specified range")
          .parameter("outside_v", "float", "value to return if the ts value is outside the specified range")
          .returns(
            "inside_tsv", "TsVector", "New TsVector where each element is an evaluated-on-demand inside time-series")())

      .def(
        "transform",
        +[](ats_vector const &me, numpy_boost<double, 2> const &pyarray, interpolation_scheme scheme) {
          return me.transform(xy_point_curve(pyarray), scheme);
        },
        (py::arg("self"), py::arg("points"), py::arg("method")),
        doc.intro("Create a transformed ts-vector, having values taken from pointwise function evaluation.")
          .intro("Function values are determined by interpolating the given points, using the specified method.")
          .intro("Valid method arguments are 'polynomial', 'linear' and 'catmull-rom'.")
          .returns(
            "transform_tsv",
            "TsVector",
            "New TsVector where each element is an evaluated-on-demand transformed time-series.")())
      .def(
        "clone_expression",
        &ats_vector::clone_expr,
        (py::arg("self")),
        doc.intro("create a copy of the ts-expressions, such that all directed unbound parts from")
          .intro("the terminal and upwards is copied.")
          .intro("The parts of the expression-tree that is bound, invariant, is copied by ref.")
          .notes()
          .note("this function is only useful in context where multiple bind/rebind while keeping the expression is "
                "needed.")
          .returns(
            "semantic_clone",
            "TsVector",
            "returns a copy of the ts, such that all unbound parts are copied, all bound parts and terminals are just "
            "shallow copied(referenced)")())
      .def(
        "evaluate",
        &ats_vector_ext::compute,
        (py::arg("self")),
        doc.intro("Evaluates the expressions in TsVector multithreaded,")
          .intro("and returns the resulting TsVector, where all items now")
          .intro("are  concrete terminals, that is, not expressions anymore.")
          .intro("Useful client-side if you have complex large expressions where")
          .intro("all time-series are bound (not symbols)")
          .returns("evaluated_clone", "TsVector", "returns the computed result as a new ts-vector")())
      .def(
        "extract_as_table",
        &ats_vector_ext::extract_as_table,
        (py::arg("self"), py::arg("cal"), py::arg("time_scale")),
        doc.intro("Extract values in the ts-vector as a table, where columns:")
          .intro("| [0]    is the distinct union of all time_scale*(time-points i, + cal.tz_offset(i))")
          .intro("| [1..n] is the value contribution of the i'th ts, nan if no contribution at that time-point")
          .intro("This function primary usage is within visual-layer of the shyft.dashboard package to speed up "
                 "processing.")
          .intro("The semantics and parameters reflects this.")
          .parameters()
          .parameter(
            "cal",
            "Calendar",
            "Calendar to use for tz-offset of each time-point (to resolve bokeh lack of tz-handling)")
          .parameter(
            "time_scale", "float", "time-scale to multiply the time from si-unit [s] to any scaled unit, typically ms")
          .returns("table", "DoubleVectorVector", "A 2d vector where [0] contains time, [1..n] the values")())
      .def(
        "statistics",
        &ats_vector::statistics,
        (py::arg("self"), py::arg("ta"), py::arg("p")),
        doc.intro("create a new vector of ts where each element is ts.statistics(ta,p)")
          .parameters()
          .parameter("ta", "TimeAxis", "time-axis that specifies the periods where true-average is applied")
          .parameter("p", "int", "percentile range [0..100], or statistical_property.AVERAGE|MIN_EXTREME|MAX_EXTREME")
          .returns("tsv", "TsVector", "a new time-series expression, will provide the statistics when requested")())

      // defining vector math-operations goes here
      .def(-py::self)
      .def(py::self * double())
      .def(double() * py::self)
      .def(py::self * py::self)
      .def(apoint_ts() * py::self)
      .def(py::self * apoint_ts())

      .def(py::self / double())
      .def(double() / py::self)
      .def(py::self / py::self)
      .def(apoint_ts() / py::self)
      .def(py::self / apoint_ts())

      .def(py::self + double())
      .def(double() + py::self)
      .def(py::self + py::self)
      .def(apoint_ts() + py::self)
      .def(py::self + apoint_ts())

      .def(py::self - double())
      .def(double() - py::self)
      .def(py::self - py::self)
      .def(apoint_ts() - py::self)
      .def(py::self - apoint_ts())
      .def(operator!(py::self))
      .def(py::self == py::self)
      .def(py::self != py::self);
    // expose min-max functions:
    typedef ats_vector (*f_atsv_double)(ats_vector const &, double b);
    typedef ats_vector (*f_double_atsv)(double b, ats_vector const &a);
    typedef ats_vector (*f_atsv_ats)(ats_vector const &, apoint_ts const &);
    typedef ats_vector (*f_ats_atsv)(apoint_ts const &b, ats_vector const &a);
    typedef ats_vector (*f_atsv_atsv)(ats_vector const &b, ats_vector const &a);

    py::def("min", (f_ats_atsv) min, (py::arg("ts"), py::arg("ts_vector")), "return minimum of ts and ts_vector");
    py::def("min", (f_atsv_ats) min, (py::arg("ts_vector"), py::arg("ts")), "return minimum of ts_vector and ts");
    py::def(
      "min", (f_atsv_double) min, (py::arg("ts_vector"), py::arg("number")), "return minimum of ts_vector and number");
    py::def(
      "min", (f_double_atsv) min, (py::arg("number"), py::arg("ts_vector")), "return minimum of number and ts_vector");
    py::def(
      "min",
      (f_atsv_atsv) min,
      (py::arg("a"), py::arg("b")),
      "return minimum of ts_vectors a and b (requires equal size!)");
    py::def("max", (f_ats_atsv) max, (py::arg("ts"), py::arg("ts_vector")), "return max of ts and ts_vector");
    py::def("max", (f_atsv_ats) max, (py::arg("ts_vector"), py::arg("ts")), "return max of ts_vector and ts");
    py::def(
      "max", (f_atsv_double) max, (py::arg("ts_vector"), py::arg("number")), "return max of ts_vector and number");
    py::def(
      "max", (f_double_atsv) max, (py::arg("number"), py::arg("ts_vector")), "return max of number and ts_vector");
    py::def(
      "max",
      (f_atsv_atsv) max,
      (py::arg("a"), py::arg("b")),
      "return max of ts_vectors a and b (requires equal size!)");

    py::def("pow", (f_ats_atsv) pow, (py::arg("ts"), py::arg("ts_vector")), "return pow(ts,ts_vector)->TsVector");
    py::def("pow", (f_atsv_ats) pow, (py::arg("ts_vector"), py::arg("ts")), "return pow(ts_vector,ts)->TsVector");
    py::def(
      "pow", (f_atsv_double) pow, (py::arg("ts_vector"), py::arg("number")), "return pow(ts_vector,number)->TsVector");
    py::def(
      "pow", (f_double_atsv) pow, (py::arg("number"), py::arg("ts_vector")), "return pow(number,ts_vector)->TsVector");
    py::def(
      "pow", (f_atsv_atsv) pow, (py::arg("a"), py::arg("b")), "return pow(a,b)->TsVector, (requires equal size!)");
    // expose utility functions (dashboard/bokeh)
    // we also need a vector of ats_vector for quantile_map_forecast function

    expose::expose_vector<ats_vector>(
      "TsVectorSet", doc.intro("A set of TsVector").intro("").see_also("quantile_map_forecast,TsVector")());

    auto qm_doc =
      doc.intro("Computes the quantile-mapped forecast from the supplied input.")
        .intro("***TBD:detailed description with references***")
        .parameters()
        .parameter(
          "forecast_sets",
          "TsVectorSet",
          "forecast sets, each of them a TsVector with n forecasts (might differ in size and length)")
        .parameter(
          "set_weights",
          "DoubleVector",
          "a weight for each of the forecast set in forecast-sets,correlated by same index)")
        .parameter("historical_data", "TsVector", "historical time-series that should cover the requested time-axis")
        .parameter("time_axis", "TimeAxis", "the time-axis that the resulting time-series are mapped into")
        .parameter(
          "interpolation_start",
          "int",
          "time where the historical to forecast interpolation should start, 1970 utc seconds since epoch")
        .parameter(
          "interpolation_end", "int", "time where the interpolation should end, if no_utctime, use end of forecast-set")
        .parameter(
          "interpolated_quantiles",
          "bool",
          "whether the quantile values should be interpolated or assigned the values lower than or equal to the "
          "current quantile")
        .returns("qm_forecast", "TsVector", "quantile mapped forecast with the requested time-axis");

    py::def(
      "quantile_map_forecast",
      quantile_map_forecast_5,
      (py::arg("forecast_sets"),
       py::arg("set_weights"),
       py::arg("historical_data"),
       py::arg("time_axis"),
       py::arg("interpolation_start")),
      qm_doc());
    py::def(
      "quantile_map_forecast",
      quantile_map_forecast_6,
      (py::arg("forecast_sets"),
       py::arg("set_weights"),
       py::arg("historical_data"),
       py::arg("time_axis"),
       py::arg("interpolation_start"),
       py::arg("interpolation_end")),
      qm_doc());
    py::def(
      "quantile_map_forecast",
      quantile_map_forecast_7,
      (py::arg("forecast_sets"),
       py::arg("set_weights"),
       py::arg("historical_data"),
       py::arg("time_axis"),
       py::arg("interpolation_start"),
       py::arg("interpolation_end"),
       py::arg("interpolated_quantiles")),
      qm_doc());
  }

#define DEF_STD_TS_STUFF() \
  .def( \
    "point_interpretation", \
    &pts_t::point_interpretation, \
    (py::arg("self")), \
    "returns the point interpretation policy") \
    .def( \
      "set_point_interpretation", \
      &pts_t::set_point_interpretation, \
      (py::arg("self"), py::arg("policy")), \
      "set new policy") \
    .def("value", &pts_t::value, (py::arg("self"), py::arg("i")), "returns the value at the i'th time point") \
    .def("time", &pts_t::time, (py::arg("self"), py::arg("i")), "returns the time at the i'th point") \
    .def("get", &pts_t::get, (py::arg("self"), py::arg("i")), "returns i'th point(t,v)") \
    .def("set", &pts_t::set, (py::arg("self"), py::arg("i"), py::arg("v")), "set the i'th value") \
    .def("fill", &pts_t::fill, (py::arg("self"), py::arg("v")), "fill all values with v") \
    .def("scale_by", &pts_t::scale_by, (py::arg("self"), py::arg("v")), "scale all values by the specified factor v") \
    .def("size", &pts_t::size, (py::arg("self")), "returns number of points") \
    .def( \
      "index_of", \
      (size_t(pts_t::*)(utctime t) const) & pts_t::index_of, \
      (py::arg("self"), py::arg("t")), \
      "return the index of the intervall that contains t, or npos if not found") \
    .def( \
      "total_period", \
      &pts_t::total_period, \
      (py::arg("self")), \
      "returns the total period covered by the time-axis of this time-series") \
    .def("__call__", &pts_t::operator(), (py::arg("self"), py::arg("t")), "return the f(t) value for the time-series")

  template <class TA>
  static void point_ts(char const *ts_type_name, char const *docu) {
    using pts_t = time_series::point_ts<TA>;
    py::class_<pts_t, py::bases<>, shared_ptr<pts_t>, boost::noncopyable>(ts_type_name, docu)
      .def(py::init<const TA &, vector<double> const &, time_series::ts_point_fx>(
        (py::arg("self"), py::arg("ta"), py::arg("v"), py::arg("policy")),
        doc.intro("constructs a new timeseries from timeaxis, points and policy (how the points are to be interpreted, "
                  "instant, or average of the interval)")()))
      .def(py::init<const TA &, double, time_series::ts_point_fx>(
        (py::arg("self"), py::arg("ta"), py::arg("fill_value"), py::arg("policy")),
        doc.intro("constructs a new timeseries from timeaxis, fill-value and policy")())) DEF_STD_TS_STUFF()
      .def_readonly(
        "v",
        &pts_t::v,
        doc.intro("DoubleVector: the point vector<double>, same as .values, kept around for backward compatibility")())
      .def(
        "get_time_axis",
        &pts_t::time_axis,
        (py::arg("self")),
        "returns the time-axis",
        py::return_internal_reference<>()) // have to use func plus init.py fixup due to boost py policy
      ;
  }

  using shyft::core::utctime;
  using shyft::core::seconds;
  BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(point_ts_overloads, shyft::api::TsFactory::create_point_ts, 4, 5);
  BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(time_point_ts_overloads, shyft::api::TsFactory::create_time_point_ts, 3, 4);
  extern utctime x_kwarg_utctime(py::tuple const &args, py::dict const &kwargs, size_t i, char const *kw);

  template <class T>
  static T x_arg(py::tuple const &args, size_t i) {
    if (py::len(args) + 1 < (int) i)
      throw std::runtime_error("missing arg #" + std::to_string(i));
    py::object o = args[i];
    py::extract<T> xtract_arg(o);
    return xtract_arg();
  }

  template <class T>
  static T x_kwarg(py::tuple const &args, py::dict const &kwargs, size_t i, char const *kw) {
    if (py::len(args) > (int) i) {
      return x_arg<T>(args, i);
    }
    if (kwargs.has_key(kw)) {
      py::object o = kwargs[kw];
      py::extract<T> xtract_arg(o);
      return xtract_arg();
    }
    throw std::runtime_error("missing kw arg #" + std::string(kw));
  }

  static vector<utctime>
    x_kwarg_utctime_vector(py::tuple const &args, py::dict const &kwargs, size_t i, char const *kw) {
    py::object o;
    if (py::len(args) > (int) i) {
      o = args[i];
    } else if (kwargs.has_key(kw)) {
      o = kwargs[kw];
    } else {
      throw std::runtime_error("missing kw arg #" + std::string(kw));
    }

    py::extract<vector<utctime>> xtract_utctime_vector(o);
    if (xtract_utctime_vector.check()) {
      return xtract_utctime_vector();
    }
    // TODO: this does not work, since py do a convertible lookup above, that fails
    py::extract<vector<int64_t>> xtract_int64_vector(o);
    if (xtract_int64_vector.check()) {
      auto v = xtract_int64_vector();
      vector<utctime> r;
      r.reserve(v.size());
      for (auto const &vi : v)
        r.emplace_back(seconds(vi));
      return r;
    }
    throw std::runtime_error("Expected UtcTimeVector, or Int64Vector  for kw arg #" + std::string(kw));
  }

  template <class T>
  static T
    x_kwarg_default(py::tuple const &args, py::dict const &kwargs, size_t i, char const *kw, T const &default_value) {
    if (py::len(args) > (int) i) {
      return x_arg<T>(args, i);
    }
    if (kwargs.has_key(kw)) {
      py::object o = kwargs[kw];
      py::extract<T> xtract_arg(o);
      return xtract_arg();
    }
    return default_value;
  }

  using shyft::core::utcperiod;

  /** helper to ensure we can take any time-rep as input args */
  struct ts_factory_ext {

    static shyft::api::TsFactory x_self(py::tuple const &args) {
      if (py::len(args) == 0)
        throw std::runtime_error("self is null in UtcTime");
      py::object self = args[0];
      py::extract<shyft::api::TsFactory> xtract_self(self);
      return xtract_self();
    }

    static py::object create_point_ts(py::tuple const &args, py::dict const &kwargs) {
      auto self = x_self(args);
      size_t n = x_kwarg<int>(args, kwargs, 1, "n");
      auto t = x_kwarg_utctime(args, kwargs, 2, "t");
      auto dt = x_kwarg_utctime(args, kwargs, 3, "dt");
      auto v = x_kwarg<vector<double>>(args, kwargs, 4, "values");
      auto ct = x_kwarg_default<shyft::time_series::ts_point_fx>(
        args, kwargs, 5, "interpretation", shyft::time_series::POINT_INSTANT_VALUE);
      return py::object(self.create_point_ts(n, t, dt, v, ct));
    }

    static py::object create_time_point_ts(py::tuple const &args, py::dict const &kwargs) {
      auto self = x_self(args);
      auto p = x_kwarg<utcperiod>(args, kwargs, 1, "period");
      auto t = x_kwarg_utctime_vector(args, kwargs, 2, "times");
      auto v = x_kwarg<vector<double>>(args, kwargs, 3, "values");
      auto ct = x_kwarg_default<shyft::time_series::ts_point_fx>(
        args, kwargs, 4, "interpretation", shyft::time_series::POINT_INSTANT_VALUE);
      return py::object(self.create_time_point_ts(p, t, v, ct));
    }
  };

  static void TsFactory() {
    py::class_<shyft::api::TsFactory>(
      "TsFactory",
      doc
        .intro("TsFactory is used in specific contexts, to create point time-series that exposes the "
               "ITimeSeriesOfPoint interface, using the internal ts-implementations")
        .intro("This class is intended for internal shyft-use, related to calibration/running etc.")
        .intro("For geneneral time-series, please use TimeSeries() that have plenty well defined constructors")(),
      py::init<>(py::arg("self")))
      .def(
        "create_point_ts",
        raw_function(ts_factory_ext::create_point_ts, 4),
        doc.intro("creates a fixed interval time-series based on input parameters")
          .parameters()
          .parameter("n", "int", "number of points in time-series")
          .parameter("t", "utctime", "start of first point, as seconds since epoch")
          .parameter("dt", "utctime", "interval, in seconds")
          .parameter("values", "DoubleVector", "values as DoubleVector")
          .parameter(
            "interpretation",
            "ts_point_fx",
            "point interpretation, default POINT_INSTANT_VALUE, other value is POINT_AVERAGE_VALUE ")
          .returns("ts", "TimeSeries", "constructed time-series")())
      .def(
        "create_time_point_ts",
        raw_function(ts_factory_ext::create_time_point_ts, 3),
        doc.intro("creates a variable interval time-series based on input parameters")
          .parameters()
          .parameter(
            "period",
            "utcperiod",
            " where .start should be equal to the first point in the supplied time-vector,t, and .end should be the "
            "end-time of the last interval")
          .parameter("times", "UtcTimeVector", "start time of each interval")
          .parameter("values", "DoubleVector", "values as DoubleVector, one for each interval")
          .parameter(
            "interpretation",
            "ts_point_fx",
            "point interpretation, default POINT_INSTANT_VALUE, other value is POINT_AVERAGE_VALUE ")
          .returns("ts", "TimeSeries", "constructed time-series")());
  }

  /** support shyft.dashboard bokeh plotting */
  static vector<vector<double>> ts_to_bokeh_plot_data(
    apoint_ts ts,
    calendar const &c,
    double time_scale,
    bool force_linear,
    bool remove_trailing_nans) {
    vector<double> v, t;
    if (ts.needs_bind() || ts.size() == 0)
      return vector<vector<double>>{t, v};
    // possible ts.evaluate in case of expr.
    auto pfx = ts.point_interpretation();
    auto x = ts.evaluate(); // ensure we deal with expressions fast. it goes to a nullop if no expr
    size_t sz = x.size();
    v = x.values();
    if (remove_trailing_nans) {
      while (!isfinite(v[sz - 1]) && sz > 0)
        sz--;
    }
    v.resize(sz);
    if (sz == 0)
      return vector<vector<double>>{t, v};
    auto tz = c.get_tz_info();
    auto ta = x.time_axis();
    auto bokeh_time = [&tz, time_scale](utctime t) -> double {
      return time_scale * to_seconds(t + tz->utc_offset(t));
    };
    if (force_linear || pfx == time_series::ts_point_fx::POINT_INSTANT_VALUE) {
      t.reserve(sz);
      for (size_t i = 0; i < sz; ++i)
        t.emplace_back(bokeh_time(ta.time(i)));
      return vector<vector<double>>{t, v};
    } else { // we have to double the data to make a stair-case line, double the timepoints.
      vector<double> v2;
      v2.reserve(2 * sz); // need one extra for last step-line
      t.reserve(2 * sz);
      for (size_t i = 0; i < sz; ++i) {
        v2.push_back(v[i]);
        v2.push_back(v[i]);
        auto pi = ta.period(i);
        t.push_back(bokeh_time(pi.start));
        t.push_back(bokeh_time(pi.end));
      }
      return vector<vector<double>>{t, v2};
    }
  }

  static prediction::krls_rbf_predictor apoint_ts_get_krls_predictor(
    apoint_ts const *ats,
    core::utctimespan dt,
    double rbf_gamma,
    double tol,
    std::size_t size) {
    if (ats->needs_bind())
      throw std::runtime_error("cannot get predictor for unbound ts");
    shyft::prediction::krls_rbf_predictor predictor{dt, rbf_gamma, tol, size};
    predictor.train(*ats);
    return predictor;
  }

  /*
  static shyft::time_series::dd::apoint_ts create_from_pattern(std::vector<double> const&v,shyft::core::utctime
  dt,shyft::time_axis::generic_dt const&ta) { return apoint_ts(v,dt,ta);
  }*/
#define REF_COUNT_BUG_FIXED 1

  static void expose_apoint_ts() {
    using namespace shyft::api;
    using shyft::time_series::dd::qac_parameter;
    typedef apoint_ts pts_t;
    typedef pts_t (pts_t::*self_na_t)() const;              // no arg function type
    typedef pts_t (pts_t::*self_dbl_t)(double) const;       // double arg function type
    typedef pts_t (pts_t::*self_ts_t)(pts_t const &) const; // ts arg function type

    self_dbl_t min_double_f = &pts_t::min;
    self_ts_t min_ts_f = &pts_t::min;
    self_dbl_t max_double_f = &pts_t::max;
    self_ts_t max_ts_f = &pts_t::max;
    self_dbl_t pow_double_f = &pts_t::pow;
    self_ts_t pow_ts_f = &pts_t::pow;
    self_na_t log_na_f = &pts_t::log;
    typedef ts_bind_info TsBindInfo;
    py::class_<TsBindInfo>(
      "TsBindInfo",
      doc.intro("TsBindInfo gives information about the time-series and it's binding")
        .intro("represented by encoded string reference")
        .intro("Given that you have a concrete ts,")
        .intro("you can bind that the bind_info.ts")
        .intro("using bind_info.ts.bind()")
        .intro("see also Timeseries.find_ts_bind_info() and Timeseries.bind()")(),
      py::init<>(py::arg("self")))
      .def_readwrite(
        "id",
        &ts_bind_info::reference,
        "str: a unique id/url that identifies a time-series in a ts-database/file-store/service")
      .def_readwrite(
        "ts", &ts_bind_info::ts, "TimeSeries: the ts, provides .bind(another_ts) to set the concrete values");

    typedef vector<TsBindInfo> TsBindInfoVector;
    py::class_<TsBindInfoVector>(
      "TsBindInfoVector",
      doc.intro("A vector of TsBindInfo").intro("see also TsBindInfo")(),
      py::init<>(py::arg("self")))
      .def(py::vector_indexing_suite<TsBindInfoVector>());
    py_api::iterable_converter().from_python<TsBindInfoVector>();

    apoint_ts (apoint_ts::*min_max_check_linear_fill_t)(double, double, utctimespan)
      const = &apoint_ts::min_max_check_linear_fill;
    apoint_ts (apoint_ts::*min_max_check_ts_fill_t)(double, double, utctimespan, apoint_ts const &)
      const = &apoint_ts::min_max_check_ts_fill;
    apoint_ts (apoint_ts::*min_max_check_linear_fill_i)(double, double, int64_t)
      const = &apoint_ts::min_max_check_linear_fill;
    apoint_ts (apoint_ts::*min_max_check_ts_fill_i)(double, double, int64_t, apoint_ts const &)
      const = &apoint_ts::min_max_check_ts_fill;


    py::class_<apoint_ts>("TimeSeries",
                doc.intro("A time-series providing mathematical and statistical operations and functionality.")
                .intro("")
                .intro("A time-series can be an expression, or a concrete point time-series.")
                .intro("All time-series do have a time-axis, values, and a point fx policy.")
                .intro("The value, f(t) outside the time-axis is nan")
                .intro("Operations between time-series, e.g. `a+b`, respects the mathematical `nan op something equals nan`")
                .intro("")
                .intro("The time-series can provide a value for all the intervals, and the point_fx policy")
                .intro("defines how the values should be interpreted:\n")
                .intro("POINT_INSTANT_VALUE(linear):\n")
                .intro("    the point value is valid at the start of the period, linear between points")
                .intro("    -or flat extended value if the next point is nan.")
                .intro("    typical for state-variables, like water-level, temperature measured at 12:00 etc.\n")
                .intro("POINT_AVERAGE_VALUE(stair-case):\n")
                .intro("    the point represents an average or constant value over the period")
                .intro("    typical for model-input and results, precipitation mm/h, discharge m^3/s")
                .intro("\n")
                .intro("Examples:\n")
                .intro(">>> import numpy as np")
                .intro(">>> from shyft.time_series import Calendar,deltahours,TimeAxis,TimeSeries,POINT_AVERAGE_VALUE as fx_avg")
                .intro(">>>")
                .intro(">>> utc = Calendar()  # ensure easy consistent explicit handling of calendar and time")
                .intro(">>> ta = TimeAxis(utc.time(2016, 9, 1, 8, 0, 0), deltahours(1), 10)  # create a time-axis to use")
                .intro(">>> a = TimeSeries(ta, np.linspace(0, 10, num=len(ta)), fx_avg)")
                .intro(">>> b = TimeSeries(ta, np.linspace(0,  1, num=len(ta)), fx_avg)")
                .intro(">>> c = a + b*3.0  # c is now an expression, time-axis is the overlap of a and b, lazy evaluation")
                .intro(">>> c_values = c.values.to_numpy()  # compute and extract the values, as numpy array")
                .intro(">>> c_evaluated=c.evaluate() # computes the expression, return a new concrete point-ts equal to the expression")
                .intro(">>>")
                .intro(">>> # Calculate data for new time-points")
                .intro(">>> value_1 = a(utc.time(2016, 9, 1, 8, 30)) # calculates value at a given time")
                .intro(">>> ta_target = TimeAxis(utc.time(2016, 9, 1, 7, 30), deltahours(1), 12)  # create a target time_axis")
                .intro(">>> ts_new = a.average(ta_target) # new time-series with values on ta_target")
                .intro(">>>\n\n")
                .pure("TimeSeries can also be symbolic, that is, have urls, that is resolved later, serverside using the ")
                .ref_class("DtsServer")
                .intro("")
                .intro("The TimeSeries functionality includes:\n\n")
                .pure("    * **construction**: TimeSeries(time-axis,values,point_interpretation), TimeSeries(ts_url), TimeSeries(ts_url,ts_fragment) ")
                .ref_func("create_from_pattern")
                .intro("")
                .pure("    * **mutating points**:")
                .ref_meth("set")
                .ref_meth("fill")
                .ref_meth("scale_by")
                .ref_meth("merge_points")
                .intro("")
                .pure("    * **combining/extending**:")
                .ref_meth("extend")
                .ref_meth("use_time_axis_from")
                .intro("")
                .pure("    * **resampling**:")
                .ref_meth("average")
                .ref_meth("accumulate")
                .ref_meth("time_shift")
                .ref_meth("use_time_axis_from")
                .intro("")
                .pure("    * **f(x)**:")
                .ref_meth("transform")
                .ref_meth("abs")
                .ref_meth("derivative")
                .ref_meth("integral")
                .ref_meth("min")
                .ref_meth("max")
                .ref_meth("pow")
                .ref_meth("log")
                .intro("")
                .pure("    * **boolean f(x)**:")
                .ref_meth("inside")
                .intro(", creates mask(1.0,0.0 series) you can use for math/filtering expressions")
                .pure("    * **statistics**:")
                .ref_meth("statistics")
                .ref_meth("kling_gupta")
                .ref_meth("nash_sutcliffe")
                .intro("")
                .pure("    * **filtering**:")
                .ref_meth("convolve_w")
                .ref_meth("krls_interpolation")
                .intro("")
                .pure("    * **quality and correction**:")
                .ref_meth("quality_and_self_correction")
                .ref_meth("quality_and_ts_correction")
                .intro(", min-max limits, replace by surrounding points or replacement ts")
                .pure("    * **bit-encoded**:")
                .ref_meth("decode")
                .intro("")
                .pure("    * **stacking and percentiles**:")
                .ref_meth("stack")
                .intro("")
                .pure("    * **n-ary operations**:")
                .ref_meth("TsVector.sum")
                .ref_meth("TsVector.forecast_merge")
                .intro(", operations on TsVector that results in a TimeSeries")
                .pure("    * **hydrology domain**:")
                .ref_meth("rating_curve")
                .ref_meth("bucket_to_hourly")
                .ref_meth("ice_packing")
                .ref_meth("ice_packing_recession")
                .ref_func("shyft.time_series.create_glacier_melt_ts_m3s")
                .intro("")
                .intro("")
                .pure("Other useful classes to look at:")
                .ref_class("TimeAxis")
                .pure(",")
                .ref_class("Calendar")
                .pure(",")
                .ref_class("TsVector")
                .pure(",")
                .ref_class("point_interpretation_policy")
                .intro("")
                .intro("\n\n")
                .intro("Please check the extensive test suite, notebooks, examples and time_series for usage.")()
                ,
                py::init<>( (py::arg("self")), doc.intro("constructs and empty time-series")())
            )


            .def(py::init< time_axis::generic_dt const &,  std::vector<double> const &, time_series::ts_point_fx >(
                (py::arg("self"),py::arg("ta"), py::arg("values"), py::arg("point_fx")),
                doc.intro("construct a timeseries time-axis ta, corresponding values and point interpretation policy point_fx")()
                )
            )
            //..

            .def(py::init< time_axis::generic_dt const &, double, time_series::ts_point_fx >(
                (py::arg("self"),py::arg("ta"), py::arg("fill_value"), py::arg("point_fx")),
                doc.intro("construct a time-series with time-axis ta, specified fill-value, and point interpretation policy point_fx")()
                )
            )

            .def(py::init< time_axis::fixed_dt const &,  std::vector<double> const &, time_series::ts_point_fx >(
                (py::arg("self"),py::arg("ta"),py::arg("values"), py::arg("point_fx")),
                doc.intro("construct a timeseries timeaxis ta with corresponding values, and point interpretation policy point_fx")()
                )
            )
            .def(py::init< time_axis::fixed_dt const &, double, time_series::ts_point_fx >(
                (py::arg("self"),py::arg("ta"),py::arg("fill_value"),py::arg("point_fx")),
                doc.intro("construct a timeseries with fixed-delta-t time-axis ta, specified fill-value, and point interpretation policy point_fx")()
                )
            )

            .def(py::init< time_axis::point_dt const &,  std::vector<double> const &, time_series::ts_point_fx >(
                (py::arg("self"),py::arg("ta"), py::arg("values"), py::arg("point_fx")),
                doc.intro("construct a time-series with a point-type time-axis ta, corresponding values, and point-interpretation point_fx")()
                )
            )
            .def(py::init< time_axis::point_dt const &, double, time_series::ts_point_fx>(
                (py::arg("self"),py::arg("ta"),py::arg("fill_value"), py::arg("point_fx")),
                doc.intro("construct a time-series with a point-type time-axis ta, specified fill-value, and point-interpretation point_fx")()
                )
            )
            .def(py::init< rts_t  const &>(
                (py::arg("self"),py::arg("core_result_ts")),
                doc.intro("construct a time-series from a shyft core time-series, to ease working with core-time-series in user-interface/scripting")()
                )
            )
            .def(py::init< apoint_ts const &>(
                (py::arg("self"),py::arg("clone")),
                doc.intro("creates a shallow copy of the clone time-series")()
                )
            )
#if REF_COUNT_BUG_FIXED
        // causes boost py binding to fail on refcount for ta on the *other* constructors
            .def(py::init< vector<double> const &, utctimespan, time_axis::generic_dt const& >(
                (py::arg("self"),py::arg("pattern"), py::arg("dt"), py::arg("ta")),
                doc.intro("construct a repeated pattern time-series given a equally spaced dt pattern and a time-axis ta")
                .parameters()
                .parameter("pattern","DoubleVector","a list of numbers giving the pattern")
                .parameter("dt","int","number of seconds between each of the pattern-values")
                .parameter("ta","TimeAxis","time-axis that forms the resulting time-series time-axis")()
                )
            )
#endif
            .def(py::init< vector<double> const &, utctimespan,utctime,  time_axis::generic_dt const &>(
                (py::arg("self"),py::arg("pattern"), py::arg("dt"),py::arg("t0"),py::arg("ta")),
                doc.intro("construct a time-series given a equally spaced dt pattern, starting at t0, and a time-axis ta")()
                )
            )

            .def(py::init<std::string>(
                (py::arg("self"),py::arg("ts_id")),
                doc.intro("constructs a bind-able ts,")
                .intro("providing a symbolic possibly unique id that at a later time")
                .intro("can be bound, using the .bind(ts) method to concrete values")
                .intro("if the ts is used as ts, like size(),.value(),time() before it")
                .intro("is bound, then a runtime-exception is raised")
                .parameters()
                .parameter("ts_id","str","url-like identifier for the time-series,notice that shyft://<container>/<path> is for shyft-internal store")()
                )
            )
            .def(py::init<std::string, apoint_ts const &>(
                (py::arg("self"),py::arg("ts_id"),py::arg("bts")),
                doc.intro("constructs a ready bound ts,")
                .intro("providing a symbolic possibly unique id that at a later time")
                .intro("can be used to correlate with back-end store\n")
                .parameters()
                .parameter("ts_id","str","url-type of id, notice that shyft://<container>/<path> is for shyft-internal store")
                .parameter("bts","TimeSeries","A time-series, that is either a concrete ts, or an expression that can be evaluated to form a concrete ts")()
                )
            )
#if !REF_COUNT_BUG_FIXED
            .def("create_from_pattern",&create_from_pattern,
                (py::arg("pattern"), py::arg("dt"), py::arg("ta")),
                doc.intro("construct a repeated pattern time-series given a equally spaced dt pattern and a time-axis ta")
                .parameters()
                .parameter("pattern","DoubleVector","a list of numbers giving the pattern")
                .parameter("dt","int","number of seconds between each of the pattern-values")
                .parameter("ta","TimeAxis","time-axis that forms the resulting time-series time-axis")()

            ).staticmethod("create_from_pattern")
#endif
            .def("ts_id",&apoint_ts::id,(py::arg("self")),
                doc.intro("returns ts_id of symbolic ts, or empty string if not symbolic ts")
                .intro("To create symbolic time-series use TimeSeries('url://like/id') or with payload: TimeSeries('url://like/id',ts_with_values)")
                .returns("ts_id","str","url-like ts_id as passed to constructor or empty if the ts is not a ts with ts_id")()
            )
            .def("set_ts_id",&apoint_ts::set_id,(py::arg("self"),py::arg("ts_id")),
                doc.intro("Set a new ts_id of symbolic ts, requires unbound ts.")
                .intro("To create symbolic time-series use TimeSeries('url://like/id') or with payload: TimeSeries('url://like/id',ts_with_values)")()
            )
            .def("bucket_to_hourly",&apoint_ts::bucket_to_hourly,(py::arg("self"),py::arg("start_hour_utc"),py::arg("bucket_emptying_limit")),
                doc.intro(
                "Precipitation bucket measurements have a lot of tweaks that needs to be resolved,\n"
                "including negative variations over the day due to faulty temperature-dependent\n"
                "volume/weight sensors attached.\n"
                "\n"
                "A precipitation bucket accumulates precipitation, so the readings should be strictly\n"
                "increasing by time, until the bucket is emptied (full, or as part of maintenance).\n"
                "\n"
                "The goal for the bucket_to_hourly algorithm is to provide *hourly* precipitation, based on some input signal\n"
                "that usually is hourly(averaging is used if not hourly).\n"
                "\n"
                "The main strategy is to use 24 hour differences (typically at hours in a day where the\n"
                "temperature is low, like early in the morning.), to adjust the hourly volume.\n"
                "\n"
                "Differences in periods of 24hour are distributed on all positive hourly evenets, the \n"
                "negative derivatives are zeroed out, so that the hourly result for each 24 hour\n"
                "is steady increasing, and equal to the difference of the 24hour area.\n"
                "\n"
                "The derivative is then used to compute the hourly precipitation rate in mm/h\n"
                )
                .parameters()
                .parameter("start_hour_utc","int","valid range [0..24], usually set to early morning(low-stable temperature)")
                .parameter("bucket_emptying_limit", "float", "a negative number, range[-oo..0>, limit of when to detect an emptying of a bucket in the unit of the measurements series")
                .returns("ts","TimeSeries","a new hourly rate ts, that transforms the accumulated series, compensated for the described defects")()
            )
            .def("evaluate",&apoint_ts::evaluate,(py::arg("self")),
                doc.intro("Forces evaluation of the expression, returns a new concrete time-series")
                .intro("that is detached from the expression.")
                .returns("ts","TimeSeries","the evaluated copy of the expression that self represents")()
            )
            .def("statistics", &apoint_ts::statistics, (py::arg("self"),py::arg("ta"),py::arg("p")),
                doc.intro("create a new ts that extract the specified statistics from self over the specified time-axis 'ta'")
                .parameters()
                .parameter("ta","TimeAxis","time-axis that specifies the periods where true-average is applied")
                .parameter("p","int","percentile range [0..100], or statistical_property.AVERAGE|MIN_EXTREME|MAX_EXTREME")
                .returns("ts","TimeSeries","a new time-series expression, will provide the statistics when requested")()
            )
            DEF_STD_TS_STUFF()
            //--
            // expose time_axis sih: would like to use property, but no return value policy, so we use get_ + fixup in init.py

            .def("get_time_axis", &apoint_ts::time_axis,(py::arg("self")), "TimeAxis: the time-axis", py::return_internal_reference<>())
            .add_property("values", &apoint_ts::values,"DoubleVector: the values values (possibly calculated on the fly)")
            // operators
            .def(py::self * py::self)
            .def(double() * py::self)
            .def(py::self * double())

            .def(py::self + py::self)
            .def(double() + py::self)
            .def(py::self + double())

            .def(py::self / py::self)
            .def(double() / py::self)
            .def(py::self / double())

            .def(py::self - py::self)
            .def(double() - py::self)
            .def(py::self - double())
            .def(-py::self)
            .def(operator!(py::self))
            .def(py::self==py::self)
            .def("abs", &apoint_ts::abs,(py::arg("self")),
                doc.intro("create a new ts, abs(py::self")
                .returns("ts", "TimeSeries", "a new time-series expression, that will provide the abs-values of self.values")()
            )
            .def("average", &apoint_ts::average, (py::arg("self"),py::arg("ta")),
                doc.intro("create a new ts that is the true average of self")
                .intro("over the specified time-axis ta.")
                .intro("Notice that same definition as for integral applies; non-nan parts goes into the average")
                .parameters()
                .parameter("ta","TimeAxis","time-axis that specifies the periods where true-average is applied")
                .returns("ts","TimeSeries","a new time-series expression, that will provide the true-average when requested")
                .notes()
                .note("the self point interpretation policy is used when calculating the true average")()
            )
            .def("integral", &apoint_ts::integral,( py::arg("self"),py::arg("ta")),
                doc.intro("create a new ts that is the true integral of self")
                .intro("over the specified time-axis ta.")
                .intro("defined as integral of the non-nan part of each time-axis interval")
                .parameters()
                .parameter("ta", "TimeAxis", "time-axis that specifies the periods where true-integral is applied")
                .returns("ts", "TimeSeries", "a new time-series expression, that will provide the true-integral when requested")
                .notes()
                .note("the self point interpretation policy is used when calculating the true average")()
            )
            .def("accumulate", &apoint_ts::accumulate, (py::arg("self"),py::arg("ta")),
                doc.intro("create a new ts where each i-th value ::")
                .intro("| integral f(t)*dt, from t0..ti\n")
                .intro("given the specified time-axis ta, and the point interpretation.")
		        .parameters()
                .parameter("ta","TimeAxis","time-axis that specifies the periods where accumulated integral is applied")
                .returns("ts","TimeSeries","a new time-series expression, that will provide the accumulated values when requested")
                .notes()
                .pure("\nIn contrast to ")
                .ref_meth("integral")
                .pure(", `accumulate` has a point-instant interpretation. As ")
                .ref_meth("values")
                .pure("gives the start values of each interval, see ")
                .ref_class("TimeSeries")
			    .intro(", `accumulate(ta).values` provides the accumulation over the intervals `[t0..t0, t0..t1, t0..t2, ...]`, "
			           "thus `values[0]` is always 0.)"
                       )()
            )
            .def("derivative",&apoint_ts::derivative,(py::arg("self"),py::arg("method")=derivative_method::default_diff),
                 doc.intro("Compute the derivative of the ts, according to the method specified.")
                 .intro("For linear(POINT_INSTANT_VALUE), it is always the derivative of the straight line between points,")
                 .intro("- using nan for the interval starting at the last point until end of time-axis.")
                 .intro("Default for stair-case(POINT_AVERAGE_VALUE) is the average derivative over each time-step,")
                 .intro("- using 0 as rise for the first/last half of the intervals at the boundaries.")
                 .intro("here you can influence the method used, selecting .forward_diff, .backward_diff")
                 .parameters()
                 .parameter("method","derivative_method","default value gives center/average derivative .(DEFAULT|FORWARD|BACKWARD|CENTER)")
                 .returns("derivative","TimeSeries","The derivative ts")()
             )
            .def("time_shift", &apoint_ts::time_shift,(py::arg("self"),py::arg("delta_t")),
                doc.intro("create a new ts that is a the time-shift'ed  version of self")
                .parameters()
                .parameter("delta_t","int","number of seconds to time-shift, positive values moves forward")
                .returns("ts","TimeSeries",    "a new time-series, that appears as time-shifted version of self")()
            )
            .def("use_time_axis_from",&apoint_ts::use_time_axis_from,(py::arg("self"),py::arg("other")),
                doc.intro("Create a new ts that have the same values as self, but filtered to the time-axis points from")
                .intro("from the other supplied time-series.")
                .intro("This function migth be useful for making new time-series, that exactly matches")
                .intro("the time-axis of another series.")
                .intro("Values of the resulting time-series is like like: ")
                .intro("[self(t) for t in other.time_axis.time_points[:-1]")
                .intro("")
                .parameters()
                .parameter("other","TimeSeries","time-series that provides the wanted time-axis")
                .returns("ts","TimeSeries",    "a new time-series, that appears as time-shifted version of self")()
            )
            .def("convolve_w", &apoint_ts::convolve_w, (py::arg("self"),py::arg("weights"), py::arg("policy")),
                doc.intro("create a new ts that is the convolved ts with the given weights list")
                .parameters()
                .parameter("weights","DoubleVector","the weights profile, use DoubleVector.from_numpy(...) to create these."
                                " It's the callers responsibility to ensure the sum of weights are 1.0")
                .parameter("policy","convolve_policy","(USE_NEAREST|USE_ZERO|USE_NAN + BACKWARD|FORWARD|CENTER)."
                " Specifies how to handle boundary values")
                .returns("ts","TimeSeries","a new time-series that is evaluated on request to the convolution of self")()
            )
            .def("krls_interpolation", &apoint_ts::krls_interpolation,
                ( py::arg("self"), py::arg("dt"), py::arg("gamma") = 1.E-3, py::arg("tolerance") = 0.01, py::arg("size") = 1000000u ),
                doc.intro("Compute a new TS that is a krls interpolation of self.")
                .intro("")
                .intro("The KRLS algorithm is a kernel regression algorithm for aproximating data, the implementation")
                .intro("used here is from DLib: http://dlib.net/ml.html#krls")
                .intro("The new time-series has the same time-axis as self, and the values vector contain no `nan` entries.")
                .intro("")
                .intro("If you also want the mean-squared error of the interpolation use get_krls_predictor instead, and")
                .intro("use the predictor api to generate a interpolation and a mse time-series.")
                .intro("Other related functions are TimeSeries.get_krls_predictor, KrlsRbfPredictor")

                .parameters()
                .parameter("dt", "float", "The time-step in seconds the underlying predictor is specified for."
                    " Note that this does not put a limit on time-axes used, but for best results it should be"
                    " approximatly equal to the time-step of time-axes used with the predictor. In addition it"
                    " should not be to long, else you will get poor results. Try to keep the dt less than a day,"
                    " 3-8 hours is usually fine." )
                .parameter("gamma", "float (optional)", "Determines the width of the radial basis functions for the KRLS algorithm."
                    " Lower values mean wider basis functions, wider basis functions means faster computation but lower"
                    " accuracy. Note that the tolerance parameter also affects speed and accurcy."
                    " A large value is around `1E-2`, and a small value depends on the time step. By using values larger"
                    " than `1E-2` the computation will probably take to long. Testing have reveled that `1E-3` works great"
                    " for a time-step of 3 hours, while a gamma of `1E-2` takes a few minutes to compute. Use `1E-4` for a"
                    " fast and tolerably accurate prediction."
                    " Defaults to `1E-3`" )
                .parameter("tolerance", "float (optional)", "The krls training tolerance. Lower values makes the prediction more accurate,"
                    " but slower. This typically have less effect than gamma, but is usefull for tuning. Usually it should be"
                    " either `0.01` or `0.001`."
                    " Defaults to `0.01`" )
                .parameter("size", "int (optional)", "The size of the \"memory\" of the underlying predictor. The default value is"
                    " usually enough. Defaults to `1000000`." )
                .intro("")
                .intro("Examples:\n")
                .intro(">>> import numpy as np")
                .intro(">>> import scipy.stats as stat")
                .intro(">>> from shyft.time_series import (")
                .intro("...     Calendar, utctime_now, deltahours,")
                .intro("...     TimeAxis, TimeSeries")
                .intro("... )")
                .intro(">>>")
                .intro(">>> cal = Calendar()")
                .intro(">>> t0 = utctime_now()")
                .intro(">>> dt = deltahours(1)")
                .intro(">>> n = 365*24  # one year")
                .intro(">>>")
                .intro(">>> # generate random bell-shaped data")
                .intro(">>> norm = stat.norm()")
                .intro(">>> data = np.linspace(0, 20, n)")
                .intro(">>> data = stat.norm(10).pdf(data) + norm.pdf(np.random.rand(*data.shape))")
                .intro(">>> # -----")
                .intro(">>> ta = TimeAxis(cal, t0, dt, n)")
                .intro(">>> ts = TimeSeries(ta, data)")
                .intro(">>>")
                .intro(">>> # compute the interpolation")
                .intro(">>> ts_ipol = ts.krls_interpolation(deltahours(3))\n")
                .returns("krls_ts", "TimeSeries", "A new time series being the KRLS interpolation of self.")()
            )
            .def("get_krls_predictor", &apoint_ts_get_krls_predictor,
                ( py::arg("self"), py::arg("dt"), py::arg("gamma") = 1.E-3, py::arg("tolerance") = 0.01, py::arg("size") = 1000000u ),
                doc.intro("Get a KRLS predictor trained on this time-series.")
                .intro("")
                .intro("If you only want a interpolation of self use krls_interpolation instead, this method")
                .intro("return the underlying predictor instance that can be used to generate mean-squared error")
                .intro("estimates, or can be further trained on more data.")
                .notes()
                .note("A predictor can only be generated for a bound time-series.")
                .parameters()
                .parameter("dt", "float", "The time-step in seconds the underlying predictor is specified for."
                    " Note that this does not put a limit on time-axes used, but for best results it should be"
                    " approximatly equal to the time-step of time-axes used with the predictor. In addition it"
                    " should not be to long, else you will get poor results. Try to keep the dt less than a day,"
                    " 3-8 hours is usually fine." )
                .parameter("gamma", "float (optional)", "Determines the width of the radial basis functions for the KRLS algorithm."
                    " Lower values mean wider basis functions, wider basis functions means faster computation but lower"
                    " accuracy. Note that the tolerance parameter also affects speed and accurcy."
                    " A large value is around `1E-2`, and a small value depends on the time step. By using values larger"
                    " than `1E-2` the computation will probably take to long. Testing have reveled that `1E-3` works great"
                    " for a time-step of 3 hours, while a gamma of `1E-2` takes a few minutes to compute. Use `1E-4` for a"
                    " fast and tolerably accurate prediction."
                    " Defaults to `1E-3`" )
                .parameter("tolerance", "float (optional)", "The krls training tolerance. Lower values makes the prediction more accurate,"
                    " but slower. This typically have less effect than gamma, but is usefull for tuning. Usually it should be"
                    " either `0.01` or `0.001`."
                    " Defaults to `0.01`" )
                .parameter("size", "int (optional)", "The size of the \"memory\" of the underlying predictor. The default value is"
                    " usually enough. Defaults to `1000000`." )
                .intro("")
                .intro("Examples:\n")
                .intro(">>> import numpy as np")
                .intro(">>> import scipy.stats as stat")
                .intro(">>> from shyft.time_series import (")
                .intro("...     Calendar, utctime_now, deltahours,")
                .intro("...     TimeAxis, TimeSeries")
                .intro("... )")
                .intro(">>>")
                .intro(">>> cal = Calendar()")
                .intro(">>> t0 = utctime_now()")
                .intro(">>> dt = deltahours(1)")
                .intro(">>> n = 365*24  # one year")
                .intro(">>>")
                .intro(">>> # generate random bell-shaped data")
                .intro(">>> norm = stat.norm()")
                .intro(">>> data = np.linspace(0, 20, n)")
                .intro(">>> data = stat.norm(10).pdf(data) + norm.pdf(np.random.rand(*data.shape))")
                .intro(">>> # -----")
                .intro(">>> ta = TimeAxis(cal, t0, dt, n)")
                .intro(">>> ts = TimeSeries(ta, data)")
                .intro(">>>")
                .intro(">>> # create a predictor")
                .intro(">>> pred = ts.get_krls_predictor()")
                .intro(">>> total_mse = pred.predictor_mse(ts)  # compute mse relative to ts")
                .intro(">>> krls_ts = pred.predict(ta)  # generate a prediction, this is the result from ts.krls_interpolation")
                .intro(">>> krls_mse_ts = pred.mse_ts(ts, points=6)  # compute a mse time-series using 6 points around each sample")
                .returns("krls_predictor", "KrlsRbfPredictor", "A KRLS predictor pre-trained once on self.")
                .intro("Other related methods are:")
                .ref_meth("shyft.time_series.TimeSeries.krls_interpolation")
                .intro("")()
            )
            .def("rating_curve", &apoint_ts::rating_curve,(py::arg("self"), py::arg("rc_param")),
                doc.intro("Create a new TimeSeries that is computed using a RatingCurveParameter instance.")
                .intro("")
                .intro("Examples:\n")
                .intro("")
                .intro(">>> import numpy as np")
                .intro(">>> from shyft.time_series import (")
                .intro("...     utctime_now, deltaminutes,")
                .intro("...     TimeAxis, TimeSeries,")
                .intro("...     RatingCurveFunction, RatingCurveParameters")
                .intro("... )")
                .intro(">>>")
                .intro(">>> # parameters")
                .intro(">>> t0 = utctime_now()")
                .intro(">>> dt = deltaminutes(30)")
                .intro(">>> n = 48*2")
                .intro(">>>")
                .intro(">>> # make rating function, each with two segments")
                .intro(">>> rcf_1 = RatingCurveFunction()")
                .intro(">>> rcf_1.add_segment(0, 2, 0, 1)    # add segment from level 0, computing f(h) = 2*(h - 0)**1")
                .intro(">>> rcf_1.add_segment(5.3, 1, 1, 1.4)  # add segment from level 5.3, computing f(h) = 1.3*(h - 1)**1.4")
                .intro(">>> rcf_2 = RatingCurveFunction()")
                .intro(">>> rcf_2.add_segment(0, 1, 1, 1)    # add segment from level 0, computing f(h) = 1*(h - 1)**1")
                .intro(">>> rcf_2.add_segment(8.0, 0.5, 0, 2)  # add segment from level 8.0, computing f(h) = 0.5*(h - 0)**2")
                .intro(">>>")
                .intro(">>> # add rating curves to a parameter pack")
                .intro(">>> rcp = RatingCurveParameters()")
                .intro(">>> rcp.add_curve(t0, rcf_1)  # rcf_1 is active from t0")
                .intro(">>> rcp.add_curve(t0+dt*n//2, rcf_2)  # rcf_2 takes over from t0 + dt*n/2")
                .intro(">>>")
                .intro(">>> # create a time-axis/-series")
                .intro(">>> ta = TimeAxis(t0, dt, n)")
                .intro(">>> ts = TimeSeries(ta, np.linspace(0, 12, n))")
                .intro(">>> rc_ts = ts.rating_curve(rcp)  # create a new time series computed using the rating curve functions")
                .intro(">>>")
                .parameters()
                .parameter("rc_param", "RatingCurveParameter", "RatingCurveParameter instance.")
                .returns("rcts", "TimeSeries", "A new TimeSeries computed using self and rc_param.")()
            )
            .def("ice_packing", &apoint_ts::ice_packing, (py::arg("self"), py::arg("ip_params"), py::arg("ipt_policy")),
                doc.intro("Create a binary time-series indicating whether ice-packing is occuring or not.")
                .intro("")
                .intro("Note:\n")
                .intro("    `self` is interpreted and assumed to be a temperature time-series.")
                .intro("")
                .intro("The ice packing detection is based on the mean temperature in a predetermined time")
                .intro("window before the time-point of interrest (see `IcePackingParameters.window`.")
                .intro("The algorithm determines there to be ice packing when the mean temperature is below")
                .intro("a given threshold temperature (see `IcePackingParameters.threshold_temp`).")
                .intro("")
                .parameters()
                .parameter("ip_param", "IcePackingParameters",
                              "Parameter container controlling the ice packing detection.")
                .parameter("ipt_policy", "ice_packing_temperature_policy",
                              "Policy flags for determining how to deal with missing temperature values.")
                .intro("")
                .returns("ice_packing_ts", "TimeSeries", "A time-series indicating wheter ice packing occurs or not")
                .intro("")
                .intro("Example:\n")
                .intro(">>> import numpy as np")
                .intro(">>> from shyft.time_series import (")
                .intro("...     IcePackingParameters, ice_packing_temperature_policy,")
                .intro("...     TimeAxis, TimeSeries, point_interpretation_policy, DoubleVector,")
                .intro("...     utctime_now, deltahours, deltaminutes,")
                .intro("... )")
                .intro(">>> ")
                .intro(">>> t0 = utctime_now()")
                .intro(">>> dt = deltaminutes(15)")
                .intro(">>> n = 100")
                .intro(">>> ")
                .intro(">>> # generate jittery data")
                .intro(">>> # - first descending from +5 to -5 then ascending back to +5")
                .intro(">>> # - include a NaN hole at the bottom of the V")
                .intro(">>> n_ = n if (n//2)*2 == n else n+1  # assure even")
                .intro(">>> data = np.concatenate((")
                .intro("...     np.linspace(5, -5, n_//2), np.linspace(-5, 5, n_//2)")
                .intro("... )) + np.random.uniform(-0.75, 0.75, n_)  # add uniform noise")
                .intro(">>> data[n_//2 - 1:n_//2 + 2] = float('nan')  # add some missing data")
                .intro(">>> ")
                .intro(">>> # create Shyft data structures")
                .intro(">>> ta = TimeAxis(t0, dt, n_)")
                .intro(">>> temperature_ts = TimeSeries(ta, DoubleVector.from_numpy(data),")
                .intro("...                             point_interpretation_policy.POINT_AVERAGE_VALUE)")
                .intro(">>> ")
                .intro(">>> # do the ice packing detection")
                .intro(">>> ip_param = IcePackingParameters(")
                .intro("...     threshold_window=deltahours(5),")
                .intro("...     threshold_temperature=-1.0)")
                .intro(">>> # try all the different temperature policies")
                .intro(">>> ice_packing_ts_disallow = temperature_ts.ice_packing(ip_param, ice_packing_temperature_policy.DISALLOW_MISSING)")
                .intro(">>> ice_packing_ts_initial = temperature_ts.ice_packing(ip_param, ice_packing_temperature_policy.ALLOW_INITIAL_MISSING)")
                .intro(">>> ice_packing_ts_any = temperature_ts.ice_packing(ip_param, ice_packing_temperature_policy.ALLOW_ANY_MISSING)")
                .intro(">>> ")
                .intro(">>> # plotting")
                .intro(">>> from matplotlib import pyplot as plt")
                .intro(">>> from shyft.time_series import time_axis_extract_time_points")
                .intro(">>> ")
                .intro(">>> # NOTE: The offsets below are added solely to be able to distinguish between the different time-axes")
                .intro(">>> ")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], temperature_ts.values, label='Temperature')")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], ice_packing_ts_disallow.values.to_numpy() + 1,")
                .intro("...          label='Ice packing? [DISALLOW_MISSING]')")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], ice_packing_ts_initial.values.to_numpy() - 1,")
                .intro("...          label='Ice packing? [ALLOW_INITIAL_MISSING]')")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], ice_packing_ts_any.values.to_numpy() - 3,")
                .intro("...          label='Ice packing? [ALLOW_ANY_MISSING]')")
                .intro(">>> plt.legend()")
                .intro(">>> plt.show()")()
            )
            .def("ice_packing_recession", &apoint_ts::ice_packing_recession, (py::arg("self"), py::arg("ip_ts"), py::arg("ipr_params")),
                doc.intro("Create a new time series where segments are replaced by recession curves.")
                .intro("")
                .intro("Note:\n")
                .intro("    The total period (`TimeSeries.total_period`) of `self` needs to be equal to,")
                .intro("    or contained in the total period of `ip_ts`.")
                .intro("")
                .parameters()
                .parameter("ip_ts", "TimeSeries",
                    "A binary time-series indicating if ice packing occurring. See `TimeSeries.ice_packing`.")
                .parameter("ip_param", "IcePackingParameters",
                    "Parameter container controlling the ice packing recession curve.")
                .intro("")
                .returns("ice_packing_recession_ts", "TimeSeries",
                    "A time-series where sections in `self` is replaced by recession curves as indicated by `ip_ts`.")
                .intro("")
                .intro("Example:\n")
                .intro(">>> import numpy as np")
                .intro(">>> from shyft.time_series import (")
                .intro("...     IcePackingParameters, IcePackingRecessionParameters, ice_packing_temperature_policy,")
                .intro("...     TimeAxis, TimeSeries, point_interpretation_policy, DoubleVector,")
                .intro("...     utctime_now, deltahours, deltaminutes,")
                .intro("... )")
                .intro(">>> ")
                .intro(">>> t0 = utctime_now()")
                .intro(">>> dt = deltaminutes(15)")
                .intro(">>> n = 100")
                .intro(">>> ")
                .intro(">>> # generate jittery temperature data")
                .intro(">>> # - first descending from +5 to -5 then ascending back to +5")
                .intro(">>> # - include a NaN hole at the bottom of the V")
                .intro(">>> n_ = n if (n//2)*2 == n else n+1  # assure even")
                .intro(">>> temperature_data = np.concatenate((")
                .intro("...     np.linspace(5, -5, n_//2), np.linspace(-5, 5, n_//2)")
                .intro("... )) + np.random.uniform(-0.75, 0.75, n_)  # add uniform noise")
                .intro(">>> temperature_data[n_ // 2 - 1:n_ // 2 + 2] = float('nan')  # add some missing data")
                .intro(">>> ")
                .intro(">>> # create Shyft data structures for temperature")
                .intro(">>> ta = TimeAxis(t0, dt, n_)")
                .intro(">>> temperature_ts = TimeSeries(ta, DoubleVector.from_numpy(temperature_data),")
                .intro("...                             point_interpretation_policy.POINT_AVERAGE_VALUE)")
                .intro(">>> ")
                .intro(">>> # generate jittery waterflow data")
                .intro(">>> # - an upwards curving parabola")
                .intro(">>> x0 = ta.total_period().start")
                .intro(">>> x1 = ta.total_period().end")
                .intro(">>> x = np.linspace(x0, x1, n_)")
                .intro(">>> flow_data = -0.0000000015*(x - x0)*(x - x1) + 1 + np.random.uniform(-0.5, 0.5, n_)")
                .intro(">>> del x0, x1, x")
                .intro(">>> ")
                .intro(">>> # create Shyft data structures for temperature")
                .intro(">>> flow_ts = TimeSeries(ta, DoubleVector.from_numpy(flow_data),")
                .intro("...                      point_interpretation_policy.POINT_AVERAGE_VALUE)")
                .intro(">>> ")
                .intro(">>> # do the ice packing detection")
                .intro(">>> ip_param = IcePackingParameters(")
                .intro("...     threshold_window=deltahours(5),")
                .intro("...     threshold_temperature=-1.0)")
                .intro(">>> # compute the detection time-series")
                .intro(">>> # ice_packing_ts = temperature_ts.ice_packing(ip_param, ice_packing_temperature_policy.DISALLOW_MISSING)")
                .intro(">>> # ice_packing_ts = temperature_ts.ice_packing(ip_param, ice_packing_temperature_policy.ALLOW_INITIAL_MISSING)")
                .intro(">>> ice_packing_ts = temperature_ts.ice_packing(ip_param, ice_packing_temperature_policy.ALLOW_ANY_MISSING)")
                .intro(">>> ")
                .intro(">>> # setup for the recession curve")
                .intro(">>> ipr_param = IcePackingRecessionParameters(")
                .intro("...     alpha=0.00009,")
                .intro("...     recession_minimum=2.)")
                .intro(">>> # compute a recession curve based on the ice packing ts")
                .intro(">>> ice_packing_recession_ts_initial = flow_ts.ice_packing_recession(ice_packing_ts, ipr_param)")
                .intro(">>> ")
                .intro(">>> # plotting")
                .intro(">>> from matplotlib import pyplot as plt")
                .intro(">>> from shyft.time_series import time_axis_extract_time_points")
                .intro(">>> ")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], temperature_ts.values, label='Temperature')")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], flow_ts.values, label='Flow')")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], ice_packing_ts.values.to_numpy(),")
                .intro("...          label='Ice packing?')")
                .intro(">>> plt.plot(time_axis_extract_time_points(ta)[:-1], ice_packing_recession_ts_initial.values.to_numpy(),")
                .intro("...          label='Recession curve')")
                .intro(">>> plt.legend()")
                .intro(">>> plt.show()")()
            )
            .def("extend", &apoint_ts::extend, (py::arg("self"), py::arg("ts"), py::arg("split_policy") = extend_ts_split_policy::EPS_LHS_LAST, py::arg("fill_policy") = extend_ts_fill_policy::EPF_NAN, py::arg("split_at") = utctime(seconds(0)), py::arg("fill_value") = shyft::nan),
                doc.intro("create a new time-series that is self extended with ts")
                .parameters()
                .parameter("ts", "TimeSeries", "time-series to extend self with, only values after both the start of self, and split_at is used")
                .parameter("split_policy", "extend_split_policy", "policy determining where to split between self and ts")
                .parameter("fill_policy", "extend_fill_policy", "policy determining how to fill any gap between self and ts")
                .parameter("split_at", "utctime", "time at which to split if split_policy == EPS_VALUE")
                .parameter("fill_value", "float", "value to fill any gap with if fill_policy == EPF_FILL")
                .returns("extended_ts" ,"TimeSeries", "a new time-series that is the extension of self with ts")()
            )
            .def("merge_points",&apoint_ts::merge_points,(py::arg("self"),py::arg("ts")),
                 doc.intro("Given that self is a concrete point-ts(not an expression), or empty ts,")
                 .intro("this function modifies the point-set of self, with points, (time,value) from other ts")
                 .intro("The result of the merge operation is the distinct set of time-points from self and other ts")
                 .intro("where values from other ts overwrites values of self if they happen")
                 .intro("to be at the same time-point")
                 .parameters()
                 .parameter("ts","TimeSeries","time-series to merge the time,value points from")
                 .returns("self","TimeSeries","self modified with the merged points from other ts")()
            )
            .def("slice",&apoint_ts::slice,(py::arg("self"),py::arg("i0"),py::arg("n")),
                 doc.intro("Given that self is a concrete point-ts(not an expression), or empty ts,")
                 .intro("return a new TimeSeries containing the n values starting from index i0.")
                 .parameters()
                 .parameter("i0", "int", "Index of first element to include in the slice")
                 .parameter("n", "int", "Number of elements to include in the slice")()
            )
            .def("pow",pow_double_f,(py::arg("self"),py::arg("number")),"create a new ts that contains pow(py::self,number)")
            .def("pow",pow_ts_f,(py::arg("self"),py::arg("ts_other")),"create a new ts that contains pow(py::self,ts_other)")
            .def("min",min_double_f,(py::arg("self"),py::arg("number")),"create a new ts that contains the min of self and number for each time-step")
            .def("min",min_ts_f,(py::arg("self"),py::arg("ts_other")),"create a new ts that contains the min of self and ts_other")
            .def("max",max_double_f,(py::arg("self"),py::arg("number")),"create a new ts that contains the max of self and number for each time-step")
            .def("max",max_ts_f,(py::arg("self"),py::arg("ts_other")),"create a new ts that contains the max of self and ts_other")
            .def("log", log_na_f, (py::arg("self")), "create a new ts that contains log(py::self)")
            .def("min_max_check_linear_fill",min_max_check_linear_fill_i,
                 (py::arg("self"),py::arg("v_min"),py::arg("v_max"),py::arg("dt_max")=shyft::core::max_utctime),
                 doc.intro("Create a min-max range checked ts with fill-values if value is NaN or outside range")
                 .intro("If the underlying time-series is point-instant, then fill-values are linear-interpolation,")
                 .intro("otherwise, the previous value, if available is used as fill-value.")
                 .intro("A similar function with more features is quality_and_self_correction()")
                 .parameters()
                 .parameter("v_min","float","minimum range, values < v_min are considered NaN. v_min==NaN means no lower limit")
                 .parameter("v_max","float","maximum range, values > v_max are considered NaN. v_max==NaN means no upper limit")
                 .parameter("dt_max","int","maximum time-range in seconds allowed for interpolating/extending values, default= max_utctime")
                 .returns("min_max_check_linear_fill","TimeSeries","Evaluated on demand time-series with NaN, out of range values filled in")()
            )
            .def("min_max_check_ts_fill",min_max_check_ts_fill_i,
                 (py::arg("self"),py::arg("v_min"),py::arg("v_max"),py::arg("dt_max"),py::arg("cts")),
                 doc.intro("Create a min-max range checked ts with cts-filled-in-values if value is NaN or outside range")
                 .parameters()
                 .parameter("v_min","float","minimum range, values < v_min are considered NaN. v_min==NaN means no lower limit")
                 .parameter("v_max","float","maximum range, values > v_max are considered NaN. v_max==NaN means no upper limit")
                 .parameter("dt_max","int","maximum time-range in seconds allowed for interpolating values")
                 .parameter("cts","TimeSeries","time-series that keeps the values to be filled in at points that are NaN or outside min-max-limits")
                 .returns("min_max_check_ts_fill","TimeSeries","Evaluated on demand time-series with NaN, out of range values filled in")()
            )
            .def("min_max_check_linear_fill",min_max_check_linear_fill_t,
                 (py::arg("self"),py::arg("v_min"),py::arg("v_max"),py::arg("dt_max")=shyft::core::max_utctime),
                 doc.intro("Create a min-max range checked ts with fill-values if value is NaN or outside range")
                 .intro("If the underlying time-series is point-instant, then fill-values are linear-interpolation,")
                 .intro("otherwise, the previous value, if available is used as fill-value.")
                 .intro("Similar and more parameterized function is quality_and_self_correction()")
                 .parameters()
                 .parameter("v_min","float","minimum range, values < v_min are considered NaN. v_min==NaN means no lower limit")
                 .parameter("v_max","float","maximum range, values > v_max are considered NaN. v_max==NaN means no upper limit")
                 .parameter("dt_max","int","maximum time-range in seconds allowed for interpolating/extending values, default= max_utctime")
                 .returns("min_max_check_linear_fill","TimeSeries","Evaluated on demand time-series with NaN, out of range values filled in")()
            )
            .def("min_max_check_ts_fill",min_max_check_ts_fill_t,
                 (py::arg("self"),py::arg("v_min"),py::arg("v_max"),py::arg("dt_max"),py::arg("cts")),
                 doc.intro("Create a min-max range checked ts with cts-filled-in-values if value is NaN or outside range")
                 .parameters()
                 .parameter("v_min","float","minimum range, values < v_min are considered NaN. v_min==NaN means no lower limit")
                 .parameter("v_max","float","maximum range, values > v_max are considered NaN. v_max==NaN means no upper limit")
                 .parameter("dt_max","int","maximum time-range in seconds allowed for interpolating values")
                 .parameter("cts","TimeSeries","time-series that keeps the values to be filled in at points that are NaN or outside min-max-limits")
                 .returns("min_max_check_ts_fill","TimeSeries","Evaluated on demand time-series with NaN, out of range values filled in")()
            )
            .def("quality_and_self_correction",&apoint_ts::quality_and_self_correction,
                 (py::arg("self"),py::arg("parameters")),
                 doc.intro("returns a new time-series that applies quality checks accoring to parameters")
                 .intro("and fills in values according to rules specified in parameters.")
                 .parameters()
                 .parameter("parameter","QacParameter","Parameter with rules for quality and corrections")
                 .returns("ts","TimeSeries","a new time-series where the values are subject to quality and correction as specified")()
             )
            .def("quality_and_ts_correction",&apoint_ts::quality_and_ts_correction,
                 (py::arg("self"),py::arg("parameters"),py::arg("cts")),
                 doc.intro("returns a new time-series that applies quality checks accoring to parameters")
                 .intro("and fills in values from the cts, according to rules specified in parameters.")
                 .parameters()
                 .parameter("parameter","QacParameter","Parameter with rules for quality and corrections")
                 .parameter("cts","TimeSeries","is used to fill in correct values, as f(t) for values that fails quality-checks")
                 .returns("ts","TimeSeries","a new time-series where the values are subject to quality and correction as specified")()
             )
            .def("inside", &apoint_ts::inside,
                (py::arg("self"),py::arg("min_v"),py::arg("max_v"),py::arg("nan_v")=shyft::nan,py::arg("inside_v")=1.0,py::arg("outside_v")=0.0),
                doc.intro(
                    "Create an inside min-max range ts, that transforms the point-values\n"
                    "that falls into the half open range [min_v .. max_v > to \n"
                    "the value of inside_v(default=1.0), or outside_v(default=0.0),\n"
                    "and if the value considered is nan, then that value is represented as nan_v(default=nan)\n"
                    "You would typically use this function to form a true/false series (inside=true, outside=false)\n"
                )
                .parameters()
                .parameter("min_v","float","minimum range, values <  min_v are not inside min_v==NaN means no lower limit")
                .parameter("max_v","float","maximum range, values >= max_v are not inside. max_v==NaN means no upper limit")
                .parameter("nan_v","float","value to return if the value is nan")
                .parameter("inside_v","float","value to return if the ts value is inside the specified range")
                .parameter("outside_v","float","value to return if the ts value is outside the specified range")
                .returns("inside_ts","TimeSeries","Evaluated on demand inside time-series")()
            )
            .def("upper_half_mask", &time_series::dd::upper_half_mask,
                 (py::arg("self")),
                 doc.intro(
                     "Create a ts that contains 1.0 in place of non-negative values, and 0.0 in case of negative values."
                 )
                 .returns("upper_half_mask_ts","TimeSeries","Evaluated on demand inside time-series")()
            )
            .def("lower_half_mask", &time_series::dd::lower_half_mask,
                 (py::arg("self")),
                 doc.intro(
                     "Create a ts that contains 1.0 in place of non-positive values, and 0.0 in case of positive values."
                 )
                 .returns("lower_half_mask_ts","TimeSeries","Evaluated on demand inside time-series")()
            )
            .def("upper_half", &time_series::dd::upper_half,
                 (py::arg("self")),
                 doc.intro(
                     "Create a ts that contains non-negative values only."
                 )
                 .returns("upper_half_ts","TimeSeries","Evaluated on demand inside time-series")()
            )
            .def("lower_half", &time_series::dd::lower_half,
                 (py::arg("self")),
                 doc.intro(
                     "Create a ts that contains non-negative values only."
                 )
                 .returns("lower_half_ts","TimeSeries","Evaluated on demand inside time-series")()
            )
            // .def("transform_spline", +[](const apoint_ts& self, const vector<double>& knots, const vector<double>& coeff, unsigned degree)
            //         { return self.transform_spline(knots, coeff, degree); },
            //     (py::arg("self"),py::arg("knots"),py::arg("coeff"),py::arg("degree")),
            //     doc.intro( "Apply spline transform pointwise to time-series values.")
            //     .returns("transform_spline_tsv","TsVector","New TsVector where each element is an evaluated-on-demand transformed time-series")()
            // )
            .def("transform", +[] ( apoint_ts const & self,  numpy_boost<double, 2> const & pyarray, interpolation_scheme scheme) {
                     return self.transform(xy_point_curve(pyarray), scheme);
                 },(py::arg("self"), py::arg("points"), py::arg("method")),
                 doc.intro("Create a transformed time-series, having values taken from pointwise function evaluation.")
                 .intro("Function values are determined by interpolating the given points, using the specified method.")
                 .intro("Valid method arguments are 'polynomial', 'linear' and 'catmull-rom'.")
                 .returns("transform_ts", "TimeSeries", "New TimeSeries where each element is an evaluated-on-demand transformed time-series.")()
            )
            .def("decode", &apoint_ts::decode,
                (py::arg("self"),py::arg("start_bit"),py::arg("n_bits")),
                doc.intro(
                    "Create an time-series that decodes the source using provided\n"
                    "specification start_bit and n_bits.\n"
                    "This function can typically be used to decode status-signals from sensors stored as \n"
                    "binary encoded bits, using integer representation\n"
                    "The floating point format allows up to 52 bits to be precisely stored as integer\n"
                    "- thus there are restrictions to start_bit and n_bits accordingly.\n"
                    "Practical sensors quality signals have like 32 bits of status information encoded\n"
                    "If the value in source time-series is:\n\n"
                    "     * negative\n"
                    "     * nan\n"
                    "     * larger than 52 bits\n\n"
                    "Then nan is returned for those values\n"
                    "\n"
                    "ts.decode(start_bit=1,n_bits=1) will return values [0,1,nan]\n"
                    "similar:\n"
                    "ts.decode(start_bit=1,n_bits=2) will return values [0,1,2,3,nan]\n"
                    "etc..\n"
                )
                .parameters()
                .parameter("start_bit","int","where in the n-bits integer the value is stored, range[0..51]")
                .parameter("n_bits","int","how many bits are encoded, range[0..51], but start_bit +n_bits < 51")
                .returns("decode_ts","TimeSeries","Evaluated on demand decoded time-series")()
            )

            .def("stack",&apoint_ts::stack_ts,
                (py::arg("self"),py::arg("calendar"), py::arg("t0"),py::arg("n_dt"),py::arg("dt"), py::arg("n_partitions"), py::arg("target_t0"),py::arg("dt_snap")),
                doc.intro("stack time-series into a TsVector of n_partitions time-series, each with semantic calendar length n_dt x dt.")
                .intro("The partitions are simply specified by calendar, n_dt x dt(could be symbolic, like YEAR : MONTH:DAY) and n.")
                .intro("To make yearly partitions, just pass 1, Calendar.YEAR as n_dt and dt respectively.")
                .intro("The t0 - parameter set the start - time point in the source-time-series, e.g. like 1930.09.01")
                .intro("The target_t0 - parameter set the common start-time of the stack, e.g. 2017.09.01")
                .intro("The dt_snap - parameter is useful to ensure that if target_to is a monday, then each partition is adjusted to neares monday.")
                .intro("The snap mechanism could be useful if you would like to stack something like consumption, that would follow a weekly pattern.")
                .intro("")
                .intro("The typical usage will be to use this function to partition years into a vector with")
                .intro("80 years, where we can do statistics, percentiles to compare and see the different effects of")
                .intro("yearly season variations.")
                .intro("Note that the function is more general, allowing any periodic partition, like daily, weekly, monthly etc.")
                .intro("that allows you to study any pattern or statistics that might be periodic by the partition pattern.")
                .intro("Other related methods are time_shift,average,TsVector.")
                .parameters()
                .parameter("calendar","Calendar","The calendar to use, typically utc")
                .parameter("t0","utctime","specifies where to pick the first partition, e.g. 1930.09.01")
                .parameter("n_dt","int","number of calendar units for the length of the stride")
                .parameter("dt","utctimespan","the basic calendar length unit, Calendar.YEAR,Calendar.DAY")
                .parameter("n_partitions","int","number of partitions,e.g. length of the resulting TsVector")
                .parameter("target_t0","utctime","specifies the common target time for the stack, e.g. 2017.09.01")
                .parameter("dt_snap","utctimespan","default 0, if set to WEEK, each stacked partition will be week-aligned.")
                .returns("stacked_ts","TsVector","with length n_partitions, each ts is time-shifted (calendar n_dt x n) to common_t0 expressions")()
                )
            .def("partition_by",+[](apoint_ts const&me,calendar const&cal,utctime t,utctime dt,size_t n_partitions,utctime common_t0){
                return me.stack_ts(cal,t,1,dt,n_partitions,common_t0,utctimespan{0});
                },
                (py::arg("self"),py::arg("calendar"), py::arg("t"), py::arg("partition_interval"), py::arg("n_partitions"), py::arg("common_t0")),
                doc.intro("DEPRECATED(replaced by .stack ) : from a time-series, construct a TsVector of n time-series partitions.")
                .intro("The partitions are simply specified by calendar, delta_t(could be symbolic, like YEAR : MONTH:DAY) and n.")
                .intro("To make yearly partitions, just pass Calendar.YEAR as partition_interval.")
                .intro("The t - parameter set the start - time point in the source-time-series, e.g. like 1930.09.01")
                .intro("The common_t0 - parameter set the common start - time of the new partitions, e.g. 2017.09.01")
                .intro("")
                .intro("The typical usage will be to use this function to partition years into a vector with")
                .intro("80 years, where we can do statistics, percentiles to compare and see the different effects of")
                .intro("yearly season variations.")
                .intro("Note that the function is more general, allowing any periodic partition, like daily, weekly, monthly etc.")
                .intro("that allows you to study any pattern or statistics that might be periodic by the partition pattern.")
                .intro("Other related methods are time_shift,average,TsVector.")
                .parameters()
                .parameter("calendar","Calendar","The calendar to use, typically utc")
                .parameter("t","utctime","specifies where to pick the first partition")
                .parameter("partition_interval","utctimespan","the length of each partition, Calendar.YEAR,Calendar.DAY etc.")
                .parameter("n_partitions","int","number of partitions")
                .parameter("common_t0","utctime","specifies the time to correlate all the partitions")
                .returns("ts-partitions","TsVector","with length n_partitions, each ts is time-shifted to common_t0 expressions")()
                )
            .def("repeat",&apoint_ts::repeat,(py::arg("self"),py::arg("repeat_time_axis")),
                 doc.intro("Repeat all time-series over the given repeat_time_axis periods")
                 .parameters()
                 .parameter("repeat_time_axis","TimeAxis","A time-axis that have the coarse repeat interval, like YEAR or similar")
                 .returns("repeated_ts","TimeSeries","time-series where pattern of self is repeated throughout the period of repeat_time_axis")()
            )
            .def("bind",&apoint_ts::bind,(py::arg("self"),py::arg("bts")),
                doc.intro("given that this ts,self, is a bind-able ts (aref_ts)")
                .intro("and that bts is a concrete point TimeSeries, or something that can be evaluated to one,")
                .intro("use it as representation")
                .intro("for the values of this ts.")
                .intro("Other related functions are find_ts_bind_info,TimeSeries('a-ref-string')")
                .parameters()
                .parameter("bts","TimeSeries","a concrete point ts, or ready-to-evaluate expression, with time-axis, values and fx_policy")
                .notes()
                .note("raises runtime_error if any of preconditions is not true")()
            )
            .def("bind_done",&apoint_ts::do_bind,(py::arg("self")),
                 doc.intro("after bind operations on unbound time-series of an expression is done, call bind_done()")
                 .intro("to prepare the expression for use")
                 .intro("Other related methods are .bind(), .find_ts_bind_info() and needs_bind().")
                 .notes()
                 .note("Usually this is done automatically by the dtss framework, but if not using dtss")
                 .note("this function is needed *after* the symbolic ts's are bound\n")()
            )
            .def("needs_bind",&apoint_ts::needs_bind,(py::arg("self")),
                 doc.intro("returns true if there are any unbound time-series in the expression")
                 .intro("this time-series represent")
                 .intro("These functions also supports symbolic time-series handling: .find_ts_bind_info(),bind() and bind_done()")()

            )
            .def("unbind",&apoint_ts::do_unbind,(py::arg("self")),
                 doc.intro("Reset the  ts-expression to unbound state, discarding bound symbol references.")
                 .intro("For time-series, or expressions, that does not have symbolic references, no effect, see also .find_ts_bind_info(),bind() and bind_done()")()
            )
            .def("find_ts_bind_info",&apoint_ts::find_ts_bind_info,(py::arg("self")),
                doc.intro("recursive search through the expression that this ts represents,")
                .intro("and return a list of TsBindInfo that can be used to")
                .intro("inspect and possibly 'bind' to ts-values.")
                .intro("see also related function bind()")
                .returns("bind_info","TsBindInfoVector","A list of BindInfo where each entry contains a symbolic-ref and a ts that needs binding")()
            )
            .def("clone_expression",&apoint_ts::clone_expr,(py::arg("self")),
                 doc.intro("create a copy of the ts-expression, such that all directed unbound parts from")
                 .intro("the terminal and upwards is copied.")
                 .intro("The parts of the expression-tree that is bound, invariant, is copied by ref.")
                 .notes()
                 .note("this function is only useful in context where multiple bind/rebind while keeping the expression is needed.")
                 .returns("semantic_clone","TimeSeries","returns a copy of the ts, such that all unbound parts are copied, all bound parts and terminals are just shallow copied(referenced)")()
            )
            .def("compress",&apoint_ts::compress,(py::arg("self"),py::arg("accuracy")),
                 doc.intro("Compress by reducing number of points sufficient to represent the same f(t) within accuracy.")
                 .intro("The returned ts is a new ts with break-point/variable interval representation.")
                 .intro("note: lazy binding expressions(server-side eval) is not yet supported.")
                 .parameters()
                 .parameter("accuracy","","if v[i]-v[i+1] <accuracy the v[i+1] is dropped")
                 .returns("compressed_ts","TimeSeries","a new compressed within accuracy time-series")()
            )
            .def("compress_size",&apoint_ts::compress_size,(py::arg("self"),py::arg("accuracy")),
                 doc.intro("Compute number of points this time-series could be reduced to if calling ts.compress(accuracy).")
                 .intro("note: lazy binding expressions(server-side eval) is not yet supported.")
                 .parameters()
                 .parameter("accuracy","","if v[i]-v[i+1] <accuracy the v[i+1] is dropped")
                 .returns("compressed_size","int","number of distinct point needed to represent the time-series")()
            )
            .def("serialize",&apoint_ts::serialize_to_bytes,(py::arg("self")),
                "convert ts (expression) into a binary blob\n"
            )
            .def("deserialize",&apoint_ts::deserialize_from_bytes,(py::arg("blob")),
               "convert a blob, as returned by .serialize() into a Timeseries"
            ).staticmethod("deserialize")
            .def("stringify",&apoint_ts::stringify,(py::arg("self")),
                 "return human-readable string of ts or expression"
                )
        ;
    typedef apoint_ts (*avg_func_t)(apoint_ts const &, shyft::time_axis::generic_dt const &);
    typedef apoint_ts (*int_func_t)(apoint_ts const &, shyft::time_axis::generic_dt const &);
    avg_func_t avg = average;
    int_func_t intfnc = integral;
    avg_func_t acc = accumulate;
    py::def(
      "average",
      avg,
      (py::arg("ts"), py::arg("time_axis")),
      "creates a true average time-series of ts for intervals as specified by time_axis");
    py::def(
      "integral",
      intfnc,
      (py::arg("ts"), py::arg("time_axis")),
      "creates a true integral time-series of ts for intervals as specified by time_axis");
    py::def(
      "accumulate",
      acc,
      (py::arg("ts"), py::arg("time_axis")),
      "create a new ts that is the integral f(t) *dt, t0..ti, the specified time-axis");
    // def("max",time_series::dd::max,(boost::python::arg("ts_a"),boost::python::arg("ts_b")),"creates a new time-series
    // that is the max of the supplied ts_a and ts_b");
    py::def(
      "ts_stringify",
      ts_stringify,
      (py::arg("ts")),
      doc.intro("Given a TimeSeries, return a string showing the details/expression")());
    // ts_to_bokeh_plot_data(apoint_ts ts,calendar const& c,double time_scale, bool force_linear,bool
    // remove_trailing_nans)
    py::def(
      "time_series_to_bokeh_plot_data",
      &ts_to_bokeh_plot_data,
      (py::arg("ts"),
       py::arg("calendar"),
       py::arg("time_scale"),
       py::arg("force_linear"),
       py::arg("crop_trailing_nans")),
      doc.intro("To speedup and ease sending time-series data to bokeh, a all in one function to provide")
        .intro("values from a time-series ready to be rendered by bokeh time-series plot")
        .intro("Call t=r[0].to_numpy() f=r[1].to_numpy() to get numpy arrays, before sending those to bokeh")
        .parameters()
        .parameter("ts", "TimeSeries", "time-series to extract data from")
        .parameter("calendar", "Calendar", "calendar with time-zone to make tz-adjusted values for the time-points")
        .parameter("time_scale", "float", "the time-scale, usually 1000, as bokeh uses ms scaled time-values")
        .parameter(
          "force_linear",
          "bool",
          "if set true, just provide the points as is, otherwise, ts.point_interpretation is considered")
        .parameter("crop_trailing_nans", "bool", "if true, crop away the trailing nan's from the time-series")
        .returns("plot-data", "DoubleVectorVector", "[time-stamps, values] A strongly typed Vector of DoubleVector")());


    typedef apoint_ts (*ts_op_t)(apoint_ts const &a);
    typedef apoint_ts (*ts_op_ts_t)(apoint_ts const &a, apoint_ts const &b);
    typedef apoint_ts (*double_op_ts_t)(double, apoint_ts const &b);
    typedef apoint_ts (*ts_op_double_t)(apoint_ts const &a, double);

    ts_op_ts_t max_ts_ts = time_series::dd::max;
    double_op_ts_t max_double_ts = time_series::dd::max;
    ts_op_double_t max_ts_double = time_series::dd::max;
    py::def("max", max_ts_ts, (py::arg("ts_a"), py::arg("ts_b")), "returns a new ts as max(ts_a,ts_b)");
    py::def("max", max_double_ts, (py::arg("a"), py::arg("ts_b")), "returns a new ts as max(a,ts_b)");
    py::def("max", max_ts_double, (py::arg("ts_a"), py::arg("b")), "returns a new ts as max(ts_a,b)");

    ts_op_ts_t min_ts_ts = time_series::dd::min;
    double_op_ts_t min_double_ts = time_series::dd::min;
    ts_op_double_t min_ts_double = time_series::dd::min;
    py::def("min", min_ts_ts, (py::arg("ts_a"), py::arg("ts_b")), "returns a new ts as min(ts_a,ts_b)");
    py::def("min", min_double_ts, (py::arg("a"), py::arg("ts_b")), "returns a new ts as min(a,ts_b)");
    py::def("min", min_ts_double, (py::arg("ts_a"), py::arg("b")), "returns a new ts as min(ts_a,b)");

    ts_op_ts_t pow_ts_ts = time_series::dd::pow;
    double_op_ts_t pow_double_ts = time_series::dd::pow;
    ts_op_double_t pow_ts_double = time_series::dd::pow;
    py::def("pow", pow_ts_ts, (py::arg("ts_a"), py::arg("ts_b")), "returns a new ts as pow(ts_a,ts_b)");
    py::def("pow", pow_double_ts, (py::arg("a"), py::arg("ts_b")), "returns a new ts as pow(a,ts_b)");
    py::def("pow", pow_ts_double, (py::arg("ts_a"), py::arg("b")), "returns a new ts as pow(ts_a,b)");

    ts_op_t log_ts_ts = time_series::dd::log;
    py::def("log", log_ts_ts, (py::arg("ts")), "returns a new ts as log(ts)");

    py::def(
      "time_shift",
      time_series::dd::time_shift,
      (py::arg("timeseries"), py::arg("delta_t")),
      "returns a delta_t time-shifted time-series\n"
      " the values are the same as the original,\n"
      " but the time_axis equals the original + delta_t\n");

    py::def(
      "create_glacier_melt_ts_m3s",
      create_glacier_melt_ts_m3s,
      (py::arg("temperature"), py::arg("sca_m2"), py::arg("glacier_area_m2"), py::arg("dtf")),
      doc.intro("create a ts that provide the glacier-melt algorithm based on the inputs")
        .parameters()
        .parameter("temperature", "TimeSeries", "a temperature time-series, unit [deg.Celcius]")
        .parameter("sca_m2", "TimeSeries", "a snow covered area (sca) time-series, unit [m2]")
        .parameter("glacier_area_m2", "float", "the glacier area, unit[m2]")
        .parameter(
          "dtf",
          "float",
          "degree timestep factor [mm/day/deg.C]; lit. values for Norway: 5.5 - 6.4 in Hock, R. (2003), J. Hydrol., "
          "282, 104-115")
        .returns("glacier_melt", "TimeSeries", "an expression computing the glacier melt based on the inputs")());
    /* local scope */ {

      typedef shyft::time_axis::fixed_dt ta_t;
      typedef shyft::time_series::average_accessor<pts_t, ta_t> AverageAccessorTs;
      py::class_<AverageAccessorTs>(
        "AverageAccessorTs",
        "Accessor to get out true average for the time-axis intervals for a point time-series",
        py::no_init)
        .def(py::init< pts_t const &, ta_t const &>(
          (py::arg("self"), py::arg("ts"), py::arg("ta")), "construct accessor from ts and time-axis ta"))
        .def(py::init<shared_ptr<pts_t>, ta_t const &>(
          (py::arg("self"), py::arg("ts"), py::arg("ta")), "constructor from ref ts and time-axis ta"))
        .def("value", &AverageAccessorTs::value, (py::arg("self"), py::arg("i")), "returns the i'th true average value")
        .def(
          "size",
          &AverageAccessorTs::size,
          (py::arg("self")),
          "returns number of intervals in the time-axis for this accessor");
    }
    py::class_<qac_parameter>(
      "QacParameter",
      doc
        .intro("The qac parameter controls how quailty checks are done, providing min-max range, plus repeated values "
               "checks")
        .intro("It also provides parameters that controls how the replacement/correction values are filled in,")
        .intro("like maximum time-span between two valid neighbour points that allows for linear/extension filling")())
      .def(py::init<utctimespan, double, double, utctimespan, double, double>(
        (py::arg("self"),
         py::arg("max_timespan"),
         py::arg("min_x"),
         py::arg("max_x"),
         py::arg("repeat_timespan"),
         py::arg("repeat_tolerance"),
         py::arg("constant_filler") = shyft::nan),
        doc.intro("a quite complete qac, only lacks repeat_allowed value(s)")()))
      .def(py::init<utctimespan, double, double, utctimespan, double, double, double>(
        (py::arg("self"),
         py::arg("max_timespan"),
         py::arg("min_x"),
         py::arg("max_x"),
         py::arg("repeat_timespan"),
         py::arg("repeat_tolerance"),
         py::arg("repeat_allowed"),
         py::arg("constant_filler") = shyft::nan),
        doc.intro("a quite complete qac, including one repeat_allowed value")()))
      .def_readwrite("min_v", &qac_parameter::min_x, "float: minimum value or nan for no minimum value limit")
      .def_readwrite("max_v", &qac_parameter::max_x, "float: maximum value or nan for no maximum value limit")
      .def_readwrite(
        "max_timespan",
        &qac_parameter::max_timespan,
        "time: maximum timespan between two ok values that allow interpolation, or extension of values.If zero, no "
        "linear/extend correction")
      .def_readwrite(
        "repeat_timespan",
        &qac_parameter::repeat_timespan,
        "time: maximum timespan the same value can be repeated (within repeat_tolerance).If zero, no repeat validation "
        "done")
      .def_readwrite(
        "repeat_tolerance",
        &qac_parameter::repeat_tolerance,
        "float: values are considered repeated if they differ by less than repeat_tolerance")
      .def_readwrite(
        "repeat_allowed",
        &qac_parameter::repeat_allowed,
        "bool: values that are allowed to repeat, within repeat-tolerance")
      .def_readwrite(
        "constant_filler",
        &qac_parameter::constant_filler,
        "float: this is applied to values that fails quality checks, if no correction ts, and no "
        "interpolation/extension is active");
  }

  using namespace shyft::time_series;

  /** python api need some helper classes to make this more elegant */
  struct rating_curve_t_f {
    utctime t{no_utctime};
    rating_curve_function f{};
    rating_curve_t_f() = default;

    rating_curve_t_f(utctime t, rating_curve_function const &f)
      : t{t}
      , f{f} {
    }

    bool operator==(rating_curve_t_f const &o) const {
      return t == o.t;
    } // satisfy boost py indexing find
  };

  /** helper to fix custom constructors taking the above f_t as list */
  struct rcp_ext {
    static rating_curve_parameters *create_default() {
      return new rating_curve_parameters();
    }

    static rating_curve_parameters *create_from_t_f_list(vector<rating_curve_t_f> const &f_t_list) {
      auto rcp = new rating_curve_parameters();
      for (auto const &e : f_t_list)
        rcp->add_curve(e.t, e.f);
      return rcp;
    }
  };

  static void expose_rating_curve_classes() {

    // overloads for rating_curve_segment::flow
    double (shyft::time_series::rating_curve_segment::*rcs_flow_1)(double)
      const = &shyft::time_series::rating_curve_segment::flow;
    std::vector<double> (shyft::time_series::rating_curve_segment::*rcs_flow_2)(
      std::vector<double> const &, std::size_t, std::size_t) const = &shyft::time_series::rating_curve_segment::flow;

    py::class_<shyft::time_series::rating_curve_segment>(
      "RatingCurveSegment",
      doc.intro("Represent a single rating-curve equation.")
        .intro("")
        .intro("The rating curve function is `a*(h - b)^c` where `a`, `b`, and `c` are parameters")
        .intro("for the segment and `h` is the water level to compute flow for. Additionally there")
        .intro("is a `lower` parameter for the least water level the segment is valid for. Seen")
        .intro("separatly a segment is considered valid for any level greater than `lower`.")
        .intro("")
        .intro("The function segments are gathered into many `RatingCurveFunction` to represent a")
        .intro("set of different rating functions for different levels.")
        .intro("Related classes are RatingCurveFunction, RatingCurveParameters")(),
      py::init<>(py::arg("self")))
      .def_readonly(
        "lower",
        &shyft::time_series::rating_curve_segment::lower,
        "float:Least valid water level. Not mutable after constructing a segment.")
      .def_readwrite("a", &shyft::time_series::rating_curve_segment::a, "float: Parameter a")
      .def_readwrite("b", &shyft::time_series::rating_curve_segment::b, "float: Parameter b")
      .def_readwrite("c", &shyft::time_series::rating_curve_segment::c, "float: Parameter c")
      .def(py::init<double, double, double, double>(
        (py::arg("self"), py::arg("lower"), py::arg("a"), py::arg("b"), py::arg("c")),
        "Defines a new RatingCurveSegment with the specified parameters"))
      .def(
        "valid",
        &shyft::time_series::rating_curve_segment::valid,
        (py::arg("self"), py::arg("level")),
        doc.intro("Check if a water level is valid for the curve segment")
          .parameter("level", "float", "water level")
          .returns("valid", "bool", "True if level is greater or equal to lower")())
      // NOTE: For some reason boost 1.65 needs this def *before* the other simpler def, otherwise it fails finding the
      // simple one
      .def(
        "flow",
        rcs_flow_2,
        (py::arg("self"),
         py::arg("levels"),
         py::arg("i0") = 0u,
         py::arg("iN") = std::numeric_limits<std::size_t>::max()),
        doc.intro("Compute the flow for a range of water levels")
          .parameters()
          .parameter("levels", "DoubleVector", "Vector of water levels")
          .parameter("i0", "int", "first index to use from levels, defaults to 0")
          .parameter("iN", "int", "first index _not_ to use from levels, defaults to std::size_t maximum.")
          .returns("flow", "DoubleVector", "Vector of flow values.")())
      .def(
        "flow",
        rcs_flow_1,
        (py::arg("self"), py::arg("level")),
        doc.intro("Compute the flow for the given water level.")
          .notes()
          .note("There is _no_ check to see if level is valid. It's up to the user to call")
          .note("with a correct level.")
          .parameters()
          .parameter("level", "float", "water level")
          .returns("flow", "double", "the flow for the given water level")())
      .def("__str__", &shyft::time_series::rating_curve_segment::operator std::string, "Stringify the segment.")
      .def(py::self == py::self)
      .def(py::self != py::self);

    typedef std::vector<shyft::time_series::rating_curve_segment> RatingCurveSegmentVector;
    py::class_<RatingCurveSegmentVector>(
      "RatingCurveSegments",
      doc.intro("A typed list of RatingCurveSegment, used to construct RatingCurveParameters.")(),
      py::init<>(py::arg("self")))
      .def(py::vector_indexing_suite<RatingCurveSegmentVector>())
      .def(py::init< RatingCurveSegmentVector const &>((py::arg("clone_me"))));
    py_api::iterable_converter().from_python<RatingCurveSegmentVector>();
    // overloads for rating_curve_function::flow
    double (shyft::time_series::rating_curve_function::*rcf_flow_val)(double)
      const = &shyft::time_series::rating_curve_function::flow;
    std::vector<double> (shyft::time_series::rating_curve_function::*rcf_flow_vec)(std::vector<double> const &)
      const = &shyft::time_series::rating_curve_function::flow;
    // overloads for rating_curve_function::add_segment
    void (shyft::time_series::rating_curve_function::*rcf_add_args)(
      double, double, double, double) = &shyft::time_series::rating_curve_function::add_segment;
    void (shyft::time_series::rating_curve_function::*rcf_add_obj)(
      rating_curve_segment const &) = &shyft::time_series::rating_curve_function::add_segment;

    py::class_<shyft::time_series::rating_curve_function>(
      "RatingCurveFunction",
      doc.intro("Combine multiple RatingCurveSegments into a rating function.")
        .intro("")
        .intro("RatingCurveFunction aggregates multiple RatingCurveSegments and routes.")
        .intro("computation calls to the correct segment based on the water level to compute for.")
        .see_also("RatingCurveSegment, RatingCurveParameters")(),
      py::init<>(py::arg("self"), doc.intro("Defines a new empty rating curve function.")()))
      .def(py::init< RatingCurveSegmentVector const &, bool>(
        (py::arg("self"), py::arg("segments"), py::arg("is_sorted") = true),
        doc.intro("constructs a function from a segment-list")()))
      .def(
        "size",
        &shyft::time_series::rating_curve_function::size,
        (py::arg("self")),
        "Get the number of RatingCurveSegments composing the function.")
      .def(
        "add_segment",
        rcf_add_args,
        (py::arg("self"), py::arg("lower"), py::arg("a"), py::arg("b"), py::arg("c")),
        doc.intro("Add a new curve segment with the given parameters.").see_also("RatingCurveSegment")())
      .def(
        "add_segment",
        rcf_add_obj,
        (py::arg("self"), py::arg("segment")),
        doc.intro("Add a new curve segment as a copy of an exting.").see_also("RatingCurveSegment")())
      // ref. note above regarding the order of overloaded member functions
      .def(
        "flow",
        rcf_flow_vec,
        (py::arg("self"), py::arg("levels")),
        doc.intro("Compute flow for a range of water levels.")
          .parameters()
          .parameter("levels", "DoubleVector", "Range of water levels to compute flow for.")())
      .def(
        "flow",
        rcf_flow_val,
        (py::arg("self"), py::arg("level")),
        doc.intro("Compute flow for the given level.")
          .parameters()
          .parameter("level", "float", "Water level to compute flow for.")())
      .def(
        "__iter__",
        py::range(&shyft::time_series::rating_curve_function::cbegin, &shyft::time_series::rating_curve_function::cend),
        "Constant iterator. Invalidated on calls to .add_segment")
      .def("__str__", &shyft::time_series::rating_curve_function::operator std::string, "Stringify the function.");

    py::class_<rating_curve_t_f>(
      "RatingCurveTimeFunction",
      doc.intro("Composed of time t and RatingCurveFunction")(),
      py::init<>(py::arg("self"), doc.intro("Defines empty pair t,f")))
      .def(py::init<utctime, rating_curve_function const &>(
        (py::arg("self"), py::arg("t"), py::arg("f")),
        doc.intro("Construct an object with function f valid from time t")
          .parameters()
          .parameter("t", "int", "epoch time in 1970 utc [s]")
          .parameter("f", "RatingCurveFunction", "the function")()))
      .def_readwrite("t", &rating_curve_t_f::t, doc.intro("time: .f is valid from t, the epoch 1970[s] time ")())
      .def_readwrite("f", &rating_curve_t_f::f, doc.intro("RatingCurveFunction: the rating curve function")());

    typedef vector<rating_curve_t_f> RatingCurveTimeFunctionVector;
    py::class_<RatingCurveTimeFunctionVector>(
      "RatingCurveTimeFunctions",
      doc.intro("A typed list of RatingCurveTimeFunction elements")(),
      py::init<>(py::arg("self"), doc.intro("Defines empty list pair t,f")()))
      .def(py::vector_indexing_suite<RatingCurveTimeFunctionVector>())
      .def(py::init< RatingCurveTimeFunctionVector const &>((py::arg("clone_me"))));
    py_api::iterable_converter().from_python<RatingCurveTimeFunctionVector>();

    // overloads for rating_curve_function::flow
    double (shyft::time_series::rating_curve_parameters::*rcp_flow_val)(utctime, double)
      const = &shyft::time_series::rating_curve_parameters::flow;
    std::vector<double> (shyft::time_series::rating_curve_parameters::*rcp_flow_ts)(apoint_ts const &)
      const = &shyft::time_series::rating_curve_parameters::flow<apoint_ts>;
    // overloads for rating_curve_function::add_segment
    rating_curve_parameters &(shyft::time_series::rating_curve_parameters::*rcp_add_obj)(
      utctime, rating_curve_function const &) = &shyft::time_series::rating_curve_parameters::add_curve;

    py::class_<shyft::time_series::rating_curve_parameters>(
      "RatingCurveParameters",
      doc.intro("Parameter pack controlling rating level computations.")
        .intro("")
        .intro("A parameter pack encapsulates multiple RatingCurveFunction's with time-points.")
        .intro("When used with a TimeSeries representing level values it maps computations for")
        .intro("each level value onto the correct RatingCurveFunction, which again maps onto the")
        .intro("correct RatingCurveSegment for the level value.")
        .see_also("RatingCurveSegment, RatingCurveFunction, TimeSeries.rating_curve")(),
      py::no_init // init<>((py::arg("self")),"Defines a empty RatingCurveParameter instance")
      )
      .def("__init__", py::make_constructor(&rcp_ext::create_default), "Defines a empty RatingCurveParameter instance")
      .def(
        "__init__",
        py::make_constructor(&rcp_ext::create_from_t_f_list, py::default_call_policies(), (py::arg("t_f_list"))),
        doc.intro("create parameters in one go from list of RatingCurveTimeFunction elements")
          .parameters()
          .parameter("t_f_list", "RatingCurveTimeFunctions", "a list of RatingCurveTimeFunction elements")())
      .def(
        "add_curve",
        rcp_add_obj,
        (py::arg("self"), py::arg("t"), py::arg("curve")),
        doc.intro("Add a curve to the parameter pack.")
          .parameters()
          .parameter("t", "RatingCurveFunction", "First time-point the curve is valid for.")
          .parameter("curve", "RatingCurveFunction", "RatingCurveFunction to add at t.")
          .returns("self", "RatingCurveParameters", " to allow chaining building functions")(),
        py::return_value_policy<py::reference_existing_object>()

          )
      .def(
        "flow",
        rcp_flow_val,
        (py::arg("self"), py::arg("t"), py::arg("level")),
        doc.intro("Compute the flow at a specific time point.")
          .parameters()
          .parameter("t", "utctime", "Time-point of the level value.")
          .parameter("level", "float", "Level value at t.")
          .returns(
            "flow",
            "float",
            "Flow correcponding to input level at t, `nan` if level is less than the least water level of the first "
            "segment or before the time of the first rating curve function.")())
      .def(
        "flow",
        rcp_flow_ts,
        (py::arg("self"), py::arg("ts")),
        doc.intro("Compute the flow at a specific time point.")
          .parameters()
          .parameter("ts", "TimeSeries", "Time series of level values.")
          .returns(
            "flow",
            "DoubleVector",
            "Flow correcponding to the input levels of the time-series, `nan` where the level is less than the least "
            "water level of the first segment and for time-points before the first rating curve function.")())
      .def(
        "__iter__",
        py::range(
          &shyft::time_series::rating_curve_parameters::cbegin, &shyft::time_series::rating_curve_parameters::cend),
        "Constant iterator. Invalidated on calls to .add_curve")
      .def("__str__", &shyft::time_series::rating_curve_parameters::operator std::string, "Stringify the parameters.");
  }

  static void expose_ice_packing_parameters() {
    py::enum_<shyft::time_series::ice_packing_temperature_policy>(
      "ice_packing_temperature_policy",
      doc.intro("Policy enum to specify how `TimeSeries.ice_packing` handles missing temperature values.")
        .intro("")
        .intro("The enum defines three values:")
        .intro(" * `DISALLOW_MISSING` disallows any missing values. With this policy whenever a NaN value is "
               "encountered,")
        .intro("   or the window of values to consider extends outside the range of the time series, a NaN value will "
               "be")
        .intro("   written to the result time-series.")
        .intro(" * `ALLOW_INITIAL_MISSING` disallows explicit NaN values, but allows the window of values to consider")
        .intro("   to expend past the range of the time-series for the initial values.")
        .intro(" * `ALLOW_ANY_MISSING` allow the window of values to contain NaN values, averaging what it can.")
        .intro("   Only if all the values in the window is NaN, the result wil be NaN.")())
      .value("DISALLOW_MISSING", shyft::time_series::ice_packing_temperature_policy::DISALLOW_MISSING)
      .value("ALLOW_INITIAL_MISSING", shyft::time_series::ice_packing_temperature_policy::ALLOW_INITIAL_MISSING)
      .value("ALLOW_ANY_MISSING", shyft::time_series::ice_packing_temperature_policy::ALLOW_ANY_MISSING)
      .export_values();

    py::class_<shyft::time_series::ice_packing_parameters>(
      "IcePackingParameters",
      doc.intro("Parameter pack controlling ice packing computations.")
        .intro("See `TimeSeries.ice_packing` for usage.")(),
      py::init<core::utctimespan, double>(
        (py::arg("self"), py::arg("threshold_window"), py::arg("threshold_temperature")),
        doc.intro("Defines a paramter pack for ice packing detection.")
          .intro("")
          .parameters()
          .parameter("threshold_window", "utctime", "Positive,  seconds for the lookback window.")
          .parameter("threshold_temperature", "float", "Floating point threshold temperature.")()))
      .def(py::init<int64_t, double>(
        (py::arg("self"), py::arg("threshold_window"), py::arg("threshold_temperature")),
        doc.intro("Defines a paramter pack for ice packing detection.")
          .intro("")
          .parameters()
          .parameter("threshold_window", "int", "Positive integer seconds for the lookback window.")
          .parameter("threshold_temperature", "float", "Floating point threshold temperature.")()))
      .def_readwrite(
        "threshold_window",
        &shyft::time_series::ice_packing_parameters::window,
        doc.intro("time: The period back in seconds for which the average temperature is computed when")
          .intro("looking for ice packing.")())
      .def_readwrite(
        "threshold_temperature",
        &shyft::time_series::ice_packing_parameters::threshold_temp,
        doc.intro("float: The threshold temperature for ice packing to occur. Ice packing will occur")
          .intro("when the average temperature in the `window` period is less than the threshold.")())
      .def(py::self == py::self);

    py::class_<shyft::time_series::dd::ice_packing_recession_parameters>(
      "IcePackingRecessionParameters",
      doc.intro("Parameter pack controlling ice packing recession computations.")
        .intro("See `TimeSeries.ice_packing_recession` for usage.")(),
      py::init<double, double>(
        (py::arg("self"), py::arg("alpha"), py::arg("recession_minimum")),
        doc.intro("Defines a parameter pack for ice packing reduction using a simple recession for the water-flow.")
          .intro("")
          .parameters()
          .parameter("alpha", "float", "Recession curve curving parameter.")
          .parameter("recession_minimum", "float", "Minimum value for the recession.")()))
      .def_readwrite(
        "alpha",
        &shyft::time_series::dd::ice_packing_recession_parameters::alpha,
        doc.intro("float: Parameter controlling the curving of the recession curve.")())
      .def_readwrite(
        "recession_minimum",
        &shyft::time_series::dd::ice_packing_recession_parameters::recession_minimum,
        doc.intro("float: The minimum value of the recession curve.")())
      .def(py::self == py::self);
  }

  static void expose_correlation_functions() {
    py::def(
      "kling_gupta",
      kling_gupta,
      (py::arg("observation_ts"),
       py::arg("model_ts"),
       py::arg("time_axis"),
       py::arg("s_r"),
       py::arg("s_a"),
       py::arg("s_b")),
      doc.intro("Computes the kling-gupta KGEs correlation for the two time-series over the specified time_axis")
        .parameters()
        .parameter("observed_ts", "TimeSeries", "the observed time-series")
        .parameter("model_ts", "TimeSeries", "the time-series that is the model simulated / calculated ts")
        .parameter("time_axis", "TimeAxis", "the time-axis that is used for the computation")
        .parameter("s_r", "float", "the kling gupta scale r factor(weight the correlation of goal function)")
        .parameter("s_a", "float", "the kling gupta scale a factor(weight the relative average of the goal function)")
        .parameter(
          "s_b", "float", "the kling gupta scale b factor(weight the relative standard deviation of the goal function)")
        .returns("KGEs", "float", "The  KGEs= 1-EDs that have a maximum at 1.0")());

    py::def(
      "nash_sutcliffe",
      nash_sutcliffe,
      (py::arg("observation_ts"), py::arg("model_ts"), py::arg("time_axis")),
      doc.intro("Computes the Nash-Sutcliffe model effiency coefficient (n.s) ")
        .intro("for the two time-series over the specified time_axis\n")
        .intro("Ref:  http://en.wikipedia.org/wiki/Nash%E2%80%93Sutcliffe_model_efficiency_coefficient \n")
        .parameters()
        .parameter("observed_ts", "TimeSeries", "the observed time-series")
        .parameter("model_ts", "TimeSeries", "the time-series that is the model simulated / calculated ts")
        .parameter("time_axis", "TimeAxis", "the time-axis that is used for the computation")
        .returns("ns", "float", "The  n.s performance, that have a maximum at 1.0")());
  }

  static void expose_periodic_ts() {
    py::def(
      "create_periodic_pattern_ts",
      create_periodic_pattern_ts,
      (py::arg("pattern"), py::arg("dt"), py::arg("t0"), py::arg("ta")),
      doc.intro("Create a Timeseries by repeating the pattern-specification")
        .parameters()
        .parameter("pattern", "DoubleVector", "the value-pattern as a sequence of values")
        .parameter("dt", "int", "number of seconds between the pattern values, e.g. deltahours(3)")
        .parameter("t0", "utctime", "specifies the start-time of the pattern")
        .parameter(
          "ta",
          "TimeAxis",
          "the time-axis for which the pattern is repeated\n\te.g. your pattern might be 8 3h values,and you could "
          "supply\n\ta time-axis 'ta' at hourly resolution")());
  }

  static void expose_krls() {

    using krls_rbf_predictor = shyft::prediction::krls_rbf_predictor;

    py::class_<krls_rbf_predictor>(
      "KrlsRbfPredictor",
      doc
        .intro("Time-series predictor using the KRLS algorithm with radial basis functions.\n"
               "\n"
               "The KRLS (Kernel Recursive Least-Squares) algorithm is a kernel regression\n"
               "algorithm for aproximating data, the implementation used here is from:\n"
               "\n"
               ".. _DLib:\n"
               "    http://dlib.net/ml.html#krls\n"
               "\n"
               "This predictor uses KRLS with radial basis functions (RBF).")
        .pure("Other related ")
        .ref_meth("shyft.time_series.TimeSeries.krls_interpolation")
        .ref_meth("shyft.time_series.TimeSeries.TimeSeries.get_krls_predictor")
        .intro("\n\n")
        .intro("Examples:\n")
        .intro(">>>")
        .intro(">>> import numpy as np")
        .intro(">>> import matplotlib.pyplot as plt")
        .intro(">>> from shyft.time_series import (")
        .intro("...     Calendar, utctime_now, deltahours,")
        .intro("...     TimeAxis, TimeSeries,")
        .intro("...     KrlsRbfPredictor")
        .intro("... )")
        .intro(">>>")
        .intro(">>> # setup")
        .intro(">>> cal = Calendar()")
        .intro(">>> t0 = utctime_now()")
        .intro(">>> dt = deltahours(3)")
        .intro(">>> n = 365*8  # one year")
        .intro(">>>")
        .intro(">>> # ready plot")
        .intro(">>> fig, ax = plt.subplots()")
        .intro(">>> ")
        .intro(">>> # shyft objects")
        .intro(">>> ta = TimeAxis(t0, dt, n)")
        .intro(">>> pred = KrlsRbfPredictor(")
        .intro("...     dt=deltahours(8),")
        .intro("...     gamma=1e-5,  # NOTE: this should be 1e-3 for real data")
        .intro("...     tolerance=0.001")
        .intro("... )")
        .intro(">>>")
        .intro(">>> # generate data")
        .intro(">>> total_series = 4")
        .intro(">>> data_range = np.linspace(0, 2*np.pi, n)")
        .intro(">>> ts = None  # to store the final data-ts")
        .intro(">>> # -----")
        .intro(">>> for i in range(total_series):")
        .intro(">>>     data = np.sin(data_range) + (np.random.random(data_range.shape) - 0.5)/5")
        .intro(">>>     ts = TimeSeries(ta, data)")
        .intro(">>>     # -----")
        .intro(">>>     training_mse = pred.train(ts)  # train the predictor")
        .intro(">>>     # -----")
        .intro(">>>     print(f'training step {i+1:2d}: mse={training_mse}')")
        .intro(">>>     ax.plot(ta.time_points[:-1], ts.values, 'bx')  # plot data")
        .intro(">>>")
        .intro(">>> # prediction")
        .intro(">>> ts_pred = pred.predict(ta)")
        .intro(">>> ts_mse = pred.mse_ts(ts, points=3)  # mse using 7 point wide filter")
        .intro(">>>                                     # (3 points before/after)")
        .intro(">>>")
        .intro(">>> # plot interpolation/predicton on top of results")
        .intro(">>> ax.plot(ta.time_points[:-1], ts_mse.values, '0.6', label='mse')")
        .intro(">>> ax.plot(ta.time_points[:-1], ts_pred.values, 'r-', label='prediction')")
        .intro(">>> ax.legend()")
        .intro(">>> plt.show()")())
      .def(py::init<core::utctimespan, double, double, std::size_t>(
        (py::arg("self"),
         py::arg("dt"),
         py::arg("gamma") = 1.E-3,
         py::arg("tolerance") = 0.01,
         py::arg("size") = 1000000u),
        doc.intro("Construct a new predictor.")
          .parameters()
          .parameter(
            "dt",
            "float",
            "The time-step in seconds the predictor is specified for.\n"
            "    Note that this does not put a limit on time-axes used, but for best results it should be\n"
            "    approximatly equal to the time-step of time-axes used with the predictor. In addition it\n"
            "    should not be to long, else you will get poor results. Try to keep the dt less than a day,\n"
            "    3-8 hours is usually fine.")
          .parameter(
            "gamma",
            "float (optional)",
            "Determines the width of the radial basis functions for\n"
            "    the KRLS algorithm. Lower values mean wider basis functions, wider basis functions means faster\n"
            "    computation but lower accuracy. Note that the tolerance parameter also affects speed and accurcy.\n"
            "    A large value is around `1E-2`, and a small value depends on the time step. By using values larger\n"
            "    than `1E-2` the computation will probably take to long. Testing have reveled that `1E-3` works great\n"
            "    for a time-step of 3 hours, while a gamma of `1E-2` takes a few minutes to compute. Use `1E-4` for a\n"
            "    fast and tolerably accurate prediction.\n"
            "    Defaults to `1E-3`")
          .parameter(
            "tolerance",
            "float (optional)",
            "The krls training tolerance. Lower values makes the prediction more accurate,\n"
            "    but slower. This typically have less effect than gamma, but is usefull for tuning. Usually it should "
            "be\n"
            "    either `0.01` or `0.001`.\n"
            "    Defaults to `0.01`")
          .parameter(
            "size",
            "int (optional)",
            "The size of the \"memory\" of the predictor. The default value is\n"
            "    usually enough. Defaults to `1000000`.")()))
      .def(
        "train",
        &krls_rbf_predictor::train<apoint_ts>,
        (py::arg("self"),
         py::arg("ts"),
         py::arg("offset") = 0u,
         py::arg("count") = std::numeric_limits<std::size_t>::max(),
         py::arg("stride") = 1u,
         py::arg("iterations") = 1u,
         py::arg("mse_tol") = 0.001),
        doc.intro("Train the predictor using samples from ts.")
          .parameters()
          .parameter("ts", "TimeSeries", "Time-series to train on.")
          .parameter("offset", "int (optional)", "Positive offset from the start of the time-series. Default to 0.")
          .parameter("count", "int (optional)", "Positive number of samples to to use. Default to the maximum value.")
          .parameter("stride", "int (optional)", "Positive stride between samples from the time-series. Defaults to 1.")
          .parameter(
            "iterations", "int (optional)", "Positive maximum number of times to train on the samples. Defaults to 1.")
          .parameter(
            "mse_tol",
            "float (optional)",
            "Positive tolerance for the mean-squared error over the training data.\n"
            "    If the mse after a training session is less than this skip training further. Defaults to `1E-9`.")
          .returns(
            "mse", "float (optional)", "Mean squared error of the predictor relative to the time-series trained on.")())
      .def(
        "predict",
        &krls_rbf_predictor::predict<apoint_ts, shyft::time_series::dd::gta_t>,
        (py::arg("self"), py::arg("ta")),
        doc.intro("Predict a time-series for for time-axis.")
          .notes()
          .note("The predictor will predict values outside the range of the values it is trained on, but these")
          .note("values will often be zero. This may also happen if there are long gaps in the training data")
          .note("and you try to predict values for the gap. Using wider basis functions partly remedies this,")
          .note("but makes the prediction overall less accurate.")
          .parameters()
          .parameter("ta", "TimeAxis", "Time-axis to predict values for.")
          .returns("ts", "TimeSeries", "Predicted time-series.")
          .see_also("KrlsRbfPredictor.mse_ts, KrlsRbfPredictor.predictor_mse")())
      .def(
        "mse_ts",
        &krls_rbf_predictor::mse_ts<apoint_ts, apoint_ts>,
        (py::arg("self"), py::arg("ts"), py::arg("points") = 0u),
        doc.intro("Compute a mean-squared error time-series of the predictor relative to the supplied ts.")
          .parameters()
          .parameter("ts", "TimeSeries", "Time-series to compute mse against.")
          .parameter(
            "points",
            "int (optional)",
            "Positive number of extra points around each point to use for mse.\n    Defaults to 0.")
          .returns("mse_ts", "TimeSeries", "Time-series with mean-squared error values.")
          .see_also("KrlsRbfPredictor.predictor_mse, KrlsRbfPredictor.predict")())
      .def(
        "predictor_mse",
        &krls_rbf_predictor::predictor_mse<apoint_ts>,
        (py::arg("self"),
         py::arg("ts"),
         py::arg("offset") = 0u,
         py::arg("count") = std::numeric_limits<std::size_t>::max(),
         py::arg("stride") = 1u),
        doc.intro("Compute the predictor mean-squared prediction error for count first from ts.")
          .parameters()
          .parameter("ts", "TimeSeries", "Time-series to compute mse against.")
          .parameter("offset", "int (optional)", "Positive offset from the start of the time-series. Default to 0.")
          .parameter(
            "count",
            "int (optional)",
            "Positive number of samples from the time-series to to use.\n    Default to the maximum value.")
          .parameter("stride", "int (optional)", "Positive stride between samples from the time-series. Defaults to 1.")
          .see_also("KrlsRbfPredictor.predict, KrlsRbfPredictor.mse_ts")())
      .def(
        "clear",
        &krls_rbf_predictor::clear,
        (py::arg("self")),
        doc.intro("Clear all training data from the predictor.")());
  }

  static void *np_import() {
    import_array();
    return nullptr;
  }

  void timeseries() {
    py::enum_<time_series::ts_point_fx>(
      "point_interpretation_policy",
      doc.intro("Determines how to interpret the points in a timeseries when interpreted as a function of time, "
                "f(t)")())
      .value("POINT_INSTANT_VALUE", time_series::POINT_INSTANT_VALUE)
      .value("POINT_AVERAGE_VALUE", time_series::POINT_AVERAGE_VALUE)
      .export_values();
    py::enum_<time_series::statistics_property>(
      "statistics_property",
      doc.intro("special values for percentiles evaluation to extract non percentile values from a set")())
      .value("AVERAGE", time_series::statistics_property::AVERAGE)
      .value("MIN_EXTREME", time_series::statistics_property::MIN_EXTREME)
      .value("MAX_EXTREME", time_series::statistics_property::MAX_EXTREME);

    py::enum_<time_series::dd::extend_ts_fill_policy>(
      "extend_fill_policy",
      "Ref TimeSeries.extend function, this policy determines how to represent values in a gap\n"
      "EPF_NAN : use nan values in the gap\n"
      "EPF_LAST: use the last value before the gap\n"
      "EPF_FILL: use a supplied value in the gap\n")
      .value("FILL_NAN", time_series::dd::extend_ts_fill_policy::EPF_NAN)
      .value("USE_LAST", time_series::dd::extend_ts_fill_policy::EPF_LAST)
      .value("FILL_VALUE", time_series::dd::extend_ts_fill_policy::EPF_FILL)
      .export_values();
    py::enum_<time_series::dd::extend_ts_split_policy>(
      "extend_split_policy",
      "Ref TimeSeries.extend function, this policy determines where to split/shift from one ts to the other\n"
      "EPS_LHS_LAST : use nan values in the gap\n"
      "EPS_RHS_FIRST: use the last value before the gap\n"
      "EPS_VALUE    : use a supplied value in the gap\n")
      .value("LHS_LAST", time_series::dd::extend_ts_split_policy::EPS_LHS_LAST)
      .value("RHS_FIRST", time_series::dd::extend_ts_split_policy::EPS_RHS_FIRST)
      .value("AT_VALUE", time_series::dd::extend_ts_split_policy::EPS_VALUE)
      .export_values();

    py::enum_<time_series::convolve_policy>(
      "convolve_policy",
      "Ref TimeSeries.convolve_w function, this policy determine how to handle initial conditions\n"
      "`USE_NEAREST`: value(0) is used for all values before value(0), and"
      " value(n-1) is used for all values after value(n-1) == 'mass preserving'\n"
      "`USE_ZERO` : use zero for all values before value(0) or after value(n-1) == 'shape preserving'\n"
      "`USE_NAN`  : nan is used for all values outside the ts\n"
      "`BACKWARD` : filter is 'backward looking' == boundary handling in the beginning of ts\n"
      "`FORWARD`  : filter is 'forward looking' == boundary handling in the end of ts\n"
      "`CENTER`   : filter is centered == boundary handling in both ends\n")
      .value("USE_NEAREST", time_series::convolve_policy::USE_NEAREST)
      .value("USE_ZERO", time_series::convolve_policy::USE_ZERO)
      .value("USE_NAN", time_series::convolve_policy::USE_NAN)
      .value("BACKWARD", time_series::convolve_policy::BACKWARD)
      .value("FORWARD", time_series::convolve_policy::FORWARD)
      .value("CENTER", time_series::convolve_policy::CENTER);
    py::enum_<time_series::dd::derivative_method>(
      "derivative_method",
      doc.intro("Ref. the .derivative time-series function, this defines how to compute the")
        .intro("derivative of a given time-series")())
      .value("DEFAULT", time_series::dd::derivative_method::default_diff)
      .value("FORWARD", time_series::dd::derivative_method::forward_diff)
      .value("BACKWARD", time_series::dd::derivative_method::backward_diff)
      .value("CENTER", time_series::dd::derivative_method::center_diff)
      .export_values();
    py::enum_<time_series::dd::interpolation_scheme>(
      "interpolation_scheme", doc.intro("Interpolation methods used by TimeSeries.transform")())
      .value("SCHEME_POLYNOMIAL", time_series::dd::interpolation_scheme::SCHEME_POLYNOMIAL)
      .value("SCHEME_LINEAR", time_series::dd::interpolation_scheme::SCHEME_LINEAR)
      .value("SCHEME_CATMULL_ROM", time_series::dd::interpolation_scheme::SCHEME_CATMULL_ROM)
      .export_values();
    py::class_<time_series::point>("Point", "A timeseries point specifying utctime t and value v")
      .def(py::init<utctime, double>((py::arg("t"), py::arg("v"))))
      .def_readwrite("t", &time_series::point::t, "time: utctime of the point")
      .def_readwrite("v", &time_series::point::v, "float: the value of the point");
    point_ts<time_axis::fixed_dt>(
      "TsFixed",
      "A time-series with a fixed delta t time-axis, used by the Shyft core,see also TimeSeries for end-user ts");
    point_ts<time_axis::point_dt>(
      "TsPoint",
      "A time-series with a variable delta time-axis, used by the Shyft core,see also TimeSeries for end-user ts");
    TsFactory();

    np_import();

    // used by TimeSeries.transform
    numpy_boost_python_register_type<double, 2>();

    expose_rating_curve_classes();
    expose_ice_packing_parameters();
    expose_apoint_ts();
    expose_periodic_ts();
    expose_correlation_functions();
    expose_core_ts_vector();
    expose_ats_vector();
    expose_krls();
  }
}
