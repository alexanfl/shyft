#pragma once
#include <Python.h>

namespace shyft::pyapi {

  /** @brief scoped GIL release
   *
   * This class do a scoped gil-release when declared
   *
   * - useful for typical c++  io ops that takes time.
   * when you call from python to c++, inside the impl. in py expose part.
   *
   */
  struct scoped_gil_release {
    scoped_gil_release() noexcept {
      py_thread_state = PyEval_SaveThread();
    }

    ~scoped_gil_release() noexcept {
      PyEval_RestoreThread(py_thread_state);
    }

    scoped_gil_release(scoped_gil_release const &) = delete;
    scoped_gil_release(scoped_gil_release &&) = delete;
    scoped_gil_release &operator=(scoped_gil_release const &) = delete;
   private:
    PyThreadState *py_thread_state;
  };

  /** @brief scoped GIL acquire
   *
   * This class do a scoped gil aquire,
   *
   * - useful when doing callbacks from c++ threads to python
   * use it before any callbacks to python to ensure no race conditions on py-interpreter.
   */
  struct scoped_gil_aquire {
    scoped_gil_aquire() noexcept {
      py_state = PyGILState_Ensure();
    }

    ~scoped_gil_aquire() noexcept {
      PyGILState_Release(py_state);
    }

    scoped_gil_aquire(scoped_gil_aquire const &) = delete;
    scoped_gil_aquire(scoped_gil_aquire &&) = delete;
    scoped_gil_aquire &operator=(scoped_gil_aquire const &) = delete;
   private:
    PyGILState_STATE py_state;
  };


}
