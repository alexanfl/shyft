/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

namespace shyft::energy_market::hydro_power {

  std::shared_ptr<reservoir> const &
    reservoir::input_from(std::shared_ptr<reservoir> const & me, std::shared_ptr<waterway> const & w) {
    connect(w->shared_from_this(), me->shared_from_this());
    return me;
  }

  std::shared_ptr<reservoir> const & reservoir::output_to(
    std::shared_ptr<reservoir> const & me,
    std::shared_ptr<waterway> const & w,
    connection_role role) {
    connect(me->shared_from_this(), role, w->shared_from_this());
    return me;
  }

  std::shared_ptr<reservoir> reservoir::shared_from_this() const {
    auto hps = hps_();
    return hps ? shared_from_me(this, hps->reservoirs) : nullptr;
  }

  bool reservoir::operator==(reservoir const & o) const {
    return this == &o || (id_base::operator==(o) && equal_structure(o)); // same attribs, and closure/neighbours
  }

}
