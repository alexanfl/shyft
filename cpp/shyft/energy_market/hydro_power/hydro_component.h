/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/formatters.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::hydro_power {

  using shyft::core::utctime;

  /** connection role for the first segment of a possibly longer list water-routes
      - main: a main waterroute, usually a production water-route
      - bypass: a bypass, in the meaning of bypass relative a hydro-aggregate/or production unit
      - flood: water escaping a reservoir uncontrolled, at high water-levels
      - input: input, to a hydro-component
  */
  SHYFT_DEFINE_ENUM(connection_role, std::uint8_t, (main, bypass, flood, input));

  struct hydro_power_system;
  struct hydro_component;
  struct waterway;
  struct gate;
  struct reservoir;
  struct unit;
  struct power_plant;

  /** @brief A hydro connection to connect hydro-components
   *
   * A hydro-component have a flexible number of upstream and downstream connections.
   * each of them describes the intended role that connections plays.
   *
   * A connection consists of a reference to the target component
   * and the connection-role that connection plays relative 'this' object.
   *
   * The connection objects are always two-way, and mutual, so if there is a path from
   * a reservoir 'R' to a a tunnel 'T',
   * The reservoir.downstream points to tunnel, and the tunnel.upstream points to
   * reservoir.
   *
   * \see hydro_component
   *
   */
  struct hydro_connection {
    hydro_connection(connection_role a_role, std::shared_ptr<hydro_component> const &a_target)
      : role(a_role)
      , target(a_target) {
    }

    hydro_connection() = default;
    connection_role role{connection_role::main}; // e.g. : You are at a reservoir, downstream bypass to..
    std::shared_ptr<hydro_component> target;     // weak reference to  reservoir|aggregate_|water_route_

    SHYFT_DEFINE_STRUCT(hydro_connection, (), (role, target));

    bool has_target() const {
      return target != nullptr;
    }

    auto operator<=>(hydro_connection const &) const = default;

    x_serialize_decl();
  };

  /** @brief Base for hydro connectable components
   *
   * Contains the functionality and rules for hydro components, reservoir, water-routes and aggregates(turbine..)
   * A hydro component have a unique id, a name (could be unique), up-stream and down-stream connections.
   *
   * Notice that connections are mutual pairs, so if 'A' is upstream 'B', then 'B' is downstream 'A'.
   *
   * like this:
   *
   *   A
   *      .downstream[0] (main)--> B
   *
   *   B
   *      .upstream[0](input) --> A
   *
   *
   * About up-stream/down-stream:
   *
   *  -# It's a convention, - water can flow in both directions(reservoir ..tunn.. reservoir, determined by levels).
   *  -# It's questionable if we really need up/down, or we could simplify structure with just connection(and use
   * current state to determine up/down..)
   *  -# The convention is 'major-upstream/down-stream' where water can flow bidirectional.
   *  -# It's used to traverse from hydro-component in some 'known'/intended direction.
   *
   */
  struct hydro_component : id_base {
   protected:
    hydro_component() = default; // serialization only
   public:

    SHYFT_DEFINE_STRUCT(hydro_component, (id_base), ());

    hydro_component(
      int id,
      std::string const &a_name,
      std::string const &json = "",
      std::shared_ptr<hydro_power_system> a_hps = nullptr)
      : id_base{id, a_name, json, {}, {}, {}}
      , hps{a_hps} {
    }

    void clear();
    virtual ~hydro_component();
    hydro_component(hydro_component const &) = delete;
    hydro_component(hydro_component &&) = delete;
    hydro_component &operator=(hydro_component const &) = delete;

    auto hps_() const {
      return hps.lock();
    }

    std::weak_ptr<hydro_power_system> hps;     ///< reference up to the 'owning' hydro-power system.
    std::vector<hydro_connection> upstreams;   ///< upstream connections
    std::vector<hydro_connection> downstreams; ///< downstream connections.

    /** returns true if the other hydro component have the same hydro-connects,name,id as this*/
    virtual bool equal_structure(hydro_component const &o) const;

    //-- connection rules that enforces correct mutual connects/disconnects
    static void connect(
      std::shared_ptr<hydro_component> const &upstream,
      connection_role u_role,
      std::shared_ptr<hydro_component> const &downstream,
      connection_role d_role);
    static void connect(
      std::shared_ptr<reservoir> const &upstream,
      connection_role u_role,
      std::shared_ptr<waterway> const &downstream);
    static void connect(std::shared_ptr<unit> const &upstream, std::shared_ptr<waterway> const &downstream);
    static void connect(std::shared_ptr<waterway> const &upstream_tunnel, std::shared_ptr<unit> const &power_station);
    static void connect(std::shared_ptr<waterway> const &upstream, std::shared_ptr<reservoir> const &downstream);
    static void connect(std::shared_ptr<waterway> const &upstream, std::shared_ptr<waterway> const &junction);
    static void disconnect(std::shared_ptr<hydro_component> const &c1, std::shared_ptr<hydro_component> const &c2);
    void disconnect_from(hydro_component &o);
    x_serialize_decl();
  };

  /** locate a shared_ptr<T>  from T* m searching through v, or return nullptr */
  template <class T>
  std::shared_ptr<T> shared_from_me(T const *me, std::vector<std::shared_ptr<T>> const &v) {
    for (auto const &p : v)
      if (p.get() == me)
        return p;
    return nullptr;
  }

}

x_serialize_export_key(shyft::energy_market::hydro_power::hydro_connection);
x_serialize_export_key(shyft::energy_market::hydro_power::hydro_component);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::hydro_component, 1); // proper handling of id_base serialization

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::hydro_power::connection_role);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::hydro_power::hydro_connection);

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::hydro_component, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::hydro_component, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::hydro_component>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::hydro_component, Char> { };
