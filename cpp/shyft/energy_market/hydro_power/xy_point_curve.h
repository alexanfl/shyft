/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <cmath>
#include <map>
#include <memory>
#include <limits>
#include <ranges>
#include <vector>
#include <set>
#include <type_traits>

#include <boost/container/flat_map.hpp>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/transform_spline_ts.h>

namespace shyft::energy_market::hydro_power {

  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::interpolation_scheme;

  /** point as in context of hydro-power, describing curve-shapes,
   *
   * it's used as building block for the xy- or z-xy curves.
   *
   */
  struct point {
    double x{0.0};
    double y{0.0};

    SHYFT_DEFINE_STRUCT(point, (), (x, y));

    auto operator<=>(point const &) const = default;

    x_serialize_decl();
  };

  /** @brief a curve described using points, piecewise linear
   *
   *  xy_point_curve,  conceptually y=F(x), and x=Finv(y)
   *  represented as two or more points, piecewise linear between points
   *   + extrapolated values at the end.
   *  Used as base building block for
   *    a) Reservoir Volume description (and extra requirement is asc masl/Mm3 here..)
   *    b) Pure Overflow description, corresponding to classic overflow gate ( extra requirement,masl/m3/s is ascending)
   *    c) Generator effiency curve ( and now it's now the Finv(y) does not map to one value..)
   *  List of xy_point_curves, 3D functions, x=f(z,y)
   *    d) A set of xy_point_curves is used for turbine effiency curves, each curve for a referernce height. eff=
   * f(reference_height,m3/s) e) A set of xy_point_curves is used for complex flow curve tables between reservoirs flow=
   * f(gateopening, difference_in_water_levels) f) A set of xy_point_curves is used for to describe flow between
   * reservoirs, R1,R2 , flow= f( R1.level, (R1-R2).level) The class needs points with X-ascending & unique (sorting
   * points at the input).
   */
  struct xy_point_curve {
    std::vector<point> points; ///< point in a point based description(could be functional..)

    SHYFT_DEFINE_STRUCT(xy_point_curve, (), (points));

    /** @return true if we have two ordered points or more,
     *  and X,Y are increasing, suitable for reservoir curves check..
     */
    bool is_xy_mono_increasing() const;

    /** @return true if we have two ordered points or more,
     *  and y is strictly increasing or decreasing.
     *  This property is required for invertibility.
     */
    bool is_xy_invertible() const;

    /** @return true if we have two ordered points or more,
     *  and every point (x, y) is on the convex boundary.
     */
    bool is_xy_convex() const;

    /** Interpreting the points, figure out Y, given X
     * note1:extrapolating the points if asking outside range.
     *
     * @param from_x the x-coordinate
     * @return the calculated y-value
     */
    double calculate_y(double from_x) const;
    apoint_ts calculate_y(apoint_ts const &x, interpolation_scheme = interpolation_scheme::SCHEME_LINEAR) const;

    /** Inverse of calculate_y
     *
     * @note clip input to 0.0 (hmm!)
     * @param from_y y-value
     * @return the calculated x-value
     */
    double calculate_x(double from_y) const;
    apoint_ts calculate_x(apoint_ts const &y, interpolation_scheme = interpolation_scheme::SCHEME_LINEAR) const;

    auto operator()(double x) const -> double {
      return calculate_y(x);
    }

    auto operator<=>(xy_point_curve const &) const = default;

    static xy_point_curve make(std::vector<double> const &, std::vector<double> const &);

    x_serialize_decl();
  };

  using xy_point_curve_ = std::shared_ptr<xy_point_curve>;

  /**
   * A xy_point_curve curve with a Z (or reference) value, used to
   * represents part of a 3D curve, suitable for variable gate etc.
   * To build A 3D curve, x=f(z,y), we have a list if these,
   * but the interpretation of them are different, gate flow, deltaMeter flow etc.
   */
  struct xy_point_curve_with_z {

    xy_point_curve xy_curve;

    /** The Z (or Reference) value is used when this xy_point_curve
     * plays as a member of a set of curves that describes a 3D
     * function space, like
     * x= f(z,y)
     */
    double z{0.0};

    SHYFT_DEFINE_STRUCT(xy_point_curve_with_z, (), (xy_curve, z));

    auto operator<=>(xy_point_curve_with_z const &) const = default;

    x_serialize_decl();
  };

  using xy_point_curve_with_z_ = std::shared_ptr<xy_point_curve_with_z>;

  /**
   * A collection of xy_point_curve_with_z, used to represents a function of two variables.
   * Suitable for gate characteristics (function of water level and gate position) and turbine
   * efficiency (function of volumetric flow and dynamic head).
   *
   * The function represented is absolutely continuous, weakly differentiable and a.e. smooth.
   */
  struct xyz_point_curve {
    // use flat_map for fast accesss
    using container = boost::container::flat_map<double, xy_point_curve>;
    container curves;

    SHYFT_DEFINE_STRUCT(xyz_point_curve, (), (curves));

    xyz_point_curve() = default;
    xyz_point_curve(xyz_point_curve const &) = default;
    xyz_point_curve(xyz_point_curve &&) = default;
    xyz_point_curve &operator=(xyz_point_curve const &) = default;
    xyz_point_curve &operator=(xyz_point_curve &&) = default;


    // construct from list of points
    xyz_point_curve(std::vector<xy_point_curve_with_z> const &l);

    /** Assign a curve to a z-value
     *
     * @note replaces the xy_curve for the z-value if it already exists.
     */
    void set_curve(double z, xy_point_curve const &xy);
    void set_curve(double z, xy_point_curve &&xy);

    /** Get the curve for z it it exists
     *
     * @throws runtime_error if no curve exists for z
     */
    auto get_curve(double z) const -> xy_point_curve;

    /** Compute value at any point
     * Uses linear interpolation if the object contains more than one z-values. If there is
     * a single curve, then it is treated as constant in z.
     *
     * @return the interpolated value at given (x, z)
     */
    auto evaluate(double x, double z) const -> double;

    /** Compute gradient at any point
     *
     * Computed gradient coincides with the classical gradient at any point where the curve is
     * differentiable, and otherwise coincides with the right-sided limits.
     * @return the gradient at given (x, z) as an array
     */
    auto gradient(double x, double z) const -> std::array<double, 2>;

    /** Find the right side of the interval containing z
     *
     * Used in evaluation and gradient computation.
     * Returns iterator point to (key,value) pair.
     */
    auto find_interval(double z) const -> typename container::const_iterator;

    auto operator<=>(xyz_point_curve const &) const = default;

    auto operator()(double x, double z) const {
      return evaluate(x, z);
    }

    auto operator()(std::array<double, 2> xz) const {
      auto [x, z] = xz;
      return evaluate(x, z);
    }

    // allow implicit casting?
    explicit operator std::vector<xy_point_curve_with_z>() const;

    x_serialize_decl();
  };

  using xzy_point_curve_ = std::shared_ptr<xyz_point_curve>;

  /**
   * @brief turbine efficiency
   *
   * @details
   * Defined by a set of efficiency curves, one for each net head, with optional production limits.
   * Part of the turbine description, to describe the efficiency of an entire turbine, or an isolated
   * operating zone or a Pelton needle combination. Production limits are only relevant when representing
   * an isolated operating zone or a Pelton needle combination.
   *
   * @see turbine_description
   */
  struct turbine_operating_zone {
    std::vector<xy_point_curve_with_z> efficiency_curves; ///< one or more z .. eff (xy)
    double production_min{0.0};                           ///< min production, might be more strict than eff-curve
    double production_max{0.0};                           ///< max production, might be more strict than eff-curve
    double production_nominal{0.0}; ///< nominal production, the installed/rated/nameplate capacity
    double fcr_min{std::numeric_limits<double>::quiet_NaN()}; ///< fcr reserve min production, might be less strict than eff-curve prod_min (lower allowed)
    double fcr_max{std::numeric_limits<double>::quiet_NaN()}; ///< fcr reserve max production, might be less strict than eff-curve prod_max (higher allowed)

    SHYFT_DEFINE_STRUCT(
      turbine_operating_zone,
      (),
      (production_min, production_max, production_nominal, fcr_min, fcr_max));

    auto evaluate(double x, double z) const -> double;
    auto operator<=>(turbine_operating_zone const &) const = default;

    x_serialize_decl();
  };

  /** @brief turbine description
   *
   * Complete description of efficiencies a turbine for all operating zones.
   * Pelton turbines typically have multiple operating zones; one for each needle combination.
   * Other turbines normally have only a single operating zone describing the entire turbine,
   * but may have more than one to model different isolated operating zones.
   * Each operating zone is described with a turbine efficiency object, which in turn may
   * contain multiple efficiency curves; one for each net head.
   *
   * @see turbine_operating_zone
   */
  struct turbine_description {
    std::vector<turbine_operating_zone>
      operating_zones; ///< one or more z .. eff for each isolated operation zone or Pelton needle combination

    SHYFT_DEFINE_STRUCT(turbine_description, (), (operating_zones));

    auto get_operating_zone(double p) const -> turbine_operating_zone;
    auto operator<=>(turbine_description const &) const = default;

    x_serialize_decl();
  };

  using turbine_description_ = std::shared_ptr<turbine_description>;

  double x_min(xy_point_curve const &xy);
  double x_max(xy_point_curve const &xy);
  double y_min(xy_point_curve const &xy);
  double y_max(xy_point_curve const &xy);

  double x_min(xy_point_curve_with_z const &xyz);
  double x_max(xy_point_curve_with_z const &xyz);
  double y_min(xy_point_curve_with_z const &xyz);
  double y_max(xy_point_curve_with_z const &xyz);

  double x_min(std::vector<xy_point_curve_with_z> const &xyz);
  double x_max(std::vector<xy_point_curve_with_z> const &xyz);
  double y_min(std::vector<xy_point_curve_with_z> const &xyz);
  double y_max(std::vector<xy_point_curve_with_z> const &xyz);
  double z_min(std::vector<xy_point_curve_with_z> const &xyz);
  double z_max(std::vector<xy_point_curve_with_z> const &xyz);

  /** @brief turbine capability
   *
   * A turbine capability describes whether or not the turbine can operate both in forward /
   * generating mode and in backward / pumping mode. This is deduced based on the efficiency
   * curves of the operating zones of the turbine. If there exists at least one efficiency curve
   * with a positive effective head it has forward capability, if there exists at least one
   * efficiency curve iwth a negative effective head it has backward capability.
   *
   * It is assumed that operating zones and effective heads are ordered with respect to the
   * effective head of the efficiency curve, and that efficiency curves are non-empty.
   */
  enum turbine_capability : unsigned {
    turbine_none = 0,
    turbine_forward = 1,
    turbine_backward = 2,
    turbine_reversible = 3
  };

  constexpr turbine_capability operator|(turbine_capability const &t0, turbine_capability const &t1) {
    return static_cast<turbine_capability>(
      std::underlying_type_t<turbine_capability>(t0) | std::underlying_type_t<turbine_capability>(t1));
  }

  constexpr turbine_capability &operator|=(turbine_capability &t0, turbine_capability const &t1) {
    return t0 = t0 | t1;
  }

  constexpr turbine_capability classify(xy_point_curve_with_z const &xyz) {
    if (xyz.z >= 0)
      return turbine_forward;
    else
      return turbine_backward;
  }

  constexpr auto backward_efficiency_curves(turbine_operating_zone const &z) {
    return std::views::take_while(z.efficiency_curves, [](auto const &xyz) {
      return classify(xyz) == turbine_backward;
    });
  }

  constexpr auto forward_efficiency_curves(turbine_operating_zone const &z) {
    return std::views::drop_while(z.efficiency_curves, [](auto const &xyz) {
      return classify(xyz) == turbine_backward;
    });
  }

  constexpr turbine_capability capability(turbine_description const &t) {
    auto const &Z = t.operating_zones;

    turbine_capability c = turbine_none;
    if (Z.empty())
      return c;
    c = classify(Z.front().efficiency_curves.front());
    if (c == turbine_backward)
      c |= classify(Z.back().efficiency_curves.back());
    return c;
  }

  constexpr bool has_forward_capability(turbine_capability c) {
    return c & turbine_forward;
  }

  constexpr bool has_backward_capability(turbine_capability c) {
    return c & turbine_backward;
  }

  constexpr bool has_reversible_capability(turbine_capability c) {
    return (c & turbine_reversible) == turbine_reversible;
  }

}

x_serialize_export_key(shyft::energy_market::hydro_power::point);
x_serialize_export_key(shyft::energy_market::hydro_power::xy_point_curve);
x_serialize_export_key(shyft::energy_market::hydro_power::xy_point_curve_with_z);
x_serialize_export_key(shyft::energy_market::hydro_power::xyz_point_curve);
x_serialize_export_key(shyft::energy_market::hydro_power::turbine_operating_zone);
x_serialize_export_key(shyft::energy_market::hydro_power::turbine_description);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::turbine_operating_zone, 1);

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::point, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::hydro_power::point, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::xy_point_curve, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::hydro_power::xy_point_curve, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::xy_point_curve_with_z, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::hydro_power::xy_point_curve_with_z, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::xyz_point_curve, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::hydro_power::xyz_point_curve, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::turbine_operating_zone, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::hydro_power::turbine_operating_zone, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::turbine_description, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::hydro_power::turbine_description, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::xy_point_curve, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve_with_z>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::xy_point_curve_with_z, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::turbine_description>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::turbine_description, Char> { };

template <typename Char>
struct fmt::formatter<
  std::shared_ptr<std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve>>>,
  Char>
  : shyft::ptr_formatter<
      std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve>>,
      Char> { };

template <typename Char>
struct fmt::formatter<
  std::shared_ptr<
    std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve_with_z>>>,
  Char>
  : shyft::ptr_formatter<
      std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve_with_z>>,
      Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>>, Char>
  : shyft::ptr_formatter<std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>, Char> { };

template <typename Char>
struct fmt::formatter<
  std::shared_ptr<std::map<
    shyft::core::utctime,
    std::shared_ptr<std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>>>>,
  Char>
  : shyft::ptr_formatter<
      std::map<
        shyft::core::utctime,
        std::shared_ptr<std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>>>,
      Char> { };

template <typename Char>
struct fmt::formatter<
  std::shared_ptr<
    std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::turbine_description>>>,
  Char>
  : shyft::ptr_formatter<
      std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::turbine_description>>,
      Char> { };

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::hydro_power::turbine_capability);
