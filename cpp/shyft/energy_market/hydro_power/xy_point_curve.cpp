/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <array>
#include <limits>
#include <numeric>
#include <ranges>
#include <stdexcept>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>

namespace shyft::energy_market::hydro_power {

  constexpr auto NaN = std::numeric_limits<double>::quiet_NaN();

  xy_point_curve xy_point_curve::make(std::vector<double> const & x, std::vector<double> const & y) {
    if (x.size() != y.size())
      throw std::runtime_error(
        fmt::format("x points size {} and y points size {} lists must have same dimension", x.size(), y.size()));
    xy_point_curve xy{.points = std::vector<point>(x.size())};
    std::ranges::transform(x, y, std::ranges::begin(xy.points), [](auto x, auto y) {
      return point{x, y};
    });
    std::ranges::sort(xy.points);
    return xy;
  }

  bool xy_point_curve::is_xy_mono_increasing() const {
    if (points.size() < 2)
      return false;
    for (std::size_t i = 1; i < points.size(); i++) {
      if (points[i - 1].x >= points[i].x || points[i - 1].y >= points[i].y)
        return false;
    }
    return true;
  }

  bool xy_point_curve::is_xy_invertible() const {
    if (points.size() < 2)
      return false;

    int const sgn = (points.back().y > points.front().y) - (points.back().y < points.front().y);
    auto non_monotone_segment = [&sgn](auto const & a, auto const & b) {
      return (b.x <= a.x || b.y * sgn <= a.y * sgn);
    };

    return std::ranges::adjacent_find(points, non_monotone_segment) == points.end();
  }

  bool xy_point_curve::is_xy_convex() const {
    if (points.size() < 2)
      return false;

    // Check for non-increasing x-values and reentrant corners
    double dydx = -std::numeric_limits<double>::infinity();
    auto non_convex_segment = [&dydx](auto const & a, auto const & b) {
      auto next_dydx = (b.y - a.y) / (b.x - a.x);
      if (b.x <= a.x || next_dydx < dydx)
        return true;
      dydx = next_dydx;
      return false;
    };
    return std::ranges::adjacent_find(points, non_convex_segment) == points.end();
  }

  double xy_point_curve::calculate_y(double x) const {
    if (points.size() == 0)
      return std::numeric_limits<double>::quiet_NaN();
    if (points.size() == 1)
      return points.back().y;
    apoint_ts x_ts(time_axis::fixed_dt(0.0, 1.0, 1), x, shyft::time_series::POINT_AVERAGE_VALUE);
    return calculate_y(x_ts).values().front();
  }

  apoint_ts xy_point_curve::calculate_y(apoint_ts const & x, interpolation_scheme scheme) const {
    std::vector<std::array<double, 2>> pts;
    for (auto const & p : points)
      pts.push_back(std::array<double, 2>{p.x, p.y});

    return x.transform(pts, scheme);
  }

  double xy_point_curve::calculate_x(double from_y) const {
    if (points.size() < 2)
      return std::numeric_limits<double>::quiet_NaN();
    apoint_ts y_ts(time_axis::fixed_dt(0.0, 1.0, 1), from_y, shyft::time_series::POINT_AVERAGE_VALUE);
    return calculate_x(y_ts).values().front();
  }

  apoint_ts xy_point_curve::calculate_x(apoint_ts const & y, interpolation_scheme scheme) const {
    if (!is_xy_invertible())
      throw std::runtime_error("xy_point_curve is not invertible.");

    std::vector<std::array<double, 2>> pts;
    for (auto const & p : points)
      pts.push_back(std::array<double, 2>{p.y, p.x});

    // Sorting is needed for the case that y-values are decreasing
    std::ranges::sort(pts);

    return y.transform(pts, scheme);
  }

  // xyz_point_curve

  xyz_point_curve::xyz_point_curve(std::vector<xy_point_curve_with_z> const & l) {
    // making sure constructor is consistent with set_curve
    for (auto&& [f, z] : l) {
      set_curve(z, f);
    }
  }

  void xyz_point_curve::set_curve(double z, xy_point_curve const & xy) {
    // remove any existing value for z first otherwise insertion may fail
    curves.insert_or_assign(z, xy);
  }

  void xyz_point_curve::set_curve(double z, xy_point_curve&& xy) {
    curves.insert_or_assign(z, std::forward<decltype(xy)>(xy));
  }

  auto xyz_point_curve::get_curve(double z) const -> xy_point_curve {
    return curves.at(z);
  }

  auto xyz_point_curve::find_interval(double z) const -> typename container::const_iterator {
    auto const first = std::next(std::begin(curves));
    auto const last = std::prev(std::end(curves));
    return std::min(std::max(curves.upper_bound(z), first), last);
  }

  auto xyz_point_curve::evaluate(double x, double z) const -> double {
    // nan if no data in curve
    if (curves.size() == 0)
      return NaN;

    // constant in z if only a single member
    if (curves.size() == 1)
      return curves.begin()->second(x);

    auto b = find_interval(z);
    auto a = b - 1;

    auto& [z0, f0] = *a;
    auto& [z1, f1] = *b;

    return (f1(x) * (z - z0) + f0(x) * (z1 - z)) / (z1 - z0);
  }

  auto xyz_point_curve::gradient(double x, double z) const -> std::array<double, 2> {
    // nan if no data in curve
    if (curves.size() == 0)
      return {NaN, NaN};

    // constant in z if only a single member
    if (curves.size() == 1)
      return {curves.begin()->second(x), 0};

    auto b = find_interval(z);
    auto a = b - 1;

    auto& [z0, f0] = *a;
    auto& [z1, f1] = *b;

    auto dx = NaN; // not yet implemented
    auto dz = (f1(x) - f0(x)) / (z1 - z0);

    return {dx, dz};
  }

  xyz_point_curve::operator std::vector<xy_point_curve_with_z>() const {
    auto xyz = curves | std::views::transform([](auto&& v) -> xy_point_curve_with_z {
                 return {v.second, v.first};
               });
    return {xyz.begin(), xyz.end()};
  }

  // turbine_operating_zone

  auto turbine_operating_zone::evaluate(double x, double z) const -> double {
    return xyz_point_curve(efficiency_curves).evaluate(x, z);
  }

  // turbine_description

  auto turbine_description::get_operating_zone(double p) const -> turbine_operating_zone {

    if (!operating_zones.size()) {
      throw std::runtime_error("operating_zones is empty");
    }

    // indirectly sort the operating zones for constness.

    auto indirect_compare = [this](std::size_t i, std::size_t j) {
      auto& a = operating_zones[i];
      auto& b = operating_zones[j];
      return a.production_min < b.production_min
          or (a.production_min == b.production_max and a.production_max < b.production_max);
    };

    auto indices = std::vector<std::size_t>(operating_zones.size());
    std::iota(indices.begin(), indices.end(), 0);
    std::ranges::sort(indices, indirect_compare);

    for (auto i : indices) {
      auto& zone = operating_zones[i];
      if (p < zone.production_max) {
        return zone;
      };
    }

    return operating_zones[indices.back()];
  }

  // Functions for determining min and max of curves are implement as free functions. The reason for this is that
  // the data structure for the R^2 -> R mapping is a std::vector and this bars implementation as class functions.

  // Return nan for empty tables. Using std::optional or throwing error could be better choices here.
  double x_min(xy_point_curve const & xy) {
    return xy.points.size() ? std::ranges::min_element(xy.points, std::ranges::less{}, &point::x)->x : NaN;
  }

  double x_max(xy_point_curve const & xy) {
    return xy.points.size() ? std::ranges::max_element(xy.points, std::ranges::less{}, &point::x)->x : NaN;
  }

  double y_min(xy_point_curve const & xy) {
    return xy.points.size() ? std::ranges::min_element(xy.points, std::ranges::less{}, &point::y)->y : NaN;
  }

  double y_max(xy_point_curve const & xy) {
    return xy.points.size() ? std::ranges::max_element(xy.points, std::ranges::less{}, &point::y)->y : NaN;
  }

  double x_min(xy_point_curve_with_z const & xyz) {
    return x_min(xyz.xy_curve);
  };

  double x_max(xy_point_curve_with_z const & xyz) {
    return x_max(xyz.xy_curve);
  };

  double y_min(xy_point_curve_with_z const & xyz) {
    return y_min(xyz.xy_curve);
  };

  double y_max(xy_point_curve_with_z const & xyz) {
    return y_max(xyz.xy_curve);
  };

  double x_min(std::vector<xy_point_curve_with_z> const & xyz) {
    auto xy_min = std::views::transform(xyz, [](auto const & xy) {
      return x_min(xy);
    });
    auto it = std::ranges::min_element(xy_min);
    return (it != std::ranges::end(xy_min)) ? *it : NaN;
  }

  double x_max(std::vector<xy_point_curve_with_z> const & xyz) {
    auto xy_max = std::views::transform(xyz, [](auto const & xy) {
      return x_max(xy);
    });
    auto it = std::ranges::max_element(xy_max);
    return (it != std::ranges::end(xy_max)) ? *it : NaN;
  }

  double y_min(std::vector<xy_point_curve_with_z> const & xyz) {
    auto xy_min = std::views::transform(xyz, [](auto const & xy) {
      return y_min(xy);
    });
    auto it = std::ranges::min_element(xy_min);
    return (it != std::ranges::end(xy_min)) ? *it : NaN;
  }

  double y_max(std::vector<xy_point_curve_with_z> const & xyz) {
    auto xy_max = std::views::transform(xyz, [](auto const & xy) {
      return y_max(xy);
    });
    auto it = std::ranges::max_element(xy_max);
    return (it != std::ranges::end(xy_max)) ? *it : NaN;
  }

  double z_min(std::vector<xy_point_curve_with_z> const & xyz) {
    return xyz.size() ? std::ranges::min_element(xyz, std::ranges::less{}, &xy_point_curve_with_z::z)->z : NaN;
  }

  double z_max(std::vector<xy_point_curve_with_z> const & xyz) {
    return xyz.size() ? std::ranges::max_element(xyz, std::ranges::less{}, &xy_point_curve_with_z::z)->z : NaN;
  }

}
