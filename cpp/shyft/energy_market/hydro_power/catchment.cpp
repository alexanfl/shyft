/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/catchment.h>

namespace shyft::energy_market::hydro_power {

  bool catchment::operator==(catchment const &o) const {
    return id_base::operator==(o);
  }

  void catchment::clear() {
    hps.reset();
  }

  catchment::~catchment() {
    clear();
  }

}
