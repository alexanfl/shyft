/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::hydro_power {

  std::shared_ptr<waterway> const &
    waterway::input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<waterway> const & w) {
    connect(w->shared_from_this(), me->shared_from_this());
    return me;
  }

  std::shared_ptr<waterway> const &
    waterway::input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<unit> const & p) {
    connect(p->shared_from_this(), me->shared_from_this());
    return me;
  }

  std::shared_ptr<waterway> const & waterway::input_from(
    std::shared_ptr<waterway> const & me,
    std::shared_ptr<reservoir> const & r,
    connection_role role) {
    connect(r->shared_from_this(), role, me->shared_from_this());
    return me;
  }

  std::shared_ptr<waterway> const &
    waterway::input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<reservoir> const & r) {
    connect(r->shared_from_this(), connection_role::main, me->shared_from_this());
    return me;
  }

  std::shared_ptr<waterway> const &
    waterway::output_to(std::shared_ptr<waterway> const & me, std::shared_ptr<waterway> const & w) {
    connect(me->shared_from_this(), w->shared_from_this());
    return me;
  }

  std::shared_ptr<waterway> const &
    waterway::output_to(std::shared_ptr<waterway> const & me, std::shared_ptr<reservoir> const & r) {
    connect(me->shared_from_this(), r->shared_from_this());
    return me;
  }

  std::shared_ptr<waterway> const &
    waterway::output_to(std::shared_ptr<waterway> const & me, std::shared_ptr<unit> const & p) {
    connect(me->shared_from_this(), p->shared_from_this());
    return me;
  }

  connection_role waterway::upstream_role() {
    if (upstreams.size() == 0)
      throw std::runtime_error("Waterway has no upstream connections");
    auto upstream = upstreams[0].target;
    for (auto& connection : upstream->downstreams) {
      if (connection.target->id == id) {
        return connection.role;
      }
    }
    throw std::runtime_error("Waterway has inconsistent upstream connnection");
  }

  bool waterway::operator==(waterway const & o) const {
    if (this == &o)
      return true; // equal by addr.
    if (!id_base::operator==(o))
      return false;

    return equal_structure(o)
        && std::is_permutation(
             begin(gates), end(gates), begin(o.gates), end(o.gates), [](auto const & ca, auto const & cb) {
               return ca == cb || (ca && cb && (*ca == *cb));
             });
  }

  std::shared_ptr<waterway> waterway::shared_from_this() const {
    auto hps = hps_();
    return hps ? shared_from_me(this, hps->waterways) : nullptr;
  }

  void waterway::add_gate(std::shared_ptr<waterway> const & ww, std::shared_ptr<gate> const & g) {
    auto w = ww->shared_from_this();
    if (g && g->wtr_()) {
      throw std::runtime_error(
        "This gate is already part of '" + g->wtr_()->name + "', remove the gate from that oject first");
    }
    auto f = std::ranges::find(w->gates, g);
    if (f == std::ranges::end(w->gates)) {
      g->wtr = w; // uplink ref. here
      w->gates.push_back(g);
    }
  }

  void waterway::remove_gate_ptr(gate* g) {
    if (!g)
      return;
    auto f = std::ranges::find_if(gates, [g](auto const & x) {
      return x.get() == g;
    });
    if (f != std::ranges::end(gates)) {
      g->wtr.reset(); // remove uplink ref. here.
      gates.erase(f);
    }
  }

  std::shared_ptr<gate> gate::shared_from_this() const {
    auto wtr = wtr_();
    return wtr ? shared_from_me(this, wtr->gates) : nullptr;
  }

  waterway::~waterway() {
    for (auto& g : gates) // remove uplink to this prior to vector delete
      g->wtr.reset();
  }

  gate::gate() = default;

  gate::~gate() {
  }

  gate::gate(int id, std::string const & name, std::string const & json)
    : id_base{id, name, json, {}, {}, {}} {
  }
}
