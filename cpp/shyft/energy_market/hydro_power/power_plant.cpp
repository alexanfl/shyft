/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/power_plant.h>

#include <stdexcept>

#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::hydro_power {

  std::shared_ptr<unit> const & unit::input_from(std::shared_ptr<unit> const & me, waterway_ const & w) {
    connect(w->shared_from_this(), me->shared_from_this());
    return me;
  }

  std::shared_ptr<unit> const & unit::output_to(std::shared_ptr<unit> const & me, waterway_ const & w) {
    connect(me->shared_from_this(), w->shared_from_this());
    return me;
  }

  bool unit::is_pump() const {
    return name.find("pump") != std::string::npos;
  }

  std::shared_ptr<waterway> unit::downstream() const {
    return downstreams.size() ? std::dynamic_pointer_cast<waterway>(downstreams.front().target) : nullptr;
  }

  std::shared_ptr<waterway> unit::upstream() const {
    return upstreams.size() ? std::dynamic_pointer_cast<waterway>(upstreams.front().target) : nullptr;
  }

  std::shared_ptr<unit> unit::shared_from_this() const {
    auto hps = hps_();
    return hps ? shared_from_me(this, hps->units) : nullptr;
  }

  std::shared_ptr<power_plant> power_plant::shared_from_this() const {
    auto hps = hps_();
    return hps ? shared_from_me(this, hps->power_plants) : nullptr;
  }

  void power_plant::add_unit(std::shared_ptr<power_plant> const & ps, std::shared_ptr<unit> const & a) {
    auto pp = ps->shared_from_this();
    if (a && pp && pp->hps_() == a->hps_()) {
      auto f = std::ranges::find(pp->units, a);
      if (f != std::ranges::end(pp->units))
        throw std::runtime_error("unit  '" + a->name + "'already added to power-plant '" + pp->name);
      if (a->pwr_station_())
        throw std::runtime_error(
          "unit  '" + a->name + "'already added to anoter power-plant '" + a->pwr_station_()->name);
      pp->units.push_back(a);
      a->pwr_station = pp;
    } else {
      throw std::runtime_error(
        "power_plant '" + std::string(ps ? ps->name : std::string("null")) + "' and unit '"
        + std::string(a ? a->name : std::string("null"))
        + "' must be non-null objects, and both belong to the same existing hydro-power-system :"
        + std::string(ps && ps->hps_() ? ps->hps_()->name : std::string("null")));
    }
  }

  void power_plant::remove_unit(std::shared_ptr<unit> const & a) {
    auto f = std::ranges::find_if(units, [&a](auto const & p) -> bool {
      return a == p;
    });
    if (f != std::ranges::end(units)) {
      (*f)->pwr_station.reset();
      units.erase(f);
    }
  }

  bool unit::operator==(unit const & o) const {
    return this == &o || (id_base::operator==(o) && equal_structure(o)); // same attributes and closure/neighbourhood
  }

  bool power_plant::operator==(power_plant const & o) const {
    if (this == &o)
      return true;
    if (!id_base::operator==(o))
      return false;
    return equal_structure(o);
  }

  bool power_plant::equal_structure(power_plant const & o) const {
    if (id != o.id)
      return false; // we insist on same object id to be structurally equal.
    // notice that we do not take the depth of units here, only that it points to same units
    //  which correspond to the rule of limit to close neighbour objects
    return std::ranges::is_permutation(units, o.units, [](auto const & ca, auto const & cb) {
      return ca && cb && ca->id == cb->id;
    });
  }

  power_plant::~power_plant() {
    for (auto& a : units)
      if (a)
        a->pwr_station.reset();
  }

}
