/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>

namespace shyft::energy_market::hydro_power {

  struct gate;

  /** @brief waterway - a river or a tunnel.
   *
   * Hydrology:
   *    If multiple inputs, it's a junction.
   *    If multiple outputs:
   *          For distributing flow to several turbines inside
   *          the power-house (tunnel scenario).
   *          For rivers, - could be supported, but
   *          currently it is not supported, workaround is reservoir in between.
   *    If it's a river, time-delays, volume, and routing characteristics might apply
   *    If it's a tunnel, friction-loss coefficients might apply
   *
   *
   *    In a power system, the *tunnels* can split, but then it's always pressure-balance
   *    and the water-flow might go both ways depending on the hydrostatic pressure.
   *    On the inputs, there might be hatches/gates that controls the amount of flow.
   *
   *    If the water_route_(river) input is connected to a reservoir, the amount of flow could
   *    be dependent on the gate-opening/characteristics, the reservoir level, as well as the
   *    reservoir level at the output side. The gate-opening might be variable to allow for
   *    a certain amount of flow related to restrictions/concession requirements(fish-water).
   *
   *    If the water_route_ is a tunnel, then it might still be gates/hatches at the input that limits the flow.
   *    Usually (assume always), it's either open or closed.
   *
   *   @ref
   * http://www.ivt.ntnu.no/ept/fag/tep4195/innhold/Forelesninger/forelesninger%202006/5%20-%20Hydro%20Power%20Plants.pdf
   */
  struct waterway : hydro_component {

    SHYFT_DEFINE_STRUCT(waterway, (id_base), ());

    waterway() = default;
    waterway(
      int id,
      std::string const & name,
      std::string const & json = "",
      std::shared_ptr<hydro_power_system> const & hps = nullptr)
      : hydro_component{id, name, json, hps} {};
    ~waterway();
    std::vector<std::shared_ptr<gate>> gates; ///< gates control amount of water into this water_route

    std::shared_ptr<hydro_component> downstream() const {
      return downstreams.size() ? downstreams[0].target : nullptr;
    }

    std::shared_ptr<hydro_component> upstream() const {
      return upstreams.size() ? upstreams[0].target : nullptr;
    }

    static void add_gate(std::shared_ptr<waterway> const & w, std::shared_ptr<gate> const & g);
    void remove_gate_ptr(gate* g);

    void remove_gate(std::shared_ptr<gate> const & g) {
      return remove_gate_ptr(g.get());
    }

    std::shared_ptr<waterway> shared_from_this() const;
    bool operator==(waterway const & o) const;

    bool operator!=(waterway const & o) const {
      return !operator==(o);
    }

    static std::shared_ptr<waterway> const &
      input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<waterway> const & w);
    static std::shared_ptr<waterway> const &
      input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<unit> const & p);
    static std::shared_ptr<waterway> const &
      input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<reservoir> const & r, connection_role role);
    static std::shared_ptr<waterway> const &
      input_from(std::shared_ptr<waterway> const & me, std::shared_ptr<reservoir> const & r);
    static std::shared_ptr<waterway> const &
      output_to(std::shared_ptr<waterway> const & me, std::shared_ptr<waterway> const & w);
    static std::shared_ptr<waterway> const &
      output_to(std::shared_ptr<waterway> const & me, std::shared_ptr<reservoir> const & r);
    static std::shared_ptr<waterway> const &
      output_to(std::shared_ptr<waterway> const & me, std::shared_ptr<unit> const & p);
    connection_role upstream_role(); ///< the role the water way has relative to the component above

    x_serialize_decl();
  };

  using waterway_ = std::shared_ptr<waterway>;

  /** @brief A gate controls the amount of flow into the water-route
   *
   * Gates in tunnels are usually open/closed, since leaving it half-open would
   * lead to fall-height losses and less optimal utiliziation of water.
   *
   * Gates related to reservoirs, bypass, or flood water-routes,
   * is usually controlled, using gate-opening, to
   * give a certain amount of flow, or to keep a certain water-level.
   *
   * The gate-flow in such cases can be quite complex, and depend on:
   *  1. gate-opening ( that leads to a friction-loss component)
   *  2. reservoir level of the reservoir in the 'upstream'-direction
   *  3. reservoir level of the reservoir in the 'downstream'-direction
   *
   * Modelling cases
   *
   *  uni-directional:
   *    gate to water-route(river), where downstream reservoir is lower, representing no effect on flow
   *
   *  bidirectional:
   *    gate to water-route(river), where downstream is reservoir, potentially overlapping levels
   *    gate to water-route(tunnel), junction,  where closure reservoirs, potentially overlapping levels
   *
   * @note computing correct gate-flow accurately can be complex, e.g. given multiple gates that could
   *       have mutual effects. In case they are significant, special care is needed.
   */
  struct gate : id_base {
    std::weak_ptr<waterway> wtr; ///< waterroute this gate belongs to

    gate();
    virtual ~gate();
    gate(int id, std::string const & name, std::string const & json = "");

    auto wtr_() const {
      return wtr.lock();
    }

    auto hps_() const {
      auto ww = wtr_();
      return ww ? ww->hps_() : nullptr;
    }

    std::shared_ptr<gate> shared_from_this() const; ///< shared from this using hps.gates shared pointers.

    // waterway_ & wtr_() { return wtr; }
    //  compute_flow(opening...)->m3/s,
    // or
    //  compute_friction_loss(opening...)-> m /(m3/s)^2
    bool operator==(gate const & o) const {
      return id_base::operator==(o);
    }

    bool operator!=(gate const & o) const {
      return !operator==(o);
    }

    SHYFT_DEFINE_STRUCT(gate, (), ());
    x_serialize_decl();
  };

  /** hydro_connect for easy readable type-safe connections
   *
   * allow us to write hydro_connect(rsv).input_from(wtr).output_to(wtr2) etc.
   *
   */

  template <class T>
  struct hydro_connect {
    std::shared_ptr<T> me;

    hydro_connect(std::shared_ptr<T> const & me)
      : me(me) {
    }

    template <class I>
    hydro_connect& input_from(std::shared_ptr<I> const & i) {
      T::input_from(me, i);
      return *this;
    }

    template <class I>
    hydro_connect& input_from(std::shared_ptr<I> const & i, connection_role cr) {
      T::input_from(me, i, cr);
      return *this;
    }

    template <class O>
    hydro_connect& output_to(std::shared_ptr<O> const & o) {
      T::output_to(me, o);
      return *this;
    }

    template <class O>
    hydro_connect& output_to(std::shared_ptr<O> const & o, connection_role cr) {
      T::output_to(me, o, cr);
      return *this;
    }
  };

  template <class T>
  hydro_connect<T> connect(std::shared_ptr<T> const & me) {
    return hydro_connect<T>(me);
  }

}

// x_serialize_export_key(shyft::energy_market::hydro_power::waterway);
BOOST_CLASS_EXPORT_KEY2(
  shyft::energy_market::hydro_power::waterway,
  BOOST_PP_STRINGIZE(shyft::energy_market::hydro_power::water_route));
x_serialize_export_key(shyft::energy_market::hydro_power::gate);
BOOST_CLASS_VERSION(
  shyft::energy_market::hydro_power::gate,
  2); // 2: proper handling of base class, 1:shared to weak ptr.
BOOST_CLASS_VERSION(
  shyft::energy_market::hydro_power::waterway,
  1); // proper use of parent hydro_component class serial

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::gate, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::gate, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::gate>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::gate, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::waterway, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::waterway, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::waterway>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::waterway, Char> { };
