/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <memory>
#include <ranges>
#include <set>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/em_handle.h>
#include <shyft/energy_market/hydro_power/waterway.h>

namespace shyft::energy_market::market {
  struct model_area;
}

namespace shyft::energy_market::hydro_power {

  using shyft::core::utctime;
  using shyft::core::no_utctime;
  using shyft::energy_market::market::model_area;

  struct hydro_power_system_builder;

  /**
   * @brief A Hydro Power System, hydro-components and their relations
   *
   * @details
   * A Hydro power system consists of its components:
   *        Reservoirs(including creek/inlets/buffer-reservoirs etc.)
   *        PowerStations(pure|pump|both)
   *        WaterRoutes (tunnel|rivers) provide the hydrological interconnections between Reservoirs/PowerStations
   *
   * From the above, we can conclude that it can have one or several *watercourses*.
   *
   * A watercourse is a hydrological connected system (and a Hydro Power System could have several of those).
   *
   * A *valid* watercourse consist of a minimum of a reservoir_ and a aggregate_(pure generator) interconnected by
   * waterway_(tunnel).
   *
   * Discussion: Should we allow a waterway_ to have 'null' output, or should we insist on connect to an 'ocean'
   * reservoir_ object ? Inflow(regulated/unregulated) is the *input* to the system (injected at Reservoirs), at some
   * point later, it leaves our system model after passing through  Reservoirs, WaterRoutes and hopefully also some
   * PowerStations. Should we enforce that it leaves into the 'ocean' ?
   *
   *        Associated with the hydro power system we also have the WatermarkSeries.
   */
  struct hydro_power_system : id_base {
    using super = id_base;

    utctime created = no_utctime;                       ///< keep track of when it is created (or persisted first time)
    std::vector<std::shared_ptr<reservoir>> reservoirs; ///< simply the reservoirs in the hps
    std::vector<std::shared_ptr<unit>> units;           ///< the units(generator,turbine) in the hps,
    std::vector<std::shared_ptr<waterway>> waterways;   ///< tunnels and rivers that have the water-flow


    std::vector<std::shared_ptr<catchment>> catchments; ///< represents the catchments,could be input to to reservoirs
    std::vector<std::shared_ptr<power_plant>> power_plants; ///< pp have references to units
    std::weak_ptr<model_area> mdl_area; ///< in early versions, ref. to model area(Sintef/EMPS)

    SHYFT_DEFINE_STRUCT(hydro_power_system, (), ());

    hydro_power_system() = default;
    explicit hydro_power_system(std::string a_name);

    hydro_power_system(int id, std::string const & name, std::string const & json = "")
      : id_base{id, name, json,{},{},{}} {
    }

    virtual ~hydro_power_system();
    void clear();
    std::shared_ptr<reservoir> find_reservoir_by_name(std::string const & name) const;
    std::shared_ptr<unit> find_unit_by_name(std::string const & name) const;
    std::shared_ptr<power_plant> find_power_plant_by_name(std::string const & name) const;
    std::shared_ptr<waterway> find_waterway_by_name(std::string const & name) const;
    std::shared_ptr<gate> find_gate_by_name(std::string const & name) const;
    std::shared_ptr<catchment> find_catchment_by_name(std::string const & name) const;

    std::shared_ptr<reservoir> find_reservoir_by_id(std::int64_t id) const;
    std::shared_ptr<unit> find_unit_by_id(std::int64_t id) const;
    std::shared_ptr<power_plant> find_power_plant_by_id(std::int64_t id) const;
    std::shared_ptr<waterway> find_waterway_by_id(std::int64_t id) const;
    std::shared_ptr<gate> find_gate_by_id(std::int64_t id) const;
    std::shared_ptr<catchment> find_catchment_by_id(std::int64_t id) const;

    auto gates() const {
      return waterways | std::views::transform([](auto&& w) {
               return std::views::all(w->gates);
             })
           | std::views::join;
    }

    /**
     * @brief Populates the hydro_power_system with a collection of hydro components.
     * @details
     * The hydro_power_system is populated as follows: each element (which can be either a reservoir,
     * a waterway, or a unit) contained in the collection is placed in the corresponding container
     * (std::vector) of hydro-components (e.g., an element of type reservoir in the collection will
     * be added to the reservoirs container). Moreover, for each unit, the power_plant that the unit
     * belongs to will be added to the std::vector of power plants.
     *
     * @param collection[in]  A collection (std::set) of all the hydro components belonging to the hydro_power_system.
     *
     * @see populate
     */
    void populate(std::set<std::shared_ptr<hydro_component>>& collection);

    /**
     * @brief Populates the hydro_power_system with a collection of hydro components.
     *
     * @details
     * Given a collection of hydro components (i.e., reservoir, unit, or waterway) and a hydro_power_system_builder,
     * creates new hydro components based on the elements contained in the collection and adds the components to the
     * corresponding containers (std::vector). Once the hydro components are created, it builds the hydraulic
     * connections between the hydro components based on the connectivities of the elements in the collection.
     *
     * @param collection[in]  A collection (std::set) of all the hydro components belonging to the hydro_power_system.
     * @param hpsb[in]        An instance of hydro_power_system_builder used to create the content of
     * hydro_power_system.
     *
     * @see populate
     */
    void populate(std::set<std::shared_ptr<hydro_component>>& collection, hydro_power_system_builder& hpsb);


    auto mdl_area_() {
      return mdl_area.lock();
    }

    void set_mdl_area(std::shared_ptr<model_area> const & ma) {
      mdl_area = ma;
    }

    /**
     * @brief equal_structure
     * @details This comparison only look at the equality of the  topology, and will not
     * compare any attributes of the objects. The object `.id` are used for identifying same objects.
     * Thus, this function will return true only if the hydro_power_system components have the same `.id`,
     * and they are interconnected identically.
     * Especially, the other attributes, like .name, .json etc, are NOT part of the comparison.
     * Also note, that the hydro_power_system own attributes, like `.id`, `.name` is not considered,
     * similar as for the `equal_content` method.
     *
     * @note a variant of the equal_structure, that goes more into the conceptual topology equality could also be
     * implemented. The difference would be that there is no requirement for the object `.id` to be the same, only type
     * and topology pattern.
     *
     * @param other the other hydro_power_system to compare
     * @return true if they have equal structure, otherwise false
     */

    bool equal_structure(hydro_power_system const & other) const;

    /**
     * @brief equal_content
     * @details Same as strictly equal as defined by the == operator,
     * except that the hydro power system .id/.name/.created is not compared.
     * The purpose is to allow to see if two different models do have exactly the
     * same semantically content.
     * @param other the other hydro_power_system to compare
     * @return true if they have equal content, otherwise false
     */
    bool equal_content(hydro_power_system const & other) const;

    /**
     * @brief Equal as in strict semantic equality
     * @details The term 'strictly semantic equality' means that we also ensures that
     * if the internal representation of lists and maps are slightly different ordered,
     * they are considered equal, because it is semantically same set of topology,
     * and the interconnections are the same. This also includes all identifiers, names, attributes
     * of the objects that constitutes the hydro_power_system, including the object it self.
     * @param other the other hydro_power_system to compare
     * @return true if they are identical, otherwise false
     *
     */
    bool operator==(hydro_power_system const & other) const;

    bool operator!=(hydro_power_system const & other) const {
      return !operator==(other);
    }

    static std::string to_blob(std::shared_ptr<hydro_power_system> const & me);
    static std::shared_ptr<hydro_power_system> from_blob(std::string xmls);

    template <typename U>
    static std::ranges::range_value_t<U> find_by_name(U&& v, std::string const & name) {
      auto f = std::ranges::find_if(v, [name](auto const & r) -> bool {
        return r->name == name;
      });
      return f != std::ranges::end(v) ? *f : nullptr;
    }

    template <typename U>
    static std::ranges::range_value_t<U> find_by_id(U&& v, std::int64_t id) {
      auto f = std::ranges::find_if(v, [id](auto const & r) -> bool {
        return r->id == id;
      });
      return f != std::ranges::end(v) ? *f : nullptr;
    }

    x_serialize_decl();
  };

  /**
   * @brief The hydro-power-system-builder helps building and modififying a hp-system
   * @details
   * It helps exposing the building blocks to python in a minimalistic and
   * safe way.
   * The intention is ensure that the user (at python level), follows rules,
   * and capabilities present in the system.
   *
   * We *could* make semantic builders for different processes (like LTM/STM) to
   * ensure only 'valid models within that context' is built. But it might be better
   * to rather put that effort into validator/transformators pr. process/domain,
   * since it is possible to transform detailed models into more simplified models using rules.
   *
   */
  struct hydro_power_system_builder {
    std::shared_ptr<hydro_power_system> hps;

    hydro_power_system_builder(std::shared_ptr<hydro_power_system>& hps)
      : hps(hps) {
    }

    std::shared_ptr<reservoir> create_reservoir(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<unit> create_unit(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<waterway> create_waterway(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<gate> create_gate(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<waterway> create_tunnel(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<waterway> create_river(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<catchment> create_catchment(int id, std::string const & name, std::string const & json = "");
    std::shared_ptr<power_plant> create_power_plant(int id, std::string const & name, std::string const & json = "");
  };

}

x_serialize_export_key(shyft::energy_market::hydro_power::hydro_power_system);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::hydro_power_system, 1); // custom

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::hydro_power_system, Char> {

  constexpr auto parse(auto& ctx) {
    return ctx.begin();
  }

  auto format(shyft::energy_market::hydro_power::hydro_power_system const & t, auto& ctx) const {
    auto&& out = ctx.out();
    return fmt::format_to(out, "{{.id={}, .name={}}}", t.id, t.name);
  }
};

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::hydro_power_system, Char> { };
