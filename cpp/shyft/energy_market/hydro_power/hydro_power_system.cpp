/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <sstream>
#include <stdexcept>
#include <algorithm>

#include <fmt/core.h>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/nvp.hpp>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::hydro_power {

  hydro_power_system::hydro_power_system(std::string a_name)
    : id_base{0,a_name,"",{},{},{}} {
  }

  void hydro_power_system::populate(std::set<std::shared_ptr<hydro_component>>& collection) {
    for (auto& component : collection) {
      auto res = std::dynamic_pointer_cast<reservoir>(component);
      auto wr = std::dynamic_pointer_cast<waterway>(component);
      auto agg = std::dynamic_pointer_cast<unit>(component);
      if (res) {
        reservoirs.push_back(res);
        continue;
      }
      if (wr) {
        waterways.push_back(wr);
        continue;
      }
      if (agg) {
        units.push_back(agg);
      }
    }

    bool found;
    for (auto& agg : units) {
      found = false;
      auto ps = agg->pwr_station_();
      for (auto& p : power_plants) {
        if (ps->id == p->id) {
          found = true;
          break;
        }
      }
      if (!found) {
        power_plants.push_back(ps);
      }
    }

    for (auto& ps : power_plants) {
      for (auto& agg : ps->units) {
        auto el = std::ranges::find(units, agg);
        if (el == units.end()) {
          units.push_back(agg);
        }
      }
    }
  }

  void hydro_power_system::populate(
    std::set<std::shared_ptr<hydro_component>>& collection,
    hydro_power_system_builder& hpsb) {
    std::map<int, std::shared_ptr<reservoir>> nres;
    std::map<int, std::shared_ptr<unit>> nagg;
    std::map<int, std::shared_ptr<waterway>> nwr;
    for (auto& comp : collection) {
      auto res = std::dynamic_pointer_cast<reservoir>(comp);
      auto wr = std::dynamic_pointer_cast<waterway>(comp);
      auto agg = std::dynamic_pointer_cast<unit>(comp);
      if (res) {
        if (res->name.compare("havet") != 0) {
          auto _res = hpsb.create_reservoir(res->id, res->name, res->json);
          nres.insert(std::pair<int, std::shared_ptr<reservoir>>(_res->id, _res));
        }
        continue;
      }
      if (wr) {
        auto _wr = hpsb.create_waterway(wr->id, wr->name, wr->json);
        nwr.insert(std::pair<int, std::shared_ptr<waterway>>(_wr->id, _wr));
        continue;
      }
      if (agg) {
        auto ps = agg->pwr_station_();
        if (!std::ranges::any_of(power_plants, [&ps](auto const & p) {
              return p->name == ps->name;
            })) {
          auto _ps = hpsb.create_power_plant(ps->id, ps->name, ps->json);
          for (auto& _agg : ps->units) {
            if (_agg->name != agg->name) {
              auto agg_ = hpsb.create_unit(_agg->id, _agg->name, _agg->json);
              _ps->add_unit(_ps, agg_);
            }
          }
          auto agg_ = hpsb.create_unit(agg->id, agg->name, agg->json);
          _ps->add_unit(_ps, agg_);
          nagg.insert(std::pair<int, std::shared_ptr<unit>>(agg_->id, agg_));
        }
      }
    }
    auto havet = hpsb.create_reservoir(0, "havet");
    nres.insert(std::pair<int, std::shared_ptr<reservoir>>(havet->id, havet));

    for (auto& comp : collection) {
      auto wr = std::dynamic_pointer_cast<waterway>(comp);
      if (wr) {
        for (auto& connection : wr->upstreams) {
          // auto role = connection.role;
          auto target = connection.target;

          auto res = std::dynamic_pointer_cast<reservoir>(target);
          auto _wr = std::dynamic_pointer_cast<waterway>(target);
          auto agg = std::dynamic_pointer_cast<unit>(target);
          if (res) {
            nwr[wr->id]->input_from(nwr[wr->id], nres[res->id], wr->upstream_role());
            continue;
          }
          if (_wr) {
            nwr[wr->id]->input_from(nwr[wr->id], nwr[_wr->id]);
            continue;
          }
          if (agg) {
            nwr[wr->id]->input_from(nwr[wr->id], nagg[agg->id]);
          }
        }
        for (auto& connection : wr->downstreams) {
          // auto role = connection.role;
          auto target = connection.target;

          auto res = std::dynamic_pointer_cast<reservoir>(target);
          auto _wr = std::dynamic_pointer_cast<waterway>(target);
          auto agg = std::dynamic_pointer_cast<unit>(target);
          if (res) {
            nwr[wr->id]->output_to(nwr[wr->id], nres[res->id]);
            continue;
          }
          if (_wr) {
            nwr[wr->id]->output_to(nwr[wr->id], nwr[_wr->id]);
            continue;
          }
          if (agg) {
            nwr[wr->id]->output_to(nwr[wr->id], nagg[agg->id]);
          }
        }
      }
    }
  }

  void hydro_power_system::clear() {
    for (auto& c : reservoirs)
      c->clear();
    for (auto& c : waterways)
      c->clear();
    for (auto& c : units)
      c->clear();
    for (auto& c : catchments)
      c->clear();
    power_plants.clear();
    reservoirs.clear();
    waterways.clear();
    units.clear();
    catchments.clear();
  }

  hydro_power_system::~hydro_power_system() {
    clear();
  }

  std::shared_ptr<reservoir>
    hydro_power_system_builder::create_reservoir(int id, std::string const & name, std::string const & json) {
    if (std::ranges::any_of(hps->reservoirs, [&name](auto const & r) {
          return r->name == name;
        }))
      throw std::runtime_error(
        fmt::format("reservoir name must be unique within a hydro_power_system:{}", name)); // is that reasonable ?
    auto r = std::make_shared<reservoir>(id, name, json, hps);
    hps->reservoirs.push_back(r);
    return r;
  }

  std::shared_ptr<unit>
    hydro_power_system_builder::create_unit(int id, std::string const & name, std::string const & json) {
    if (std::ranges::any_of(hps->units, [&name](auto const & p) {
          return p->name == name;
        }))
      throw std::runtime_error(
        fmt::format("unit_ name must be unique within a HydroPowerSystem:{}", name)); // is that reasonable ?
    auto p = std::make_shared<unit>(id, name, json, hps);
    hps->units.push_back(p);
    return p;
  }

  std::shared_ptr<power_plant>
    hydro_power_system_builder::create_power_plant(int id, std::string const & name, std::string const & json) {
    if (std::ranges::any_of(hps->power_plants, [&name](auto const & p) {
          return p->name == name;
        }))
      throw std::runtime_error(
        fmt::format("power_station name must be unique within a HydroPowerSystem:{}", name)); // is that reasonable ?
    auto p = std::make_shared<power_plant>(id, name, json, hps);
    hps->power_plants.push_back(p);
    return p;
  }

  std::shared_ptr<waterway>
    hydro_power_system_builder::create_waterway(int id, std::string const & name, std::string const & json) {
    if (std::ranges::any_of(hps->waterways, [&name](auto const & w) {
          return w->name == name;
        }))
      throw std::runtime_error(
        fmt::format("waterway_ name must be unique within a HydroPowerSystem:{}", name)); // is that reasonable ?
    auto w = std::make_shared<waterway>(id, name, json, hps);
    hps->waterways.push_back(w);
    return w;
  }

  std::shared_ptr<gate>
    hydro_power_system_builder::create_gate(int id, std::string const & name, std::string const & json) {
    auto gates = hps->gates();
    if (std::ranges::any_of(gates, [&name](auto const & w) {
          return w->name == name;
        }))
      throw std::runtime_error(
        fmt::format("gate_ name must be unique within a HydroPowerSystem:{}", name)); // is that reasonable ?
    auto w = std::make_shared<gate>(id, name, json);
    return w;
  }

  std::shared_ptr<catchment>
    hydro_power_system_builder::create_catchment(int id, std::string const & name, std::string const & json) {
    if (std::ranges::any_of(hps->catchments, [&name](auto const & w) {
          return w->name == name;
        }))
      throw std::runtime_error(
        fmt::format("catchment name must be unique within a HydroPowerSystem:{}", name)); // is that reasonable ?
    auto w = std::make_shared<catchment>(id, name, json, hps);
    hps->catchments.push_back(w);
    return w;
  }

  std::shared_ptr<waterway>
    hydro_power_system_builder::create_tunnel(int id, std::string const & name, std::string const & json) {
    return create_waterway(id, name, json);
  }

  std::shared_ptr<waterway>
    hydro_power_system_builder::create_river(int id, std::string const & name, std::string const & json) {
    return create_waterway(id, name, json);
  }

  std::shared_ptr<reservoir> hydro_power_system::find_reservoir_by_name(std::string const & name) const {
    return find_by_name(reservoirs, name);
  }

  std::shared_ptr<unit> hydro_power_system::find_unit_by_name(std::string const & name) const {
    return find_by_name(units, name);
  }

  std::shared_ptr<power_plant> hydro_power_system::find_power_plant_by_name(std::string const & name) const {
    return find_by_name(power_plants, name);
  }

  std::shared_ptr<waterway> hydro_power_system::find_waterway_by_name(std::string const & name) const {
    return find_by_name(waterways, name);
  }

  std::shared_ptr<gate> hydro_power_system::find_gate_by_name(std::string const & name) const {
    auto gts = gates();
    return find_by_name(gts, name);
  }

  std::shared_ptr<catchment> hydro_power_system::find_catchment_by_name(std::string const & name) const {
    return find_by_name(catchments, name);
  }

  std::shared_ptr<reservoir> hydro_power_system::find_reservoir_by_id(std::int64_t id) const {
    return find_by_id(reservoirs, id);
  }

  std::shared_ptr<unit> hydro_power_system::find_unit_by_id(std::int64_t id) const {
    return find_by_id(units, id);
  }

  std::shared_ptr<power_plant> hydro_power_system::find_power_plant_by_id(std::int64_t id) const {
    return find_by_id(power_plants, id);
  }

  std::shared_ptr<waterway> hydro_power_system::find_waterway_by_id(std::int64_t id) const {
    return find_by_id(waterways, id);
  }

  std::shared_ptr<gate> hydro_power_system::find_gate_by_id(std::int64_t id) const {
    auto gts = gates();
    return find_by_id(gts, id);
  }

  std::shared_ptr<catchment> hydro_power_system::find_catchment_by_id(std::int64_t id) const {
    return find_by_id(catchments, id);
  }

  /* Verify that system a and b are structurally
   * equal.
   * This means almost all attributes& relations,
   * exceptions are typically db-identifiers/created/modified time.
   */
  bool hydro_power_system::equal_structure(hydro_power_system const & b) const {
    auto const & a = *this;

    /** returns true if the vector<ptr_obj>s are same set, and have equal structure */
    auto is_equal_structure = [](auto const & av, auto const & bv) {
      return std::is_permutation(begin(av), end(av), begin(bv), end(bv), [](auto const & ca, auto const & cb) {
        return ca->equal_structure(*cb);
      });
    };

    return is_equal_structure(a.units, b.units) && is_equal_structure(a.reservoirs, b.reservoirs)
        && is_equal_structure(a.waterways, b.waterways) && is_equal_structure(a.power_plants, b.power_plants)
        && is_equal_structure(a.catchments, b.catchments);
  }

  bool hydro_power_system::equal_content(hydro_power_system const & b) const {
    auto const & a = *this;

    /** returns true if the vector<ptr_obj>s are same set, including each object equal to each other */
    auto is_same_set_of_objects = [](auto const & av, auto const & bv) {
      return std::ranges::is_permutation(av, bv, [](auto const & ca, auto const & cb) {
        return *ca == *cb;
      });
    };

    return is_same_set_of_objects(a.units, b.units) && is_same_set_of_objects(a.reservoirs, b.reservoirs)
        && is_same_set_of_objects(a.waterways, b.waterways) && is_same_set_of_objects(a.power_plants, b.power_plants)
        && is_same_set_of_objects(a.catchments, b.catchments);
  }

  bool hydro_power_system::operator==(hydro_power_system const & o) const {
    if (!id_base::operator==(o) || created != o.created)
      return false;
    return equal_content(o);
  }

  std::string hydro_power_system::to_blob(std::shared_ptr<hydro_power_system> const & hps) {
    using namespace std;
    ostringstream xmls;
    {
      boost::archive::binary_oarchive oa(xmls);
      oa << boost::serialization::make_nvp("hps", hps);
    }
    xmls.flush();
    return xmls.str();
  }

  std::shared_ptr<hydro_power_system> hydro_power_system::from_blob(std::string xmls) {
    std::shared_ptr<hydro_power_system> hps;
    std::istringstream xmli(xmls);
    {
      boost::archive::binary_iarchive ia(xmli);
      ia >> boost::serialization::make_nvp("hps", hps);
    }
    return hps;
  }

}
