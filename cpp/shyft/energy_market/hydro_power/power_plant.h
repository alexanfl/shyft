/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/core_serialization.h>

#include <shyft/energy_market/hydro_power/hydro_component.h>

namespace shyft::energy_market::hydro_power {

  struct power_plant; // fwd

  /** @brief A hydro power unit, turbine and generator assembly
   *
   *
   * Can be pure production unit, or a pure pump, or both, depending on the construction.
   *
   * The power_station is the 'building' that keeps the units (generator-turbine).
   *
   * For the LTM models, power-stations are always simplified to one 'super-unit' that
   * resembles the properties equal to the sum of physical units.
   *
   * For the STM models, units are accurately described, and power-station is
   * just a natural group of those aggregates.
   *
   *
   * Hydrology: It takes one input from a water_route and have one output to water_route&
   */
  struct unit : hydro_component {

    SHYFT_DEFINE_STRUCT(unit, (hydro_component), ());

    unit() = default;
    unit(
      int id,
      std::string const & name,
      std::string const & json = "",
      std::shared_ptr<hydro_power_system> const & hps = nullptr)
      : hydro_component{id, name, json, hps} {};

    std::weak_ptr<power_plant>
      pwr_station; ///< should be weak ref. to the ps, but exposure through boost python gives a zero -ref.

    // ref SO: https://stackoverflow.com/questions/8233252/boostpython-and-weak-ptr-stuff-disappearing
    // for similar problem.
    auto pwr_station_() const {
      return pwr_station.lock();
    }

    std::shared_ptr<unit> shared_from_this() const; // via hps
    static std::shared_ptr<unit> const &
      input_from(std::shared_ptr<unit> const & me, std::shared_ptr<waterway> const & r);
    static std::shared_ptr<unit> const &
      output_to(std::shared_ptr<unit> const & me, std::shared_ptr<waterway> const & r);

    bool is_pump() const; ///< returns true if the unit is a pump, otherwise, false
    std::shared_ptr<waterway> downstream() const;
    std::shared_ptr<waterway> upstream() const;

    bool operator==(unit const & o) const;

    bool operator!=(unit const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  /**@brief A hydro power station is a collection of units
   *
   * The station concept in this context is a group of units, that
   * one would like to consider as one unit, where there can be associated
   * requirement/properties that are applicable to all, or to sum-aggregate constraint.
   *
   * Currently we place it as a separate well defined entity within the hydro power system.
   *
   * For the STM as such, - there might be other groups of units, where there is similar
   * needs to attach constraints, or properties.
   *
   * The navigation from station to it's units is by reference.
   * The reverse navigation, could be done using a weak-reference to the station.
   *
   */
  struct power_plant : id_base {

    SHYFT_DEFINE_STRUCT(power_plant, (id_base), ());

    power_plant() = default;

    power_plant(
      int id,
      std::string const & name,
      std::string const & json = "",
      std::shared_ptr<hydro_power_system> const & hps = nullptr)
      : id_base{id, name, json, {}, {}, {}}
      , hps(hps) {
    }

    virtual ~power_plant();
    std::vector<std::shared_ptr<unit>> units; ///< the units allocated to this object

    static void add_unit(std::shared_ptr<power_plant> const & ps, std::shared_ptr<unit> const & a);
    void remove_unit(std::shared_ptr<unit> const & a);
    bool equal_structure(power_plant const & o) const;
    bool operator==(power_plant const & o) const;

    bool operator!=(power_plant const & o) const {
      return !operator==(o);
    }

    auto hps_() const {
      return hps.lock();
    }

    std::weak_ptr<hydro_power_system> hps;                 ///< reference up to the 'owning' hydro-power system.
    std::shared_ptr<power_plant> shared_from_this() const; ///< shared from this using hps.power_plants shared pointers.
    x_serialize_decl();
  };


}

BOOST_CLASS_EXPORT_KEY2(
  shyft::energy_market::hydro_power::unit,
  BOOST_PP_STRINGIZE(shyft::energy_market::hydro_power::aggregate));
BOOST_CLASS_EXPORT_KEY2(
  shyft::energy_market::hydro_power::power_plant,
  BOOST_PP_STRINGIZE(shyft::energy_market::hydro_power::power_station));
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::unit, 2);

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::unit, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::unit, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::unit>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::unit, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::power_plant, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::power_plant, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::power_plant>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::power_plant, Char> { };
