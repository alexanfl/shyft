/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <memory>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/formatters.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::hydro_power {

  // fwd:
  struct hydro_power_system;

  /** @brief A catchment in context stm
   *
   * A catchment in this context is a water-source, that injects water into reservoirs/creeks
   * The catchment runoff [m3/s], is a f(t), projected to the reservoirs/creeks.
   * In near future the runoff is known, but due to weather and catchment model uncertainity,
   * the variations after a few days could be quite large.
   * It is common to represent this uncertainity as a set of scenarios.
   * Important to notice that the price-forecast is also influenced by the same set of scenarios in
   * a hydro-dominated market. So take care to ensure that prices and inflow forecasts are synchronized.
   */
  struct catchment : id_base {
    std::weak_ptr<hydro_power_system> hps;

    SHYFT_DEFINE_STRUCT(catchment, (id_base), ());

    catchment() = default;

    catchment(int id, std::string const & name, std::string const & json, std::shared_ptr<hydro_power_system> a_hps)
      : id_base{id, name, json, {}, {}, {}}
      , hps(a_hps) {
    }

    virtual ~catchment(); // Make catchment polymorphic

    auto hps_() const {
      return hps.lock();
    }

    void clear();

    bool equal_structure(catchment const & o) const {
      return id == o.id;
    }

    bool operator==(catchment const & o) const;

    bool operator!=(catchment const & o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  typedef std::shared_ptr<catchment> catchment_;

}

x_serialize_export_key(shyft::energy_market::hydro_power::catchment);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::catchment, 1); // proper handling of baseclass serialization

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::catchment, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::catchment, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::catchment>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::catchment, Char> { };
