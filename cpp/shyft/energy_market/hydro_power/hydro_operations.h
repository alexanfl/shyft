#pragma once
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <map>

namespace shyft::energy_market::hydro_power {

  /** @brief A collection of hydro operations
   *
   * Available operations:
   * 1) Find path to ocean for a given hydro-component (reservoir, waterway, or unit)
   * 2) Find the hydraulic path between two hydro-components
   * 3) Find out whether two components are connected or not.
   * 4) Extract sub-hydro systems from an exciting hydro system.
   *
   */
  struct hydro_operations {

    hydro_operations() = delete; // Disallow creating an instance of this type

    /** @brief Path to ocean
     *
     * Finds the complete list of hydro-components connected through a given connection role (main, bypass, flood)
     * which constitutes a water way to the ocean.
     *
     * @param path[in, out]       Water way to the ocean
     * @param role[in]            Connection role
     *
     * @see get_path_to_ocean
     */
    static void path_to_ocean(std::vector<std::shared_ptr<hydro_component>> &path, connection_role role);

    /** @brief Path to ocean
     *
     * Finds the complete list of hydro-components connected through a main connection role
     * which constitutes a water way to the ocean.
     *
     * @param path[in, out]       Water way to the ocean
     *
     * @see get_path_to_ocean
     */
    static void path_to_ocean(std::vector<std::shared_ptr<hydro_component>> &path);

    /** @brief Path to ocean
     *
     * Returns the complete list of hydro-components connected to the given hydro-component through the specified
     * connection role (main, bypass, flood, input) which constitutes a water way to the ocean.
     *
     * @param component[in]       A hydro_component (e.g., reservoir, unit, waterway)
     * @param role[in]            Connection role
     * @return                    A std::vector of hydro-components constituting the water way to the ocean
     *
     * @see path_to_ocean
     */
    static std::vector<std::shared_ptr<hydro_component>>
      get_path_to_ocean(std::shared_ptr<hydro_component> const &component, connection_role role);

    /** @brief Path to ocean
     *
     * Returns the complete list of hydro-components connected to the given hydro-component through a main
     * connection role which constitutes a water way to the ocean.
     *
     * @param component[in]       A hydro_component (e.g., reservoir, unit, waterway)
     * @return                    A std::vector of hydro-components constituting the water way to the ocean
     *
     * @see path_to_ocean
     */
    static std::vector<std::shared_ptr<hydro_component>>
      get_path_to_ocean(std::shared_ptr<hydro_component> const &component);

    /** @brief Hydraulic path between two hydro components
     *
     * Returns the complete list of hydro-components connected to the given hydro-components through the specified
     * connection role (main, bypass, flood).
     *
     * @param comp_1[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param comp_2[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param role[in]            Connection role (main, bypass, flood, input)
     * @return                    A std::vector of hydro-components constituting the path between the components
     *
     * @see is_connected
     */
    static std::vector<std::shared_ptr<hydro_component>> get_path_between(
      std::shared_ptr<hydro_component> const &comp_1,
      std::shared_ptr<hydro_component> const &comp_2,
      connection_role role);

    /** @brief Hydraulic path between two hydro components
     *
     * Returns the complete list of hydro-components connected to the given hydro-components through a main
     * connection role.
     *
     * @param comp_1[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param comp_2[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @return                    A std::vector of hydro-components constituting the path between the components
     *
     * @see is_connected
     */
    static std::vector<std::shared_ptr<hydro_component>>
      get_path_between(std::shared_ptr<hydro_component> const &comp_1, std::shared_ptr<hydro_component> const &comp_2);

    /** @brief Determines whether there is a hydraulic path between two hydro components or not
     *
     * Returns true if there is a hydraulic path connecting the two hydro components through the given
     * connection role (main, bypass, flood, input), otherwise, returns false.
     *
     * @param comp_1[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param comp_2[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param role[in]            Connection role
     * @return                    True (bool) if the components are connected, otherwise, false (bool)
     *
     * @see get_path_between
     */
    static bool is_connected(
      std::shared_ptr<hydro_component> const &comp_1,
      std::shared_ptr<hydro_component> const &comp_2,
      connection_role role);

    /** @brief Determines whether there is a hydraulic path between two hydro components or not
     *
     * Returns true if there is a hydraulic path connecting the two hydro components through a main
     * connection role, otherwise, returns false.
     *
     * @param comp_1[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param comp_2[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @return                    True (bool) if the components are connected, otherwise, false (bool)
     *
     * @see get_path_between
     */
    static bool
      is_connected(std::shared_ptr<hydro_component> const &comp_1, std::shared_ptr<hydro_component> const &comp_2);

    /** @brief Creates watercourses (a collection of hydro-power systems)
     *
     * Given a hydro-power system, partitions the system into separate hydraulically coupled sub-systems/watercourses.
     *
     * @param hps[in]          A hydro-power system
     * @return                 A collection of watercourses (hydro-power systems)
     *
     * @see get_water_course
     */
    static std::vector<std::shared_ptr<hydro_power_system>>
      extract_water_courses(std::shared_ptr<hydro_power_system> &hps);

   private:
    /** @brief Adds component to the collection of hydro-components
     *
     * Adds the component to the set of the collection of connected hydro components if it's not already present.
     *
     * @param component[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param collection[in, out]    A collection of hydro-components
     * @return                       True (bool) if the component is in the collection, otherwise, false (bool)
     *
     * @see add_neighbors, get_water_course, extract_water_courses
     */
    static bool add_to_collection(
      std::shared_ptr<hydro_component> const &component,
      std::set<std::shared_ptr<hydro_component>> &collection);

    /** @brief Adds up- and downstreams of the component to the collection of hydro-components
     *
     * Adds the neighbors of component to the collection of connected hydro components, which are stored in
     * upstreams and downstreams std::vectors.
     *
     * @param component[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @param collection[in, out]    A collection of hydro-components
     *
     * @see add_neighbors, get_water_course, extract_water_courses
     */
    static void add_neighbors(
      std::shared_ptr<hydro_component> const &component,
      std::set<std::shared_ptr<hydro_component>> &collection);

    /**@brief Creates a watercourse (a hydro_power_system)
     *
     * Given a component, finds all the other components connected to the component through all possible
     * water routes. The water route may be a main water route, an input, bypass or caused by a flood.
     * Once all the hydro-components connected to the component are found, a watercourse (hydro_power_system)
     * is constructed and returned.
     *
     * @param component[in]          A hydro_component (e.g., reservoir, waterway, unit)
     * @return                       A watercourse (a std::shared_ptr<hydro_power_system>)
     *
     * @see extract_water_courses
     */
    static std::shared_ptr<hydro_power_system> get_water_course(std::shared_ptr<hydro_component> const &component);
  };

} // namespace shyft::energy_market::hydro_power
