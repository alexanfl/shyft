/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/hydro_component.h>

#include <stdexcept>
#include <algorithm>
#include <typeinfo>

#include <fmt/core.h>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>

namespace shyft::energy_market::hydro_power {

  void hydro_component::connect(
    std::shared_ptr<reservoir> const & upstream,
    connection_role u_role,
    std::shared_ptr<waterway> const & downstream) {
    if (u_role == connection_role::input)
      throw std::runtime_error(
        fmt::format("Legal water_route reservoir roles are main,bypass,flood, supplied role was illegal {}", u_role));
    if (downstream->upstreams.size() > 0)
      throw std::runtime_error(fmt::format(
        "water_route take only input from one reservoir: The {} input is already connected to {}",
        downstream->name,
        downstream->upstreams[0].target->name));
    connect(upstream, u_role, downstream, connection_role::input);
  }

  void hydro_component::connect(std::shared_ptr<unit> const & upstream, std::shared_ptr<waterway> const & downstream) {
    if (upstream->downstreams.size() > 0)
      throw std::runtime_error(fmt::format(
        "aggregate can only have one output connected: The {} output is already connected to {}",
        upstream->name,
        upstream->downstreams[0].target->name));
    connect(upstream, connection_role::main, downstream, connection_role::input);
  }

  void hydro_component::connect(
    std::shared_ptr<waterway> const & upstream_tunnel,
    std::shared_ptr<unit> const & power_station) {
    if (power_station->upstreams.size() > 0)
      throw std::runtime_error(fmt::format(
        "aggregate take only input from one water_route_: The {} input is already connected to {}",
        power_station->name,
        power_station->upstreams[0].target->name));
    if (upstream_tunnel->downstreams.size() > 0)
      throw std::runtime_error(fmt::format(
        "water_route_ have only one main output: The {} output is already connected to {}",
        upstream_tunnel->name,
        upstream_tunnel->downstreams[0].target->name));
    connect(upstream_tunnel, connection_role::main, power_station, connection_role::input);
  }

  void hydro_component::connect(
    std::shared_ptr<waterway> const & upstream,
    std::shared_ptr<reservoir> const & downstream) {
    if (upstream->downstreams.size() > 0)
      throw std::runtime_error(fmt::format(
        "water-route output can only be connected to one object: The {} output is already connected to {}",
        upstream->name,
        upstream->downstreams[0].target->name));
    connect(upstream, connection_role::main, downstream, connection_role::input);
  }

  void
    hydro_component::connect(std::shared_ptr<waterway> const & upstream, std::shared_ptr<waterway> const & junction) {
    connect(upstream, connection_role::main, junction, connection_role::input);
  }

  void hydro_component::disconnect(
    std::shared_ptr<hydro_component> const & c1,
    std::shared_ptr<hydro_component> const & c2) {
    c1->disconnect_from(*c2);
  }

  bool hydro_component::equal_structure(hydro_component const & o) const {
    // Check that local structure is the same
    // 1: check that basic own attributes are the same
    if (this == &o)
      return true; // same ref, always equal
    if (id != o.id)
      return false; // because we insist on same object id's
    // 1: Check that we have the same component type
    if (typeid(*this) != typeid(o))
      return false;


    // 2: Check that the up- and downstream connections have the same role and target, and possibly target type
    constexpr auto equal_hydro_connection_predicate = [](auto const & c1, auto const & c2) -> bool {
      constexpr auto equal_targets = [](auto const & a, auto const & b) {
        return (a == nullptr && b == nullptr)
            || ((a && b && typeid(*a) == typeid(*b))
                && a->id == b->id); // not checking type-id..might be ok, it is the same structure.
      };
      return c1.role == c2.role && equal_targets(c1.target, c2.target);
    };

    if (!std::is_permutation(
          upstreams.begin(), upstreams.end(), o.upstreams.begin(), o.upstreams.end(), equal_hydro_connection_predicate))
      return false;


    if (!std::is_permutation(
          downstreams.begin(),
          downstreams.end(),
          o.downstreams.begin(),
          o.downstreams.end(),
          equal_hydro_connection_predicate))
      return false;

    return true;
  }

  void hydro_component::connect(
    std::shared_ptr<hydro_component> const & upstream,
    connection_role u_role,
    std::shared_ptr<hydro_component> const & downstream,
    connection_role d_role) {
    if (upstream == nullptr || downstream == nullptr)
      throw std::runtime_error("Only connect to non-nullptr components are allowed");

    if (upstream->hps_() != downstream->hps_()) {
      throw std::runtime_error(fmt::format(
        "Only components within the same system are allowed to interconnect,\t component1 system={}\n\t component2 "
        "system {}",
        (upstream->hps_() != nullptr ? upstream->hps_()->name : std::string("none")),
        (downstream->hps_() != nullptr ? downstream->hps_()->name : std::string("none"))));
    }
    // check for exsiting connection, update it. (note that two relations for same object does not give meaning in this
    // context/model)
    auto us_ds_hc = std::ranges::find_if(upstream->downstreams, [&](auto const & c) {
      return c.target.get() == downstream.get();
    });
    if (us_ds_hc != std::end(upstream->downstreams))
      us_ds_hc->role = u_role; // update existing role
    else
      upstream->downstreams.push_back(hydro_connection(u_role, downstream)); // add new
    auto ds_us_hc = std::ranges::find_if(downstream->upstreams, [&](auto const & c) {
      return c.target.get() == upstream.get();
    });
    if (ds_us_hc != std::end(downstream->upstreams))
      ds_us_hc->role = d_role;
    else
      downstream->upstreams.push_back(hydro_connection(d_role, upstream));
  }

  /** get rid of all references between this and the other object o */
  void hydro_component::disconnect_from(hydro_component& c2) {
    auto& c1 = *this;
    c1.downstreams.erase(
      remove_if(
        c1.downstreams.begin(),
        c1.downstreams.end(),
        [&c2](auto const & c) -> bool {
          return c.target.get() == &c2;
        }),
      c1.downstreams.end());
    c2.downstreams.erase(
      remove_if(
        c2.downstreams.begin(),
        c2.downstreams.end(),
        [&c1](auto const & c) -> bool {
          return c.target.get() == &c1;
        }),
      c2.downstreams.end());
    c1.upstreams.erase(
      remove_if(
        c1.upstreams.begin(),
        c1.upstreams.end(),
        [&c2](auto const & c) -> bool {
          return c.target.get() == &c2;
        }),
      c1.upstreams.end());
    c2.upstreams.erase(
      remove_if(
        c2.upstreams.begin(),
        c2.upstreams.end(),
        [&c1](auto const & c) -> bool {
          return c.target.get() == &c1;
        }),
      c2.upstreams.end());
  }

  void hydro_component::clear() {
    while (upstreams.size()) {
      disconnect_from(*(upstreams.back().target));
    }
    while (downstreams.size()) {
      disconnect_from(*(downstreams.back().target));
    }
    upstreams.clear();
    downstreams.clear();
    hps.reset();
  }

  hydro_component::~hydro_component() {
    clear();
  }


}
