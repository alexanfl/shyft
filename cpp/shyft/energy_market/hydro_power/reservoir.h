/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/formatters.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

namespace shyft::energy_market::hydro_power {

  struct reservoir_aggregate;

  /** @brief A reservoir, stores water
   *
   * Any reservoir of any size, including those practically 0 (open tunnel inlets, creeks).
   *
   *  Hydrology: It takes multiple inputs from WaterRoutes, and have unlimited
   *            outputs to WaterRoutes via roles main,flood, bypass
   *              each of these WaterRoutes might have hatches/gates on their input-side that controls the amount of
   * flow It do have a volume-curve, - mas -> Mm3
   */
  struct reservoir : hydro_component {

    SHYFT_DEFINE_STRUCT(reservoir, (hydro_component), ());

    reservoir() = default;

    // copy and assign is problematic
    // from a semantic point of view
    // due to hydro-connections.
    // -- maybe provide a limited copy ?
    reservoir(reservoir const &) = delete;
    reservoir &operator=(reservoir const &) = delete;

    //--
    reservoir(
      int id,
      std::string const &name,
      std::string const &json = "",
      std::shared_ptr<hydro_power_system> hps = nullptr)
      : hydro_component{id, name, json, hps} {};
    std::shared_ptr<reservoir> shared_from_this() const;
    static std::shared_ptr<reservoir> const &
      input_from(std::shared_ptr<reservoir> const &me, std::shared_ptr<waterway> const &r);
    static std::shared_ptr<reservoir> const &
      output_to(std::shared_ptr<reservoir> const &me, std::shared_ptr<waterway> const &w, connection_role role);

    // bool equal_structure(const reservoir& o) const;
    bool operator==(reservoir const &o) const;

    bool operator!=(reservoir const &o) const {
      return !operator==(o);
    }

    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::energy_market::hydro_power::reservoir);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::reservoir, 1); // proper handling of id_base serialization

template <typename Char>
struct fmt::formatter<shyft::energy_market::hydro_power::reservoir, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::hydro_power::reservoir, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::hydro_power::reservoir>, Char>
  : shyft::ptr_formatter<shyft::energy_market::hydro_power::reservoir, Char> { };
