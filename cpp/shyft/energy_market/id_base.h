/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <map>
#include <functional>

#include <fmt/format.h>

#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/energy_market/em_handle.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market {

  /**
   * @brief identity and common stuff for python enabled components
   *
   * Usually the id is unique within the scope (e.g. area, hydro-power-system).
   * The name could also be unique, depending on context.
   * The tsm is for storing additinal time series, extending the regular set
   * of attributes exposed as individual members on specialized class.
   * The json is for storing arbitrary data, that is primarly handled in python.
   */
  struct id_base {
    std::int64_t id{0}; ///< unique, in the scope of context (type/level) etc.
    std::string name;   ///< the name of the object
    std::string json;   ///< json, to be used by the python-side
    std::map<std::string, time_series::dd::apoint_ts, std::less<>> tsm; ///< custom time series
    std::map<std::string, stm::any_attr, std::less<>> custom;           ///< custom attributes
    em_handle h;                                                        ///< handle to external python objects

    SHYFT_DEFINE_STRUCT(id_base, (), ());

    bool operator==(id_base const & o) const {
      return id == o.id && name == o.name && json == o.json && tsm == o.tsm && custom == o.custom;
    }

    bool operator!=(id_base const & o) const {
      return !operator==(o);
    }

    bool operator<(id_base const & o) const {
      return name < o.name;
    }
    void generate_url(std::back_insert_iterator<std::string>& /*rbi*/, int /*levels*/ = -1, int /*template_levels*/ = -1) const {};
    x_serialize_decl();
  };

  template <typename From, typename Char>
  using id_base_formatter = cast_formatter<From, id_base, Char>;

}

x_serialize_export_key(shyft::energy_market::id_base);
BOOST_CLASS_VERSION(shyft::energy_market::id_base, 2); // custom

template <typename Char>
struct fmt::formatter<shyft::energy_market::id_base, Char> {
  FMT_CONSTEXPR auto parse(auto& ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  auto format(shyft::energy_market::id_base const & base, auto& ctx) const {
    auto out = ctx.out();
    return fmt::format_to(out, "{{.id = {},.name = {}}}", base.id, base.name);
  }
};
