/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>

#include <utility> // For pair
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_iterator.hpp>
#include <boost/graph/properties.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/property_map/property_map.hpp> // For put_get_helper

/**
 * Classes and functions to adapt shyft/energy_market/hydro_power_system for the boost.graph
 * algorithms. Cf. https://www.boost.org/doc/libs/1_71_0/libs/graph/doc/index.html.
 *
 * The idea is that the hydro_power_system structure stores data well-suited for the
 * IncidenceGraph- and VertexListGraph concepts utilized by Boost.Graph.
 *
 * Here, we implement an overload of boost::graph_traits<hydro_power_system>,
 * together with specialized iterator classes to traverse vertices and in- and out- edges of the graph.
 */

namespace boost {
  using hps = shyft::energy_market::hydro_power::hydro_power_system;
  using shyft::energy_market::hydro_power::hydro_connection;
  using shyft::energy_market::hydro_power::hydro_component;
  using hps_edge = std::pair<std::shared_ptr<hydro_component>, hydro_connection>;

  using shyft::energy_market::hydro_power::reservoir;
  using shyft::energy_market::hydro_power::unit;
  using shyft::energy_market::hydro_power::waterway;

  /**
   * Class to encapsulate what types of traversal are allowed for a hydro power system
   */
  struct hps_traversal_category
    : public virtual vertex_list_graph_tag
    ,
      // public virtual edge_list_graph_tag,
      public virtual bidirectional_graph_tag { };

  // Forward declarations of iterators for graph_traits:
  class hps_vertex_iterator;
  class hps_edge_iterator;
  class hps_out_edge_iterator;

  // Forward declarations of paired iterator access functions:
  std::pair<hps_vertex_iterator, hps_vertex_iterator> vertices(hps const &);
  std::pair<hps_out_edge_iterator, hps_out_edge_iterator>
    out_edges(std::shared_ptr<hydro_component> const &, hps const &);

  /**
   * The graph_traits<hps> overload.
   */
  template <>
  struct graph_traits<hps> {
    using traversal_category = hps_traversal_category; // What types of traversal over vertices and edges are allowed
    using directed_category = undirected_tag;
    using edge_parallel_category = allow_parallel_edge_tag;
    // How we represent vertices and edges in a hps graph:
    using vertex_descriptor = std::shared_ptr<hydro_component>;
    using edge_descriptor = hps_edge;

    // Misc. size types used:
    using vertices_size_type = size_t;
    using degree_size_type = size_t;
    using edges_size_type = size_t;

    // Various iterator classes used to traverse graph components:
    using vertex_iterator = hps_vertex_iterator;
    using edge_iterator = hps_edge_iterator;
    using out_edge_iterator = hps_out_edge_iterator;
    using in_edge_iterator = hps_out_edge_iterator;
    // using adjacency_iterator = adjacency_iterator_generator<hps, vertex_descriptor, out_edge_iterator>::type;
  };

  /**
   * vertex_iterator: Used to iterate over vertices in a hps.
   * Note that the vertices in a hps is the union of reservoirs, power units and water_ways.
   * Here we treat the set of all vertices as the concatenation of these three vectors in the following order:
   *      reservoirs -> units -> waterways
   *
   * We overload boost's iterator facade: Cf.
   * https://www.boost.org/doc/libs/1_71_0/libs/iterator/doc/iterator_facade.html
   */
  class hps_vertex_iterator
    : public iterator_facade<
        hps_vertex_iterator,
        std::shared_ptr<hydro_component>, // The dereferenced value-type
        bidirectional_traversal_tag,      // We can both increment and decrement
        std::shared_ptr<hydro_component>  // The value type of a reference
        > {
   public:
    hps_vertex_iterator(std::shared_ptr<hydro_component> u = nullptr, hps const *agraph = nullptr)
      : base(u)
      , g(agraph) {
      if (base && g) {
        // Find base in g:
        auto rnode = std::dynamic_pointer_cast<reservoir>(u);
        if (rnode)
          rsv_it = std::find_if(g->reservoirs.begin(), g->reservoirs.end(), [&u](auto el) {
            return *u == *el;
          });
        else
          rsv_it = g->reservoirs.end();

        auto unode = std::dynamic_pointer_cast<unit>(u);
        if (unode)
          u_it = std::find_if(g->units.begin(), g->units.end(), [&u](auto el) {
            return *u == *el;
          });
        else
          u_it = g->units.end();

        auto wnode = std::dynamic_pointer_cast<waterway>(u);
        if (wnode)
          wtr_it = std::find_if(g->waterways.begin(), g->waterways.end(), [&u](auto el) {
            return *u == *el;
          });
        else
          wtr_it = g->waterways.end();
      } else if (g) {
        rsv_it = g->reservoirs.end();
        u_it = g->units.end();
        wtr_it = g->waterways.end();
      }
    }

   private:
    // Member functions needed by iterator_facade:
    const std::shared_ptr<hydro_component> dereference() const {
      return base;
    }

    bool equal(hps_vertex_iterator const &other) const {
      if (!(other.base) && !base)
        return true; // If both are unset
      if (!(other.base) || !base)
        return false; // If one, but not both are set.
      return *other.base == *base;
    }

    // Helper function to set base from current state of iterators:
    void base_from_iterator() {
      if (rsv_it != g->reservoirs.end()) {
        base = *rsv_it;
        return;
      }
      if (u_it != g->units.end()) {
        base = *u_it;
        return;
      }
      if (wtr_it != g->waterways.end()) {
        base = *wtr_it;
        return;
      }
      base = nullptr;
    }

    /**
     * How to go to the next element of our "container".
     * In our case we need to take special care when we're switching from reservoirs to units,
     * or units to waterways.
     */
    void increment() {
      if (rsv_it != g->reservoirs.end()) {
        ++rsv_it;
        // If at the end, go to next container:
        if (rsv_it == g->reservoirs.end())
          u_it = g->units.begin();
        if (u_it == g->units.end())
          wtr_it = g->waterways.begin();
      } else if (u_it != g->units.end()) {
        ++u_it;
        if (u_it == g->units.end())
          wtr_it = g->waterways.begin();
      } else if (wtr_it != g->waterways.end()) {
        ++wtr_it;
      }
      base_from_iterator();
    }

    /**
     * How to go to the previous element. Same as increment, but in reverse.
     */
    void decrement() {
      if (rsv_it != g->reservoirs.end()) {
        --rsv_it;
      } else if (u_it != g->units.end()) {
        if (u_it == g->units.begin()) {
          u_it = g->units.end();
          rsv_it = g->reservoirs.end();
          if (g->reservoirs.size())
            --rsv_it;
        } else {
          --u_it;
        }
      } else {
        if (wtr_it == g->waterways.begin()) {
          wtr_it = g->waterways.end();
          u_it = g->units.end();
          if (g->units.size())
            --u_it;
          else {
            rsv_it = g->reservoirs.end();
            --rsv_it;
          }
        } else {
          --wtr_it;
        }
      }
      base_from_iterator();
    }

    // Members:
    std::shared_ptr<hydro_component> base;
    hps const *g;

    std::vector<std::shared_ptr<reservoir>>::const_iterator rsv_it;
    std::vector<std::shared_ptr<unit>>::const_iterator u_it;
    std::vector<std::shared_ptr<waterway>>::const_iterator wtr_it;

    friend class iterator_core_access;
  };

  /**
   * Outgoing edge iterator for hps.
   * This is as this as possibly wrapper for a standard iterator for hydro_component.downstreams
   * concatenated with upstreams, but
   * Boost.Graph also needs to know about the source, so we also provide that when dereferencing.
   */
  class hps_out_edge_iterator
    : public iterator_facade<hps_out_edge_iterator, hps_edge, bidirectional_traversal_tag, hps_edge> {
   public:
    hps_out_edge_iterator(hydro_connection const *conn = nullptr, std::shared_ptr<hydro_component> asource = nullptr)
      : source(asource) {
      // Check if we can find the connection among source's connections
      if (source) {
        if (conn) {
          out_iter = std::find_if(source->downstreams.begin(), source->downstreams.end(), [conn](auto el) {
            return *conn == el;
          });
          if (out_iter == source->downstreams.end()) {
            in_iter = std::find_if(source->upstreams.begin(), source->upstreams.end(), [conn](auto el) {
              return *conn == el;
            });
          } else
            in_iter = source->upstreams.end();
        } else {
          out_iter = source->downstreams.end();
          in_iter = source->upstreams.end();
        }
      }
    }

   private:
    const hps_edge dereference() const {
      if (source) {
        if (out_iter != source->downstreams.end()) {
          return std::make_pair(source, *out_iter);
        } else if (in_iter != source->upstreams.end()) {
          return std::make_pair(source, *in_iter);
        }
      }
      throw std::runtime_error("Trying to dereference an invalid hps_out_edge_iterator");
    }

    /* Compares to true if the iterators have the same source, same role and same target. */
    bool equal(hps_out_edge_iterator const &other) const {
      if (!source || !other.source)
        return false; // One of the sources isn't set.
      if (*source != *other.source)
        return false; // They don't have the same source.
      return out_iter == other.out_iter && in_iter == other.in_iter;
    }

    void increment() {
      // Check if we're currently on downstreams:
      if (out_iter != source->downstreams.end()) {
        ++out_iter;
        if (out_iter == source->downstreams.end())
          in_iter = source->upstreams.begin();
      } else if (in_iter != source->upstreams.end())
        ++in_iter;
    }

    void decrement() {
      if (out_iter != source->downstreams.end()) {
        --out_iter;
      } else {
        if (in_iter == source->upstreams.begin()) {
          in_iter = source->upstreams.end();
          out_iter = source->downstreams.end();
          --out_iter;
        } else
          --in_iter;
      }
    }

    // This can definitely be simplified using e.g. boost::range::join
    std::vector<hydro_connection>::const_iterator out_iter;
    std::vector<hydro_connection>::const_iterator in_iter;
    std::shared_ptr<hydro_component> source;

    friend class iterator_core_access;
  };

  /**
   * edge_iterator: Used to iterate over edges in a hps.
   *
   * This combines 'hps_vertex_iterator' and 'hps_out_edge' iterator to
   * construct an iterator traversing all edges in the hps. It essentially
   * consists of an outer iterator keeping track of vertices and an inner
   * iterator tracking local edges. Some care have to be taken in handling
   * a vertex that does not have any edges.
   *
   * We overload boost's iterator facade: Cf.
   * https://www.boost.org/doc/libs/1_71_0/libs/iterator/doc/iterator_facade.html
   */
  class hps_edge_iterator : public iterator_facade<hps_edge_iterator, hps_edge, bidirectional_traversal_tag, hps_edge> {
   public:
    hps_edge_iterator(
      hydro_connection const *conn = nullptr,
      std::shared_ptr<hydro_component> source = nullptr,
      hps const *agraph = nullptr)
      : g(agraph) {
      if (g) {
        std::tie(vertices_begin, vertices_end) = vertices(*g);
        vertex_iter = hps_vertex_iterator(source, g);

        if (source) {
          std::tie(local_edges_begin, local_edges_end) = out_edges(source, *g);

          if (conn) {
            local_edge_iter = hps_out_edge_iterator(conn, source);
          } else
            local_edge_iter = local_edges_end;
        }
        // no meaing to local edge iterators if we reach end of vertices
      }
    }

   private:
    const hps_edge dereference() const {
      if (vertex_iter != vertices_end && local_edge_iter != local_edges_end) {
        return *local_edge_iter;
      }

      throw std::runtime_error("Trying to dereference an invalid hps_edge_iterator");
    }

    bool equal(hps_edge_iterator const &other) const {
      // local_edge_iter becomes meaningless after exhausting vertices
      return vertex_iter == other.vertex_iter
          && (vertex_iter == vertices_end || local_edge_iter == other.local_edge_iter);
    }

    void increment() {
      if (vertex_iter != vertices_end) {
        if (local_edge_iter != local_edges_end) {
          ++local_edge_iter;
        }
        // there may be empty vertices with no edges, those we have to skip
        while (local_edge_iter == local_edges_end) {
          if (++vertex_iter == vertices_end)
            // we reached the end, local_edge_iter is now meaningless
            break;

          std::tie(local_edges_begin, local_edges_end) = out_edges(*vertex_iter, *g);
          local_edge_iter = local_edges_begin;
        }
      }
    }

    void decrement() {
      if (vertex_iter == vertices_end) {
        // move to first preceding vertex with at least one edge
        do {
          if (vertex_iter == vertices_begin)
            break;

          --vertex_iter;
          std::tie(local_edges_begin, local_edges_end) = out_edges(*vertex_iter, *g);
          local_edge_iter = local_edges_end;
        } while (local_edges_begin == local_edges_end); // continue until first vertex with edges
      }
      // when at first local edge, move to first preceding vertex with at least one edge
      while (local_edge_iter == local_edges_begin) {
        if (vertex_iter == vertices_begin)
          break;

        --vertex_iter;
        std::tie(local_edges_begin, local_edges_end) = out_edges(*vertex_iter, *g);
        local_edge_iter = local_edges_end;
      }
      --local_edge_iter;
    }

    hps_vertex_iterator vertex_iter;
    hps_vertex_iterator vertices_begin;
    hps_vertex_iterator vertices_end;

    hps_out_edge_iterator local_edge_iter;
    hps_out_edge_iterator local_edges_begin;
    hps_out_edge_iterator local_edges_end;

    hps const *g;

    friend class iterator_core_access;
  };

  /**
   * Property maps for HPS:
   */
  // Getting the ID of a vertex.
  // TODO: Improve design and implementation.
  class hps_id_map : public put_get_helper<int, hps_id_map> {
   public:
    using category = readable_property_map_tag;
    using value_type = int;
    using reference = int;
    using key_type = graph_traits<hps>::vertex_descriptor;

    hps_id_map(hps const *ag)
      : g(ag) {
      rsv_offset = g->reservoirs.size();
      unit_offset = rsv_offset + g->units.size();
    }

    value_type operator[](key_type const &u) const {
      auto rsv = std::dynamic_pointer_cast<reservoir>(u);
      if (rsv) {
        auto it = std::find_if(g->reservoirs.begin(), g->reservoirs.end(), [&u](auto el) {
          return el->id == u->id;
        });
        if (it != g->reservoirs.end())
          return it - g->reservoirs.begin();
      }
      auto ut = std::dynamic_pointer_cast<unit>(u);
      if (ut) {
        auto it = std::find_if(g->units.begin(), g->units.end(), [&u](auto el) {
          return el->id == u->id;
        });
        if (it != g->units.end())
          return it - g->units.begin() + rsv_offset;
      }
      auto wtr = std::dynamic_pointer_cast<waterway>(u);
      if (wtr) {
        auto it = std::find_if(g->waterways.begin(), g->waterways.end(), [&u](auto el) {
          return el->id == u->id;
        });
        if (it != g->waterways.end())
          return it - g->waterways.begin() + unit_offset;
      }
      throw std::runtime_error("Unable to for the given vertex in the hydro power system");
    }

   private:
    int rsv_offset;
    int unit_offset;
    hps const *g;
  };

  inline hps_id_map get(vertex_index_t, hps const &g) {
    return hps_id_map(&g);
  }

  // Although strictly not needed, we also overload the propertymap for vertex indices:
  template <>
  struct property_map<hps, vertex_index_t> {
    using type = hps_id_map;
    using const_type = hps_id_map;
  };

  /*
   * Property map of a vertex:
   */
  class hps_name_map : public put_get_helper<std::string, hps_name_map> {
   public:
    using category = readable_property_map_tag;
    using value_type = std::string;
    using reference = std::string const &;
    using key_type = graph_traits<hps>::vertex_descriptor;

    hps_name_map() = default;

    reference operator[](graph_traits<hps>::vertex_descriptor const &u) const {
      return u->name;
    }
  };

  inline hps_name_map get(vertex_name_t, hps const & /*g*/) {
    return hps_name_map();
  }

  template <>
  struct property_map<hps, vertex_name_t> {
    using type = hps_name_map;
    using const_type = hps_name_map;
  };

  /*
   * Functions for hps to satisfy the VertexListGraph concept
   */
  inline graph_traits<hps>::vertices_size_type num_vertices(hps const &g) {
    return g.reservoirs.size() + g.units.size() + g.waterways.size();
  }

  inline std::pair<graph_traits<hps>::vertex_iterator, graph_traits<hps>::vertex_iterator> vertices(hps const &g) {
    using Iter = graph_traits<hps>::vertex_iterator;
    if (g.reservoirs.size())
      return std::make_pair(Iter(g.reservoirs[0], &g), Iter(nullptr, &g));
    else if (g.units.size())
      return std::make_pair(Iter(g.units[0], &g), Iter(nullptr, &g));
    else if (g.waterways.size())
      return std::make_pair(Iter(g.waterways[0], &g), Iter(nullptr, &g));
    else
      throw std::runtime_error("Unable to create hps_vertex_iterators because hps looks to be empty.");
  }

  inline std::pair<graph_traits<hps>::out_edge_iterator, graph_traits<hps>::out_edge_iterator>
    out_edges(graph_traits<hps>::vertex_descriptor const &u, hps const &) {
    using Iter = graph_traits<hps>::out_edge_iterator;
    if (u->downstreams.size())
      return std::make_pair(Iter(&u->downstreams[0], u), Iter(nullptr, u));
    else if (u->upstreams.size())
      return std::make_pair(Iter(&u->upstreams[0], u), Iter(nullptr, u));
    else
      return std::make_pair(Iter(nullptr, u), Iter(nullptr, u));
  }

  /*
   * Functions for hps to satisfy the EdgeListGraph concept
   */
  inline graph_traits<hps>::edges_size_type num_edges(hps const &g) {
    size_t num = 0; // counting the number of downstream connections

    for (auto const &c : g.reservoirs)
      num += c->downstreams.size();

    for (auto const &c : g.units)
      num += c->downstreams.size();

    for (auto const &c : g.waterways)
      num += c->downstreams.size();

    return 2 * num; // bidirectional graph
  }

  inline std::pair<graph_traits<hps>::edge_iterator, graph_traits<hps>::edge_iterator> edges(hps const &g) {
    using Iter = graph_traits<hps>::edge_iterator;

    // find first vertex that has at least one edge
    for (auto [vi, vertices_end] = vertices(g); vi != vertices_end; ++vi) {
      if ((*vi)->downstreams.size() || (*vi)->upstreams.size()) {
        auto conn = (*vi)->downstreams.size() ? (*vi)->downstreams[0] : (*vi)->upstreams[0];
        return std::make_pair(Iter(&conn, *vi, &g), Iter(nullptr, nullptr, &g));
      }
    }
    throw std::runtime_error("Unable to create hps_edge_iterators because hps to not have any connections.");
  }

  /*
   * Functions for hps to satisfy IncidenceGraph concept
   */
  inline graph_traits<hps>::vertex_descriptor source(graph_traits<hps>::edge_descriptor const &e, hps const &) {
    return e.first;
  }

  inline graph_traits<hps>::vertex_descriptor target(graph_traits<hps>::edge_descriptor const &e, hps const &) {
    return e.second.target;
  }

  inline graph_traits<hps>::degree_size_type out_degree(graph_traits<hps>::vertex_descriptor const &u, hps const &) {
    return u->downstreams.size() + u->upstreams.size();
  }

  /*
   * Function for hps to satisfy BidirectionalGraph concept
   */
  inline std::pair<graph_traits<hps>::in_edge_iterator, graph_traits<hps>::in_edge_iterator>
    in_edges(graph_traits<hps>::vertex_descriptor const &u, hps const &g) {
    return out_edges(u, g);
  }

  inline graph_traits<hps>::degree_size_type in_degree(graph_traits<hps>::vertex_descriptor const &u, hps const &g) {
    return out_degree(u, g);
  }

  inline graph_traits<hps>::degree_size_type degree(graph_traits<hps>::vertex_descriptor const &u, hps const &g) {
    return out_degree(u, g);
  }
}

namespace shyft::energy_market::graph {
  // this namespace contains all the overloaded functions
  using graph = shyft::energy_market::hydro_power::hydro_power_system;
  using traits = boost::graph_traits<shyft::energy_market::hydro_power::hydro_power_system>;

  // for VertexListGraph
  using boost::num_vertices;
  using boost::vertices;

  // for EdgeListGraph
  using boost::num_edges;
  using boost::edges;

  // for IncidenceGraph
  using boost::source;
  using boost::target;
  using boost::out_degree;
  using boost::out_edges;

  // for BidirectionalGraph
  using boost::in_edges;
  using boost::in_degree;
  using boost::degree;
}
