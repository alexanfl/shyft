#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include <boost/hana.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/time_series/common.h>

// #include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
  using shyft::nan;

  struct stm_system; // fwd

  struct optimization_summary : public id_base {
    using super = id_base;
    stm_system* mdl{nullptr}; ///< _not_ owned ref to the owning model that is _required_ to outlive the scope of the
                              ///< run_parameters,  The pointer should be const, but the model can be modified.

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    optimization_summary();
    optimization_summary& operator=(optimization_summary const & o); // need to take care of this
    bool operator==(optimization_summary const & other) const;

    bool operator!=(optimization_summary const & other) const {
      return !(*this == other);
    }

    struct reservoir_ {
      reservoir_()
        : end_value{nan}
        , sum_ramping_penalty{nan}
        , sum_limit_penalty{nan}
        , end_limit_penalty{nan}
        , hard_limit_penalty{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        reservoir_,
        (double, end_value),
        (double, sum_ramping_penalty),
        (double, sum_limit_penalty),
        (double, end_limit_penalty),
        (double, hard_limit_penalty));

      SHYFT_DEFINE_STRUCT(
        reservoir_,
        (),
        (end_value, sum_ramping_penalty, sum_limit_penalty, end_limit_penalty, hard_limit_penalty));
    };

    struct gate_ {
      gate_()
        : ramping_penalty{nan}
        , discharge_cost{nan}
        , discharge_constraint_penalty{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        gate_,
        (double, ramping_penalty),
        (double, discharge_cost),
        (double, discharge_constraint_penalty));

      SHYFT_DEFINE_STRUCT(gate_, (), (ramping_penalty, discharge_cost, discharge_constraint_penalty));
    };

    struct waterway_ {
      waterway_()
        : vow_in_transit{nan}
        , sum_discharge_fee{nan}
        , discharge_group_penalty{nan}
        , discharge_group_ramping_penalty{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        waterway_,
        (double, vow_in_transit),
        (double, sum_discharge_fee),
        (double, discharge_group_penalty),
        (double, discharge_group_ramping_penalty));

      SHYFT_DEFINE_STRUCT(
        waterway_,
        (),
        (vow_in_transit, sum_discharge_fee, discharge_group_penalty, discharge_group_ramping_penalty));
    };

    struct spill_ {
      spill_()
        : cost{nan}
        , physical_cost{nan}
        , nonphysical_cost{nan}
        , physical_volume{nan}
        , nonphysical_volume{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        spill_,
        (double, cost),
        (double, physical_cost),
        (double, nonphysical_cost),
        (double, physical_volume),
        (double, nonphysical_volume));

      SHYFT_DEFINE_STRUCT(spill_, (), (cost, physical_cost, nonphysical_cost, physical_volume, nonphysical_volume));
    };

    struct bypass_ {
      bypass_()
        : cost{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(bypass_, (double, cost));

      SHYFT_DEFINE_STRUCT(bypass_, (), (cost));
    };

    struct ramping_ {
      ramping_()
        : ramping_penalty{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(ramping_, (double, ramping_penalty));

      SHYFT_DEFINE_STRUCT(ramping_, (), (ramping_penalty));
    };

    struct reserve_ {
      reserve_()
        : violation_penalty{nan}
        , sale_buy{nan}
        , obligation_value{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(reserve_, (double, violation_penalty), (double, sale_buy), (double, obligation_value));

      SHYFT_DEFINE_STRUCT(reserve_, (), (violation_penalty, sale_buy, obligation_value));
    };

    struct unit_ {
      unit_()
        : startup_cost{nan}
        , schedule_penalty{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(unit_, (double, startup_cost), (double, schedule_penalty));

      SHYFT_DEFINE_STRUCT(unit_, (), (startup_cost, schedule_penalty));
    };

    struct plant_ {
      plant_()
        : production_constraint_penalty{nan}
        , discharge_constraint_penalty{nan}
        , schedule_penalty{nan}
        , ramping_penalty{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        plant_,
        (double, production_constraint_penalty),
        (double, discharge_constraint_penalty),
        (double, schedule_penalty),
        (double, ramping_penalty));

      SHYFT_DEFINE_STRUCT(
        plant_,
        (),
        (production_constraint_penalty, discharge_constraint_penalty, schedule_penalty, ramping_penalty));
    };

    struct market_ {
      market_()
        : sum_sale_buy{nan}
        , load_penalty{nan}
        , load_value{nan} {
      }

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(market_, (double, sum_sale_buy), (double, load_penalty), (double, load_value));

      SHYFT_DEFINE_STRUCT(market_, (), (sum_sale_buy, load_penalty, load_value));
    };

    BOOST_HANA_DEFINE_STRUCT(
      optimization_summary,
      (reservoir_, reservoir),
      (waterway_, waterway),
      (gate_, gate),
      (spill_, spill),
      (bypass_, bypass),
      (ramping_, ramping),
      (reserve_, reserve),
      (unit_, unit),
      (plant_, plant),
      (market_, market),
      (double, total),
      (double, sum_penalties),
      (double, minor_penalties),
      (double, major_penalties),
      (double, grand_total));

    SHYFT_DEFINE_STRUCT(
      optimization_summary,
      (),
      (reservoir,
       waterway,
       gate,
       spill,
       bypass,
       ramping,
       reserve,
       unit,
       plant,
       market,
       total,
       sum_penalties,
       minor_penalties,
       major_penalties,
       grand_total));

    /** @return all urls of form dstm://M123/O.total, i.e prefix/path, where prefix is like dstm://M123
     */
    std::vector<std::string> all_urls(std::string const & prefix) const;
    x_serialize_decl();
  };

  using optimization_summary_ = std::shared_ptr<optimization_summary>;
}

x_serialize_export_key(shyft::energy_market::stm::optimization_summary);
BOOST_CLASS_VERSION(shyft::energy_market::stm::optimization_summary, 1);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::optimization_summary, Char> {
  FMT_CONSTEXPR auto parse(auto& ctx) {
    return ctx.begin();
  }

  auto format(shyft::energy_market::stm::optimization_summary const &, auto& ctx) const {
    auto out = ctx.out();
    return fmt::detail::write<Char>(out, "optimization_summary");
  }
};

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::optimization_summary>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::optimization_summary, Char> { };
