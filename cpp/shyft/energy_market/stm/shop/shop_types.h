/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
/*
 * Adapting stm types to shop api.
 */
#pragma once

#include <shyft/energy_market/stm/shop/api/shop_api.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm::shop {

  using hydro_power::xy_point_curve_with_z;
  using shyft::time_series::dd::apoint_ts;

  /* NOTE: The following type-defs are vital:
   * They dress up the templated types in the shop-api with concrete stm::side
   * basic-types. The shop_api = shop::api<xyz,ts> deals with all shop interface
   * comms. The specific shop-types are _proxies_ for the in-shop-engine objects,
   * identified by object-type and object-id. As we map objects from the stm model
   * to the shop model, we collect the returned 'shop-object-proxies', so that we
   * can keep a map between the stm-object(s) and the corresponding shop-objects.
   */
  using shop_api = ::shop::api<xy_point_curve_with_z, apoint_ts>;
  template <int T>
  using shop_object = ::shop::proxy::obj<shop_api, T>;
  using shop_reservoir = ::shop::reservoir<shop_api>;
  using shop_power_plant = ::shop::power_plant<shop_api>;
  using shop_unit = ::shop::unit<shop_api>;
  using shop_needle_combination = ::shop::needle_combination<shop_api>;
  using shop_pump = ::shop::pump<shop_api>;
  using shop_gate = ::shop::gate<shop_api>;
  using shop_tunnel = ::shop::tunnel<shop_api>;
  using shop_river = ::shop::river<shop_api>;
  using shop_contract = ::shop::contract<shop_api>;
  using shop_network = ::shop::network<shop_api>;
  using shop_market = ::shop::market<shop_api>;
  using shop_global_settings = ::shop::global_settings<shop_api>;
  using shop_reserve_group = ::shop::reserve_group<shop_api>;
  using shop_commit_group = ::shop::commit_group<shop_api>;
  using shop_discharge_group = ::shop::discharge_group<shop_api>;
  using shop_scenario = ::shop::scenario<shop_api>;
  using shop_objective = ::shop::objective<shop_api>;
  using shop_bid_group = ::shop::bid_group<shop_api>;
  using shop_cut_group = ::shop::cut_group<shop_api>;
  using shop_inflow_series = ::shop::inflow_series<shop_api>;
  using shop_lp_model = ::shop::lp_model<shop_api>;

}
