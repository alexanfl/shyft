#pragma once
#include <vector>
#include <memory>
#include <shyft/core/subscription.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/api/shop_proxy.h>

namespace shyft::energy_market::stm::shop {

  enum class run_type {
    incremental,
    full
  };

  /** @brief Visitor class to update stm_system or subscriptions
   * at certain points of execution of shop.
   */
  class shop_visitor {

    std::shared_ptr<stm_system> model;
    std::shared_ptr<model_info> info;

    std::string prefix; /// <- Prefix used to get attribute <--> url mapping correct.
    std::vector<std::string>
      subs; /// <- A container with what observers should be updated. This must be done by a call to notify_changes.
    run_type rt = run_type::full; // Defaults to full

    template <class PA>
    void add_subscription(std::string a_name, PA& pa) {
      subs.push_back(proxy_attr(model->run_params, a_name, pa).url(prefix));
    }

    // +------------------------------------------+
    // | Handling of different types of commands  |
    // +------------------------------------------+
    void set_code(std::string const & option) {
      if (option == "full")
        rt = run_type::full;
      else if (option == "incremental")
        rt = run_type::incremental;
      else if (option == "head") {
        model->run_params.head_opt = true;
        add_subscription("head_opt", model->run_params.head_opt);
      }
    }

    /** @brief increase the n_<run_type>_runs attribute of references stm_system
     * based on what run type is currently active.
     * Observable to notify change of is also updated.
     *
     * @param n: Number to increase by.
     */
    void increase_runs(int n) {
      switch (rt) {
      case run_type::full:
        model->run_params.n_full_runs = model->run_params.n_full_runs /*.value_or(0)*/ + n;
        add_subscription("n_full_runs", model->run_params.n_full_runs);
        return;
      case run_type::incremental:
        model->run_params.n_inc_runs = model->run_params.n_inc_runs /*.value_or(0)*/ + n;
        add_subscription("n_inc_runs", model->run_params.n_inc_runs);
        return;
      }
    }

   public:
    shop_visitor(std::shared_ptr<stm_system> model_, std::shared_ptr<model_info> info_, std::string const & prefix)
      : model(model_)
      , info(info_)
      , prefix{prefix} {
      // We assume the visitor is created before running SHOP optimization.
      // We therefore initialize all attributes:
      model->run_params.n_inc_runs = 0;
      model->run_params.n_full_runs = 0;
      model->run_params.head_opt = false;
    }

    /** @brief Function to call when updating the run_time_axis of the model.
     * Adds url of attribute to vector of observables that need to be updated.
     *
     * @param ta: mdl.run_time_axis will be set to this value.
     */
    void update_run_time_axis(time_axis::generic_dt const & ta) {
      model->run_params.run_time_axis = ta;
      add_subscription("run_time_axis", model->run_params.run_time_axis);
    }

    /** @brief Extend the context's message list with new messages.
     *
     * @returns: true if new messages were added, false if input messages was empty
     */
    bool add_shop_log(std::vector<log_entry> const & entries) {
      if (entries.size()) {
        info->add_shop_log(entries);
        // TODO: With new messages added, we should notify change on this log.
        return true;
      }
      return false;
    }

    /** @brief Update referenced stm_system based on a command (that has been, or will be executed.
     */
    void update_by_command(shop_command const & cmd) {
      if (cmd.keyword == "set" && cmd.specifier == "code") {
        if (cmd.options.size() > 0)
          set_code(cmd.options[0]);
      } else if (cmd.keyword == "start" && cmd.specifier == "sim") {
        if (cmd.objects.size() > 0)
          increase_runs(std::stoi(cmd.objects[0]));
      }
    }

    void notify_changes(shyft::core::subscription::manager& sm) const {
      sm.notify_change(subs);
    }
  };

  using shop_visitor_ = std::shared_ptr<shop_visitor>;
}
