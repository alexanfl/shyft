#include <cstdint>
#include <cstdlib>
#include <vector>

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/shop/shop_command.h>

namespace shyft::energy_market::stm::shop {

  shyft::core::utctime calc_suggested_timelimit(std::vector<shop_command> const & cmds) {
    // Helpers to compare shop command types
    auto eq_cmd_type = [](shop_command const & a, shop_command const & b) {
      return a.keyword == b.keyword && a.specifier == b.specifier;
    };
    auto is_timelimit_cmd = [&eq_cmd_type](shop_command const & cmd) {
      return eq_cmd_type(cmd, shop_command::set_timelimit(0));
    };
    auto is_start_cmd = [&eq_cmd_type](shop_command const & cmd) {
      return eq_cmd_type(cmd, shop_command::start_sim(0)) || eq_cmd_type(cmd, shop_command::start_shopsim());
    };

    // Find last timelimit between start commands, and add to total timelimit
    std::int64_t total_timelimit{0};
    std::int64_t last_timelimit{0};
    for (auto const & cmd : cmds) {
      if (is_timelimit_cmd(cmd))
        last_timelimit = std::stoi(cmd.objects.at(0)); // Assume shop command for timelimit is constructed correctly
      else if (is_start_cmd(cmd)) {
        int n_iterations = cmd.objects.size() ? std::atoi(cmd.objects[0].c_str()) : 0;
        if (n_iterations == 0)
          n_iterations = 1;
        if (last_timelimit != 0) {
          total_timelimit += last_timelimit * n_iterations;
          last_timelimit = 0;
        } else {
          total_timelimit = 0;
          break;
        }
      }
    }

    if (total_timelimit == 0)
      total_timelimit = 1 * 3600;
    else {
      // shop needs to finish ongoing iteration before it can exit on timeout.
      // add a margin to give shop a chance to finish on timelimit, before 'hard' timeout
      auto const multiplier = 1.5;
      total_timelimit *= multiplier;
    }
    return shyft::core::from_seconds(total_timelimit);
  }

}
