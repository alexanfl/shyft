#include <shyft/energy_market/stm/shop/shop_adapter.h>

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/time_series/dd/inside_ts.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>
#include <shyft/energy_market/stm/shop/shop_string_converter.h>

namespace shyft::energy_market::stm::shop {

  namespace {

    inline auto set_pump_ts(apoint_ts& ts, auto const & shop_pump_attr) {
      // Set value of stm time series as a combination of shop generation/production/discharge
      // represented with positive values and consumption/pumping/upflow represented with negative values.
      // Here we only set the contribution from the shop pump representation.
      if (!ts)
        ts = -shop_pump_attr.get();
      else
        ts = ts - shop_pump_attr.get();
    }

    inline auto set_unit_ts(apoint_ts& ts, auto const & shop_gen_attr, auto const & shop_pump_attr) {
      // Set value of stm time series as a combination of shop generation/production/discharge
      // represented with positive values and consumption/pumping/upflow represented with negative values.
      // Here we only set the contribution from the shop pump representation.
      if (shop_gen_attr.exists())
        ts = shop_gen_attr.get();
      if (shop_pump_attr.exists())
        set_pump_ts(ts, shop_pump_attr);
    }


  }

  shop_reserve_group shop_adapter::to_shop(unit_group const & a, apoint_ts const & mask_ts, string const & name) const {
    auto b = api.create<shop_reserve_group>(name);
    // a.group_type == would be fcr_n|d, affr,mfrr, frr, rr
    // and that selects what attribute to map the
    // a.constraint(.limit,.flag,.cost , result .penalty)
    // into the shop_reserve_group property.'
    // TODO: consider find common  group/properties, and collapse common reserve group into one group.
    //       it is unclear if this will change the optimisation rules.
    auto create_group_ts = [&mask_ts](apoint_ts const & ts) {
      if (!ts)
        return ts;
      shyft::time_series::dd::qac_parameter nan_to_0;
      nan_to_0.constant_filler = 0.0;
      nan_to_0.max_timespan = utctime(0);
      // resample to mask_ts time axis, set 0 out when nan/uspecified, then apply mask (set 0 when mask
      // 0)
      return ts.use_time_axis_from(mask_ts).quality_and_self_correction(nan_to_0) * mask_ts;
    };
    switch (a.group_type) {
    case unit_group_type::fcr_n_up:
      set_optional(b.fcr_n_up_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.fcr_n_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::fcr_n_down:
      set_optional(b.fcr_n_down_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.fcr_n_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::fcr_d_up:
      set_optional(b.fcr_d_up_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.fcr_d_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::fcr_d_down:
      set_optional(b.fcr_d_down_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.fcr_d_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::afrr_up:
      set_optional(b.frr_up_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.frr_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::afrr_down:
      set_optional(b.frr_down_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.frr_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::mfrr_up:
      set_optional(b.rr_up_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.rr_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    case unit_group_type::mfrr_down:
      set_optional(b.rr_down_obligation, create_group_ts(a.obligation.schedule));
      set_optional(b.rr_penalty_cost, create_group_ts(a.obligation.cost));
      break;
    default:
      break;
    }
    return b;
  }

  shop_market shop_adapter::to_shop(energy_market_area const & a) const {
    auto b = api.create<shop_market>(a.name);
    auto ug = a.get_unit_group();

    auto is_energy_market = !ug || ug->group_type == unit_group_type::production;
    // do not set prod_area for reserve markets
    // no ug means all units
    if (is_energy_market) {
      b.prod_area = ema_prod_area_id++;
    }
    if (valid(a.price)) {
      set(b.sale_price, a.price);
      set(
        b.buy_price,
        ((apoint_ts) a.price) + 0.0000001); // TODO: Currently same as sale-price with fixed small addition (0.1 NOK/MWH
                                            // == 0,0000001 NOK/WH), should add as dedicated stm attribute?
    }
    set_optional(b.max_sale, a.max_sale);
    set_optional(b.max_buy, a.max_buy);
    set_optional(b.load, a.load);
    if (
      is_energy_market
      && (exists(a.demand.bids) || exists(a.supply.bids))) { // is there a price/delivery descriptive data?
      auto sale_pd = convert_to_price_delivery_tsv(a.demand.bids, time_axis.total_period().end);
      auto buy_pd = convert_to_price_delivery_tsv(a.supply.bids, time_axis.total_period().end);
      size_t n_sale = sale_pd.size() ? sale_pd[0].size() : 0;
      size_t n_buy = buy_pd.size() ? buy_pd[0].size() : 0;
      size_t n_mkts = n_buy > n_sale ? n_buy : n_sale;
      vector<shop_market> sm; // submarkets

      apoint_ts zeros{time_axis, 0.0, POINT_AVERAGE_VALUE};
      for (size_t i = 0; i < n_mkts; ++i) {
        auto m = api.create<shop_market>(fmt::format("{}m{}", a.name, i));
        sm.push_back(m);
        m.prod_area = b.prod_area; // same prod-area
        if (i < n_sale) {
          set(m.sale_price, sale_pd[0][i]); //.average(time_axis).evaluate());
          set(m.max_sale, sale_pd[1][i]);   //.average(time_axis).evaluate());
        } else {
          set(m.max_sale, zeros);
          set(m.sale_price, zeros); // hmm.
        }
        if (i < n_buy) {
          set(m.buy_price, buy_pd[0][i]); //.average(time_axis).evaluate());
          set(m.max_buy, buy_pd[1][i]);   //.average(time_axis).evaluate());
        } else {
          set(m.max_buy, zeros);
          // set(m.sale_price,zeros);//hmm.
        }
      }
      ema_map[b.prod_area] = sm; // keep it so we can sum results in from_shop.
    }
    if (ug) {
      set_optional(b.market_type, get_shop_market_type(ug->group_type));
    }
    return b;
  }

  shop_contract shop_adapter::to_shop(contract const & a) const {
    auto b = api.create<shop_contract>(a.name);
    set_optional(b.trade_curve, a.options);
    set_optional(b.min_trade, a.constraint.min_trade);
    set_optional(b.max_trade, a.constraint.max_trade);
    set_optional(b.ramping_up, a.constraint.ramping_up);
    set_optional(b.ramping_down, a.constraint.ramping_down);
    set_optional(b.ramping_up_penalty_cost, a.constraint.ramping_up_penalty_cost);
    set_optional(b.ramping_down_penalty_cost, a.constraint.ramping_down_penalty_cost);
    return b;
  }

  xy_point_curve_ shop_adapter::get_spill_description(reservoir const & a) const {
    waterway_ wtr = nullptr;
    for (auto& conn : a.downstreams) {
      if (conn.role == connection_role::flood) {
        wtr = std::dynamic_pointer_cast<waterway>(conn.target);
        break;
      }
    }
    if (wtr) {
      if (wtr->gates.size() == 1) {
        auto gt = std::dynamic_pointer_cast<gate>(wtr->gates[0]);
        if (valid(gt->flow_description)) {
          auto xyz_list = get(gt->flow_description); // Return std::shared_ptr<vector<xy_point_curve_with_z_list>>
          if (xyz_list && xyz_list->size()) {
            return std::make_shared<xy_point_curve>((*xyz_list)[0].xy_curve);
          }
        }
      } else {
        // Error handling if not the correct number of gates
        return nullptr;
      }
    } else {
      // Error handling if no spill waterway was found
      return nullptr;
    }
    return nullptr;
  }

  shop_reservoir shop_adapter::to_shop(reservoir const & a) const {
    auto b = api.create<shop_reservoir>(a.name);
    double default_lrl = 0., default_hrl = 0., default_max_vol = 0.;
    shared_ptr<xy_point_curve> vmap;
    auto valid_vmap = [&vmap]() {
      return vmap && vmap->points.size() > 1;
    };
    if (valid(a.volume_level_mapping)) { // Exists and valid at period.start
      vmap = get(a.volume_level_mapping);
      if (valid_vmap()) { // require 2 or more points to consider valid
        default_lrl = vmap->points.front().y;
        default_hrl = vmap->points.back().y; // if spill description this default value will be replaced
        default_max_vol = vmap->points.back().x;
        set(b.vol_head, vmap);
      }
    }
    auto v = get_spill_description(a);
    if (v != nullptr) {
      default_hrl = v->points.front().x;
      if (valid_vmap())
        default_max_vol = vmap->calculate_x(default_hrl);
      set(b.flow_descr, v);
    }
    set_required(b.lrl, a.level.regulation_min, default_lrl);
    set_required(b.hrl, a.level.regulation_max, default_hrl);
    set_required(
      b.max_vol,
      a.volume.static_max,
      default_max_vol); // for some reason flooding does not work unless max_vol/ hrl is set
    if (exists(a.water_value.endpoint_desc))
      set_optional(b.energy_value_input, get(a.water_value.endpoint_desc, period().end - utctimespan(1)));
    set_optional(b.min_head_constr, a.level.constraint.min);
    set_optional(b.max_head_constr, a.level.constraint.max);
    set_optional(b.min_vol_constr, a.volume.constraint.min);
    set_optional(b.max_vol_constr, a.volume.constraint.max);

    set_optional(b.tactical_limit_min, a.volume.constraint.tactical.min.limit);
    set_optional(b.tactical_limit_min_flag, a.volume.constraint.tactical.min.flag);
    set_optional(b.tactical_cost_min, a.volume.constraint.tactical.min.cost);
    // set_optional(b.tactical_cost_min_flag, a.volume.constraint.tactical.min.cost.flag);

    set_optional(b.tactical_limit_max, a.volume.constraint.tactical.max.limit);
    set_optional(b.tactical_limit_max_flag, a.volume.constraint.tactical.max.flag);
    set_optional(b.tactical_cost_max, a.volume.constraint.tactical.max.cost);
    // set_optional(b.tactical_cost_max_flag, a.volume.constraint.tactical.max.cost.flag);

    set_optional(b.level_ramping_up, a.ramping.level_up);
    set_optional(b.level_ramping_down, a.ramping.level_down);

    set_optional(b.lower_slack, a.volume.slack.lower);
    set_optional(b.upper_slack, a.volume.slack.upper);
    set_optional(b.flood_volume_cost_curve, a.volume.cost.flood.curve);
    set_optional(b.peak_volume_cost_curve, a.volume.cost.peak.curve);

    // shop might seg if nan in inflow
    time_series::dd::apoint_ts run_ts(time_axis, 0.0, time_series::POINT_AVERAGE_VALUE);
    auto z_fill = [&run_ts](time_series::dd::apoint_ts const & x) {
      if (!x || x.total_period().contains(run_ts.total_period()))
        return x;
      time_series::dd::qac_parameter nan_to_0;
      nan_to_0.constant_filler = 0.0;
      nan_to_0.max_timespan = utctime(0);
      return x.use_time_axis_from(run_ts).quality_and_self_correction(nan_to_0);
    };
    // Require user to use either realised or schedule
    if (exists(a.inflow.schedule)) {
      set_optional(b.inflow, z_fill(a.inflow.schedule));
    } else {
      set_optional(b.inflow, z_fill(a.inflow.realised));
    }

    set_optional(b.volume_schedule, a.volume.schedule);
    set_optional(b.level_schedule, a.level.schedule);
    set_optional(b.start_head, a.level.realised); // Set start reservoir scalar value in Shop from value at start of
                                                  // period in historical time series in stm
    return b;
  }

  shop_unit shop_adapter::to_shop_generator_unit(unit const & a) const {
    auto b = api.create<shop_unit>(a.name);
    double default_pmin = 0., default_pmax = 0.;
    apoint_ts run_ts(time_axis, 0.0, shyft::time_series::POINT_AVERAGE_VALUE); // a ts for entire time_axis with zeros

    auto z_fill = [&run_ts](apoint_ts const & x) { // ensures zeros where nan/unspecified
      if (!x || x.total_period().contains(run_ts.total_period()))
        return x;                                  // if empty or covering the run time-axis, return as-is
      using shyft::time_series::dd::qac_parameter; // otherwise, resample(could be average..) and replace nan with z0.
      qac_parameter nan_to_0;
      nan_to_0.constant_filler = 0.0;
      nan_to_0.max_timespan = utctime(0);
      return x.use_time_axis_from(run_ts).quality_and_self_correction(nan_to_0);
    };

    auto nan_fill = [&run_ts](apoint_ts const & x) { // ensures zeros where nan/unspecified
      if (!x || x.total_period().contains(run_ts.total_period()))
        return x; // if empty or covering the run time-axis, return as-is
      // using shyft::time_series::dd::qac_parameter;//otherwise, resample(could be average..) and replace nan with z0.
      // qac_parameter nan_to_0;nan_to_0.constant_filler=0.0;nan_to_0.max_timespan=utctime(0);
      return x.use_time_axis_from(run_ts); //.quality_and_self_correction(nan_to_0);
    };

    // Generator efficiency
    if (valid(a.generator_description)) { // Exists and valid at period.start
      auto v = get(a.generator_description);
      if (v != nullptr) {
        decltype(v->points) gen_points;
        for (auto const & p : v->points) {
          if (p.x >= 0 && p.y >= 0) // p.x > 0 is ok, the y is efficiency, so we should trust that ?
            gen_points.push_back(p);
        }

        if (!std::empty(gen_points)) {
          auto gen_gen_desc = xy_point_curve{gen_points};
          set(b.gen_eff_curve, gen_gen_desc);
          default_pmin = gen_points.front().x;
          set_optional(b.p_min, default_pmin);
          default_pmax = gen_points.back().x;
          set_optional(b.p_max, default_pmax);
        }
      }
    }

    // Min/max/nom prod
    // Note: If generator efficiency was defined then default min/max from that are already set above.
    // TODO: Not used on aggregate level for pelton turbines when specified with needle combinations,
    // then it is part of the needle combination instead, but perhaps it does not hurt to always set them?
    set_optional(b.p_min, a.production.static_min);
    set_optional(b.p_max, a.production.static_max);
    b.p_nom = b.p_max; // Nominal production is often same as maximum, and we don't need it to be anything, but Shop
                       // uses it for calculating the
    set_optional(b.p_nom, a.production.nominal); // Maximal FCR bounds and historically used it for several other uses
                                                 // and therefore in general recommend it to always be set!

    // Turbine efficiency
    if (valid(a.turbine_description)) { // Exists and valid at period.start
      auto v = get(a.turbine_description);
      if (v != nullptr && v->operating_zones.size() > 0) {
        if (v->operating_zones.size() < 2 || enforce_single_operating_zone) {
          // Only one efficiency: Set it as total turbine efficiency
          // b.turb_eff_curves = v->operating_zones.front().efficiency_curves;
          std::vector<hydro_power::xy_point_curve_with_z> gen_efficiency_curves;

          auto& operating_zone =
            (v->operating_zones.size() == 1) ? v->operating_zones.front() : v->operating_zones.back();

          for (auto const & c : forward_efficiency_curves(operating_zone)) {
            if (c.z >= 0)
              gen_efficiency_curves.push_back(c);
          }
          set(b.turb_eff_curves, gen_efficiency_curves);
        } else {
          // More than one efficiency: Isolated operating zones or Pelton needle combination, emit as needle combination
          // objects.
          auto const mk_txy = [&](double v) -> apoint_ts {
            return apoint_ts(time_axis, v, shyft::time_series::POINT_AVERAGE_VALUE);
          };
          size_t i = 0;
          for (auto& nc : v->operating_zones) {
            auto b2 = api.create<shop_needle_combination>(fmt::format("{}{}", a.name, ++i));
            b2.p_min = nc.production_min;
            b2.p_max = nc.production_max;
            b2.p_nom = nc.production_nominal; // See unit production.nominal above
            b2.turb_eff_curves = nc.efficiency_curves;
            // emit fcr_max/min, assuming start posistion the same (later we might collect from t-mapped turbine
            // description)
            if (!std::isnan(nc.fcr_max))
              b2.p_fcr_max = mk_txy(nc.fcr_max);
            if (!std::isnan(nc.fcr_min))
              b2.p_fcr_min = mk_txy(nc.fcr_min);
            api.connect_objects(b, b.connection_standard, b2.id);
          }
        }
      }
    }

    // Other attributes
    set_optional(b.min_p_constr, a.production.constraint.min);
    set_optional(b.max_p_constr, a.production.constraint.max);
    set_optional(b.min_q_constr, a.discharge.constraint.min);
    set_optional(b.max_q_constr, a.discharge.constraint.max);
    set_optional(b.max_q_limit_rsv_down, a.discharge.constraint.max_from_downstream_level);
    set_optional(b.startcost, a.cost.start);
    set_optional(b.stopcost, a.cost.stop);
    set_optional(b.maintenance_flag, a.unavailability);
    set_optional(b.priority, a.priority);
    if (exists(a.discharge.schedule))
      set(b.discharge_schedule, upper_half(a.discharge.schedule));

    //--------------------------------------------------------
    // operational reserve attributes
    // notice that we currently do not emit the group membership time-series here
    // instead, we emit those along with the unit-group emitter.
    //    so first emit group-definitions with the requirement/constraint to be reached
    //       then  emit each unit member-ship time-series into the group for the respective
    //                  7 reserve types (fcr_n.up|down, fcr_d, frr.up|down, rr.up|down)
    auto const set_opt_w_flag =
      [this, nan_fill](
        auto&& shop_schedule,
        auto&& /*shop_flag*/,
        auto&& shop_min,
        auto&& shop_max,
        auto&& shop_cost,
        auto&& ts,
        auto&& ts_min,
        auto&& ts_max,
        auto&& ts_cost) {
        if (exists(ts)) {
          set_optional(shop_schedule, nan_fill(ts)); // currently we do not need to worry about the flag, just pass nans
          // After tip from Ole Andreas/Tellef 2021.03.09: try to leave the flag, just pass on nans
          // and it could work, even better.
          // auto flag=a.reserve.droop.schedule.inside(0.5,1000.0,0.0, 1.0,0.0);// just set limits low..high enough for
          // the practical usage. set_optional(shop_flag,z_fill(flag));
        }
        set_optional(shop_min, ts_min); // there was a comment in shop manual about schedule at the same time as
        set_optional(shop_max, ts_max); // maybe with flag?
        set_optional(shop_cost, ts_cost);
      };

    set_opt_w_flag(
      b.fixed_droop,
      b.fixed_droop_flag,
      b.droop_min,
      b.droop_max,
      b.droop_cost,
      a.reserve.droop.schedule,
      a.reserve.droop.min,
      a.reserve.droop.max,
      a.reserve.droop.cost);

    set_optional(b.fcr_n_droop_cost, a.reserve.droop.fcr_n.cost);
    set_optional(b.fcr_d_down_droop_cost, a.reserve.droop.fcr_d_down.cost);
    set_optional(b.fcr_d_up_droop_cost, a.reserve.droop.fcr_d_up.cost);

    auto set_droop_steps = [this](auto const & stm_a, auto& shop_a) {
      bool a_set = false;
      if (valid(stm_a)) {
        auto v = get(stm_a);
        if (v != nullptr && v->points.size() > 0) {
          std::vector<double> y;
          for (const auto& p : v->points) {
            y.push_back(p.y);
          }
          shop_a = y;
          a_set = true;
        }
      }
      return a_set;
    };

    set_droop_steps(a.reserve.droop_steps, b.discrete_droop_values);
    bool fcr_n = set_droop_steps(a.reserve.droop.fcr_n.steps, b.fcr_n_discrete_droop_values);
    bool fcr_d_up = set_droop_steps(a.reserve.droop.fcr_d_up.steps, b.fcr_d_up_discrete_droop_values);
    bool fcr_d_down = set_droop_steps(a.reserve.droop.fcr_d_down.steps, b.fcr_d_down_discrete_droop_values);
    if (fcr_n && (fcr_d_up || fcr_d_down)) {
      b.separate_droop = 1;
    }

    set_opt_w_flag(
      b.fcr_n_up_schedule,
      b.fcr_n_up_schedule_flag,
      b.fcr_n_up_min,
      b.fcr_n_up_max,
      b.fcr_n_up_cost,
      a.reserve.fcr_n.up.schedule,
      a.reserve.fcr_n.up.min,
      a.reserve.fcr_n.up.max,
      a.reserve.fcr_n.up.cost);
    set_opt_w_flag(
      b.fcr_n_down_schedule,
      b.fcr_n_down_schedule_flag,
      b.fcr_n_down_min,
      b.fcr_n_down_max,
      b.fcr_n_down_cost,
      a.reserve.fcr_n.down.schedule,
      a.reserve.fcr_n.down.min,
      a.reserve.fcr_n.down.max,
      a.reserve.fcr_n.down.cost);

    set_opt_w_flag(
      b.fcr_d_up_schedule,
      b.fcr_d_up_schedule_flag,
      b.fcr_d_up_min,
      b.fcr_d_up_max,
      b.fcr_d_up_cost,
      a.reserve.fcr_d.up.schedule,
      a.reserve.fcr_d.up.min,
      a.reserve.fcr_d.up.max,
      a.reserve.fcr_d.up.cost);
    set_opt_w_flag(
      b.fcr_d_down_schedule,
      b.fcr_d_down_schedule_flag,
      b.fcr_d_down_min,
      b.fcr_d_down_max,
      b.fcr_d_down_cost,
      a.reserve.fcr_d.down.schedule,
      a.reserve.fcr_d.down.min,
      a.reserve.fcr_d.down.max,
      a.reserve.fcr_d.down.cost);

    set_opt_w_flag(
      b.frr_up_schedule,
      b.frr_up_schedule_flag,
      b.frr_up_min,
      b.frr_up_max,
      b.frr_up_cost,
      a.reserve.afrr.up.schedule,
      a.reserve.afrr.up.min,
      a.reserve.afrr.up.max,
      a.reserve.afrr.up.cost);
    set_opt_w_flag(
      b.frr_down_schedule,
      b.frr_down_schedule_flag,
      b.frr_down_min,
      b.frr_down_max,
      b.frr_down_cost,
      a.reserve.afrr.down.schedule,
      a.reserve.afrr.down.min,
      a.reserve.afrr.down.max,
      a.reserve.afrr.down.cost);

    set_opt_w_flag(
      b.rr_up_schedule,
      b.rr_up_schedule_flag,
      b.rr_up_min,
      b.rr_up_max,
      b.rr_up_cost,
      a.reserve.mfrr.up.schedule,
      a.reserve.mfrr.up.min,
      a.reserve.mfrr.up.max,
      a.reserve.mfrr.up.cost);
    set_opt_w_flag(
      b.rr_down_schedule,
      b.rr_down_schedule_flag,
      b.rr_down_min,
      b.rr_down_max,
      b.rr_down_cost,
      a.reserve.mfrr.down.schedule,
      a.reserve.mfrr.down.min,
      a.reserve.mfrr.down.max,
      a.reserve.mfrr.down.cost);

    set_optional(b.p_fcr_min, z_fill(a.reserve.fcr_static_min));
    set_optional(b.p_fcr_max, z_fill(a.reserve.fcr_static_max));

    set_optional(b.fcr_mip_flag, a.reserve.fcr_mip);
    set_optional(b.p_rr_min, a.reserve.mfrr_static_min);


    // This can be controlled with the commited_flag
    // Case:
    // If commited_in is set to true (1), in the same interval as a schedule,
    // schedule should prevail over commited_in

    // After tip from Ole Andreas/Tellef 2021.03.09: try to leave the flag, just pass on nans
    // and it could work, even better.

    // production_schedule serves two roles in SHOP:
    //  - input to plan optimisation - planned production
    //  - input to inflow calculation - actual production

    // Require user to use either realised or schedule
    if (exists(a.production.schedule) || exists(a.production.commitment)) {
      if (exists(a.production.schedule)) {
        set(b.production_schedule, upper_half(a.production.schedule));
        set(b.ref_production, upper_half(a.production.schedule));
      }
      set_optional(b.committed_in, a.production.commitment);
    } else {
      set_optional(b.production_schedule, a.production.realised);
      set_optional(b.ref_production, a.production.realised);
    }
    return b;
  }

  shop_pump shop_adapter::to_shop_pump_unit(unit const & a) const {
    auto b = api.create<shop_pump>(a.name);

    auto turbine_description = [&]() -> hydro_power::turbine_description const * {
      if (!valid(a.turbine_description))
        return nullptr;
      auto turbine_description = get(a.turbine_description);
      if (!turbine_description)
        return nullptr;
      if (std::ranges::size(turbine_description->operating_zones) != 1)
        return nullptr;
      if (turbine_description->operating_zones[0].efficiency_curves.empty())
        return nullptr;
      return turbine_description.get();
    }();
    if (!turbine_description || turbine_description->operating_zones[0].efficiency_curves.empty())
      throw std::runtime_error("invalid pump turbine description");

    auto& turbine_operating_zone = turbine_description->operating_zones[0];

    // Turbine efficiency
    // Note: Setting as regular turbine_description attribute, instead of obsolete pump_description attribute.
    auto to_shop_turbine_efficiency_curve = [&](xy_point_curve_with_z c) {
      c.z *= -1;
      for (auto& p : c.xy_curve.points)
        p.x *= -1;
      std::ranges::reverse(c.xy_curve.points);
      return c;
    };
    auto shop_turbine_description = [&] {
      std::vector<xy_point_curve_with_z> c;
      std::ranges::copy(
        std::views::transform(backward_efficiency_curves(turbine_operating_zone), to_shop_turbine_efficiency_curve),
        std::back_inserter(c));
      std::ranges::reverse(c); // NOTE: assume curve is sorted increasing wrt. to z - jeh
      return c;
    }();
    if (shop_turbine_description.empty())
      throw std::runtime_error("invalid pump operating zone");
    set(b.turb_eff_curves, shop_turbine_description);

    auto shop_pump_efficiency_curve = [&]() -> std::optional<xy_point_curve> {
      if (!valid(a.generator_description))
        return std::nullopt;
      auto generator_description = get(a.generator_description);
      if (!generator_description)
        return std::nullopt;
      xy_point_curve shop_pump_efficiency_curve;

      std::ranges::copy(
        std::views::transform(
          std::views::filter(
            generator_description->points,
            [](const auto& p) {
              return p.x < 0;
            }),
          [](const auto& p) {
            return hydro_power::point{.x = -p.x, .y = p.y};
          }),
        std::back_inserter(shop_pump_efficiency_curve.points));

      if (shop_pump_efficiency_curve.points.empty())
        return std::nullopt;
      std::ranges::reverse(shop_pump_efficiency_curve.points);
      return shop_pump_efficiency_curve;
    }();

    if (!shop_pump_efficiency_curve)
      // NOTE:
      //   set up synthetic efficiency curve for pump.
      //   required by SHOP. range doesn't matter.
      //   - jeh
      shop_pump_efficiency_curve = xy_point_curve{
        .points{{.x = 0.0e9, .y = 100.0}, {.x = 1.0e9, .y = 100.0}}
      };

    set(b.gen_eff_curve, *shop_pump_efficiency_curve);

    // Note: Currently stm are not using production.static_min/static_max/nominal for pumps, but if
    // generator efficiency is defined then min/max/nom values deduced from that could be set above.
    // However, Shop does not require these to be set for binary pumps.

    // Other attributes
    set_optional(b.startcost, a.cost.pump_start);
    set_optional(b.stopcost, a.cost.pump_stop);
    set_optional(b.maintenance_flag, a.pump_unavailability);
    if (exists(a.discharge.schedule))
      set_optional(b.upflow_schedule, -lower_half(a.discharge.schedule));
    if (exists(a.production.schedule))
      set_optional(b.consumption_schedule, -lower_half(a.production.schedule));

    set_optional(b.committed_in, a.production.pump_commitment);
    set_optional(b.downstream_min, a.pump_constraint.min_downstream_level);

    return b;
  }

  shop_power_plant shop_adapter::to_shop(power_plant const & a) const {

    auto b = api.create<shop_power_plant>(a.name);
    set_optional(b.outlet_line, a.outlet_level);
    set_optional(b.mip_flag, a.mip);
    set_optional(b.block_merge_tolerance, a.production.merge_tolerance);
    set_optional(b.power_ramping_up, a.production.ramping_up);
    set_optional(b.power_ramping_down, a.production.ramping_down);
    set_optional(b.discharge_ramping_up, a.discharge.ramping_up);
    set_optional(b.discharge_ramping_down, a.discharge.ramping_down);
    set_optional(b.min_p_constr, a.production.constraint_min);
    set_optional(b.max_p_constr, a.production.constraint_max);
    set_optional(b.min_q_constr, a.discharge.constraint_min);
    set_optional(b.max_q_constr, a.discharge.constraint_max);
    if (exists(a.production.schedule)) {
      set_optional(b.production_schedule, upper_half(a.production.schedule));
      set_optional(b.consumption_schedule, -lower_half(a.production.schedule));
    }
    if (exists(a.discharge.schedule)) {
      set_optional(b.discharge_schedule, upper_half(a.discharge.schedule));
      set_optional(b.upflow_schedule, -lower_half(a.discharge.schedule));
    }
    set_optional(b.intake_loss_from_bypass_flag, a.discharge.intake_loss_from_bypass_flag);
    set_optional(b.maintenance_flag, a.unavailability);
    set_optional(b.bp_dyn_wv_flag, a.best_profit.dynamic_water_value);
    if (exists(a.fees.feeding_fee)) {
      apoint_ts run_ts(time_axis, 0.0, shyft::time_series::POINT_AVERAGE_VALUE); // a ts for entire time_axis with zeros
      auto flag = a.fees.feeding_fee.use_time_axis_from(run_ts).inside(
        std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max(), 0.0, 1.0, 0.0);
      set_optional(b.feeding_fee, a.fees.feeding_fee);
      set_optional(b.feeding_fee_flag, flag); // the flag series is 0 for all parts(run axis) where there is no fee
                                              // (nan), and 1.0 for all places with values
    }
    return b;
  }

  shop_tunnel shop_adapter::to_shop(waterway const & wtr) const {
    auto b = api.create<shop_tunnel>(wtr.name);
    // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
    set_optional(b.max_flow, wtr.discharge.static_max);
    auto loss_coeff = valid_temporal(wtr.head_loss_coeff) ? get_temporal(wtr.head_loss_coeff, 0.0) : 0.0;
    set_optional(b.loss_factor, loss_coeff);
    set_optional(b.start_height, wtr.geometry.z0);
    set_optional(b.end_height, wtr.geometry.z1);
    set_optional(b.diameter, wtr.geometry.diameter);
    set_optional(b.length, wtr.geometry.length);
    if (wtr.gates.size() == 1) {
      if (auto gt = std::dynamic_pointer_cast<gate>(wtr.gates[0])) {
        set_optional(b.gate_adjustment_cost, gt->cost);
        set_optional(b.gate_opening_curve, gt->opening.constraint.positions);
        auto continuous_gate_flag = valid_temporal(gt->opening.constraint.continuous)
                                    ? get_temporal(gt->opening.constraint.continuous, 0.0)
                                    : 0.0;
        set(b.continuous_gate, continuous_gate_flag >= 0.5 ? 1 : 0);
        set_optional(b.gate_opening_schedule, gt->opening.schedule);
        set_optional(b.initial_opening, gt->opening.realised); // Set initial opening value in Shop from value at start
                                                               // of period in historical time series in stm
        set_optional(b.min_flow, gt->discharge.constraint.min);
        set_optional(b.max_flow, gt->discharge.constraint.max);
      }
    }
    return b;
  }

  shop_river shop_adapter::to_shop_river(waterway const & wtr, gate const * /*gt*/) const {
    auto r = api.create<shop_river>(wtr.name);

    set_shop_time_delay(r, wtr.delay);
    set_optional(r.upstream_elevation, wtr.geometry.z0);
    set_optional(r.downstream_elevation, wtr.geometry.z1);
    if (exists(wtr.discharge.constraint.max))
      set(r.max_flow, wtr.discharge.constraint.max);
    else
      set_optional(r.max_flow, wtr.discharge.static_max);
    set_optional(r.min_flow, wtr.discharge.constraint.min);
    set_optional(r.flow_cost, wtr.discharge.penalty.cost.rate);
    set_optional(r.min_flow_penalty_cost, wtr.discharge.penalty.cost.constraint_min);
    set_optional(r.max_flow_penalty_cost, wtr.discharge.penalty.cost.constraint_max);
    set_optional(r.ramping_up_penalty_cost, wtr.discharge.penalty.cost.ramping_up);
    set_optional(r.ramping_down_penalty_cost, wtr.discharge.penalty.cost.ramping_down);
    set_optional(r.ramping_up, wtr.discharge.constraint.ramping_up);
    set_optional(r.ramping_down, wtr.discharge.constraint.ramping_down);

    // Gate-less flow description, convert to fully open gate description
    if (exists(wtr.flow_description.upstream_ref)) {
      auto fd = std::make_shared<t_xyz_list_::element_type>();
      for (auto p : *wtr.flow_description.upstream_ref) {
        fd->emplace(std::make_pair(p.first, std::make_shared<std::vector<xy_point_curve_with_z>>()));
        fd->at(p.first)->emplace_back();
        fd->at(p.first)->back().xy_curve.points = p.second->points;
        fd->at(p.first)->back().z = 1.0;
      };
      set_optional(r.up_head_flow_curve, fd);
    }
    set_optional(r.delta_head_ref_up_flow_curve, wtr.flow_description.delta_meter.upstream_ref);
    set_optional(r.delta_head_ref_down_flow_curve, wtr.flow_description.delta_meter.downstream_ref);


    if (bool needs_discharge_group =
          exists(wtr.discharge.reference) && exists(wtr.discharge.realised)
          && (exists(wtr.discharge.constraint.accumulated_max) || exists(wtr.discharge.constraint.accumulated_min));
        needs_discharge_group) {
      auto dg = api.create<shop_discharge_group>("dg_" + wtr.name);
      set_required(
        dg.initial_deviation_mm3,
        exists(wtr.deviation.realised)
          ? wtr.deviation.realised(time_axis.total_period().start)
          : initial_acc_deviation(time_axis.total_period().start, wtr.discharge.realised, wtr.discharge.reference));

      set_required(dg.weighted_discharge_m3s, wtr.discharge.reference);
      set_optional(dg.max_accumulated_deviation_mm3_up, wtr.discharge.constraint.accumulated_max);
      set_optional(dg.max_accumulated_deviation_mm3_down, wtr.discharge.constraint.accumulated_min);
      set_optional(dg.penalty_cost_down_per_mm3, wtr.discharge.penalty.cost.accumulated_min);
      set_optional(dg.penalty_cost_up_per_mm3, wtr.discharge.penalty.cost.accumulated_max);
      api.connect_objects(dg, dg.connection_standard, wtr.id);
    }

    return r;
  };

  shop_gate shop_adapter::to_shop_gate(waterway const & wtr, gate const * gt) const {
    auto b = api.create<shop_gate>(gt ? gt->name : wtr.name);
    // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
    set_optional(b.max_discharge, wtr.discharge.static_max);
    // set_optional(b.shape_discharge, wtr.delay); // time delay on the flow in the river bed and so comply to all the
    // parallel gates as a whole
    // time delay on the flow in the river bed and so comply to all the parallel gates as a whole
    set_shop_time_delay(b, wtr.delay);
    if (gt) {
      if (&wtr != std::dynamic_pointer_cast<waterway>(gt->wtr_()).get()) // Gate is assumed to be in given water route!
        throw std::runtime_error("Specified waterroute and gate are not connected");

      set_optional(b.max_discharge, gt->discharge.static_max);
      set_optional(b.min_flow, gt->discharge.constraint.min);
      set_optional(b.max_flow, gt->discharge.constraint.max);
      set_optional(
        b.functions_meter_m3s,
        gt->flow_description); // if flood gate, this is already set on the rsv, is it ok to emit?
      set_optional(b.functions_deltameter_m3s, gt->flow_description_delta_h);
      set_optional(b.block_merge_tolerance, gt->discharge.merge_tolerance);
      // Note that only one of 'schedule_percent' and 'schedule_m3s' can exist,
      // setting 'schedule_percent' will erase 'schedule_m3s' and vice versa.
      if (exists(gt->discharge.schedule) || exists(gt->opening.schedule)) {
        // assume plan is intended and set to planned values
        set_optional(b.schedule_percent, gt->opening.schedule);
        set_optional(b.schedule_m3s, gt->discharge.schedule); // Last to be set and taking precendence if available
      } else {
        // provide realised production data for inflow calculation, if available
        set_optional(b.schedule_percent, gt->opening.realised);
        set_optional(b.schedule_m3s, gt->discharge.realised); // Last to be set and taking precendence if available
      }
      set_optional(b.peak_flow_cost_curve, wtr.discharge.penalty.cost.peak_curve);
    }
    return b;
  }

  shop_discharge_group shop_adapter::to_shop_discharge_group(waterway const & wtr) {
    auto b = api.create<shop_discharge_group>("dg_" + wtr.name);
    if (
      exists(wtr.discharge.reference) && exists(wtr.discharge.realised)
      && (exists(wtr.discharge.constraint.accumulated_max) || exists(wtr.discharge.constraint.accumulated_min))) {
      set_required(b.weighted_discharge_m3s, wtr.discharge.reference);
      set_required(
        b.initial_deviation_mm3,
        exists(wtr.deviation.realised)
          ? wtr.deviation.realised(time_axis.total_period().start)
          : initial_acc_deviation(time_axis.total_period().start, wtr.discharge.realised, wtr.discharge.reference));
      set_optional(b.max_accumulated_deviation_mm3_up, wtr.discharge.constraint.accumulated_max);
      set_optional(b.max_accumulated_deviation_mm3_down, wtr.discharge.constraint.accumulated_min);
    }
    set_optional(b.min_discharge_m3s, wtr.discharge.constraint.min);
    set_optional(b.max_discharge_m3s, wtr.discharge.constraint.max);
    set_optional(b.ramping_up_m3s, wtr.discharge.constraint.ramping_up);
    set_optional(b.ramping_down_m3s, wtr.discharge.constraint.ramping_down);
    set_optional(b.min_discharge_penalty_cost, wtr.discharge.penalty.cost.constraint_min);
    set_optional(b.max_discharge_penalty_cost, wtr.discharge.penalty.cost.constraint_max);
    set_optional(b.ramping_up_penalty_cost, wtr.discharge.penalty.cost.ramping_up);
    set_optional(b.ramping_down_penalty_cost, wtr.discharge.penalty.cost.ramping_down);
    set_optional(b.penalty_cost_down_per_mm3, wtr.discharge.penalty.cost.accumulated_min);
    set_optional(b.penalty_cost_up_per_mm3, wtr.discharge.penalty.cost.accumulated_max);
    return b;
  }

  void shop_adapter::from_shop(energy_market_area& mkt, shop_market const & shop_mkt) {
    set_optional(mkt.buy, shop_mkt.buy);
    set_optional(mkt.sale, shop_mkt.sale);
    set_optional(mkt.sale, shop_mkt.sim_sale); // Alternative result attribute when running simulation
    set_optional(mkt.reserve_obligation_penalty, shop_mkt.reserve_obligation_penalty);

    if (ema_map.find(shop_mkt.prod_area) != ema_map.end()) {

      auto const add_contrib = [&](auto& ema_a, auto& shop_a) {
        if (shop_a.exists()) {
          if (ema_a.size() == 0) {
            ema_a = shop_a.get();
          } else {
            ema_a = ema_a + shop_a.get();
          }
        }
      };

      auto sms = ema_map[shop_mkt.prod_area];
      for (auto const & sm : sms) {
        add_contrib(mkt.sale, sm.sale);
        add_contrib(mkt.buy, sm.buy);
      }
      if (exists(mkt.buy)) {
        mkt.supply.usage.result = 1.0 * mkt.buy;
      }
      mkt.supply.price.result = effective_price(
        mkt.supply.usage.result, mkt.supply.bids, true); // Take cheapest supply offerings
      if (exists(mkt.sale)) {
        mkt.demand.usage.result = -1.0 * mkt.sale;
      }
      mkt.demand.price.result = effective_price(
        mkt.demand.usage.result, mkt.demand.bids, false); // Take best paid offers first
    }
  }

  void shop_adapter::from_shop(contract& ctr, shop_contract const & shop_ctr) {
    set_optional(ctr.quantity, shop_ctr.trade);
  }

  void shop_adapter::from_shop(reservoir& rsv, shop_reservoir const & shop_rsv) {
    set_optional(rsv.volume.result, shop_rsv.storage);
    set_optional(rsv.level.result, shop_rsv.head);
    set_optional(rsv.volume.penalty, shop_rsv.penalty);
    set_optional(rsv.volume.constraint.tactical.min.penalty, shop_rsv.tactical_penalty_down);
    set_optional(rsv.volume.constraint.tactical.max.penalty, shop_rsv.tactical_penalty_up);
    set_optional(rsv.volume.cost.flood.penalty, shop_rsv.flood_volume_penalty);
    set_optional(rsv.volume.cost.peak.penalty, shop_rsv.peak_volume_penalty);

    // Read from alternative result attributes when running inflow calculation mode
    set_optional(rsv.inflow.result, shop_rsv.sim_inflow);
    set_optional(rsv.volume.result, shop_rsv.sim_storage);
    set_optional(rsv.level.result, shop_rsv.sim_head);

    // Results from water value calculations
    set_optional(rsv.water_value.result.local_energy, shop_rsv.energy_value_local_result);
    set_optional(rsv.water_value.result.local_volume, shop_rsv.water_value_local_result);
    set_optional(rsv.water_value.result.global_volume, shop_rsv.water_value_global_result);
    set_optional(rsv.water_value.result.end_value, shop_rsv.end_value);
  }

  void shop_adapter::from_shop(power_plant& pp, shop_power_plant const & shop_ps) {

    set_optional(pp.gross_head, shop_ps.gross_head);

    // FIXME: use two separate calls for write optimization & simulation results - jeh
    // Read from alternative result attributes when running inflow calculation mode
    if (shop_ps.sim_production.exists() || shop_ps.sim_consumption.exists()) {
      set_unit_ts(pp.production.result, shop_ps.sim_production, shop_ps.sim_consumption);
      set_unit_ts(pp.discharge.result, shop_ps.sim_discharge, shop_ps.sim_upflow);
    } else {
      set_unit_ts(pp.production.result, shop_ps.production, shop_ps.consumption);
      set_unit_ts(pp.discharge.result, shop_ps.discharge, shop_ps.upflow);
    }

    // Results from best profit (BP) calculations
    set_optional(pp.best_profit.discharge, shop_ps.best_profit_q);
    set_optional(pp.best_profit.cost_average, shop_ps.best_profit_ac);
    set_optional(pp.best_profit.cost_marginal, shop_ps.best_profit_mc);
    set_optional(pp.best_profit.cost_commitment, shop_ps.best_profit_commitment_cost);
    set_optional(pp.production.instant_max, shop_ps.max_prod); // pickup instant max, 15.2.1.0
  }

  void shop_adapter::from_shop(unit& agg, shop_unit const & shop_agg) {
    set_optional(agg.discharge.result, shop_agg.discharge);
    set_optional(agg.production.result, shop_agg.production);

    // From operational reserve (frequency control)
    set_optional(agg.reserve.fcr_n.up.result, shop_agg.fcr_n_up_delivery);
    set_optional(agg.reserve.fcr_n.down.result, shop_agg.fcr_n_down_delivery);
    set_optional(agg.reserve.fcr_d.up.result, shop_agg.fcr_d_up_delivery);
    set_optional(agg.reserve.fcr_d.down.result, shop_agg.fcr_d_down_delivery);
    set_optional(agg.reserve.afrr.up.result, shop_agg.frr_up_delivery);
    set_optional(agg.reserve.afrr.down.result, shop_agg.frr_down_delivery);
    set_optional(agg.reserve.mfrr.up.result, shop_agg.rr_up_delivery);
    set_optional(agg.reserve.mfrr.down.result, shop_agg.rr_down_delivery);
    set_optional(agg.reserve.fcr_n.up.penalty, shop_agg.fcr_n_up_schedule_penalty);
    set_optional(agg.reserve.fcr_n.down.penalty, shop_agg.fcr_n_down_schedule_penalty);
    set_optional(agg.reserve.afrr.up.penalty, shop_agg.frr_up_schedule_penalty);
    set_optional(agg.reserve.afrr.down.penalty, shop_agg.frr_down_schedule_penalty);
    set_optional(agg.reserve.mfrr.up.penalty, shop_agg.rr_up_schedule_penalty);
    set_optional(agg.reserve.mfrr.down.penalty, shop_agg.rr_down_schedule_penalty);
    set_optional(agg.reserve.droop.result, shop_agg.droop_result);
    set_optional(agg.reserve.droop.fcr_n.result, shop_agg.fcr_n_droop_result);
    set_optional(agg.reserve.droop.fcr_d_down.result, shop_agg.fcr_d_down_droop_result);
    set_optional(agg.reserve.droop.fcr_d_up.result, shop_agg.fcr_d_up_droop_result);

    // Results from production-discharge (PQ) relation calculations
    set_optional(agg.production_discharge_relation.original, shop_agg.original_pq_curves);
    set_optional(agg.production_discharge_relation.convex, shop_agg.convex_pq_curves);
    set_optional(agg.production_discharge_relation.final, shop_agg.final_pq_curves);

    // Results from best profit (BP) calculations
    set_optional(agg.best_profit.discharge, shop_agg.best_profit_q);
    set_optional(agg.best_profit.production, shop_agg.best_profit_p);
    set_optional(agg.best_profit.discharge_production_ratio, shop_agg.best_profit_dq_dp);
    set_optional(agg.best_profit.operating_zone, shop_agg.best_profit_needle_comb);

    // Read from alternative result attributes when running inflow calculation mode
    set_optional(agg.production.result, shop_agg.sim_production); // ? usually a forcing variable..
    set_optional(agg.discharge.result, shop_agg.sim_discharge);   //
    set_optional(agg.effective_head, shop_agg.sim_eff_head);
  }

  void shop_adapter::from_shop(unit& pu, shop_pump const & shop_pu) {

    // FIXME: move the collection of inflow results and optimization results into
    //        separate function, and call the relevant collect-function from whatever
    //        is using SHOP. typically dstm-comptue or something - jeh

    // Read from alternative result attributes when running inflow calculation mode
    if (shop_pu.sim_consumption.exists())
      set_pump_ts(pu.production.result, shop_pu.sim_consumption);
    else if (shop_pu.consumption.exists())
      set_pump_ts(pu.production.result, shop_pu.consumption);

    if (shop_pu.sim_upflow.exists())
      set_pump_ts(pu.discharge.result, shop_pu.sim_upflow);
    else if (shop_pu.upflow.exists())
      set_pump_ts(pu.discharge.result, shop_pu.upflow);
  }

  void shop_adapter::from_shop(waterway& wtr, shop_gate const & shop_gt) {
    set_optional(wtr.discharge.result, shop_gt.discharge);
    set_optional(wtr.discharge.result, shop_gt.sim_discharge); // Alternative result attribute when running simulator
  }

  void shop_adapter::from_shop(waterway& wtr, shop_river const & shop_riv) {
    set_optional(wtr.discharge.result, shop_riv.flow);
    set_optional(wtr.discharge.result, shop_riv.sim_flow); // Alternative result attribute when running simulator

    set_optional(wtr.discharge.penalty.result.rate, shop_riv.flow_penalty);
    set_optional(wtr.discharge.penalty.result.constraint_min, shop_riv.min_flow_penalty);
    set_optional(wtr.discharge.penalty.result.constraint_max, shop_riv.max_flow_penalty);
    set_optional(wtr.discharge.penalty.result.ramping_up, shop_riv.ramping_up_penalty);
    set_optional(wtr.discharge.penalty.result.ramping_down, shop_riv.ramping_down_penalty);
  }

  void shop_adapter::from_shop(waterway& wtr, shop_discharge_group const & shop_dg) {
    set_optional(wtr.discharge.result, shop_dg.actual_discharge_m3s);
    set_optional(wtr.discharge.penalty.result.constraint_min, shop_dg.min_discharge_penalty);
    set_optional(wtr.discharge.penalty.result.constraint_max, shop_dg.max_discharge_penalty);
    set_optional(wtr.discharge.penalty.result.ramping_up, shop_dg.ramping_up_penalty);
    set_optional(wtr.discharge.penalty.result.ramping_down, shop_dg.ramping_down_penalty);
    set_optional(wtr.discharge.penalty.result.accumulated_min, shop_dg.lower_penalty_mm3);
    set_optional(wtr.discharge.penalty.result.accumulated_max, shop_dg.upper_penalty_mm3);
    set_optional(wtr.deviation.result, shop_dg.accumulated_deviation_mm3);
  }

  void shop_adapter::from_shop(gate& gt, shop_gate const & shop_gt) {
    set_optional(gt.discharge.result, shop_gt.discharge);
    set_optional(gt.discharge.result, shop_gt.sim_discharge); // Alternative result attribute when running simulator
  }

  void shop_adapter::from_shop(waterway& wtr, shop_tunnel const & shop_tn) {
    set_optional(wtr.discharge.result, shop_tn.flow);
    set_optional(wtr.discharge.result, shop_tn.sim_flow); // Alternative result attribute when running simulator
    if (wtr.gates.size() == 1) {
      if (auto gt = std::dynamic_pointer_cast<gate>(wtr.gates[0])) {
        set_optional(gt->opening.result, shop_tn.gate_opening);
        set_optional(gt->discharge.result, shop_tn.flow);
        set_optional(gt->discharge.result, shop_tn.sim_flow);
      }
    }
  }

  void shop_adapter::from_shop(unit_group& a, shop_reserve_group const & b) {
    auto add_optional = [this](apoint_ts& ts, auto const & shop_attr) {
      if (!ts) {
        set_optional(ts, shop_attr);
      } else { // Read values from shop attribute and add to the time series
        apoint_ts tmp_ts(
          time_axis,
          0.0,
          shyft::time_series::POINT_AVERAGE_VALUE); // A ts filled with zeros for each step of entire time axis
        set_optional(tmp_ts, shop_attr);
        ts = ts + tmp_ts;
      }
    };
    switch (a.group_type) {
    case unit_group_type::fcr_n_up:
      add_optional(a.obligation.result, b.fcr_n_up_slack);
      add_optional(a.obligation.penalty, b.fcr_n_up_violation);
      break;
    case unit_group_type::fcr_n_down:
      add_optional(a.obligation.result, b.fcr_n_down_slack);
      add_optional(a.obligation.penalty, b.fcr_n_down_violation);
      break;
    case unit_group_type::fcr_d_up:
      add_optional(a.obligation.result, b.fcr_d_up_slack);
      add_optional(a.obligation.penalty, b.fcr_d_up_violation);
      break;
    case unit_group_type::fcr_d_down:
      add_optional(a.obligation.penalty, b.fcr_d_down_violation);
      add_optional(a.obligation.result, b.fcr_d_down_slack);
      break;
    case unit_group_type::afrr_up:
      add_optional(a.obligation.result, b.frr_up_slack);
      add_optional(a.obligation.penalty, b.frr_up_violation);
      break;
    case unit_group_type::afrr_down:
      add_optional(a.obligation.result, b.frr_down_slack);
      add_optional(a.obligation.penalty, b.frr_down_violation);
      break;
    case unit_group_type::mfrr_up:
      add_optional(a.obligation.result, b.rr_up_slack);
      add_optional(a.obligation.penalty, b.rr_up_violation);
      break;
    case unit_group_type::mfrr_down:
      add_optional(a.obligation.result, b.rr_down_slack);
      add_optional(a.obligation.penalty, b.rr_down_violation);
      break;
    default:
      break;
    }
  }

  void shop_adapter::from_shop(optimization_summary& osm, shop_objective const & shop_obj) const {
    /* Note double set_optional on shop attrs containing sim_**
     * If shop is used for optimization, then the attr NOT containing sim_** will be set.
     * Else, if shop is used for simulation, the sim_** attr will be set. Hence we expose
     * the shop attr to stm model by context, how the model i runned by the user.
     **/
    set_optional(osm.total, shop_obj.total);
    set_optional(osm.sum_penalties, shop_obj.sum_penalties);
    set_optional(osm.minor_penalties, shop_obj.minor_penalties);
    set_optional(osm.major_penalties, shop_obj.major_penalties);
    set_optional(osm.grand_total, shop_obj.grand_total);
    set_optional(
      osm.grand_total, shop_obj.sim_grand_total); // Alternative result attribute when running inflow calculation mode

    // Reservoir
    set_optional(osm.reservoir.sum_ramping_penalty, shop_obj.rsv_ramping_penalty);
    set_optional(osm.reservoir.sum_limit_penalty, shop_obj.rsv_penalty);
    set_optional(osm.reservoir.sum_limit_penalty, shop_obj.sim_rsv_penalty);
    set_optional(osm.reservoir.end_value, shop_obj.rsv_end_value);
    set_optional(osm.reservoir.end_value, shop_obj.sim_rsv_end_value);
    set_optional(osm.reservoir.end_limit_penalty, shop_obj.rsv_end_penalty);
    set_optional(osm.reservoir.hard_limit_penalty, shop_obj.rsv_hard_limit_penalty);

    // Waterway
    set_optional(osm.waterway.vow_in_transit, shop_obj.vow_in_transit);
    set_optional(osm.waterway.sum_discharge_fee, shop_obj.sum_discharge_fee);
    set_optional(osm.waterway.discharge_group_penalty, shop_obj.discharge_group_penalty);
    set_optional(osm.waterway.discharge_group_ramping_penalty, shop_obj.discharge_group_ramping_penalty);

    // Gate
    set_optional(osm.gate.ramping_penalty, shop_obj.gate_ramping_penalty);
    set_optional(osm.gate.discharge_cost, shop_obj.gate_discharge_cost);
    set_optional(osm.gate.discharge_constraint_penalty, shop_obj.gate_q_constr_penalty);

    // Spill
    set_optional(osm.spill.cost, shop_obj.gate_spill_cost);
    set_optional(osm.spill.physical_cost, shop_obj.physical_spill_cost);
    set_optional(osm.spill.nonphysical_cost, shop_obj.nonphysical_spill_cost);
    set_optional(osm.spill.physical_volume, shop_obj.physical_spill_volume);
    set_optional(osm.spill.nonphysical_volume, shop_obj.nonphysical_spill_volume);

    // Bypass
    set_optional(osm.bypass.cost, shop_obj.bypass_cost);

    // Ramping
    set_optional(osm.ramping.ramping_penalty, shop_obj.sum_ramping_penalty);

    // Reserve
    set_optional(osm.reserve.violation_penalty, shop_obj.reserve_violation_penalty);
    set_optional(osm.reserve.sale_buy, shop_obj.reserve_sale_buy);
    set_optional(osm.reserve.obligation_value, shop_obj.reserve_oblig_value);

    // Unit
    set_optional(osm.unit.startup_cost, shop_obj.startup_costs);
    set_optional(osm.unit.startup_cost, shop_obj.sim_startup_costs);
    set_optional(osm.unit.schedule_penalty, shop_obj.gen_schedule_penalty);

    // Plant
    set_optional(osm.plant.production_constraint_penalty, shop_obj.plant_p_constr_penalty);
    set_optional(osm.plant.discharge_constraint_penalty, shop_obj.plant_q_constr_penalty);
    set_optional(osm.plant.schedule_penalty, shop_obj.plant_schedule_penalty);
    set_optional(osm.plant.ramping_penalty, shop_obj.plant_ramping_penalty);

    // Market
    set_optional(osm.market.sum_sale_buy, shop_obj.market_sale_buy);
    set_optional(
      osm.market.sum_sale_buy,
      shop_obj.sim_market_sale_buy); // Alternative result attribute when running inflow calculation mode

    set_optional(osm.market.load_penalty, shop_obj.load_penalty);
    set_optional(osm.market.load_value, shop_obj.load_value);
  }
}
