#pragma once
#include <memory>
#include <vector>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/shop/api/shop_proxy.h>

/*
 * Data adapters for STM specific representations of XY and TXY used by the shop object proxy.
 */
namespace shop::data {

  using xy_point = shyft::energy_market::hydro_power::point;
  using xy = shyft::energy_market::hydro_power::xy_point_curve_with_z;
  using xy_only = shyft::energy_market::hydro_power::xy_point_curve;
  using ts = shyft::time_series::dd::apoint_ts;

  //-- some utility macros to help doing zip/unzip vector -> point and x,y
  template <class E, class Xe, class Ye, class Ux, class Uy>
  static std::vector<E> _zip_dv(std::size_t n, std::unique_ptr<Xe[]> xv, std::unique_ptr<Ye[]> yv) {
    std::vector<E> r;
    r.reserve(n);
    for (std::size_t i = 0; i < n; ++i)
      r.emplace_back(E{Ux::to_base(xv[i]), Uy::to_base(yv[i])});
    return r;
  }

  template <class V, class C, class Fx>
  static std::unique_ptr<V[]> _xtract_v(C const &c, Fx &&_x) {
    auto r = std::make_unique<V[]>(c.size());
    std::transform(std::begin(c), std::end(c), r.get(), _x);
    return r;
  }

  // The xy <-> shop::data::XY adapter
  template <class Ux, class Uy>
  struct xy_factory<xy, Ux, Uy> {
    static xy create(double z, std::size_t n, std::unique_ptr<double[]> xv, std::unique_ptr<double[]> yv) {
      return {xy_only{_zip_dv<xy_point, double, double, Ux, Uy>(n, std::move(xv), std::move(yv))}, z};
    }

    static XY convert_to_shop(xy const &o) {
      return {
        o.z,
        int(o.xy_curve.points.size()),
        _xtract_v<double>(
          o.xy_curve.points,
          [](xy_point const &i) {
            return Ux::from_base(i.x);
          }),
        _xtract_v<double>(o.xy_curve.points, [](xy_point const &i) {
          return Uy::from_base(i.y);
        })};
    }
  };

  // The txy <-> shop::data::TXY adapter
  template <class U>
  struct txy_factory<ts, U> {
    static ts create(
      std::vector<time_t> const &t_axis,
      std::size_t n_values,
      std::unique_ptr<int[]> t_values,
      std::unique_ptr<double[]> y_values) {
      using shyft::core::utctime, shyft::core::utctimespan, shyft::core::utctime_from_seconds64,
        shyft::core::to_seconds64;
// TODO: The integer values in t_values (offset from start time), scaled with shop_time_resolution_unit (to get number
// of seconds), must match what is in t_axis. Currently we use the t_values integers to build the time axis of the
// resulting apoint_ts, but can chose to verify that it matches the t_axis to avoid trouble, by defining VERIFY_T_AXIS.
// We could do the opposite, convert the t_axis into a real time axis to be used for the apoint_ts, and either assuming
// or verify that the given t_values do match the points on this axis.
#define VERIFY_T_AXIS
      std::vector<utctime> t;
      t.reserve(n_values);
      std::vector<double> v;
      v.reserve(n_values);
      std::size_t n_axis{t_axis.size()};
      if (n_values > n_axis) { // n_values should be equal to n_axis-1 for most results, or equal to n_axis for some
                               // such as reservoir level results.
        throw std::runtime_error(
          "Expected number of values is less than or equal to number of points on the time axis");
      }
      auto t_axis_start{t_axis.front()};
      std::size_t i{0};
      for (; i < n_values; ++i) {
        auto sec = t_axis_start + t_values[i] * shop_time_resolution_unit;
#ifdef VERIFY_T_AXIS
        if (sec != t_axis[i]) {
          throw std::runtime_error("Time axis mismatch");
        }
#endif
        t.push_back(utctime_from_seconds64(sec));
        v.push_back(U::to_base(y_values[i]));
      }
      auto point_fx = shyft::time_series::POINT_AVERAGE_VALUE; // assume time-series represents average on the intervals
      if (n_values == n_axis) {
        // Reservoir level results contain additional value at end, so we to define a new end of the time axis
        t.push_back(
          utctime_from_seconds64(t_axis.back()) + utctimespan(1)); // t.push_back(time_axis.total_period().end+utctimespan(1));
                                                                   // // Time axis needs extra point (end of last value)
        point_fx = shyft::time_series::POINT_INSTANT_VALUE; // interpret points as state valid at the time.
      } else {
#ifdef VERIFY_T_AXIS
        if (to_seconds64(t.back()) >= t_axis.back()) {
          throw std::runtime_error("Time axis mismatch");
        }
#endif
        t.push_back(utctime_from_seconds64(
          t_axis
            .back())); // t.push_back(time_axis.total_period().end); // Time axis needs extra point (end of last value)
      }
      // t.push_back(time_axis.total_period().end); // Time axis needs extra point (end of last value)
      return {shyft::time_axis::point_dt{t}, v, point_fx};
    }

    static TXY convert_to_shop(ts const &ts, std::vector<time_t> const &t_axis) {
      using shyft::core::utctime, shyft::core::to_seconds64;
      std::vector<utctime> utc_axis;
      utc_axis.reserve(t_axis.size());
      std::transform(
        std::cbegin(t_axis), std::cend(t_axis), std::back_inserter(utc_axis), shyft::core::utctime_from_seconds64);
      auto t_start{utc_axis.front()};
      shop_time start_time{(time_t) to_seconds64(t_start)};
      if (!ts) {
        return {start_time, 0, nullptr, nullptr};
      } else {
        // Transform ts to time axis and convert the points to shop representation.
        // Note: Important for performance reasons to evaluate the entire series first,
        // the average transformation but then also the input ts if it is an expression.
        auto tsa{ts.average(shyft::time_axis::generic_dt{utc_axis}).evaluate()};
        auto n{tsa.size()};
        auto t{std::make_unique<int[]>(n)};
        auto y{std::make_unique<double[]>(n)};
        for (std::size_t i = 0; i < n; ++i) {
          t[i] = (int) (to_seconds64(tsa.time(i) - t_start) / shop_time_resolution_unit);
          y[i] = U::from_base(tsa.value(i));
        }
        shop_time start_time{(time_t) to_seconds64(t_start)};
        return {start_time, (int) n, std::move(t), std::move(y)};
      }
    }
  };

}
