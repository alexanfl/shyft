#pragma once
// auto genereated files based on active version of Sintef SHOP api dll
#include "shop_proxy.h"

namespace shop {

  using proxy::obj;
  using proxy::rw;
  using proxy::ro;
  using namespace proxy::unit;

  template <class A>
  struct reservoir : obj<A, 0> {
    using super = obj<A, 0>;
    reservoir() = default;

    reservoir(A* s, int oid)
      : super(s, oid) {
    }

    reservoir(reservoir const & o)
      : super(o) {
    }

    reservoir(reservoir&& o)
      : super(std::move(o)) {
    }

    reservoir& operator=(reservoir const & o) {
      super::operator=(o);
      return *this;
    }

    reservoir& operator=(reservoir&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
      connection_spill,
      connection_bypass,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      case relation::connection_spill:
        return "connection_spill";
      case relation::connection_bypass:
        return "connection_bypass";
      default:
        return nullptr;
      }
    }

    ro<reservoir, 3, int, no_unit, no_unit> energy_value_converted{this};                // x[no_unit],y[no_unit]
    rw<reservoir, 4, double, no_unit, no_unit> latitude{this};                           // x[no_unit],y[no_unit]
    rw<reservoir, 5, double, no_unit, no_unit> longitude{this};                          // x[no_unit],y[no_unit]
    rw<reservoir, 6, double, mm3, mm3> max_vol{this};                                    // x[mm3],y[mm3]
    rw<reservoir, 7, double, meter, meter> lrl{this};                                    // x[meter],y[meter]
    rw<reservoir, 8, double, meter, meter> hrl{this};                                    // x[meter],y[meter]
    rw<reservoir, 9, typename A::_xy, mm3, meter> vol_head{this};                        // x[mm3],y[meter]
    rw<reservoir, 10, typename A::_xy, meter, km2> head_area{this};                      // x[meter],y[km2]
    rw<reservoir, 11, typename A::_txy, no_unit, meter> elevation_adjustment{this};      // x[no_unit],y[meter]
    rw<reservoir, 12, double, mm3, mm3> start_vol{this};                                 // x[mm3],y[mm3]
    rw<reservoir, 13, double, meter, meter> start_head{this};                            // x[meter],y[meter]
    rw<reservoir, 14, typename A::_txy, no_unit, m3_per_s> inflow{this};                 // x[no_unit],y[m3_per_s]
    rw<reservoir, 15, typename A::_txy, no_unit, no_unit> inflow_flag{this};             // x[no_unit],y[no_unit]
    rw<reservoir, 16, typename A::_txy, no_unit, no_unit> sim_inflow_flag{this};         // x[no_unit],y[no_unit]
    rw<reservoir, 17, typename A::_xy, meter, m3_per_s> flow_descr{this};                // x[meter],y[m3_per_s]
    rw<reservoir, 18, typename A::_txy, no_unit, no_unit> overflow_mip_flag{this};       // x[no_unit],y[no_unit]
    rw<reservoir, 19, typename A::_txy, no_unit, nok_per_mm3> overflow_cost{this};       // x[no_unit],y[nok_per_mm3]
    rw<reservoir, 20, typename A::_txy, no_unit, no_unit> overflow_cost_flag{this};      // x[no_unit],y[no_unit]
    rw<reservoir, 21, typename A::_txy, no_unit, mm3> min_vol_constr{this};              // x[no_unit],y[mm3]
    rw<reservoir, 22, typename A::_txy, no_unit, no_unit> min_vol_constr_flag{this};     // x[no_unit],y[no_unit]
    rw<reservoir, 23, typename A::_txy, no_unit, mm3> max_vol_constr{this};              // x[no_unit],y[mm3]
    rw<reservoir, 24, typename A::_txy, no_unit, no_unit> max_vol_constr_flag{this};     // x[no_unit],y[no_unit]
    rw<reservoir, 25, typename A::_txy, no_unit, meter> min_head_constr{this};           // x[no_unit],y[meter]
    rw<reservoir, 26, typename A::_txy, no_unit, no_unit> min_head_constr_flag{this};    // x[no_unit],y[no_unit]
    rw<reservoir, 27, typename A::_txy, no_unit, meter> max_head_constr{this};           // x[no_unit],y[meter]
    rw<reservoir, 28, typename A::_txy, no_unit, no_unit> max_head_constr_flag{this};    // x[no_unit],y[no_unit]
    rw<reservoir, 29, typename A::_txy, no_unit, mm3> tactical_limit_min{this};          // x[no_unit],y[mm3]
    rw<reservoir, 30, typename A::_txy, no_unit, no_unit> tactical_limit_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir, 31, typename A::_txy, no_unit, nok_per_mm3h> tactical_cost_min{this};  // x[no_unit],y[nok_per_mm3h]
    rw<reservoir, 32, typename A::_txy, no_unit, no_unit> tactical_cost_min_flag{this};  // x[no_unit],y[no_unit]
    rw<reservoir, 33, typename A::_txy, no_unit, mm3> tactical_limit_max{this};          // x[no_unit],y[mm3]
    rw<reservoir, 34, typename A::_txy, no_unit, no_unit> tactical_limit_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir, 35, typename A::_txy, no_unit, nok_per_mm3h> tactical_cost_max{this};  // x[no_unit],y[nok_per_mm3h]
    rw<reservoir, 36, typename A::_txy, no_unit, no_unit> tactical_cost_max_flag{this};  // x[no_unit],y[no_unit]
    rw<reservoir, 37, typename A::_txy, no_unit, mm3> upper_slack{this};                 // x[no_unit],y[mm3]
    rw<reservoir, 38, typename A::_txy, no_unit, mm3> lower_slack{this};                 // x[no_unit],y[mm3]
    rw<reservoir, 39, typename A::_txy, no_unit, mm3> schedule{this};                    // x[no_unit],y[mm3]
    rw<reservoir, 40, typename A::_txy, no_unit, no_unit> schedule_flag{this};           // x[no_unit],y[no_unit]
    rw<reservoir, 41, typename A::_txy, no_unit, mm3> volume_schedule{this};             // x[no_unit],y[mm3]
    rw<reservoir, 42, typename A::_txy, no_unit, meter> level_schedule{this};            // x[no_unit],y[meter]
    rw<reservoir, 43, typename A::_txy, no_unit, mm3> volume_ramping_up{this};           // x[no_unit],y[mm3]
    rw<reservoir, 44, typename A::_txy, no_unit, mm3> volume_ramping_down{this};         // x[no_unit],y[mm3]
    rw<reservoir, 45, typename A::_txy, no_unit, meter> level_ramping_up{this};          // x[no_unit],y[meter]
    rw<reservoir, 46, typename A::_txy, no_unit, meter> level_ramping_down{this};        // x[no_unit],y[meter]
    rw<reservoir, 47, std::vector<typename A::_xy>, mm3, nok_per_mm3> water_value_input{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir, 48, double, nok_per_mwh, nok_per_mwh> energy_value_input{this};       // x[nok_per_mwh],y[nok_per_mwh]
    rw<reservoir, 49, typename A::_xy, mm3, nok_per_mm3> peak_volume_cost_curve{this};  // x[mm3],y[nok_per_mm3]
    rw<reservoir, 50, typename A::_xy, mm3, nok_per_mm3> flood_volume_cost_curve{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir, 51, typename A::_txy, no_unit, meter_per_hour> evaporation_rate{this}; // x[no_unit],y[meter_per_hour]
    rw<reservoir, 52, typename A::_txy, no_unit, nok_per_meter> level_rolling_ramping_penalty_cost{
      this}; // x[no_unit],y[nok_per_meter]
    rw<reservoir, 53, typename A::_txy, no_unit, nok_per_meter> level_period_ramping_penalty_cost{
      this}; // x[no_unit],y[nok_per_meter]
    rw<reservoir, 54, std::map<int64_t, typename A::_xy>, minute, meter> average_level_rolling_ramping_up{
      this}; // x[minute],y[meter]
    rw<reservoir, 55, std::map<int64_t, typename A::_xy>, minute, meter> average_level_rolling_ramping_down{
      this}; // x[minute],y[meter]
    rw<reservoir, 56, std::map<int64_t, typename A::_xy>, minute, meter> limit_level_rolling_ramping_up{
      this}; // x[minute],y[meter]
    rw<reservoir, 57, std::map<int64_t, typename A::_xy>, minute, meter> limit_level_rolling_ramping_down{
      this}; // x[minute],y[meter]
    rw<reservoir, 58, std::map<int64_t, typename A::_xy>, minute, meter> average_level_period_ramping_up{
      this}; // x[minute],y[meter]
    rw<reservoir, 59, std::map<int64_t, typename A::_xy>, minute, meter> average_level_period_ramping_down{
      this}; // x[minute],y[meter]
    rw<reservoir, 60, std::map<int64_t, typename A::_xy>, minute, meter> limit_level_period_ramping_up{
      this}; // x[minute],y[meter]
    rw<reservoir, 61, std::map<int64_t, typename A::_xy>, minute, meter> limit_level_period_ramping_down{
      this};                                                                    // x[minute],y[meter]
    rw<reservoir, 62, typename A::_txy, no_unit, meter> historical_level{this}; // x[no_unit],y[meter]
    //--TODO: rw<reservoir,63,sy,minute,minute> average_level_period_ramping_up_offset{this}; // x[minute],y[minute]
    //--TODO: rw<reservoir,64,sy,minute,minute> average_level_period_ramping_down_offset{this}; // x[minute],y[minute]
    //--TODO: rw<reservoir,65,sy,minute,minute> limit_level_period_ramping_up_offset{this}; // x[minute],y[minute]
    //--TODO: rw<reservoir,66,sy,minute,minute> limit_level_period_ramping_down_offset{this}; // x[minute],y[minute]
    rw<reservoir, 67, std::map<int64_t, typename A::_xy>, minute, mm3> volume_nonseq_ramping_limit_up{
      this}; // x[minute],y[mm3]
    rw<reservoir, 68, std::map<int64_t, typename A::_xy>, minute, mm3> volume_nonseq_ramping_limit_down{
      this}; // x[minute],y[mm3]
    rw<reservoir, 69, std::map<int64_t, typename A::_xy>, minute, mm3> volume_amplitude_ramping_limit_up{
      this}; // x[minute],y[mm3]
    rw<reservoir, 70, std::map<int64_t, typename A::_xy>, minute, mm3> volume_amplitude_ramping_limit_down{
      this}; // x[minute],y[mm3]
    rw<reservoir, 71, std::map<int64_t, typename A::_xy>, minute, meter> level_nonseq_ramping_limit_up{
      this}; // x[minute],y[meter]
    rw<reservoir, 72, std::map<int64_t, typename A::_xy>, minute, meter> level_nonseq_ramping_limit_down{
      this}; // x[minute],y[meter]
    rw<reservoir, 73, std::map<int64_t, typename A::_xy>, minute, meter> level_amplitude_ramping_limit_up{
      this}; // x[minute],y[meter]
    rw<reservoir, 74, std::map<int64_t, typename A::_xy>, minute, meter> level_amplitude_ramping_limit_down{
      this};                                                                       // x[minute],y[meter]
    ro<reservoir, 75, typename A::_txy, no_unit, mm3> storage{this};               // x[no_unit],y[mm3]
    ro<reservoir, 76, typename A::_txy, no_unit, mm3> sim_storage{this};           // x[no_unit],y[mm3]
    ro<reservoir, 77, typename A::_txy, no_unit, meter> head{this};                // x[no_unit],y[meter]
    ro<reservoir, 78, typename A::_txy, no_unit, km2> area{this};                  // x[no_unit],y[km2]
    ro<reservoir, 79, typename A::_txy, no_unit, meter> sim_head{this};            // x[no_unit],y[meter]
    ro<reservoir, 80, typename A::_txy, no_unit, m3_per_s> sim_inflow{this};       // x[no_unit],y[m3_per_s]
    ro<reservoir, 81, typename A::_txy, mm3, mm3> endpoint_penalty{this};          // x[mm3],y[mm3]
    ro<reservoir, 82, typename A::_txy, no_unit, mm3> penalty{this};               // x[no_unit],y[mm3]
    ro<reservoir, 83, typename A::_txy, no_unit, mm3> tactical_penalty_up{this};   // x[no_unit],y[mm3]
    ro<reservoir, 84, typename A::_txy, no_unit, mm3> tactical_penalty_down{this}; // x[no_unit],y[mm3]
    ro<reservoir, 85, typename A::_txy, no_unit, nok> end_penalty{this};           // x[no_unit],y[nok]
    ro<reservoir, 86, typename A::_txy, no_unit, nok> penalty_nok{this};           // x[no_unit],y[nok]
    ro<reservoir, 87, typename A::_txy, no_unit, nok> tactical_penalty{this};      // x[no_unit],y[nok]
    ro<reservoir, 88, typename A::_txy, no_unit, nok_per_mm3> water_value_global_result{
      this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir, 89, typename A::_txy, no_unit, nok_per_mm3> water_value_local_result{
      this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir, 90, typename A::_txy, no_unit, nok_per_mwh> energy_value_local_result{
      this};                                                                            // x[no_unit],y[nok_per_mwh]
    ro<reservoir, 91, typename A::_txy, no_unit, nok> end_value{this};                  // x[no_unit],y[nok]
    ro<reservoir, 92, typename A::_txy, no_unit, nok> change_in_end_value{this};        // x[no_unit],y[nok]
    ro<reservoir, 93, typename A::_txy, no_unit, nok> vow_in_transit{this};             // x[no_unit],y[nok]
    ro<reservoir, 94, double, nok_per_mm3, nok_per_mm3> calc_global_water_value{this};  // x[nok_per_mm3],y[nok_per_mm3]
    ro<reservoir, 95, double, mwh_per_mm3, mwh_per_mm3> energy_conversion_factor{this}; // x[mwh_per_mm3],y[mwh_per_mm3]
    ro<reservoir, 96, std::vector<typename A::_xy>, mm3, nok_per_mm3> water_value_cut_result{
      this};                                                                       // x[mm3],y[nok_per_mm3]
    ro<reservoir, 97, int, no_unit, no_unit> added_to_network{this};               // x[no_unit],y[no_unit]
    ro<reservoir, 98, int, no_unit, no_unit> network_no{this};                     // x[no_unit],y[no_unit]
    ro<reservoir, 99, typename A::_txy, no_unit, nok> peak_volume_penalty{this};   // x[no_unit],y[nok]
    ro<reservoir, 100, typename A::_txy, no_unit, nok> flood_volume_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir, 101, typename A::_txy, no_unit, meter> linearized_level{this};   // x[no_unit],y[meter]
  };

  template <class A>
  struct power_plant : obj<A, 1> {
    using super = obj<A, 1>;
    power_plant() = default;

    power_plant(A* s, int oid)
      : super(s, oid) {
    }

    power_plant(power_plant const & o)
      : super(o) {
    }

    power_plant(power_plant&& o)
      : super(std::move(o)) {
    }

    power_plant& operator=(power_plant const & o) {
      super::operator=(o);
      return *this;
    }

    power_plant& operator=(power_plant&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<power_plant, 106, int, no_unit, no_unit> num_gen{this};                           // x[no_unit],y[no_unit]
    ro<power_plant, 107, int, no_unit, no_unit> num_pump{this};                          // x[no_unit],y[no_unit]
    rw<power_plant, 111, double, no_unit, no_unit> less_distribution_eps{this};          // x[no_unit],y[no_unit]
    rw<power_plant, 118, double, no_unit, no_unit> latitude{this};                       // x[no_unit],y[no_unit]
    rw<power_plant, 119, double, no_unit, no_unit> longitude{this};                      // x[no_unit],y[no_unit]
    rw<power_plant, 120, double, no_unit, no_unit> power_head_optimization_factor{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 121, double, percent, percent> ownership{this};                      // x[percent],y[percent]
    rw<power_plant, 122, typename A::_txy, no_unit, no_unit> prod_area{this};            // x[no_unit],y[no_unit]
    rw<power_plant, 123, typename A::_txy, no_unit, no_unit> prod_area_flag{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 124, double, kwh_per_mm3, kwh_per_mm3> prod_factor{this};        // x[kwh_per_mm3],y[kwh_per_mm3]
    rw<power_plant, 125, double, meter, meter> outlet_line{this};                    // x[meter],y[meter]
    rw<power_plant, 126, typename A::_txy, no_unit, meter> intake_line{this};        // x[no_unit],y[meter]
    rw<power_plant, 127, std::vector<double>, s2_per_m5, s2_per_m5> main_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<power_plant, 128, std::vector<double>, s2_per_m5, s2_per_m5> penstock_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<power_plant, 129, std::vector<typename A::_xy>, m3_per_s, meter> tailrace_loss{this}; // x[m3_per_s],y[meter]
    rw<power_plant, 130, typename A::_txy, no_unit, no_unit> tailrace_loss_from_bypass_flag{
      this};                                                                               // x[no_unit],y[no_unit]
    rw<power_plant, 131, std::vector<typename A::_xy>, m3_per_s, meter> intake_loss{this}; // x[m3_per_s],y[meter]
    rw<power_plant, 132, typename A::_txy, no_unit, no_unit> intake_loss_from_bypass_flag{
      this};                                                                           // x[no_unit],y[no_unit]
    rw<power_plant, 133, typename A::_txy, no_unit, delta_meter> tides{this};          // x[no_unit],y[delta_meter]
    rw<power_plant, 134, int, no_unit, no_unit> time_delay{this};                      // x[no_unit],y[no_unit]
    rw<power_plant, 135, typename A::_xy, hour, no_unit> shape_discharge{this};        // x[hour],y[no_unit]
    rw<power_plant, 136, typename A::_txy, no_unit, nok_per_mm3> discharge_fee{this};  // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 137, typename A::_txy, no_unit, no_unit> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 138, typename A::_xy, m3_per_s, nok_per_h_per_m3_per_s> discharge_cost_curve{
      this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<power_plant, 139, typename A::_txy, no_unit, nok_per_mwh> feeding_fee{this};      // x[no_unit],y[nok_per_mwh]
    rw<power_plant, 140, typename A::_txy, no_unit, no_unit> feeding_fee_flag{this};     // x[no_unit],y[no_unit]
    rw<power_plant, 141, typename A::_txy, no_unit, nok_per_mwh> production_fee{this};   // x[no_unit],y[nok_per_mwh]
    rw<power_plant, 142, typename A::_txy, no_unit, no_unit> production_fee_flag{this};  // x[no_unit],y[no_unit]
    rw<power_plant, 143, typename A::_txy, no_unit, nok_per_mwh> consumption_fee{this};  // x[no_unit],y[nok_per_mwh]
    rw<power_plant, 144, typename A::_txy, no_unit, no_unit> consumption_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 145, typename A::_txy, no_unit, no_unit> linear_startup_flag{this};  // x[no_unit],y[no_unit]
    rw<power_plant, 146, typename A::_txy, no_unit, no_unit> maintenance_flag{this};     // x[no_unit],y[no_unit]
    rw<power_plant, 147, typename A::_txy, no_unit, no_unit> mip_flag{this};             // x[no_unit],y[no_unit]
    rw<power_plant, 148, typename A::_txy, no_unit, no_unit> mip_length{this};           // x[no_unit],y[no_unit]
    rw<power_plant, 149, typename A::_txy, no_unit, no_unit> mip_length_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 150, std::vector<int>, no_unit, no_unit> gen_priority{this};         // x[no_unit],y[no_unit]
    rw<power_plant, 151, typename A::_txy, no_unit, no_unit> n_seg_down{this};           // x[no_unit],y[no_unit]
    rw<power_plant, 152, typename A::_txy, no_unit, no_unit> n_seg_up{this};             // x[no_unit],y[no_unit]
    rw<power_plant, 153, typename A::_txy, no_unit, no_unit> n_mip_seg_down{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 154, typename A::_txy, no_unit, no_unit> n_mip_seg_up{this};         // x[no_unit],y[no_unit]
    rw<power_plant, 155, typename A::_txy, no_unit, no_unit> dyn_pq_seg_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 156, typename A::_txy, no_unit, no_unit> dyn_mip_pq_seg_flag{this};  // x[no_unit],y[no_unit]
    rw<power_plant, 157, typename A::_txy, no_unit, no_unit> build_original_pq_curves_by_turb_eff{
      this};                                                                                // x[no_unit],y[no_unit]
    rw<power_plant, 158, typename A::_txy, no_unit, mw> min_p_constr{this};                 // x[no_unit],y[mw]
    rw<power_plant, 159, typename A::_txy, no_unit, no_unit> min_p_constr_flag{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 160, typename A::_txy, no_unit, no_unit> min_p_penalty_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 161, typename A::_txy, no_unit, nok_per_mwh> min_p_penalty_cost{this};  // x[no_unit],y[nok_per_mwh]
    rw<power_plant, 162, typename A::_txy, no_unit, no_unit> min_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 163, typename A::_txy, no_unit, mw> max_p_constr{this};                 // x[no_unit],y[mw]
    rw<power_plant, 164, typename A::_txy, no_unit, no_unit> max_p_constr_flag{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 165, typename A::_txy, no_unit, no_unit> max_p_penalty_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 166, typename A::_txy, no_unit, nok_per_mwh> max_p_penalty_cost{this};  // x[no_unit],y[nok_per_mwh]
    rw<power_plant, 167, typename A::_txy, no_unit, no_unit> max_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 168, typename A::_txy, no_unit, m3_per_s> min_q_constr{this};           // x[no_unit],y[m3_per_s]
    rw<power_plant, 169, typename A::_txy, no_unit, no_unit> min_q_constr_flag{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 170, typename A::_txy, no_unit, no_unit> min_q_penalty_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 171, typename A::_txy, no_unit, nok_per_mm3> min_q_penalty_cost{this};  // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 172, typename A::_txy, no_unit, no_unit> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 173, typename A::_txy, no_unit, m3_per_s> max_q_constr{this};           // x[no_unit],y[m3_per_s]
    rw<power_plant, 174, typename A::_txy, no_unit, no_unit> max_q_constr_flag{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 175, typename A::_txy, no_unit, no_unit> max_q_penalty_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 176, typename A::_txy, no_unit, nok_per_mm3> max_q_penalty_cost{this};  // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 177, typename A::_txy, no_unit, no_unit> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 178, typename A::_xy, meter, m3_per_s> max_q_limit_rsv_up{this};        // x[meter],y[m3_per_s]
    rw<power_plant, 179, typename A::_xy, meter, m3_per_s> max_q_limit_rsv_down{this};      // x[meter],y[m3_per_s]
    rw<power_plant, 180, typename A::_txy, no_unit, mw> production_schedule{this};          // x[no_unit],y[mw]
    rw<power_plant, 181, typename A::_txy, no_unit, no_unit> production_schedule_flag{this};  // x[no_unit],y[no_unit]
    rw<power_plant, 182, typename A::_txy, no_unit, m3_per_s> discharge_schedule{this};       // x[no_unit],y[m3_per_s]
    rw<power_plant, 183, typename A::_txy, no_unit, no_unit> discharge_schedule_flag{this};   // x[no_unit],y[no_unit]
    rw<power_plant, 184, typename A::_txy, no_unit, mw> consumption_schedule{this};           // x[no_unit],y[mw]
    rw<power_plant, 185, typename A::_txy, no_unit, no_unit> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 186, typename A::_txy, no_unit, m3_per_s> upflow_schedule{this};          // x[no_unit],y[m3_per_s]
    rw<power_plant, 187, typename A::_txy, no_unit, no_unit> upflow_schedule_flag{this};      // x[no_unit],y[no_unit]
    rw<power_plant, 188, double, nok, nok> sched_penalty_cost_down{this};                     // x[nok],y[nok]
    rw<power_plant, 189, double, nok, nok> sched_penalty_cost_up{this};                       // x[nok],y[nok]
    rw<power_plant, 190, typename A::_txy, no_unit, mw_hour> power_ramping_up{this};          // x[no_unit],y[mw_hour]
    rw<power_plant, 191, typename A::_txy, no_unit, mw_hour> power_ramping_down{this};        // x[no_unit],y[mw_hour]
    rw<power_plant, 192, typename A::_txy, no_unit, m3sec_hour> discharge_ramping_up{this}; // x[no_unit],y[m3sec_hour]
    rw<power_plant, 193, typename A::_txy, no_unit, m3sec_hour> discharge_ramping_down{
      this};                                                                                 // x[no_unit],y[m3sec_hour]
    rw<power_plant, 194, typename A::_txy, no_unit, mw> block_merge_tolerance{this};         // x[no_unit],y[mw]
    rw<power_plant, 195, typename A::_txy, no_unit, hour> block_generation_mwh{this};        // x[no_unit],y[hour]
    rw<power_plant, 196, typename A::_txy, no_unit, hour> block_generation_m3s{this};        // x[no_unit],y[hour]
    rw<power_plant, 197, typename A::_txy, no_unit, mw> frr_up_min{this};                    // x[no_unit],y[mw]
    rw<power_plant, 198, typename A::_txy, no_unit, mw> frr_up_max{this};                    // x[no_unit],y[mw]
    rw<power_plant, 199, typename A::_txy, no_unit, mw> frr_down_min{this};                  // x[no_unit],y[mw]
    rw<power_plant, 200, typename A::_txy, no_unit, mw> frr_down_max{this};                  // x[no_unit],y[mw]
    rw<power_plant, 201, typename A::_txy, no_unit, mw> rr_up_min{this};                     // x[no_unit],y[mw]
    rw<power_plant, 202, typename A::_txy, no_unit, no_unit> frr_symmetric_flag{this};       // x[no_unit],y[no_unit]
    rw<power_plant, 203, typename A::_txy, no_unit, no_unit> bp_dyn_wv_flag{this};           // x[no_unit],y[no_unit]
    ro<power_plant, 204, typename A::_txy, no_unit, mw> ref_prod{this};                      // x[no_unit],y[mw]
    rw<power_plant, 205, typename A::_txy, no_unit, no_unit> plant_unbalance_recommit{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 206, typename A::_txy, no_unit, mw> spinning_reserve_up_max{this};       // x[no_unit],y[mw]
    rw<power_plant, 207, typename A::_txy, no_unit, mw> spinning_reserve_down_max{this};     // x[no_unit],y[mw]
    rw<power_plant, 208, std::vector<typename A::_xy>, mw, mw> ramping_steps{this};          // x[mw],y[mw]
    rw<power_plant, 209, typename A::_txy, no_unit, nok_per_m3_per_s> discharge_rolling_ramping_penalty_cost{
      this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<power_plant, 210, typename A::_txy, no_unit, nok_per_m3_per_s> discharge_period_ramping_penalty_cost{
      this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<power_plant, 211, std::map<int64_t, typename A::_xy>, minute, m3_per_s> average_discharge_rolling_ramping_up{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 212, std::map<int64_t, typename A::_xy>, minute, m3_per_s> average_discharge_rolling_ramping_down{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 213, std::map<int64_t, typename A::_xy>, minute, m3_per_s> limit_discharge_rolling_ramping_up{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 214, std::map<int64_t, typename A::_xy>, minute, m3_per_s> limit_discharge_rolling_ramping_down{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 215, std::map<int64_t, typename A::_xy>, minute, m3_per_s> average_discharge_period_ramping_up{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 216, std::map<int64_t, typename A::_xy>, minute, m3_per_s> average_discharge_period_ramping_down{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 217, std::map<int64_t, typename A::_xy>, minute, m3_per_s> limit_discharge_period_ramping_up{
      this}; // x[minute],y[m3_per_s]
    rw<power_plant, 218, std::map<int64_t, typename A::_xy>, minute, m3_per_s> limit_discharge_period_ramping_down{
      this};                                                                              // x[minute],y[m3_per_s]
    rw<power_plant, 219, typename A::_txy, no_unit, m3_per_s> historical_discharge{this}; // x[no_unit],y[m3_per_s]
    //--TODO: rw<power_plant,220,sy,minute,minute> average_discharge_period_ramping_up_offset{this}; //
    //x[minute],y[minute]
    //--TODO: rw<power_plant,221,sy,minute,minute> average_discharge_period_ramping_down_offset{this}; //
    //x[minute],y[minute]
    //--TODO: rw<power_plant,222,sy,minute,minute> limit_discharge_period_ramping_up_offset{this}; //
    //x[minute],y[minute]
    //--TODO: rw<power_plant,223,sy,minute,minute> limit_discharge_period_ramping_down_offset{this}; //
    //x[minute],y[minute]
    rw<power_plant, 224, double, hour, hour> fcr_n_up_activation_time{this};                   // x[hour],y[hour]
    rw<power_plant, 225, double, hour, hour> fcr_n_down_activation_time{this};                 // x[hour],y[hour]
    rw<power_plant, 226, double, hour, hour> fcr_d_up_activation_time{this};                   // x[hour],y[hour]
    rw<power_plant, 227, double, hour, hour> fcr_d_down_activation_time{this};                 // x[hour],y[hour]
    rw<power_plant, 228, double, hour, hour> frr_up_activation_time{this};                     // x[hour],y[hour]
    rw<power_plant, 229, double, hour, hour> frr_down_activation_time{this};                   // x[hour],y[hour]
    rw<power_plant, 230, double, hour, hour> rr_up_activation_time{this};                      // x[hour],y[hour]
    rw<power_plant, 231, double, hour, hour> rr_down_activation_time{this};                    // x[hour],y[hour]
    rw<power_plant, 232, typename A::_txy, no_unit, no_unit> fcr_n_up_activation_factor{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 233, typename A::_txy, no_unit, no_unit> fcr_n_down_activation_factor{
      this};                                                                                   // x[no_unit],y[no_unit]
    rw<power_plant, 234, typename A::_txy, no_unit, no_unit> fcr_d_up_activation_factor{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 235, typename A::_txy, no_unit, no_unit> fcr_d_down_activation_factor{
      this};                                                                                   // x[no_unit],y[no_unit]
    rw<power_plant, 236, typename A::_txy, no_unit, no_unit> frr_up_activation_factor{this};   // x[no_unit],y[no_unit]
    rw<power_plant, 237, typename A::_txy, no_unit, no_unit> frr_down_activation_factor{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 238, typename A::_txy, no_unit, no_unit> rr_up_activation_factor{this};    // x[no_unit],y[no_unit]
    rw<power_plant, 239, typename A::_txy, no_unit, no_unit> rr_down_activation_factor{this};  // x[no_unit],y[no_unit]
    rw<power_plant, 240, typename A::_txy, no_unit, nok_per_mm3> fcr_n_up_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 241, typename A::_txy, no_unit, nok_per_mm3> fcr_n_down_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 242, typename A::_txy, no_unit, nok_per_mm3> fcr_d_up_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 243, typename A::_txy, no_unit, nok_per_mm3> fcr_d_down_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 244, typename A::_txy, no_unit, nok_per_mm3> frr_up_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 245, typename A::_txy, no_unit, nok_per_mm3> frr_down_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 246, typename A::_txy, no_unit, nok_per_mm3> rr_up_activation_penalty_cost{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 247, typename A::_txy, no_unit, nok_per_mm3> rr_down_activation_penalty_cost{
      this};                                                                              // x[no_unit],y[nok_per_mm3]
    rw<power_plant, 248, typename A::_txy, no_unit, mw> sum_reserve_up_max{this};         // x[no_unit],y[mw]
    rw<power_plant, 249, typename A::_txy, no_unit, mw> sum_reserve_down_max{this};       // x[no_unit],y[mw]
    rw<power_plant, 250, typename A::_txy, no_unit, m3_per_s> max_q_reserve_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant, 251, typename A::_txy, no_unit, m3_per_s> min_q_reserve_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant, 252, typename A::_txy, no_unit, nok_per_m3_per_s> max_q_reserve_constr_penalty_cost{
      this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<power_plant, 253, typename A::_txy, no_unit, nok_per_m3_per_s> min_q_reserve_constr_penalty_cost{
      this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<power_plant, 254, typename A::_txy, no_unit, nok_per_mw> sum_reserve_up_max_penalty_cost{
      this}; // x[no_unit],y[nok_per_mw]
    rw<power_plant, 255, typename A::_txy, no_unit, nok_per_mw> sum_reserve_down_max_penalty_cost{
      this}; // x[no_unit],y[nok_per_mw]
    rw<power_plant, 256, double, no_unit, no_unit> reserve_tactical_activation_cost_scaling{
      this};                                                                                    // x[no_unit],y[no_unit]
    rw<power_plant, 257, typename A::_txy, no_unit, no_unit> coupled_fcr_activation_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant, 258, typename A::_txy, m3_per_s, no_unit> ramping_step_reference_discharge{
      this};                                                                          // x[m3_per_s],y[no_unit]
    ro<power_plant, 259, typename A::_txy, no_unit, mw> production{this};             // x[no_unit],y[mw]
    ro<power_plant, 260, typename A::_txy, no_unit, mw> solver_production{this};      // x[no_unit],y[mw]
    ro<power_plant, 261, typename A::_txy, no_unit, mw> sim_production{this};         // x[no_unit],y[mw]
    ro<power_plant, 262, typename A::_txy, no_unit, mw> prod_unbalance{this};         // x[no_unit],y[mw]
    ro<power_plant, 263, typename A::_txy, no_unit, mw> consumption{this};            // x[no_unit],y[mw]
    ro<power_plant, 264, typename A::_txy, no_unit, mw> sim_consumption{this};        // x[no_unit],y[mw]
    ro<power_plant, 265, typename A::_txy, no_unit, mw> solver_consumption{this};     // x[no_unit],y[mw]
    ro<power_plant, 266, typename A::_txy, no_unit, mw> cons_unbalance{this};         // x[no_unit],y[mw]
    ro<power_plant, 267, typename A::_txy, no_unit, m3_per_s> discharge{this};        // x[no_unit],y[m3_per_s]
    ro<power_plant, 268, typename A::_txy, no_unit, m3_per_s> solver_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant, 269, typename A::_txy, no_unit, m3_per_s> sim_discharge{this};    // x[no_unit],y[m3_per_s]
    ro<power_plant, 270, typename A::_txy, no_unit, m3_per_s> upflow{this};           // x[no_unit],y[m3_per_s]
    ro<power_plant, 271, typename A::_txy, no_unit, m3_per_s> sim_upflow{this};       // x[no_unit],y[m3_per_s]
    ro<power_plant, 272, typename A::_txy, no_unit, m3_per_s> solver_upflow{this};    // x[no_unit],y[m3_per_s]
    ro<power_plant, 273, typename A::_txy, no_unit, meter> gross_head{this};          // x[no_unit],y[meter]
    ro<power_plant, 274, typename A::_txy, no_unit, meter> eff_head{this};            // x[no_unit],y[meter]
    ro<power_plant, 275, typename A::_txy, no_unit, meter> head_loss{this};           // x[no_unit],y[meter]
    ro<power_plant, 276, typename A::_txy, no_unit, mw> min_p_penalty{this};          // x[no_unit],y[mw]
    ro<power_plant, 277, typename A::_txy, no_unit, mw> max_p_penalty{this};          // x[no_unit],y[mw]
    ro<power_plant, 278, typename A::_txy, no_unit, mm3> min_q_penalty{this};         // x[no_unit],y[mm3]
    ro<power_plant, 279, typename A::_txy, no_unit, mm3> max_q_penalty{this};         // x[no_unit],y[mm3]
    ro<power_plant, 280, typename A::_txy, no_unit, nok> p_constr_penalty{this};      // x[no_unit],y[nok]
    ro<power_plant, 281, typename A::_txy, no_unit, nok> q_constr_penalty{this};      // x[no_unit],y[nok]
    ro<power_plant, 282, typename A::_txy, no_unit, nok> schedule_up_penalty{this};   // x[no_unit],y[nok]
    ro<power_plant, 283, typename A::_txy, no_unit, nok> schedule_down_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant, 284, typename A::_txy, no_unit, nok> schedule_penalty{this};      // x[no_unit],y[nok]
    ro<power_plant, 285, typename A::_txy, mw, mw> max_prod{this};                    // x[mw],y[mw]
    ro<power_plant, 286, std::map<int64_t, typename A::_xy>, mw, m3_per_s> best_profit_q{this}; // x[mw],y[m3_per_s]
    ro<power_plant, 287, std::map<int64_t, typename A::_xy>, mw, nok_per_mw> best_profit_mc{
      this}; // x[mw],y[nok_per_mw]
    ro<power_plant, 288, std::map<int64_t, typename A::_xy>, mw, nok_per_mw> best_profit_ac{
      this}; // x[mw],y[nok_per_mw]
    ro<power_plant, 289, std::map<int64_t, typename A::_xy>, mw, nok> best_profit_commitment_cost{this}; // x[mw],y[nok]
    ro<power_plant, 290, std::map<int64_t, typename A::_xy>, mw, nok_per_mw> best_profit_bid_matrix{
      this};                                                                       // x[mw],y[nok_per_mw]
    ro<power_plant, 291, int, no_unit, no_unit> times_of_wrong_pq_uploading{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct unit : obj<A, 2> {
    using super = obj<A, 2>;
    unit() = default;

    unit(A* s, int oid)
      : super(s, oid) {
    }

    unit(unit const & o)
      : super(o) {
    }

    unit(unit&& o)
      : super(std::move(o)) {
    }

    unit& operator=(unit const & o) {
      super::operator=(o);
      return *this;
    }

    unit& operator=(unit&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<unit, 299, int, no_unit, no_unit> type{this};                                      // x[no_unit],y[no_unit]
    ro<unit, 300, int, no_unit, no_unit> num_needle_comb{this};                           // x[no_unit],y[no_unit]
    rw<unit, 301, typename A::_txy, no_unit, no_unit> prod_area{this};                    // x[no_unit],y[no_unit]
    rw<unit, 302, int, no_unit, no_unit> initial_state{this};                             // x[no_unit],y[no_unit]
    rw<unit, 303, int, no_unit, no_unit> penstock{this};                                  // x[no_unit],y[no_unit]
    rw<unit, 304, int, no_unit, no_unit> separate_droop{this};                            // x[no_unit],y[no_unit]
    rw<unit, 305, double, mw, mw> p_min{this};                                            // x[mw],y[mw]
    rw<unit, 306, double, mw, mw> p_max{this};                                            // x[mw],y[mw]
    rw<unit, 307, double, mw, mw> p_nom{this};                                            // x[mw],y[mw]
    rw<unit, 308, typename A::_xy, mw, percent> gen_eff_curve{this};                      // x[mw],y[percent]
    rw<unit, 309, std::vector<typename A::_xy>, m3_per_s, percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<unit, 310, int, no_unit, no_unit> affinity_eq_flag{this};                          // x[no_unit],y[no_unit]
    rw<unit, 311, typename A::_txy, no_unit, no_unit> maintenance_flag{this};             // x[no_unit],y[no_unit]
    rw<unit, 312, typename A::_txy, no_unit, nok> startcost{this};                        // x[no_unit],y[nok]
    rw<unit, 313, typename A::_txy, no_unit, nok> stopcost{this};                         // x[no_unit],y[nok]
    rw<unit, 314, typename A::_xy, m3_per_s, nok_per_h_per_m3_per_s> discharge_cost_curve{
      this};                                                                    // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<unit, 315, typename A::_txy, no_unit, no_unit> priority{this};           // x[no_unit],y[no_unit]
    rw<unit, 316, typename A::_txy, no_unit, no_unit> committed_in{this};       // x[no_unit],y[no_unit]
    rw<unit, 317, typename A::_txy, no_unit, no_unit> committed_flag{this};     // x[no_unit],y[no_unit]
    rw<unit, 318, typename A::_txy, no_unit, mw> min_p_constr{this};            // x[no_unit],y[mw]
    rw<unit, 319, typename A::_txy, no_unit, no_unit> min_p_constr_flag{this};  // x[no_unit],y[no_unit]
    rw<unit, 320, typename A::_txy, no_unit, mw> max_p_constr{this};            // x[no_unit],y[mw]
    rw<unit, 321, typename A::_txy, no_unit, no_unit> max_p_constr_flag{this};  // x[no_unit],y[no_unit]
    rw<unit, 322, typename A::_txy, no_unit, m3_per_s> min_q_constr{this};      // x[no_unit],y[m3_per_s]
    rw<unit, 323, typename A::_txy, no_unit, no_unit> min_q_constr_flag{this};  // x[no_unit],y[no_unit]
    rw<unit, 324, typename A::_txy, no_unit, m3_per_s> max_q_constr{this};      // x[no_unit],y[m3_per_s]
    rw<unit, 325, typename A::_txy, no_unit, no_unit> max_q_constr_flag{this};  // x[no_unit],y[no_unit]
    rw<unit, 326, typename A::_xy, meter, m3_per_s> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<unit, 327, typename A::_txy, no_unit, meter> upstream_min{this};         // x[no_unit],y[meter]
    rw<unit, 328, typename A::_txy, no_unit, no_unit> upstream_min_flag{this};  // x[no_unit],y[no_unit]
    rw<unit, 329, typename A::_txy, no_unit, meter> downstream_max{this};       // x[no_unit],y[meter]
    rw<unit, 330, typename A::_txy, no_unit, no_unit> downstream_max_flag{this};               // x[no_unit],y[no_unit]
    rw<unit, 331, typename A::_txy, no_unit, mw> production_schedule{this};                    // x[no_unit],y[mw]
    rw<unit, 332, typename A::_txy, no_unit, no_unit> production_schedule_flag{this};          // x[no_unit],y[no_unit]
    rw<unit, 333, typename A::_txy, no_unit, m3_per_s> discharge_schedule{this};               // x[no_unit],y[m3_per_s]
    rw<unit, 334, typename A::_txy, no_unit, no_unit> discharge_schedule_flag{this};           // x[no_unit],y[no_unit]
    rw<unit, 335, typename A::_txy, no_unit, no_unit> fcr_mip_flag{this};                      // x[no_unit],y[no_unit]
    rw<unit, 336, typename A::_txy, no_unit, mw> p_fcr_min{this};                              // x[no_unit],y[mw]
    rw<unit, 337, typename A::_txy, no_unit, mw> p_fcr_max{this};                              // x[no_unit],y[mw]
    rw<unit, 338, typename A::_txy, no_unit, mw> p_frr_min{this};                              // x[no_unit],y[mw]
    rw<unit, 339, typename A::_txy, no_unit, mw> p_frr_max{this};                              // x[no_unit],y[mw]
    rw<unit, 340, typename A::_txy, no_unit, mw> p_rr_min{this};                               // x[no_unit],y[mw]
    rw<unit, 341, typename A::_txy, no_unit, mw> frr_up_min{this};                             // x[no_unit],y[mw]
    rw<unit, 342, typename A::_txy, no_unit, mw> frr_up_max{this};                             // x[no_unit],y[mw]
    rw<unit, 343, typename A::_txy, no_unit, mw> frr_down_min{this};                           // x[no_unit],y[mw]
    rw<unit, 344, typename A::_txy, no_unit, mw> frr_down_max{this};                           // x[no_unit],y[mw]
    rw<unit, 345, typename A::_txy, no_unit, mw> fcr_n_up_min{this};                           // x[no_unit],y[mw]
    rw<unit, 346, typename A::_txy, no_unit, mw> fcr_n_up_max{this};                           // x[no_unit],y[mw]
    rw<unit, 347, typename A::_txy, no_unit, mw> fcr_n_down_min{this};                         // x[no_unit],y[mw]
    rw<unit, 348, typename A::_txy, no_unit, mw> fcr_n_down_max{this};                         // x[no_unit],y[mw]
    rw<unit, 349, typename A::_txy, no_unit, mw> fcr_d_up_min{this};                           // x[no_unit],y[mw]
    rw<unit, 350, typename A::_txy, no_unit, mw> fcr_d_up_max{this};                           // x[no_unit],y[mw]
    rw<unit, 351, typename A::_txy, no_unit, mw> fcr_d_down_min{this};                         // x[no_unit],y[mw]
    rw<unit, 352, typename A::_txy, no_unit, mw> fcr_d_down_max{this};                         // x[no_unit],y[mw]
    rw<unit, 353, typename A::_txy, no_unit, mw> rr_up_min{this};                              // x[no_unit],y[mw]
    rw<unit, 354, typename A::_txy, no_unit, mw> rr_up_max{this};                              // x[no_unit],y[mw]
    rw<unit, 355, typename A::_txy, no_unit, mw> rr_down_min{this};                            // x[no_unit],y[mw]
    rw<unit, 356, typename A::_txy, no_unit, mw> rr_down_max{this};                            // x[no_unit],y[mw]
    rw<unit, 357, typename A::_txy, no_unit, mw> fcr_n_up_schedule{this};                      // x[no_unit],y[mw]
    rw<unit, 358, typename A::_txy, no_unit, no_unit> fcr_n_up_schedule_flag{this};            // x[no_unit],y[no_unit]
    rw<unit, 359, typename A::_txy, no_unit, mw> fcr_n_down_schedule{this};                    // x[no_unit],y[mw]
    rw<unit, 360, typename A::_txy, no_unit, no_unit> fcr_n_down_schedule_flag{this};          // x[no_unit],y[no_unit]
    rw<unit, 361, typename A::_txy, no_unit, mw> fcr_d_up_schedule{this};                      // x[no_unit],y[mw]
    rw<unit, 362, typename A::_txy, no_unit, no_unit> fcr_d_up_schedule_flag{this};            // x[no_unit],y[no_unit]
    rw<unit, 363, typename A::_txy, no_unit, mw> fcr_d_down_schedule{this};                    // x[no_unit],y[mw]
    rw<unit, 364, typename A::_txy, no_unit, no_unit> fcr_d_down_schedule_flag{this};          // x[no_unit],y[no_unit]
    rw<unit, 365, typename A::_txy, no_unit, mw> frr_up_schedule{this};                        // x[no_unit],y[mw]
    rw<unit, 366, typename A::_txy, no_unit, no_unit> frr_up_schedule_flag{this};              // x[no_unit],y[no_unit]
    rw<unit, 367, typename A::_txy, no_unit, mw> frr_down_schedule{this};                      // x[no_unit],y[mw]
    rw<unit, 368, typename A::_txy, no_unit, no_unit> frr_down_schedule_flag{this};            // x[no_unit],y[no_unit]
    rw<unit, 369, typename A::_txy, no_unit, mw> rr_up_schedule{this};                         // x[no_unit],y[mw]
    rw<unit, 370, typename A::_txy, no_unit, no_unit> rr_up_schedule_flag{this};               // x[no_unit],y[no_unit]
    rw<unit, 371, typename A::_txy, no_unit, mw> rr_down_schedule{this};                       // x[no_unit],y[mw]
    rw<unit, 372, typename A::_txy, no_unit, no_unit> rr_down_schedule_flag{this};             // x[no_unit],y[no_unit]
    rw<unit, 373, typename A::_txy, no_unit, nok> droop_cost{this};                            // x[no_unit],y[nok]
    rw<unit, 374, typename A::_txy, no_unit, no_unit> fixed_droop{this};                       // x[no_unit],y[no_unit]
    rw<unit, 375, typename A::_txy, no_unit, no_unit> fixed_droop_flag{this};                  // x[no_unit],y[no_unit]
    rw<unit, 376, typename A::_txy, no_unit, no_unit> droop_min{this};                         // x[no_unit],y[no_unit]
    rw<unit, 377, typename A::_txy, no_unit, no_unit> droop_max{this};                         // x[no_unit],y[no_unit]
    rw<unit, 378, std::vector<double>, no_unit, no_unit> discrete_droop_values{this};          // x[no_unit],y[no_unit]
    rw<unit, 379, std::vector<double>, no_unit, no_unit> fcr_n_discrete_droop_values{this};    // x[no_unit],y[no_unit]
    rw<unit, 380, std::vector<double>, no_unit, no_unit> fcr_d_up_discrete_droop_values{this}; // x[no_unit],y[no_unit]
    rw<unit, 381, std::vector<double>, no_unit, no_unit> fcr_d_down_discrete_droop_values{
      this};                                                                                 // x[no_unit],y[no_unit]
    rw<unit, 382, typename A::_txy, no_unit, nok> reserve_ramping_cost_up{this};             // x[no_unit],y[nok]
    rw<unit, 383, typename A::_txy, no_unit, nok> reserve_ramping_cost_down{this};           // x[no_unit],y[nok]
    rw<unit, 384, typename A::_txy, no_unit, mw> ref_production{this};                       // x[no_unit],y[mw]
    rw<unit, 385, typename A::_txy, no_unit, no_unit> schedule_deviation_flag{this};         // x[no_unit],y[no_unit]
    rw<unit, 386, typename A::_txy, no_unit, no_unit> gen_turn_off_limit{this};              // x[no_unit],y[no_unit]
    rw<unit, 387, typename A::_txy, no_unit, nok_per_mw> fcr_n_up_cost{this};                // x[no_unit],y[nok_per_mw]
    rw<unit, 388, typename A::_txy, no_unit, nok_per_mw> fcr_n_down_cost{this};              // x[no_unit],y[nok_per_mw]
    rw<unit, 389, typename A::_txy, no_unit, nok_per_mw> fcr_d_up_cost{this};                // x[no_unit],y[nok_per_mw]
    rw<unit, 390, typename A::_txy, no_unit, nok_per_mw> fcr_d_down_cost{this};              // x[no_unit],y[nok_per_mw]
    rw<unit, 391, typename A::_txy, no_unit, nok_per_mw> frr_up_cost{this};                  // x[no_unit],y[nok_per_mw]
    rw<unit, 392, typename A::_txy, no_unit, nok_per_mw> frr_down_cost{this};                // x[no_unit],y[nok_per_mw]
    rw<unit, 393, typename A::_txy, no_unit, nok_per_mw> rr_up_cost{this};                   // x[no_unit],y[nok_per_mw]
    rw<unit, 394, typename A::_txy, no_unit, nok_per_mw> rr_down_cost{this};                 // x[no_unit],y[nok_per_mw]
    rw<unit, 395, typename A::_txy, no_unit, mw> spinning_reserve_up_max{this};              // x[no_unit],y[mw]
    rw<unit, 396, typename A::_txy, no_unit, mw> spinning_reserve_down_max{this};            // x[no_unit],y[mw]
    rw<unit, 397, typename A::_txy, no_unit, nok> fcr_n_droop_cost{this};                    // x[no_unit],y[nok]
    rw<unit, 398, typename A::_txy, no_unit, nok> fcr_d_up_droop_cost{this};                 // x[no_unit],y[nok]
    rw<unit, 399, typename A::_txy, no_unit, nok> fcr_d_down_droop_cost{this};               // x[no_unit],y[nok]
    rw<unit, 400, typename A::_txy, no_unit, no_unit> fcr_n_fixed_droop{this};               // x[no_unit],y[no_unit]
    rw<unit, 401, typename A::_txy, no_unit, no_unit> fcr_d_up_fixed_droop{this};            // x[no_unit],y[no_unit]
    rw<unit, 402, typename A::_txy, no_unit, no_unit> fcr_d_down_fixed_droop{this};          // x[no_unit],y[no_unit]
    rw<unit, 403, typename A::_txy, no_unit, no_unit> fcr_n_droop_min{this};                 // x[no_unit],y[no_unit]
    rw<unit, 404, typename A::_txy, no_unit, no_unit> fcr_d_up_droop_min{this};              // x[no_unit],y[no_unit]
    rw<unit, 405, typename A::_txy, no_unit, no_unit> fcr_d_down_droop_min{this};            // x[no_unit],y[no_unit]
    rw<unit, 406, typename A::_txy, no_unit, no_unit> fcr_n_droop_max{this};                 // x[no_unit],y[no_unit]
    rw<unit, 407, typename A::_txy, no_unit, no_unit> fcr_d_up_droop_max{this};              // x[no_unit],y[no_unit]
    rw<unit, 408, typename A::_txy, no_unit, no_unit> fcr_d_down_droop_max{this};            // x[no_unit],y[no_unit]
    rw<unit, 409, typename A::_txy, no_unit, no_unit> couple_fcr_delivery_flag{this};        // x[no_unit],y[no_unit]
    rw<unit, 410, typename A::_txy, no_unit, mw> historical_production{this};                // x[no_unit],y[mw]
    rw<unit, 411, int, minute, minute> min_uptime{this};                                     // x[minute],y[minute]
    rw<unit, 412, int, minute, minute> min_downtime{this};                                   // x[minute],y[minute]
    rw<unit, 413, typename A::_txy, no_unit, no_unit> min_uptime_flag{this};                 // x[no_unit],y[no_unit]
    rw<unit, 414, typename A::_txy, no_unit, no_unit> min_downtime_flag{this};               // x[no_unit],y[no_unit]
    ro<unit, 415, typename A::_txy, no_unit, meter> eff_head{this};                          // x[no_unit],y[meter]
    ro<unit, 416, typename A::_txy, no_unit, meter> sim_eff_head{this};                      // x[no_unit],y[meter]
    ro<unit, 417, typename A::_txy, no_unit, meter> head_loss{this};                         // x[no_unit],y[meter]
    ro<unit, 418, typename A::_txy, no_unit, mw> production{this};                           // x[no_unit],y[mw]
    ro<unit, 419, typename A::_txy, no_unit, mw> solver_production{this};                    // x[no_unit],y[mw]
    ro<unit, 420, typename A::_txy, no_unit, mw> sim_production{this};                       // x[no_unit],y[mw]
    ro<unit, 421, typename A::_txy, no_unit, m3_per_s> discharge{this};                      // x[no_unit],y[m3_per_s]
    ro<unit, 422, typename A::_txy, no_unit, m3_per_s> solver_discharge{this};               // x[no_unit],y[m3_per_s]
    ro<unit, 423, typename A::_txy, no_unit, m3_per_s> sim_discharge{this};                  // x[no_unit],y[m3_per_s]
    ro<unit, 424, typename A::_txy, no_unit, no_unit> committed_out{this};                   // x[no_unit],y[no_unit]
    ro<unit, 425, typename A::_txy, no_unit, mw> production_schedule_penalty{this};          // x[no_unit],y[mw]
    ro<unit, 426, typename A::_txy, no_unit, mm3> discharge_schedule_penalty{this};          // x[no_unit],y[mm3]
    ro<unit, 427, typename A::_txy, no_unit, mw> fcr_n_up_delivery{this};                    // x[no_unit],y[mw]
    ro<unit, 428, typename A::_txy, no_unit, mw> fcr_n_down_delivery{this};                  // x[no_unit],y[mw]
    ro<unit, 429, typename A::_txy, no_unit, mw> fcr_d_up_delivery{this};                    // x[no_unit],y[mw]
    ro<unit, 430, typename A::_txy, no_unit, mw> fcr_d_down_delivery{this};                  // x[no_unit],y[mw]
    ro<unit, 431, typename A::_txy, no_unit, mw> frr_up_delivery{this};                      // x[no_unit],y[mw]
    ro<unit, 432, typename A::_txy, no_unit, mw> frr_down_delivery{this};                    // x[no_unit],y[mw]
    ro<unit, 433, typename A::_txy, no_unit, mw> rr_up_delivery{this};                       // x[no_unit],y[mw]
    ro<unit, 434, typename A::_txy, no_unit, mw> rr_down_delivery{this};                     // x[no_unit],y[mw]
    ro<unit, 435, typename A::_txy, no_unit, mw> fcr_n_up_delivery_physical{this};           // x[no_unit],y[mw]
    ro<unit, 436, typename A::_txy, no_unit, mw> fcr_n_down_delivery_physical{this};         // x[no_unit],y[mw]
    ro<unit, 437, typename A::_txy, no_unit, mw> fcr_d_up_delivery_physical{this};           // x[no_unit],y[mw]
    ro<unit, 438, typename A::_txy, no_unit, mw> fcr_d_down_delivery_physical{this};         // x[no_unit],y[mw]
    ro<unit, 439, typename A::_txy, no_unit, mw> fcr_n_up_schedule_penalty{this};            // x[no_unit],y[mw]
    ro<unit, 440, typename A::_txy, no_unit, mw> fcr_n_down_schedule_penalty{this};          // x[no_unit],y[mw]
    ro<unit, 441, typename A::_txy, no_unit, mw> fcr_d_up_schedule_penalty{this};            // x[no_unit],y[mw]
    ro<unit, 442, typename A::_txy, no_unit, mw> fcr_d_down_schedule_penalty{this};          // x[no_unit],y[mw]
    ro<unit, 443, typename A::_txy, no_unit, mw> frr_up_schedule_penalty{this};              // x[no_unit],y[mw]
    ro<unit, 444, typename A::_txy, no_unit, mw> frr_down_schedule_penalty{this};            // x[no_unit],y[mw]
    ro<unit, 445, typename A::_txy, no_unit, mw> rr_up_schedule_penalty{this};               // x[no_unit],y[mw]
    ro<unit, 446, typename A::_txy, no_unit, mw> rr_down_schedule_penalty{this};             // x[no_unit],y[mw]
    ro<unit, 447, typename A::_txy, no_unit, no_unit> droop_result{this};                    // x[no_unit],y[no_unit]
    ro<unit, 448, std::map<int64_t, typename A::_xy>, mw, m3_per_s> best_profit_q{this};     // x[mw],y[m3_per_s]
    ro<unit, 449, std::map<int64_t, typename A::_xy>, mw, mw> best_profit_p{this};           // x[mw],y[mw]
    ro<unit, 450, std::map<int64_t, typename A::_xy>, mw, m3_per_s> best_profit_dq_dp{this}; // x[mw],y[m3_per_s]
    ro<unit, 451, std::map<int64_t, typename A::_xy>, mw, no_unit> best_profit_needle_comb{this}; // x[mw],y[no_unit]
    ro<unit, 452, typename A::_txy, no_unit, nok> startup_cost_mip_objective{this};               // x[no_unit],y[nok]
    ro<unit, 453, typename A::_txy, no_unit, nok> startup_cost_total_objective{this};             // x[no_unit],y[nok]
    ro<unit, 454, typename A::_txy, no_unit, nok> discharge_fee_objective{this};                  // x[no_unit],y[nok]
    ro<unit, 455, typename A::_txy, no_unit, nok> feeding_fee_objective{this};                    // x[no_unit],y[nok]
    ro<unit, 456, typename A::_txy, no_unit, nok> schedule_penalty{this};                         // x[no_unit],y[nok]
    ro<unit, 457, typename A::_txy, no_unit, nok> market_income{this};                            // x[no_unit],y[nok]
    ro<unit, 458, std::map<int64_t, typename A::_xy>, m3_per_s, mw> original_pq_curves{this};     // x[m3_per_s],y[mw]
    ro<unit, 459, std::map<int64_t, typename A::_xy>, m3_per_s, mw> convex_pq_curves{this};       // x[m3_per_s],y[mw]
    ro<unit, 460, std::map<int64_t, typename A::_xy>, m3_per_s, mw> final_pq_curves{this};        // x[m3_per_s],y[mw]
    ro<unit, 461, typename A::_txy, no_unit, mw> max_prod{this};                                  // x[no_unit],y[mw]
    ro<unit, 462, typename A::_txy, no_unit, mw> min_prod{this};                                  // x[no_unit],y[mw]
    ro<unit, 463, typename A::_txy, no_unit, no_unit> fcr_n_droop_result{this};      // x[no_unit],y[no_unit]
    ro<unit, 464, typename A::_txy, no_unit, no_unit> fcr_d_up_droop_result{this};   // x[no_unit],y[no_unit]
    ro<unit, 465, typename A::_txy, no_unit, no_unit> fcr_d_down_droop_result{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct needle_combination : obj<A, 3> {
    using super = obj<A, 3>;
    needle_combination() = default;

    needle_combination(A* s, int oid)
      : super(s, oid) {
    }

    needle_combination(needle_combination const & o)
      : super(o) {
    }

    needle_combination(needle_combination&& o)
      : super(std::move(o)) {
    }

    needle_combination& operator=(needle_combination const & o) {
      super::operator=(o);
      return *this;
    }

    needle_combination& operator=(needle_combination&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<needle_combination, 468, double, mw, mw> p_max{this}; // x[mw],y[mw]
    rw<needle_combination, 469, double, mw, mw> p_min{this}; // x[mw],y[mw]
    rw<needle_combination, 470, double, mw, mw> p_nom{this}; // x[mw],y[mw]
    rw<needle_combination, 471, std::vector<typename A::_xy>, m3_per_s, percent> turb_eff_curves{
      this};                                                                            // x[m3_per_s],y[percent]
    rw<needle_combination, 472, typename A::_txy, no_unit, mw> p_fcr_min{this};         // x[no_unit],y[mw]
    rw<needle_combination, 473, typename A::_txy, no_unit, mw> p_fcr_max{this};         // x[no_unit],y[mw]
    rw<needle_combination, 474, typename A::_txy, no_unit, mw> p_frr_min{this};         // x[no_unit],y[mw]
    rw<needle_combination, 475, typename A::_txy, no_unit, mw> p_frr_max{this};         // x[no_unit],y[mw]
    rw<needle_combination, 476, typename A::_xy, mw, nok_per_mw> production_cost{this}; // x[mw],y[nok_per_mw]
    ro<needle_combination, 477, std::map<int64_t, typename A::_xy>, m3_per_s, mw> original_pq_curves{
      this}; // x[m3_per_s],y[mw]
    ro<needle_combination, 478, std::map<int64_t, typename A::_xy>, m3_per_s, mw> convex_pq_curves{
      this}; // x[m3_per_s],y[mw]
    ro<needle_combination, 479, std::map<int64_t, typename A::_xy>, m3_per_s, mw> final_pq_curves{
      this};                                                                   // x[m3_per_s],y[mw]
    ro<needle_combination, 480, typename A::_txy, no_unit, mw> max_prod{this}; // x[no_unit],y[mw]
    ro<needle_combination, 481, typename A::_txy, no_unit, mw> min_prod{this}; // x[no_unit],y[mw]
  };

  template <class A>
  struct pump : obj<A, 4> {
    using super = obj<A, 4>;
    pump() = default;

    pump(A* s, int oid)
      : super(s, oid) {
    }

    pump(pump const & o)
      : super(o) {
    }

    pump(pump&& o)
      : super(std::move(o)) {
    }

    pump& operator=(pump const & o) {
      super::operator=(o);
      return *this;
    }

    pump& operator=(pump&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<pump, 487, int, no_unit, no_unit> initial_state{this};                               // x[no_unit],y[no_unit]
    rw<pump, 488, int, no_unit, no_unit> penstock{this};                                    // x[no_unit],y[no_unit]
    rw<pump, 489, int, no_unit, no_unit> separate_droop{this};                              // x[no_unit],y[no_unit]
    rw<pump, 490, double, mw, mw> p_min{this};                                              // x[mw],y[mw]
    rw<pump, 491, double, mw, mw> p_max{this};                                              // x[mw],y[mw]
    rw<pump, 492, double, mw, mw> p_nom{this};                                              // x[mw],y[mw]
    rw<pump, 493, typename A::_xy, mw, percent> gen_eff_curve{this};                        // x[mw],y[percent]
    rw<pump, 494, std::vector<typename A::_xy>, m3_per_s, percent> turb_eff_curves{this};   // x[m3_per_s],y[percent]
    rw<pump, 495, typename A::_txy, no_unit, no_unit> maintenance_flag{this};               // x[no_unit],y[no_unit]
    rw<pump, 496, typename A::_txy, no_unit, nok> startcost{this};                          // x[no_unit],y[nok]
    rw<pump, 497, typename A::_txy, no_unit, nok> stopcost{this};                           // x[no_unit],y[nok]
    rw<pump, 498, typename A::_txy, no_unit, no_unit> committed_in{this};                   // x[no_unit],y[no_unit]
    rw<pump, 499, typename A::_txy, no_unit, no_unit> committed_flag{this};                 // x[no_unit],y[no_unit]
    rw<pump, 500, typename A::_txy, no_unit, meter> upstream_max{this};                     // x[no_unit],y[meter]
    rw<pump, 501, typename A::_txy, no_unit, no_unit> upstream_max_flag{this};              // x[no_unit],y[no_unit]
    rw<pump, 502, typename A::_txy, no_unit, meter> downstream_min{this};                   // x[no_unit],y[meter]
    rw<pump, 503, typename A::_txy, no_unit, meter> downstream_min_flag{this};              // x[no_unit],y[meter]
    rw<pump, 504, typename A::_txy, no_unit, mw> consumption_schedule{this};                // x[no_unit],y[mw]
    rw<pump, 505, typename A::_txy, no_unit, no_unit> consumption_schedule_flag{this};      // x[no_unit],y[no_unit]
    rw<pump, 506, typename A::_txy, no_unit, m3_per_s> upflow_schedule{this};               // x[no_unit],y[m3_per_s]
    rw<pump, 507, typename A::_txy, no_unit, no_unit> upflow_schedule_flag{this};           // x[no_unit],y[no_unit]
    rw<pump, 508, typename A::_txy, no_unit, no_unit> fcr_mip_flag{this};                   // x[no_unit],y[no_unit]
    rw<pump, 509, typename A::_txy, no_unit, mw> p_fcr_min{this};                           // x[no_unit],y[mw]
    rw<pump, 510, typename A::_txy, no_unit, mw> p_fcr_max{this};                           // x[no_unit],y[mw]
    rw<pump, 511, typename A::_txy, no_unit, mw> p_rr_min{this};                            // x[no_unit],y[mw]
    rw<pump, 512, typename A::_txy, no_unit, mw> frr_up_min{this};                          // x[no_unit],y[mw]
    rw<pump, 513, typename A::_txy, no_unit, mw> frr_up_max{this};                          // x[no_unit],y[mw]
    rw<pump, 514, typename A::_txy, no_unit, mw> frr_down_min{this};                        // x[no_unit],y[mw]
    rw<pump, 515, typename A::_txy, no_unit, mw> frr_down_max{this};                        // x[no_unit],y[mw]
    rw<pump, 516, typename A::_txy, no_unit, mw> fcr_n_up_min{this};                        // x[no_unit],y[mw]
    rw<pump, 517, typename A::_txy, no_unit, mw> fcr_n_up_max{this};                        // x[no_unit],y[mw]
    rw<pump, 518, typename A::_txy, no_unit, mw> fcr_n_down_min{this};                      // x[no_unit],y[mw]
    rw<pump, 519, typename A::_txy, no_unit, mw> fcr_n_down_max{this};                      // x[no_unit],y[mw]
    rw<pump, 520, typename A::_txy, no_unit, mw> fcr_d_up_min{this};                        // x[no_unit],y[mw]
    rw<pump, 521, typename A::_txy, no_unit, mw> fcr_d_up_max{this};                        // x[no_unit],y[mw]
    rw<pump, 522, typename A::_txy, no_unit, mw> fcr_d_down_min{this};                      // x[no_unit],y[mw]
    rw<pump, 523, typename A::_txy, no_unit, mw> fcr_d_down_max{this};                      // x[no_unit],y[mw]
    rw<pump, 524, typename A::_txy, no_unit, mw> rr_up_min{this};                           // x[no_unit],y[mw]
    rw<pump, 525, typename A::_txy, no_unit, mw> rr_up_max{this};                           // x[no_unit],y[mw]
    rw<pump, 526, typename A::_txy, no_unit, mw> rr_down_min{this};                         // x[no_unit],y[mw]
    rw<pump, 527, typename A::_txy, no_unit, mw> rr_down_max{this};                         // x[no_unit],y[mw]
    rw<pump, 528, typename A::_txy, no_unit, mw> fcr_n_up_schedule{this};                   // x[no_unit],y[mw]
    rw<pump, 529, typename A::_txy, no_unit, no_unit> fcr_n_up_schedule_flag{this};         // x[no_unit],y[no_unit]
    rw<pump, 530, typename A::_txy, no_unit, mw> fcr_n_down_schedule{this};                 // x[no_unit],y[mw]
    rw<pump, 531, typename A::_txy, no_unit, no_unit> fcr_n_down_schedule_flag{this};       // x[no_unit],y[no_unit]
    rw<pump, 532, typename A::_txy, no_unit, mw> fcr_d_up_schedule{this};                   // x[no_unit],y[mw]
    rw<pump, 533, typename A::_txy, no_unit, no_unit> fcr_d_up_schedule_flag{this};         // x[no_unit],y[no_unit]
    rw<pump, 534, typename A::_txy, no_unit, mw> fcr_d_down_schedule{this};                 // x[no_unit],y[mw]
    rw<pump, 535, typename A::_txy, no_unit, no_unit> fcr_d_down_schedule_flag{this};       // x[no_unit],y[no_unit]
    rw<pump, 536, typename A::_txy, no_unit, mw> frr_up_schedule{this};                     // x[no_unit],y[mw]
    rw<pump, 537, typename A::_txy, no_unit, no_unit> frr_up_schedule_flag{this};           // x[no_unit],y[no_unit]
    rw<pump, 538, typename A::_txy, no_unit, mw> frr_down_schedule{this};                   // x[no_unit],y[mw]
    rw<pump, 539, typename A::_txy, no_unit, no_unit> frr_down_schedule_flag{this};         // x[no_unit],y[no_unit]
    rw<pump, 540, typename A::_txy, no_unit, mw> rr_up_schedule{this};                      // x[no_unit],y[mw]
    rw<pump, 541, typename A::_txy, no_unit, no_unit> rr_up_schedule_flag{this};            // x[no_unit],y[no_unit]
    rw<pump, 542, typename A::_txy, no_unit, mw> rr_down_schedule{this};                    // x[no_unit],y[mw]
    rw<pump, 543, typename A::_txy, no_unit, no_unit> rr_down_schedule_flag{this};          // x[no_unit],y[no_unit]
    rw<pump, 544, typename A::_txy, no_unit, nok> droop_cost{this};                         // x[no_unit],y[nok]
    rw<pump, 545, typename A::_txy, no_unit, no_unit> fixed_droop{this};                    // x[no_unit],y[no_unit]
    rw<pump, 546, typename A::_txy, no_unit, no_unit> fixed_droop_flag{this};               // x[no_unit],y[no_unit]
    rw<pump, 547, typename A::_txy, no_unit, no_unit> droop_min{this};                      // x[no_unit],y[no_unit]
    rw<pump, 548, typename A::_txy, no_unit, no_unit> droop_max{this};                      // x[no_unit],y[no_unit]
    rw<pump, 549, typename A::_txy, no_unit, nok_per_mw> reserve_ramping_cost_up{this};     // x[no_unit],y[nok_per_mw]
    rw<pump, 550, typename A::_txy, no_unit, nok_per_mw> reserve_ramping_cost_down{this};   // x[no_unit],y[nok_per_mw]
    rw<pump, 551, std::vector<double>, no_unit, no_unit> discrete_droop_values{this};       // x[no_unit],y[no_unit]
    rw<pump, 552, std::vector<double>, no_unit, no_unit> fcr_n_discrete_droop_values{this}; // x[no_unit],y[no_unit]
    rw<pump, 553, std::vector<double>, no_unit, no_unit> fcr_d_up_discrete_droop_values{this}; // x[no_unit],y[no_unit]
    rw<pump, 554, std::vector<double>, no_unit, no_unit> fcr_d_down_discrete_droop_values{
      this};                                                                         // x[no_unit],y[no_unit]
    rw<pump, 555, typename A::_txy, no_unit, nok_per_mw> fcr_n_up_cost{this};        // x[no_unit],y[nok_per_mw]
    rw<pump, 556, typename A::_txy, no_unit, nok_per_mw> fcr_n_down_cost{this};      // x[no_unit],y[nok_per_mw]
    rw<pump, 557, typename A::_txy, no_unit, nok_per_mw> fcr_d_up_cost{this};        // x[no_unit],y[nok_per_mw]
    rw<pump, 558, typename A::_txy, no_unit, nok_per_mw> fcr_d_down_cost{this};      // x[no_unit],y[nok_per_mw]
    rw<pump, 559, typename A::_txy, no_unit, nok_per_mw> frr_up_cost{this};          // x[no_unit],y[nok_per_mw]
    rw<pump, 560, typename A::_txy, no_unit, nok_per_mw> frr_down_cost{this};        // x[no_unit],y[nok_per_mw]
    rw<pump, 561, typename A::_txy, no_unit, nok_per_mw> rr_up_cost{this};           // x[no_unit],y[nok_per_mw]
    rw<pump, 562, typename A::_txy, no_unit, nok_per_mw> rr_down_cost{this};         // x[no_unit],y[nok_per_mw]
    rw<pump, 563, typename A::_txy, no_unit, mw> spinning_reserve_up_max{this};      // x[no_unit],y[mw]
    rw<pump, 564, typename A::_txy, no_unit, mw> spinning_reserve_down_max{this};    // x[no_unit],y[mw]
    rw<pump, 565, typename A::_txy, no_unit, nok> fcr_n_droop_cost{this};            // x[no_unit],y[nok]
    rw<pump, 566, typename A::_txy, no_unit, nok> fcr_d_up_droop_cost{this};         // x[no_unit],y[nok]
    rw<pump, 567, typename A::_txy, no_unit, nok> fcr_d_down_droop_cost{this};       // x[no_unit],y[nok]
    rw<pump, 568, typename A::_txy, no_unit, no_unit> fcr_n_fixed_droop{this};       // x[no_unit],y[no_unit]
    rw<pump, 569, typename A::_txy, no_unit, no_unit> fcr_d_up_fixed_droop{this};    // x[no_unit],y[no_unit]
    rw<pump, 570, typename A::_txy, no_unit, no_unit> fcr_d_down_fixed_droop{this};  // x[no_unit],y[no_unit]
    rw<pump, 571, typename A::_txy, no_unit, no_unit> fcr_n_droop_min{this};         // x[no_unit],y[no_unit]
    rw<pump, 572, typename A::_txy, no_unit, no_unit> fcr_d_up_droop_min{this};      // x[no_unit],y[no_unit]
    rw<pump, 573, typename A::_txy, no_unit, no_unit> fcr_d_down_droop_min{this};    // x[no_unit],y[no_unit]
    rw<pump, 574, typename A::_txy, no_unit, no_unit> fcr_n_droop_max{this};         // x[no_unit],y[no_unit]
    rw<pump, 575, typename A::_txy, no_unit, no_unit> fcr_d_up_droop_max{this};      // x[no_unit],y[no_unit]
    rw<pump, 576, typename A::_txy, no_unit, no_unit> fcr_d_down_droop_max{this};    // x[no_unit],y[no_unit]
    ro<pump, 577, typename A::_txy, no_unit, meter> eff_head{this};                  // x[no_unit],y[meter]
    ro<pump, 578, typename A::_txy, no_unit, meter> head_loss{this};                 // x[no_unit],y[meter]
    ro<pump, 579, typename A::_txy, no_unit, mw> consumption{this};                  // x[no_unit],y[mw]
    ro<pump, 580, typename A::_txy, no_unit, mw> sim_consumption{this};              // x[no_unit],y[mw]
    ro<pump, 581, typename A::_txy, no_unit, mw> solver_consumption{this};           // x[no_unit],y[mw]
    ro<pump, 582, typename A::_txy, no_unit, m3_per_s> upflow{this};                 // x[no_unit],y[m3_per_s]
    ro<pump, 583, typename A::_txy, no_unit, m3_per_s> sim_upflow{this};             // x[no_unit],y[m3_per_s]
    ro<pump, 584, typename A::_txy, no_unit, m3_per_s> solver_upflow{this};          // x[no_unit],y[m3_per_s]
    ro<pump, 585, typename A::_txy, no_unit, no_unit> committed_out{this};           // x[no_unit],y[no_unit]
    ro<pump, 586, typename A::_txy, no_unit, mw> consumption_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump, 587, typename A::_txy, no_unit, mm3> upflow_schedule_penalty{this};     // x[no_unit],y[mm3]
    ro<pump, 588, typename A::_txy, no_unit, mw> fcr_n_up_delivery{this};            // x[no_unit],y[mw]
    ro<pump, 589, typename A::_txy, no_unit, mw> fcr_n_down_delivery{this};          // x[no_unit],y[mw]
    ro<pump, 590, typename A::_txy, no_unit, mw> fcr_d_up_delivery{this};            // x[no_unit],y[mw]
    ro<pump, 591, typename A::_txy, no_unit, mw> fcr_d_down_delivery{this};          // x[no_unit],y[mw]
    ro<pump, 592, typename A::_txy, no_unit, mw> frr_up_delivery{this};              // x[no_unit],y[mw]
    ro<pump, 593, typename A::_txy, no_unit, mw> frr_down_delivery{this};            // x[no_unit],y[mw]
    ro<pump, 594, typename A::_txy, no_unit, mw> rr_up_delivery{this};               // x[no_unit],y[mw]
    ro<pump, 595, typename A::_txy, no_unit, mw> rr_down_delivery{this};             // x[no_unit],y[mw]
    ro<pump, 596, typename A::_txy, no_unit, mw> fcr_n_up_delivery_physical{this};   // x[no_unit],y[mw]
    ro<pump, 597, typename A::_txy, no_unit, mw> fcr_n_down_delivery_physical{this}; // x[no_unit],y[mw]
    ro<pump, 598, typename A::_txy, no_unit, mw> fcr_d_up_delivery_physical{this};   // x[no_unit],y[mw]
    ro<pump, 599, typename A::_txy, no_unit, mw> fcr_d_down_delivery_physical{this}; // x[no_unit],y[mw]
    ro<pump, 600, typename A::_txy, no_unit, mw> fcr_n_up_schedule_penalty{this};    // x[no_unit],y[mw]
    ro<pump, 601, typename A::_txy, no_unit, mw> fcr_n_down_schedule_penalty{this};  // x[no_unit],y[mw]
    ro<pump, 602, typename A::_txy, no_unit, mw> fcr_d_up_schedule_penalty{this};    // x[no_unit],y[mw]
    ro<pump, 603, typename A::_txy, no_unit, mw> fcr_d_down_schedule_penalty{this};  // x[no_unit],y[mw]
    ro<pump, 604, typename A::_txy, no_unit, mw> frr_up_schedule_penalty{this};      // x[no_unit],y[mw]
    ro<pump, 605, typename A::_txy, no_unit, mw> frr_down_schedule_penalty{this};    // x[no_unit],y[mw]
    ro<pump, 606, typename A::_txy, no_unit, mw> rr_up_schedule_penalty{this};       // x[no_unit],y[mw]
    ro<pump, 607, typename A::_txy, no_unit, mw> rr_down_schedule_penalty{this};     // x[no_unit],y[mw]
    ro<pump, 608, typename A::_txy, no_unit, no_unit> droop_result{this};            // x[no_unit],y[no_unit]
    ro<pump, 609, std::map<int64_t, typename A::_xy>, m3_per_s, mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<pump, 610, std::map<int64_t, typename A::_xy>, m3_per_s, mw> convex_pq_curves{this};   // x[m3_per_s],y[mw]
    ro<pump, 611, std::map<int64_t, typename A::_xy>, m3_per_s, mw> final_pq_curves{this};    // x[m3_per_s],y[mw]
    ro<pump, 612, typename A::_txy, no_unit, mw> max_cons{this};                              // x[no_unit],y[mw]
    ro<pump, 613, typename A::_txy, no_unit, mw> min_cons{this};                              // x[no_unit],y[mw]
    ro<pump, 614, typename A::_txy, no_unit, no_unit> fcr_n_droop_result{this};               // x[no_unit],y[no_unit]
    ro<pump, 615, typename A::_txy, no_unit, no_unit> fcr_d_up_droop_result{this};            // x[no_unit],y[no_unit]
    ro<pump, 616, typename A::_txy, no_unit, no_unit> fcr_d_down_droop_result{this};          // x[no_unit],y[no_unit]
  };

  template <class A>
  struct gate : obj<A, 5> {
    using super = obj<A, 5>;
    gate() = default;

    gate(A* s, int oid)
      : super(s, oid) {
    }

    gate(gate const & o)
      : super(o) {
    }

    gate(gate&& o)
      : super(std::move(o)) {
    }

    gate& operator=(gate const & o) {
      super::operator=(o);
      return *this;
    }

    gate& operator=(gate&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
      connection_spill,
      connection_bypass,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      case relation::connection_spill:
        return "connection_spill";
      case relation::connection_bypass:
        return "connection_bypass";
      default:
        return nullptr;
      }
    }

    ro<gate, 619, int, no_unit, no_unit> type{this};                     // x[no_unit],y[no_unit]
    rw<gate, 621, int, no_unit, no_unit> time_delay{this};               // x[no_unit],y[no_unit]
    rw<gate, 622, int, no_unit, no_unit> add_slack{this};                // x[no_unit],y[no_unit]
    rw<gate, 623, double, m3_per_s, m3_per_s> max_discharge{this};       // x[m3_per_s],y[m3_per_s]
    rw<gate, 626, double, no_unit, no_unit> lin_rel_a{this};             // x[no_unit],y[no_unit]
    rw<gate, 627, double, mm3, mm3> lin_rel_b{this};                     // x[mm3],y[mm3]
    rw<gate, 628, typename A::_xy, hour, no_unit> shape_discharge{this}; // x[hour],y[no_unit]
    rw<gate, 629, typename A::_xy, m3_per_s, nok_per_m3_per_s> spill_cost_curve{
      this}; // x[m3_per_s],y[nok_per_m3_per_s]
    rw<gate, 630, typename A::_xy, m3_per_s, nok_per_m3_per_s> peak_flow_cost_curve{
      this};                                                               // x[m3_per_s],y[nok_per_m3_per_s]
    ro<gate, 631, typename A::_txy, no_unit, nok> peak_flow_penalty{this}; // x[no_unit],y[nok]
    rw<gate, 632, std::vector<typename A::_xy>, meter, m3_per_s> functions_meter_m3s{this}; // x[meter],y[m3_per_s]
    rw<gate, 633, std::vector<typename A::_xy>, delta_meter, m3_per_s> functions_deltameter_m3s{
      this};                                                                        // x[delta_meter],y[m3_per_s]
    rw<gate, 634, typename A::_txy, no_unit, m3_per_s> min_flow{this};              // x[no_unit],y[m3_per_s]
    rw<gate, 635, typename A::_txy, no_unit, no_unit> min_flow_flag{this};          // x[no_unit],y[no_unit]
    rw<gate, 636, typename A::_txy, no_unit, m3_per_s> max_flow{this};              // x[no_unit],y[m3_per_s]
    rw<gate, 637, typename A::_txy, no_unit, no_unit> max_flow_flag{this};          // x[no_unit],y[no_unit]
    rw<gate, 638, typename A::_txy, no_unit, m3_per_s> schedule_m3s{this};          // x[no_unit],y[m3_per_s]
    rw<gate, 639, typename A::_txy, no_unit, percent> schedule_percent{this};       // x[no_unit],y[percent]
    rw<gate, 640, typename A::_txy, no_unit, no_unit> schedule_flag{this};          // x[no_unit],y[no_unit]
    rw<gate, 641, typename A::_txy, no_unit, no_unit> setting{this};                // x[no_unit],y[no_unit]
    rw<gate, 642, typename A::_txy, no_unit, no_unit> setting_flag{this};           // x[no_unit],y[no_unit]
    rw<gate, 643, typename A::_txy, no_unit, nok_per_mm3> discharge_fee{this};      // x[no_unit],y[nok_per_mm3]
    rw<gate, 644, typename A::_txy, no_unit, no_unit> discharge_fee_flag{this};     // x[no_unit],y[no_unit]
    rw<gate, 645, typename A::_txy, no_unit, m3_per_s> block_merge_tolerance{this}; // x[no_unit],y[m3_per_s]
    rw<gate, 646, typename A::_txy, no_unit, no_unit> min_q_penalty_flag{this};     // x[no_unit],y[no_unit]
    rw<gate, 647, typename A::_txy, no_unit, no_unit> max_q_penalty_flag{this};     // x[no_unit],y[no_unit]
    rw<gate, 648, typename A::_txy, no_unit, m3sec_hour> ramping_up{this};          // x[no_unit],y[m3sec_hour]
    rw<gate, 649, typename A::_txy, no_unit, no_unit> ramping_up_flag{this};        // x[no_unit],y[no_unit]
    rw<gate, 650, typename A::_txy, no_unit, m3sec_hour> ramping_down{this};        // x[no_unit],y[m3sec_hour]
    rw<gate, 651, typename A::_txy, no_unit, no_unit> ramping_down_flag{this};      // x[no_unit],y[no_unit]
    rw<gate, 652, typename A::_txy, no_unit, nok_per_m3_per_s> ramp_penalty_cost{
      this};                                                                         // x[no_unit],y[nok_per_m3_per_s]
    rw<gate, 653, typename A::_txy, no_unit, no_unit> ramp_penalty_cost_flag{this};  // x[no_unit],y[no_unit]
    rw<gate, 654, typename A::_txy, no_unit, nok_per_mm3> max_q_penalty_cost{this};  // x[no_unit],y[nok_per_mm3]
    rw<gate, 655, typename A::_txy, no_unit, nok_per_mm3> min_q_penalty_cost{this};  // x[no_unit],y[nok_per_mm3]
    rw<gate, 656, typename A::_txy, no_unit, no_unit> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<gate, 657, typename A::_txy, no_unit, no_unit> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    ro<gate, 658, typename A::_txy, no_unit, nok> min_q_penalty{this};               // x[no_unit],y[nok]
    ro<gate, 659, typename A::_txy, no_unit, nok> max_q_penalty{this};               // x[no_unit],y[nok]
    ro<gate, 660, typename A::_txy, no_unit, m3_per_s> discharge{this};              // x[no_unit],y[m3_per_s]
    ro<gate, 661, typename A::_txy, no_unit, m3_per_s> sim_discharge{this};          // x[no_unit],y[m3_per_s]
  };

  template <class A>
  struct thermal : obj<A, 6> {
    using super = obj<A, 6>;
    thermal() = default;

    thermal(A* s, int oid)
      : super(s, oid) {
    }

    thermal(thermal const & o)
      : super(o) {
    }

    thermal(thermal&& o)
      : super(std::move(o)) {
    }

    thermal& operator=(thermal const & o) {
      super::operator=(o);
      return *this;
    }

    thermal& operator=(thermal&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<thermal, 1238, typename A::_txy, nok_per_mwh, nok_per_mwh> fuel_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<thermal, 1239, typename A::_txy, nok_per_mwh, nok_per_mwh> quadratic_fuel_cost{
      this};                                                       // x[nok_per_mwh],y[nok_per_mwh]
    rw<thermal, 1240, int, no_unit, no_unit> n_segments{this};     // x[no_unit],y[no_unit]
    rw<thermal, 1241, typename A::_txy, mw, mw> min_prod{this};    // x[mw],y[mw]
    rw<thermal, 1242, typename A::_txy, mw, mw> max_prod{this};    // x[mw],y[mw]
    rw<thermal, 1243, typename A::_txy, nok, nok> startcost{this}; // x[nok],y[nok]
    rw<thermal, 1244, typename A::_txy, nok, nok> stopcost{this};  // x[nok],y[nok]
    ro<thermal, 1245, typename A::_txy, mw, mw> production{this};  // x[mw],y[mw]
  };

  template <class A>
  struct junction : obj<A, 7> {
    using super = obj<A, 7>;
    junction() = default;

    junction(A* s, int oid)
      : super(s, oid) {
    }

    junction(junction const & o)
      : super(o) {
    }

    junction(junction&& o)
      : super(std::move(o)) {
    }

    junction& operator=(junction const & o) {
      super::operator=(o);
      return *this;
    }

    junction& operator=(junction&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<junction, 675, int, no_unit, no_unit> junc_slack{this};                      // x[no_unit],y[no_unit]
    rw<junction, 676, double, meter, meter> altitude{this};                         // x[meter],y[meter]
    ro<junction, 677, typename A::_txy, no_unit, m3_per_s> tunnel_flow_1{this};     // x[no_unit],y[m3_per_s]
    ro<junction, 678, typename A::_txy, no_unit, m3_per_s> tunnel_flow_2{this};     // x[no_unit],y[m3_per_s]
    ro<junction, 679, typename A::_txy, no_unit, m3_per_s> sim_tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction, 680, typename A::_txy, no_unit, m3_per_s> sim_tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    rw<junction, 681, double, s2_per_m5, s2_per_m5> loss_factor_1{this};            // x[s2_per_m5],y[s2_per_m5]
    rw<junction, 682, double, s2_per_m5, s2_per_m5> loss_factor_2{this};            // x[s2_per_m5],y[s2_per_m5]
    rw<junction, 683, typename A::_txy, no_unit, meter> min_pressure{this};         // x[no_unit],y[meter]
    ro<junction, 684, typename A::_txy, no_unit, meter> pressure_height{this};      // x[no_unit],y[meter]
    ro<junction, 685, typename A::_txy, no_unit, meter> sim_pressure_height{this};  // x[no_unit],y[meter]
    ro<junction, 686, typename A::_txy, no_unit, nok> incr_cost{this};              // x[no_unit],y[nok]
    ro<junction, 687, typename A::_txy, no_unit, nok> local_incr_cost{this};        // x[no_unit],y[nok]
  };

  template <class A>
  struct junction_gate : obj<A, 8> {
    using super = obj<A, 8>;
    junction_gate() = default;

    junction_gate(A* s, int oid)
      : super(s, oid) {
    }

    junction_gate(junction_gate const & o)
      : super(o) {
    }

    junction_gate(junction_gate&& o)
      : super(std::move(o)) {
    }

    junction_gate& operator=(junction_gate const & o) {
      super::operator=(o);
      return *this;
    }

    junction_gate& operator=(junction_gate&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<junction_gate, 664, int, no_unit, no_unit> add_slack{this};                   // x[no_unit],y[no_unit]
    rw<junction_gate, 665, double, meter, meter> height_1{this};                     // x[meter],y[meter]
    rw<junction_gate, 666, double, s2_per_m5, s2_per_m5> loss_factor_1{this};        // x[s2_per_m5],y[s2_per_m5]
    rw<junction_gate, 667, double, s2_per_m5, s2_per_m5> loss_factor_2{this};        // x[s2_per_m5],y[s2_per_m5]
    rw<junction_gate, 668, typename A::_txy, no_unit, no_unit> schedule{this};       // x[no_unit],y[no_unit]
    ro<junction_gate, 669, typename A::_txy, no_unit, meter> pressure_height{this};  // x[no_unit],y[meter]
    ro<junction_gate, 670, typename A::_txy, no_unit, m3_per_s> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate, 671, typename A::_txy, no_unit, m3_per_s> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
  };

  template <class A>
  struct creek_intake : obj<A, 9> {
    using super = obj<A, 9>;
    creek_intake() = default;

    creek_intake(A* s, int oid)
      : super(s, oid) {
    }

    creek_intake(creek_intake const & o)
      : super(o) {
    }

    creek_intake(creek_intake&& o)
      : super(std::move(o)) {
    }

    creek_intake& operator=(creek_intake const & o) {
      super::operator=(o);
      return *this;
    }

    creek_intake& operator=(creek_intake&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
      connection_spill,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      case relation::connection_spill:
        return "connection_spill";
      default:
        return nullptr;
      }
    }

    rw<creek_intake, 690, double, meter, meter> net_head{this};                          // x[meter],y[meter]
    rw<creek_intake, 691, double, m3_per_s, m3_per_s> max_inflow{this};                  // x[m3_per_s],y[m3_per_s]
    rw<creek_intake, 692, typename A::_txy, no_unit, m3_per_s> max_inflow_dynamic{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake, 693, typename A::_txy, no_unit, m3_per_s> inflow{this};             // x[no_unit],y[m3_per_s]
    ro<creek_intake, 694, typename A::_txy, no_unit, m3_per_s> sim_inflow{this};         // x[no_unit],y[m3_per_s]
    ro<creek_intake, 695, typename A::_txy, no_unit, meter> sim_pressure_height{this};   // x[no_unit],y[meter]
    rw<creek_intake, 696, typename A::_txy, no_unit, m3_per_s> inflow_percentage{this};  // x[no_unit],y[m3_per_s]
    rw<creek_intake, 697, typename A::_txy, no_unit, nok_per_mm3> overflow_cost{this};   // x[no_unit],y[nok_per_mm3]
    ro<creek_intake, 698, typename A::_txy, no_unit, no_unit> non_physical_overflow_flag{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct contract : obj<A, 10> {
    using super = obj<A, 10>;
    contract() = default;

    contract(A* s, int oid)
      : super(s, oid) {
    }

    contract(contract const & o)
      : super(o) {
    }

    contract(contract&& o)
      : super(std::move(o)) {
    }

    contract& operator=(contract const & o) {
      super::operator=(o);
      return *this;
    }

    contract& operator=(contract&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<contract, 1325, double, mw, mw> initial_trade{this};                                    // x[mw],y[mw]
    rw<contract, 1326, std::map<int64_t, typename A::_xy>, mw, nok_per_mwh> trade_curve{this}; // x[mw],y[nok_per_mwh]
    rw<contract, 1327, typename A::_txy, no_unit, mw> min_trade{this};                         // x[no_unit],y[mw]
    rw<contract, 1328, typename A::_txy, no_unit, mw> max_trade{this};                         // x[no_unit],y[mw]
    rw<contract, 1329, typename A::_txy, no_unit, mw_hour> ramping_up{this};                   // x[no_unit],y[mw_hour]
    rw<contract, 1330, typename A::_txy, no_unit, mw_hour> ramping_down{this};                 // x[no_unit],y[mw_hour]
    rw<contract, 1331, typename A::_txy, no_unit, nok_per_mw> ramping_up_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<contract, 1332, typename A::_txy, no_unit, nok_per_mw> ramping_down_penalty_cost{
      this};                                                                       // x[no_unit],y[nok_per_mw]
    ro<contract, 1333, typename A::_txy, no_unit, mw> trade{this};                 // x[no_unit],y[mw]
    ro<contract, 1334, typename A::_txy, no_unit, nok> ramping_up_penalty{this};   // x[no_unit],y[nok]
    ro<contract, 1335, typename A::_txy, no_unit, nok> ramping_down_penalty{this}; // x[no_unit],y[nok]
  };

  template <class A>
  struct network : obj<A, 11> {
    using super = obj<A, 11>;
    network() = default;

    network(A* s, int oid)
      : super(s, oid) {
    }

    network(network const & o)
      : super(o) {
    }

    network(network&& o)
      : super(std::move(o)) {
    }

    network& operator=(network const & o) {
      super::operator=(o);
      return *this;
    }

    network& operator=(network&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
  };

  template <class A>
  struct market : obj<A, 12> {
    using super = obj<A, 12>;
    market() = default;

    market(A* s, int oid)
      : super(s, oid) {
    }

    market(market const & o)
      : super(o) {
    }

    market(market&& o)
      : super(std::move(o)) {
    }

    market& operator=(market const & o) {
      super::operator=(o);
      return *this;
    }

    market& operator=(market&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<market, 699, int, no_unit, no_unit> prod_area{this};                          // x[no_unit],y[no_unit]
    rw<market, 700, std::string, no_unit, no_unit> market_type{this};                // x[no_unit],y[no_unit]
    rw<market, 701, typename A::_txy, no_unit, mw> load{this};                       // x[no_unit],y[mw]
    rw<market, 702, typename A::_txy, no_unit, mw> max_buy{this};                    // x[no_unit],y[mw]
    rw<market, 703, typename A::_txy, no_unit, mw> max_sale{this};                   // x[no_unit],y[mw]
    rw<market, 704, typename A::_txy, no_unit, nok_per_mwh> load_price{this};        // x[no_unit],y[nok_per_mwh]
    rw<market, 705, typename A::_txy, no_unit, nok_per_mwh> buy_price{this};         // x[no_unit],y[nok_per_mwh]
    rw<market, 706, typename A::_txy, no_unit, nok_per_mwh> sale_price{this};        // x[no_unit],y[nok_per_mwh]
    rw<market, 707, typename A::_txy, no_unit, nok_per_mwh> buy_delta{this};         // x[no_unit],y[nok_per_mwh]
    rw<market, 708, typename A::_txy, no_unit, nok_per_mwh> sale_delta{this};        // x[no_unit],y[nok_per_mwh]
    rw<market, 709, typename A::_txy, no_unit, no_unit> bid_flag{this};              // x[no_unit],y[no_unit]
    rw<market, 710, typename A::_txy, no_unit, no_unit> common_scenario{this};       // x[no_unit],y[no_unit]
    ro<market, 711, typename A::_txy, no_unit, mw> buy{this};                        // x[no_unit],y[mw]
    ro<market, 712, typename A::_txy, no_unit, mw> sale{this};                       // x[no_unit],y[mw]
    ro<market, 713, typename A::_txy, no_unit, mw> sim_sale{this};                   // x[no_unit],y[mw]
    ro<market, 714, typename A::_txy, no_unit, mw> sim_buy{this};                    // x[no_unit],y[mw]
    ro<market, 715, typename A::_txy, no_unit, mw> reserve_obligation_penalty{this}; // x[no_unit],y[mw]
    ro<market, 716, typename A::_txy, no_unit, mw> load_penalty{this};               // x[no_unit],y[mw]
  };

  template <class A>
  struct global_settings : obj<A, 13> {
    using super = obj<A, 13>;
    global_settings() = default;

    global_settings(A* s, int oid)
      : super(s, oid) {
    }

    global_settings(global_settings const & o)
      : super(o) {
    }

    global_settings(global_settings&& o)
      : super(std::move(o)) {
    }

    global_settings& operator=(global_settings const & o) {
      super::operator=(o);
      return *this;
    }

    global_settings& operator=(global_settings&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    rw<global_settings, 908, int, no_unit, no_unit> load_penalty_flag{this};                   // x[no_unit],y[no_unit]
    rw<global_settings, 909, int, no_unit, no_unit> rsv_penalty_flag{this};                    // x[no_unit],y[no_unit]
    rw<global_settings, 910, int, no_unit, no_unit> volume_ramp_penalty_flag{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 911, int, no_unit, no_unit> level_ramp_penalty_flag{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 912, int, no_unit, no_unit> production_ramp_penalty_flag{this};        // x[no_unit],y[no_unit]
    rw<global_settings, 913, int, no_unit, no_unit> plant_min_q_penalty_flag{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 914, int, no_unit, no_unit> plant_min_p_penalty_flag{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 915, int, no_unit, no_unit> plant_max_q_penalty_flag{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 916, int, no_unit, no_unit> plant_max_p_penalty_flag{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 917, int, no_unit, no_unit> gate_min_q_penalty_flag{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 918, int, no_unit, no_unit> gate_max_q_penalty_flag{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 919, int, no_unit, no_unit> gate_ramp_penalty_flag{this};              // x[no_unit],y[no_unit]
    rw<global_settings, 920, int, no_unit, no_unit> plant_schedule_penalty_flag{this};         // x[no_unit],y[no_unit]
    rw<global_settings, 921, int, no_unit, no_unit> gen_discharge_schedule_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings, 922, int, no_unit, no_unit> pump_schedule_penalty_flag{this};          // x[no_unit],y[no_unit]
    rw<global_settings, 923, int, no_unit, no_unit> power_limit_penalty_flag{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 924, double, nok_per_mwh, nok_per_mwh> power_limit_penalty_cost{
      this};                                                                            // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings, 925, double, nok_per_mwh, nok_per_mwh> load_penalty_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings, 926, double, nok_per_mm3, nok_per_mm3> rsv_penalty_cost{this};  // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 927, double, nok_per_mm3, nok_per_mm3> rsv_hard_limit_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 928, double, nok_per_mm3, nok_per_mm3> volume_ramp_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 929, double, nok_per_meter, nok_per_meter> level_ramp_penalty_cost{
      this}; // x[nok_per_meter],y[nok_per_meter]
    rw<global_settings, 930, double, nok_per_mwh, nok_per_mwh> production_ramp_penalty_cost{
      this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings, 931, double, nok_per_m3_per_s, nok_per_m3_per_s> plant_discharge_ramp_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings, 932, double, nok_per_mwh, nok_per_mwh> plant_soft_p_penalty{
      this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings, 933, double, nok_per_mm3, nok_per_mm3> plant_soft_q_penalty{
      this};                                                                        // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 934, double, nok, nok> plant_sched_penalty_cost_up{this};   // x[nok],y[nok]
    rw<global_settings, 935, double, nok, nok> plant_sched_penalty_cost_down{this}; // x[nok],y[nok]
    rw<global_settings, 936, double, nok_per_mm3, nok_per_mm3> gen_discharge_sched_penalty_cost_up{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 937, double, nok_per_mm3, nok_per_mm3> gen_discharge_sched_penalty_cost_down{
      this};                                                                       // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 938, double, nok, nok> pump_sched_penalty_cost_up{this};   // x[nok],y[nok]
    rw<global_settings, 939, double, nok, nok> pump_sched_penalty_cost_down{this}; // x[nok],y[nok]
    rw<global_settings, 940, double, nok_per_m3_per_s, nok_per_m3_per_s> gate_ramp_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings, 941, double, nok_per_mm3, nok_per_mm3> discharge_group_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 942, double, nok_per_mw, nok_per_mw> reserve_schedule_penalty_cost{
      this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 943, double, nok_per_mw, nok_per_mw> reserve_group_penalty_cost{
      this};                                                                        // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 944, double, nok_per_mm3, nok_per_mm3> bypass_cost{this};   // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 945, double, nok_per_mm3, nok_per_mm3> gate_cost{this};     // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 946, double, nok_per_mm3, nok_per_mm3> overflow_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 947, double, nok_per_mm3h, nok_per_mm3h> overflow_cost_time_factor{
      this}; // x[nok_per_mm3h],y[nok_per_mm3h]
    rw<global_settings, 948, double, nok_per_mw, nok_per_mw> gen_reserve_ramping_cost{
      this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 949, double, nok_per_mw, nok_per_mw> pump_reserve_ramping_cost{
      this};                                                                    // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 950, double, nok, nok> reserve_contribution_cost{this}; // x[nok],y[nok]
    rw<global_settings, 951, double, nok_per_m3_per_s, nok_per_m3_per_s> gate_ramp_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings, 952, double, nok_per_mw, nok_per_mw> reserve_group_slack_cost{
      this};                                                                             // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 953, double, nok_per_mw, nok_per_mw> fcr_n_ramping_cost{this};   // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 954, double, nok_per_mw, nok_per_mw> fcr_d_ramping_cost{this};   // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 955, double, nok_per_mw, nok_per_mw> frr_ramping_cost{this};     // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 956, double, nok_per_mw, nok_per_mw> rr_ramping_cost{this};      // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings, 957, double, no_unit, no_unit> fcr_n_up_activation_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings, 958, double, no_unit, no_unit> fcr_n_down_activation_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings, 959, double, no_unit, no_unit> fcr_d_up_activation_factor{this};   // x[no_unit],y[no_unit]
    rw<global_settings, 960, double, no_unit, no_unit> fcr_d_down_activation_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings, 961, double, no_unit, no_unit> frr_up_activation_factor{this};     // x[no_unit],y[no_unit]
    rw<global_settings, 962, double, no_unit, no_unit> frr_down_activation_factor{this};   // x[no_unit],y[no_unit]
    rw<global_settings, 963, double, no_unit, no_unit> rr_up_activation_factor{this};      // x[no_unit],y[no_unit]
    rw<global_settings, 964, double, no_unit, no_unit> rr_down_activation_factor{this};    // x[no_unit],y[no_unit]
    rw<global_settings, 965, double, no_unit, no_unit> fcr_n_up_activation_time{this};     // x[no_unit],y[no_unit]
    rw<global_settings, 966, double, hour, hour> fcr_n_down_activation_time{this};         // x[hour],y[hour]
    rw<global_settings, 967, double, hour, hour> fcr_d_up_activation_time{this};           // x[hour],y[hour]
    rw<global_settings, 968, double, hour, hour> fcr_d_down_activation_time{this};         // x[hour],y[hour]
    rw<global_settings, 969, double, hour, hour> frr_up_activation_time{this};             // x[hour],y[hour]
    rw<global_settings, 970, double, hour, hour> frr_down_activation_time{this};           // x[hour],y[hour]
    rw<global_settings, 971, double, hour, hour> rr_up_activation_time{this};              // x[hour],y[hour]
    rw<global_settings, 972, double, hour, hour> rr_down_activation_time{this};            // x[hour],y[hour]
    rw<global_settings, 973, double, nok_per_mm3, nok_per_mm3> fcr_n_up_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 974, double, nok_per_mm3, nok_per_mm3> fcr_n_down_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 975, double, nok_per_mm3, nok_per_mm3> fcr_d_up_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 976, double, nok_per_mm3, nok_per_mm3> fcr_d_down_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 977, double, nok_per_mm3, nok_per_mm3> frr_up_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 978, double, nok_per_mm3, nok_per_mm3> frr_down_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 979, double, nok_per_mm3, nok_per_mm3> rr_up_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 980, double, nok_per_mm3, nok_per_mm3> rr_down_activation_penalty_cost{
      this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings, 981, double, no_unit, no_unit> reserve_tactical_activation_cost_scaling{
      this};                                                                                   // x[no_unit],y[no_unit]
    rw<global_settings, 982, int, no_unit, no_unit> use_heuristic_basis{this};                 // x[no_unit],y[no_unit]
    rw<global_settings, 983, int, no_unit, no_unit> nodelog{this};                             // x[no_unit],y[no_unit]
    rw<global_settings, 984, int, no_unit, no_unit> max_num_threads{this};                     // x[no_unit],y[no_unit]
    rw<global_settings, 985, int, no_unit, no_unit> parallelmode{this};                        // x[no_unit],y[no_unit]
    rw<global_settings, 986, double, second, second> timelimit{this};                          // x[second],y[second]
    rw<global_settings, 987, double, no_unit, no_unit> mipgap_rel{this};                       // x[no_unit],y[no_unit]
    rw<global_settings, 988, double, nok, nok> mipgap_abs{this};                               // x[nok],y[nok]
    rw<global_settings, 989, double, no_unit, no_unit> inteps{this};                           // x[no_unit],y[no_unit]
    rw<global_settings, 990, std::string, no_unit, no_unit> input_basis_name{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 991, std::string, no_unit, no_unit> output_basis_name{this};           // x[no_unit],y[no_unit]
    rw<global_settings, 992, std::string, no_unit, no_unit> solver_algorithm{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 993, int, no_unit, no_unit> n_seg_up{this};                            // x[no_unit],y[no_unit]
    rw<global_settings, 994, int, no_unit, no_unit> n_seg_down{this};                          // x[no_unit],y[no_unit]
    rw<global_settings, 995, int, no_unit, no_unit> n_mip_seg_up{this};                        // x[no_unit],y[no_unit]
    rw<global_settings, 996, int, no_unit, no_unit> n_mip_seg_down{this};                      // x[no_unit],y[no_unit]
    rw<global_settings, 997, int, no_unit, no_unit> dyn_pq_seg_flag{this};                     // x[no_unit],y[no_unit]
    rw<global_settings, 998, int, no_unit, no_unit> dyn_mip_pq_seg_flag{this};                 // x[no_unit],y[no_unit]
    rw<global_settings, 999, int, no_unit, no_unit> bypass_segments{this};                     // x[no_unit],y[no_unit]
    rw<global_settings, 1000, int, no_unit, no_unit> gate_segments{this};                      // x[no_unit],y[no_unit]
    rw<global_settings, 1001, int, no_unit, no_unit> overflow_segments{this};                  // x[no_unit],y[no_unit]
    rw<global_settings, 1002, double, no_unit, no_unit> gravity{this};                         // x[no_unit],y[no_unit]
    rw<global_settings, 1003, double, no_unit, no_unit> gen_reserve_min_free_cap_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings, 1004, double, no_unit, no_unit> fcr_n_band{this};                      // x[no_unit],y[no_unit]
    rw<global_settings, 1005, double, no_unit, no_unit> fcr_d_band{this};                      // x[no_unit],y[no_unit]
    rw<global_settings, 1006, int, no_unit, no_unit> universal_mip{this};                      // x[no_unit],y[no_unit]
    rw<global_settings, 1007, int, no_unit, no_unit> universal_overflow_mip{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 1008, int, no_unit, no_unit> universal_river_mip{this};                // x[no_unit],y[no_unit]
    rw<global_settings, 1009, int, no_unit, no_unit> linear_startup{this};                     // x[no_unit],y[no_unit]
    rw<global_settings, 1010, int, no_unit, no_unit> dyn_flex_mip_steps{this};                 // x[no_unit],y[no_unit]
    rw<global_settings, 1011, int, no_unit, no_unit> merge_blocks{this};                       // x[no_unit],y[no_unit]
    rw<global_settings, 1012, int, no_unit, no_unit> power_head_optimization{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 1013, double, no_unit, no_unit> droop_discretization_limit{this};      // x[no_unit],y[no_unit]
    rw<global_settings, 1014, double, no_unit, no_unit> droop_cost_exponent{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 1015, double, no_unit, no_unit> droop_ref_value{this};                 // x[no_unit],y[no_unit]
    rw<global_settings, 1016, int, no_unit, no_unit> create_cuts{this};                        // x[no_unit],y[no_unit]
    rw<global_settings, 1017, int, no_unit, no_unit> print_sim_inflow{this};                   // x[no_unit],y[no_unit]
    rw<global_settings, 1018, int, no_unit, no_unit> pump_head_optimization{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 1019, int, no_unit, no_unit> save_pq_curves{this};                     // x[no_unit],y[no_unit]
    rw<global_settings, 1020, int, no_unit, no_unit> build_original_pq_curves_by_turb_eff{
      this};                                                                              // x[no_unit],y[no_unit]
    rw<global_settings, 1021, int, no_unit, no_unit> plant_unbalance_recommit{this};      // x[no_unit],y[no_unit]
    rw<global_settings, 1022, int, no_unit, no_unit> prefer_start_vol{this};              // x[no_unit],y[no_unit]
    rw<global_settings, 1023, int, no_unit, no_unit> bp_print_discharge{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 1024, int, no_unit, no_unit> prod_from_ref_prod{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 1025, int, no_unit, no_unit> bp_min_points{this};                 // x[no_unit],y[no_unit]
    rw<global_settings, 1026, int, no_unit, no_unit> bp_mode{this};                       // x[no_unit],y[no_unit]
    rw<global_settings, 1027, int, no_unit, no_unit> stop_cost_from_start_cost{this};     // x[no_unit],y[no_unit]
    rw<global_settings, 1028, int, no_unit, no_unit> ownership_scaling{this};             // x[no_unit],y[no_unit]
    rw<global_settings, 1029, std::string, no_unit, no_unit> time_delay_unit{this};       // x[no_unit],y[no_unit]
    rw<global_settings, 1030, int, no_unit, no_unit> bypass_loss{this};                   // x[no_unit],y[no_unit]
    rw<global_settings, 1031, double, no_unit, no_unit> gen_turn_off_limit{this};         // x[no_unit],y[no_unit]
    rw<global_settings, 1032, double, no_unit, no_unit> pump_turn_off_limit{this};        // x[no_unit],y[no_unit]
    rw<global_settings, 1033, int, no_unit, no_unit> fcr_n_equality_flag{this};           // x[no_unit],y[no_unit]
    rw<global_settings, 1034, int, no_unit, no_unit> delay_valuation_mode{this};          // x[no_unit],y[no_unit]
    rw<global_settings, 1035, int, no_unit, no_unit> universal_affinity_flag{this};       // x[no_unit],y[no_unit]
    rw<global_settings, 1036, int, no_unit, no_unit> ramp_code{this};                     // x[no_unit],y[no_unit]
    rw<global_settings, 1037, int, no_unit, no_unit> print_original_pq_curves{this};      // x[no_unit],y[no_unit]
    rw<global_settings, 1038, int, no_unit, no_unit> print_convex_pq_curves{this};        // x[no_unit],y[no_unit]
    rw<global_settings, 1039, int, no_unit, no_unit> print_final_pq_curves{this};         // x[no_unit],y[no_unit]
    rw<global_settings, 1040, std::string, no_unit, no_unit> shop_log_name{this};         // x[no_unit],y[no_unit]
    rw<global_settings, 1041, std::string, no_unit, no_unit> shop_yaml_log_name{this};    // x[no_unit],y[no_unit]
    rw<global_settings, 1042, std::string, no_unit, no_unit> solver_log_name{this};       // x[no_unit],y[no_unit]
    rw<global_settings, 1043, std::string, no_unit, no_unit> model_file_name{this};       // x[no_unit],y[no_unit]
    rw<global_settings, 1044, std::string, no_unit, no_unit> pq_curves_name{this};        // x[no_unit],y[no_unit]
    rw<global_settings, 1045, int, no_unit, no_unit> print_loss{this};                    // x[no_unit],y[no_unit]
    rw<global_settings, 1046, int, no_unit, no_unit> shop_xmllog{this};                   // x[no_unit],y[no_unit]
    rw<global_settings, 1047, int, no_unit, no_unit> print_optimized_startup_costs{this}; // x[no_unit],y[no_unit]
    rw<global_settings, 1048, int, no_unit, no_unit> get_duals_from_mip{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 1049, int, no_unit, no_unit> bid_aggregation_level{this};         // x[no_unit],y[no_unit]
    rw<global_settings, 1050, int, no_unit, no_unit> simple_pq_recovery{this};            // x[no_unit],y[no_unit]
    rw<global_settings, 1051, int, no_unit, no_unit> rr_up_schedule_slack_flag{this};     // x[no_unit],y[no_unit]
    ro<global_settings, 1052, int, no_unit, no_unit> bp_ref_mc_from_market{this};         // x[no_unit],y[no_unit]
    rw<global_settings, 1053, int, no_unit, no_unit> bp_bid_matrix_points{this};          // x[no_unit],y[no_unit]
    rw<global_settings, 1054, double, no_unit, no_unit> ramp_scale_factor{this};          // x[no_unit],y[no_unit]
    rw<global_settings, 1055, double, nok_per_m3_per_s, nok_per_m3_per_s> river_flow_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings, 1056, double, nok_per_m3_per_s, nok_per_m3_per_s> river_flow_schedule_penalty_cost{
      this};                                                         // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings, 1057, int, no_unit, no_unit> recommit{this}; // x[no_unit],y[no_unit]
    ro<global_settings, 1101, std::string, no_unit, no_unit> shop_version{this};  // x[no_unit],y[no_unit]
    rw<global_settings, 1102, double, no_unit, no_unit> river_big_m_factor{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct reserve_group : obj<A, 14> {
    using super = obj<A, 14>;
    reserve_group() = default;

    reserve_group(A* s, int oid)
      : super(s, oid) {
    }

    reserve_group(reserve_group const & o)
      : super(o) {
    }

    reserve_group(reserve_group&& o)
      : super(std::move(o)) {
    }

    reserve_group& operator=(reserve_group const & o) {
      super::operator=(o);
      return *this;
    }

    reserve_group& operator=(reserve_group&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<reserve_group, 717, int, no_unit, no_unit> group_id{this};                           // x[no_unit],y[no_unit]
    rw<reserve_group, 718, typename A::_txy, no_unit, mw> fcr_n_up_obligation{this};        // x[no_unit],y[mw]
    rw<reserve_group, 719, typename A::_txy, no_unit, mw> fcr_n_down_obligation{this};      // x[no_unit],y[mw]
    rw<reserve_group, 720, typename A::_txy, no_unit, mw> fcr_d_up_obligation{this};        // x[no_unit],y[mw]
    rw<reserve_group, 721, typename A::_txy, no_unit, mw> fcr_d_down_obligation{this};      // x[no_unit],y[mw]
    rw<reserve_group, 722, typename A::_txy, no_unit, mw> frr_up_obligation{this};          // x[no_unit],y[mw]
    rw<reserve_group, 723, typename A::_txy, no_unit, mw> frr_down_obligation{this};        // x[no_unit],y[mw]
    rw<reserve_group, 724, typename A::_txy, no_unit, mw> rr_up_obligation{this};           // x[no_unit],y[mw]
    rw<reserve_group, 725, typename A::_txy, no_unit, mw> rr_down_obligation{this};         // x[no_unit],y[mw]
    rw<reserve_group, 726, typename A::_txy, no_unit, nok_per_mw> fcr_n_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group, 727, typename A::_txy, no_unit, nok_per_mw> fcr_d_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group, 728, typename A::_txy, no_unit, nok_per_mw> frr_penalty_cost{this};   // x[no_unit],y[nok_per_mw]
    rw<reserve_group, 729, typename A::_txy, no_unit, nok_per_mw> rr_penalty_cost{this};    // x[no_unit],y[nok_per_mw]
    ro<reserve_group, 730, typename A::_txy, no_unit, mw> fcr_n_up_slack{this};             // x[no_unit],y[mw]
    ro<reserve_group, 731, typename A::_txy, no_unit, mw> fcr_n_down_slack{this};           // x[no_unit],y[mw]
    ro<reserve_group, 732, typename A::_txy, no_unit, mw> fcr_d_up_slack{this};             // x[no_unit],y[mw]
    ro<reserve_group, 733, typename A::_txy, no_unit, mw> fcr_d_down_slack{this};           // x[no_unit],y[mw]
    ro<reserve_group, 734, typename A::_txy, no_unit, mw> frr_up_slack{this};               // x[no_unit],y[mw]
    ro<reserve_group, 735, typename A::_txy, no_unit, mw> frr_down_slack{this};             // x[no_unit],y[mw]
    ro<reserve_group, 736, typename A::_txy, no_unit, mw> rr_up_slack{this};                // x[no_unit],y[mw]
    ro<reserve_group, 737, typename A::_txy, no_unit, mw> rr_down_slack{this};              // x[no_unit],y[mw]
    ro<reserve_group, 738, typename A::_txy, no_unit, mw> fcr_n_up_violation{this};         // x[no_unit],y[mw]
    ro<reserve_group, 739, typename A::_txy, no_unit, mw> fcr_n_down_violation{this};       // x[no_unit],y[mw]
    ro<reserve_group, 740, typename A::_txy, no_unit, mw> fcr_d_up_violation{this};         // x[no_unit],y[mw]
    ro<reserve_group, 741, typename A::_txy, no_unit, mw> fcr_d_down_violation{this};       // x[no_unit],y[mw]
    ro<reserve_group, 742, typename A::_txy, no_unit, mw> frr_up_violation{this};           // x[no_unit],y[mw]
    ro<reserve_group, 743, typename A::_txy, no_unit, mw> frr_down_violation{this};         // x[no_unit],y[mw]
    ro<reserve_group, 744, typename A::_txy, no_unit, mw> rr_up_violation{this};            // x[no_unit],y[mw]
    ro<reserve_group, 745, typename A::_txy, no_unit, mw> rr_down_violation{this};          // x[no_unit],y[mw]
  };

  template <class A>
  struct commit_group : obj<A, 15> {
    using super = obj<A, 15>;
    commit_group() = default;

    commit_group(A* s, int oid)
      : super(s, oid) {
    }

    commit_group(commit_group const & o)
      : super(o) {
    }

    commit_group(commit_group&& o)
      : super(std::move(o)) {
    }

    commit_group& operator=(commit_group const & o) {
      super::operator=(o);
      return *this;
    }

    commit_group& operator=(commit_group&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<commit_group, 791, typename A::_txy, no_unit, no_unit> deactivate_exclusion_flag{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct discharge_group : obj<A, 16> {
    using super = obj<A, 16>;
    discharge_group() = default;

    discharge_group(A* s, int oid)
      : super(s, oid) {
    }

    discharge_group(discharge_group const & o)
      : super(o) {
    }

    discharge_group(discharge_group&& o)
      : super(std::move(o)) {
    }

    discharge_group& operator=(discharge_group const & o) {
      super::operator=(o);
      return *this;
    }

    discharge_group& operator=(discharge_group&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<discharge_group, 746, double, mm3, mm3> initial_deviation_mm3{this}; // x[mm3],y[mm3]
    rw<discharge_group, 747, typename A::_txy, no_unit, mm3> max_accumulated_deviation_mm3_up{
      this}; // x[no_unit],y[mm3]
    rw<discharge_group, 748, typename A::_txy, no_unit, mm3> max_accumulated_deviation_mm3_down{
      this}; // x[no_unit],y[mm3]
    rw<discharge_group, 749, typename A::_txy, no_unit, m3_per_s> weighted_discharge_m3s{
      this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group, 750, typename A::_txy, no_unit, nok_per_mm3> penalty_cost_up_per_mm3{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<discharge_group, 751, typename A::_txy, no_unit, nok_per_mm3> penalty_cost_down_per_mm3{
      this};                                                                               // x[no_unit],y[nok_per_mm3]
    rw<discharge_group, 752, typename A::_txy, no_unit, m3_per_s> min_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group, 753, typename A::_txy, no_unit, m3_per_s> max_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group, 754, typename A::_txy, no_unit, nok_per_h_per_m3_per_s> min_discharge_penalty_cost{
      this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group, 755, typename A::_txy, no_unit, nok_per_h_per_m3_per_s> max_discharge_penalty_cost{
      this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group, 756, typename A::_txy, no_unit, m3sec_hour> ramping_up_m3s{this};   // x[no_unit],y[m3sec_hour]
    rw<discharge_group, 757, typename A::_txy, no_unit, m3sec_hour> ramping_down_m3s{this}; // x[no_unit],y[m3sec_hour]
    rw<discharge_group, 758, typename A::_txy, no_unit, nok_per_h_per_m3_per_s> ramping_up_penalty_cost{
      this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group, 759, typename A::_txy, no_unit, nok_per_h_per_m3_per_s> ramping_down_penalty_cost{
      this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group, 760, std::map<int64_t, typename A::_xy>, minute, m3_per_s> min_average_discharge{
      this}; // x[minute],y[m3_per_s]
    rw<discharge_group, 761, std::map<int64_t, typename A::_xy>, minute, m3_per_s> max_average_discharge{
      this}; // x[minute],y[m3_per_s]
    rw<discharge_group, 762, double, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s> min_average_discharge_penalty_cost{
      this}; // x[nok_per_h_per_m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<discharge_group, 763, double, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s> max_average_discharge_penalty_cost{
      this}; // x[nok_per_h_per_m3_per_s],y[nok_per_h_per_m3_per_s]
    ro<discharge_group, 764, typename A::_txy, no_unit, m3_per_s> actual_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    ro<discharge_group, 765, typename A::_txy, no_unit, mm3> accumulated_deviation_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group, 766, typename A::_txy, no_unit, mm3> upper_penalty_mm3{this};         // x[no_unit],y[mm3]
    ro<discharge_group, 767, typename A::_txy, no_unit, mm3> lower_penalty_mm3{this};         // x[no_unit],y[mm3]
    ro<discharge_group, 768, typename A::_txy, no_unit, mm3> upper_slack_mm3{this};           // x[no_unit],y[mm3]
    ro<discharge_group, 769, typename A::_txy, no_unit, mm3> lower_slack_mm3{this};           // x[no_unit],y[mm3]
    ro<discharge_group, 770, typename A::_txy, no_unit, nok> min_discharge_penalty{this};     // x[no_unit],y[nok]
    ro<discharge_group, 771, typename A::_txy, no_unit, nok> max_discharge_penalty{this};     // x[no_unit],y[nok]
    ro<discharge_group, 772, typename A::_txy, no_unit, nok> ramping_up_penalty{this};        // x[no_unit],y[nok]
    ro<discharge_group, 773, typename A::_txy, no_unit, nok> ramping_down_penalty{this};      // x[no_unit],y[nok]
  };

  template <class A>
  struct production_group : obj<A, 17> {
    using super = obj<A, 17>;
    production_group() = default;

    production_group(A* s, int oid)
      : super(s, oid) {
    }

    production_group(production_group const & o)
      : super(o) {
    }

    production_group(production_group&& o)
      : super(std::move(o)) {
    }

    production_group& operator=(production_group const & o) {
      super::operator=(o);
      return *this;
    }

    production_group& operator=(production_group&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<production_group, 774, double, mwh, mwh> energy_target{this}; // x[mwh],y[mwh]
    rw<production_group, 775, double, nok_per_mwh, nok_per_mwh> energy_penalty_cost_up{
      this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<production_group, 776, double, nok_per_mwh, nok_per_mwh> energy_penalty_cost_down{
      this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<production_group, 777, typename A::_txy, no_unit, mwh> energy_target_period_flag{this}; // x[no_unit],y[mwh]
    rw<production_group, 778, typename A::_txy, no_unit, mw> max_p_limit{this};                // x[no_unit],y[mw]
    rw<production_group, 779, typename A::_txy, no_unit, mw> min_p_limit{this};                // x[no_unit],y[mw]
    rw<production_group, 780, typename A::_txy, no_unit, nok_per_mw> max_p_penalty_cost{
      this}; // x[no_unit],y[nok_per_mw]
    rw<production_group, 781, typename A::_txy, no_unit, nok_per_mw> min_p_penalty_cost{
      this};                                                                         // x[no_unit],y[nok_per_mw]
    ro<production_group, 782, typename A::_txy, no_unit, mw> sum_production{this};   // x[no_unit],y[mw]
    ro<production_group, 783, typename A::_txy, no_unit, nok> min_p_penalty{this};   // x[no_unit],y[nok]
    ro<production_group, 784, typename A::_txy, no_unit, nok> max_p_penalty{this};   // x[no_unit],y[nok]
    ro<production_group, 785, typename A::_txy, nok, nok> energy_penalty_up{this};   // x[nok],y[nok]
    ro<production_group, 786, typename A::_txy, nok, nok> energy_penalty_down{this}; // x[nok],y[nok]
  };

  template <class A>
  struct volume_constraint : obj<A, 18> {
    using super = obj<A, 18>;
    volume_constraint() = default;

    volume_constraint(A* s, int oid)
      : super(s, oid) {
    }

    volume_constraint(volume_constraint const & o)
      : super(o) {
    }

    volume_constraint(volume_constraint&& o)
      : super(std::move(o)) {
    }

    volume_constraint& operator=(volume_constraint const & o) {
      super::operator=(o);
      return *this;
    }

    volume_constraint& operator=(volume_constraint&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<volume_constraint, 787, typename A::_txy, no_unit, mm3> min_vol{this}; // x[no_unit],y[mm3]
    rw<volume_constraint, 788, typename A::_txy, no_unit, mm3> max_vol{this}; // x[no_unit],y[mm3]
    rw<volume_constraint, 789, typename A::_txy, no_unit, nok_per_mm3> min_vol_penalty{
      this}; // x[no_unit],y[nok_per_mm3]
    rw<volume_constraint, 790, typename A::_txy, no_unit, nok_per_mm3> max_vol_penalty{
      this}; // x[no_unit],y[nok_per_mm3]
  };

  template <class A>
  struct scenario : obj<A, 19> {
    using super = obj<A, 19>;
    scenario() = default;

    scenario(A* s, int oid)
      : super(s, oid) {
    }

    scenario(scenario const & o)
      : super(o) {
    }

    scenario(scenario&& o)
      : super(std::move(o)) {
    }

    scenario& operator=(scenario const & o) {
      super::operator=(o);
      return *this;
    }

    scenario& operator=(scenario&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    rw<scenario, 792, int, no_unit, no_unit> scenario_id{this};                  // x[no_unit],y[no_unit]
    rw<scenario, 793, typename A::_txy, no_unit, no_unit> probability{this};     // x[no_unit],y[no_unit]
    rw<scenario, 794, typename A::_txy, no_unit, no_unit> common_scenario{this}; // x[no_unit],y[no_unit]
    rw<scenario, 795, typename A::_txy, no_unit, no_unit> common_history{this};  // x[no_unit],y[no_unit]
  };

  template <class A>
  struct objective : obj<A, 20> {
    using super = obj<A, 20>;
    objective() = default;

    objective(A* s, int oid)
      : super(s, oid) {
    }

    objective(objective const & o)
      : super(o) {
    }

    objective(objective&& o)
      : super(std::move(o)) {
    }

    objective& operator=(objective const & o) {
      super::operator=(o);
      return *this;
    }

    objective& operator=(objective&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    ro<objective, 796, std::string, no_unit, no_unit> solver_status{this};                // x[no_unit],y[no_unit]
    ro<objective, 797, int, no_unit, no_unit> times_of_wrong_pq_uploading{this};          // x[no_unit],y[no_unit]
    ro<objective, 798, double, nok, nok> grand_total{this};                               // x[nok],y[nok]
    ro<objective, 799, double, nok, nok> sim_grand_total{this};                           // x[nok],y[nok]
    ro<objective, 800, double, nok, nok> total{this};                                     // x[nok],y[nok]
    ro<objective, 801, double, nok, nok> sum_penalties{this};                             // x[nok],y[nok]
    ro<objective, 802, double, nok, nok> minor_penalties{this};                           // x[nok],y[nok]
    ro<objective, 803, double, nok, nok> major_penalties{this};                           // x[nok],y[nok]
    ro<objective, 804, double, nok, nok> rsv_end_value{this};                             // x[nok],y[nok]
    ro<objective, 805, double, nok, nok> sim_rsv_end_value{this};                         // x[nok],y[nok]
    ro<objective, 806, double, nok, nok> rsv_end_value_relative{this};                    // x[nok],y[nok]
    ro<objective, 807, double, nok, nok> vow_in_transit{this};                            // x[nok],y[nok]
    ro<objective, 808, double, nok, nok> rsv_spill_vol_end_value{this};                   // x[nok],y[nok]
    ro<objective, 809, double, nok, nok> market_sale_buy{this};                           // x[nok],y[nok]
    ro<objective, 810, double, nok, nok> sim_market_sale_buy{this};                       // x[nok],y[nok]
    ro<objective, 811, double, nok, nok> load_value{this};                                // x[nok],y[nok]
    ro<objective, 812, double, nok, nok> reserve_sale_buy{this};                          // x[nok],y[nok]
    ro<objective, 813, double, nok, nok> reserve_oblig_value{this};                       // x[nok],y[nok]
    ro<objective, 814, double, nok, nok> contract_value{this};                            // x[nok],y[nok]
    ro<objective, 815, double, nok, nok> startup_costs{this};                             // x[nok],y[nok]
    ro<objective, 816, double, nok, nok> sim_startup_costs{this};                         // x[nok],y[nok]
    ro<objective, 817, double, nok, nok> sum_feeding_fee{this};                           // x[nok],y[nok]
    ro<objective, 818, double, nok, nok> sum_discharge_fee{this};                         // x[nok],y[nok]
    ro<objective, 819, double, nok, nok> thermal_cost{this};                              // x[nok],y[nok]
    ro<objective, 820, double, nok, nok> production_cost{this};                           // x[nok],y[nok]
    ro<objective, 821, double, nok, nok> reserve_allocation_cost{this};                   // x[nok],y[nok]
    ro<objective, 822, double, nok, nok> rsv_tactical_penalty{this};                      // x[nok],y[nok]
    ro<objective, 823, double, nok, nok> plant_p_constr_penalty{this};                    // x[nok],y[nok]
    ro<objective, 824, double, nok, nok> plant_q_constr_penalty{this};                    // x[nok],y[nok]
    ro<objective, 825, double, nok, nok> plant_schedule_penalty{this};                    // x[nok],y[nok]
    ro<objective, 826, double, nok, nok> plant_rsv_q_limit_penalty{this};                 // x[nok],y[nok]
    ro<objective, 827, double, nok, nok> gen_schedule_penalty{this};                      // x[nok],y[nok]
    ro<objective, 828, double, nok, nok> pump_schedule_penalty{this};                     // x[nok],y[nok]
    ro<objective, 829, double, nok, nok> gate_q_constr_penalty{this};                     // x[nok],y[nok]
    ro<objective, 830, double, nok, nok> gate_discharge_cost{this};                       // x[nok],y[nok]
    ro<objective, 831, double, nok, nok> bypass_cost{this};                               // x[nok],y[nok]
    ro<objective, 832, double, nok, nok> gate_spill_cost{this};                           // x[nok],y[nok]
    ro<objective, 833, double, nok, nok> physical_spill_cost{this};                       // x[nok],y[nok]
    ro<objective, 834, double, mm3, mm3> physical_spill_volume{this};                     // x[mm3],y[mm3]
    ro<objective, 835, double, nok, nok> nonphysical_spill_cost{this};                    // x[nok],y[nok]
    ro<objective, 836, double, mm3, mm3> nonphysical_spill_volume{this};                  // x[mm3],y[mm3]
    ro<objective, 837, double, nok, nok> gate_slack_cost{this};                           // x[nok],y[nok]
    ro<objective, 838, double, nok, nok> gate_ramping_cost{this};                         // x[nok],y[nok]
    ro<objective, 839, double, nok, nok> creek_spill_cost{this};                          // x[nok],y[nok]
    ro<objective, 840, double, nok, nok> creek_physical_spill_cost{this};                 // x[nok],y[nok]
    ro<objective, 841, double, nok, nok> creek_nonphysical_spill_cost{this};              // x[nok],y[nok]
    ro<objective, 842, double, nok, nok> junction_slack_cost{this};                       // x[nok],y[nok]
    ro<objective, 843, double, nok, nok> reserve_violation_penalty{this};                 // x[nok],y[nok]
    ro<objective, 844, double, nok, nok> reserve_slack_cost{this};                        // x[nok],y[nok]
    ro<objective, 845, double, nok, nok> reserve_schedule_penalty{this};                  // x[nok],y[nok]
    ro<objective, 846, double, nok, nok> rsv_peak_volume_penalty{this};                   // x[nok],y[nok]
    ro<objective, 847, double, nok, nok> gate_peak_flow_penalty{this};                    // x[nok],y[nok]
    ro<objective, 848, double, nok, nok> rsv_flood_volume_penalty{this};                  // x[nok],y[nok]
    ro<objective, 849, double, nok, nok> river_peak_flow_penalty{this};                   // x[nok],y[nok]
    ro<objective, 850, double, nok, nok> river_flow_penalty{this};                        // x[nok],y[nok]
    ro<objective, 851, double, nok, nok> river_gate_adjustment_penalty{this};             // x[nok],y[nok]
    ro<objective, 852, double, nok, nok> plant_reserve_discharge_penalty{this};           // x[nok],y[nok]
    ro<objective, 853, double, nok, nok> solar_curtailment_cost{this};                    // x[nok],y[nok]
    ro<objective, 854, double, nok, nok> wind_curtailment_cost{this};                     // x[nok],y[nok]
    ro<objective, 855, double, nok, nok> plant_sum_reserve_penalty{this};                 // x[nok],y[nok]
    ro<objective, 856, double, nok, nok> reserve_activation_penalty{this};                // x[nok],y[nok]
    ro<objective, 857, double, nok, nok> reserve_tactical_activation_penalty{this};       // x[nok],y[nok]
    ro<objective, 858, double, nok, nok> rsv_penalty{this};                               // x[nok],y[nok]
    ro<objective, 859, double, nok, nok> rsv_hard_limit_penalty{this};                    // x[nok],y[nok]
    ro<objective, 860, double, nok, nok> rsv_over_limit_penalty{this};                    // x[nok],y[nok]
    ro<objective, 861, double, nok, nok> sim_rsv_penalty{this};                           // x[nok],y[nok]
    ro<objective, 862, double, nok, nok> rsv_end_penalty{this};                           // x[nok],y[nok]
    ro<objective, 863, double, nok, nok> load_penalty{this};                              // x[nok],y[nok]
    ro<objective, 864, double, nok, nok> group_time_period_penalty{this};                 // x[nok],y[nok]
    ro<objective, 865, double, nok, nok> group_time_step_penalty{this};                   // x[nok],y[nok]
    ro<objective, 866, double, nok, nok> sum_ramping_penalty{this};                       // x[nok],y[nok]
    ro<objective, 867, double, nok, nok> plant_ramping_penalty{this};                     // x[nok],y[nok]
    ro<objective, 868, double, nok, nok> rsv_ramping_penalty{this};                       // x[nok],y[nok]
    ro<objective, 869, double, nok, nok> gate_ramping_penalty{this};                      // x[nok],y[nok]
    ro<objective, 870, double, nok, nok> contract_ramping_penalty{this};                  // x[nok],y[nok]
    ro<objective, 871, double, nok, nok> group_ramping_penalty{this};                     // x[nok],y[nok]
    ro<objective, 872, double, nok, nok> discharge_group_penalty{this};                   // x[nok],y[nok]
    ro<objective, 873, double, nok, nok> discharge_group_ramping_penalty{this};           // x[nok],y[nok]
    ro<objective, 874, double, nok, nok> discharge_group_average_discharge_penalty{this}; // x[nok],y[nok]
    ro<objective, 875, double, nok, nok> production_group_energy_penalty{this};           // x[nok],y[nok]
    ro<objective, 876, double, nok, nok> production_group_power_penalty{this};            // x[nok],y[nok]
    ro<objective, 877, double, nok, nok> river_min_flow_penalty{this};                    // x[nok],y[nok]
    ro<objective, 878, double, nok, nok> river_max_flow_penalty{this};                    // x[nok],y[nok]
    ro<objective, 879, double, nok, nok> river_ramping_penalty{this};                     // x[nok],y[nok]
    ro<objective, 880, double, nok, nok> river_gate_ramping_penalty{this};                // x[nok],y[nok]
    ro<objective, 881, double, nok, nok> river_flow_schedule_penalty{this};               // x[nok],y[nok]
    ro<objective, 882, double, nok, nok> level_rolling_ramping_penalty{this};             // x[nok],y[nok]
    ro<objective, 883, double, nok, nok> level_period_ramping_penalty{this};              // x[nok],y[nok]
    ro<objective, 884, double, nok, nok> discharge_rolling_ramping_penalty{this};         // x[nok],y[nok]
    ro<objective, 885, double, nok, nok> discharge_period_ramping_penalty{this};          // x[nok],y[nok]
    ro<objective, 886, double, nok, nok> rsv_nonseq_ramping_penalty{this};                // x[nok],y[nok]
    ro<objective, 887, double, nok, nok> rsv_amplitude_ramping_penalty{this};             // x[nok],y[nok]
    ro<objective, 888, double, nok, nok> common_decision_penalty{this};                   // x[nok],y[nok]
    ro<objective, 889, double, nok, nok> bidding_penalty{this};                           // x[nok],y[nok]
    ro<objective, 890, double, nok, nok> safe_mode_universal_penalty{this};               // x[nok],y[nok]
  };

  template <class A>
  struct bid_group : obj<A, 21> {
    using super = obj<A, 21>;
    bid_group() = default;

    bid_group(A* s, int oid)
      : super(s, oid) {
    }

    bid_group(bid_group const & o)
      : super(o) {
    }

    bid_group(bid_group&& o)
      : super(std::move(o)) {
    }

    bid_group& operator=(bid_group const & o) {
      super::operator=(o);
      return *this;
    }

    bid_group& operator=(bid_group&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<bid_group, 892, int, no_unit, no_unit> price_dimension{this};                           // x[no_unit],y[no_unit]
    ro<bid_group, 893, int, no_unit, no_unit> time_dimension{this};                            // x[no_unit],y[no_unit]
    ro<bid_group, 894, int, no_unit, no_unit> bid_start_interval{this};                        // x[no_unit],y[no_unit]
    ro<bid_group, 895, int, no_unit, no_unit> bid_end_interval{this};                          // x[no_unit],y[no_unit]
    ro<bid_group, 897, double, no_unit, no_unit> reduction_cost{this};                         // x[no_unit],y[no_unit]
    ro<bid_group, 899, std::map<int64_t, typename A::_xy>, nok_per_mwh, mwh> bid_curves{this}; // x[nok_per_mwh],y[mwh]
    ro<bid_group, 900, typename A::_txy, nok_per_mwh, mwh> bid_penalty{this};                  // x[nok_per_mwh],y[mwh]
    ro<bid_group, 901, std::map<int64_t, typename A::_xy>, nok_per_mwh, mwh> best_profit_bid_matrix{
      this}; // x[nok_per_mwh],y[mwh]
  };

  template <class A>
  struct cut_group : obj<A, 22> {
    using super = obj<A, 22>;
    cut_group() = default;

    cut_group(A* s, int oid)
      : super(s, oid) {
    }

    cut_group(cut_group const & o)
      : super(o) {
    }

    cut_group(cut_group&& o)
      : super(std::move(o)) {
    }

    cut_group& operator=(cut_group const & o) {
      super::operator=(o);
      return *this;
    }

    cut_group& operator=(cut_group&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<cut_group, 902, std::vector<typename A::_xy>, no_unit, nok> rhs{this};      // x[no_unit],y[nok]
    ro<cut_group, 903, typename A::_txy, no_unit, nok> end_value{this};            // x[no_unit],y[nok]
    ro<cut_group, 904, typename A::_txy, no_unit, no_unit> binding_cut_up{this};   // x[no_unit],y[no_unit]
    ro<cut_group, 905, typename A::_txy, no_unit, no_unit> binding_cut_down{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct inflow_series : obj<A, 23> {
    using super = obj<A, 23>;
    inflow_series() = default;

    inflow_series(A* s, int oid)
      : super(s, oid) {
    }

    inflow_series(inflow_series const & o)
      : super(o) {
    }

    inflow_series(inflow_series&& o)
      : super(std::move(o)) {
    }

    inflow_series& operator=(inflow_series const & o) {
      super::operator=(o);
      return *this;
    }

    inflow_series& operator=(inflow_series&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<inflow_series, 906, std::vector<typename A::_xy>, mm3, nok_per_mm3> cut_coeffs{this}; // x[mm3],y[nok_per_mm3]
  };

  template <class A>
  struct opt_system : obj<A, 24> {
    using super = obj<A, 24>;
    opt_system() = default;

    opt_system(A* s, int oid)
      : super(s, oid) {
    }

    opt_system(opt_system const & o)
      : super(o) {
    }

    opt_system(opt_system&& o)
      : super(std::move(o)) {
    }

    opt_system& operator=(opt_system const & o) {
      super::operator=(o);
      return *this;
    }

    opt_system& operator=(opt_system&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    ro<opt_system, 907, std::vector<typename A::_xy>, nok, nok> cut_output_rhs{this}; // x[nok],y[nok]
  };

  template <class A>
  struct unit_combination : obj<A, 25> {
    using super = obj<A, 25>;
    unit_combination() = default;

    unit_combination(A* s, int oid)
      : super(s, oid) {
    }

    unit_combination(unit_combination const & o)
      : super(o) {
    }

    unit_combination(unit_combination&& o)
      : super(std::move(o)) {
    }

    unit_combination& operator=(unit_combination const & o) {
      super::operator=(o);
      return *this;
    }

    unit_combination& operator=(unit_combination&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<unit_combination, 1103, std::map<int64_t, typename A::_xy>, mw, m3_per_s> discharge{this}; // x[mw],y[m3_per_s]
    ro<unit_combination, 1104, std::map<int64_t, typename A::_xy>, mw, nok_per_mw> marginal_cost{
      this}; // x[mw],y[nok_per_mw]
    ro<unit_combination, 1105, std::map<int64_t, typename A::_xy>, mw, nok_per_mw> average_cost{
      this}; // x[mw],y[nok_per_mw]
  };

  template <class A>
  struct tunnel : obj<A, 26> {
    using super = obj<A, 26>;
    tunnel() = default;

    tunnel(A* s, int oid)
      : super(s, oid) {
    }

    tunnel(tunnel const & o)
      : super(o) {
    }

    tunnel(tunnel&& o)
      : super(std::move(o)) {
    }

    tunnel& operator=(tunnel const & o) {
      super::operator=(o);
      return *this;
    }

    tunnel& operator=(tunnel&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<tunnel, 1106, double, meter, meter> start_height{this};                        // x[meter],y[meter]
    rw<tunnel, 1107, double, meter, meter> end_height{this};                          // x[meter],y[meter]
    rw<tunnel, 1108, double, meter, meter> diameter{this};                            // x[meter],y[meter]
    rw<tunnel, 1109, double, meter, meter> length{this};                              // x[meter],y[meter]
    rw<tunnel, 1110, double, s2_per_m5, s2_per_m5> loss_factor{this};                 // x[s2_per_m5],y[s2_per_m5]
    rw<tunnel, 1111, double, meter, meter> weir_width{this};                          // x[meter],y[meter]
    rw<tunnel, 1112, int, no_unit, no_unit> time_delay{this};                         // x[no_unit],y[no_unit]
    rw<tunnel, 1113, typename A::_xy, no_unit, no_unit> gate_opening_curve{this};     // x[no_unit],y[no_unit]
    rw<tunnel, 1114, typename A::_txy, nok, nok> gate_adjustment_cost{this};          // x[nok],y[nok]
    rw<tunnel, 1115, typename A::_txy, no_unit, no_unit> gate_opening_schedule{this}; // x[no_unit],y[no_unit]
    rw<tunnel, 1116, double, no_unit, no_unit> initial_opening{this};                 // x[no_unit],y[no_unit]
    rw<tunnel, 1117, int, no_unit, no_unit> continuous_gate{this};                    // x[no_unit],y[no_unit]
    ro<tunnel, 1118, typename A::_txy, meter, meter> end_pressure{this};              // x[meter],y[meter]
    ro<tunnel, 1119, typename A::_txy, meter, meter> sim_end_pressure{this};          // x[meter],y[meter]
    ro<tunnel, 1120, typename A::_txy, m3_per_s, m3_per_s> flow{this};                // x[m3_per_s],y[m3_per_s]
    ro<tunnel, 1121, typename A::_txy, m3_per_s, m3_per_s> physical_flow{this};       // x[m3_per_s],y[m3_per_s]
    ro<tunnel, 1122, typename A::_txy, m3_per_s, m3_per_s> sim_flow{this};            // x[m3_per_s],y[m3_per_s]
    ro<tunnel, 1123, typename A::_txy, no_unit, no_unit> gate_opening{this};          // x[no_unit],y[no_unit]
    ro<tunnel, 1124, int, no_unit, no_unit> network_no{this};                         // x[no_unit],y[no_unit]
    rw<tunnel, 1125, typename A::_txy, m3_per_s, m3_per_s> min_flow{this};            // x[m3_per_s],y[m3_per_s]
    rw<tunnel, 1126, typename A::_txy, m3_per_s, m3_per_s> max_flow{this};            // x[m3_per_s],y[m3_per_s]
    rw<tunnel, 1127, typename A::_txy, nok, nok> min_flow_penalty_cost{this};         // x[nok],y[nok]
    rw<tunnel, 1128, typename A::_txy, nok, nok> max_flow_penalty_cost{this};         // x[nok],y[nok]
    rw<tunnel, 1129, typename A::_txy, meter, meter> min_start_pressure{this};        // x[meter],y[meter]
  };

  template <class A>
  struct interlock_constraint : obj<A, 27> {
    using super = obj<A, 27>;
    interlock_constraint() = default;

    interlock_constraint(A* s, int oid)
      : super(s, oid) {
    }

    interlock_constraint(interlock_constraint const & o)
      : super(o) {
    }

    interlock_constraint(interlock_constraint&& o)
      : super(std::move(o)) {
    }

    interlock_constraint& operator=(interlock_constraint const & o) {
      super::operator=(o);
      return *this;
    }

    interlock_constraint& operator=(interlock_constraint&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<interlock_constraint, 1130, typename A::_txy, no_unit, no_unit> min_open{this};   // x[no_unit],y[no_unit]
    rw<interlock_constraint, 1131, typename A::_txy, no_unit, no_unit> max_open{this};   // x[no_unit],y[no_unit]
    rw<interlock_constraint, 1132, double, no_unit, no_unit> forward_switch_time{this};  // x[no_unit],y[no_unit]
    rw<interlock_constraint, 1133, double, no_unit, no_unit> backward_switch_time{this}; // x[no_unit],y[no_unit]
  };

  template <class A>
  struct flow_constraint : obj<A, 28> {
    using super = obj<A, 28>;
    flow_constraint() = default;

    flow_constraint(A* s, int oid)
      : super(s, oid) {
    }

    flow_constraint(flow_constraint const & o)
      : super(o) {
    }

    flow_constraint(flow_constraint&& o)
      : super(std::move(o)) {
    }

    flow_constraint& operator=(flow_constraint const & o) {
      super::operator=(o);
      return *this;
    }

    flow_constraint& operator=(flow_constraint&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }
  };

  template <class A>
  struct lp_model : obj<A, 29> {
    using super = obj<A, 29>;
    lp_model() = default;

    lp_model(A* s, int oid)
      : super(s, oid) {
    }

    lp_model(lp_model const & o)
      : super(o) {
    }

    lp_model(lp_model&& o)
      : super(std::move(o)) {
    }

    lp_model& operator=(lp_model const & o) {
      super::operator=(o);
      return *this;
    }

    lp_model& operator=(lp_model&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    rw<lp_model, 1134, int, no_unit, no_unit> sim_mode{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,1135,string_array,no_unit,no_unit> var_type_names{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,1136,string_array,no_unit,no_unit> var_type_abbrev{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1137, std::vector<int>, no_unit, no_unit> var_type_index_type_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1138, std::vector<int>, no_unit, no_unit> var_type_index_type_cnt{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1139, std::vector<int>, no_unit, no_unit> var_type_index_type_val{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,1140,string_array,no_unit,no_unit> row_type_names{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1141, std::vector<int>, no_unit, no_unit> row_type_index_type_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1142, std::vector<int>, no_unit, no_unit> row_type_index_type_cnt{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1143, std::vector<int>, no_unit, no_unit> row_type_index_type_val{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,1144,string_array,no_unit,no_unit> index_type_names{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1145, std::vector<int>, no_unit, no_unit> index_type_desc_beg{this};   // x[no_unit],y[no_unit]
    ro<lp_model, 1146, std::vector<int>, no_unit, no_unit> index_type_desc_cnt{this};   // x[no_unit],y[no_unit]
    ro<lp_model, 1147, std::vector<int>, no_unit, no_unit> index_type_desc_index{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,1148,string_array,no_unit,no_unit> index_type_desc_val{this}; // x[no_unit],y[no_unit]
    ro<lp_model, 1149, std::vector<double>, no_unit, no_unit> AA{this};             // x[no_unit],y[no_unit]
    ro<lp_model, 1150, std::vector<int>, no_unit, no_unit> Irow{this};              // x[no_unit],y[no_unit]
    ro<lp_model, 1151, std::vector<int>, no_unit, no_unit> Jcol{this};              // x[no_unit],y[no_unit]
    ro<lp_model, 1152, std::vector<double>, no_unit, no_unit> rhs{this};            // x[no_unit],y[no_unit]
    ro<lp_model, 1153, std::vector<int>, no_unit, no_unit> sense{this};             // x[no_unit],y[no_unit]
    ro<lp_model, 1154, std::vector<double>, no_unit, no_unit> ub{this};             // x[no_unit],y[no_unit]
    ro<lp_model, 1155, std::vector<double>, no_unit, no_unit> lb{this};             // x[no_unit],y[no_unit]
    ro<lp_model, 1156, std::vector<double>, no_unit, no_unit> cc{this};             // x[no_unit],y[no_unit]
    ro<lp_model, 1157, std::vector<int>, no_unit, no_unit> bin{this};               // x[no_unit],y[no_unit]
    ro<lp_model, 1158, std::vector<double>, no_unit, no_unit> x{this};              // x[no_unit],y[no_unit]
    ro<lp_model, 1159, std::vector<double>, no_unit, no_unit> dual{this};           // x[no_unit],y[no_unit]
    ro<lp_model, 1160, std::vector<int>, no_unit, no_unit> var_type{this};          // x[no_unit],y[no_unit]
    ro<lp_model, 1161, std::vector<int>, no_unit, no_unit> var_index_beg{this};     // x[no_unit],y[no_unit]
    ro<lp_model, 1162, std::vector<int>, no_unit, no_unit> var_index_cnt{this};     // x[no_unit],y[no_unit]
    ro<lp_model, 1163, std::vector<int>, no_unit, no_unit> var_index_val{this};     // x[no_unit],y[no_unit]
    ro<lp_model, 1164, std::vector<int>, no_unit, no_unit> row_type{this};          // x[no_unit],y[no_unit]
    ro<lp_model, 1165, std::vector<int>, no_unit, no_unit> row_index_beg{this};     // x[no_unit],y[no_unit]
    ro<lp_model, 1166, std::vector<int>, no_unit, no_unit> row_index_cnt{this};     // x[no_unit],y[no_unit]
    ro<lp_model, 1167, std::vector<int>, no_unit, no_unit> row_index_val{this};     // x[no_unit],y[no_unit]
    rw<lp_model, 1168, int, no_unit, no_unit> add_row_type{this};                   // x[no_unit],y[no_unit]
    rw<lp_model, 1169, std::vector<int>, no_unit, no_unit> add_row_index{this};     // x[no_unit],y[no_unit]
    rw<lp_model, 1170, std::vector<int>, no_unit, no_unit> add_row_variables{this}; // x[no_unit],y[no_unit]
    rw<lp_model, 1171, std::vector<double>, no_unit, no_unit> add_row_coeff{this};  // x[no_unit],y[no_unit]
    rw<lp_model, 1172, double, no_unit, no_unit> add_row_rhs{this};                 // x[no_unit],y[no_unit]
    rw<lp_model, 1173, int, no_unit, no_unit> add_row_sense{this};                  // x[no_unit],y[no_unit]
    ro<lp_model, 1174, int, no_unit, no_unit> add_row_last{this};                   // x[no_unit],y[no_unit]
    rw<lp_model, 1175, int, no_unit, no_unit> add_var_type{this};                   // x[no_unit],y[no_unit]
    rw<lp_model, 1176, std::vector<int>, no_unit, no_unit> add_var_index{this};     // x[no_unit],y[no_unit]
    rw<lp_model, 1177, double, no_unit, no_unit> add_var_ub{this};                  // x[no_unit],y[no_unit]
    rw<lp_model, 1178, double, no_unit, no_unit> add_var_lb{this};                  // x[no_unit],y[no_unit]
    rw<lp_model, 1179, double, no_unit, no_unit> add_var_cc{this};                  // x[no_unit],y[no_unit]
    rw<lp_model, 1180, int, no_unit, no_unit> add_var_bin{this};                    // x[no_unit],y[no_unit]
    ro<lp_model, 1181, int, no_unit, no_unit> add_var_last{this};                   // x[no_unit],y[no_unit]
  };

  template <class A>
  struct river : obj<A, 30> {
    using super = obj<A, 30>;
    river() = default;

    river(A* s, int oid)
      : super(s, oid) {
    }

    river(river const & o)
      : super(o) {
    }

    river(river&& o)
      : super(std::move(o)) {
    }

    river& operator=(river const & o) {
      super::operator=(o);
      return *this;
    }

    river& operator=(river&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<river, 1182, double, meter, meter> length{this};                                 // x[meter],y[meter]
    rw<river, 1183, double, meter, meter> upstream_elevation{this};                     // x[meter],y[meter]
    rw<river, 1184, double, meter, meter> downstream_elevation{this};                   // x[meter],y[meter]
    rw<river, 1185, double, hour, hour> time_delay_const{this};                         // x[hour],y[hour]
    rw<river, 1186, double, nok_per_mwh, nok_per_mwh> delayed_water_energy_value{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<river, 1187, double, no_unit, no_unit> initial_gate_opening{this};               // x[no_unit],y[no_unit]
    rw<river, 1188, double, m3_per_s, m3_per_s> linear_ref_flow{this};                  // x[m3_per_s],y[m3_per_s]
    rw<river, 1189, double, no_unit, no_unit> implicit_factor{this};                    // x[no_unit],y[no_unit]
    rw<river, 1190, int, no_unit, no_unit> main_river{this};                            // x[no_unit],y[no_unit]
    rw<river, 1191, int, no_unit, no_unit> submerged_weir{this};                        // x[no_unit],y[no_unit]
    rw<river, 1192, int, no_unit, no_unit> linear_submerged_weir_flow{this};            // x[no_unit],y[no_unit]
    rw<river, 1193, typename A::_xy, meter, meter> width_depth_curve{this};             // x[meter],y[meter]
    rw<river, 1194, typename A::_xy, m3_per_s, nok_per_m3_per_s> flow_cost_curve{
      this}; // x[m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1195, typename A::_xy, m3_per_s, nok_per_m3_per_s> peak_flow_cost_curve{
      this}; // x[m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1196, std::vector<typename A::_xy>, hour, m3_per_s> time_delay_curve{this};    // x[hour],y[m3_per_s]
    rw<river, 1197, typename A::_txy, no_unit, m3_per_s> past_upstream_flow{this};           // x[no_unit],y[m3_per_s]
    rw<river, 1198, std::vector<typename A::_xy>, meter, m3_per_s> up_head_flow_curve{this}; // x[meter],y[m3_per_s]
    rw<river, 1199, typename A::_xy, no_unit, meter> gate_opening_curve{this};               // x[no_unit],y[meter]
    rw<river, 1200, std::vector<typename A::_xy>, meter, m3_per_s> delta_head_ref_up_flow_curve{
      this}; // x[meter],y[m3_per_s]
    rw<river, 1201, std::vector<typename A::_xy>, meter, m3_per_s> delta_head_ref_down_flow_curve{
      this};                                                              // x[meter],y[m3_per_s]
    rw<river, 1202, typename A::_txy, m3_per_s, m3_per_s> inflow{this};   // x[m3_per_s],y[m3_per_s]
    rw<river, 1203, typename A::_txy, m3_per_s, m3_per_s> min_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<river, 1204, typename A::_txy, m3_per_s, m3_per_s> max_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<river, 1205, typename A::_txy, nok_per_m3_per_s, nok_per_m3_per_s> min_flow_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1206, typename A::_txy, nok_per_m3_per_s, nok_per_m3_per_s> max_flow_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1207, typename A::_txy, m3sec_hour, m3sec_hour> ramping_up{this};   // x[m3sec_hour],y[m3sec_hour]
    rw<river, 1208, typename A::_txy, m3sec_hour, m3sec_hour> ramping_down{this}; // x[m3sec_hour],y[m3sec_hour]
    rw<river, 1209, typename A::_txy, nok_per_m3_per_s, nok_per_m3_per_s> ramping_up_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1210, typename A::_txy, nok_per_m3_per_s, nok_per_m3_per_s> ramping_down_penalty_cost{
      this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1211, typename A::_txy, nok_per_m3_per_s, nok_per_m3_per_s> cost_curve_scaling{
      this};                                                                  // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river, 1212, typename A::_txy, no_unit, m3_per_s> flow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<river, 1213, typename A::_txy, no_unit, nok_per_m3_per_s> flow_schedule_penalty_cost{
      this};                                                                         // x[no_unit],y[nok_per_m3_per_s]
    rw<river, 1214, typename A::_txy, no_unit, no_unit> gate_opening_schedule{this}; // x[no_unit],y[no_unit]
    rw<river, 1215, typename A::_txy, no_unit, m3_per_s> flow_block_merge_tolerance{this}; // x[no_unit],y[m3_per_s]
    rw<river, 1216, typename A::_txy, no_unit, meter_per_hour> gate_ramping{this}; // x[no_unit],y[meter_per_hour]
    rw<river, 1217, typename A::_txy, no_unit, nok_per_meter_hour> gate_ramping_penalty_cost{
      this}; // x[no_unit],y[nok_per_meter_hour]
    rw<river, 1218, typename A::_txy, no_unit, nok_per_meter> gate_adjustment_cost{this}; // x[no_unit],y[nok_per_meter]
    rw<river, 1219, typename A::_txy, no_unit, nok_per_m3_per_s> flow_cost{this};  // x[no_unit],y[nok_per_m3_per_s]
    rw<river, 1220, typename A::_txy, no_unit, no_unit> mip_flag{this};            // x[no_unit],y[no_unit]
    ro<river, 1221, typename A::_txy, m3_per_s, m3_per_s> flow{this};              // x[m3_per_s],y[m3_per_s]
    ro<river, 1222, typename A::_txy, m3_per_s, m3_per_s> upstream_flow{this};     // x[m3_per_s],y[m3_per_s]
    ro<river, 1223, typename A::_txy, m3_per_s, m3_per_s> downstream_flow{this};   // x[m3_per_s],y[m3_per_s]
    ro<river, 1224, typename A::_txy, meter, meter> gate_height{this};             // x[meter],y[meter]
    ro<river, 1225, typename A::_txy, nok, nok> min_flow_penalty{this};            // x[nok],y[nok]
    ro<river, 1226, typename A::_txy, nok, nok> max_flow_penalty{this};            // x[nok],y[nok]
    ro<river, 1227, typename A::_txy, nok, nok> ramping_up_penalty{this};          // x[nok],y[nok]
    ro<river, 1228, typename A::_txy, nok, nok> ramping_down_penalty{this};        // x[nok],y[nok]
    ro<river, 1229, typename A::_txy, no_unit, nok> flow_penalty{this};            // x[no_unit],y[nok]
    ro<river, 1230, typename A::_txy, no_unit, nok> peak_flow_penalty{this};       // x[no_unit],y[nok]
    ro<river, 1231, typename A::_txy, no_unit, nok> gate_ramping_penalty{this};    // x[no_unit],y[nok]
    ro<river, 1232, typename A::_txy, no_unit, nok> gate_adjustment_penalty{this}; // x[no_unit],y[nok]
    ro<river, 1233, typename A::_txy, no_unit, nok> flow_schedule_penalty{this};   // x[no_unit],y[nok]
    ro<river, 1234, typename A::_txy, no_unit, m3_per_s> physical_flow{this};      // x[no_unit],y[m3_per_s]
    ro<river, 1235, typename A::_txy, no_unit, m3_per_s> initial_downstream_flow{this};    // x[no_unit],y[m3_per_s]
    ro<river, 1236, typename A::_xy, hour, m3_per_s> distributed_past_upstream_flow{this}; // x[hour],y[m3_per_s]
    ro<river, 1237, typename A::_txy, no_unit, m3_per_s> sim_flow{this};                   // x[no_unit],y[m3_per_s]
  };

  template <class A>
  struct busbar : obj<A, 31> {
    using super = obj<A, 31>;
    busbar() = default;

    busbar(A* s, int oid)
      : super(s, oid) {
    }

    busbar(busbar const & o)
      : super(o) {
    }

    busbar(busbar&& o)
      : super(std::move(o)) {
    }

    busbar& operator=(busbar const & o) {
      super::operator=(o);
      return *this;
    }

    busbar& operator=(busbar&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<busbar, 1246, typename A::_txy, no_unit, mw> load{this}; // x[no_unit],y[mw]
    //--TODO: rw<busbar,1247,sy,no_unit,no_unit> ptdf{this}; // x[no_unit],y[no_unit]
    ro<busbar, 1248, typename A::_txy, no_unit, nok_per_mwh> energy_price{this}; // x[no_unit],y[nok_per_mwh]
    ro<busbar, 1249, typename A::_txy, no_unit, mwh> power_deficit{this};        // x[no_unit],y[mwh]
    ro<busbar, 1250, typename A::_txy, no_unit, mwh> power_excess{this};         // x[no_unit],y[mwh]
  };

  template <class A>
  struct ac_line : obj<A, 32> {
    using super = obj<A, 32>;
    ac_line() = default;

    ac_line(A* s, int oid)
      : super(s, oid) {
    }

    ac_line(ac_line const & o)
      : super(o) {
    }

    ac_line(ac_line&& o)
      : super(std::move(o)) {
    }

    ac_line& operator=(ac_line const & o) {
      super::operator=(o);
      return *this;
    }

    ac_line& operator=(ac_line&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<ac_line, 1251, typename A::_txy, no_unit, mw> max_forward_flow{this};  // x[no_unit],y[mw]
    rw<ac_line, 1252, typename A::_txy, no_unit, mw> max_backward_flow{this}; // x[no_unit],y[mw]
    rw<ac_line, 1253, int, no_unit, no_unit> n_loss_segments{this};           // x[no_unit],y[no_unit]
    rw<ac_line, 1254, double, no_unit, no_unit> loss_factor_linear{this};     // x[no_unit],y[no_unit]
    rw<ac_line, 1255, double, no_unit, no_unit> loss_factor_quadratic{this};  // x[no_unit],y[no_unit]
    ro<ac_line, 1256, typename A::_txy, no_unit, mw> flow{this};              // x[no_unit],y[mw]
    ro<ac_line, 1257, typename A::_txy, no_unit, mw> forward_loss{this};      // x[no_unit],y[mw]
    ro<ac_line, 1258, typename A::_txy, no_unit, mw> backward_loss{this};     // x[no_unit],y[mw]
  };

  template <class A>
  struct dc_line : obj<A, 33> {
    using super = obj<A, 33>;
    dc_line() = default;

    dc_line(A* s, int oid)
      : super(s, oid) {
    }

    dc_line(dc_line const & o)
      : super(o) {
    }

    dc_line(dc_line&& o)
      : super(std::move(o)) {
    }

    dc_line& operator=(dc_line const & o) {
      super::operator=(o);
      return *this;
    }

    dc_line& operator=(dc_line&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<dc_line, 1259, typename A::_txy, no_unit, mw> max_forward_flow{this};  // x[no_unit],y[mw]
    rw<dc_line, 1260, typename A::_txy, no_unit, mw> max_backward_flow{this}; // x[no_unit],y[mw]
    rw<dc_line, 1261, int, no_unit, no_unit> n_loss_segments{this};           // x[no_unit],y[no_unit]
    rw<dc_line, 1262, double, no_unit, no_unit> loss_constant{this};          // x[no_unit],y[no_unit]
    rw<dc_line, 1263, double, no_unit, no_unit> loss_factor_linear{this};     // x[no_unit],y[no_unit]
    rw<dc_line, 1264, double, no_unit, no_unit> loss_factor_quadratic{this};  // x[no_unit],y[no_unit]
    ro<dc_line, 1265, typename A::_txy, no_unit, mw> flow{this};              // x[no_unit],y[mw]
    ro<dc_line, 1266, typename A::_txy, no_unit, mw> forward_loss{this};      // x[no_unit],y[mw]
    ro<dc_line, 1267, typename A::_txy, no_unit, mw> backward_loss{this};     // x[no_unit],y[mw]
  };

  template <class A>
  struct gen_reserve_capability : obj<A, 34> {
    using super = obj<A, 34>;
    gen_reserve_capability() = default;

    gen_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    gen_reserve_capability(gen_reserve_capability const & o)
      : super(o) {
    }

    gen_reserve_capability(gen_reserve_capability&& o)
      : super(std::move(o)) {
    }

    gen_reserve_capability& operator=(gen_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    gen_reserve_capability& operator=(gen_reserve_capability&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<gen_reserve_capability, 1313, std::string, no_unit, no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<gen_reserve_capability, 1314, typename A::_txy, no_unit, mw> p_extended{this};        // x[no_unit],y[mw]
    rw<gen_reserve_capability, 1315, typename A::_txy, no_unit, mw> schedule{this};          // x[no_unit],y[mw]
  };

  template <class A>
  struct pump_reserve_capability : obj<A, 35> {
    using super = obj<A, 35>;
    pump_reserve_capability() = default;

    pump_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    pump_reserve_capability(pump_reserve_capability const & o)
      : super(o) {
    }

    pump_reserve_capability(pump_reserve_capability&& o)
      : super(std::move(o)) {
    }

    pump_reserve_capability& operator=(pump_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    pump_reserve_capability& operator=(pump_reserve_capability&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<pump_reserve_capability, 1316, std::string, no_unit, no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<pump_reserve_capability, 1317, typename A::_txy, no_unit, mw> p_extended{this};        // x[no_unit],y[mw]
    rw<pump_reserve_capability, 1318, typename A::_txy, no_unit, mw> schedule{this};          // x[no_unit],y[mw]
  };

  template <class A>
  struct plant_reserve_capability : obj<A, 36> {
    using super = obj<A, 36>;
    plant_reserve_capability() = default;

    plant_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    plant_reserve_capability(plant_reserve_capability const & o)
      : super(o) {
    }

    plant_reserve_capability(plant_reserve_capability&& o)
      : super(std::move(o)) {
    }

    plant_reserve_capability& operator=(plant_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    plant_reserve_capability& operator=(plant_reserve_capability&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<plant_reserve_capability, 1319, std::string, no_unit, no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<plant_reserve_capability, 1320, typename A::_txy, no_unit, mw> p_gen_extended{this};    // x[no_unit],y[mw]
    rw<plant_reserve_capability, 1321, typename A::_txy, no_unit, mw> p_pump_extended{this};   // x[no_unit],y[mw]
    rw<plant_reserve_capability, 1322, typename A::_txy, no_unit, mw> schedule{this};          // x[no_unit],y[mw]
  };

  template <class A>
  struct needle_comb_reserve_capability : obj<A, 37> {
    using super = obj<A, 37>;
    needle_comb_reserve_capability() = default;

    needle_comb_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    needle_comb_reserve_capability(needle_comb_reserve_capability const & o)
      : super(o) {
    }

    needle_comb_reserve_capability(needle_comb_reserve_capability&& o)
      : super(std::move(o)) {
    }

    needle_comb_reserve_capability& operator=(needle_comb_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    needle_comb_reserve_capability& operator=(needle_comb_reserve_capability&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<needle_comb_reserve_capability, 1323, std::string, no_unit, no_unit> reserve_type_name{
      this};                                                                                  // x[no_unit],y[no_unit]
    rw<needle_comb_reserve_capability, 1324, typename A::_txy, no_unit, mw> p_extended{this}; // x[no_unit],y[mw]
  };

  template <class A>
  struct battery : obj<A, 38> {
    using super = obj<A, 38>;
    battery() = default;

    battery(A* s, int oid)
      : super(s, oid) {
    }

    battery(battery const & o)
      : super(o) {
    }

    battery(battery&& o)
      : super(std::move(o)) {
    }

    battery& operator=(battery const & o) {
      super::operator=(o);
      return *this;
    }

    battery& operator=(battery&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<battery, 1268, double, no_unit, no_unit> charge_efficiency{this};              // x[no_unit],y[no_unit]
    rw<battery, 1269, double, no_unit, no_unit> discharge_efficiency{this};           // x[no_unit],y[no_unit]
    rw<battery, 1270, double, mw, mw> max_charge_power{this};                         // x[mw],y[mw]
    rw<battery, 1271, double, mw, mw> max_discharge_power{this};                      // x[mw],y[mw]
    rw<battery, 1272, double, mwh, mwh> max_energy{this};                             // x[mwh],y[mwh]
    rw<battery, 1273, double, mwh, mwh> initial_energy{this};                         // x[mwh],y[mwh]
    rw<battery, 1274, double, nok_per_mwh, nok_per_mwh> discharge_cost{this};         // x[nok_per_mwh],y[nok_per_mwh]
    rw<battery, 1275, double, nok_per_mwh, nok_per_mwh> charge_cost{this};            // x[nok_per_mwh],y[nok_per_mwh]
    rw<battery, 1276, typename A::_txy, mw, mw> max_charge_power_constraint{this};    // x[mw],y[mw]
    rw<battery, 1277, typename A::_txy, mw, mw> max_discharge_power_constraint{this}; // x[mw],y[mw]
    rw<battery, 1278, typename A::_txy, mwh, mwh> max_energy_constraint{this};        // x[mwh],y[mwh]
    rw<battery, 1279, typename A::_txy, mwh, mwh> min_energy_constraint{this};        // x[mwh],y[mwh]
    rw<battery, 1280, typename A::_txy, no_unit, no_unit> charge_discharge_mip{this}; // x[no_unit],y[no_unit]
    rw<battery, 1281, typename A::_txy, mw, mw> charge_schedule{this};                // x[mw],y[mw]
    rw<battery, 1282, typename A::_txy, mw, mw> discharge_schedule{this};             // x[mw],y[mw]
    rw<battery, 1283, typename A::_txy, mw, mw> net_discharge_schedule{this};         // x[mw],y[mw]
    rw<battery, 1284, typename A::_txy, no_unit, nok_per_mw> fcr_n_up_cost{this};     // x[no_unit],y[nok_per_mw]
    rw<battery, 1285, typename A::_txy, no_unit, nok_per_mw> fcr_n_down_cost{this};   // x[no_unit],y[nok_per_mw]
    rw<battery, 1286, typename A::_txy, no_unit, nok_per_mw> fcr_d_up_cost{this};     // x[no_unit],y[nok_per_mw]
    rw<battery, 1287, typename A::_txy, no_unit, nok_per_mw> fcr_d_down_cost{this};   // x[no_unit],y[nok_per_mw]
    rw<battery, 1288, typename A::_txy, no_unit, nok_per_mw> frr_up_cost{this};       // x[no_unit],y[nok_per_mw]
    rw<battery, 1289, typename A::_txy, no_unit, nok_per_mw> frr_down_cost{this};     // x[no_unit],y[nok_per_mw]
    rw<battery, 1290, typename A::_txy, no_unit, nok_per_mw> rr_up_cost{this};        // x[no_unit],y[nok_per_mw]
    rw<battery, 1291, typename A::_txy, no_unit, nok_per_mw> rr_down_cost{this};      // x[no_unit],y[nok_per_mw]
    ro<battery, 1292, typename A::_txy, mw, mw> power_charge{this};                   // x[mw],y[mw]
    ro<battery, 1293, typename A::_txy, mw, mw> power_discharge{this};                // x[mw],y[mw]
    ro<battery, 1294, typename A::_txy, mw, mw> net_power_discharge{this};            // x[mw],y[mw]
    ro<battery, 1295, typename A::_txy, mwh, mwh> energy{this};                       // x[mwh],y[mwh]
    ro<battery, 1296, typename A::_txy, nok_per_mwh, nok_per_mwh> energy_value{this}; // x[nok_per_mwh],y[nok_per_mwh]
    ro<battery, 1297, typename A::_txy, no_unit, mw> fcr_n_up_delivery{this};         // x[no_unit],y[mw]
    ro<battery, 1298, typename A::_txy, no_unit, mw> fcr_n_down_delivery{this};       // x[no_unit],y[mw]
    ro<battery, 1299, typename A::_txy, no_unit, mw> fcr_d_up_delivery{this};         // x[no_unit],y[mw]
    ro<battery, 1300, typename A::_txy, no_unit, mw> fcr_d_down_delivery{this};       // x[no_unit],y[mw]
    ro<battery, 1301, typename A::_txy, no_unit, mw> frr_up_delivery{this};           // x[no_unit],y[mw]
    ro<battery, 1302, typename A::_txy, no_unit, mw> frr_down_delivery{this};         // x[no_unit],y[mw]
    ro<battery, 1303, typename A::_txy, no_unit, mw> rr_up_delivery{this};            // x[no_unit],y[mw]
    ro<battery, 1304, typename A::_txy, no_unit, mw> rr_down_delivery{this};          // x[no_unit],y[mw]
  };

  template <class A>
  struct wind : obj<A, 39> {
    using super = obj<A, 39>;
    wind() = default;

    wind(A* s, int oid)
      : super(s, oid) {
    }

    wind(wind const & o)
      : super(o) {
    }

    wind(wind&& o)
      : super(std::move(o)) {
    }

    wind& operator=(wind const & o) {
      super::operator=(o);
      return *this;
    }

    wind& operator=(wind&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<wind, 1305, typename A::_txy, mw, mw> power_forecast{this};                     // x[mw],y[mw]
    rw<wind, 1306, typename A::_txy, nok_per_mwh, nok_per_mwh> curtailment_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    ro<wind, 1307, typename A::_txy, mw, mw> power_delivered{this};                    // x[mw],y[mw]
    ro<wind, 1308, typename A::_txy, mw, mw> power_curtailed{this};                    // x[mw],y[mw]
  };

  template <class A>
  struct solar : obj<A, 40> {
    using super = obj<A, 40>;
    solar() = default;

    solar(A* s, int oid)
      : super(s, oid) {
    }

    solar(solar const & o)
      : super(o) {
    }

    solar(solar&& o)
      : super(std::move(o)) {
    }

    solar& operator=(solar const & o) {
      super::operator=(o);
      return *this;
    }

    solar& operator=(solar&& o) {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<solar, 1309, typename A::_txy, mw, mw> power_forecast{this};                     // x[mw],y[mw]
    rw<solar, 1310, typename A::_txy, nok_per_mwh, nok_per_mwh> curtailment_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    ro<solar, 1311, typename A::_txy, mw, mw> power_delivered{this};                    // x[mw],y[mw]
    ro<solar, 1312, typename A::_txy, mw, mw> power_curtailed{this};                    // x[mw],y[mw]
  };

}
