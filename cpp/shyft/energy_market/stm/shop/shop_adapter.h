/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
/*
 * Adapting stm types to shop api.
 */
#pragma once
#include <algorithm>
#include <cmath>
#include <map>
#include <memory>
#include <ranges>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/shop/shop_types.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/contract.h>

namespace shyft::energy_market::stm::shop {

  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using shyft::time_series::dd::apoint_ts;
  using hydro_power::xy_point_curve;
  using hydro_power::xy_point_curve_with_z;
  using hydro_power::connection_role;

  /** @brief adapter for exchanging object and their attribute between stm and shop
   *  @details
   *  This class acts as a helper for the shop-emitter,
   *  especially in the emitter-phase: .to_shop(object..) and then in the collector phase .from_shop methods.
   *  Things that this class resolves are like:
   *   # the stm have temporal variables for effiency curves, so we map the version available at the start of the
   * time_axis # some attributes in the stm are optional, and is mapped optionally # some results after stepping the
   * opt-engine is optional, thus we collect them optionally.
   *
   */
  struct shop_adapter {
    shop_api& api; ///< upward not owning reference to the shop_api (as provided by the emitter.
    shyft::time_axis::generic_dt time_axis; ///< the shop run time-axis
    utctime time_delay_unit;                ///< the shop time resolution for time delay values
    mutable int ema_prod_area_id{1}; ///< seems that the only solution that works stable is to give markets strictly
                                     ///< increasing prod_areas, starting from 1(!)
    mutable std::map<int, std::vector<shop_market>> ema_map;
    bool enforce_single_operating_zone = false;

    shop_adapter(shop_api& api, shyft::time_axis::generic_dt const & ta)
      : api{api}
      , time_axis{ta} {
      if (!time_axis.total_period().valid())
        throw std::runtime_error("Creating shop adapter needs a valid time-axis");
      // Configure time delay handling to use finest possible fixed resolution,
      // currently minutes, instead of the default which is according to timestep length!
      time_delay_unit = shyft::core::utctime_from_seconds64(60);
    }

    /** utility to create a one-step constant ts for the total period of the adapter.time_axis */
    apoint_ts mk_constant_ts(double value) const {
      return apoint_ts{
        shyft::time_axis::point_dt{{time_axis.total_period().start, time_axis.total_period().end}},
        value,
        shyft::time_series::POINT_AVERAGE_VALUE};
    }

    /** returns the time_axis.total_period() */
    utcperiod period() const {
      return time_axis.total_period();
    }

    using ts_t =
      apoint_ts; // Time series, which may be "series-like" time-varying values, or it may be temporal scalar value.
    using ts_value_t = double;

    template <typename V>
    using tv_t = t_T<V>; // Temporal stm attributes represented by map:utctime->v (t_double_ (legacy), t_xy_, t_xyz_,
                         // t_xyz_list_ and t_turbine_description_)

    template <typename O, int a, typename V, typename Ux, typename Uy>
    using shop_r_t = ::shop::ro<O, a, V, Ux, Uy>;
    template <typename O, int a, typename V, typename Ux, typename Uy>
    using shop_w_t = ::shop::rw<O, a, V, Ux, Uy>;

    // Checking exists, only relevant for stm attributes (but anything can be checked)
    template <typename V>
    static bool exists(V const &) {
      return true;
    }

    // Check that time series is not empty
    static bool exists(ts_t const & v) {
      return (v.ts && !v.needs_bind()) ? true : false;
    }

    // empty string_views do not exist:
    static bool exists(std::string_view v) {
      return !v.empty();
    }

    // Check that temporal attribute is not empty (shared_ptr non-null and contained map is not empty)
    template <typename V>
    static bool exists(tv_t<V> const & tv) {
      return tv && !tv->empty();
    }

    // Checking valid specifically for non-temporal values (just checking exists so not realy a validity check)
    template <typename V>
    static bool valid(V const & v) {
      return exists(v);
    }

    // Checking valid specifically for temporal values, with explicit valid time
    static bool valid_temporal(ts_t const & ts, utctime t) {
      return exists(ts) && ts.total_period().contains(t);
    } // Should only be called for time series representing temporal attribute value

    template <typename V>
    static bool valid_temporal(tv_t<V> const & tv, utctime t) {
      return exists(tv) && tv->begin()->first <= t;
    }

    // Checking valid specifically for temporal values, with implicit valid time according to current period
    template <typename V>
    bool valid_temporal(V const & v) const {
      return valid_temporal(v, time_axis.time(0));
    }

    // Checking valid for implicitely deduced temporal values, with implicit valid time according to current period
    // Note that this only works for temporal types t_T (map:utctime->v), time series must be explicitely checked
    // as temporal since it could be a non-temporal regular time series value
    template <typename V>
    bool valid(tv_t<V> const & tv) const {
      return valid_temporal(tv);
    }

    // Check for any values, not considering nan or infinite.
    // Note: Currently only implemented for stm attributes, and only used as a common utility from emitter etc.
    static bool has_values(ts_t const & v) {
      // False if time series object is empty or describes no points.
      if (v.size() == 0)
        return false;
      // True if time series has any non-nan values.
      auto const values = v.values(); // Note: Throws exception if unbound, check needs_bind() first to avoid it.
      return std::ranges::any_of(values, [](double const & v) {
        return std::isfinite(v);
      });
    };

    /* Section with 'getter methods' both temporal and non-temporal that helps getting the 'value' type out from stm.
     * some of them require mapping from time-variant structures to simple structure as indicated as this class
     * functional requirements.
     */
    template <typename V>
    static V const & get(V const & v) {
      return v;
    } // Basic value

    template <typename O, int a, typename V, typename Ux, typename Uy>
    V get(shop_r_t<O, a, V, Ux, Uy>& v) {
      return (V) v;
    } // Shop attribute value

    // Get value of implicit temporal types t_T (map:utctime->v), using start of current period as implicit valid time.
    template <typename V, typename... Args>
    V get(tv_t<V> const & v, Args... args) const {
      return get_temporal(v, time_axis.time(0), args...);
    }

    // Get value of explicit temporal type, using start of current period as implicit valid time. This is the only way
    // to get value of temporal time series, since it cannot implicitely known if it is a regular time series
    // attribute or a temporal attribute stored as a time series.
    template <typename V, typename... Args>
    auto get_temporal(V const & v, Args... args) const {
      return get_temporal(v, time_axis.time(0), args...);
    }

    // Static helpers for looking up value of temporal attributes valid at specified time
    template <typename V, typename... Args>
    static auto get(V const & v, Args... args) { // Temporal value (implicit when additional arguments
      return get_temporal(v, args...);
    }

    // Specific overload to avoid utctime argument being handled as
    // part of args... after period.start in member version!
    template <typename V, typename... Args>
    static V get(tv_t<V> const & tv, utctime t, Args... args) {
      return get_temporal(tv, t, args...);
    }

    // Get value valid at t from time series (throws if not valid)
    static ts_value_t get_temporal(ts_t const & ts, utctime t) {
      if (!ts)
        throw std::runtime_error(fmt::format("Temporal attribute is empty, hence not valid at time {}", t.count()));
      if (!ts.total_period().contains(t))
        throw std::runtime_error(fmt::format("Temporal attribute is not valid at time {}", t.count()));
      return ts(t);
    }

    static ts_value_t get_temporal(
      ts_t const & ts,
      utctime t,
      ts_value_t const & invalid_v) { // Get value valid at t, or specified value if none
      if (!ts)
        return invalid_v;
      // Implictly handles the case of !ts (total_period then returns an empty period)
      if (!ts.total_period().contains(t))
        return invalid_v;
      return ts(t);
    }

    // Get value valid at t, or specified value if none, or specifed value if nan
    static ts_value_t get_temporal(ts_t const & ts, utctime t, ts_value_t const & invalid_v, ts_value_t const & nan_v) {
      if (!ts)
        return invalid_v;
      if (!ts.total_period().contains(t))
        return invalid_v;
      ts_value_t v = ts(t);
      return std::isfinite(v) ? v : nan_v;
    }

    template <typename V>
    static V get_temporal(tv_t<V> const & tv, utctime t) { // Get value valid at t (throws if not valid)
      if (!tv)
        throw std::runtime_error(fmt::format("Temporal attribute is not set, hence not valid at time {}", t.count()));
      auto it = tv->lower_bound(t);
      if (it == tv->cend()) {
        if (tv->empty())
          throw std::runtime_error(
            fmt::format("Temporal attribute is empty, hence not valid at time {}", t.count())); // Never valid
        --it;
      } else if (it->first > t) {
        if (it == tv->cbegin())
          throw std::runtime_error(
            fmt::format("Temporal attribute is not valid at time {}", t.count())); // Only valid after t
        --it;
      }
      return it->second;
    }

    // Get value valid at t, or specified value if none
    template <typename V>
    static V get_temporal(tv_t<V> const & tv, utctime t, V const & invalid_v) {
      if (!tv)
        return invalid_v; // Return default value since attribute is not set
      auto it = tv->lower_bound(t);
      if (it == tv->cend()) {
        if (tv->empty())
          return invalid_v; // Return default value since attribute is empty (no validity segments)
        --it;
      } else if (it->first > t) {
        if (it == tv->cbegin())
          return invalid_v; // Return default value since attribute only has values valid after t
        --it;
      }
      return it->second;
    }

    /* Section for 'setters', that validates the stm atribute to set, before extracting the value and injecting it to
     shop

     Check if specified source is valid considering the destination, which will implicit deduce temporal values and
     consider according to current period Note that it does not tell if the source can actually be assigned to the
     destination, it just checks the validity of the source!
     */
    template <typename D, typename S>
    bool valid_to_set(D const & /*d*/, S const & s) const {
      return valid(s);
    }

    // Temporal types time series, assuming the time series represents a temporal attribute value and not "time series
    // data" since destination is scalar double, using start of current period as implicite valid time
    bool valid_to_set(ts_value_t const &, ts_t const & s) const {
      return valid_temporal(s, time_axis.time(0));
    }

    template <typename DO, int da, typename Ux, typename Uy>
    bool valid_to_set(shop_w_t<DO, da, ts_value_t, Ux, Uy> const & /*d*/, ts_t const & s) const {
      return valid_temporal(s, time_axis.time(0));
    }

    // Basic assignment/copy
    template <typename D, typename S>
    D& set(D& d, S const & s) const {
      d = s;
      return d;
    }

    // Set shop attributes from stm attributes where some additional conversion is needed
    template <typename DO, int da, typename V, typename Ux, typename Uy>
    shop_w_t<DO, da, V, Ux, Uy>& set(shop_w_t<DO, da, V, Ux, Uy>& d, std::shared_ptr<V> const & s) const {
      // Dereference shared pointer before assigning to Shop attribute
      d = *s;
      return d;
    }

    // Convert plain xy into xyz (with z=0) before assigning to Shop attribute
    template <typename DO, int da, typename Ux, typename Uy>
    shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>&
      set(shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>& d, xy_point_curve const & s) const {
      d = xy_point_curve_with_z{s, 0.0};
      return d;
    }

    template <typename DO, int da, typename Ux, typename Uy>
    shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>&
      set(shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>& d, xy_point_curve_ const & s) const {
      // Dereference shared pointer xy to xyz mapping above
      set(d, *s);
      return d;
    }

    template <typename DO, int da, typename Ux, typename Uy>
    shop_w_t<DO, da, std::map<time_t, xy_point_curve_with_z>, Ux, Uy>&
      set(shop_w_t<DO, da, std::map<time_t, xy_point_curve_with_z>, Ux, Uy>& d, t_xy_ const & s) const {
      // Convert t_xy_ with map key type utctime into xyt with map key type time_t before assigning to Shop attribute
      std::map<time_t, xy_point_curve_with_z> a;
      for (auto const& [time, xy] : *s) {
        a.emplace(shyft::core::to_seconds64(time), xy_point_curve_with_z{*xy, 0.0});
      }
      d = a;
      return d;
    }

    // Set temporal types t_T (map:utctime->v) using start of current period as implicite valid time
    template <typename D, typename V, typename... Args>
    D& set(D& d, tv_t<V> const & s, Args... args) const {
      return set(d, get(s, time_axis.time(0), args...));
    }

    // Set temporal types time series, assuming the time series represents a temporal attribute value and not "time
    // series data" since destination is scalar double, using start of current period as implicite valid time
    template <typename... Args>
    ts_value_t& set(ts_value_t& d, ts_t const & s, Args... args) const {
      return set(d, get(s, time_axis.time(0), args...));
    }

    template <typename DO, int da, typename Ux, typename Uy, typename... Args>
    shop_w_t<DO, da, ts_value_t, Ux, Uy>&
      set(shop_w_t<DO, da, ts_value_t, Ux, Uy>& d, ts_t const & s, Args... args) const {
      return set(d, get(s, time_axis.time(0), args...));
    }

    // Set string_view on string attribute:
    template <typename DO, int da, typename Ux, typename Uy>
    shop_w_t<DO, da, std::string, Ux, Uy>& set(shop_w_t<DO, da, std::string, Ux, Uy>& d, std::string_view s) const {
      api.set<Ux, Uy>(d.p->id, d.aid, s);
      return d;
    }

    // Set stm attributes from shop attributes where som additional conversion is needed
    template <typename DO, int da, typename V, typename Ux, typename Uy, typename D>
    D& set(D& d, shop_w_t<DO, da, V, Ux, Uy> const & s) const {
      // Helper needed for template specialization/overload resolution to work correct,
      // simply upcasting shop_w_t to shop_r_t, which enables more specialized variants
      // to consistently use shop_r_t as source type while also handling shop_w_t.
      return set(d, shop_r_t<DO, da, V, Ux, Uy>(s));
    }

    template <typename DO, int da, typename V, typename Ux, typename Uy>
    std::shared_ptr<V>& set(std::shared_ptr<V>& d, shop_r_t<DO, da, V, Ux, Uy> const & s) const {
      // Assigning value into shared_ptr
      if (!d)
        d = std::make_shared<V>();
      return *d = s;
    }

    template <typename DO, int da, typename V, typename Ux, typename Uy>
    tv_t<V>& set(tv_t<V>& d, shop_r_t<DO, da, V, Ux, Uy> const & s) const {
      // Convert non-temporal value into temporal before assigning to stm attribute
      if (d)
        d->clear();
      else
        d = std::make_shared<tv_t<V>::element_type>();
      d->emplace(time_axis.time(0), s);
      return d;
    }

    template <typename DO, int da, typename V, typename Ux, typename Uy>
    t_xy_& set(t_xy_& d, shop_r_t<DO, da, V, Ux, Uy> const & s) const {
      // Convert xyz into plain xy before assigning to stm attribute
      if (d)
        d->clear();
      else
        d = std::make_shared<t_xy_::element_type>();
      xy_point_curve_with_z xyz = s; // maybe set(xyz, s)?
      auto xy = std::make_shared<xy_point_curve>();
      *xy = xyz.xy_curve;
      d->emplace(time_axis.time(0), std::move(xy));
      return d;
    }

    template <typename DO, int da, typename Ux, typename Uy>
    t_xy_& set(t_xy_& d, shop_r_t<DO, da, std::map<time_t, xy_point_curve_with_z>, Ux, Uy> const & s) const {
      // Convert xyt with map key type time_t into t_xy_ with map key type utctime before assigning to stm attribute
      if (d)
        d->clear();
      else
        d = std::make_shared<t_xy_::element_type>();
      std::map<time_t, xy_point_curve_with_z> xyt = s; // maybe set(xyt, s)?
      for (auto it = std::cbegin(xyt); it != std::cend(xyt); ++it) {
        auto xy = std::make_shared<xy_point_curve>();
        *xy = it->second.xy_curve;
        d->emplace(shyft::core::utctime_from_seconds64(it->first), std::move(xy));
      }
      return d;
    }

    // Conditional setters, checking if source exists
    // Note if source is temporal it will still throw if source is not valid at specified time, unless an additional
    // default value is also specified (which will be passed into parameter invalid_v of at())
    template <typename D, typename S, typename... Args>
    D& set_if(D& d, S const & s, Args... args)
      const // Set value from attribute if the attribute exists, else do nothing.
    {
      return exists(s) ? set(d, s, args...) : d;
    }

    // Set value from attribute if the attribute exists, or set specified default value.
    template <typename D, typename S, typename V, typename... Args>
    D& set_or(D& d, S const & s, V const & v_not_exists, Args... args) const {
      if (exists(s)) {
        return set(d, s, args...);
      } else {
        return set(d, v_not_exists);
      }
    }

    // Higher level setters for pure optional values
    // May or may not set a value. Similar to conditional setters (set_if/set_or) but does not throw (does nothing) if
    // source does not exist or if temporal value not valid at specified time.
    // Set pure optional: May or may not set a value. Like set_if does nothing (skip) if source not exists, but
    // in addition also does nothing (does not throw) if source exists but is not valid at specified time and no
    // default value is specified (can still specify additional default value which will be passed into
    // parameter invalid_v of at())
    template <typename D, typename S, typename... Args>
    D& set_optional(D& d, S const & s, Args... args) const {
      try {
        return set_if(d, s, args...);
      } catch (...) {
        return d;
      }
    }

    // Set pure optional: May or may not set a value. Like set_or sets default value if source not exists, but
    // does nothing (does not throw) if source exists but is not valid at specified time and no default value is
    // specified (can still specify additional default value which will be passed into parameter invalid_v of
    // at())
    template <typename D, typename S, typename V, typename... Args>
    D& set_optional(D& d, S const & s, V const & v_not_exists, Args... args) const {
      try {
        return set_or(d, s, v_not_exists, args...);
      } catch (...) {
        return d;
      }
    }

    // Higher level setters for pure required values
    // Will never silently skip setting of results, will either ensure a value is set or throw exception.
    template <typename D, typename S, typename... Args>
    D& set_required(D& d, S const & s, Args... args)
      const // Set pure required: Will set value or throw. Is just an alias for set.
    {
      return set(d, s, args...);
    }

    // Set pure required: Will always set value and never throw. Like set_optional sets default value if source
    // not exists, but also sets the same value if source exists but is not valid at specified time and no
    // additional default value is specified (can still specify additional default value which will be passed
    // into parameter invalid_v of at())
    template <typename D, typename S, typename V, typename... Args>
    D& set_required(D& d, S const & s, V const & v_default, Args... args) const {
      try {
        return set_or(d, s, v_default, args...);
      } catch (...) {
        return set(d, v_default);
      }
    }

    void set_shop_time_delay(auto& o_shop, t_xy_ const & delay) const {

      // Convert stm delay xy into shop attributes, constant delay is assumed to be
      // called either time_delay or const_time_delay, variable delay is assumed to be
      // time_delay_curve or shape_discharge.
      // Shop requires first X value in shape_discharge to be 0.0.
      // In stm we allow it to be >0 in which case we move it to the scalar
      // time_delay attribute instead, and skew all other values correspondingly.

      auto const_delay_scale = std::max<std::int64_t>(core::to_seconds64(time_delay_unit), 1);
      static constexpr bool has_time_delay_curve = requires { o_shop.time_delay_curve; };
      auto const variable_delay_scale = [&] -> std::int64_t {
        if constexpr (has_time_delay_curve)
          return 1;
        else
          return std::max<std::int64_t>(core::to_seconds64(time_delay_unit), 1);
      }();
      auto&& const_delay = [&] -> auto& {
        if constexpr (requires { o_shop.time_delay; })
          return o_shop.time_delay;
        else
          return o_shop.time_delay_const;
      }();
      auto&& variable_delay = [&] -> auto& {
        if constexpr (has_time_delay_curve)
          return o_shop.time_delay_curve;
        else
          return o_shop.shape_discharge;
      }();
      auto set_variable_delay = [&](xy_point_curve xy) {
        if constexpr (has_time_delay_curve) {
          for (auto& p : xy.points)
            p.x /= variable_delay_scale;
          set_optional(
            o_shop.time_delay_curve,
            std::vector<xy_point_curve_with_z>{
              xy_point_curve_with_z{.xy_curve = std::move(xy), .z = 0.0}
          });
        } else {
          // simplify variablle to constant
          if (xy.points.size() == 2 && xy.points[0].y == 0.0) {
            set_optional(const_delay, xy.points[1].x / variable_delay_scale);
            return;
          }
          // Either xy has non-zero first x:
          //   - That means it is not valid as shape_discharge, but we move that
          //     initial value, representing an offset, into time_delay, and
          //     set the rest of the xy points (if any), representing the wave,
          //     as shape_discharge, but with x-values reduced according to the offset.
          // Or, xy has zero first x:
          //   - Else, the xy is assumed to be a valid shape_discharge, e.g.
          //     difference between any two succeeding x values are the same as the
          //     difference between the first two.
          auto delay_offset = xy.points[0].x / variable_delay_scale;
          if (delay_offset > 0)
            set_optional(const_delay, delay_offset);
          xy.points[0].x = 0.0;
          for (auto& p : std::views::drop(xy.points, 1))
            p.x = p.x / variable_delay_scale - delay_offset;
          set_optional(o_shop.shape_discharge, xy);
        }
      };

      if (!const_delay.is_default() || variable_delay.exists())
        return; // Time delay already set!
      if (!valid(delay))
        return;
      auto const xy = get(delay);
      if (!xy)
        return;
      if (xy->points.empty())
        return;
      else if (xy->points.size() == 1) {
        // simplify variable to constant
        auto delay_offset = xy->points.front().x / const_delay_scale;
        if (delay_offset > 0)
          set_optional(const_delay, xy->points.front().x / const_delay_scale);
      } else {
        set_variable_delay(*xy);
      }
    }

    /** Section for .to_shop(some-stm-type-object)  */
    shop_market to_shop(energy_market_area const & a) const;
    shop_reserve_group to_shop(unit_group const & a, apoint_ts const & mask_ts, std::string const & name) const;
    xy_point_curve_ get_spill_description(reservoir const & a) const;
    shop_reservoir to_shop(reservoir const & a) const;
    shop_unit to_shop_generator_unit(unit const & a) const;
    shop_pump to_shop_pump_unit(unit const & a) const;
    shop_power_plant to_shop(power_plant const & a) const;
    shop_tunnel to_shop(waterway const & wtr) const;
    shop_discharge_group to_shop_discharge_group(waterway const & wtr);
    shop_river to_shop_river(waterway const & wtr, gate const * gt) const;
    shop_gate to_shop_gate(waterway const & wtr, gate const * gt) const;
    shop_contract to_shop(contract const & ctr) const;

    void from_shop(energy_market_area& mkt, shop_market const & shop_mkt);
    void from_shop(contract& ctr, shop_contract const & shop_ctr);
    void from_shop(reservoir& rsv, shop_reservoir const & shop_rsv);
    void from_shop(unit& agg, shop_unit const & shop_agg);
    void from_shop(unit& pu, shop_pump const & shop_pu);
    void from_shop(waterway& wtr, shop_gate const & shop_gt);
    void from_shop(waterway& wtr, shop_river const & shop_riv);
    void from_shop(waterway& wtr, shop_discharge_group const & shop_dg);
    void from_shop(gate& gt, shop_gate const & shop_gt);
    void from_shop(waterway& wtr, shop_tunnel const & shop_tn);
    void from_shop(unit_group& ug, shop_reserve_group const & shop_ug);
    void from_shop(power_plant& pp, shop_power_plant const & shop_ps);
    void from_shop(optimization_summary& osm, shop_objective const & shop_obj) const;
  };

  constexpr bool should_emit(time_axis::generic_dt const &, auto const &) {
    return true;
  }
  inline bool should_emit(time_axis::generic_dt const & t, contract const & c) {
    // acontract without any valid entries in attribute "options" (emitted as
    // "trade_curve" to Shop) is not relevant (and currently leads to crash in Shop)
    return shop_adapter::valid_temporal(c.options, t.total_period().end - utctimespan(1));
  }


}
