#pragma once
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/mp.h>

namespace shyft::energy_market::stm {

  /** @brief Power module
   *  Represents consumption/production. If positive then production, if negative then consumption.
   *  A power module is connected to a busbar, which represent a node in a transmission network
   */
  struct power_module : id_base {
    using super = id_base;

    power_module() {
      mk_url_fx(this);
    }

    power_module(int id, std::string const & name, std::string const & json, std::shared_ptr<stm_system> const & sys)
      : super{id, name, json, {}, {}, {}}
      , sys{sys} {
      mk_url_fx(this);
    }

    /** @brief generate an almost unique, url-like string for a reservoir.
     *
     * @param rbi: back inserter to store result
     * @param levels: How many levels of the url to include.
     * 		levels == 0 includes only this level. Use level < 0 to include all levels.
     * @param placeholders: The last element of the vector states wethers to use the reservoir ID
     * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
     * 		If the vector is empty, the function defaults to not using placeholders.
     * @return
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    bool operator==(power_module const & other) const;

    bool operator!=(power_module const & other) const {
      return !(*this == other);
    }

    auto sys_() const {
      return sys.lock();
    }

    std::weak_ptr<stm_system> sys;

    struct power_ {
      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        power_,
        (apoint_ts, realised), ///< Historical fact. W, J/s, if positive then production, if negative then consumption
        (apoint_ts,
         schedule),         ///< the current schedule. W, J/s, if positive then production, if negative then consumption
        (apoint_ts, result) ///< the optimal/simulated/estimated result. W, J/s, if positive then production, if
                            ///< negative then consumption
      );

      SHYFT_DEFINE_STRUCT(power_, (), (realised, schedule, result));
    };

    BOOST_HANA_DEFINE_STRUCT(power_module, (power_, power));

    SHYFT_DEFINE_STRUCT(power_module, (id_base), (power));

    x_serialize_decl();
  };

  using power_module_ = std::shared_ptr<power_module>;
}

x_serialize_export_key(shyft::energy_market::stm::power_module);
BOOST_CLASS_VERSION(shyft::energy_market::stm::power_module, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_module::power_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_module);

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::power_module>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::power_module, Char> { };
