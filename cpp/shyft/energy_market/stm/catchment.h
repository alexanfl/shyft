#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
  using std::string;
  using std::shared_ptr;
  using std::dynamic_pointer_cast;
  using std::map;
  using shyft::core::utctime;

  struct catchment : hydro_power::catchment {
    using super = hydro_power::catchment;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

    // using rds = hps_rds<catchment>; // sih: when needed

    catchment(int id, string const & name, string const & json, stm_hps_& hps)
      : super(id, name, json, hps) {
    }

    catchment() = default;

    bool operator==(catchment const & other) const;

    bool operator!=(catchment const & other) const {
      return !(*this == other);
    }

    BOOST_HANA_DEFINE_STRUCT(catchment, (apoint_ts, inflow_m3s));

    SHYFT_DEFINE_STRUCT(catchment, (hydro_power::catchment), ())

    x_serialize_decl();
  };

  using catchment_ = shared_ptr<catchment>;
}

x_serialize_export_key(shyft::energy_market::stm::catchment);
BOOST_CLASS_VERSION(shyft::energy_market::stm::catchment, 1);
