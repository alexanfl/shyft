#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {
  /** @brief Wind Farm
   *
   * Wind Farm, a grouping of wind turbines
   */
  struct wind_farm : id_base {
    using super = id_base;

    wind_farm() {
      mk_url_fx(this);
    }

    wind_farm(int id, std::string const & name, std::string const & json, std::shared_ptr<stm_system> const & sys)
      : super{id, name, json, {}, {}, {}}
      , sys{sys} {
      mk_url_fx(this);
    }

    bool operator==(wind_farm const & other) const;

    bool operator!=(wind_farm const & other) const {
      return !(*this == other);
    }

    auto sys_() const {
      return sys.lock();
    }

    std::weak_ptr<stm_system> sys;

    /** @brief generate an almost unique, url-like string for a wind farm.
     *
     * @param rbi: back inserter to store result
     * @param levels: How many levels of the url to include.
     * 		levels == 0 includes only this level. Use level < 0 to include all levels.
     * @param placeholders: The last element of the vector states wethers to use the wind farm ID
     * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
     * 		If the vector is empty, the function defaults to not using placeholders.
     * @return
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    struct production_ {
      BOOST_HANA_DEFINE_STRUCT(
        production_,
        (apoint_ts, forecast), // expected production
        (apoint_ts, realised), //
        (apoint_ts, result)    //
      );
      SHYFT_DEFINE_STRUCT(production_, (), (forecast, realised, result));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    BOOST_HANA_DEFINE_STRUCT(wind_farm, (production_, production));
    SHYFT_DEFINE_STRUCT(wind_farm, (), (production));


    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::energy_market::stm::wind_farm);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::production_);

SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::wind_farm);
