#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::time_series::dd::apoint_ts;

  struct reservoir_aggregate : id_base {

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    reservoir_aggregate(
      int id,
      std::string const & name,
      std::string const & json = "",
      std::shared_ptr<stm_hps> const & hps = nullptr);
    reservoir_aggregate();
    ~reservoir_aggregate();

    bool operator==(reservoir_aggregate const & other) const;

    bool operator!=(reservoir_aggregate const & other) const {
      return !(*this == other);
    }

    std::vector<std::shared_ptr<reservoir>> reservoirs; ///< the reservoirs allocated to this object

    auto hps_() const {
      return hps.lock();
    }

    std::weak_ptr<stm_hps> hps; ///< reference up to the 'owning' hydro-power system.
    std::shared_ptr<reservoir_aggregate>
      shared_from_this() const; ///< shared from this using hps.reservoir_aggregates shared pointers.

    static void add_reservoir(reservoir_aggregate_ const & ra, std::shared_ptr<reservoir> const & r);
    void remove_reservoir(std::shared_ptr<reservoir> const & r);

    struct inflow_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        inflow_,
        (apoint_ts, schedule), ///< m3/s
        (apoint_ts, realised), ///< m3/s
        (apoint_ts, result)    ///< m3/s
      );
      SHYFT_DEFINE_STRUCT(inflow_, (), (schedule, realised, result));
    };

    struct volume_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        volume_,
        (apoint_ts, static_max), ///< m3
        (apoint_ts, schedule),   ///< m3
        (apoint_ts, realised),   ///< m3
        (apoint_ts, result)      ///< m3
      );
      SHYFT_DEFINE_STRUCT(volume_, (), (static_max, schedule, realised, result));
    };

    BOOST_HANA_DEFINE_STRUCT(reservoir_aggregate, (inflow_, inflow), (volume_, volume));
    SHYFT_DEFINE_STRUCT(reservoir_aggregate, (id_base), (inflow, volume));

    x_serialize_decl();
  };

  using reservoir_aggregate_ = std::shared_ptr<reservoir_aggregate>;
}

x_serialize_export_key(shyft::energy_market::stm::reservoir_aggregate);
BOOST_CLASS_VERSION(shyft::energy_market::stm::reservoir_aggregate, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir_aggregate::inflow_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir_aggregate::volume_);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::reservoir_aggregate, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::reservoir_aggregate, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::reservoir_aggregate>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::reservoir_aggregate, Char> { };
