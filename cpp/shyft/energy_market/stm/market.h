#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <vector>
#include <memory>

#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

  using shyft::time_series::dd::apoint_ts;

  struct unit_group;
  using unit_group_ = std::shared_ptr<unit_group>;

  /* @brief energy market area
   *
   * suited for the stm optimization, keeps attributes for an area where the price/load and
   * other parameters controlling the optimization problem can be kept
   */
  struct energy_market_area : id_base {
    using super = id_base;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    energy_market_area();
    energy_market_area(int id, std::string const & name, std::string const & json, stm_system_ const & sys);

    bool operator==(energy_market_area const & other) const;

    bool operator!=(energy_market_area const & other) const {
      return !(*this == other);
    }

    std::vector<std::shared_ptr<unit_group>> unit_groups;
    /** set a unit group on the energy market area creating an association */
    void set_unit_group(std::shared_ptr<unit_group> const & ug);
    /** get the unit group, if available */
    std::shared_ptr<unit_group> get_unit_group() const;

    bool has_unit_group() const {
      return !unit_groups.empty() && unit_groups.front();
    }

    /** disassociating a unit group with the energy market area */
    std::shared_ptr<unit_group> remove_unit_group();
    /** Get transmission lines to other market area */
    std::vector<std::shared_ptr<transmission_line>>
      transmission_lines_to(std::shared_ptr<energy_market_area> const & to) const;
    /** Get transmission lines from other market area */
    std::vector<std::shared_ptr<transmission_line>>
      transmission_lines_from(std::shared_ptr<energy_market_area> const & from) const;

    /** Create a unit group of type 'production', that will derive its units from associated busbars.
     *  The function also adds the unit group to the system, and call set_unit_group on this class,
     *  which associates the group with this market area.
     *  If busbars are added/removed later, the group is updated accordingly.*/
    std::shared_ptr<unit_group>
      create_busbar_derived_unit_group(int id, std::string const & name, std::string const & json);
    /** Get the busbar_derived_unit_group. Used by busbars to add/remove units */
    std::shared_ptr<unit_group> get_busbar_derived_unit_group() const;
    void set_busbar_derived_unit_group(std::shared_ptr<unit_group> const & ug);
    std::vector<std::shared_ptr<contract>> contracts; ///< association with contracts
    std::vector<std::shared_ptr<busbar>> busbars;     ///< association with busbars

    auto sys_() const {
      return sys.lock();
    }

    std::weak_ptr<stm_system> sys;

    /** for practial planning usage, we end up in .realised,.schedule and .result */
    struct ts_triplet_ {
      url_fx_t url_fx;
      BOOST_HANA_DEFINE_STRUCT(
        ts_triplet_,
        (apoint_ts, realised), ///< SI unit, as in historical fact
        (apoint_ts, schedule), ///< the current schedule
        (apoint_ts, result)    ///< the optimal/simulated/estimated result
      );

      SHYFT_DEFINE_STRUCT(ts_triplet_, (), (realised, schedule, result));
    };

    /** */
    struct offering_ {
      url_fx_t url_fx;
      BOOST_HANA_DEFINE_STRUCT(
        offering_,
        (t_xy_, bids),        ///< x= price[money/J], y= amount [J/s] aka [W], ordered by t and then x,y x-ascending
        (ts_triplet_, usage), ///< how much has been used out of the bids
        (ts_triplet_, price)  ///< based on usage, what is the effective price paid for the usage [money/J]
      );

      SHYFT_DEFINE_STRUCT(offering_, (), (bids, usage, price));
    };

    BOOST_HANA_DEFINE_STRUCT(
      energy_market_area,
      (apoint_ts, price),                      ///< money/J (input, see also deman/supply)
      (apoint_ts, load),                       ///< W (requirement for optimiser)
      (apoint_ts, max_buy),                    ///< W (constraint for optimiser)
      (apoint_ts, max_sale),                   ///< W (constraint for optimiser)
      (apoint_ts, buy),                        ///< W (result, as pr. optimiser)
      (apoint_ts, sale),                       ///< W (result, as pr. optimiser)
      (apoint_ts, production),                 ///< W (result, as pr. optimiser)
      (apoint_ts, reserve_obligation_penalty), ///< money (result from optimiser)
      (offering_,
       demand), ///< the demand side of the market, willing to buy power(as seller you can sell to pricy offering first)
      (offering_, supply) ///< the supply side of the market, wants to sell power(as buyer, you can pick the cheapest
                          ///< offering first)
    );

    SHYFT_DEFINE_STRUCT(
      energy_market_area,
      (id_base),
      (price, load, max_buy, max_sale, buy, sale, production, reserve_obligation_penalty, demand, supply));

    /** get_production(), get_consumption()
     *  For potentially n number of units and power_modules, a simple example:
     *   -----------------------------------
     *  | name     | t0 | t1 | t2 | t3 | t4 | // Time-series points
     *   -----------------------------------
     *  | unit1    | 10 | 10 | 2  | 2  | 8  | // Producing unit
     *  | pm1      | -7 | -5 | -6 | 0  | 3  | // Consuming/producing power_module
     *   -----------------------------------
     *  | prod_cont| 10 | 10 | 2  | 2  | 11 | // get_production() : Production contribution
     *  | cons_cont| 7  | 5  | 6  | 0  | 0  | // get_consumption(): Consumption contribution, as positive values
     *   -----------------------------------
     */

    // separate from ts_triplet_ for python exposure purposes
    struct ts_triplet_result {
      apoint_ts realised; ///< SI unit, as in historical fact
      apoint_ts schedule; ///< the current schedule
      apoint_ts result;   ///< the optimal/simulated/estimated result

      SHYFT_DEFINE_STRUCT(ts_triplet_result, (), (realised, schedule, result));
    };

    /** Get the sum of the production contribution */
    ts_triplet_result get_production() const;

    /** Get the sum of the consumption contribution, as positive values */
    ts_triplet_result get_consumption() const;

    /** Get the import time-series, which is the positive part of the sum of the busbar flows */
    ts_triplet_result get_import() const;

    /** Get the export time-series, which is the negative part of the sum of the busbar flows.
     * Values are inverted, so that the negative part is presented as positive */
    ts_triplet_result get_export() const;

    x_serialize_decl();

   private:
    static bool
      busbar_exists_in_market_area(std::shared_ptr<energy_market_area> const &, std::shared_ptr<busbar> const & b);
    static void insert(std::vector<std::shared_ptr<transmission_line>>&, std::shared_ptr<transmission_line> t);
    std::shared_ptr<unit_group> busbar_derived_unit_group{};

    enum power_sum_type {
      production_only,
      consumption_only
    };

    enum flow_type {
      import,
      exp
    };

    ts_triplet_result get_busbars_flow_sum(flow_type) const;
    ts_triplet_result get_busbars_power_sum(power_sum_type) const;
  };

  using energy_market_area_ = std::shared_ptr<energy_market_area>;
}

x_serialize_export_key(shyft::energy_market::stm::energy_market_area);
BOOST_CLASS_VERSION(shyft::energy_market::stm::energy_market_area, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::energy_market_area::ts_triplet_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::energy_market_area::offering_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::energy_market_area::ts_triplet_result);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::energy_market_area, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::energy_market_area, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::energy_market_area>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::energy_market_area, Char> { };
