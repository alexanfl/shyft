#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <string>
#include <memory>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::time_series::dd::apoint_ts;

  struct unit : hydro_power::unit {
    using super = hydro_power::unit;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    unit();
    unit(int id, std::string const & name, std::string const & json, std::shared_ptr<stm_hps> const & hps);

    bool operator==(unit const & other) const;

    bool operator!=(unit const & other) const {
      return !(*this == other);
    }

    struct production_ {
      struct constraint_ {
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min), ///< W
          (apoint_ts, max)  ///< W
        );
        SHYFT_DEFINE_STRUCT(constraint_, (), (min, max));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        production_,             // units are W, watt
        (apoint_ts, schedule),   ///< W
        (apoint_ts, commitment), ///< (1, 0), indicating if unit should produce(1) or not(0), alternative to schedule
        (apoint_ts,
         pump_commitment),       ///< (1, 0), indicating if pump should consume(1) or not(0), alternative to schedule
        (apoint_ts, realised),   ///< W  as in historical fact
        (apoint_ts, static_min), ///< W
        (apoint_ts, static_max), ///< W
        (apoint_ts, nominal),    ///< W
        (constraint_, constraint),
        (apoint_ts, result) ///< W
      );
      SHYFT_DEFINE_STRUCT(
        production_,
        (),
        (schedule, commitment, pump_commitment, realised, static_min, static_max, nominal, constraint, result));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    //-- definition of nested composition groups
    struct discharge_ {
      struct constraint_ {
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min),                  ///< m3/s
          (apoint_ts, max),                  ///< m3/s
          (t_xy_, max_from_downstream_level) ///< masl -> m3/s
        );
        SHYFT_DEFINE_STRUCT(constraint_, (), (min, max, max_from_downstream_level));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        discharge_,
        (apoint_ts, result),   ///< m3/s
        (apoint_ts, schedule), ///< m3/s
        (apoint_ts, realised), ///< m3/s non trivial computation, based on production.realised etc.
        (constraint_, constraint));
      SHYFT_DEFINE_STRUCT(discharge_, (), (result, schedule, realised, constraint));

      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    struct pump_constraint_ {
      BOOST_HANA_DEFINE_STRUCT(
        pump_constraint_,
        (apoint_ts, min_downstream_level) ///< masl
      );

      SHYFT_DEFINE_STRUCT(pump_constraint_, (), (min_downstream_level));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    struct cost_ {
      BOOST_HANA_DEFINE_STRUCT(
        cost_,
        (apoint_ts, start),      ///< money/#start
        (apoint_ts, stop),       ///< money/#stop
        (apoint_ts, pump_start), ///< money/#start
        (apoint_ts, pump_stop)   ///< money/#stop
      );
      SHYFT_DEFINE_STRUCT(cost_, (), (start, stop, pump_start, pump_stop));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    /** operational reserve, frequency balancing support */
    struct reserve_ { //

      /** reserve_.spec_ provides schedule, or min-max + result mode
       *  that is: if the schedule is provided, the optimizer will respect that
       *  otherwise, plan-mode: find .result within min..max so that external
       *  group requirement is satisfied
       */
      struct spec_ {
        BOOST_HANA_DEFINE_STRUCT(
          spec_,
          (apoint_ts, schedule), ///< W, if specified 'schedule-mode' and next members ignored
          (apoint_ts, min),      ///< W
          (apoint_ts, max),      ///< W
          (apoint_ts, cost),     ///< money
          (apoint_ts, result),   ///< W
          (apoint_ts, penalty),  ///< money
          (apoint_ts, realised)  ///< as in historical fact,possibly computed from production/min/max etc.
        );

        SHYFT_DEFINE_STRUCT(spec_, (), (schedule, min, max, cost, result, penalty, realised))
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      struct fcr_droop_ {
        BOOST_HANA_DEFINE_STRUCT(
          fcr_droop_,
          (apoint_ts, cost),   ///< money
          (apoint_ts, result), ///< %
          (t_xy_, steps)       ///< x=step number, asc, 1..n, y= discrete droop setting
        );

        SHYFT_DEFINE_STRUCT(fcr_droop_, (), (cost, result, steps))
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      struct droop_ {
        BOOST_HANA_DEFINE_STRUCT(
          droop_,
          (fcr_droop_, fcr_n),
          (fcr_droop_, fcr_d_up),
          (fcr_droop_, fcr_d_down),
          (apoint_ts, schedule),
          (apoint_ts, min),
          (apoint_ts, max),
          (apoint_ts, cost),
          (apoint_ts, result),
          (apoint_ts, penalty),
          (apoint_ts, realised));

        SHYFT_DEFINE_STRUCT(
          droop_,
          (),
          (fcr_n, fcr_d_up, fcr_d_down, schedule, min, max, cost, result, penalty, realised))
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      /** most reserve modes goes in up and down regulations */
      struct pair_ {
        BOOST_HANA_DEFINE_STRUCT(
          pair_,
          (spec_, up),  ///< up regulation reserve
          (spec_, down) ///< down regulation reserve
        );
        SHYFT_DEFINE_STRUCT(pair_, (), (up, down));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        reserve_,
        (apoint_ts, fcr_static_min),  ///< W if specified overrides long running static_min for FCR calculations
        (apoint_ts, fcr_static_max),  ///< W if specified overrides long running static_max for FCR calculations
        (pair_, fcr_n),               ///< FCR up,down
        (pair_, afrr),                ///< aFRR up,down
        (pair_, mfrr),                ///< mFRR up,down
        (apoint_ts, mfrr_static_min), ///< minimum production for RR
        (pair_, rr),                  ///< RR up/down
        (pair_, fcr_d),               ///< FCR-D up/down
        (apoint_ts, fcr_mip),         ///< FCR flag for SHOP
        (pair_, frr),                 ///< FRR  up,down
        (droop_, droop),              ///< droop control, relates to fcr_n and fcr_d
        (t_xy_, droop_steps)          ///< x=step number, asc, 1..n, y= discrete droop setting
      );
      SHYFT_DEFINE_STRUCT(
        reserve_,
        (),
        (fcr_static_min,
         fcr_static_max,
         fcr_n,
         afrr,
         mfrr,
         mfrr_static_min,
         rr,
         fcr_d,
         fcr_mip,
         frr,
         droop,
         droop_steps));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    struct production_discharge_relation_ {
      BOOST_HANA_DEFINE_STRUCT(production_discharge_relation_, (t_xy_, original), (t_xy_, convex), (t_xy_, final));
      SHYFT_DEFINE_STRUCT(production_discharge_relation_, (), (original, convex, final));
      url_fx_t url_fx;
    };

    struct best_profit_ {
      BOOST_HANA_DEFINE_STRUCT(
        best_profit_,
        (t_xy_, production),
        (t_xy_, discharge),
        (t_xy_, discharge_production_ratio),
        (t_xy_, operating_zone));
      SHYFT_DEFINE_STRUCT(best_profit_, (), (production, discharge, discharge_production_ratio, operating_zone));
      url_fx_t url_fx;
    };

    //-- class descriptive members
    BOOST_HANA_DEFINE_STRUCT(
      unit,
      (apoint_ts, effective_head), ///< meter
      (t_xy_, generator_description),
      (t_turbine_description_, turbine_description),
      (t_xyz_list_, pump_description),
      (apoint_ts, unavailability),      ///< no-unit, 1== unavailable, (0,nan)-> available
      (apoint_ts, pump_unavailability), ///< no-pump, 1== unavailable, (0,nan)-> available
      (apoint_ts, priority),            ///< unit priority value for uploading order
      (production_, production),
      (discharge_, discharge),
      (pump_constraint_, pump_constraint),
      (cost_, cost),       ///< start/stop
      (reserve_, reserve), ///< operational reserve, frequency balancing support
      (production_discharge_relation_, production_discharge_relation), ///< PQ curves
      (best_profit_, best_profit)                                      /// BP curves
    );
    SHYFT_DEFINE_STRUCT(
      unit,
      (hydro_power::unit),
      (effective_head,
       generator_description,
       turbine_description,
       pump_description,
       unavailability,
       pump_unavailability,
       priority,
       production,
       discharge,
       pump_constraint,
       cost,
       reserve,
       production_discharge_relation,
       best_profit));

    x_serialize_decl();
  };

  using unit_ = std::shared_ptr<unit>;

  /** @brief deduce the capabilities of the unit
   *
   * Deduces the capabilities of the unit based on the capabilities of the
   * turbine efficiency curve. Assumes that the capabilities are the same
   * for all the turbine description versions.
   */
  inline auto turbine_capability(unit const & u) {
    if (!u.turbine_description || u.turbine_description->empty())
      return hydro_power::turbine_none;
    else {
      auto t = u.turbine_description->begin()->second.get();
      return t ? capability(*t) : hydro_power::turbine_none;
    }
  }

}

x_serialize_export_key(shyft::energy_market::stm::unit);
BOOST_CLASS_VERSION(shyft::energy_market::stm::unit, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::production_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::production_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::discharge_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::discharge_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::pump_constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::cost_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::reserve_::spec_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::reserve_::pair_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::reserve_::droop_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::reserve_::fcr_droop_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::reserve_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::production_discharge_relation_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit::best_profit_);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::unit, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::unit, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::unit>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::unit, Char> { };
