#pragma once

#include <algorithm>
#include <cstdint>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <thread>
#include <utility>
#include <variant>
#include <vector>

#include <fmt/core.h>
#include <boost/asio/post.hpp>
#include <boost/asio/use_future.hpp>

#include <shyft/core/reflection.h>
#include <shyft/core/shared_strand.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/dd/is_cyclic.h>
#include <shyft/time_series/dd/resolve_ts.h>

namespace shyft::energy_market::stm {

  /// Enum indicating the state of a shared stm-model.
  ///   - idle:  Model is available in full to the user
  ///   - running: Model is currently running an optimizing procedure, limited availability
  ///   - tuning: Indicates that model is being tuned, used is experimenting.
  ///   - finished: Indicates that the algorithm is finished with success and results are ready.
  ///   - failed: Indicates that the algorithm failed to produce results for the model.
  SHYFT_DEFINE_ENUM(model_state, std::int8_t, (idle, running, tuning, finished, failed));

  struct model_info {
    model_state state;
    std::vector<log_entry> log{};
    std::int32_t log_version{0};

    void log_clear() {
      // each time we start a new log, increment log_version so observers can get a fresh start
      log.clear();
      ++log_version;
    }
  };

  template <bool is_const>
  struct model_view {
    std::shared_ptr<add_const_if_t<is_const, stm_system>> model;
    std::shared_ptr<add_const_if_t<is_const, model_info>> info;
  };

  using const_model_view = model_view<true>;
  using mutable_model_view = model_view<false>;

  template <typename T>
  concept model_mutator = std::invocable<T, mutable_model_view>;

  template <typename T>
  using model_mutator_result = std::invoke_result_t<T, mutable_model_view>;

  template <typename T, typename U>
  concept model_mutator_fallback =
    std::invocable<T> && std::is_same_v<std::invoke_result_t<T>, model_mutator_result<U>>;

  template <typename T>
  concept model_observer = std::invocable<T, const_model_view>;

  template <typename T>
  using model_observer_result = std::invoke_result_t<T, const_model_view>;

  template <typename T, typename U>
  concept model_observer_fallback =
    std::invocable<T> && std::is_same_v<std::invoke_result_t<T>, model_observer_result<U>>;

  template <typename Executor>
  struct shared_model {

    shared_strand<Executor> strand;
    std::shared_ptr<stm_system> model;
    std::shared_ptr<model_info> info;

    static auto post_impl(auto &&strand, auto view, auto &&action) {
      if (!view.model || !view.info)
        throw std::runtime_error("invalid model");
      return boost::asio::post(
        SHYFT_FWD(strand), boost::asio::use_future([view = std::move(view), action = SHYFT_FWD(action)] {
          return std::invoke(SHYFT_FWD(action), std::move(view));
        }));
    }

    auto mutate(model_mutator auto &&action) {
      return post_impl(strand.unique(), mutable_model_view{.model = model, .info = info}, SHYFT_FWD(action));
    }

    auto observe(model_observer auto &&action) const {
      return post_impl(
        strand,
        const_model_view{
          .model = std::const_pointer_cast<stm_system>(model), .info = std::const_pointer_cast<model_info>(info)},
        SHYFT_FWD(action));
    }
  };

  template <typename Executor>
  std::shared_ptr<shared_model<Executor>> make_shared_model(Executor executor, std::shared_ptr<stm_system> model) {
    return std::make_shared<shared_model<Executor>>(shared_model<Executor>{
      .strand = make_shared_strand(executor), .model = std::move(model), .info = std::make_shared<model_info>()});
  }

  template <typename E>
  using shared_model_container = std::map<std::string, std::shared_ptr<shared_model<E>>, std::less<>>;

  template <typename T, typename E>
  concept models_mutator = std::invocable<T, shared_model_container<E> &>;

  template <typename T, typename E>
  concept models_observer = std::invocable<T, shared_model_container<E> const &>;

  template <typename Executor>
  struct shared_models {

    shared_strand<Executor> strand;
    shared_model_container<Executor> container;

    static auto post_impl(auto &&strand, auto &models, auto &&action) {
      return boost::asio::post(SHYFT_FWD(strand), boost::asio::use_future([&, action = SHYFT_FWD(action)] {
                                 return std::invoke(SHYFT_FWD(action), models);
                               }));
    }

    auto mutate(models_mutator<Executor> auto &&action) {
      return post_impl(strand.unique(), container, SHYFT_FWD(action));
    }

    auto observe(models_observer<Executor> auto &&action) const {
      return post_impl(strand, container, SHYFT_FWD(action));
    }

    auto find(std::string_view model_id) {
      return observe(
        [&, model_id = std::string(model_id)](auto const &models) -> std::shared_ptr<shared_model<Executor>> {
          auto it = models.find(model_id);
          if (it == models.end())
            return nullptr;
          return it->second;
        });
    }

    template <model_mutator A>
    auto mutate_or(std::string_view model_id, A &&action, model_mutator_fallback<A> auto &&fallback) {
      auto model = find(model_id).get();
      if (!model)
        return std::async(std::launch::deferred, SHYFT_FWD(fallback));
      return model->mutate(SHYFT_FWD(action));
    }

    template <model_observer A>
    auto observe_or(std::string_view model_id, A &&action, model_observer_fallback<A> auto &&fallback) {
      auto model = find(model_id).get();
      if (!model)
        return std::async(std::launch::deferred, SHYFT_FWD(fallback));
      return model->observe(SHYFT_FWD(action));
    }

    template <model_mutator A>
    auto mutate_or_throw(std::string_view model_id, A &&action) {
      return mutate_or(model_id, SHYFT_FWD(action), [model_id = std::string(model_id)]() -> model_mutator_result<A> {
        throw std::runtime_error(fmt::format("invalid model: '{}'", model_id));
      });
    }

    template <model_observer A>
    auto observe_or_throw(std::string_view model_id, A &&action) {
      return observe_or(model_id, SHYFT_FWD(action), [model_id = std::string(model_id)]() -> model_observer_result<A> {
        throw std::runtime_error(fmt::format("invalid model: '{}'", model_id));
      });
    }

    auto add(std::string_view model_id, std::shared_ptr<stm_system> model = std::make_shared<stm_system>()) {
      return mutate([&, model_id = std::string(model_id), model = std::move(model)](auto &container) {
        return container.try_emplace(model_id, make_shared_model(strand.get_inner_executor(), std::move(model))).second;
      });
    }

    auto remove(std::string_view model_id) {
      return mutate([&, model_id = std::string(model_id)](auto &container) {
        auto it = container.find(model_id);
        if (it == container.end())
          return false;
        container.erase(it);
        return true;
      });
    }

    auto copy(std::string_view source, std::string_view target) {
      // FIXME:
      //   not good, need chaining.
      //   not sure cloning should require a mutable ptr.
      //   - jeh
      auto target_model =
        mutate_or(
          source,
          [&](auto &&view) {
            return stm_system::clone_stm_system(view.model);
          },
          [&] {
            return std::shared_ptr<stm_system>{};
          })
          .get();
      if (!target_model)
        return std::async(std::launch::deferred, [] {
          return false;
        });
      rebind_ts(*target_model, std::string(target));
      return mutate([&, target_model = std::move(target_model), target = std::string(target)](auto &container) {
        if (container.find(target) != container.end())
          return false;
        container.emplace(target, make_shared_model(strand.get_inner_executor(), std::move(target_model)));
        return true;
      });
    }
  };

  template <typename T>
  concept shared_models_init =
    std::ranges::input_range<T>
    && std::is_same_v<std::ranges::range_value_t<T>, std::pair<std::string const, std::shared_ptr<stm_system>>>;

  template <
    typename Executor,
    shared_models_init T = std::initializer_list<std::pair<std::string const, std::shared_ptr<stm_system>>>>
  auto make_shared_models(Executor executor, T models = {}) {
    auto initial_shared_models = std::views::transform(models, [&](auto &&model) {
      return std::pair{model.first, make_shared_model(executor, std::move(model.second))};
    });
    return shared_models<Executor>{
      .strand = make_shared_strand(executor),
      .container =
        shared_model_container<Executor>{std::begin(initial_shared_models), std::end(initial_shared_models)}
    };
  }

  /**
   * @brief rebind_expression
   * @details
   *
   * This routine traverses all time-series of the locked and mutable stm_system mdl,
   * its purpose is to be used after optimization/computation have updated the result terminals
   * of the stm_system. In that case, any expressions refering to those terminals needs to be
   * properly recomputed/reconfigured for computation.
   *
   * The algorithm outline:
   * for each ts of the model, if it's an expression,
   *   find all the terminal nodes of the expression
   *     and save their bound time-series
   *   then IF there was 1 or more dstm://refs,
   *        unbind the expression (will wipe out bound entities)
   *        then put into place the saved bound time-series
   *        finally, re-evaluate/prepare the expression.
   *
   * @param mdl a locked exclusive mode stm_system
   * @param new_mkey the dstm m_key for the stm_system
   * @param nan_fill_missing if true, then replace empty/unbound terminals with nan ts using mdl. run_time_axis
   * @return true if any rebind occured (e.g. there was at least one dtstm://new_mkey/ ref. in the expressions)
   */
  bool rebind_expression(stm_system &mdl, std::string const &new_mkey, bool nan_fill_missing);

  /**
   * @brief get_attrs
   * @details
   *
   * Getter method that returns internal representation of underlying models data.
   *
   * The algorithm outline:
   * There is a constraint that all attrs found need to be returned in same order as they have been
   * requested by the client.
   *
   * Vector if indices is introduced that is sorted on url value, keeping the original place of each input for
   * return convenience. Then, we iterate through sorted vector, and peek model_id... From the place iterator is,
   * we iterate forward until we find a model_id that is different that one peaked. This allowes us to get a range
   * of iterators which have same model_id. And once that is obtained we can perform actions on a chunk of data, not
   * just one by one.
   *
   * Once we have iterated through the vector, we transform the sorted vector of indices to a value of returned data,
   * and populate it into result vector so the placing of any element is the same as the request
   *
   * @param models models to traverse from
   * @param urls range of urls to obtain data from
   * @return std::vector<std::variant<any_attr, url_resolve_error>> vector of variants with value or error output
   */
  template <typename Executor>
  auto get_attrs(shared_models<Executor> &models, std::ranges::random_access_range auto &&urls) {
    using vector_attrs = std::vector<std::variant<any_attr, url_resolve_error>>;
    using attr_type = std::variant<any_attr, url_resolve_error>;
    using futures_vector = std::vector<std::future<vector_attrs>>;

    futures_vector futures;
    std::vector<std::size_t> attr_indices(urls.size());
    std::ranges::iota(attr_indices, std::size_t{0});

    auto projection = [&](auto index) -> auto const & {
      return urls[index];
    };
    std::ranges::sort(attr_indices, {}, projection);
    auto it = attr_indices.begin();
    while (it != attr_indices.end()) {
      auto peek_current_url = url_peek_model_id(urls[*it]);

      auto url_comparator = [&](auto const &index) {
        return peek_current_url == url_peek_model_id(urls[index]);
      };

      auto find_first_not_equal = std::ranges::find_if_not(it, attr_indices.end(), url_comparator);

      auto model_handler = [=](auto &&view) {
        auto transformer = [&](const auto &index) {
          auto parse_result = url_parse_to_result(urls[index]);

          auto result_handler = [&]<typename T>(const T &d) -> attr_type {
            if constexpr (is_url_err_v<T>) {
              return stm::url_resolve_error::parse_fail(urls[index], d.what);
            } else {
              static_assert(is_url_tuple_v<T>);
              auto &&[url_model, url_comp_path, url_attr] = d;
              return stm::url_resolve(
                *view.model,
                url_comp_path,
                url_attr,
                [&](auto &&attribute) -> attr_type {
                  if constexpr (requires { any_attr{attribute}; })
                    return any_attr{attribute};
                  return stm::url_resolve_error::not_representable(url_attr);
                },
                [&](auto &&state) -> attr_type {
                  return stm::url_resolve_error::not_found_in_model(state, urls[index], url_attr);
                });
            }
          };

          return std::visit(result_handler, parse_result);
        };

        vector_attrs ret_vec(std::distance(it, find_first_not_equal));
        std::ranges::transform(it, find_first_not_equal, ret_vec.begin(), transformer);
        return ret_vec;
      };

      auto fallback_handler = [=] {
        vector_attrs ret_vec(std::distance(it, find_first_not_equal));
        std::ranges::transform(it, find_first_not_equal, ret_vec.begin(), [&](const auto &index) {
          return stm::url_resolve_error::model_not_found(peek_current_url, urls[index]);
        });
        return ret_vec;
      };

      if (peek_current_url.empty()) {
        auto error_handler = [=] {
          vector_attrs ret_vec(std::distance(it, find_first_not_equal));
          auto transformer = [&](const auto &index) -> attr_type {
            auto parse_result = url_parse_to_result(urls[index]);
            auto error = std::get<url_parse_error>(parse_result);
            return stm::url_resolve_error::parse_fail(urls[index], error.what);
          };

          std::ranges::transform(it, find_first_not_equal, ret_vec.begin(), transformer);
          return ret_vec;
        };
        futures.emplace_back(std::async(std::launch::deferred, std::move(error_handler)));
      } else {
        futures.emplace_back(models.observe_or(peek_current_url, model_handler, fallback_handler));
      }

      it = find_first_not_equal;
    }

    vector_attrs result(urls.size());
    for (auto [index, input] :
         std::views::zip(attr_indices, std::views::join(std::views::transform(futures, [](auto &future) {
                           return future.get();
                         })))) {
      result[index] = input;
    }
    return result;
  }

  /**
   * @brief get_ts
   * @details
   *
   * Simple getter method that takes any_attr variant and checks if it's of timeseries type
   *
   * @param attr variant attribute
   * @return nullptr if type is not time series, and populates shared_ptr with timeseries type if it is
   */
  template <typename Executor>
  auto get_ts(shared_models<Executor> &models, std::string_view const &url)
    -> std::shared_ptr<time_series::dd::ipoint_ts const> {
    auto result = get_attrs(models, std::views::single(url));
    if (result.empty()) {
      return nullptr;
    }
    auto p_get_attr = std::get_if<any_attr>(&result[0]);
    if (!p_get_attr) {
      return nullptr;
    }
    auto p_get_ts = std::get_if<time_series::dd::apoint_ts>(p_get_attr);
    if (!p_get_ts) {
      return nullptr;
    }
    return p_get_ts->ts;
  }

  /**
   * @brief set_attrs
   * @details
   *
   * Setter method that pulls internal representation of underlying models data and modifies them.
   *
   * The algorithm outline:
   * There is a constraint that all attrs that have been modified need to report status in same order as they have
   * been requested by the client.
   *
   * Vector if indices is introduced that is sorted on first string url value of attr request, keeping the original
   * place of each input for return conviniece. Then, we iterate thru sorted vector, and peek model_id... From the place
   * iterator is, we iterate forward until we find a model_id that is different that one peaked. This allowes us to get
   * a range of iterators which have same model_id. And once thats obtained we can perform actions on a chunck of data,
   * not just one by one.
   *
   * Once we have iterated thru the vector, we transform the sorted vector of indices to a value of returned data,
   * and populate it into result vector so the placing of any element is the same as the request
   *
   * @param models models to traverse from
   * @param attrs attrs to be mutated, usually a vector<pair<string, attr>>
   * @return std::vector<std::optional<stm::url_resolve_error>> status of each attr wanted to be modified
   */
  template <typename Executor>
  auto set_attrs(shared_models<Executor> &models, std::ranges::random_access_range auto &&attrs) {

    using optional_vector_error = std::vector<std::optional<stm::url_resolve_error>>;
    using optional_error_type = std::optional<stm::url_resolve_error>;
    using futures_vector = std::vector<std::future<optional_vector_error>>;

    futures_vector futures;
    std::vector<std::size_t> attr_indices(attrs.size());
    std::ranges::iota(attr_indices, std::size_t{0});

    auto projection = [&](auto index) -> auto const & {
      return attrs[index].first;
    };
    std::ranges::sort(attr_indices, {}, projection);

    optional_vector_error result(attrs.size(), std::nullopt);

    auto it = attr_indices.begin();
    while (it != attr_indices.end()) {

      auto peek_current_url = url_peek_model_id(attrs[*it].first);

      auto url_comparator = [&](auto const &index) {
        auto peek_url = url_peek_model_id(attrs[index].first);
        return peek_url == peek_current_url;
      };

      auto find_first_not_equal = std::ranges::find_if_not(it, attr_indices.end(), url_comparator);

      auto model_handler = [=](auto &&view) {
        auto transformer = [&](const auto &index) {
          const auto [url, attr] = attrs[index];
          auto parse_result = url_parse_to_result(url);

          auto result_handler = [&]<typename T>(const T &d) -> optional_error_type {
            if constexpr (shyft::energy_market::stm::is_url_err_v<T>) {
              return stm::url_resolve_error::parse_fail(url, d.what);
            } else {
              static_assert(shyft::energy_market::stm::is_url_tuple_v<T>);
              auto &&[url_model, url_comp_path, url_attr] = d;
              return stm::url_resolve(
                *view.model,
                url_comp_path,
                url_attr,
                [&](auto &&attribute) {
                  return std::visit(
                    [&](auto &&target_attribute) -> optional_error_type {
                      if constexpr (requires { attribute = std::move(target_attribute); }) {
                        attribute = std::move(target_attribute);
                        return std::nullopt;
                      }
                      return stm::url_resolve_error::not_representable(url_attr);
                    },
                    attr);
                },
                [&](auto &&state) -> optional_error_type {
                  return stm::url_resolve_error::not_found_in_model(state, url, url_attr);
                });
            }
          };
          return std::visit(result_handler, parse_result);
        };
        optional_vector_error ret_vec(std::distance(it, find_first_not_equal));
        std::ranges::transform(it, find_first_not_equal, ret_vec.begin(), transformer);
        return ret_vec;
      };

      auto fallback_handler = [=] {
        optional_vector_error ret_vec(std::distance(it, find_first_not_equal));
        std::ranges::transform(it, find_first_not_equal, ret_vec.begin(), [=](const auto &index) {
          return stm::url_resolve_error::model_not_found(peek_current_url, attrs[index].first);
        });
        return ret_vec;
      };

      if (peek_current_url.empty()) {
        auto error_handler = [=] -> optional_vector_error {
          optional_vector_error ret_vec(std::distance(it, find_first_not_equal));
          auto transformer = [&](const auto &index) -> optional_error_type {
            auto parse_result = url_parse_to_result(attrs[index].first);
            auto error = std::get<url_parse_error>(parse_result);
            return stm::url_resolve_error::parse_fail(attrs[index].first, error.what);
          };

          std::ranges::transform(it, find_first_not_equal, ret_vec.begin(), transformer);
          return ret_vec;
        };
        futures.emplace_back(std::async(std::launch::deferred, std::move(error_handler)));
      } else {
        futures.emplace_back(models.mutate_or(peek_current_url, model_handler, fallback_handler));
      }
      it = find_first_not_equal;
    }

    for (auto [index, input] :
         std::views::zip(attr_indices, std::views::join(std::views::transform(futures, [](auto &future) {
                           return future.get();
                         })))) {
      result[index] = input;
    }

    return result;
  }

  template <typename Executor, typename Model>
  bool causes_cycle(shared_models<Executor> &models, std::string_view model_id, std::shared_ptr<Model> model) {
    url_string_buffer buffer{};
    url_format_root_to(std::back_inserter(buffer), model_id);
    bool is_cyclic = false;
    auto check_if_cyclic = [&]<typename T>(ignore_t, T &attr, [[maybe_unused]] std::string_view url) {
      if (is_cyclic)
        return;
      if constexpr (std::is_same_v<T, time_series::dd::apoint_ts>)
        is_cyclic = time_series::dd::is_cyclic(
          [&](ignore_t, std::string_view url) -> std::shared_ptr<time_series::dd::ipoint_ts const> {
            if (auto attr_opt = url_get_attr(*model, url)) {
              if (auto p_get_ts = std::get_if<time_series::dd::apoint_ts>(&attr_opt.value()))
                return p_get_ts->ts;
              return nullptr;
            }
            return get_ts(models, url);
          },
          attr);
    };
    url_with_attrs<[](auto comp_type) {
      return paths_of<unwrap_member<typename decltype(comp_type)::type>>{};
    }>(buffer, *model, check_if_cyclic);
    return is_cyclic;
  }

}
