#pragma once
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/time_series/dd/ats_vector.h>

namespace shyft::energy_market::stm {
  using namespace shyft::time_axis;
  using namespace shyft::time_series;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::apoint_ts;
  using std::vector;
  using xy_point = hydro_power::point;
  using xy_points = hydro_power::xy_point_curve;

  /**
   * @brief construct a time-axis for t_xy_
   * @details
   * Iterates through the time dependent structure, building time-axis points
   * until sequence empty, or t_end is met.
   * t_end is used as termination of the returned time-axis.
   * @note Caller must ensure that there is at least one time-point less than t_end.
   * @param pd t-xy structure to use
   * @param t_end the end of last interval wanted.
   * @return time-axis, of type points.(no optimize)
   */
  extern generic_dt mk_t_xy_time_axis(t_xy_ const & pd, utctime t_end);


  /**
   * @brief Give t_xy rep of price/delivery, convert to tuple price-tsv, delivery-tsv
   * @details
   * The `pd` supplied time-dependent series of price,delivery (production), pairs is
   * converted to time-series, stashed into ts-vectors in ascending price order.
   * The result is  delivered as a vector of either zero (empty), or two ats_vector's,
   * where the first ats_vector represents the price steps,
   * and the second the corresponding delivery(production) steps.
   * @param pd price-delivery time dependent table of xy (sorted by x ascending).
   * @param t_end the end time of the last interval of the supplied time dependent series
   * @return a vector with price and delivery ts-vectors according to time-steps, and number of price/delivery pairs
   */
  extern vector<ats_vector> convert_to_price_delivery_tsv(t_xy_ const & pd, utctime t_end);

  /**
   * @brief given amount y, and xy_points compute true average x, from specified direction(take_cheapest default true)
   * @details
   * This is the core routine to compute the average price for a specificed amount y(effect).
   * @note the semantics are specific stm effective price computations.
   * @param y the value to compute x average for
   * @param t the xy_points transform, x=price, y=amount
   * @param take_cheapest consume merit order asc, or desc (buyer/seller)
   * @return average x for specified y
   */
  double effective_price(double y, xy_points const & t, bool take_cheapest = true);

  /**
   * @brief compute effective price of used delivery from time-dependent price-delivery description
   * @details
   * Algorithm:
   * Compute the effective time-axis of `d` and `pd`
   * for each interval in the effective time-axis,
   * compute the resulting price.
   * @param d the used amount of delivery/supply
   * @param pd the time-dependent price/delivery desription.
   * @param take_cheapest consume merit order asc, or desc (buyer/seller)
   * @return the effective price
   * @see efffective_price(y,xy_points_)
   */
  apoint_ts effective_price(apoint_ts const & d, t_xy_ const & pd, bool take_cheapest = true);
}
