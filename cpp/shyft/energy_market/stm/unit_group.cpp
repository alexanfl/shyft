#include <shyft/energy_market/stm/unit_group.h>

#include <cstdint>
#include <limits>
#include <map>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/market.h>

namespace shyft::energy_market::stm {

  unit_group::unit_group()
    : mdl{nullptr}
    , group_type{} {
    mk_url_fx(this);
  }

  unit_group::unit_group(stm_system* mdl)
    : mdl{mdl}
    , group_type{} {
    mk_url_fx(this);
  }

  unit_group::unit_group(int id, std::string const & name, std::string const & json, stm_system* mdl)
    : id_base{id, name, json, {}, {}, {}}
    , mdl{mdl} {
    mk_url_fx(this);
  }

  std::int64_t unit_group_member::id() const {
    return unit ? unit->id : 0;
  }

  namespace {
    std::string ug_tag(std::int64_t id) noexcept {
      return "/u" + std::to_string(id);
    }
  }

  void unit_group::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
    if (levels && mdl) {
      mdl->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/u{parent_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = ug_tag(id);
      std::ranges::copy(idstr, rbi);
    }
  }

  bool unit_group::operator==(unit_group const & other) const {
    if (this == &other)
      return true;     // equal by addr.
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
             mp::leaf_accessors(hana::type_c<unit_group>),
             id_base::operator==(other), // initial value of the fold
             [this, &other](bool s, auto&& a) {
               return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                        : false; // only evaluate equal if the fold state is still true
             })
        // compare members by value
        && (std::ranges::equal(
          members,
          other.members,
          {},
          [](auto const & a) -> auto const & {
            return *a;
          },
          [](auto const & a) -> auto const & {
            return *a;
          }));
  }

  void unit_group::add_unit(unit_ const & u, apoint_ts const & active_ts) {

    for (auto const & x : members) {
      // consider just return silent, since it's already there, the intention is reached.
      if (u.get() == x->unit.get())
        throw std::runtime_error("unit_group: unit '" + x->unit->name + "' already  in the group");
    }
    auto ugm = std::make_shared<unit_group_member>(this, u, active_ts);
    members.emplace_back(ugm);
    update_sum_expressions();
  }

  /** adding and removing units must update expressions, and ensure unique set */
  void unit_group::remove_unit(unit_ const & u) {
    auto f = std::ranges::find_if(members, [&u](auto const & x) {
      return x->unit.get() == u.get();
    });
    if (f != std::ranges::end(members))
      members.erase(f);
    update_sum_expressions();
  }

  /** @brief update the production and sum to express the units
   *
   *  @note That is not the ideal solution: better with compute on demand, maybe as a fx-expr..
   */
  void unit_group::update_sum_expressions() {
    using shyft::time_series::dd::ats_vector;
    constexpr auto mk_sum = [](auto const & members, auto&& fx_result) -> apoint_ts {
      ats_vector p;
      for (auto const & m : members) {
        auto const & result = fx_result(m);
        if (result.ts) {
          if (!m->active.ts) // if no active ts, then assume it's always active
            p.emplace_back(result);
          else
            p.emplace_back(result * m->active); // use math and mask
        }
      }
      return p.size() > 0 ? p.sum() : apoint_ts{}; // notice: empty ts if empty input
    };
    production = mk_sum(members, [](auto const & m) {
      return m->unit->production.result;
    });
    flow = mk_sum(members, [](auto const & m) {
      return m->unit->discharge.result;
    });
    switch (group_type) {
    //-- operational reserve groups
    case unit_group_type::fcr_n_up: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_n.up.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_n.up.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_n.up.schedule;
      });
    } break;
    case unit_group_type::fcr_n_down: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_n.down.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_n.down.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_n.down.schedule;
      });
    } break;
    case unit_group_type::fcr_d_up: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_d.up.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_d.up.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_d.up.schedule;
      });
    } break;
    case unit_group_type::fcr_d_down: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_d.down.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_d.down.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.fcr_d.down.schedule;
      });
    } break;
    case unit_group_type::afrr_up: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.afrr.up.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.afrr.up.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.afrr.up.schedule;
      });
    } break;
    case unit_group_type::afrr_down: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.afrr.down.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.afrr.down.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.afrr.down.schedule;
      });
    } break;
    case unit_group_type::mfrr_up: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.mfrr.up.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.mfrr.up.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.mfrr.up.schedule;
      });
    } break;
    case unit_group_type::mfrr_down: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.mfrr.down.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.mfrr.down.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->reserve.mfrr.down.schedule;
      });
    } break;
    case unit_group_type::ffr:
      break;
    //-- spinning
    case unit_group_type::commit: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->production.result.inside(1e-6, 1e100, 0.0, 1.0, 0.0);
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->production.realised.inside(1e-6, 1e100, 0.0, 1.0, 0.0);
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->production.schedule.inside(1e-6, 1e100, 0.0, 1.0, 0.0);
      }); // TODO: include production.commitment
    } break;
    //-- with production requirement
    case unit_group_type::unspecified: // make it production, or null? break;
    case unit_group_type::production: {
      delivery.result = mk_sum(members, [](auto const & m) {
        return m->unit->production.result;
      });
      delivery.realised = mk_sum(members, [](auto const & m) {
        return m->unit->production.realised;
      });
      delivery.schedule = mk_sum(members, [](auto const & m) {
        return m->unit->production.schedule;
      });
    } break;
    default:
      break;
    }
  }

  energy_market_area_ unit_group::get_energy_market_area() const {
    if (!mdl)
      return nullptr; //
    for (auto const & ema : mdl->market) {
      for (auto const & ug : ema->unit_groups) {
        if (ug.get() == this) {
          return ema;
        }
      }
    }
    return nullptr;
  }

  std::vector<std::string> unit_group::all_urls(std::string const & prefix) const {
    std::vector<std::string> r;
    std::string pre = prefix + ug_tag(id) + ".";
    hana::for_each(mp::leaf_accessor_map(hana::type_c<unit_group>), [&r, &pre](auto p) {
      r.push_back(pre + hana::first(p).c_str());
    });
    return r;
  }

  //-- unit group member.
  unit_group_member::unit_group_member() {
    mk_url_fx(this);
  }

  unit_group_member::unit_group_member(unit_group* group, unit_ const & u, apoint_ts const & a)
    : group{group}
    , unit{u}
    , active{a} {
    mk_url_fx(this);
  }

  bool unit_group_member::operator==(unit_group_member const & other) const {
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
             mp::leaf_accessors(hana::type_c<unit_group_member>),
             true, // initial value of the fold
             [this, &other](bool s, auto&& a) {
               return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                        : false; // only evaluate equal if the fold state is still true
             })
        && (unit == other.unit
            || (unit && other.unit && (*unit == *other.unit))); // require pointers to be equal in this case
  }

  void unit_group_member::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels)
    const {
    if (levels && group) {
      group->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/M{o_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/M" + (unit ? std::to_string(unit->id) : std::string("?"));
      std::ranges::copy(idstr, rbi);
    }
  }

  /** ref header file for docs
   */
  apoint_ts compute_unit_group_membership_ts(
    std::vector<unit_group_> const & groups,
    unit_ const & u,
    apoint_ts run_ts, //  a ts filled with one's for all time-steps of interest
    unit_group_type group_type,
    std::map<std::int64_t, std::int64_t> const * id_map) {
    apoint_ts result;                           // accumulate group-id at timesteps as pr. run_ts
    double const min_group = 1.0;               //  group-id has to be in range 1 .. 2**60
    double const max_group = std::pow(2.0, 60); // arbitrary large range
    for (auto const & ug : groups) {
      if (ug->group_type != group_type)
        continue;
      auto ug_id = ug->id;
      if (id_map) { // if supplied to translation
        auto f = id_map->find(ug_id);
        if (f == id_map->end())
          throw std::runtime_error(
            "Unit group id-map must have complete mapping, failed to find " + std::to_string(ug_id));
        ug_id = f->second;
      }
      for (auto const & m : ug->members) { // match member to u

        if (m->unit.get() == u.get()) { // got it!
          apoint_ts c = !m->active ?    // empty means always active
                          run_ts
                                   : // all time-steps of interest is active, 1.0
                          m->active.use_time_axis_from(run_ts).inside(
                            0.9, 1.1, 0.0, 1.0, 0.0); // resample to run_ts time-axis, 0 out when not active/nan
          // notice that c now is spanning the run_ts time-axis range (maybe with some zeros filled in)

          if (!result) {                // first time contribution?
            result = c * double(ug_id); // make result equal to the first contribution
          } else {                      // we must check if there is conflicts with existing membership accumulated.
            auto conflicts = (result.inside(min_group, max_group, 0.0, 1.0, 0.0) + c).values(); // make result 1| 0, add
            for (std::size_t i = 0; i < conflicts.size(); ++i) {
              if (conflicts[i] > 1.0 + 0.1) {
                calendar utc;
                throw std::runtime_error(
                  "Unit group conflict for unit " + u->name + " at time " + utc.to_string(run_ts.time(i)));
              }
            }
            result = result + c * double(ug_id); // multiply 1.0 with ug->id, so we get 0 or group_id, then add.
          }
        }
      }
    }
    return result;
  }

  /** ref header file for docs
   */
  apoint_ts compute_group_unit_combinations_ts(unit_group_ const & group, generic_dt const & time_axis) {
    if (group->members.size() >= std::numeric_limits<std::size_t>::digits) {
      // Since we are representing members as bits in a size_t, 64-bit unsigned integer, only 63 members are possible.
      throw std::runtime_error(
        std::string("Unit group member count ") + std::to_string(group->members.size())
        + std::string(" is larger than the ") + std::to_string(std::numeric_limits<std::size_t>::digits - 1)
        + std::string(" maximum possible to represent in a unit combinations ts"));
    }
    apoint_ts result{time_axis, 0.0, time_series::POINT_AVERAGE_VALUE};
    for (std::size_t i = 0, n = group->members.size(); i < n; ++i) {
      auto const & ugm = group->members[i];
      if (!ugm->active) {                                     // empty means always active
        result = result + (static_cast<std::size_t>(1) << i); // set unit number bit in all time steps
      } else {
        result = result
               + ugm->active.use_time_axis_from(result).inside(0.9, 1.1, 0.0, 1.0, 0.0)
                   * (static_cast<std::size_t>(1) << i); // resample active ts to time-axis, set 0 out when not
                                                         // active/nan, and set bit encoded unit id when active
      }
    }
    return result;
  }
}
