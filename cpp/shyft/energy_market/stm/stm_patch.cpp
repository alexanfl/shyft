#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>

#include <fmt/format.h>

namespace shyft::energy_market::stm {

  using shyft::energy_market::hydro_power::hydro_component;
  using shyft::energy_market::hydro_power::connection_role;

  /**
   * @brief locate and create/insert(w.auto unique id) if missing
   * @param v some iterable, where the members are shared_ptr, castable to type T
   * @param o the object to be located, and if not found, a clone should be created using o as template
   * @param clone_me a callable that takes o as parameter, and is expected to return back a shared_ptr of type T
   * @return the located shared ptr of type T that matches o, or a new clone of o, also appended to the vector v.
   */
  template <typename T>
  constexpr auto locate_or_create_component =
    [](auto &v, auto &o, auto &&clone_me, std::function<std::int64_t()> uid_fx = nullptr) {
      std::int64_t uid{0}; // in case we reach the create component conclusion, we might need a unique/fresh id
      auto f = std::ranges::find_if(v, [&o, &uid](auto const &x) {
        uid = std::max(uid, x->id);
        return x->id == o->id;
      });
      if (f != v.end())
        return std::dynamic_pointer_cast<T>(*f);
      auto x = clone_me(std::dynamic_pointer_cast<T>(o));
      if (x->id <= 0) { // if 0 or less than 0, then auto allocate new unique id, as in unique in the vector/list v
        x->id = uid_fx ? uid_fx() : uid + 1; // the uid+1 works for most things, except.. wtr.gates, must be unique
                                             // within hps scope, so we need uid_fx in that case
        o->id =
          x->id; // NOTE: we mutate the patch in this case, because we use patch obj.id when establishing the relations
      }
      v.push_back(x);
      return x;
    };

  /**
   * @brief assign lhs (shared_ptr) with content of rhs (pure value)
   * @return lhs for easy chaining
   */
  constexpr auto assign_struct = [](auto const &lhs, auto const &rhs) {
    hana::for_each(mp::leaf_accessors(hana::type_c<std::remove_cvref_t<decltype(rhs)>>), [&lhs, &rhs](auto a) {
      mp::leaf_access(*lhs, a) = mp::leaf_access(rhs, a);
    });
    lhs->tsm = rhs.tsm; // assign custom ts attributes as well
    return lhs;         // return to make it brief
  };

  /**
   * @brief locate patch or update vector
   * @tparam D the real type of the shared_ptr_T_, e.g. stm::reservoir vs. hydro_power::reservoir
   * @param s_parent ref to parent of the vector items
   * @param sv the system vector to patch
   * @param pv the vector that contains patches
   */
  template <typename D>
  constexpr auto update_vector = [](auto const &s_parent, auto &sv, auto const &pv) {
    for (auto const &e : pv)
      locate_or_create_component<D>(sv, e, [&s_parent](auto const &o) {
        return assign_struct(std::make_shared<D>(o->id, o->name, o->json, s_parent), *o);
      });
  };

  /**
   * @brief remove objects in patch from vector
   * @param sv the system vector to patch
   * @param pv the vector that contains patches
   *
   * @returns true if all objects in pv were found and removed
   */
  constexpr auto remove_from_vector = [](auto &sv, auto const &pv) {
    auto erased = std::erase_if(sv, [&](auto const &o) {
      return std::ranges::any_of(pv, [&](auto const &p) {
        return p->id == o->id;
      });
    });
    return erased == pv.size();
  };

  /**
   * @brief find an object as specified by fx
   * @detals find an object, and if not found, throw
   * @returns found object (by value, so, this works for ptr types.)
   */
  constexpr auto find_or_throw_fx = [](auto const &v, auto &&fx) {
    auto x = std::ranges::find_if(v, fx);
    if (x == std::end(v))
      throw std::runtime_error("Failed to lookup object");
    return *x;
  };

  ///< convinience to find an object with specified obj.id
  constexpr auto find_or_throw = [](auto const &v, std::int64_t id) {
    return find_or_throw_fx(v, [id](auto const &o) {
      return id == o->id;
    });
  };


  /**
   * @brief find hydrocomponent of specific type and id
   * @details
   * The search uses dynamic cast of c to determine the type,
   * then lookups in respective vectors(unit,reservoirs,waterways)
   * to find the matching id.
   * @note the c is only used for it's true type and id.
   * @param o the hps system that should be searched
   * @param c the hydro component to be found
   * @return found type in the hps o, that matches the type and object id of c.
   */
  constexpr auto find_hydro_component = [](auto const &o, auto const &c) -> std::shared_ptr<hydro_component> {
    if (auto x = std::dynamic_pointer_cast<waterway>(c)) {
      return find_or_throw(o->waterways, c->id);
    } else if (auto x = std::dynamic_pointer_cast<unit>(c)) {
      return find_or_throw(o->units, c->id);
    } else if (auto x = std::dynamic_pointer_cast<reservoir>(c)) {
      return find_or_throw(o->reservoirs, c->id);
    }
    return nullptr;
  };

  /**
   * @brief Check if hydro_component connections needs update
   * @details Verifies if the existing hydro_connection between upstream//downstream needs an update.
   * @param us the upstream component
   * @param us_role the role the ds component plays for us, e.g. a reservoir that flows into a downstream waterway as
   * bypass
   * @param ds the downstream component
   * @param ds_role the role the us component plays for ds, usually, always as 'input'
   * @return true if an update is needed (missing relation, or roles are different)
   */
  constexpr auto connect_needs_update =
    [](
      std::shared_ptr<hydro_component> const &us,
      connection_role us_role,
      std::shared_ptr<hydro_component> const &ds,
      connection_role ds_role) {
      auto ds_miss = std::end(us->downstreams) == std::ranges::find_if(us->downstreams, [&](auto const &hc) {
                       return hc.target.get() == ds.get() && (hc.role == us_role);
                     });
      auto us_miss = std::end(ds->upstreams) == std::ranges::find_if(ds->upstreams, [&](auto const &hc) {
                       return hc.target.get() == us.get() && (hc.role == ds_role);
                     });
      return ds_miss || us_miss;
    };

  /**
   * @brief patch stm_system s with patch p
   * @details
   * Given a stm_system,`s`, apply patch `p`,
   * additive, like:
   * 1. add new objects in p to s
   * 2. establish/ensure relations as mentioned in the patch model
   * 3. sideeffect on patch `p`, object-ids<=0 updated to assigned unique object ids.
   * @param s the stm_system to patch
   * @param p the patch described as a minimal stm_system only containing new objects/relations
   */
  bool add_patch(stm_system_ const &s, stm_system &p) {
    if (s == nullptr)
      return false;
    // phase 1:
    //   - just add new objects, by constructing new/assign relevant attributes on the target model.
    //   - also ensure parent/uplinks are in place
    //   - prepare for phase 2 (establish relations of the patch).
    for (auto const &o : p.hps) {
      auto x = locate_or_create_component<stm_hps>(
        s->hps,
        o,
        [](auto const &o) { // clone the hps it self:
          auto x = std::make_shared<stm_hps>(o->id, o->name);
          x->created = o->created;
          // handle h, we leave out, -its complicated-
          return x;
        });
      update_vector<reservoir_aggregate>(x, x->reservoir_aggregates, o->reservoir_aggregates);
      update_vector<power_plant>(x, x->power_plants, o->power_plants);
      for (auto const &u : o->units) {
        locate_or_create_component<unit>(x->units, u, [&x](auto const &o) {
          auto xo = std::make_shared<unit>(o->id, o->name, o->json, x);
          assign_struct(xo, *o);
          // now, we have to establish parent relations
          if (auto pp_id = o->pwr_station_() ? o->pwr_station_()->id : 0; pp_id != 0) {
            auto f = std::ranges::find_if(x->power_plants, [&pp_id](auto const &p) {
              return p->id == pp_id;
            });
            if (f != x->power_plants.end()) {
              power_plant::add_unit(std::dynamic_pointer_cast<power_plant>(*f), xo);
            }
          }
          return xo;
        });
      }
      update_vector<reservoir>(x, x->reservoirs, o->reservoirs);
      // some effort needed to ensure that auto allocated unique id for gate are hps unique.
      std::int64_t gate_uid{0}; // gate unique id
      for (auto const &w : o->waterways) {
        for (auto const &g : w->gates)
          gate_uid = std::max(g->id, gate_uid);
      }
      auto mk_unique_gate_id = [&gate_uid]() {
        return ++gate_uid; // so first will be 1, etc, if we start from scratch
      };

      for (auto const &w : o->waterways) {
        auto ws = locate_or_create_component<waterway>(x->waterways, w, [&x, &w](auto const &o) {
          return assign_struct(std::make_shared<waterway>(o->id, o->name, o->json, x), *o);
        });
        // then take care of the gates/nested
        for (auto const &g : w->gates) {
          locate_or_create_component<gate>(
            ws->gates,
            g,
            [&ws](auto const &gg) {
              auto xg = assign_struct(std::make_shared<gate>(gg->id, gg->name, gg->json), *gg);
              xg->wtr = ws; // assign uplink
              return xg;
            },
            mk_unique_gate_id // notice this one, if new gate id is allocated, then we supply the fx to do it properly
          );
        }
      }
    }
    // .market(s)

    update_vector<energy_market_area>(s, s->market, p.market);
    update_vector<contract>(s, s->contracts, p.contracts);
    update_vector<contract_portfolio>(s, s->contract_portfolios, p.contract_portfolios);
    // .networks
    for (auto const &n : p.networks) {
      auto x = locate_or_create_component<network>(s->networks, n, [&s](auto const &o) {
        return assign_struct(std::make_shared<network>(o->id, o->name, o->json, s), *o);
      });
      update_vector<transmission_line>(x, x->transmission_lines, n->transmission_lines);
      update_vector<busbar>(x, x->busbars, n->busbars); // add .busbars
    }
    // .power_modules
    update_vector<power_module>(s, s->power_modules, p.power_modules);
    // .unit_groups
    for (auto const &g : p.unit_groups) {
      auto x = locate_or_create_component<unit_group>(s->unit_groups, g, [&s](auto const &o) {
        return assign_struct(std::make_shared<unit_group>(o->id, o->name, o->json, s.get()), *o);
      });
      // sub components are unit_group.members,
      // we do have units by now so we can go ahead and create those
      for (auto const &m : g->members) {
        if (!m->unit)
          throw std::runtime_error(fmt::format("unit_group id= {}, patch with null unit", g->id));
        auto u = find_or_throw_fx(x->members, [&m](auto const &o) {
                   return o->unit->id == m->unit->id;
                 })->unit;         // locate unit by id...hmm. so m.unit.hps.id, and m.unit.id
        x->add_unit(u, m->active); // add u as active, need to be distinct..
      }
    }
    // TODO: .runparams(?)
    // TODO: .summary

    // phase 2: releate objects.
    //  - start with hps, they do have the hydro relations, and only waterways can releate
    //  -

    for (auto const &o : p.hps) { // walk over patch relations, and ensure they exists on target system s.
      auto xo = find_or_throw(s->hps, o->id);
      for (auto const &w : o->waterways) {
        auto xw = find_or_throw(xo->waterways, w->id);
        // upstream/downstreams. hydro_components
        // hydro_component::connect(upstream,urole, downstream,drole)
        // us->downstreams [us_role, ds]  R1. main->W1
        // ds->upstreams [ds_role,us]     W1. input ->R1
        for (auto const &hc : w->upstreams) {
          auto ds = xw; // this is the object where we are. us->downstreams[()]
          auto ds_role = hc.role;
          auto us = find_hydro_component(
            xo, hc.target); // the upstream hc.target, is a hydro component, of type waterway, reservoir or unit
          auto us_hc = find_or_throw_fx(hc.target->downstreams, [&](auto const &o) {
            return o.target == w;
          });
          auto us_role = us_hc.role;
          if (connect_needs_update(us, us_role, ds, ds_role))
            hydro_component::connect(us, us_role, ds, ds_role);
        }
        for (auto const &hc : w->downstreams) {
          auto us = xw;           // we are the upstream part here
          auto us_role = hc.role; // this is how we see the downstream object
          auto ds = find_hydro_component(xo, hc.target);
          auto ds_hc = find_or_throw_fx(hc.target->upstreams, [&](auto const &o) {
            return o.target == w;
          });
          auto ds_role = ds_hc.role;
          if (connect_needs_update(us, us_role, ds, ds_role))
            hydro_component::connect(us, us_role, ds, ds_role);
        }
      }
    }

    // .markets (relates to contracts,busbars,unit_groups,... and private unit_group)
    for (auto const &m : p.market) {
      auto ms = find_or_throw(s->market, m->id);
      for (auto const &c : m->contracts)
        ms->contracts.emplace_back(find_or_throw(ms->contracts, c->id));

      for (auto const &b : m->busbars)
        ms->busbars.emplace_back(find_or_throw(ms->busbars, b->id));

      for (auto const &u : m->unit_groups)
        ms->unit_groups.emplace_back(find_or_throw(ms->unit_groups, u->id));

      auto pug = m->get_busbar_derived_unit_group();
      if (pug) { // only *add* releation
        auto pugs = ms->get_busbar_derived_unit_group();
        if (!pugs || pugs->id != pug->id) { // different relation? then update
          auto bb_ug = find_or_throw(ms->unit_groups, pug->id);
          ms->set_busbar_derived_unit_group(bb_ug);
        }
      }
    }

    // .contracts (relates to power plants(aka units), and other contracts)
    for (auto const &c : p.contracts) {
      auto sc = find_or_throw(s->contracts, c->id);
      for (auto const &c_pp : c->power_plants) { // fixup power-plants(will be units) relation
        auto const &c_pp_hps = find_or_throw(s->hps, c_pp->hps_()->id);
        auto sc_pp = std::dynamic_pointer_cast<power_plant>(find_or_throw(c_pp_hps->power_plants, c_pp->id));
        if (!std::ranges::any_of(sc->power_plants, [&c_pp](auto const &o) {
              return o->id == c_pp->id;
            }))
          sc->power_plants.push_back(sc_pp);
      }
      for (auto const &r_c : c->relations) { // fixup relations for contract r_c
        if (!std::ranges::any_of(sc->relations, [&r_c](auto const &o) {
              return o->id == r_c->id;
            })) {
          std::shared_ptr<contract> related{
            r_c->related ? // in theory, could be null.
              find_or_throw(s->contracts, r_c->related->id)
                         : nullptr};
          sc->add_relation(r_c->id, related, r_c->relation_type);
        }
      }
    }
    // TODO: .contract_portfolios
    // TODO: .networks
    // TODO: .power_modules
    // TODO: .unit_groups
    return true;
  }

  /**
   * @brief remove objects contained in patch p from stm_system s
   * @details
   * Given a stm_system,`s`, apply patch `p`,
   *
   * 1. remove relations and references ro the removed objects
   * 2. remove objects in p from s if present
   * @param s the stm_system to patch
   * @param p the patch described as a minimal stm_system only containing objects to remove
   *
   * TODO: only implemented for contracts
   */
  bool remove_objects_patch(std::shared_ptr<stm_system> const &s, stm_system const &p) {
    if (s == nullptr)
      return false;
    // phase 1: remove relations of objects to be removed mentioned:

    // remove contracts from portfolios:
    for (auto const &pf : s->contract_portfolios) {
      remove_from_vector(pf->contracts, p.contracts);
    }
    // remove relations refering to contracts:
    for (auto const &c : s->contracts) {
      std::erase_if(c->relations, [&](auto const &r) {
        return std::ranges::any_of(p.contracts, [&](auto const &cr) {
          return cr->id == r->related->id;
        });
      });
    }

    // phase 2: Remove objects:
    // TODO: .hps
    // TODO: .market(s)
    auto all_found = remove_from_vector(s->contracts, p.contracts);
    // TODO: .networks
    // TODO: .power_modules
    // TODO: .unit_groups
    // TODO: .runparams(?)
    // TODO: .summary
    return all_found;
  }

  /**
   * @brief patch a system s with a patch p
   * @details
   *
   * The patch operation can be one of:
   *
   *  - add: objects/relations are added, so that the system s is updated with all (new) information and relations from
   * p
   *  - remove_relations: ensure to remove relations mentioned in the patch model p.
   *  - remove_objects: remove leaf-node objects mentioned in the patch model p.
   *
   * Precondition and rules:
   *
   * - The objects/stm_system is sufficiently unique using the object.id to identify same object of same type.
   * - For add operation, objects with type& object.id similar to the stm_system s model, are considered references(no
   * update on the target model happens)
   * - For add operation, objects with new type&id identification is created, and the attributes of the new object is
   * transferred to the new object in the model
   *
   * - For remove_object operation, contracts that match id are removed, together with all references to them.
   * - remove_object returns true if all contracts where found and removed, false if some of the patch object where not
   * found all found contracts are removes on a best-effort strategy
   *
   * @param s the stm_system to be patched
   * @param op the patch operation to be performed
   * @param p the patch, described as a partial stm_system(keeping only the essentials for the patch)
   * @note that the patch `p` object ids are updated with new/unique object ids if they are <=0
   */
  bool patch(stm_system_ const &s, stm_patch_op op, stm_system &p) {
    if (op == stm_patch_op::add) {
      return add_patch(s, p);
    }
    if (op == stm_patch_op::remove_objects) {
      return remove_objects_patch(s, p);
    }
    // TODO: implement remove_relations
    return false; // operation not yet supported.
  }
}
