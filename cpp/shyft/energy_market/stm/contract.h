#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <memory>

#include <boost/hana/define_struct.hpp>
#include <fmt/core.h>

#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/formatters.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/power_plant.h>

namespace shyft::energy_market::stm {
  using std::string;
  using std::vector;
  using std::shared_ptr;
  using std::weak_ptr;
  using time_series::dd::apoint_ts;

  struct contract_portfolio;
  using contract_portfolio_ = shared_ptr<contract_portfolio>;

  struct contract_relation;
  using contract_relation_ = shared_ptr<contract_relation>;

  /**
   * @brief Contract
   *
   * @details
   * Represents a financial or physical contract, optionally associated with
   * energy market area, power plant, or portfolio.
   *
   * It also has related contracts, relations,  to support building and maintaining contracts/bids that
   * are inclusive, exclusive etc.
   *
   * A contract has an identity, id, as provided by the user.
   * An important property of contracts/bids are they need state/life-time support.
   * E.g. when a contract is formulated (proposed), its kind of 'new', and only known to one of the participants.
   * Later, when the contract(s) is sent to the other participants, e.g. a bid sent to the market,
   * then it is promoted to 'obligation', assuming until
   */
  struct contract : id_base {
    using super = id_base;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

    contract() {
      mk_url_fx(this);
    }

    contract(int id, string const & name, string const & json, stm_system_ const & sys)
      : super{id, name, json, {}, {}, {}}
      , sys{sys} {
      mk_url_fx(this);
    }

    bool operator==(contract const & other) const;

    bool operator!=(contract const & other) const {
      return !(*this == other);
    }

    stm_system_ sys_() const {
      return sys.lock();
    }

    stm_system__ sys;                   ///< Reference up to the 'owning' optimization system.
    contract_ shared_from_this() const; // Get shared pointer from this, via sys.

    struct constraint_ {
      url_fx_t url_fx;
      BOOST_HANA_DEFINE_STRUCT(
        constraint_,
        (apoint_ts, min_trade),                ///< Minimum quantity (volume) that can be traded on the contract
        (apoint_ts, max_trade),                ///< Maximum quantity (volume) that can be traded on the contract
        (apoint_ts, ramping_up),               ///< Max limit of ramping up power between timesteps
        (apoint_ts, ramping_down),             ///< Min limit of ramping up power between timesteps
        (apoint_ts, ramping_up_penalty_cost),  ///< Penalty of violating ramping up limit
        (apoint_ts, ramping_down_penalty_cost) ///< Penalty of violating ramping down limit
      );
    };

    BOOST_HANA_DEFINE_STRUCT(
      contract,
      (apoint_ts, quantity), ///< Quantity (volume), e.g. W,J/s (maybe energy_flow is better?).
      (apoint_ts, price),    ///< Price per quantity unit, e.g. EUR/J.
      (apoint_ts, fee),      ///< Any contract fees, currency unit.
      (apoint_ts, revenue),  ///< Revenue, actual or forecast. Usually a calculated value, e.g. cash_flow = volume*price
                             ///< - fee [EUR/s].
      (string, parent_id),   ///< Optional reference to parent (contract) (maybe use weakptr?).
      (string, buyer),       ///< Name of the buyer actor (later a ref to actor).
      (string, seller),      ///< Name of the seller actor (later a ref to actor).
      (apoint_ts, active),   ///< Contract status (e.g. active, inactive).
      (apoint_ts,
       validation),     ///< Validation status (e.g. whether the contract has been validated, and at which level).
      (t_xy_, options), ///< A xy representaion of energy bid/ask curve along time axis t.
      (constraint_, constraint) ///< Contract restrictions related to volume (power)
    );

    SHYFT_DEFINE_STRUCT(
      contract,
      (),
      (quantity, price, fee, revenue, parent_id, buyer, seller, active, validation, options, constraint));

    /** Get associated energy market areas. */
    vector<energy_market_area_> get_energy_market_areas() const;
    /** Associated with energy market area. */
    void add_to_energy_market_area(energy_market_area_ market) const;

    /** Get associated contract portfolios. */
    vector<contract_portfolio_> get_portfolios() const;
    /** Associate with contract portfolio. */
    void add_to_portfolio(contract_portfolio_ portfolio) const;

    /**Add and remove contract relation */
    contract_relation_ add_relation(int64_t id, contract_ new_related, uint16_t relation_type);
    void remove_relation(contract_relation_ const &);

    /** find_related_to_this */
    vector<contract_> find_related_to_this() const;

    vector<power_plant_> power_plants; ///< Association with power plants.
    vector<contract_relation_>
      relations; ///< Relations to other contracts/bids. notice that we NEED to use add/remove above for semantics

    x_serialize_decl();
  };

  using contract_ = shared_ptr<contract>;
  using contract__ = weak_ptr<contract>;

  /**
   * @brief contract relation support
   *
   * @details
   * Used by contract to build/maintain contextual relations to other contracts.
   *
   * The object is currently shared to py as shared_ptr.
   * and is contained in the contract as vector of shared ptrs.
   *
   * The releation it self is meant to be 'used', created, maintained
   * in the context of contract. Eg. contract is the sole owner for the relation.
   * (Which we usually would represent as by value vector objects.)
   *
   */
  struct contract_relation {
    contract_relation();
    contract_relation(int64_t id, contract_ const & owner, contract_ const & c, uint16_t relation_type);

    contract__ owner; ///< the  contract_relation owner (weak ptr to soft assume lifetime(contract)>= contract_relation)

    contract_ owner_() const {
      return owner.lock();
    };

    contract_relation_ shared_from_this() const; // needed to get a true shared ptr from this in py-department

    //-- stuff to ensure url-generation
    void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;
    url_fx_t url_fx;

    bool operator==(contract_relation const & other) const;

    bool operator!=(contract_relation const & other) const {
      return !(*this == other);
    }

    contract_ related; ///< contract for the relation

    std::int64_t id{0};

    BOOST_HANA_DEFINE_STRUCT(
      contract_relation,
      (std::uint16_t, relation_type) ///< integer relation type, to be set by user
    );

    SHYFT_DEFINE_STRUCT(contract_relation, (), (id, relation_type));

    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::energy_market::stm::contract);
x_serialize_export_key(shyft::energy_market::stm::contract_relation);
BOOST_CLASS_VERSION(shyft::energy_market::stm::contract, 1);
BOOST_CLASS_VERSION(shyft::energy_market::stm::contract_relation, 1);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::contract, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::contract, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::contract>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::contract, Char> { };

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::contract_relation);

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::contract_relation>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::contract_relation, Char> { };
