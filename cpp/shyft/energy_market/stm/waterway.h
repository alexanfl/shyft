#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <iterator>
#include <map>
#include <string>
#include <stdexcept>

#include <fmt/core.h>

#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/formatters.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::time_series::dd::apoint_ts;

  struct gate;
  using gate_ = std::shared_ptr<gate>;

  struct waterway;
  using waterway_ = std::shared_ptr<waterway>;

  /** @brief Waterway, or water route, representing a tunnel or river
   *
   */
  struct waterway : hydro_power::waterway {
    using super = hydro_power::waterway;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    waterway(int id, std::string const & name, std::string const & json, stm_hps_& hps)
      : super(id, name, json, hps) {
      mk_url_fx(this);
    }

    waterway() {
      mk_url_fx(this);
    }

    bool operator==(waterway const & other) const;

    bool operator!=(waterway const & other) const {
      return !(*this == other);
    }

    // static gate_ add_gate(waterway_ const& w, int id, const std::string& name, const std::string& json);
    // static gate_ add_gate(waterway_ const& w, gate_ const& g);

    struct geometry_ {
      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        geometry_,
        (apoint_ts, length),   ///< m, the length of the waterway, the ts reflect that it can change
        (apoint_ts, diameter), ///< m, the diameter of typically a tunnel
        (apoint_ts, z0),       ///< masl at first end
        (apoint_ts, z1)        ///< masl at second end
      );
      SHYFT_DEFINE_STRUCT(geometry_, (), (length, diameter, z0, z1));
    };

    struct discharge_ {

      struct constraint_ {
        url_fx_t url_fx;
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min),
          (apoint_ts, max),
          (apoint_ts, ramping_up),
          (apoint_ts, ramping_down),
          (apoint_ts, accumulated_min),
          (apoint_ts, accumulated_max));
        SHYFT_DEFINE_STRUCT(constraint_, (), (min, max, ramping_up, ramping_down, accumulated_min, accumulated_max));
      };

      struct penalty_ {
        struct cost_ {
          url_fx_t url_fx;
          BOOST_HANA_DEFINE_STRUCT(
            cost_,
            (apoint_ts, rate),
            (apoint_ts, constraint_min),
            (apoint_ts, constraint_max),
            (apoint_ts, ramping_up),
            (apoint_ts, ramping_down),
            (apoint_ts, accumulated_min),
            (apoint_ts, accumulated_max),
            (t_xy_, peak_curve)); // peak flow  m3/s, associated cost money/m3/s
          SHYFT_DEFINE_STRUCT(
            cost_,
            (),
            (rate,
             constraint_min,
             constraint_max,
             ramping_up,
             ramping_down,
             accumulated_min,
             accumulated_max,
             peak_curve));
        };

        struct result_ {
          url_fx_t url_fx;
          BOOST_HANA_DEFINE_STRUCT(
            result_,
            (apoint_ts, rate),
            (apoint_ts, constraint_min),
            (apoint_ts, constraint_max),
            (apoint_ts, ramping_down),
            (apoint_ts, ramping_up),
            (apoint_ts, accumulated_min),
            (apoint_ts, accumulated_max));
          SHYFT_DEFINE_STRUCT(
            result_,
            (),
            (rate, constraint_min, constraint_max, ramping_up, ramping_down, accumulated_min, accumulated_max));
        };

        url_fx_t url_fx;
        BOOST_HANA_DEFINE_STRUCT(penalty_, (cost_, cost), (result_, result));

        SHYFT_DEFINE_STRUCT(penalty_, (), (cost, result));
      };

      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        discharge_,
        (apoint_ts, schedule),   // m3/s scheduled flow
        (apoint_ts, reference),  // m3/s constraint reference, acc-dev, could be different from schedule
        (apoint_ts, static_max), // m3/s abs. value, both directions
        (apoint_ts, result),     // m3/s as in result from optimization/simulation
        (apoint_ts, realised),   // m3/s as in best estimated/measured/observed historical fact
        (constraint_, constraint),
        (penalty_, penalty));
      SHYFT_DEFINE_STRUCT(discharge_, (), (schedule, reference, static_max, result, realised, constraint, penalty));
    };

    struct deviation_ {
      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(deviation_, (apoint_ts, realised), (apoint_ts, result));
      SHYFT_DEFINE_STRUCT(deviation_, (), (realised, result));
    };
    struct flow_description_ {

      struct delta_meter_ {
        url_fx_t url_fx;
        BOOST_HANA_DEFINE_STRUCT(
          delta_meter_,
          (t_xyz_list_, upstream_ref),   ///< x: difference between up- and downstream level, y: flow, z: upstream level
          (t_xyz_list_, downstream_ref)  ///< x: difference between up- and downstream level, y: flow, z: downstream level
        );
        SHYFT_DEFINE_STRUCT(delta_meter_, (), (upstream_ref, downstream_ref));
      };

      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        flow_description_,
        (t_xy_, upstream_ref),   ///< x: upstream level, y: flow at given upstream level.
        (delta_meter_, delta_meter)
        );
      SHYFT_DEFINE_STRUCT(flow_description_, (), (upstream_ref));
    };

    BOOST_HANA_DEFINE_STRUCT(
      waterway,
      (apoint_ts, head_loss_coeff),  ///< m/(m3/s)**2 head_loss = q*abs(q)*head_loss_coeff
      (t_xyz_list_, head_loss_func), ///< head_loss= f(flow,delta_h) for complex head-loss, using table lookup
      (t_xy_, delay),
      (geometry_, geometry),
      (flow_description_, flow_description),
      (discharge_, discharge),
      (deviation_, deviation));
    SHYFT_DEFINE_STRUCT(
      waterway,
      (hydro_power::waterway),
      (head_loss_coeff, head_loss_func, delay, geometry, flow_description, discharge, deviation));

    x_serialize_decl();
  };

  /** @brief Gate, or hatch, which controls flow into waterways
   *
   * Gate's are active/passive elements of the hydro-power-system.
   * They can be controlled, some are binary (open/closed), typically tunnel-gates.
   * Gates for flood/bypass and be operated from closed up to full opening in steps,
   * to fulfil a certian plan or requirement.
   * Passive gates, are gates where the water flows when above certain level, typically for flooding
   * reservoirs etc.
   *
   * There can be several gates into a water-route, and they can be operated separately.
   *
   * Gate-plans (or requirements), can be positional, or by wanted flow.
   * For positional gate-plans (cm, m, % of max opening etc.), this influences the gate-opening
   * and the other factors, such as water-level up/down-stream determines the resulting flow.
   * For flow-based gate-plan (m3/s), - it's the other way around, the system would then
   * try to figure out what gate-position (if any) that would give the wanted flow.
   *
   */
  struct gate : hydro_power::gate {
    using super = hydro_power::gate;

    /** @brief Generate an almost unique, url-like std::string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    gate(int id, std::string const & name, std::string const & json);
    gate();

    bool operator==(gate const & other) const;

    bool operator!=(gate const & other) const {
      return !(*this == other);
    };

    struct opening_ {
      struct constraint_ {
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (t_xy_, positions),     ///< x: arbitrary position / y: opening factor
          (apoint_ts, continuous) ///< x: time / y: 0 or 1
        );
        SHYFT_DEFINE_STRUCT(constraint_, (), (positions, continuous));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        opening_,
        (apoint_ts, schedule),
        (apoint_ts, realised),
        (constraint_, constraint),
        (apoint_ts, result));
      SHYFT_DEFINE_STRUCT(opening_, (), (schedule, realised, constraint, result))
    };

    struct discharge_ {
      struct constraint_ {
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min), ///< m3/s
          (apoint_ts, max)  ///< m3/s
        );
        url_fx_t url_fx; // needed by .url(...) to python exposure
        SHYFT_DEFINE_STRUCT(constraint_, (), (min, max));
      };

      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        discharge_,
        (apoint_ts, static_max),
        (apoint_ts, schedule),
        (apoint_ts, realised),
        (constraint_, constraint),
        (apoint_ts, merge_tolerance), ///< m3/s, Max deviation in discharge between two timesteps
        (apoint_ts, result));
      SHYFT_DEFINE_STRUCT(discharge_, (), (static_max, schedule, realised, constraint, merge_tolerance, result));
    };

    BOOST_HANA_DEFINE_STRUCT(
      gate,
      (t_xyz_list_, flow_description),
      (t_xyz_list_, flow_description_delta_h),
      (apoint_ts, cost), ///< money
      (opening_, opening),
      (discharge_, discharge));
    SHYFT_DEFINE_STRUCT(
      gate,
      (hydro_power::gate),
      (flow_description, flow_description_delta_h, cost, opening, discharge));

    x_serialize_decl();
  };

  // ref impl. for details/context
  extern double initial_acc_deviation(utctime t0, apoint_ts const & actual, apoint_ts const & reference);
}

x_serialize_export_key(shyft::energy_market::stm::waterway);
x_serialize_export_key(shyft::energy_market::stm::gate);
BOOST_CLASS_VERSION(shyft::energy_market::stm::waterway, 3);
BOOST_CLASS_VERSION(shyft::energy_market::stm::gate, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::geometry_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::discharge_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::discharge_::penalty_::cost_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::discharge_::penalty_::result_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::discharge_::penalty_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::discharge_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::flow_description_::delta_meter_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::flow_description_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::waterway::deviation_);


SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::gate::opening_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::gate::opening_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::gate::discharge_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::gate::discharge_);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::waterway, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::waterway, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::waterway>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::waterway, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::gate, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::gate, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::gate>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::gate, Char> { };
