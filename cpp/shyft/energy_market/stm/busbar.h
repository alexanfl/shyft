#pragma once

#include <iterator>
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/mp.h>

namespace shyft::energy_market::stm {

  struct unit;
  using unit_ = std::shared_ptr<unit>;

  struct unit_member {
    unit_member();
    unit_member(busbar* owner, unit_ const & u, apoint_ts const & active);
    bool operator==(unit_member const & other) const;

    bool operator!=(unit_member const & other) const {
      return !(*this == other);
    }

    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;
    int64_t id() const;

    busbar* owner{};
    url_fx_t url_fx;
    unit_ unit;

    BOOST_HANA_DEFINE_STRUCT(
      unit_member,
      (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
    );

    SHYFT_DEFINE_STRUCT(unit_member, (), (active));

    x_serialize_decl();
  };

  using unit_member_ = std::shared_ptr<unit_member>;

  struct power_module_member {
    power_module_member();
    power_module_member(busbar* owner, power_module_ const & u, apoint_ts const & active);
    bool operator==(power_module_member const & other) const;

    bool operator!=(power_module_member const & other) const {
      return !(*this == other);
    }

    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;
    int64_t id() const;

    busbar* owner{};
    url_fx_t url_fx;
    power_module_ power_module;
    BOOST_HANA_DEFINE_STRUCT(
      power_module_member,
      (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
    );

    SHYFT_DEFINE_STRUCT(power_module_member, (), (active));

    x_serialize_decl();
  };

  using power_module_member_ = std::shared_ptr<power_module_member>;

  struct wind_farm_member {
    wind_farm_member();
    wind_farm_member(busbar* owner, std::shared_ptr<wind_farm> const & t, apoint_ts const & active);
    bool operator==(wind_farm_member const & other) const;

    bool operator!=(wind_farm_member const & other) const {
      return !(*this == other);
    }

    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;
    int64_t id() const;

    busbar* owner{};
    url_fx_t url_fx;
    std::shared_ptr<wind_farm> farm;
    BOOST_HANA_DEFINE_STRUCT(
      wind_farm_member,
      (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
    );

    SHYFT_DEFINE_STRUCT(wind_farm_member, (), (active));

    x_serialize_decl();
  };

  /** @brief Busbar
   *
   * Busbar, a hub connected by transmission lines
   */
  struct busbar : id_base {
    using super = id_base;

    busbar() {
      mk_url_fx(this);
    }

    busbar(int id, std::string const & name, std::string const & json, network_ const & net)
      : super{id, name, json, {}, {}, {}}
      , net{net} {
      mk_url_fx(this);
    }

    bool operator==(busbar const & other) const;

    bool operator!=(busbar const & other) const {
      return !(*this == other);
    }

    /** @brief generate an almost unique, url-like std::string for a busbar.
     *
     * @param rbi: back inserter to store result
     * @param levels: How many levels of the url to include.
     * 		levels == 0 includes only this level. Use level < 0 to include all levels.
     * @param placeholders: The last element of the vector states wethers to use the busbar ID
     * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
     * 		If the vector is empty, the function defaults to not using placeholders.
     * @return
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    network_ net_() const {
      return net.lock();
    }

    network__ net;                    ///< Reference up to the 'owning' network.
    busbar_ shared_from_this() const; // Get shared pointer from this, via network.

    std::vector<unit_member_> units;
    std::vector<power_module_member_> power_modules;
    std::vector<std::shared_ptr<wind_farm_member>> wind_farms;

    void add_unit(unit_ u, apoint_ts const & active_ts);
    void remove_unit(unit_);
    void add_power_module(power_module_, apoint_ts const & active_ts);
    void remove_power_module(power_module_);
    void add_wind_farm(std::shared_ptr<wind_farm>, apoint_ts const & active_ts);
    void remove_wind_farm(std::shared_ptr<wind_farm>);

    struct ts_triplet {
      url_fx_t url_fx;
      BOOST_HANA_DEFINE_STRUCT(
        ts_triplet,
        (apoint_ts, realised), ///< SI unit, as in historical fact
        (apoint_ts, schedule), ///< the current schedule
        (apoint_ts, result)    ///< the optimal/simulated/estimated result
      );

      SHYFT_DEFINE_STRUCT(ts_triplet, (), (realised, schedule, result));
    };

    BOOST_HANA_DEFINE_STRUCT(
      busbar,
      (ts_triplet, flow), ///< flow in/out of busbar, W, J/s, flow in=negative, flow out=positive
      (ts_triplet, price) ///< price
    );
    SHYFT_DEFINE_STRUCT(busbar, (id_base), (flow, price));

    std::vector<transmission_line_> get_transmission_lines_from_busbar() const;
    std::vector<transmission_line_> get_transmission_lines_to_busbar() const;
    std::vector<energy_market_area_> get_market_areas() const;

    void add_to_start_of_transmission_line(transmission_line_) const;
    void add_to_end_of_transmission_line(transmission_line_) const;
    void add_to_market_area(energy_market_area_) const;

    x_serialize_decl();
  };

  using busbar_ = std::shared_ptr<busbar>;
  using busbar__ = weak_ptr<busbar>;
}

x_serialize_export_key(shyft::energy_market::stm::busbar);
x_serialize_export_key(shyft::energy_market::stm::unit_member);
x_serialize_export_key(shyft::energy_market::stm::power_module_member);
x_serialize_export_key(shyft::energy_market::stm::wind_farm_member);
BOOST_CLASS_VERSION(shyft::energy_market::stm::busbar, 2);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_module_member);
SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::power_module_member);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::unit_member);
SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::unit_member);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm_member);
SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::wind_farm_member);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::busbar::ts_triplet);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::busbar);
SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::busbar);
