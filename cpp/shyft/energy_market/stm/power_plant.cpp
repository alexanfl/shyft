#include <shyft/energy_market/stm/power_plant.h>

#include <iterator>
#include <memory>
#include <ranges>
#include <string>
#include <string_view>

#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  power_plant::power_plant(
    int id,
    std::string const & name,
    std::string const & json,
    std::shared_ptr<stm_hps> const & hps)
    : super(id, name, json, hps) {
    mk_url_fx(this);
  }

  power_plant::power_plant() {
    mk_url_fx(this);
  }

  void power_plant::add_unit(std::shared_ptr<power_plant> const & ps, std::shared_ptr<unit> const & a) {
    hydro_power::power_plant::add_unit(ps, a);
  }

  void power_plant::remove_unit(std::shared_ptr<unit> const & a) {
    super::remove_unit(a);
  }

  void power_plant::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = std::dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }

    if (!template_levels) {
      constexpr std::string_view a = "/P{o_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/P" + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }

  bool power_plant::operator==(power_plant const & other) const {
    if (this == &other)
      return true; // equal by addr.

    auto equal_attributes = hana::fold(
      mp::leaf_accessors(hana::type_c<power_plant>), super::operator==(other), [this, &other](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)) : false;
      });
    auto equal_unit_attributes = [this, &other]() {
      return std::is_permutation(
        units.begin(), units.end(), other.units.begin(), other.units.end(), [](const auto& a, const auto& b) {
          return a == b || // this is questionable, because, equal-attributes starts ensuring neighbor is the same(by
                           // id), this is doing value compare, of not owned objects
                 (std::dynamic_pointer_cast<unit>(a) && std::dynamic_pointer_cast<unit>(b)
                  && (*std::dynamic_pointer_cast<unit>(a) == *std::dynamic_pointer_cast<unit>(b)));
        });
    };
    return equal_attributes && equal_unit_attributes();
  }
}
