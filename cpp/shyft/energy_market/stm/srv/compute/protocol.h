#pragma once

#include <cstdint>
#include <iostream>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

#include <fmt/core.h>
#include <fmt/chrono.h>
#include <fmt/std.h>
#include <fmt/ranges.h>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

#include <shyft/core/boost_serialization_std_variant.h>
#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/fmt_std_opt.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::compute {

#define SHYFT_STM_COMPUTE_STATE (idle, started, running, done)

  SHYFT_DEFINE_ENUM(state, std::uint8_t, SHYFT_STM_COMPUTE_STATE)

#define SHYFT_STM_COMPUTE_PROTOCOL (start, get_status, get_attrs, set_attrs, stop, plan, get_plan)

  SHYFT_DEFINE_ENUM(message_tag, std::uint8_t, SHYFT_STM_COMPUTE_PROTOCOL);

  inline constexpr auto message_num = enumerator_count<message_tag>;

  inline constexpr message_tag error_tag{message_num};

  template <message_tag message>
  struct request {
    static constexpr std::integral_constant<message_tag, message> tag{};

    SHYFT_DEFINE_STRUCT(request, (), ());

    auto operator<=>(request const &) const = default;
  };

  template <>
  struct request<message_tag::get_status> {
    static constexpr std::integral_constant<message_tag, message_tag::get_status> tag{};
    std::optional<std::size_t> log_index;

    SHYFT_DEFINE_STRUCT(request, (), (log_index));

    auto operator<=>(request const &) const = default;
  };

  template <>
  struct request<message_tag::start> {
    static constexpr std::integral_constant<message_tag, message_tag::start> tag{};

    std::string model_id;
    std::shared_ptr<stm_system> model;

    SHYFT_DEFINE_STRUCT(request, (), (model_id, model));

    auto operator<=>(request const &) const = default;
  };

  template <>
  struct request<message_tag::plan> {
    static constexpr std::integral_constant<message_tag, message_tag::plan> tag{};

    time_axis::generic_dt time_axis;
    std::vector<shop::shop_command> commands;

    SHYFT_DEFINE_STRUCT(request, (), (time_axis, commands));

    auto operator<=>(request const &) const = default;
  };

  template <>
  struct request<message_tag::get_attrs> {
    static constexpr std::integral_constant<message_tag, message_tag::get_attrs> tag{};

    std::vector<std::string> urls;

    SHYFT_DEFINE_STRUCT(request, (), (urls));

    auto operator<=>(request const &) const = default;
  };

  template <>
  struct request<message_tag::set_attrs> {
    static constexpr std::integral_constant<message_tag, message_tag::set_attrs> tag{};

    std::vector<std::pair<std::string, stm::any_attr>> attrs;

    SHYFT_DEFINE_STRUCT(request, (), (attrs));

    auto operator<=>(request const &) const = default;
  };

#define SHYFT_LAMBDA(r, data, elem) using elem##_request = request<message_tag::elem>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA

  template <message_tag message>
  struct reply {
    static constexpr std::integral_constant<message_tag, message> tag{};

    SHYFT_DEFINE_STRUCT(reply, (), ());

    auto operator<=>(reply const &) const = default;
  };

  template <>
  struct reply<message_tag::get_status> {
    static constexpr std::integral_constant<message_tag, message_tag::get_status> tag{};

    compute::state state;
    std::vector<log_entry> log;

    SHYFT_DEFINE_STRUCT(reply, (), (state, log));

    auto operator<=>(reply const &) const = default;
  };

  template <>
  struct reply<message_tag::get_attrs> {
    static constexpr std::integral_constant<message_tag, message_tag::get_attrs> tag{};

    std::vector<std::optional<stm::any_attr>> attrs;

    SHYFT_DEFINE_STRUCT(reply, (), (attrs));

    auto operator<=>(reply const &) const = default;
  };

  template <>
  struct reply<message_tag::set_attrs> {
    static constexpr std::integral_constant<message_tag, message_tag::set_attrs> tag{};

    std::vector<bool> attrs;

    SHYFT_DEFINE_STRUCT(reply, (), (attrs));

    auto operator<=>(reply const &) const = default;
  };

  template <>
  struct reply<message_tag::get_plan> {

    static constexpr std::integral_constant<message_tag, message_tag::get_plan> tag{};

    std::shared_ptr<optimization_summary> summary; // NOTE: use shared_ptr to get a regular type - jeh

    SHYFT_DEFINE_STRUCT(reply, (), (summary));

    auto operator<=>(reply const &) const = default;
  };

#define SHYFT_LAMBDA(r, data, elem) using elem##_reply = reply<message_tag::elem>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA

  using any_request = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, request>>;
  using any_reply = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, reply>>;

  inline auto read_blob(std::istream &s, message_tag &m) {
    return reflection::read_blob(s, m);
  }

  inline auto write_blob(std::ostream &s, message_tag const &m) {
    return reflection::write_blob(s, m);
  }

  inline void write_error_message(std::ostream &s, std::string_view o) {
    std::uint64_t n = o.size();
    s.write((char const *) &n, sizeof(n));
    s.write(o.data(), o.size());
  }

  inline std::optional<std::string> try_read_error_message(std::istream &s) {
    std::uint64_t n{0};
    s.read((char *) &n, sizeof(n));
    if (!s)
      return std::nullopt;
    std::string e(n, '\0');
    s.read(e.data(), n);
    if (!s)
      return std::nullopt;
    return e;
  }

  template <typename A, message_tag t>
  void serialize(A &archive, reply<t> &m, unsigned int const v) {
    reflection::serialize(archive, m, v);
  }

  template <typename A, message_tag t>
  void serialize(A &archive, request<t> &m, unsigned int const v) {
    reflection::serialize(archive, m, v);
  }

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::compute::state);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::compute::message_tag);

template <shyft::energy_market::stm::srv::compute::message_tag M, typename Char>
struct fmt::formatter<shyft::energy_market::stm::srv::compute::request<M>, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::stm::srv::compute::request<M>, Char> { };

template <shyft::energy_market::stm::srv::compute::message_tag M, typename Char>
struct fmt::formatter<shyft::energy_market::stm::srv::compute::reply<M>, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::stm::srv::compute::reply<M>, Char> { };
