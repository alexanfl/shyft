#include <shyft/energy_market/stm/srv/compute/manager.h>

#include <algorithm>
#include <iterator>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

namespace shyft::energy_market::stm::srv::compute {

  dlib::logger manager::log{"compute_manager"};

  void managed_server::unassign(compute::client &client) {
    try {
      client.stop({});
      state = managed_server_state::idle;
    } catch (dlib::error const &err) {
      manager::log << dlib::LERROR << fmt::format("compute manager: unassign failed with {}", err.what());
      state = managed_server_state::dead;
    }
  }

  namespace {
    bool assign_server(managed_server &server, std::string model_id, std::shared_ptr<stm_system> const &model) {
      compute::client client{.connection{server.address}};
      try {
        server.model_id = std::move(model_id);
        client.start({.model_id = server.model_id, .model = model});
        server.last_send = utctime_now();
        server.state = managed_server_state::assigned;
        return true;
      } catch (std::exception const &err) {
        manager::log << dlib::LERROR << fmt::format("compute manager: assign_server failed with {}", err.what());
        server.state = managed_server_state::dead;
      }
      return false;
    }
  }

  void managed_server::unassign() {
    compute::client client{.connection{address}};
    unassign(client);
  }

  namespace {
    bool reset_server(managed_server &server) {
      compute::client client{.connection{server.address}};
      try {
        client.stop({});
        return true;
      } catch (std::exception const &err) {
        manager::log << dlib::LINFO << fmt::format("compute manager: reset_server failed with {}", err.what());
        return false;
      }
    }
  }

  bool manager::manage(std::string_view address) {
    std::scoped_lock lock{mutex};
    auto server = std::ranges::find(servers, address, &managed_server::address);
    if (server != std::ranges::end(servers)) {
      if ((*server)->state != managed_server_state::dead)
        return false;
      (*server)->state = managed_server_state::idle;
      reset_server(**server);
      return true;
    }
    servers.push_back(std::shared_ptr<managed_server>(new managed_server{
      .address = std::string(address),
      .state = managed_server_state::idle,
      .model_id = "",
      .watchdog = {},
      .last_send = utctime_now()}));
    reset_server(*servers.back());
    return true;
  }

  namespace {
    bool is_assigned(managed_server_state state) {
      return (state != managed_server_state::idle && state != managed_server_state::dead);
    }
  }

  std::shared_ptr<managed_server> manager::assign(std::string_view model_id, std::shared_ptr<stm_system> const &model) {
    std::scoped_lock lock{mutex};

    std::shared_ptr<managed_server> server{};
    for (auto &server_ : servers) {
      managed_server_state state = server_->state;
      if (!server && state == managed_server_state::idle)
        server = server_;
      else if (is_assigned(state) && server_->model_id == model_id)
        return server_;
    }
    if (!server || !assign_server(*server, std::string(model_id), model))
      return nullptr;
    return server;
  }

  std::shared_ptr<managed_server> manager::assigned(std::string_view model_id) {
    std::scoped_lock lock{mutex};
    auto it = std::ranges::find_if(servers, [&](auto const &server) {
      managed_server_state state = server->state;
      return is_assigned(state) && server->model_id == model_id;
    });
    if (it == std::ranges::end(servers))
      return nullptr;
    return *it;
  }

  std::vector<managed_server_status> manager::status() {
    std::scoped_lock lock{mutex};
    std::vector<managed_server_status> status;
    std::ranges::transform(servers, std::back_inserter(status), [&](auto const &server) {
      return managed_server_status{
        .address = server->address,
        .state = server->state,
        .model_id = server->model_id,
        .last_send = server->last_send};
    });
    return status;
  }

  void manager::tidy(utctime stale_duration) {
    std::scoped_lock lock{mutex};
    auto is_stale = [&](auto &server) {
      managed_server_state state = server->state;
      return state == managed_server_state::dead
          || (state != managed_server_state::idle && (utctime_now() - server->last_send) > stale_duration);
    };
    auto [stale_begin, stale_end] = std::ranges::stable_partition(servers, std::not_fn(is_stale));
    for (auto &server : std::ranges::subrange(stale_begin, stale_end))
      if (server->state != managed_server_state::dead)
        reset_server(*server);
    servers.erase(stale_begin, stale_end);
  }
}
