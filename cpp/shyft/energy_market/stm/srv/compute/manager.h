#pragma once

#include <algorithm>
#include <atomic>
#include <cstdint>
#include <deque>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>

#include <boost/asio/basic_waitable_timer.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>
#include <fmt/chrono.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/client.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::compute {

  using namespace shyft::core;

#define SHYFT_STM_COMPUTE_MANAGED_SERVER_STATE (idle, assigned, busy, dead)
  SHYFT_DEFINE_ENUM(managed_server_state, std::uint8_t, SHYFT_STM_COMPUTE_MANAGED_SERVER_STATE);

#define SHYFT_STM_COMPUTE_TASK (start, tick, done, fail, dead)
  SHYFT_DEFINE_ENUM(task_tag, std::uint8_t, SHYFT_STM_COMPUTE_TASK);

  template <task_tag H>
  struct task {
    static constexpr task_tag tag = H;

    compute::client &client;
  };

  template <>
  struct task<task_tag::tick> {
    static constexpr task_tag tag = task_tag::tick;

    compute::client &client;
    std::vector<log_entry> log{};
  };

  template <task_tag T>
  requires(T == task_tag::fail || T == task_tag::dead)
  struct task<T> {
    static constexpr task_tag tag = T;

    std::string_view what;
  };

#define SHYFT_LAMBDA(r, data, elem) using task_##elem = task<task_tag::elem>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_TASK))
#undef SHYFT_LAMBDA

  struct managed_server {

    std::string address;
    std::atomic<managed_server_state> state;
    std::string model_id;
    std::future<void> watchdog;
    utctime last_send = utctime_now();

    void unassign(compute::client &);
    void unassign();

    auto task(auto hooks) {

      if (auto expected = managed_server_state::assigned;
          !state.compare_exchange_strong(expected, managed_server_state::busy))
        return false;

      watchdog = std::async(std::launch::async, [this, hooks = std::move(hooks)] {
        using enum task_tag;
        compute::client client{.connection{address}};
        last_send = utctime_now();
        std::size_t log_index = 0;
        try {
          auto timeout = utctime_now() + hooks(task_start{.client = client});
          do {
            auto [state, log] = client.get_status({.log_index = log_index});
            log_index += log.size();
            last_send = utctime_now();
            hooks(task_tick{.client = client, .log = std::move(log)});
            if (state != compute::state::running)
              break;
            if (utctime_now() >= timeout)
              throw dlib::socket_error("timeout");
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
          } while (1);
          if (hooks(task_done{.client = client}))
            unassign(client);
          else
            state = managed_server_state::assigned;
        } catch (dlib::error const &ex) {
          hooks(task_dead{.what = std::string_view{ex.what()}});
          state = managed_server_state::dead;
        } catch (const std::exception &ex) {
          hooks(task_fail{.what = std::string_view{ex.what()}});
          unassign(client);
        }
      });
      return true;
    }
  };

  struct managed_server_status {
    std::string address;
    managed_server_state state;
    std::string model_id;
    utctime last_send;

    SHYFT_DEFINE_STRUCT(managed_server_status, (), (address, state, model_id, last_send));

    auto operator<=>(managed_server_status const &) const = default;

    SHYFT_DEFINE_SERIALIZE(managed_server_status);
  };

  /**
   * @brief manager keeps a list of available compute-servers
   */
  struct manager {
    static dlib::logger log;

    std::mutex mutex;
    std::vector<std::shared_ptr<managed_server>> servers;

    bool manage(std::string_view address);
    std::shared_ptr<managed_server> assign(std::string_view model_id, std::shared_ptr<stm_system> const &);
    std::shared_ptr<managed_server> assigned(std::string_view model_id);

    std::vector<managed_server_status> status();

    void tidy(utctime stale_duration = std::chrono::milliseconds(1800000));
  };

  template <typename Executor>
  using janitor_timer = boost::asio::basic_waitable_timer<utc_clock, boost::asio::wait_traits<utc_clock>, Executor>;
  template <typename Executor>
  using janitor_duration = typename janitor_timer<Executor>::duration;

  template <typename Executor>
  struct janitor {

    janitor_timer<Executor> timer;
    janitor_duration<Executor> duration;
    utctime stale_threshold;
    std::atomic<bool> stopped;

    void start(compute::manager &manager) {
      timer.expires_after(duration);
      timer.async_wait([&](boost::system::error_code error) {
        if (error || stopped)
          return;
        manager.tidy(stale_threshold);
        start(manager);
      });
    }

    void stop() {
      stopped = true;
      timer.cancel();
    }
  };

  struct janitor_config {
    utctime duration = std::chrono::milliseconds(10000);
    utctime stale_threshold = std::chrono::milliseconds(30000);
    SHYFT_DEFINE_STRUCT(janitor_config, (), (duration, stale_threshold));

    auto operator<=>(janitor_config const &) const = default;
  };

  template <typename Executor>
  auto make_janitor(Executor executor, janitor_config const &j_config) {
    return janitor<Executor>{
      .timer = janitor_timer<Executor>(executor),
      .duration{std::chrono::duration_cast<janitor_duration<Executor>>(j_config.duration)},
      .stale_threshold{j_config.stale_threshold},
      .stopped{false}};
  }

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::compute::managed_server_state);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::compute::task_tag);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::srv::compute::managed_server_status);
