#include <shyft/energy_market/stm/srv/compute/client.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <iterator>
#include <memory>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include <fmt/core.h>

#include <shyft/config.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/is_cyclic.h>
#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/time_series/time_axis.h>
#if defined(SHYFT_WITH_SHOP)
// FIXME: move the functionality of whether or not something is io out of shop - jeh
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#endif

namespace shyft::energy_market::stm::srv::compute {

  set_attrs_request set_plan_attrs_request(
    dtss::server &server,
    std::string_view model_id,
    stm_system const &model,
    generic_dt const &time_axis) {


    std::vector<std::string> ts_attr_urls;
    time_series::dd::ats_vector ts_attrs;

    set_attrs_request request;
    url_with_planning_inputs(model_id, model, [&]<typename T>(auto const &comp, T attr, std::string_view url) {
#if defined(SHYFT_WITH_SHOP)
      if (!shop::should_emit(time_axis, comp))
        return;
#endif
      if constexpr (std::is_same_v<T, time_series::dd::apoint_ts>) {
        ts_attr_urls.push_back(std::string(url));
        ts_attrs.push_back(attr.clone_expr());
      } else
        request.attrs.push_back(std::pair{std::string(url), any_attr{attr}});
    });

    // FIXME: add to protocol!
    {
      auto const bind_period = [&] {
        auto period = time_axis.total_period();
        return shyft::core::utcperiod(period.start - shyft::core::deltahours(24 * 7), period.end);
      }();

      auto ts_attr_result = evaluate_ts(std::bind_front(&dtss::server::do_read, &server), model, ts_attrs, bind_period);

      request.attrs.reserve(request.attrs.size() + ts_attrs.size());

      std::ranges::transform(
        std::views::zip(ts_attr_urls, ts_attr_result), std::back_inserter(request.attrs), [&](auto &&ts) {
          return std::pair{std::move(ts.first), any_attr{stm::evaluate_ts_get(std::move(ts.second))}};
        });
    }
    return request;
  }

  get_attrs_request get_plan_attrs_request(
    std::string_view model_id,
    stm_system const &model,
    [[maybe_unused]] generic_dt const &time_axis) {
    get_attrs_request request{};
    url_with_planning_outputs(model_id, model, [&]([[maybe_unused]] auto const &comp, ignore_t, std::string_view url) {
#if defined(SHYFT_WITH_SHOP)
      if (!shop::should_emit(time_axis, comp))
        return;
#endif
      request.urls.push_back(std::string(url));
    });
    return request;
  }

  shyft::core::utctime send_input_and_plan(
    dtss::server &dtss,
    std::string_view model_id,
    stm_system &model,
    client &client,
    generic_dt const &time_axis,
    std::vector<shop::shop_command> const &shop_commands) {

    auto const shop_commands_timelimit = shop::calc_suggested_timelimit(shop_commands);
    model.run_params.run_time_axis = time_axis;
    client.send(set_plan_attrs_request(dtss, model_id, model, time_axis));
    client.plan({.time_axis = time_axis, .commands = std::move(shop_commands)});
    return shop_commands_timelimit;
  }

  void get_plan_result(
    std::string_view model_id,
    shyft::core::subscription::manager *dtss_sm,
    shyft::core::subscription::manager &sm,
    stm_system &model,
    client &client,
    generic_dt const &time_axis) {
    {
      auto [summary] = client.get_plan({});
      *(model.summary) = *summary;
    }

    auto get_attrs_request = compute::get_plan_attrs_request(model_id, model, time_axis);
    auto &result_urls = get_attrs_request.urls;
    auto [result] = client.send(get_attrs_request);

    {
      // TODO: avoid copies (have set_attrs take a range), use zip in 23 - jeh
      std::vector<std::pair<std::string, any_attr>> attrs;
      attrs.reserve(result.size());
      std::ranges::copy(
        std::views::transform(
          std::views::filter(
            std::views::iota(std::size_t{0}, result_urls.size()),
            [&](auto index) {
              return result[index] != std::nullopt;
            }),
          [&](auto index) {
            return std::pair{result_urls[index], *result[index]};
          }),
        std::back_inserter(attrs));
      stm::url_set_attrs(model, attrs);
    }

    std::string model_id_(model_id);
    rebind_expression(model, model_id_, true); // FIXME: string-view - jeh
    for (auto const &g : model.unit_groups)
      g->update_sum_expressions();

    auto model_prefix = url_fmt_header(model_id);

    if (dtss_sm)
      dtss_sm->notify_change(result_urls);

    sm.notify_change(result_urls);
    for (auto const &g : model.unit_groups)
      sm.notify_change(g->all_urls(model_prefix));
    sm.notify_change(model.summary->all_urls(model_prefix));
    sm.notify_change(model.run_params.all_urls(model_prefix));
  }

}
