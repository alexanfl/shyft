/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
 * See file COPYING for more details. **/

#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/srv/run.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::stm::srv {

  using std::string;
  using std::vector;
  using std::shared_ptr;
  using shyft::core::utctime;
  using shyft::core::no_utctime;

  template <class T>
  bool vector_compare(vector<T> const & lhs, vector<T> const & rhs) {
    if (lhs.size() != rhs.size())
      return false;
    return std::is_permutation(
      lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), [](auto const & t1, auto const & t2) -> bool {
        return t1 == t2;
      });
  }

  template <class T>
  bool vector_compare(vector<shared_ptr<T>> const & lhs, vector<shared_ptr<T>> const & rhs) {
    if (lhs.size() != rhs.size())
      return false;
    return std::is_permutation(
      lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), [](auto const & t1, auto const & t2) -> bool {
        // If both are null ptr:
        if (!t1 && !t2)
          return true;
        // If one, but not the other:
        if (!t1 || !t2)
          return false;
        // If both are non-null pointers:
        return *t1 == *t2;
      });
  }

  /** @brief dstm run model_ref
   *
   * @details Keeps info about a specific stm run model,
   * and on what server to find the model (it's serviced by the dstm-service)
   * Refer to the `dstm`server for details.
   *
   */
  struct model_ref {
    string host;          ///< Host where server with model is running.
    int port_num{-1};     ///< Port-number to interface with server.
    int api_port_num{-1}; ///< Api-port-number to access model via
    string model_key;     ///< Under what model-key the model is stored on the server.
    vector<string>
      labels{}; ///< labels that describes properties of this task to ease working with flexible structures/lists

    // For python expose:
    model_ref() = default;

    model_ref(string const & host, int port_num, int api_port_num, string const & mk)
      : host{host}
      , port_num{port_num}
      , api_port_num{api_port_num}
      , model_key{mk} {
    }

    bool operator==(model_ref const & o) const noexcept {
      return host == o.host && port_num == o.port_num && model_key == o.model_key && api_port_num == o.api_port_num
          && vector_compare(labels, o.labels);
    }

    bool operator!=(model_ref const & o) const noexcept {
      return !operator==(o);
    }

    x_serialize_decl();
  };

  using model_ref_ = shared_ptr<model_ref>;

  /** @brief a case is a collection of stm model run references
   *
   * @details It is maintained through the life-time of the stm model runs, and keeps
   * those together as a logical entity. It plays a role as case for the runs
   * described in `stm_task`. Keeping logical model-runs together.
   *
   */
  struct stm_case {
    int64_t id{0};                   ///< the identifier for the case
    string name;                     ///< the name of the case
    string json;                     ///< customizable json string, to bue used for typical python scripting tasks
    utctime created{no_utctime};     ///< or modified.
    vector<string> labels{};         ///< Optional, tags to make a run more easily searchable.
    vector<model_ref_> model_refs{}; ///< Collection of model_refs making up the stm_run.

    stm_case() = default;
    stm_case(stm_case const & arun) = default;
    stm_case& operator=(stm_case const & arun) = default;

    stm_case(
      int64_t id,
      string const & name,
      utctime created,
      string json = string{},
      vector<string> labels = vector<string>{},
      vector<model_ref_> model_refs = vector<model_ref_>{})
      : id{id}
      , name{name}
      , json{json}
      , created{created}
      , labels{labels}
      , model_refs{model_refs} {
    }

    bool operator==(stm_case const & o) const noexcept {
      bool eq = id == o.id && name == o.name && json == o.json && created == o.created;
      if (!eq)
        return false;
      if (!vector_compare(labels, o.labels))
        return false;
      if (!vector_compare(model_refs, o.model_refs))
        return false;
      return true;
    }

    bool operator!=(stm_case const & o) const noexcept {
      return !operator==(o);
    }

    void add_model_ref(model_ref_ const & amr) {
      // Check for uniqueness:
      if (!amr || std::any_of(model_refs.begin(), model_refs.end(), [&amr](auto const & mr) -> bool {
            return *mr == *amr;
          })) {
        throw std::runtime_error("Run " + name + "[" + std::to_string(id) + "] already contains provided model_ref.");
      }
      model_refs.push_back(amr);
    }

    /** @brief remove a model reference from the run based on model-key
     *
     *  @param mkey: Model key of reference to remove
     *  @note: If more than one reference has the same model key it removes the first occurence
     */
    bool remove_model_ref(string const & mkey) {
      auto it = std::find_if(model_refs.begin(), model_refs.end(), [&mkey](auto const & r) -> bool {
        return r && r->model_key == mkey;
      });
      if (it != model_refs.end()) {
        model_refs.erase(it);
        return true;
      }
      return false;
    }

    /** @brief get a model reference based on model_key
     *
     * @param mkey : model key to reference you want.
     * @return model_reference. nullptr if not found
     */
    model_ref_ get_model_ref(string const & mkey) {
      auto it = std::find_if(model_refs.begin(), model_refs.end(), [&mkey](auto const & r) -> bool {
        return r && r->model_key == mkey;
      });
      return (it != model_refs.end()) ? *it : nullptr;
    }

    x_serialize_decl();
  };

  using stm_case_ = std::shared_ptr<stm_case>;

  /** @brief stm_task represents a task
   *
   * @details A task have purpose, and when created, it's initially empty, only with its identity.
   * The users (scripting or c++ app), can then fill in labels, usually used to
   * describe what kind of task this is, like 'operative', 'test', 'task-category'.
   * The `base_mdl` member keep the reference to the remote in-memory stm run model (ref. to dstm service).
   * There is a c++/python-enabled callback that is possible to attach to the server.
   * The idea is that when the user sends/calls the `fx` function, like optimize,
   * then the callback at the server-side does the needed operations to
   * make a clone of the `base_mdl`,
   * then bind/attach input time-series and data according to the tasks purpose,
   * and then finally start optimizations.
   * While doing this, the callback also can populate the 'task' with a new 'case',
   * so that the observer of the task can see what is happening, and
   * collect the results from  the runs in the 'case' as they get finished.
   *
   * This class is what keep a business task together through its lifetime,
   * and really supports the business users to get stuff done systematically and with
   * full control and visiblity.
   *
   * It could be a replanning-task, plan-day-ahead-market, plan-reserve-market,
   * or any task that involves operations on a collection of stm_system models.
   *
   *
   */
  struct stm_task {
    int64_t id{0};
    string name;
    string json;
    utctime created{no_utctime}; ///< or modified.
    vector<string> labels{};     ///< Optional, labels for sessions be more easily searchable, &c.
    vector<stm_case_> cases{};   ///< Colllection of cases associated with the model and task
    model_ref base_mdl;          ///< Reference to base model used in task
    string task_name;            ///< Name of task connected to task hmm.(hierarachy? task->subtask?).

    stm_task() = default;

    stm_task(
      int64_t id,
      string const & name,
      utctime created,
      string json = string{},
      vector<string> labels = vector<string>{},
      vector<stm_case_> runs = vector<stm_case_>{},
      model_ref base_mdl = model_ref{},
      string task_name = string())
      : id{id}
      , name{name}
      , json{json}
      , created{created}
      , labels{labels}
      , cases{runs}
      , base_mdl{base_mdl}
      , task_name{task_name} {
    }

    bool operator==(stm_task const & o) const noexcept {
      bool eq = id == o.id && name == o.name && json == o.json && created == o.created;
      if (!eq)
        return false;
      if (!vector_compare(labels, o.labels))
        return false;
      if (!vector_compare(cases, o.cases))
        return false;
      return base_mdl == o.base_mdl && task_name == o.task_name;
    }

    bool operator!=(stm_task const & o) const noexcept {
      return !operator==(o);
    }

    void add_case(stm_case_ const & arun) {
      // We want ID and name to be unique within a task:
      if (std::any_of(cases.begin(), cases.end(), [&arun](auto const & r) -> bool {
            return arun->id == r->id || arun->name == r->name;
          })) {
        throw std::runtime_error("Provided name or ID of case was not unique within task " + std::to_string(id));
      }
      cases.push_back(arun);
    }

    /** @brief remove a case based on ID
     * returns true if the case with the given id was removed.
     * return false if no such case could be found.*/
    bool remove_case(int64_t aid) {
      // See if we can find run with given ID:
      auto it = std::find_if(cases.begin(), cases.end(), [aid](auto const & r) -> bool {
        return r->id == aid;
      });
      if (it != cases.end()) {
        cases.erase(it);
        return true;
      }
      return false;
    }

    /** @brief remove a case based on name */
    bool remove_case(string const & aname) {
      auto it = std::find_if(cases.begin(), cases.end(), [&aname](auto const & r) -> bool {
        return r->name == aname;
      });
      if (it != cases.end()) {
        cases.erase(it);
        return true;
      }
      return false;
    }

    /** @brief get case from task based on id
     *
     * @param id: run id of run to get
     * @return shared pointer to requested run. nullptr if not found
     */
    stm_case_ get_case(int64_t aid) {
      auto it = std::find_if(cases.begin(), cases.end(), [aid](auto const & r) -> bool {
        return r->id == aid;
      });
      return (it != cases.end()) ? *it : nullptr;
    }

    /** @brief get case from task based on name
     *
     * @param rname: name of case to get
     * @return shared pointer to requested run. nullptr if not found
     */
    stm_case_ get_case(string const & rname) {
      auto it = std::find_if(cases.begin(), cases.end(), [&rname](auto const & r) -> bool {
        return r->name == rname;
      });
      return (it != cases.end()) ? *it : nullptr;
    }

    /** @brief Update case inplace
     *
     * Only allow meta-info update, keep case id and model_ref immutable.
     *
     * @param arun:: stm_case to update inplace
     * @return Bool success or failed
     */
    bool update_case(stm_case const & arun) {
      auto arun_ptr = get_case(arun.id);
      if (arun_ptr != get_case(arun.id)) {
        return false;
      }
      arun_ptr->name = arun.name;
      arun_ptr->json = arun.json;
      arun_ptr->created = arun.created;
      arun_ptr->labels = arun.labels;
      return true;
    }

    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::energy_market::stm::srv::model_ref);
x_serialize_export_key(shyft::energy_market::stm::srv::stm_case);
x_serialize_export_key(shyft::energy_market::stm::srv::stm_task);
