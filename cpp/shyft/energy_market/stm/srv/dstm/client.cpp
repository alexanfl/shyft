#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>

#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/boost_serialization_std_variant.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time_series/expression_serialization.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::core::core_iarchive;
  using shyft::core::core_oarchive;
  using shyft::core::core_arch_flags;
  using shyft::core::scoped_connect;

  client::client(string host_port, int timeout_ms)
    : c{host_port, timeout_ms} {
  }

  string client::get_version_info() {
    scoped_connect sc(c);
    string r{};
    do_io_with_repair_and_retry(c, [&r](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::VERSION_INFO, io);
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::VERSION_INFO) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::create_model(string const & mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c, [&r, &mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::CREATE_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::CREATE_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::add_model(string const & mid, stm_system_ mdl) {
    scoped_connect sc(c);
    bool r{false};
    stm_system::fix_uplinks(mdl);// ensure we have nice models with uplinks ok!
    do_io_with_repair_and_retry(c, [&r, &mid, &mdl](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::ADD_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << mdl;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::ADD_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::remove_model(string const & mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c, [&r, &mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::REMOVE_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::REMOVE_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::rename_model(string const & old_mid, string const & new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c, [&r, &old_mid, &new_mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::RENAME_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << old_mid << new_mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::RENAME_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::clone_model(string const & old_mid, string const & new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c, [&r, &old_mid, &new_mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::CLONE_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << old_mid << new_mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::CLONE_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  vector<string> client::get_model_ids() {
    scoped_connect sc(c);
    vector<string> r;
    do_io_with_repair_and_retry(c, [&r](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_MODEL_IDS, io);
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_MODEL_IDS) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  std::map<std::string, model_info, std::less<>> client::get_model_infos() {
    scoped_connect sc(c);
    std::map<std::string, model_info, std::less<>> r;
    do_io_with_repair_and_retry(c, [&r](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_MODEL_INFOS, io);
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_MODEL_INFOS) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  stm_system_ client::get_model(string const & mid) {
    scoped_connect sc(c);
    stm_system_ r;
    do_io_with_repair_and_retry(c, [&r, &mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
        stm_system::fix_uplinks(r);// ensure we fix it (it could have been create server side)
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::optimize(
    string const & mid,
    generic_dt const & ta,
    vector<shyft::energy_market::stm::shop::shop_command> const & cmd,
    bool opt_only) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&r, &mid, &ta, &cmd, opt_only](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::OPTIMIZE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << ta << cmd << opt_only;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::OPTIMIZE) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::start_tune(std::string const & mid) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::START_TUNE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::START_TUNE) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::tune(
    std::string const & mid,
    generic_dt const & time_axis,
    std::vector<shop::shop_command> const & shop_commands) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::TUNE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << time_axis << shop_commands;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::TUNE) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::stop_tune(std::string const & mid) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::STOP_TUNE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::STOP_TUNE) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  /**
   * @brief get SHOP log for model.
   */
  vector<log_entry> client::get_log(string const & mid) {
    scoped_connect sc(c);
    vector<log_entry> r;
    do_io_with_repair_and_retry(c, [&r, &mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_LOG, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_LOG) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  /**
   * @brief get state of a model:
   */
  model_state client::get_state(string const & mid) {
    scoped_connect sc(c);
    model_state r;
    do_io_with_repair_and_retry(c, [&r, &mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_STATE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_STATE) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  optimization_summary_ client::get_optimization_summary(string const & mid) {
    scoped_connect sc(c);
    optimization_summary_ r;
    do_io_with_repair_and_retry(c, [&r, &mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_OPTIMIZATION_SUMMARY, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_OPTIMIZATION_SUMMARY) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });

    return r;
  }

  void client::set_state(string const & mid, model_state x) {
    scoped_connect sc(c);
    do_io_with_repair_and_retry(c, [&mid, x](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::SET_STATE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << x;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::SET_STATE) {
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
  }

  /** @brief exeute fx(mid,fx_arg) on the server side
   */
  bool client::fx(string const & mid, string const & fx_arg) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&r, &mid, &fx_arg](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::FX, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << fx_arg;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::FX) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  /** @brief evaluate any unbound time series attributes in a model.
   */
  bool client::evaluate_model(
    string const & mid,
    utcperiod bind_period,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(
      c, [&r, &mid, &bind_period, use_ts_cached_read, update_ts_cache, &clip_period](srv_connection& c) {
        auto& io = *c.io;
        msg::write_type(message_type::EVALUATE_MODEL, io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << bind_period << use_ts_cached_read << update_ts_cache << clip_period;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
          auto re = msg::read_exception(io);
          throw re;
        } else if (response_type == message_type::EVALUATE_MODEL) {
          core_iarchive ia(io, core_arch_flags);
          ia >> r;
        } else {
          throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
        }
      });
    return r;
  }

  std::vector<std::variant<time_series::dd::apoint_ts, evaluate_ts_error>> client::evaluate_ts(
    time_series::dd::ats_vector const & tsv,
    utcperiod bind_period,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period) {
    if (!bind_period.valid())
      throw std::runtime_error("evaluate requires a valid period-specification");
    std::vector<std::variant<time_series::dd::apoint_ts, evaluate_ts_error>> result;

    if (tsv.empty())
      return result;
    do_io_with_repair_and_retry(c, [&](srv_connection& sc) {
      auto& io = *sc.io;
      msg::write_type(message_type::EVALUATE_TS, io);
      core_oarchive oa(io, core_arch_flags);
      oa << time_series::dd::expression_compressor::compress(tsv) << bind_period << use_ts_cached_read
         << update_ts_cache << clip_period;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::EVALUATE_TS) {
        core_iarchive ia(io, core_arch_flags);
        ia >> result;
      } else if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", static_cast<int>(response_type)));
      }
    });
    return result;
  }

  ats_vector client::get_ts(string const & mid, vector<string> const & ts_urls) {
    scoped_connect sc(c);
    ats_vector r;
    do_io_with_repair_and_retry(c, [&r, &mid, &ts_urls](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_TS, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << ts_urls;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_TS) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  void client::set_ts(string const & mid, ats_vector const & tsv) {
    scoped_connect sc(c);
    do_io_with_repair_and_retry(c, [&mid, &tsv](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::SET_TS, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << tsv;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::SET_TS) {
        core_iarchive ia(io, core_arch_flags);
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
  }

  bool client::add_compute_server(std::string const & address) {
    scoped_connect sc(c);
    bool r = false;
    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::ADD_COMPUTE_NODE, io);
      core_oarchive oa(io, core_arch_flags);
      oa << address;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::ADD_COMPUTE_NODE) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  /** @brief close, until needed again, the server connection
   *
   */
  void client::close() {
    c.close(); // just close-down connection, it will auto-open if needed
  }

  bool client::kill_optimization(string const & mid) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&r, mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::KILL_OPTIMIZATION, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::KILL_OPTIMIZATION) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  std::vector<std::optional<stm::url_resolve_error>>
    client::set_attrs(std::vector<std::pair<std::string, any_attr>> const & attrs) {
    scoped_connect sc(c);
    std::vector<std::optional<stm::url_resolve_error>> r;
    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::SET_ATTRS, io);
      core_oarchive oa(io, core_arch_flags);
      oa << attrs;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::SET_ATTRS) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  std::vector<std::variant<any_attr, stm::url_resolve_error>>
    client::get_attrs(std::vector<std::string> const & attr_urls) {
    scoped_connect sc(c);
    std::vector<std::variant<any_attr, stm::url_resolve_error>> attrs;

    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::GET_ATTRS, io);
      core_oarchive oa(io, core_arch_flags);
      oa << attr_urls;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::GET_ATTRS) {
        core_iarchive ia(io, core_arch_flags);
        ia >> attrs;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });

    return attrs;
  }

  bool client::reset_model(string const & mid) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [&r, mid](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::RESET_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::RESET_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  bool client::patch(string const & mid, stm_patch_op op, stm_system_ const & mdl) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c, [&r, &mid, op, &mdl](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::PATCH_MODEL, io);
      core_oarchive oa(io, core_arch_flags);
      oa << mid << op << mdl;
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::PATCH_MODEL) {
        core_iarchive ia(io, core_arch_flags);
        ia >> r;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return r;
  }

  std::vector<compute::managed_server_status> client::compute_server_status() {
    scoped_connect sc(c);
    std::vector<compute::managed_server_status> status;
    do_io_with_repair_and_retry(c, [&](srv_connection& c) {
      auto& io = *c.io;
      msg::write_type(message_type::COMPUTE_NODE_INFO, io);
      auto response_type = msg::read_type(io);
      if (response_type == message_type::SERVER_EXCEPTION) {
        auto re = msg::read_exception(io);
        throw re;
      } else if (response_type == message_type::COMPUTE_NODE_INFO) {
        core_iarchive ia(io, core_arch_flags);
        ia >> status;
      } else {
        throw std::runtime_error(fmt::format("Got unexpected response: {}", (int) response_type));
      }
    });
    return status;
  }

}
