#pragma once

#include <string>
#include <cstdint>
#include <exception>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/fast_iosockstream.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::core::srv_connection;
  using shyft::srv::model_info;
  using shyft::time_series::dd::ats_vector;

  /**
   * @brief a client
   *
   *
   * This class take care of message exchange to the remote server,
   * using the supplied connection parameters.
   *
   * It implements the message protocol of the server, sending
   * message-prefix, arguments, waiting for response
   * deserialize the response and handle it back to the user.
   *
   * @see server
   *
   */
  struct client {
    srv_connection c;

    client(std::string host_port, int timeout_ms = 1000);

    std::string get_version_info();

    bool create_model(std::string const &mid);
    bool add_model(std::string const &mid, std::shared_ptr<stm_system> mdl);
    bool remove_model(std::string const &mid);
    bool rename_model(std::string const &old_mid, std::string const &new_mid);
    bool clone_model(std::string const &old_mid, std::string const &new_mid);
    std::vector<std::string> get_model_ids();
    std::map<std::string, model_info, std::less<>> get_model_infos();
    std::shared_ptr<stm_system> get_model(std::string const &mid);

    bool optimize(
      std::string const &mid,
      generic_dt const &ta,
      std::vector<shop::shop_command> const &cmd,
      bool opt_only = false);

    bool start_tune(std::string const &mid);
    bool tune(std::string const &mid, generic_dt const &, std::vector<shop::shop_command> const &);
    bool stop_tune(std::string const &mid);

    /** @brief get SHOP log for model.
     */
    std::vector<log_entry> get_log(std::string const &mid);

    std::shared_ptr<optimization_summary> get_optimization_summary(std::string const &mid);

    /** @brief get state of a model:
     */
    model_state get_state(std::string const &mid);

    /** @brief set state of a model
     *
     * @details it is not possible to set state running
     */
    void set_state(std::string const &mid, model_state x);

    /** @brief exeute fx(mid,fx_arg) on the server side
     */
    bool fx(std::string const &mid, std::string const &fx_arg);

    /** @brief evaluate some time-series expressions
     */
    std::vector<std::variant<time_series::dd::apoint_ts, evaluate_ts_error>> evaluate_ts(
      time_series::dd::ats_vector const &tsv,
      utcperiod bind_period,
      bool use_ts_cached_read = true,
      bool update_ts_cache = true,
      utcperiod clip_result = utcperiod{});

    /** @brief evaluate any unbound time series attributes in a model.
     */
    bool evaluate_model(
      std::string const &mid,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period = utcperiod{});

    /** @brief close, until needed again, the server connection
     *
     */
    void close();

    /** @brief send kill signal to server to kill an ongoing optimization of a model (if any)
     */
    bool kill_optimization(std::string const &mid);

    /**
     * @brief get_ts from model
     * @details
     * Given model id, and  ts-urls, resolve and return ts_vector
     * Used from python/c++ to read time-series attached to the model.
     * Similar to the web-api, using same underlying mechanisms.
     * @param mid the model id,the ts_urls should all refer to this model id
     * @param ts_urls, list of dstm://Mmid/path..
     * @return resolved list of time-series, 1 to 1 as for the request
     */
    ats_vector get_ts(std::string const &mid, std::vector<std::string> const &ts_urls);

    /**
     * @brief set_ts into the model
     * @details
     * Given model id, and  ts-urls, resolve and set the supplied time-series
     * accoring to the supplied ts-urls. They should all refer to mid.
     * Used from python/c++ to set time-series attached to the model.
     * Similar to the web-api, using same underlying mechanisms,
     * including notify change to subscribers
     * @param mid the model id,the ts_urls should all refer to this model id
     * @param ts_urls, list of dstm://Mmid/path..
     *
     */
    void set_ts(std::string const &mid, ats_vector const &tsv);

    /**
     * @brief add compute node by host_port
     * @details
     * add the specified host:port to the server so that optimizations can be run
     * on the specified node. If the host:port is already added this is a no-op.
     * Note that if the host port in question is marked for removal(pending remove),
     * this mark is cleared(so that it's not removed).
     *
     * @param host_port formatted as a validhost:port address
     */
    bool add_compute_server(std::string const &address);

    /**
     * @brief set_attrs on model
     * @param attrs
     * @return
     * A vector of optional stm::url_resolve_error, empty error indicating succesfull set.
     */
    std::vector<std::optional<stm::url_resolve_error>>
      set_attrs(std::vector<std::pair<std::string, any_attr>> const &attrs);
    /**
     * @brief get_attrs on model
     * @param attr_urls
     * @return
     * Returns the resolved attributes or a stm::url_resolve_error if resolution failed.
     */
    std::vector<std::variant<any_attr, stm::url_resolve_error>> get_attrs(std::vector<std::string> const &attr_urls);

    /** @brief reset model
     */
    bool reset_model(std::string const &mid);

    /**
     * @brief patch model
     * @param mind the model id
     * @param op the patch operation, add, remove ,remove relation
     * @param p the patch to apply
     */
    bool patch(std::string const &mid, stm_patch_op op, stm_system_ const &p);

    std::vector<compute::managed_server_status> compute_server_status();
  };

}
