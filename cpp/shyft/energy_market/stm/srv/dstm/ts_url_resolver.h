#pragma once
/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <string>
#include <functional>

#include <boost/asio/thread_pool.hpp>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/stm/url_parse.h>
#include <shyft/energy_market/stm/context.h>

namespace shyft::energy_market::stm {
  struct stm_system; // fwd

  namespace srv::dstm {
    using shyft::time_series::dd::apoint_ts;

    /**
     * @brief Provides resolving url to attributes time_series on the dstm model
     * @details
     * This class is used in the dstm server in to resolve/find time_series
     * within the stm system model, based on specified url.
     * The dstm_ts_url_grammar requires a resolver function as input,
     * and this particular class is used within the
     * dstm server to help the dtss getting access to the stm model time_series.
     * This allow us to use expressions that involves attribute-references on the
     * model.
     */
    struct ts_url_resolver {
      stm::shared_models<boost::asio::thread_pool::executor_type>& models;

      apoint_ts operator()(std::string_view mid, std::vector<c_id> const & cpth, std::string_view attr_id) const;
    };

    /** @brief Provides enforcing scoped read-only ts_url_resolver
     * @details
     */
    struct scoped_ts_url_resolver {
      stm_system const * mdl{nullptr}; ///< reference to the resolved/locked stm-system for the specified mid
      std::string mid;                 ///< the model id, as, in dstm://Mmymodel, matching the mdl above

      scoped_ts_url_resolver(stm_system const * mdl, std::string_view mid)
        : mdl{mdl}
        , mid{mid} {
      }

      apoint_ts operator()(std::string_view mid, std::vector<c_id> const & cpth, std::string_view attr_id) const;
    };

    /**
     * @brief Provides enforcing scoped read-write ts_url_resolver for setting time-series attributes
     * @details
     *  This resolver figure out the location of the attribute,
     *  then assigneds the found target with the supplied ts value.
     */
    struct scoped_ts_url_resolver_assign_fx {
      std::function<void(apoint_ts& lhs)> assign_fx; ///< reference to the assign_fx.
      stm_system const * mdl{nullptr}; ///< reference to the resolved/locked stm-system for the specified mid
      std::string mid;                 ///< the model id, as, in dstm://Mmymodel, matching the mdl above
      apoint_ts operator()(std::string_view mid, std::vector<c_id> const & cpth, std::string_view attr_id) const;
    };

    /**
     * @brief Provides enforcing scoped read-write ts_url_resolver for setting time-series attributes
     * @details
     *  This resolver figure out the location of the attribute,
     *  then assigneds the found target with the supplied ts value.
     */
    struct scoped_ts_url_resolver_assign {
      stm_system const * mdl{nullptr}; ///< reference to the resolved/locked stm-system for the specified mid
      std::string mid;                 ///< the model id, as, in dstm://Mmymodel, matching the mdl above
      apoint_ts v;                     ///< the time-series to assign to the resolved attribute
      apoint_ts operator()(std::string_view mid, std::vector<c_id> const & cpth, std::string_view attr_id) const;
    };
  }
}
