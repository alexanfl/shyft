#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <vector>
#include <string>

namespace shyft::energy_market::stm {

  struct stm_system; // fwd:

  namespace srv::dstm {

    /** @brief Generate ts-result urls for a given stm_system
     *
     * @details
     * Given a stm_system, generate and return a complete vector with all the time-series urls
     * that are considered result, or might contain result, after running an algorithmic process.
     * The primar usage is between the master/slave dstm, but it is also exposed to python,
     * so that it can be utilized in scripting as well.
     * @param prefix like dstm://M<mid>
     * @return a complete list of all ts-urls, like dstm://M<mid>/H1/R1.volume.result etc.
     */
    extern std::vector<std::string> ts_url_generator(std::string const & prefix, stm_system const & s);
  }

}
