#pragma once
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

namespace shyft::energy_market::stm::srv::dstm {
  using time_series::dd::gpoint_ts;
  using time_series::dd::aref_ts;
  using time_series::dd::apoint_ts;
  using time_series::dd::ats_vector;

  enum struct ts_merge_result {
    merged,          // simply merged to local value
    saved_to_dtss,   // the lhs was just shyft:// ref, so forwarded to the dtss(and saved there)
    fail_expression, // not allowed to assign to expressions
    fail_dtss,       // dtss not avalilable
  };

  /**
   * @brief do a sematic set/merge values on a ts
   * @details
   * Given a apoint_ts old_val and a new_val that we would like to 'assign'.
   *
   * Do the needed checks and semantics according to the supplied policy arguments.
   * This could involve a write operation to a related dtss, if the old_val is an unbound ref.series.
   * In that case the values are reflected back to the dtss, as if writing to the dtss.
   * If its a ref ts, but bound, we modify it, as if it was just a plain memory ts. e.g. no dtss/store operation
   * involved. (thus evaluating the model, means, it's frozen)
   *
   * The LHS side determines the semantics in this case, so its unfortunately a
   * bit magic assign.
   *
   * @tparam C a dtss like ptr type
   * @param dtss ref. to the dtss handler, used to fwd. assignments of unbound shyft:// urls.
   * @param old_val The ref to the old/current time-series, typically an attribute of an object
   * @param new_val The const ref to a new value, to be assign to the old_val
   * @param merge Controls if the values supplied should be 'merged' into the old one.
   * @param recreate Controls if the old_val should simply be replaced with the values from new one (as in wipe the old,
   * set the new ones)
   * @param cache_on_write Controls if dtss related ts, then also cache on write (usually a good thing to do)
   * @returns the outcome of the operation.
   */
  template <class C>
  ts_merge_result ts_magic_merge_values(
    C const & dtss,
    apoint_ts& old_val,
    apoint_ts const & new_val,
    bool merge,
    bool recreate,
    bool cache_on_write) {

    auto simple_assign = [&]() { // common fx for simple ts, or bound ts.
      if (!recreate || merge) {
        old_val.merge_points(new_val);
      } else
        old_val = new_val;
      return ts_merge_result::merged;
    };

    if (
      old_val.ts == nullptr || ts_as<gpoint_ts>(old_val.ts)) // Case 1: A plain concret ts, treated as a local variable
      return simple_assign();

    if (auto sts = ts_as<aref_ts>(old_val.ts)) {                      // Case 2: Reference to a time series
      if (sts->needs_bind() && !(sts->id.rfind("dstm://", 0) == 0)) { // just a shyft:// ref..??
        if (!dtss)
          return ts_merge_result::fail_dtss; // not likely, nevertheless.
        // then forward the assigment to the dtss (could be a remote), .. store it
        ats_vector tsv;
        tsv.push_back(apoint_ts(old_val.id(), new_val));
        if (merge)
          dtss->do_merge_store_ts(tsv, cache_on_write);
        else
          dtss->do_store_ts(tsv, recreate, cache_on_write);
        return ts_merge_result::saved_to_dtss;
      } else // its a bound shyft ts, or a dstm series, in either cases we merge the values, it's a local change, no
             // dtss updates(!)
        return simple_assign();
    }
    // Case 3: The time series is an expression and should not be set
    // because: given a model, with alive subs, this will hit/invalidate those subs(inconsistent).
    // so those usepatterns we do not support.
    // expressions, as of now should be created before model is pushed to dstm
    // then, keept constant, to ensure that if any subs, they remains valid/consistent.
    return ts_merge_result::fail_expression;
  }

}
