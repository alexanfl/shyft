#pragma once
#include <string>
#include <dlib/logger.h>

namespace shyft::energy_market::stm::srv::dstm {
  using std::string;
  using std::ofstream;

  /**
   * @brief Configuration class for Dlib logger:
   */
  class server_log_hook {
   public:
    string log_file;                            ///< name of file to log to
    dlib::log_level current_level{dlib::LWARN}; ///< only log to file for these levels
    server_log_hook();
    server_log_hook(string const & log_file);
    server_log_hook(server_log_hook const &) = delete;
    server_log_hook(server_log_hook&&) = delete;
    server_log_hook& operator=(server_log_hook const &) = delete;
    server_log_hook& operator=(server_log_hook&&) = delete;

    static void configure(string const & fname, const dlib::log_level ll);
  };

  extern void configure_logger(server_log_hook& hook, dlib::log_level const & ll);
}
