#pragma once

#include <algorithm>
#include <cstdint>
#include <exception>
#include <functional>
#include <future>
#include <memory>
#include <string>

#include <boost/asio/thread_pool.hpp>
#include <dlib/logger.h>

#include <shyft/core/fs_compat.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::core::utcperiod;
  using shyft::core::utctime;
  using shyft::core::utctime_now;
  using subscription_manager = shyft::core::subscription::manager;

  using ts_server = shyft::dtss::server;
  using shyft::dtss::ts_vector_t;
  using shyft::dtss::id_vector_t;

  using shyft::time_series::dd::ats_vector;

  using server_iostream_t = shyft::srv::fast_server_iostream;
  using shyft::srv::model_info;

  /// the type of callback provided, 1st arg model-id, 2nd arg fx-verb
  using fx_call_back_t = std::function<bool(std::string, std::string)>;

  /**
   * @brief an in memory reactive stm model server
   * @details
   * The server provides dual interfaces, web-api and raw socket for py.
   * It keeps a list of named stm systems, descriptive energy market models, that can be
   * used/manipulated communicated with over the above mentioned interfaces.
   * The web api provides requests with subscription(notified when changed),
   * as well as setting set of attributes.
   * The py api provides functions for uploading/cloning/ new stm system models.
   * The server also provides scalable compute node capability, that harvest
   * the input from the descriptive, then sends the stm system and its input to
   * a compute node that when finished, provides results back to the
   * descriptive stm system model (decorated when finished).
   */
  struct server : server_iostream_t {

    using execution_context_type = boost::asio::thread_pool;
    using executor_type = execution_context_type::executor_type;
    using shared_model_type = stm::shared_model<executor_type>;


    server(compute::janitor_config const &j_config = {});
    ~server();

    fx_call_back_t fx_cb; ///< user specified callback that can be invoked using web-api fx(..) or python c.fx(..)
                          ///< typically run model

    execution_context_type execution_context;
    stm::shared_models<executor_type> models;

    static dlib::logger slog; ///< server-side logger

    std::unique_ptr<ts_server> dtss; ///< Server for storing and handling time series and expressions related to models.
    /// the subscription_manager so that web-api can support observable expression-vectors
    std::shared_ptr<subscription_manager> sm = std::make_shared< subscription_manager>();
    utctime shared_lock_timeout{
      shyft::core::from_seconds(0.2)}; ///< max time to wait for a shared lock on an stm_model while doing evaluate.

    compute::manager compute_manager;
    compute::janitor<executor_type> compute_janitor;

    /** @brief start the server in background, return the listening port used in case it was set unspecified */
    int start_server();

    /** @brief set up the dtss for the server */
    void setup_dtss();

    /** @brief When reading time series using the DTss, it will search in its own containers if the ts_url starts with
     * shyft://. nFor ts_url's of the type dstm://model_key/hps_id/(R|W|U|P|...)component_id/attribute_id we use the
     * following callbak function to resolve to time series.
     */
    ts_vector_t dtss_read_callback(id_vector_t const &ts_ids, utcperiod p);

    /**
     * @brief Set the dtss to master/slave mode
     * @details This means that the embedded dtss server redirects all IO to a master dtss
     * getting the real ts from the master
     */
    void set_master(
      std::string ip,
      int port,
      double master_poll_time,
      std::size_t unsubscribe_min_threshold,
      double unsubscribe_max_delay);

    /** @brief add a container to the dtss */
    void add_container(std::string const &container_name, std::string const &root_dir);

    /** @brief get current api version */
    string do_get_version_info();

    /** @brief create a new model with id */
    bool do_create_model(std::string const &mid);

    /** @brief add existing model with id */
    bool do_add_model(std::string const &mid, stm_system_ mdl);

    /** @brief remove (free up mem etc) model by id */
    bool do_remove_model(std::string const &mid);

    /** @brief rename a model by id */
    bool do_rename_model(std::string old_mid, std::string new_mid);

    /** @brief clone existing model with id */
    bool do_clone_model(std::string const &old_mid, std::string new_mid);

    /** @brief get models, returns a string list with model identifiers */
    std::vector<std::string> do_get_model_ids();

    /** @brief get model infos. Returns a mapping from model identifiers to model_infos */
    std::map<std::string, model_info, std::less<>> do_get_model_infos();

    model_state do_get_state(std::string const &mid);

    bool is_optimization_running(std::string const &mid);

    /**
     * @brief copy result time-series(etc) from src to dst model
     * @details
     * Copy as if shop_api collect,
     * so same members, and possibly notifications.
     * Access to the dst model should be ensured
     * by the caller using context mutex etc..
     */
    void copy_results(stm_system_ const &src, stm_system_ const &dst);

    /** @brief Get log for a model
     */
    std::vector<log_entry> do_get_log(std::string const &mid);
    /** @brief start optimization on a model
     */
    bool
      do_optimize(std::string const &mid, generic_dt const &, std::vector<shop::shop_command> const &, bool opt_only);
    /** @brief start tuning, tune and stop tuning a model
     */
    bool do_start_tune(std::string_view model_id);
    bool do_tune(std::string_view model_id, generic_dt const &, std::vector<shop::shop_command> const &);
    bool do_stop_tune(std::string_view model_id);

    /** @brief optimization summary for stm system
     */
    optimization_summary_ do_get_optimization_summary(std::string const &mid);

    std::vector<std::variant<time_series::dd::apoint_ts, evaluate_ts_error>> do_evaluate_ts(
      time_series::dd::ats_vector &tsv,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period);

    /** @brief Evaluate any unbound time series of a model.
     * NOTE: This should only be called for clones of models stored on server.
     * Otherwise, the model will not need binding, which may lead to subsequent
     * requests to read attributes not yielding up-to-date values.
     */
    bool do_evaluate_model(
      std::string const &mid,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period = utcperiod{});
    bool evaluate_stm_system(
      stm_system const &mdl,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period = utcperiod{});

    static bool stm_system_needs_bind(stm_system const &mdl);

    /**
     * @brief get_ts from model
     * @details
     * Given model id, and  ts-urls, resolve and return ts_vector
     * Used from python/c++ to read time-series attached to the model.
     * Similar to the web-api, using same underlying mechanisms.
     * @param mid the model id,the ts_urls should all refer to this model id
     * @param ts_urls, list of dstm://Mmid/path..
     * @return resolved list of time-series, 1 to 1 as for the request
     */
    ats_vector do_get_ts(std::string_view mid, std::vector<std::string> const &ts_urls);

    /** @brief set annotated time-series on the model
     *
     * @details
     * The `tsv` parameter should contain apoint_ts with, id and payload(the values).
     * It attaches/replaces the values to the attribute that is addressed.
     * Ref to do_set_ts for details.
     *
     * @param mid the model context identifier(all urls should address this)
     * @param tsv the time-series, with id, ts-urls, like dstm://Mmid
     * @param merge_assign keep existing ts-value, and apply the new ts-value fragment on the existing
     * @param rebind when done setting values, unbind, and re-evaluate the dstm model so that expressions derived
     * reflects the new values.
     */
    void do_set_ts(string const &mid, ats_vector const &tsv, bool merge_assign = false, bool rebind = true);

    /** @brief set kill flag model optimization */
    void set_kill_flag(std::string const &mid);

    /** @brief check if model optimization is to be killed */
    bool has_kill_flag(std::string const &mid);

    /** @brief clear kill flag for model optimization */
    void clear_kill_flag(std::string const &mid);

    /** @brief kill self by raising sigterm */
    void raise_sigterm();

    /**@brief do fx(arg1,arg2)
     *
     * if the fx_cb exist, call it
     */
    bool do_fx(std::string mid, std::string action);
    bool do_add_compute_server(std::string host_port);

    std::vector<std::optional<stm::url_resolve_error>>
      do_set_attrs(std::vector<std::pair<std::string, any_attr>> const &);
    std::vector<std::variant<any_attr, stm::url_resolve_error>> do_get_attrs(std::vector<std::string> const &);

    bool do_reset_model(std::string const &mid);

    /**
     * @brief patch a specified model with patch p, using operation 'op'
     * @details
     * The target model represented by `mid` is patched using operation `op` with the stm_model p.
     * The operation can be one of:
     *  + : new objects missing by id in the mid-model are added, relations in the patch are established/ensured.
     *  - : relations between objects mentioned in the patch model are removed from the mid model
     *  * : delete leaf-objects (with relations) in the patch model are removed from the mid model
     * @param mid the model id to patch
     * @param op  the patch operation to do, +:additive, - remove relations, * remove leaf objects(with relations to
     * them)
     * @param p the patch containing information needed to do patching.
     * @returns true if model mid found and patch applied.
     */
    bool do_patch_model(std::string mid, stm_patch_op op, stm_system_ p);

    /** @brief handle one client connection
     *
     * Reads messages/requests from the clients,
     * - act and perform request,
     * - return response
     * for as long as the client keep the connection
     * open.
     *
     */
    void on_connect(
      std::istream &in,
      std::ostream &out,
      std::string const & /*foreign_ip*/,
      std::string const & /*local_ip*/,
      unsigned short /*foreign_port*/,
      unsigned short /*local_port*/,
      dlib::uint64 /*connection_id*/
      ) override;
  };

}
