#include <shyft/energy_market/stm/srv/dstm/server_logger.h>
#include <shyft/core/dlib_utils.h>

namespace shyft::energy_market::stm::srv::dstm {

  using namespace std;
  using namespace shyft::core;

  server_log_hook::server_log_hook() {
  }

  server_log_hook::server_log_hook(string const & fname)
    : log_file{fname} {
  }

  void server_log_hook::configure(std::string const & fname, const dlib::log_level ll) {
    logging::output_file(fname);
    logging::level(ll);
  }

  void configure_logger(server_log_hook& hook, dlib::log_level const & ll) {
    server_log_hook::configure(hook.log_file, ll);
  }

}
