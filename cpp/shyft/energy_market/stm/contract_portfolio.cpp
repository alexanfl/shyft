#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  using std::static_pointer_cast;
  using std::dynamic_pointer_cast;
  using std::make_shared;
  using std::runtime_error;

  void contract_portfolio::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels)
    const {
    if (levels) {
      auto tmp = sys_();
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/p{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto a = "/p" + std::to_string(id);
      std::copy(std::begin(a), std::end(a), rbi);
    }
  }

  bool contract_portfolio::operator==(contract_portfolio const & o) const {
    if (this == &o)
      return true; // equal by addr.
    // check contracts..:
    auto c_equal = equal_vector_ptr_content<contract>(contracts, o.contracts);
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
      mp::leaf_accessors(hana::type_c<contract_portfolio>),
      c_equal && super::operator==(o), // initial value of the fold
      [this, &o](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a))
                 : false; // only evaluate equal if the fold state is still true
      });
  }

}
