#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  bool wind_farm::operator==(wind_farm const & other) const {
    if (this == &other) {
      return true; // equal by addr.
    }
    if (super::operator!=(other)) {
      return false; // neq by base class
    }
    auto equal_attributes = hana::fold(
      mp::leaf_accessors(hana::type_c<wind_farm>),
      id_base::operator==(other), // initial value of the fold
      [this, &other](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                 : false; // only evaluate equal if the fold state is still true
      });
    return equal_attributes;
  }

  void wind_farm::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_system>(sys_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    // W for Wind farm
    if (!template_levels) {
      constexpr std::string_view a = "/W{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/W" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }
}