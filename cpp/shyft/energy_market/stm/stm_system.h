#pragma once

#include <string>
#include <memory>
#include <vector>

#include <fmt/core.h>

#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/unit_group_type.h>

namespace shyft::energy_market::stm {
  using std::shared_ptr;
  using std::weak_ptr;
  using std::vector;
  using std::string;
  using std::logic_error;
  using shyft::core::utctime;
  using shyft::core::utctimespan;
  using shyft::core::utcperiod;
  using shyft::time_axis::generic_dt;
  using std::static_pointer_cast;

  // --
  // fwd enough of the sub-components so that the top level is well defined.
  // this file should be possible to include from the sub-components,
  // so we have to forward decl. types here,
  struct energy_market_area;
  using energy_market_area_ = shared_ptr<energy_market_area>;
  struct contract;
  using contract_ = shared_ptr<contract>;
  struct contract_portfolio;
  using contract_portfolio_ = shared_ptr<contract_portfolio>;
  struct network;
  using network_ = shared_ptr<network>;
  struct transmission_line;
  using transmission_line_ = shared_ptr<transmission_line>;
  struct busbar;
  using busbar_ = shared_ptr<busbar>;
  struct power_module;
  using power_module_ = shared_ptr<power_module>;
  struct reservoir;
  using reservoir_ = shared_ptr<reservoir>;
  struct unit;
  using unit_ = shared_ptr<unit>;
  struct unit_group;
  using unit_group_ = shared_ptr<unit_group>;
  struct unit_group_member;
  struct waterway;
  using waterway_ = shared_ptr<waterway>;
  struct gate;
  using gate_ = shared_ptr<gate>;
  struct catchment;
  using catchment_ = shared_ptr<catchment>;
  struct power_plant;
  using power_plant_ = shared_ptr<power_plant>;
  struct reservoir_aggregate;
  using reservoir_aggregate_ = shared_ptr<reservoir_aggregate>;
  struct optimization_summary;
  using optimization_summary_ = shared_ptr<optimization_summary>;
  struct wind_farm;
  struct stm_system;//fwd

  /**
   * @brief stm hydro_power_system
   *
   * @details
   * Is the same as the core hydro_power_system,
   * but with added attributes suitable for the stm.
   *
   * Keeps weak_ptr to owning stm_system
   *
   */
  struct stm_hps : hydro_power::hydro_power_system {
    using super = hydro_power::hydro_power_system;

    stm_hps();
    stm_hps(int id, string const & name,string const& json="");
    stm_hps(int id, string const & name,string const& json, std::shared_ptr<stm_system> const&sys);

    std::weak_ptr<stm_system> system; ///< weak ptr to owning system

    std::shared_ptr<stm_system> system_() const { return system.lock();}

    static string to_blob(shared_ptr<stm_hps> const & s);
    static shared_ptr<stm_hps> from_blob(string const & xmls);

    reservoir_aggregate_ find_reservoir_aggregate_by_name(string const & name) const;
    reservoir_aggregate_ find_reservoir_aggregate_by_id(int64_t id) const;

    bool operator==(stm_hps const & other) const;

    bool operator!=(stm_hps const & other) const {
      return !(*this == other);
    }

    /**
     * @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

    BOOST_HANA_DEFINE_STRUCT(stm_hps, (vector<reservoir_aggregate_>, reservoir_aggregates));

    SHYFT_DEFINE_STRUCT(stm_hps, (hydro_power::hydro_power_system), (reservoir_aggregates));

    x_serialize_decl();
  };

  using stm_hps_ = shared_ptr<stm_hps>;
  using stm_hps__ = weak_ptr<stm_hps>;

  struct stm_rule_exception : logic_error {
    stm_rule_exception(string const & why)
      : logic_error(why) {
    }
  };

  /**
   * @brief A builder that ensure building rules are followed
   * @details
   * The idea here is to provid functions that build a system that
   * is verified as it is built, including uniqueue identifiers and naming
   * for each individual component.
   *
   * Primary use is on the python-api -side, but it could be useful on
   * c++ side since it enforces one set of rules.
   */
  struct stm_hps_builder {
    stm_hps_ s;

    explicit stm_hps_builder(stm_hps_ const & s)
      : s{s} {
    }

    catchment_ create_catchment(int id, string const & name, string const & json);
    reservoir_ create_reservoir(int id, string const & name, string const & json);
    reservoir_aggregate_ create_reservoir_aggregate(int id, string const & name, string const & json);
    unit_ create_unit(int id, string const & name, string const & json);
    waterway_ create_waterway(int id, string const & name, string const & json);
    gate_ create_gate(int id, string const & name, string const & json);
    power_plant_ create_power_plant(int id, string const & name, string const & json);

    waterway_ create_tunnel(int id, string const & name, string const & json) {
      return create_waterway(id, name, json);
    }

    waterway_ create_river(int id, string const & name, string const & json) {
      return create_waterway(id, name, json);
    }
  };

  /**
   * @brief stm system
   * @details
   * Contains all needed components to describe a stm system.
   * This is basically the energy_market_model, but tuned to the
   * short term optimization processes and models.
   *
   * The stm system model contains
   *
   *  * (stm) hydro-power-systems (one or more)
   *  * (energy) market-price-areas that contains the hydro-power-systems
   *  * other groups, or kind of market, like frequency or frequency-reserve markets
   *
   */
  struct stm_system : id_base {
    using super = id_base;
    BOOST_HANA_DEFINE_STRUCT(
      stm_system,
      (vector<stm_hps_>, hps),
      (vector<energy_market_area_>, market),
      (vector<contract_>, contracts),
      (vector<contract_portfolio_>, contract_portfolios),
      (vector<network_>, networks),
      (vector<power_module_>, power_modules),
      (run_parameters, run_params),
      (vector<unit_group_>, unit_groups),
      (optimization_summary_, summary),
      (std::vector<std::shared_ptr<wind_farm>>, wind_farms));

    SHYFT_DEFINE_STRUCT(
      stm_system,
      (id_base),
      (hps,
       market,
       contracts,
       contract_portfolios,
       networks,
       power_modules,
       run_params,
       unit_groups,
       summary,
       wind_farms));

    stm_system();
    stm_system(int id, string name, string json);
    bool operator==(stm_system const & other) const;

    bool operator!=(stm_system const & other) const {
      return !(*this == other);
    }

    unit_group_ add_unit_group(int id, string name, string json, unit_group_type group_type = {});
    void set_summary(optimization_summary_ const & x);
    static string to_blob(shared_ptr<stm_system> const & s);
    static shared_ptr<stm_system> from_blob(string const & xmls);
    static shared_ptr<stm_system> clone_stm_system(shared_ptr<stm_system> const & s);
    static bool fix_uplinks(shared_ptr<stm_system> const & s);

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

    x_serialize_decl();
  };

  using stm_system_ = shared_ptr<stm_system>;
  using stm_system__ = weak_ptr<stm_system>;

  struct stm_builder {
    stm_system_ sys;

    explicit stm_builder(stm_system_ stmsys)
      : sys(stmsys) {
    }

    power_module_ create_power_module(int id, string const & name, string const & json);
    network_ create_network(int id, string const & name, string const & json);
    energy_market_area_ create_market_area(int id, string const & name, string const & json);
    std::shared_ptr<wind_farm> create_wind_farm(int id, string const & name, string const & json);
    stm_hps_ create_hydro_power_system(int id,string const &name,string const &json);
  };

  /**
   * @brief generic stm ts operations class
   * @details
   * Visits all the ts attributes of the stm system and applies some kind of operation
   * of type bool fx(apoint_ts&) for all attributes of type apoint_ts (including .tsm[].
   *
   * Typical usage is when making copy, rebind, or any other function that require
   * a complete traversal of all time-series.
   * The class assumes that the stm_system in hand is 'locked'/reserved for
   * the purpose of this function. Eg. thread safe access.
   * Since the apply function might do modifification to the system.
   */
  struct stm_ts_operation {
    std::function<bool(apoint_ts&)> fx; ///< callable(apoint_ts&s)->bool

    bool apply_tsm(unit_group_member&) const; // does not have .tsm attribute

    bool apply_tsm(id_base& t) const; // deals with .tsm attribute of the id_base class

    /** apply fx to class T of base-ptr class B */
    template <class T, class B>
    bool apply_object(shared_ptr<B> const & oo) const {
      bool done = false;
      auto o = dynamic_cast<T*>(oo.get());
      if (o) {
        done = apply_tsm(*o);
        hana::for_each(mp::leaf_accessors(hana::type_c<T>), [&done, o, this](auto a) {
          auto a_value = mp::leaf_access(*o, a);
          if constexpr (std::is_same_v<decltype(a_value), apoint_ts>) {
            done |= this->fx(a_value);
          }
        });
      }
      return done;
    }

    /** apply fx to vector of ptr objects */
    template <class T>
    bool apply_objects(auto&& objs) const {
      bool done = false;
      for (auto&& oo : objs) {
        done |= apply_object<T>(oo);
      }
      return done;
    }

    ///< apply fx for hps (part of apply for entire model>
    bool apply(stm_hps& hps) const;
    ///< apply fx for the entire model
    bool apply(stm_system& mdl) const;
  };

  // inputs.h
  // for-each-input(stm_system &mdl);

  //---------------------------------------------------
  // HELPER FUNCTIONS FOR CLONING MODELS WITH
  // REFERENCES TO OTHER ATTRIBUTES IN THE MODEL
  //---------------------------------------------------

  /** @brief Replace model-key part of aref_ts.id if
   * the ID starts with dstm://
   *
   * returns true if replacement was made, false if either
   * the underlying time series is not an aref_ts, or its
   * ID doesn't start with dstm://M<model_key>
   */
  bool replace_model_key_in_id(apoint_ts& ats, std::string const & new_mkey);

  /** @brief Rebind time series id of any time series type.
   * If ts is an aref_ts: executes replace_model_key_in_id
   * If ts is an expression, or has nonzero bind info: Goes through each time series in bind info
   *  and executed replace_model_key_in_id.
   * Else: Does nothing.
   *
   * @param ats: Time series to rebind
   * @param new_mkey: New model_key to replace part of url.
   * @return
   */
  bool rebind_ts(apoint_ts& ats, std::string const & new_mkey);

  /** @brief Rebind all time series in the stm_hps
   *
   */
  bool rebind_ts(stm_hps& hps, std::string const & new_mkey);
  bool rebind_ts(stm_system& mdl, std::string const & new_mkey);

  ///-----------------------------------------
  /// a stm_system can  modified by means of patch operations,
  ///

  SHYFT_DEFINE_ENUM(stm_patch_op, std::uint8_t, (add, remove_relations, remove_objects))

  /**
   * @brief patch stm_system s with patch p
   * @details
   * Given a stm_system,`s`, apply patch `p`,
   * additive, like:
   * 1. add **new** objects in p to s
   * 2. establish/ensure relations as mentioned in the patch model into s
   * @param s the stm_system to patch
   * @param op how to apply the patch
   * @param p the patch described as a minimal stm_system only containing new objects/relations
   * @note The intentional side-effect on patch `p` for the ADD operation, the auto obj.id are set to the new unique
   * object id
   */
  bool patch(stm_system_ const & s, stm_patch_op op, stm_system& p);

}

x_serialize_export_key(shyft::energy_market::stm::stm_hps);
x_serialize_export_key(shyft::energy_market::stm::stm_system);

BOOST_CLASS_VERSION(shyft::energy_market::stm::stm_hps, 4);
BOOST_CLASS_VERSION(shyft::energy_market::stm::stm_system, 3);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::stm_hps, Char>
  : shyft::
      cast_formatter<shyft::energy_market::stm::stm_hps, shyft::energy_market::hydro_power::hydro_power_system, Char> {
};

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::stm_hps>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::stm_hps, Char> { };

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::stm_system, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::stm_system, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::stm_system>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::stm_system, Char> { };
