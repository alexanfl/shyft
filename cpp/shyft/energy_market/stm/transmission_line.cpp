#include <iterator>
#include <memory>
#include <ranges>
#include <string_view>

#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  bool transmission_line::operator==(transmission_line const & o) const {
    if (this == &o)
      return true; // equal by addr.

    auto pp_equal = stm::equal_attribute(from_bb, o.from_bb) && stm::equal_attribute(to_bb, o.to_bb);

    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
      mp::leaf_accessors(hana::type_c<transmission_line>),
      pp_equal && super::operator==(o), // initial value of the fold
      [this, &o](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a))
                 : false; // only evaluate equal if the fold state is still true
      });
  }

  void transmission_line::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = std::dynamic_pointer_cast<network>(net_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/t{o_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/t" + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }
}
