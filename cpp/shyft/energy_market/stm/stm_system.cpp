#include <algorithm>
#include <memory>
#include <stdexcept>
#include <string>
#include <iterator>

#include <fmt/core.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/time_series/dd/aref_ts.h>

namespace shyft::energy_market::stm {

  stm_system::stm_system() {
    // as empty as possible.. serialization etc.
    mk_url_fx(this);
    run_params.mdl = this;
    summary = std::make_shared<optimization_summary>();
    summary->mdl = this;
  }

  stm_system::stm_system(int id, string name, string json)
    : id_base{id, name, json, {}, {}, {}} {
    mk_url_fx(this);
    run_params.mdl = this;
    summary = std::make_shared<optimization_summary>();
    summary->mdl = this;
  }

  unit_group_ stm_system::add_unit_group(int id, string name, string json, unit_group_type group_type) {
    for (auto const & ug : unit_groups) {
      if (ug->id == id || ug->name == name)
        throw std::runtime_error(
          fmt::format("unit group with same id or name already exists: id={},name={}", ug->id, ug->name));
    }
    auto ug = std::make_shared<unit_group>(this);
    ug->id = id;
    ug->name = name;
    ug->json = json;
    ug->group_type = group_type;
    unit_groups.push_back(ug);
    return ug;
  }

  shared_ptr<stm_system> stm_system::clone_stm_system(shared_ptr<stm_system> const & s) {
    auto blob = to_blob(s);
    return from_blob(blob);
  }

  bool stm_system::fix_uplinks(shared_ptr<stm_system> const & s) {
    bool changed = false;

    for (auto const & o : s->hps)
      if (o && o->system.expired()) {
        o->system = s;
        changed = true;
      }

    for (auto const & o : s->market)
      if (o && o->sys.expired()) {
        changed = true;
        o->sys = s;
      }

    for (auto const & o : s->contracts)
      if (o && o->sys.expired()) {
        changed = true;
        o->sys = s;
      }

    for (auto const & o : s->contract_portfolios)
      if (o && o->sys.expired()) {
        changed = true;
        o->sys = s;
      }

    for (auto const & o : s->networks)
      if (o && o->sys.expired()) {
        changed = true;
        o->sys = s;
      }

    for (auto const & o : s->power_modules)
      if (o && o->sys.expired()) {
        changed = true;
        o->sys = s;
      }

    for (auto const & o : s->unit_groups)
      if (o && o->mdl == nullptr) {
        changed = true;
        o->mdl = s.get();
      }

    for (auto const & o : s->wind_farms)
      if (o && o->sys.expired()) {
        changed = true;
        o->sys = s;
      }

    return changed;
  }

  static void _generate_url(
    std::string const & prefix,
    std::string const & id_expr,
    int id,
    std::back_insert_iterator<string>& rbi,
    int /*levels*/,
    int template_levels) {
    if (!template_levels) {
      auto const a = prefix + id_expr;
      std::ranges::copy(a, rbi);
    } else {
      auto const idstr = prefix + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }

  void stm_system::generate_url(std::back_insert_iterator<string>&, int, int) const {
    // ref issue https://gitlab.com/shyft-os/shyft/-/issues/829
    //_generate_url("/M","{o_id}",id,rbi,levels,template_levels);//do not make url at this level
  }

  stm_hps::stm_hps() {
    // as empty as possible.. serialization etc.
  }

  stm_hps::stm_hps(int id, string const & name, string const & json)
    : super(id, name, json) {
  }

  stm_hps::stm_hps(int id, string const & name, string const & json, stm_system_ const & sys)
    : super(id, name, json)
    , system{sys} {
  }

  void stm_hps::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    _generate_url("/H", "{parent_id}", id, rbi, levels, template_levels);
  }

  bool stm_hps::operator==(stm_hps const & other) const {
    if (this == &other)
      return true; // equal by addr.
    // NOTE:  Q: should we compare owning sys? A: no
    return super::operator==(other) // basic structure compared equal
        && equal_vector_ptr_content<reservoir>(reservoirs, other.reservoirs)
        && equal_vector_ptr_content<unit>(units, other.units)
        && equal_vector_ptr_content<waterway>(waterways, other.waterways)
        && equal_vector_ptr_content<power_plant>(power_plants, other.power_plants)
        && equal_vector_ptr_content<reservoir_aggregate>(reservoir_aggregates, other.reservoir_aggregates);
  }

  reservoir_aggregate_ stm_hps::find_reservoir_aggregate_by_name(string const & name) const {
    return hydro_power_system::find_by_name(reservoir_aggregates, name);
  }

  reservoir_aggregate_ stm_hps::find_reservoir_aggregate_by_id(int64_t id) const {
    return hydro_power_system::find_by_id(reservoir_aggregates, id);
  }

  template <class T, class CT>
  void ensure_unique_id_and_name(
    std::string const & sysname,
    string const & tp_name,
    CT& c,
    int id,
    std::string const & name,
    std::string const & /*json*/) {
    if (std::ranges::any_of(c, [&name](auto const & w) -> bool {
          return w->name == name;
        }))
      throw stm_rule_exception(
        fmt::format("{} name must be unique within a {}, name '{}' already exists", tp_name, sysname, name));
    if (std::ranges::any_of(c, [&id](auto const & w) -> bool {
          return w->id == id;
        }))
      throw stm_rule_exception(
        fmt::format("{} id must be unique within a {}, id {} already exists", tp_name, sysname, name));
  }

  template <class T, class CT>
  shared_ptr<T> add_ensure_unique_id_and_name(
    stm_hps_& sys,
    string const & tp_name,
    CT& c,
    int id,
    string const & name,
    string const & json) {
    ensure_unique_id_and_name<T>("HydroPowerSystem", tp_name, c, id, name, json);
    auto o = std::make_shared<T>(id, name, json, sys);
    c.push_back(o);
    return o;
  }

  template <class T, class CT>
  shared_ptr<T> add_ensure_unique_id_and_name(
    stm_system_& sys,
    string const & tp_name,
    CT& c,
    int id,
    string const & name,
    string const & json) {
    ensure_unique_id_and_name<T>("StmSystem", tp_name, c, id, name, json);
    auto o = std::make_shared<T>(id, name, json, sys);
    c.push_back(o);
    return o;
  }

  reservoir_ stm_hps_builder::create_reservoir(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<reservoir>(s, "Reservoir", s->reservoirs, id, name, json);
  }

  reservoir_aggregate_ stm_hps_builder::create_reservoir_aggregate(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<reservoir_aggregate>(
      s, "ReservoirAggregate", s->reservoir_aggregates, id, name, json);
  }

  unit_ stm_hps_builder::create_unit(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<unit>(s, "Unit", s->units, id, name, json);
  }

  power_plant_ stm_hps_builder::create_power_plant(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<power_plant>(s, "PowerPlant", s->power_plants, id, name, json);
  }

  waterway_ stm_hps_builder::create_waterway(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<waterway>(s, "Waterway", s->waterways, id, name, json);
  }

  catchment_ stm_hps_builder::create_catchment(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<catchment>(s, "Catchment", s->catchments, id, name, json);
  }

  gate_ stm_hps_builder::create_gate(int id, string const & name, string const & json) {
    auto gts = s->gates();
    ensure_unique_id_and_name<gate>("HydroPowerSystem", "Gate", gts, id, name, json);
    return std::make_shared<gate>(id, name, json);
  }

  power_module_ stm_builder::create_power_module(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<power_module>(sys, "PowerModule", sys->power_modules, id, name, json);
  }

  network_ stm_builder::create_network(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<network>(sys, "Network", sys->networks, id, name, json);
  }

  energy_market_area_ stm_builder::create_market_area(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<energy_market_area>(sys, "MarketArea", sys->market, id, name, json);
  }

  std::shared_ptr<wind_farm> stm_builder::create_wind_farm(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<wind_farm>(sys, "WindFarm", sys->wind_farms, id, name, json);
  }

  stm_hps_ stm_builder::create_hydro_power_system(int id, string const & name, string const & json) {
    return add_ensure_unique_id_and_name<stm_hps>(sys, "HydroPowerSystem", sys->hps, id, name, json);
  }

  void stm_system::set_summary(optimization_summary_ const & x) {
    if (!x) {
      if (summary)
        *summary = optimization_summary{};
    } else {
      if (summary)
        *summary = *x;
    }
  }

  bool stm_system::operator==(stm_system const & o) const {
    if (this == &o)
      return true; // equal by addr.
    return super::operator==(o) && equal_vector_ptr_content<stm_hps>(hps, o.hps)
        && equal_vector_ptr_content<energy_market_area>(market, o.market)
        && equal_vector_ptr_content<contract>(contracts, o.contracts)
        && equal_vector_ptr_content<contract_portfolio>(contract_portfolios, o.contract_portfolios)
        && equal_vector_ptr_content<network>(networks, o.networks)
        && equal_vector_ptr_content<power_module>(power_modules, o.power_modules)
        && equal_vector_ptr_content<wind_farm>(wind_farms, o.wind_farms) && run_params == o.run_params
        && equal_vector_ptr_content<unit_group>(unit_groups, o.unit_groups)
        && (summary == o.summary || (summary && o.summary && *summary == *o.summary));
  }

  bool stm_ts_operation::apply_tsm(unit_group_member&) const {
    return false;
  }

  bool stm_ts_operation::apply_tsm(id_base& t) const {
    bool done = false;
    for (auto it = t.tsm.begin(); it != t.tsm.end(); ++it) {
      done |= fx(it->second);
    }
    return done;
  }

  bool stm_ts_operation::apply(stm_hps& hps) const {
    bool done = false;
    done |= apply_objects<stm::reservoir>(hps.reservoirs);
    done |= apply_objects<stm::unit>(hps.units);
    done |= apply_objects<stm::waterway>(hps.waterways);
    done |= apply_objects<stm::catchment>(hps.catchments);
    done |= apply_objects<stm::power_plant>(hps.power_plants);
    done |= apply_objects<stm::gate>(hps.gates());
    done |= apply_objects<stm::reservoir_aggregate>(hps.reservoir_aggregates);
    return done;
  }

  bool stm_ts_operation::apply(stm_system& mdl) const {
    bool done = false;
    for (auto& h : mdl.hps) {
      done |= apply(*h);
    }
    done |= apply_objects<stm::energy_market_area>(mdl.market);
    done |= apply_objects<stm::unit_group>(mdl.unit_groups);
    done |= apply_objects<stm::contract>(mdl.contracts);
    done |= apply_objects<stm::contract_portfolio>(mdl.contract_portfolios);
    done |= apply_objects<stm::network>(mdl.networks);
    done |= apply_objects<stm::power_module>(mdl.power_modules);
    for (auto const & ug : mdl.unit_groups) { // ensure to rebind ug.members
      done |= apply_objects<stm::unit_group_member>(ug->members);
    }
    return done;
  }

  bool replace_model_key_in_id(apoint_ts& ats, std::string const & new_mkey) {
    using namespace time_series::dd;
    if (auto ts = ts_as_mutable<aref_ts>(ats.ts)) {
      if (ts->id.rfind("dstm://M", 0) == 0) {
        // Get position where model-key ends:
        auto rpos = ts->id.find("/", 8); //"dstm://M".size() == 8
        if (rpos == std::string::npos)
          return false; // assume nothing, false is ok to return
        if (new_mkey != ts->id.substr(8, rpos - 8)) {
          ts->id = ts->id.replace(8, rpos - 8, new_mkey);
          return true;
        }
      }
    }
    return false;
  }

  bool rebind_ts(apoint_ts& ats, std::string const & new_mkey) {
    auto atsv = ats.find_ts_bind_info();
    bool rebind_done = false;
    for (auto& tsi : atsv) {
      rebind_done |= replace_model_key_in_id(tsi.ts, new_mkey);
    }
    return rebind_done;
  }

  bool rebind_ts(stm_system& mdl, std::string const & new_mkey) {
    stm_ts_operation r{[&new_mkey](apoint_ts& ats) {
      auto atsv = ats.find_ts_bind_info();
      bool done = false;
      for (auto& tsi : atsv) {
        done |= replace_model_key_in_id(tsi.ts, new_mkey);
      }
      return done;
    }};
    return r.apply(mdl);
  }

}
