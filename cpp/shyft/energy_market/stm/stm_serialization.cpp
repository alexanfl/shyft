#include <shyft/energy_market/stm/stm_serialization.h>
#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/energy_market/stm/stm_serialize_v0.h>
#include <shyft/mp/assign.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
// instantiate serialization templates here, ref. shyft expression-serialization for the ids_c  type
using namespace boost::serialization;
using shyft::energy_market::stm::serialize_stm_attributes;
using core_iarchive = boost::archive::binary_iarchive;
using core_oarchive = boost::archive::binary_oarchive;
using shyft::core::core_arch_flags;
using shyft::core::core_nvp;

template <class Archive>
void shyft::energy_market::stm::reservoir::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this));
  if (version < 1) {
    ar& core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this));
    v0::reservoir r;
    serialize_stm_attributes(r, ar);
    mp::leaf_access_assign(*this, r);
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::reservoir_aggregate::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this)) & core_nvp("hps", hps)
    & core_nvp("reservoirs", reservoirs);
  if (version < 1) {
    v0::reservoir_aggregate ra;
    serialize_stm_attributes(ra, ar);
    mp::leaf_access_assign(*this, ra);
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::unit::serialize(Archive& ar, unsigned int const version) {

  ar& core_nvp("super", base_object<super>(*this));
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this));
      v0::unit u;
      serialize_stm_attributes(u, ar);
      mp::leaf_access_assign(*this, u); // would throw.. if we did not trick the unit v0
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::power_plant::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this));
  if (version < 1) {
    ar& core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this));
    v0::power_plant p;
    serialize_stm_attributes(p, ar);
    mp::leaf_access_assign(*this, p);
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::catchment::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this));
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this));
      v0::catchment c;
      serialize_stm_attributes(c, ar);
      mp::leaf_access_assign(*this, c);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::waterway::serialize(Archive& ar, unsigned int const version) {
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core_nvp("super", base_object<super>(*this))
        & core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this));
      v0::waterway w; // snapshot of what was v0 of waterway, we use it to correctly extract values from archive
      serialize_stm_attributes(w, ar);
      mp::leaf_access_assign(*this, w); // perform assing hana attrs from s to d, effectively d=s
    }
  } else {
    ar& core_nvp("super", base_object<super>(*this));
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::gate::serialize(Archive& ar, unsigned int const version) {
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core_nvp("super", base_object<super>(*this))
        & core_nvp("id_base", base_object<shyft::energy_market::id_base>(*this));
      v0::gate g;
      serialize_stm_attributes(g, ar);
      mp::leaf_access_assign(*this, g);
    }
  } else {
    ar& core_nvp("super", base_object<super>(*this));
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::stm_hps::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) // core hydro power goes here(core model,topology)
    & core_nvp("reservoir_aggregates", reservoir_aggregates);
  if(version>3) {
    if constexpr(Archive::is_loading::value) {
      stm_system_ s;
      ar& core_nvp("system",s);
      system=s;
    } else {
      stm_system_ s=system.lock();
      ar& core_nvp("system",s);
    }
  }
}

template <class Archive>
void shyft::energy_market::stm::run_parameters::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("mdl", mdl);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::run_parameters r;
      serialize_stm_attributes(r, ar);
      mp::leaf_access_assign(*this, r);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::unit_group::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("mdl", mdl) & core_nvp("members", members);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::unit_group ug;
      serialize_stm_attributes(ug, ar);
      mp::leaf_access_assign(*this, ug);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::unit_group_member::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("group", group) & core_nvp("unit", unit);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::unit_group_member ugm;
      serialize_stm_attributes(ugm, ar);
      mp::leaf_access_assign(*this, ugm);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::stm_system::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this));
  if (version < 3) {
    if constexpr (Archive::is_loading::value) {
      // OBS! care must be taken here, the members(unit_group, network) have uplink to this, as ptr and weak_ptr,
      // so v2 stream and simple assign will fail.
      // And since we do not have share_ptr of this, then we can not
      // repair it either (unless we also override shared_ptr/weakptr for the stm_system)
      // thus just mimic the v2 leaf node deserialize is safe,minimal, efficient and precise
      ar& core_nvp("hps", hps) & core_nvp("market", market) & core_nvp("contracts", contracts)
        & core_nvp("contract_portfolio", contract_portfolios) & core_nvp("networks", networks)
        & core_nvp("power_modules", power_modules) & core_nvp("run_params.n_inc_runs", run_params.n_inc_runs)
        & core_nvp("run_params.n_full_runs", run_params.n_full_runs)
        & core_nvp("run_params.head_opt", run_params.head_opt)
        & core_nvp("run_params.run_time_axis", run_params.run_time_axis)
        & core_nvp("run_params.fx_log", run_params.fx_log) & core_nvp("unit_groups", unit_groups)
        & core_nvp("summary", summary);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::energy_market_area::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("sys", sys) & core_nvp("unit_groups", unit_groups)
    & core_nvp("contracts", contracts);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::energy_market_area ma;
      serialize_stm_attributes(ma, ar);
      mp::leaf_access_assign(*this, ma);
    }
  } else {
    ar& core_nvp("busbars", busbars);
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::contract::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("sys", sys) // weak_ptr Hmm. did that really work ..
    & core_nvp("power_plants", power_plants);                             // remember to stream out relations
  ;
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::contract c;
      serialize_stm_attributes(c, ar);
      mp::leaf_access_assign(*this, c);
    }
  } else {
    ar& core_nvp("relations", relations); // remember to stream out relations
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::contract_relation::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("owner", owner) & core_nvp("id", id) & core_nvp("related", related);
  serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::contract_portfolio::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("sys", sys) & core_nvp("contracts", contracts);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::contract_portfolio cp;
      serialize_stm_attributes(cp, ar);
      mp::leaf_access_assign(*this, cp);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::network::serialize(Archive& ar, unsigned int const /*version*/) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("sys", sys) // weak_ptr
    ;
  serialize_stm_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::power_module::serialize(Archive& ar, unsigned int const version) {

  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("sys", sys) // weak_ptr
    ;
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      shared_ptr<busbar> connected;
      ar& core_nvp("connected", connected); // TODO: figure out if we need to do more that discard this
      v0::power_module m;
      serialize_stm_attributes(m, ar);
      mp::leaf_access_assign(*this, m);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::transmission_line::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("net", net) // weak_ptr
    & core_nvp("from_bb", from_bb) & core_nvp("to_bb", to_bb);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core_nvp("capacity", capacity); // effectively what the old serialize_stm_attributes did
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::busbar::serialize(Archive& ar, unsigned int const version) {
  ar // common from version 0:
      & core_nvp("super", base_object<super>(*this))
    & core_nvp("net", net) // weak_ptr
    ;
  if (version < 1) {
    if constexpr (Archive::is_loading::value) { // bw compat read
      apoint_ts dummy;
      ar& core_nvp("dummy", dummy); // read away the dummy
    }
  } else {
    ar // common from version 1:
        & core_nvp("power_modules", power_modules)
      & core_nvp("units", units);
    if (version > 1) {
      ar // common from version 2:
        & core_nvp("wind_farms", wind_farms);
    }
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::power_module_member::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("owner", owner) & core_nvp("power_module", power_module);
  serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::unit_member::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("owner", owner) & core_nvp("unit", unit);
  serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::wind_farm_member::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("owner", owner) & core_nvp("farm", farm);
  serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::srv::model_ref::serialize(Archive& ar, unsigned int const /*version*/) {
  ar& core_nvp("host", host) & core_nvp("port_num", port_num) & core_nvp("api_port_num", api_port_num)
    & core_nvp("model_key", model_key) & core_nvp("labels", labels);
}

template <class Archive>
void shyft::energy_market::stm::srv::stm_case::serialize(Archive& ar, unsigned int const /*version*/) {
  ar& core_nvp("id", id) & core_nvp("name", name) & core_nvp("json", json) & core_nvp("created", created)
    & core_nvp("labels", labels) & core_nvp("model_refs", model_refs);
}

template <class Archive>
void shyft::energy_market::stm::srv::stm_task::serialize(Archive& ar, unsigned int const /*version*/) {
  ar& core_nvp("id", id) & core_nvp("name", name) & core_nvp("json", json) & core_nvp("created", created)
    & core_nvp("labels", labels) & core_nvp("runs", cases) & core_nvp("base_mdl", base_mdl)
    & core_nvp("task_name", task_name);
}

template <class Archive>
void shyft::energy_market::stm::optimization_summary::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("mdl", mdl);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      v0::optimization_summary os;
      serialize_stm_attributes(os, ar);
      mp::leaf_access_assign(*this, os);
    }
  } else {
    serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::wind_farm::serialize(Archive& ar, unsigned int const version) {
  ar& core_nvp("super", base_object<super>(*this)) & core_nvp("sys", sys) // weak ptr
    ;
  serialize_named_attributes(*this, ar);
}

//
// 4. Then include the archive supported
//

// repeat template instance for each archive class
#define xxx_arch(T) x_serialize_archive(T, boost::archive::binary_oarchive, boost::archive::binary_iarchive)

xxx_arch(shyft::energy_market::stm::unit);
xxx_arch(shyft::energy_market::stm::unit_group);
xxx_arch(shyft::energy_market::stm::unit_group_member);
xxx_arch(shyft::energy_market::stm::power_plant);
xxx_arch(shyft::energy_market::stm::reservoir);
xxx_arch(shyft::energy_market::stm::reservoir_aggregate);
xxx_arch(shyft::energy_market::stm::catchment);
xxx_arch(shyft::energy_market::stm::waterway);
xxx_arch(shyft::energy_market::stm::gate);
xxx_arch(shyft::energy_market::stm::stm_hps);
xxx_arch(shyft::energy_market::stm::run_parameters);
xxx_arch(shyft::energy_market::stm::stm_system);
xxx_arch(shyft::energy_market::stm::energy_market_area);
xxx_arch(shyft::energy_market::stm::contract);
xxx_arch(shyft::energy_market::stm::contract_relation);
xxx_arch(shyft::energy_market::stm::contract_portfolio);
xxx_arch(shyft::energy_market::stm::network);
xxx_arch(shyft::energy_market::stm::transmission_line);
xxx_arch(shyft::energy_market::stm::busbar);
xxx_arch(shyft::energy_market::stm::power_module);
xxx_arch(shyft::energy_market::stm::unit_member);
xxx_arch(shyft::energy_market::stm::power_module_member);
xxx_arch(shyft::energy_market::stm::srv::model_ref);
xxx_arch(shyft::energy_market::stm::srv::stm_case);
xxx_arch(shyft::energy_market::stm::srv::stm_task);
xxx_arch(shyft::energy_market::stm::optimization_summary);
xxx_arch(shyft::energy_market::stm::wind_farm);

#undef xxx_arch

namespace shyft::energy_market::stm {

  /** polymorphic types needed some pre-registration to stream
   * If you get 'unknown class' error while doing serialization,
   * this is the place.
   * NOTE: add at the end, .. if needed, otherwise bw.compat is broken.
   */
  template <class Archive>
  void register_types(Archive& a) {
    a.template register_type<reservoir>();
    a.template register_type<reservoir_aggregate>();
    a.template register_type<waterway>();
    a.template register_type<unit>();
    a.template register_type<unit_group>();
    a.template register_type<gate>();
    a.template register_type<power_plant>();
    a.template register_type<catchment>();
    a.template register_type<stm_hps>();
    a.template register_type<stm_system>();
    a.template register_type<energy_market_area>();
    a.template register_type<contract>();
    a.template register_type<contract_portfolio>();
    a.template register_type<network>();
    a.template register_type<transmission_line>();
    a.template register_type<busbar>();
    a.template register_type<power_module>();
    a.template register_type<optimization_summary>();
  }

  /**fx_to_blob simply serializes an object to a blob */
  template <class T>
  static string fx_to_blob(shared_ptr<T> const & s) {
    std::ostringstream xmls;
    {
      core_oarchive oa(xmls, core_arch_flags);
      register_types(oa);
      oa << core_nvp("hps", s);
    }
    xmls.flush();
    return xmls.str();
  }

  /** fx_from_blob de-serializes a blob to a fully working object*/
  template <class T>
  static shared_ptr<T> fx_from_blob(string const & xmls) {
    shared_ptr<T> s;
    std::istringstream xmli(xmls);
    {
      // boost::archive::xml_iarchive ia(xmli);
      core_iarchive ia(xmli, core_arch_flags);
      register_types(ia);
      ia >> core_nvp("hps", s);
    }
    return s;
  }

  // implementation for main classes, using the templates above

  string stm_hps::to_blob(shared_ptr<stm_hps> const & s) {
    return fx_to_blob<stm_hps>(s);
  }

  shared_ptr<stm_hps> stm_hps::from_blob(string const & xmls) {
    return fx_from_blob<stm_hps>(xmls);
  }

  string stm_system::to_blob(shared_ptr<stm_system> const & s) {
    return fx_to_blob<stm_system>(s);
  }

  shared_ptr<stm_system> stm_system::from_blob(string const & xmls) {
    auto sys= fx_from_blob<stm_system>(xmls);
    stm_system::fix_uplinks(sys);// ensure uplinks are in place for any version
    return sys;
  }

}
