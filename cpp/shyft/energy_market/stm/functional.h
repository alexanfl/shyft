#pragma once

#include <cstdint>
#include <functional>
#include <type_traits>
#include <tuple>
#include <utility>
#include <variant>

#include <boost/hana/accessors.hpp>
#include <boost/hana/ext/std/tuple.hpp>
#include <boost/hana/fold.hpp>
#include <boost/hana/pair.hpp>
#include <boost/hana/transform.hpp>
#include <boost/preprocessor/punctuation/remove_parens.hpp>

#include <shyft/core/utility.h>
#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/model.h>

namespace shyft::energy_market::stm {

  constexpr std::int64_t component_id(auto &&comp) {
    if constexpr (requires { comp.id(); })
      return comp.id();
    else
      return comp.id;
  }

#define SHYFT_ENERGY_MARKET_STM_COMPS \
  (stm_hps, \
   energy_market_area, \
   contract, \
   contract_portfolio, \
   network, \
   power_module, \
   run_parameters, \
   unit_group, \
   optimization_summary)

  namespace detail {
    template <typename... T>
    using root_component_types_impl = std::tuple<std::type_identity<T>...>;
  }

  inline constexpr auto root_component_types =
    detail::root_component_types_impl< BOOST_PP_REMOVE_PARENS(SHYFT_ENERGY_MARKET_STM_COMPS) >{};

  namespace detail {
    template <typename T>
    struct default_subcomponent_type_impl : identity_t<T> { };

    template <typename T>
    requires(is_std_vector<T>)
    struct default_subcomponent_type_impl<T> : default_subcomponent_type_impl<typename T::value_type> { };

    template <typename T>
    requires(is_std_shared_ptr<T>)
    struct default_subcomponent_type_impl<T> : default_subcomponent_type_impl<typename T::element_type> { };
  }

  template <auto member_ptr>
  using default_subcomponent_type_t = typename detail::default_subcomponent_type_impl<member_type_t<member_ptr>>::type;

  template <auto member_ptr, typename type = default_subcomponent_type_t<member_ptr>>
  inline constexpr auto subcomponent_v = std::tuple{constant_v<member_ptr>, identity_v<type>};

  template <typename tag>
  inline constexpr auto subcomponent_member_v = std::tuple_element_t<0, tag>::value;
  template <typename tag>
  using subcomponent_type_t = typename std::tuple_element_t<1, tag>::type;

  template <typename T>
  constexpr auto subcomponents(identity_t<T>) {
    return std::make_tuple();
  }

  constexpr auto subcomponents(identity_t<stm_system>) {
    using T = stm_system;
    return std::make_tuple(
      subcomponent_v<&T::hps>,
      subcomponent_v<&T::market>,
      subcomponent_v<&T::contracts>,
      subcomponent_v<&T::contract_portfolios>,
      subcomponent_v<&T::networks>,
      subcomponent_v<&T::power_modules>,
      subcomponent_v<&T::run_params>,
      subcomponent_v<&T::unit_groups>,
      subcomponent_v<&T::summary>);
  }

  constexpr auto subcomponents(identity_t<stm_hps>) {
    using T = stm_hps;
    return std::make_tuple(
      subcomponent_v<&T::reservoirs, stm::reservoir>,
      subcomponent_v<&T::units, stm::unit>,
      subcomponent_v<&T::waterways, stm::waterway>,
      subcomponent_v<&T::catchments, stm::catchment>,
      subcomponent_v<&T::power_plants, stm::power_plant>,
      subcomponent_v<&T::reservoir_aggregates>);
  }

  constexpr auto subcomponents(identity_t<stm::waterway>) {
    using T = stm::waterway;
    return std::make_tuple(subcomponent_v<&T::gates, stm::gate>);
  }

  constexpr auto subcomponents(identity_t<stm::network>) {
    using T = stm::network;
    return std::make_tuple(subcomponent_v<&T::transmission_lines>, subcomponent_v<&T::busbars>);
  }

  constexpr auto subcomponents(identity_t<stm::busbar>) {
    using T = stm::busbar;
    return std::make_tuple(subcomponent_v<&T::units>, subcomponent_v<&T::power_modules>);
  }

  namespace concepts {

    template <typename T>
    concept stm_system = std::is_same_v<stm_system, std::remove_cvref_t<T>>;

  }

  namespace detail {
    template <bool is_const, typename tag, typename member>
    constexpr auto component_traverse_unwrap(tag /*t*/, member &m) {
      using component = subcomponent_type_t<tag>;
      using unqual_member = std::remove_cvref_t<member>;
      if constexpr (is_std_shared_ptr<unqual_member>) {
        using component_base = typename unqual_member::element_type;
        if constexpr (!std::is_same_v<component_base, component>)
          return std::ref(dynamic_cast<add_const_if_t<is_const, component> &>(*m));
        else
          return std::ref(*m);
      } else {
        static_assert(std::is_same_v<component, unqual_member>);
        return std::ref(m);
      }
    }
  }

  // FIXME: constrain ops... - jeh
  template <typename R>
  constexpr auto component_traverse(R &&component, auto value, auto &&op_pre, auto &&op_post) {
    constexpr auto is_const = std::is_const_v<std::remove_cvref_t<R>>;

    auto visit = [&](auto &&value, auto tag, auto &subcomponent) {
      auto subcomponent_ref = detail::component_traverse_unwrap<is_const>(tag, subcomponent);
      return std::invoke(
        op_post,
        component_traverse(
          subcomponent_ref.get(), std::invoke(op_pre, std::move(value), tag, subcomponent_ref.get()), op_pre, op_post),
        tag,
        subcomponent_ref.get());
    };

    return boost::hana::fold(
      subcomponents(identity_v<std::remove_cvref_t<R>>), std::move(value), [&]<typename tag>(auto &&value, tag) {
        auto &subcomponent = (component.*subcomponent_member_v<tag>);
        if constexpr (is_std_vector<std::remove_cvref_t<decltype(subcomponent)>>) {
          for (auto &&subelement : subcomponent)
            value = visit(std::move(value), tag{}, subelement);
          return value;
        } else
          return visit(std::move(value), tag{}, subcomponent);
      });
  }

}
