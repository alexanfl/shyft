#pragma once
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <type_traits>
#include <algorithm>

#include <fmt/chrono.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/energy_market/constraints.h>
#include <shyft/core/math_utilities.h>

namespace shyft::energy_market::stm {
  using std::shared_ptr;
  using std::vector;
  using std::map;
  using std::string;
  using std::pair;

  using shyft::core::utctime;
  using shyft::core::utctimespan;
  using shyft::core::calendar;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::POINT_AVERAGE_VALUE;
  using shyft::time_axis::generic_dt;

  using hydro_power::xy_point_curve_;
  using hydro_power::xy_point_curve_with_z_;
  using xy_point_curve_with_z_list = vector<hydro_power::xy_point_curve_with_z>;
  using xy_point_curve_with_z_list_ = shared_ptr<xy_point_curve_with_z_list>;
  using hydro_power::turbine_description_;

  template <typename T>
  using t_T = shared_ptr<map<utctime, T>>; ///< all attributes are time-dependent (as in 'from that time, the value of
                                           ///< this attribute should be...')

  //-- these are the basic types (in addition to the already existing value-types from time_series library)
  using t_xy_ = t_T<xy_point_curve_>;                   ///< given t: y=f(x)
  using t_xyz_ = t_T<xy_point_curve_with_z_>;           ///< given t: y=f(x,z)
  using t_xyz_list_ = t_T<xy_point_curve_with_z_list_>; ///< given t: give z, -> xy
  using t_turbine_description_ = t_T<turbine_description_>;

  // -- Comparison operators for above types (note that the operators are specific to this namespace)
  //    There could potentially be some issues because this == is called instead of std::operator==.
  //    More testing needed.
  //
  //    Attribute comparison could be simplified if some of the shared pointers can be dropped.
  namespace impl {

    template <typename T, typename = void>
    struct has_equality : std::false_type { };

    template <typename T>
    struct has_equality<T, std::void_t<decltype(std::declval<T&>() == std::declval<T&>())>> : std::true_type { };

    // fwds: required for gcc 12.1 and onwards.
    // fwds: that need to have the default args (the impl goes with empty enble if t placeholder
    template <typename T, typename = std::enable_if_t<has_equality<T>::value>>
    bool operator==(std::shared_ptr<T> const & lhs, std::shared_ptr<T> const & rhs);
    template <typename T, typename = std::enable_if_t<has_equality<T>::value>>
    bool operator==(std::vector<std::shared_ptr<T>> const & lhs, std::vector<std::shared_ptr<T>> const & rhs);
    template <typename K, typename V, typename = std::enable_if_t<has_equality<V>::value>>
    bool operator==(std::map<K, V> const & lhs, std::map<K, V> const & rhs);

    template <typename T, typename>
    bool operator==(std::shared_ptr<T> const & lhs, std::shared_ptr<T> const & rhs) {
      // check ptr contents if both are valid
      return std::operator==(lhs, rhs) || ((lhs && rhs) && (*lhs == *rhs));
    }

    template <typename T, typename = std::enable_if_t<has_equality<T>::value>>
    bool operator!=(std::shared_ptr<T> const & lhs, std::shared_ptr<T> const & rhs) {
      return !operator==(lhs, rhs);
    }

    template <typename T, typename>
    bool operator==(std::vector<std::shared_ptr<T>> const & lhs, std::vector<std::shared_ptr<T>> const & rhs) {
      return (lhs.size() == rhs.size())
          && std::equal(lhs.cbegin(), lhs.cend(), rhs.cbegin(), [](auto const & lhs, auto const & rhs) {
               return std::operator==(lhs, rhs) || ((lhs && rhs) && *lhs == *rhs);
             });
    }

    template <typename T, typename = std::enable_if_t<has_equality<T>::value>>
    bool operator!=(std::vector<std::shared_ptr<T>> const & lhs, std::vector<std::shared_ptr<T>> const & rhs) {
      return !operator==(lhs, rhs);
    }

    template <typename K, typename V, typename>
    bool operator==(std::map<K, V> const & lhs, std::map<K, V> const & rhs) {
      // check values with == as defined above
      return std::operator==(lhs, rhs)
          || ((lhs.size() == rhs.size()) && std::all_of(lhs.begin(), lhs.end(), [&rhs](auto const & a) {
                return rhs.count(a.first) && rhs.at(a.first) == a.second;
              }));
    }

    template <typename K, typename V, typename = std::enable_if_t<has_equality<V>::value>>
    bool operator!=(std::map<K, V> const & lhs, std::map<K, V> const & rhs) {
      return !operator==(lhs, rhs);
    }

  }

  // Comparison function stm attributes
  // TODO: We need to be able to compare unbound time series(better done on apoint_ts, non-trivial for strong def)
  template <typename T>
  inline bool equal_attribute(T const & lhs, T const & rhs) {
    using impl::operator==;
    return lhs == rhs;
  }

  template <>
  inline bool equal_attribute(double const & lhs, double const & rhs) {
    return ::shyft::core::nan_equal(lhs, rhs);
  }

  using any_attr = std::variant<
    bool,
    double,
    std::int64_t,
    std::uint64_t,
    time_series::dd::apoint_ts,
    t_xy_,
    t_xyz_,
    t_xyz_list_,
    t_turbine_description_,
    std::string,
    time_series::dd::ats_vector>;

  template <typename T>
  inline constexpr bool is_any_attr_v = std::is_same_v<std::remove_cvref_t<T>, any_attr>;

}
