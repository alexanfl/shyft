#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  bool busbar::operator==(busbar const & o) const {
    if (this == &o)
      return true; // equal by addr.
    if (super::operator!=(o))
      return false;

    // Compare relations
    constexpr auto equal_id_base = [](super const & s1, super const & s2) {
      return s1 == s2;
    };
    constexpr auto equal_object = [equal_id_base](auto const & t1, auto const & t2) -> bool {
      if (t1 == t2)
        return true; // equal by addr
      if (t1 == nullptr || t2 == nullptr)
        return false;
      return equal_id_base(*t1, *t2); // safe to dereference shared_ptr here
    };
    constexpr auto equal_vector = [equal_object](auto const & a, auto const & b) {
      return std::is_permutation(a.begin(), a.end(), b.begin(), b.end(), equal_object);
    };

    if (!equal_vector(get_transmission_lines_to_busbar(), o.get_transmission_lines_to_busbar()))
      return false;
    if (!equal_vector(get_transmission_lines_from_busbar(), o.get_transmission_lines_from_busbar()))
      return false;
    if (!equal_vector(get_market_areas(), o.get_market_areas()))
      return false;

    // Compare unit and power_module members
    constexpr auto equal_member = [equal_id_base](auto const & t1, auto const & t2) -> bool {
      if (t1 == t2)
        return true; // equal by addr
      if (t1 == nullptr || t2 == nullptr)
        return false;
      if (t1->id() != t2->id())
        return false;
      return *t1 == *t2;
    };
    constexpr auto equal_members = [equal_member](auto const & a, auto const & b) {
      return std::is_permutation(a.begin(), a.end(), b.begin(), b.end(), equal_member);
    };

    if (!equal_members(units, o.units))
      return false;
    if (!equal_members(power_modules, o.power_modules))
      return false;
    if (!equal_members(wind_farms, o.wind_farms))
      return false;
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
      mp::leaf_accessors(hana::type_c<busbar>),
      super::operator==(o), // initial value of the fold
      [this, &o](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a))
                 : false; // only evaluate equal if the fold state is still true
      });
  }

  void busbar::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      if (auto network = net_(); network)
        network->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/b{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/b" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  busbar_ busbar::shared_from_this() const {
    if (auto n = net_(); n) {
      for (auto const & b : n->busbars)
        if (b.get() == this)
          return b;
    }
    return nullptr;
  }

  vector<transmission_line_> busbar::get_transmission_lines_from_busbar() const {
    vector<transmission_line_> result;
    if (auto n = net_(); n) {
      for (auto const & tl : n->transmission_lines) {
        if (tl->from_bb.get() == this) {
          result.push_back(tl);
        }
      }
    }
    return result;
  }

  vector<transmission_line_> busbar::get_transmission_lines_to_busbar() const {
    vector<transmission_line_> result;
    if (auto n = net_(); n) {
      for (auto const & tl : n->transmission_lines) {
        if (tl->to_bb.get() == this) {
          result.push_back(tl);
        }
      }
    }
    return result;
  }

  std::vector<energy_market_area_> busbar::get_market_areas() const {
    std::vector<energy_market_area_> ma_list;

    if (auto n = net_(); n) {
      if (auto s = n->sys_(); s) {
        for (auto market_area : s->market) {
          if (any_of(market_area->busbars.begin(), market_area->busbars.end(), [this](const busbar_ b) {
                return b.get() == this;
              }))
            ma_list.push_back(market_area);
        }
      }
    }
    return ma_list;
  }

  void busbar::add_to_start_of_transmission_line(transmission_line_ tl) const {
    auto me = shared_from_this();
    if (!me)
      throw std::runtime_error("this busbar is not associated with a network");
    tl->from_bb = me;
  }

  void busbar::add_to_end_of_transmission_line(transmission_line_ tl) const {
    auto me = shared_from_this();
    if (!me)
      throw std::runtime_error("this busbar is not associated with a network");
    tl->to_bb = me;
  }

  void busbar::add_to_market_area(energy_market_area_ ma) const {
    auto me = shared_from_this();
    if (!me)
      throw std::runtime_error("this busbar is not associated with a network");
    if (!any_of(ma->busbars.begin(), ma->busbars.end(), [me](const busbar_ b) {
          return b == me;
        })) { // don't add duplicates
      ma->busbars.push_back(me);
    }
  }

  unit_member::unit_member() {
    mk_url_fx(this);
  }

  unit_member::unit_member(busbar* b, unit_ const & u, apoint_ts const & a)
    : owner{b}
    , unit{u}
    , active{a} {
    mk_url_fx(this);
  }

  int64_t unit_member::id() const {
    return unit ? unit->id : 0;
  }

  bool unit_member::operator==(unit_member const & other) const {
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
             mp::leaf_accessors(hana::type_c<unit_member>),
             true, // initial value of the fold
             [this, &other](bool s, auto&& a) {
               return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                        : false; // only evaluate equal if the fold state is still true
             })
        && (unit == other.unit
            || (unit && other.unit && (*unit == *other.unit))); // require pointers to be equal in this case
  }

  void unit_member::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      if (owner)
        owner->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/M{member_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/M" + (unit ? std::to_string(unit->id) : std::string("?"));
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  power_module_member::power_module_member() {
    mk_url_fx(this);
  }

  power_module_member::power_module_member(busbar* b, power_module_ const & pm, apoint_ts const & a)
    : owner{b}
    , power_module{pm}
    , active{a} {
    mk_url_fx(this);
  }

  int64_t power_module_member::id() const {
    return power_module ? power_module->id : 0;
  }

  bool power_module_member::operator==(power_module_member const & other) const {
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
             mp::leaf_accessors(hana::type_c<power_module_member>),
             true, // initial value of the fold
             [this, &other](bool s, auto&& a) {
               return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                        : false; // only evaluate equal if the fold state is still true
             })
        && (power_module == other.power_module
            || (power_module && other.power_module && (*power_module == *other.power_module))); // require pointers to
                                                                                                // be equal in this case
  }

  void
    power_module_member::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      if (owner)
        owner->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/P{member_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/P" + (power_module ? std::to_string(power_module->id) : std::string("?"));
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  wind_farm_member::wind_farm_member() {
    mk_url_fx(this);
  }

  wind_farm_member::wind_farm_member(busbar* b, std::shared_ptr<wind_farm> const & t, apoint_ts const & a)
    : owner{b}
    , farm{t}
    , active{a} {
    mk_url_fx(this);
  }

  int64_t wind_farm_member::id() const {
    return farm ? farm->id : 0;
  }

  bool wind_farm_member::operator==(wind_farm_member const & other) const {
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
             mp::leaf_accessors(hana::type_c<wind_farm_member>),
             true, // initial value of the fold
             [this, &other](bool s, auto&& a) {
               return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                        : false; // only evaluate equal if the fold state is still true
             })
        && (farm == other.farm
            || (farm && other.farm && (*farm == *other.farm))); // require pointers to be equal in this case
  }

  void wind_farm_member::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      if (owner)
        owner->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/W{member_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/W" + (farm ? std::to_string(farm->id) : std::string("?"));
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  void busbar::add_unit(unit_ u, apoint_ts const & active_ts) {
    for (auto const & un : units) {
      if (*un->unit == *u) {
        throw std::runtime_error("busbar units: unit '" + un->unit->name + "' already a member");
      }
    }

    units.emplace_back(make_shared<unit_member>(this, u, active_ts));

    for (auto const & ma : get_market_areas()) {
      if (auto ug = ma->get_busbar_derived_unit_group(); ug) {
        ug->add_unit(u, active_ts);
        ug->update_sum_expressions();
      }
    }
  }

  void busbar::remove_unit(unit_ u) {
    auto f = std::find_if(std::begin(units), std::end(units), [&u](auto const & x) {
      return x->unit.get() == u.get();
    });
    if (f != std::end(units)) {
      units.erase(f);
    }
    for (auto const & ma : get_market_areas()) {
      if (auto ug = ma->get_busbar_derived_unit_group(); ug) {
        ug->remove_unit(u);
        ug->update_sum_expressions();
      }
    }
  }

  void busbar::add_power_module(power_module_ p, apoint_ts const & active_ts) {
    for (auto const & pm : power_modules) {
      if (*pm->power_module == *p) {
        throw std::runtime_error("busbar power_module: unit '" + pm->power_module->name + "' already a member");
      }
    }

    power_modules.emplace_back(make_shared<power_module_member>(this, p, active_ts));
    // update_sum_expressions();
  }

  void busbar::remove_power_module(power_module_ p) {
    auto f = std::find_if(std::begin(power_modules), std::end(power_modules), [&p](auto const & x) {
      return x->power_module.get() == p.get();
    });
    if (f != std::end(power_modules)) {
      power_modules.erase(f);
    }
    // update_sum_expressions();
  }

  void busbar::add_wind_farm(std::shared_ptr<wind_farm> t, apoint_ts const & active_ts) {
    for (auto const & wt : wind_farms) {
      if (*wt->farm == *t) {
        throw std::runtime_error("busbar wind_farm:'" + wt->farm->name + "' already a member");
      }
    }

    wind_farms.emplace_back(std::make_shared<wind_farm_member>(this, t, active_ts));
  }

  void busbar::remove_wind_farm(std::shared_ptr<wind_farm> p) {
    auto f = std::find_if(std::begin(wind_farms), std::end(wind_farms), [&p](auto const & x) {
      return x->farm.get() == p.get();
    });
    if (f != std::end(wind_farms)) {
      wind_farms.erase(f);
    }
  }
}
