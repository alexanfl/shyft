#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/busbar.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  void power_module::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_system>(sys_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/P{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/P" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  bool power_module::operator==(power_module const & o) const {
    if (this == &o)
      return true; // equal by addr.

    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
      mp::leaf_accessors(hana::type_c<power_module>),
      super::operator==(o), // initial value of the fold
      [this, &o](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a))
                 : false; // only evaluate equal if the fold state is still true
      });
  }
}
