#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <string>
#include <memory>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/mp.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/formatters.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::time_series::dd::apoint_ts;

  struct power_plant : hydro_power::power_plant {
    using super = hydro_power::power_plant;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    power_plant(int id, std::string const & name, std::string const & json, std::shared_ptr<stm_hps> const & hps);
    power_plant();

    bool operator==(power_plant const & other) const;

    bool operator!=(power_plant const & other) const {
      return !(*this == other);
    }

    static void add_unit(std::shared_ptr<power_plant> const & ps, std::shared_ptr<unit> const & a);
    void remove_unit(std::shared_ptr<unit> const & a);

    struct production_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        production_,
        (apoint_ts, constraint_min),  ///< W
        (apoint_ts, constraint_max),  ///< W
        (apoint_ts, schedule),        ///< W
        (apoint_ts, realised),        ///< W, the sum of unit.production.realised
        (apoint_ts, merge_tolerance), ///< W, Max deviation in production plan
        (apoint_ts, ramping_up),      ///< W, Constraint of ramping up production
        (apoint_ts, ramping_down),    ///< W, Constraint of ramping down production
        (apoint_ts, result),          ///< W
        (apoint_ts, instant_max)      ///< W instant max production of rotating units
      );
      SHYFT_DEFINE_STRUCT(
        production_,
        (),
        (constraint_min,
         constraint_max,
         schedule,
         realised,
         merge_tolerance,
         ramping_up,
         ramping_down,
         result,
         instant_max));
    };

    struct discharge_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        discharge_,
        (apoint_ts, constraint_min),
        (apoint_ts, constraint_max),
        (apoint_ts, schedule),
        (apoint_ts, result),
        (apoint_ts, realised),                     ///< the sum of all unit.discharge.realised
        (apoint_ts, intake_loss_from_bypass_flag), ////< x: time / y: 0 or 1
        (t_xy_, upstream_level_constraint),        ///< x: m : y: m3/s
        (t_xy_, downstream_level_constraint),      ///< x: m / y: m3/s
        (apoint_ts, ramping_up),                   ///< W, Constraint of ramping up discharge
        (apoint_ts, ramping_down)                  ///< W, Constraint of ramping down discharge
      );
      SHYFT_DEFINE_STRUCT(
        discharge_,
        (),
        (constraint_min,
         constraint_max,
         schedule,
         result,
         realised,
         intake_loss_from_bypass_flag,
         upstream_level_constraint,
         downstream_level_constraint,
         ramping_up,
         ramping_down));
    };

    struct best_profit_ {
      BOOST_HANA_DEFINE_STRUCT(
        best_profit_,
        (t_xy_, discharge),
        (t_xy_, cost_average),
        (t_xy_, cost_marginal),
        (t_xy_, cost_commitment),
        (apoint_ts, dynamic_water_value));
      SHYFT_DEFINE_STRUCT(
        best_profit_,
        (),
        (discharge, cost_average, cost_marginal, cost_commitment, dynamic_water_value));
      url_fx_t url_fx;
    };

    struct fees_ {
      BOOST_HANA_DEFINE_STRUCT(
        fees_,
        (apoint_ts, feeding_fee) ////<  cost of feeding to the grid (Money/J)
      );
      SHYFT_DEFINE_STRUCT(fees_, (), (feeding_fee));
      url_fx_t url_fx;
    };

    BOOST_HANA_DEFINE_STRUCT(
      power_plant,
      (apoint_ts, outlet_level),   ///< masl, (input, or result ?)
      (apoint_ts, gross_head),     ///< meter, gross head between power plant and upstream reservoirs
      (apoint_ts, mip),            ///< bool, opt. with mip
      (apoint_ts, unavailability), ///< bool, ..
      (production_, production),
      (discharge_, discharge),
      (best_profit_, best_profit), /// BP curves
      (fees_, fees));
    SHYFT_DEFINE_STRUCT(
      power_plant,
      (hydro_power::power_plant),
      (outlet_level, gross_head, mip, unavailability, production, discharge, best_profit, fees));

    x_serialize_decl();
  };

  using power_plant_ = std::shared_ptr<power_plant>;
}

x_serialize_export_key(shyft::energy_market::stm::power_plant);
BOOST_CLASS_VERSION(shyft::energy_market::stm::power_plant, 2);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_plant::production_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_plant::discharge_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_plant::best_profit_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::power_plant::fees_);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::power_plant, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::power_plant, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::power_plant>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::power_plant, Char> { };
