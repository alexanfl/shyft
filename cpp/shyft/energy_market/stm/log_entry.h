#pragma once
#include <cstdint>
#include <string>

#include <fmt/core.h>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

#define SHYFT_STM_LOG_SEVERITY (information, warning, error)

  SHYFT_DEFINE_ENUM(log_severity, std::uint8_t, SHYFT_STM_LOG_SEVERITY)

  struct log_entry {
    log_severity severity;
    std::string message;
    int code;
    shyft::core::utctime time;

    SHYFT_DEFINE_STRUCT(log_entry, (), (severity, message, code, time));

    auto operator<=>(log_entry const &) const = default;

    friend void serialize(auto &archive, log_entry &e, unsigned const v) {
      reflection::serialize(archive, e, v);
    }
  };

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::log_severity);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::log_entry, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::stm::log_entry, Char> { };
