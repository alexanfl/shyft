#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/formatters.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::time_series::dd::apoint_ts;

  struct reservoir : hydro_power::reservoir {
    using super = hydro_power::reservoir;

    /** @brief generate an almost unique, url-like string for a reservoir.
     *
     * @param rbi: back inserter to store result
     * @param levels: How many levels of the url to include.
     * 		levels == 0 includes only this level. Use level < 0 to include all levels.
     * @param placeholders: The last element of the vector states wethers to use the reservoir ID
     * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
     * 		If the vector is empty, the function defaults to not using placeholders.
     * @return
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    reservoir(int id, std::string const & name, std::string const & json, stm_hps_ const & hps);

    reservoir() {
      mk_url_fx(this);
    }

    bool operator==(reservoir const & other) const;

    bool operator!=(reservoir const & other) const {
      return !(*this == other);
    }

    std::weak_ptr<reservoir_aggregate> rsv_aggregate;

    auto rsv_aggregate_() const {
      return rsv_aggregate.lock();
    }

    struct level_ {
      struct constraint_ {
        url_fx_t url_fx; // needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min), ///< masl
          (apoint_ts, max)  ///< masl
        );

        SHYFT_DEFINE_STRUCT(constraint_, (), (min, max));
      };

      url_fx_t url_fx; // needed by .url(...) to python exposure
      BOOST_HANA_DEFINE_STRUCT(
        level_,
        (apoint_ts, regulation_min), ///< masl
        (apoint_ts, regulation_max), ///< masl
        (apoint_ts, realised),       ///< masl
        (apoint_ts, schedule),       ///< masl
        (apoint_ts, result),         ///< masl
        (constraint_, constraint));

      SHYFT_DEFINE_STRUCT(level_, (), (regulation_min, regulation_max, realised, schedule, result, constraint));
    };

    struct volume_ {
      struct constraint_ {
        struct tactical_ {
          url_fx_t url_fx; // needed to link up py wrapped url paths
          BOOST_HANA_DEFINE_STRUCT(
            tactical_,
            (penalty_constraint, min), ///< m3
            (penalty_constraint, max)  ///< m3
          );

          SHYFT_DEFINE_STRUCT(tactical_, (), (min, max));
        };

        url_fx_t url_fx; // needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min), ///< m3
          (apoint_ts, max), ///< m3
          (tactical_, tactical));
        SHYFT_DEFINE_STRUCT(constraint_, (), (min, max, tactical));
      };

      struct slack_ {
        url_fx_t url_fx; // needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(
          slack_,
          (apoint_ts, lower), ///< m3
          (apoint_ts, upper)  ///< m3
        );
        SHYFT_DEFINE_STRUCT(slack_, (), (lower, upper));
      };

      struct cost_ {
        struct cost_curve_ {
          url_fx_t url_fx; // needed to link up py wrapped url paths
          BOOST_HANA_DEFINE_STRUCT(
            cost_curve_,
            (t_xy_, curve),      ///< m3 -> money/m3
            (apoint_ts, penalty) ///< money
          );
          SHYFT_DEFINE_STRUCT(cost_curve_, (), (curve, penalty));
        };

        url_fx_t url_fx; // needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(cost_, (cost_curve_, flood), (cost_curve_, peak));
        SHYFT_DEFINE_STRUCT(cost_, (), (flood, peak));
      };

      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        volume_,
        (apoint_ts, static_max), ///< m3
        (apoint_ts, schedule),   ///< m3
        (apoint_ts, realised),   ///< m3
        (apoint_ts, result),     ///< m3
        (apoint_ts, penalty),    ///< m3
        (constraint_, constraint),
        (slack_, slack),
        (cost_, cost));

      SHYFT_DEFINE_STRUCT(volume_, (), (static_max, schedule, realised, result, penalty, constraint, slack, cost));
    };

    struct inflow_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(
        inflow_,
        (apoint_ts, schedule), ///< m3/s
        (apoint_ts, realised), ///< m3/s
        (apoint_ts, result)    ///< m3/s
      );
      SHYFT_DEFINE_STRUCT(inflow_, (), (schedule, realised, result));
    };

    struct water_value_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths

      struct result_ {
        url_fx_t url_fx; // needed to link up py wrapped url paths
        BOOST_HANA_DEFINE_STRUCT(
          result_,
          (apoint_ts, local_volume),  ///< money/m3
          (apoint_ts, global_volume), ///< money/m3
          (apoint_ts, local_energy),  ///< money/joule
          (apoint_ts, end_value)      ///< money
        );
        SHYFT_DEFINE_STRUCT(result_, (), (local_volume, global_volume, local_energy, end_value))
      };

      BOOST_HANA_DEFINE_STRUCT(
        water_value_,
        (apoint_ts, endpoint_desc), ///< money/joule
        (result_, result));
      SHYFT_DEFINE_STRUCT(water_value_, (), (endpoint_desc, result));
    };

    struct ramping_ {
      url_fx_t url_fx; // needed to link up py wrapped url paths
      BOOST_HANA_DEFINE_STRUCT(ramping_, (apoint_ts, level_down), (apoint_ts, level_up));
      SHYFT_DEFINE_STRUCT(ramping_, (), (level_down, level_up));
    };

    BOOST_HANA_DEFINE_STRUCT(
      reservoir,
      (t_xy_, volume_level_mapping),
      (level_, level),
      (volume_, volume),
      (inflow_, inflow),
      (ramping_, ramping),
      (water_value_, water_value));
    SHYFT_DEFINE_STRUCT(
      reservoir,
      (hydro_power::reservoir),
      (volume_level_mapping, level, volume, inflow, ramping, water_value));
    x_serialize_decl();
  };

  using reservoir_ = std::shared_ptr<reservoir>;
}

x_serialize_export_key(shyft::energy_market::stm::reservoir);
BOOST_CLASS_VERSION(shyft::energy_market::stm::reservoir, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::level_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::level_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::volume_::constraint_::tactical_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::volume_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::volume_::slack_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::volume_::cost_::cost_curve_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::volume_::cost_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::inflow_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::water_value_::result_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::water_value_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::ramping_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::reservoir::volume_);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::reservoir, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::reservoir, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::reservoir>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::reservoir, Char> { };
