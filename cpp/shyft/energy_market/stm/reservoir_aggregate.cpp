#include <shyft/energy_market/stm/reservoir_aggregate.h>

#include <string>
#include <vector>

#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::stm {

  reservoir_aggregate::reservoir_aggregate() {
    mk_url_fx(this);
  }

  reservoir_aggregate::reservoir_aggregate(
    int id,
    std::string const & name,
    std::string const & json,
    std::shared_ptr<stm_hps> const & hps)
    : id_base{id, name, json, {}, {}, {}}
    , hps(hps) {
    mk_url_fx(this);
  }

  reservoir_aggregate::~reservoir_aggregate() {
    for (auto& a : reservoirs)
      if (a)
        a->rsv_aggregate.reset();
  }

  std::shared_ptr<reservoir_aggregate> reservoir_aggregate::shared_from_this() const {
    auto hps = hps_();
    return hps ? hydro_power::shared_from_me(this, hps->reservoir_aggregates) : nullptr;
  }

  void reservoir_aggregate::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels)
    const {
    if (levels) {
      auto tmp = std::dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }

    if (!template_levels) {
      constexpr std::string_view a = "/A{o_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/A" + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }

  void reservoir_aggregate::add_reservoir(
    std::shared_ptr<reservoir_aggregate> const & ra,
    std::shared_ptr<reservoir> const & r) {
    auto rr = ra->shared_from_this();
    if (r && rr && rr->hps_() == r->hps_()) {
      auto f = std::ranges::find(rr->reservoirs, r);
      if (f != std::ranges::end(rr->reservoirs))
        throw std::runtime_error("reservoir  '" + r->name + "'already added to reservoir aggregate '" + rr->name);
      if (r->rsv_aggregate_())
        throw std::runtime_error(
          "reservoir  '" + r->name + "'already added to anoter reservoir aggregate '" + r->rsv_aggregate_()->name);
      rr->reservoirs.push_back(r);
      r->rsv_aggregate = rr;
    } else {
      throw std::runtime_error(
        "reservoir aggregate '" + std::string(ra ? ra->name : std::string("null")) + "' and reservoir '"
        + std::string(r ? r->name : std::string("null"))
        + "' must be non-null objects, and both belong to the same existing hydro-power-system :"
        + std::string(ra && ra->hps_() ? ra->hps_()->name : std::string("null")));
    }
  }

  void reservoir_aggregate::remove_reservoir(reservoir_ const & r) {
    auto f = std::ranges::find_if(reservoirs, [&r](auto const & p) {
      return r == p;
    });
    if (f != std::ranges::end(reservoirs)) {
      (*f)->rsv_aggregate.reset();
      reservoirs.erase(f);
    }
  }

  bool reservoir_aggregate::operator==(reservoir_aggregate const & other) const {
    if (this == &other)
      return true; // equal by addr.
    auto equal_attributes = hana::fold(
      mp::leaf_accessors(hana::type_c<reservoir_aggregate>),
      id_base::operator==(other),
      [this, &other](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)) : false;
      });
    // since r-aggretate does not own reservoirs, we could stop at checking
    // shallow identity match
    return equal_attributes && equal_vector_ptr_content<reservoir>(reservoirs, other.reservoirs);
  }

}
