#pragma once

#include <memory>
#include <unordered_map>
#include <ranges>
#include <set>
#include <stdexcept>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/is_cyclic.h>
#include <shyft/time_series/dd/resolve_ts.h>

namespace shyft::energy_market::stm {

  struct evaluate_ts_error {
    std::string what;
    SHYFT_DEFINE_STRUCT(evaluate_ts_error, (), (what))

    auto operator<=>(evaluate_ts_error const &) const = default;

    static auto cyclic_expression(std::string_view ts_as_str) {
      return evaluate_ts_error(fmt::format("ts {} contains a cyclic expression", ts_as_str));
    }

    static auto dstm_not_resolved(std::string_view ts_as_str) {
      return evaluate_ts_error(fmt::format("dstm timeseries {} did not resolve", ts_as_str));
    }

    static auto dstm_not_resolved(std::string_view ts_as_str, std::string_view err) {
      return evaluate_ts_error(fmt::format("dstm timeseries {} did not resolve with error: {}", ts_as_str, err));
    }

    static auto dtss_throws(std::string_view err) {
      return evaluate_ts_error(fmt::format("dtss get timeseries failed with error: {}", err));
    }

    // FIXME: find a less intrusive way of exposing errors:
    static auto dtss_not_resolved(std::string_view url, std::string_view err) {
      return evaluate_ts_error(fmt::format("dtss timeseries with url {} did not resolve with error: {}", url, err));
    }

    auto &extend_dtss_not_resolved(std::string_view url, std::string_view err) {
      what.append(fmt::format(", dtss timeseries with url {} did not resolve with error: {}", url, err));
      return *this;
    }

    template <typename A>
    friend void serialize(A &archive, evaluate_ts_error &m, unsigned int const v) {
      reflection::serialize(archive, m, v);
    }
  };

  template <typename T>
  inline constexpr bool is_evaluate_ts_error_v = std::is_same_v<std::remove_cvref_t<T>, evaluate_ts_error>;

  using evaluate_ts_result = std::variant<time_series::dd::apoint_ts, evaluate_ts_error>;

  template <typename T>
  requires std::same_as<std::remove_cvref_t<T>, evaluate_ts_result>
  constexpr auto &&evaluate_ts_get(T &&t) {
    auto p = std::get_if<time_series::dd::apoint_ts>(&t);
    if (!p)
      throw std::runtime_error(std::get<evaluate_ts_error>(SHYFT_FWD(t)).what);
    return std::forward<qual_like_t<time_series::dd::apoint_ts, T &&>>(*p);
  }

  namespace detail {

    template <typename GetAttr>
    struct evaluate_ts_impl_resolver {

      struct ts_hash {
        using is_transparent = void;

        template <typename T>
        auto operator()(T const &t) const {
          return std::hash<T>{}(t);
        }
      };

      using ts_ptr = std::shared_ptr<time_series::dd::ipoint_ts const> const *;
      using deferred_dtss_resolves_type = std::unordered_map<
        std::string,
        std::vector<std::pair<ts_ptr, std::variant<apoint_ts, evaluate_ts_error> *>>,
        ts_hash,
        std::equal_to<>>;
      using cached_dstm_resolves_type =
        std::unordered_map<std::string, std::shared_ptr<time_series::dd::ipoint_ts const>, ts_hash, std::equal_to<>>;

      GetAttr get_attr;
      // shared_models<Executor> &models;
      deferred_dtss_resolves_type deferred_dtss_resolves{};
      cached_dstm_resolves_type cached_dstm_resolves{};

      void defer_dtss_resolve(
        std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url,
        evaluate_ts_result *res) {
        auto it = deferred_dtss_resolves.find(url);
        if (it == deferred_dtss_resolves.end()) {
          deferred_dtss_resolves.insert({std::string(url), {{std::addressof(ref), res}}});
        } else {
          it->second.push_back({std::addressof(ref), res});
        }
      }

      auto get_ts_or_throw(std::string_view url) {
        auto result = get_attr(url);
        auto visitor = []<typename T>(T &&v) -> std::shared_ptr<time_series::dd::ipoint_ts const> {
          if constexpr (shyft::energy_market::stm::is_url_resolve_error_v<T>) {
            throw std::runtime_error(v.what);
          } else {
            static_assert(shyft::energy_market::stm::is_any_attr_v<T>);
            return std::get<time_series::dd::apoint_ts>(v).ts;
          }
        };
        return std::visit(visitor, result);
      }

      std::shared_ptr<time_series::dd::ipoint_ts const> *try_dstm_resolve(
        [[maybe_unused]] std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url) {
        if (stm::url_peek_model_id(url).empty())
          return nullptr;
        if (auto already_resolved = cached_dstm_resolves.find(url); already_resolved != cached_dstm_resolves.end())
          return std::addressof(already_resolved->second);
        auto result = get_ts_or_throw(url);
        if (result && result->needs_bind())
          result = result->clone_expr();
        return std::addressof(cached_dstm_resolves.emplace(url, result).first->second);
      }

      using ignore_dtss_urls_t = std::false_type;
      using defer_dtss_urls_t = std::true_type;
      static constexpr ignore_dtss_urls_t ignore_dtss_urls{};
      static constexpr defer_dtss_urls_t defer_dtss_urls{};

      auto operator()(
        ignore_dtss_urls_t,
        std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url) -> std::shared_ptr<time_series::dd::ipoint_ts const> {
        if (stm::url_peek_model_id(url).empty() || !ref->needs_bind())
          return nullptr;
        if (auto resolved_dstm_ref = try_dstm_resolve(ref, url))
          return *resolved_dstm_ref;
        return ref;
      }

      auto const &operator()(
        defer_dtss_urls_t,
        evaluate_ts_result *res,
        std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url) {
        if (!ref->needs_bind())
          return ref;
        if (auto resolved_dstm_ref = try_dstm_resolve(ref, url))
          return *resolved_dstm_ref;
        defer_dtss_resolve(ref, url, res);
        return ref;
      }
    };

    auto evaluate_ts_impl(
      auto &&dtss,
      auto &&get_attr,
      time_series::dd::ats_vector &tsv,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {

      // FIXME: group tsv, ..., clip_period params into a dtss-query struct or similar - jeh

      // Collect results
      std::vector<evaluate_ts_result> result(tsv.size(), time_series::dd::apoint_ts{});
      // view containing tsv with result and index
      auto tsv_and_res = std::views::zip(tsv, result);
      evaluate_ts_impl_resolver resolver{.get_attr = SHYFT_FWD(get_attr)};
      for (auto &&[ts, res] : tsv_and_res) {
        try {
          if (time_series::dd::is_cyclic(std::bind_front(std::ref(resolver), resolver.ignore_dtss_urls), ts)) {
            res = evaluate_ts_error::cyclic_expression(ts.stringify());
            continue;
          }
          if (!time_series::dd::resolve_ts(std::bind_front(std::ref(resolver), resolver.defer_dtss_urls, &res), ts)) {
            res = evaluate_ts_error::dstm_not_resolved(ts.stringify());
          }
        } catch (std::runtime_error const &e) {
          res = evaluate_ts_error::dstm_not_resolved(ts.stringify(), e.what());
        }
      }

      // resolve one by one, getting errors
      std::vector<std::string> deferred_dtss_urls(resolver.deferred_dtss_resolves.size());
      std::ranges::copy(std::views::keys(resolver.deferred_dtss_resolves), deferred_dtss_urls.data());
      try {
        auto dtss_tsv = dtss(deferred_dtss_urls, bind_period, use_ts_cached_read, update_ts_cache);
        // FIXME: assume precondition when changing underlying api
        if (dtss_tsv.size() != deferred_dtss_urls.size())
          throw std::runtime_error("dtss did not return same size of ts as requested");

        for (auto &&[dtss_ts, deferred_dtss_resolves] :
             std::views::zip(dtss_tsv, std::views::values(resolver.deferred_dtss_resolves))) {
          for (auto &&dtss_ts_ref : std::views::elements<0>(deferred_dtss_resolves)) {
            auto rts = time_series::dd::ts_as_mutable<time_series::dd::aref_ts>(*dtss_ts_ref);
            if (!rts) {
              throw std::runtime_error("resolver tries to assign dtss to non ref");
            }
            auto as_gpoint = std::dynamic_pointer_cast<shyft::time_series::dd::gpoint_ts const>(dtss_ts.ts);
            if (!as_gpoint)
              throw std::runtime_error("dtss returns non gpoint from read");
            rts->rep = std::move(as_gpoint);
          }
        }

      } catch (std::runtime_error const &error) {
        for (auto &&[dtss_url, deferred_dtss_resolves] : resolver.deferred_dtss_resolves) {
          // FIXME: return only relevant error when dtss api is fixed
          auto eval_err = evaluate_ts_error::dtss_throws(error.what());
          for (auto &&res_ptr : std::views::elements<1>(deferred_dtss_resolves))
            (*res_ptr) = eval_err;
        }
      }

      // currently valid ones:
      auto valid_tsv_and_res = std::views::filter(tsv_and_res, [&](auto &&v) {
        auto &&[_1, res] = v;
        return std::holds_alternative<time_series::dd::apoint_ts>(res);
      });

      auto valid_inputs = std::views::elements<0>(valid_tsv_and_res);

      // this part should never fail if we caught all the errors before
      std::ranges::for_each(valid_inputs, &time_series::dd::apoint_ts::do_bind);
      time_series::dd::ats_vector tsv_results{
        time_series::dd::deflate_ts_vector<time_series::dd::apoint_ts>(valid_inputs)};
      if (clip_period.valid())
        tsv_results = clip_to_period(tsv_results, clip_period);

      std::ranges::move(
        tsv_results, std::ranges::begin(std::views::elements<1>(std::ranges::ref_view{valid_tsv_and_res})));

      return result;
    }

  }

  /**
   * @brief Evaluate time series expressions.
   * @param dtss a dtss resolver
   * @param model the dstm model that might be referred to in expressions
   * @param tsv the time series to evaluate, is mutated by this function
   * @param bind_period the period to read from dtss
   * @param use_ts_cached_read allow reading results from already existing cached results
   * @param update_ts_cache when reading, also update the ts-cache with the results
   * @param clip_period the period to clip the results against
   * @returns vector of evaluated time series
   */
  auto evaluate_ts(
    auto &&dtss,
    stm_system const &model,
    time_series::dd::ats_vector &tsv,
    utcperiod bind_period,
    bool use_ts_cached_read = true,
    bool update_ts_cache = false,
    utcperiod clip_period = {}) {

    return detail::evaluate_ts_impl(
      SHYFT_FWD(dtss),
      [&](std::string_view url) {
        return url_try_get_attr(model, url);
      },
      tsv,
      std::move(bind_period),
      use_ts_cached_read,
      update_ts_cache,
      std::move(clip_period));
  }

  /**
   * @brief Evaluate time series expressions.
   * @param dtss a dtss resolver
   * @param models the dstm models that might be referred to in expressions
   * @param tsv the time series to evaluate, is mutated by this function
   * @param bind_period the period to read from dtss
   * @param use_ts_cached_read allow reading results from already existing cached results
   * @param update_ts_cache when reading, also update the ts-cache with the results
   * @param clip_period the period to clip the results against
   * @returns vector of evaluated time series
   */
  template <typename Executor>
  auto evaluate_ts(
    auto &&dtss,
    shared_models<Executor> &models,
    time_series::dd::ats_vector &tsv,
    utcperiod bind_period,
    bool use_ts_cached_read = true,
    bool update_ts_cache = false,
    utcperiod clip_period = {}) {
    return detail::evaluate_ts_impl(
      SHYFT_FWD(dtss),
      [&](std::string_view url) {
        return get_attrs(models, std::views::single(url)).at(0);
      },
      tsv,
      std::move(bind_period),
      use_ts_cached_read,
      update_ts_cache,
      std::move(clip_period));
  }

}

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::evaluate_ts_error, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::stm::evaluate_ts_error, Char> { };
