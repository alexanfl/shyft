#pragma once

#include <algorithm>
#include <array>
#include <cctype>
#include <charconv>
#include <optional>
#include <string_view>
#include <string>
#include <tuple>
#include <variant>
#include <vector>

#include <boost/hof/fix.hpp>
#include <fmt/core.h>

namespace shyft::energy_market::stm {

  inline constexpr char dstm_schema[] = "dstm://";
  inline constexpr char dtss_schema[] = "shyft://";
  inline constexpr char model_token = 'M';
  inline constexpr char any_delimiter[] = "./";
  inline constexpr char component_delimiter = '/';
  inline constexpr char attr_delimiter = '.';
  inline constexpr char ts_attr_prefix[] = "ts.";
  inline constexpr char custom_attr_prefix[] = "custom.";

  /**
   * @brief struct to expose resulting url in dtss schema
   */
  struct url_dtss_schema {
    std::string_view url;
  };

  /**
   * @brief struct to expose remainder of url in dstm schema, with the schema stripped
   */
  struct url_dstm_schema {
    std::string_view remainder;
  };

  template <typename F>
  concept schema_invokable = std::invocable<F, url_dtss_schema> && std::invocable<F, url_dstm_schema>;

  /**
   * @brief struct to expose model name in dstm url, and the remainder of the url with the model and model name stripped
   */
  struct url_parsed_model {
    std::string_view name;
    std::string_view remainder;
  };

  template <typename F>
  concept model_invokable = std::invocable<F, url_parsed_model>;

  /**
   * @brief struct to hold component type and id coded as std::uint8_t (char) and int64_t, like U123 for unit
   */
  struct c_id {
    std::uint8_t tp{0};
    std::int64_t id{0}; ///< the id could be 64bits, and ms c++ is 32bits on int, need to be specific

    auto operator<=>(c_id const &) const = default;
  };

  /**
   * @brief struct to hold c_id for a given component, and expose the remainder of the url with the component stripped
   */
  struct url_parsed_component {
    c_id comp;
    std::string_view remainder;
  };

  template <typename F>
  concept component_invokable = std::invocable<F, url_parsed_component>;

  /**
   * @brief struct to expose the attrs of the url
   */
  struct url_parsed_attrs {
    std::string_view attrs;
  };

  template <typename F>
  concept attrs_invokable = std::invocable<F, url_parsed_attrs>;

  /**
   * @brief struct to expose the attrs of the url
   */
  struct url_parse_error {
    std::string what;

    static url_parse_error schema(std::string_view url) {
      return url_parse_error(
        fmt::format("url {} starts neither with dstm schema ({}) or dtss schema ({})", url, dstm_schema, dtss_schema));
    }

    static url_parse_error model() {
      return url_parse_error(url_parse_error(fmt::format("model needs to start with {}", model_token)));
    }

    static url_parse_error no_attr_comp() {
      return url_parse_error(fmt::format("does not contain any attr or component"));
    }

    static url_parse_error invalid_attr(std::string_view url) {
      return url_parse_error(fmt::format("{} is not a valid attribute", url));
    }

    static url_parse_error invalid_comp_char(std::string_view url) {
      return url_parse_error(fmt::format("{} does not start with a single component letter", url));
    }

    static url_parse_error no_component_id(std::string_view url) {
      return url_parse_error(fmt::format("{} does not start with a number", url));
    }

    static url_parse_error oversized_component_id(std::string_view url) {
      return url_parse_error(fmt::format("{} number is bigger then representable in an int64", url));
    }

    static url_parse_error missing_delimiter(std::string_view url) {
      return url_parse_error(fmt::format("{} does not start with an attribute or component delimiter", url));
    }
  };

  template <typename F>
  concept error_invokable = std::invocable<F, url_parse_error>;

  constexpr bool url_is_attr(std::string_view url) {

    auto is_word = [](char c) {
      return std::isalpha(c) || c == '_';
    };
    auto is_wordnum = [](char c) {
      return std::isalnum(c) || c == '_';
    };
    auto is_separator = [](char c) {
      return c == '.';
    };
    auto is_print = [](char c) {
      return std::isprint(c);
    };

    // deal with custom attributes.
    if (url.starts_with(ts_attr_prefix)) {
      url.remove_prefix(std::size(ts_attr_prefix) - 1);
      return !url.empty() && std::ranges::all_of(url, is_print);
    }

    if (url.starts_with(custom_attr_prefix)) {
      url.remove_prefix(std::size(custom_attr_prefix) - 1);
      return !url.empty() && std::ranges::all_of(url, is_print);
    }

    // parse '.' separated strings of identifier like segments.
    // they must start with a word-like character, and be followed
    // by zero or more word-like or numeric characters.
    auto it = url.begin(), end = url.end();
    while (it != end && is_word(*it)) {
      it = std::ranges::find_if_not(++it, end, is_wordnum);
      if (it == end)
        return true;
      else if (!is_separator(*it))
        return false;
      ++it;
    }
    return false;
  }

  constexpr auto url_parse(std::string_view url, auto success, error_invokable auto error) {
    if (url.starts_with(dstm_schema)) {
      url.remove_prefix(std::size(dstm_schema) - 1);
      return success(url_dstm_schema(url));
    }
    if (url.starts_with(dtss_schema)) {
      return success(url_dtss_schema(url));
    }
    return error(url_parse_error::schema(url));
  }

  constexpr auto url_parse_model(std::string_view url, model_invokable auto success, error_invokable auto error) {
    if (!url.starts_with(model_token)) {
      return error(url_parse_error::model());
    }
    url.remove_prefix(1);
    auto end = url.find_first_of(any_delimiter);
    auto name = url.substr(0, end);
    return success(url_parsed_model(name, url.substr(end)));
  }

  constexpr auto url_parse_components_or_attrs(
    std::string_view url,
    component_invokable auto successComponent,
    attrs_invokable auto success_attrs,
    error_invokable auto error) {
    if (url.empty()) {
      return error(url_parse_error::no_attr_comp());
    }
    if (url[0] == attr_delimiter) { // is attr, return rest
      url.remove_prefix(1);
      if (!url_is_attr(url)) {
        return error(url_parse_error::invalid_attr(url));
      }
      return success_attrs(url_parsed_attrs(url));
    }
    if (url[0] == component_delimiter) {
      // interpret rest as component:
      url.remove_prefix(1);
      if (!std::isalpha(static_cast<unsigned char>(url[0]))) { // first letter is not [a-zA-Z]
        return error(url_parse_error::invalid_comp_char(url));
      }
      c_id comp(static_cast<std::uint8_t>(url[0]), 0);
      url.remove_prefix(1);
      auto [ptr, ec] = std::from_chars(url.data(), url.data() + url.size(), comp.id);
      if (ptr == url.data()) {
        return error(url_parse_error::no_component_id(url));
      }
      if (ec == std::errc::result_out_of_range) {
        return error(url_parse_error::oversized_component_id(url));
      }
      url.remove_prefix(ptr - url.data());
      return successComponent(url_parsed_component(comp, url));
    }
    return error(url_parse_error::missing_delimiter(url));
  }

  using url_tuple = std::tuple<std::string_view, std::vector<c_id>, std::string_view>;
  using url_result = std::variant<url_tuple, url_parse_error>;

  template <typename T>
  inline constexpr bool is_url_tuple_v = std::is_same_v<std::remove_cvref_t<T>, url_tuple>;

  template <typename T>
  inline constexpr bool is_url_err_v = std::is_same_v<std::remove_cvref_t<T>, url_parse_error>;

  static url_result url_parse_to_result(std::string_view url) {
    std::optional<url_parse_error> err;
    std::string_view model, attrs;
    std::vector<c_id> cid_res;
    auto error_dispatcher = [&](url_parse_error const &e) {
      err = e;
    };
    auto dispatcher_a = [&](url_parsed_attrs const &d) {
      attrs = d.attrs;
    };
    auto dispatcher_c = boost::hof::fix([&](auto self, url_parsed_component const &d) -> void {
      cid_res.push_back(d.comp);
      url_parse_components_or_attrs(d.remainder, self, dispatcher_a, error_dispatcher);
    });

    url_parse(
      url,
      [&]<typename T>(T const &d) {
        if constexpr (std::is_same_v<T, url_dtss_schema>) {
          error_dispatcher(url_parse_error(fmt::format("url_dtss_schema with url {}", d.url)));
        } else {
          static_assert(std::is_same_v<T, url_dstm_schema>);
          url_parse_model(
            d.remainder,
            [&](const url_parsed_model &m) {
              model = m.name;
              url_parse_components_or_attrs(m.remainder, dispatcher_c, dispatcher_a, error_dispatcher);
            },
            error_dispatcher);
        }
      },
      error_dispatcher);
    if (err) {
      return *err;
    }
    return std::make_tuple(model, cid_res, attrs);
  }
}

template <>
struct fmt::formatter<shyft::energy_market::stm::c_id> {
  constexpr auto parse(auto &ctx) {
    return ctx.begin();
  }

  template <typename FormatContext>
  auto format(shyft::energy_market::stm::c_id const &v, FormatContext &ctx) const {
    return fmt::format_to(ctx.out(), "{}{}", (char) v.tp, v.id);
  }
};
