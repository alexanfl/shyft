#pragma once

#include <iterator>
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/mp.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {

  using shyft::time_series::dd::apoint_ts;

  /**
   * @brief Transmission line
   *
   * @details The transmission line connecting busbars
   */
  struct transmission_line : id_base {
    using super = id_base;

    transmission_line() {
      mk_url_fx(this);
    }

    transmission_line(int id, std::string const & name, std::string const & json, network_ const & net)
      : super{id, name, json, {}, {}, {}}
      , net{net} {
      mk_url_fx(this);
    }

    bool operator==(transmission_line const & other) const;

    bool operator!=(transmission_line const & other) const {
      return !(*this == other);
    }

    /** @brief generate an almost unique, url-like string for a reservoir.
     *
     * @param rbi: back inserter to store result
     * @param levels: How many levels of the url to include.
     * 		levels == 0 includes only this level. Use level < 0 to include all levels.
     * @param placeholders: The last element of the vector states wethers to use the reservoir ID
     * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
     * 		If the vector is empty, the function defaults to not using placeholders.
     * @return
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    network_ net_() const {
      return net.lock();
    }

    network__ net; ///< Reference up to the 'owning' network.

    BOOST_HANA_DEFINE_STRUCT(
      transmission_line,
      (apoint_ts, capacity) ///< Transmission line capacity, unit W
    );
    SHYFT_DEFINE_STRUCT(transmission_line, (id_base), (capacity));

    busbar_ from_bb; ///< Association busbar at start of transmission line
    busbar_ to_bb;   ///< Association busbar at end of transmission line

    x_serialize_decl();
  };

  using transmission_line_ = std::shared_ptr<transmission_line>;
}

x_serialize_export_key(shyft::energy_market::stm::transmission_line);
BOOST_CLASS_VERSION(shyft::energy_market::stm::transmission_line, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::transmission_line);

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::transmission_line>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::transmission_line, Char> { };
