#include <shyft/energy_market/stm/waterway.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  using std::static_pointer_cast;
  using std::dynamic_pointer_cast;
  using std::make_shared;
  using std::runtime_error;

  gate::gate(int id, string const & name, string const & json)
    : super{id, name, json} {
    mk_url_fx(this);
  }

  gate::gate() {
    mk_url_fx(this);
  }

  void waterway::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }

    if (!template_levels) {
      constexpr std::string_view a = "/W{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/W" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  bool waterway::operator==(waterway const & other) const {
    if (this == &other)
      return true;                      // equal by addr.
    auto equal_attributes = hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s
                                        // c++ , so we use this that seems to be robust cross platform construct
      mp::leaf_accessors(hana::type_c<waterway>),
      id_base::operator==(other), // initial value of the fold
      [this, &other](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                 : false; // only evaluate equal if the fold state is still true
      });

    auto equal_gate_attributes = [this, &other]() {
      return (gates.size() == other.gates.size())
          && std::equal(gates.begin(), gates.end(), other.gates.begin(), [](const auto& a, const auto& b) {
               return a == b || *dynamic_pointer_cast<gate>(a) == *dynamic_pointer_cast<gate>(b);
             });
    };

    return equal_attributes && equal_gate_attributes();
  }

  void gate::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/G{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/G" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  bool gate::operator==(gate const & other) const {
    if (this == &other)
      return true;     // equal by addr.
    return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use
                       // this that seems to be robust cross platform construct
      mp::leaf_accessors(hana::type_c<gate>),
      id_base::operator==(other), // initial value of the fold
      [this, &other](bool s, auto&& a) {
        return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a))
                 : false; // only evaluate equal if the fold state is still true
      });
  }

  /**
   * @brief compute intial accumulated deviation
   * @details In the context of stm::shop, wtr constraints, Given `actual` and `reference` for the flow (m3/s),
   * compute the integral (actual-reference) from common start of the two arguments up to the specified t0.
   * If any of the two series entirely lacks valid values before t0, the 0.0 is returned.
   *
   * @param t0 time to compute the actual deviation for.
   * @param actual flow in m3/s
   * @param reference flow in m3/s
   * @return integral of (actual-reference) dt over interval common t_start to t0
   */
  double initial_acc_deviation(utctime t0, apoint_ts const & actual, apoint_ts const & reference) {
    using shyft::core::seconds;
    using shyft::core::is_valid;
    if (!actual || !reference || !is_valid(t0)) {
      return 0.0; // we need all the above, if any missing, then by def. result is 0.0
    }
    if (actual.total_period().start >= t0 || reference.total_period().start >= t0) {
      return 0.0; // both must start before t0, otherwise 0.0
    }
    auto t_start = min(actual.total_period().start, reference.total_period().start);
    time_axis::generic_dt acc_ta{
      vector<utctime>{t_start, t0},
      t0 + utctime{1}
    };
    auto acc_dev = (actual - reference).accumulate(acc_ta).values()[1]; // pick second value, the first is 0.0 by def.
    return std::isfinite(acc_dev) ? acc_dev : 0.0;
  }
}
