#include <algorithm>
#include <iterator>
#include <string>
#include <string_view>

#include <fmt/core.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::stm {

  bool network::operator==(network const & o) const {
    if (this == &o)
      return true; // equal by addr.
    return super::operator==(o) && equal_vector_ptr_content<transmission_line>(transmission_lines, o.transmission_lines)
        && equal_vector_ptr_content<busbar>(busbars, o.busbars);
  }

  void network::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_system>(sys_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/n{parent_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/n" + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }

  template <class TList>
  static void ensure_unique_id_and_name(std::string const & object_name, TList& list, int id, string const & name) {
    if (any_of(begin(list), end(list), [&name](auto const & w) -> bool {
          return w->name == name;
        }))
      throw stm_rule_exception(
        fmt::format("{} name must be unique within a Network, name '{}' already exists", object_name, name));
    if (any_of(begin(list), end(list), [&id](auto const & w) -> bool {
          return w->id == id;
        }))
      throw stm_rule_exception(
        fmt::format("{} id must be unique within a Network, id {} already exists", object_name, id));
  }

  template <class T, class TList>
  static shared_ptr<T> create_object(
    network_& net,
    std::string const & object_name,
    TList& list,
    int id,
    string const & name,
    string const & json) {
    ensure_unique_id_and_name(object_name, list, id, name);
    auto o = make_shared<T>(id, name, json, net);
    list.push_back(o);
    return o;
  }

  transmission_line_ network_builder::create_transmission_line(int id, string const & name, string const & json) {
    return create_object<transmission_line>(n, "TransmissionLine", n->transmission_lines, id, name, json);
  }

  busbar_ network_builder::create_busbar(int id, string const & name, string const & json) {
    return create_object<busbar>(n, "Busbar", n->busbars, id, name, json);
  }
}
