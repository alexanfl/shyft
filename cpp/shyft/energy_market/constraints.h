/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <boost/preprocessor/stringize.hpp>
#include <boost/serialization/export.hpp>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market {

  // FIXME: legacy support, find migration path - jeh
  struct constraint_base {
    void serialize(auto & /* ar */, unsigned int const /*version*/) {
    }
  };

  /** @brief Class for absolute constraint, where not satisfying the constraint incurs an infinite penalty
   *  This class requires no other data than that from constraint_base
   */
  struct absolute_constraint {
    BOOST_HANA_DEFINE_STRUCT(
      absolute_constraint,
      (time_series::dd::apoint_ts, limit), ///< Time series for the threshold of the constraint (the limit for whether a
                                           ///< value satisfies a constraint or not)
      (time_series::dd::apoint_ts, flag)   ///< Time series specifying when the constraint is active.
    );
    url_fx_t url_fx; // needed to link up py wrapped url paths

    SHYFT_DEFINE_STRUCT(absolute_constraint, (), (limit, flag));

    bool operator==(absolute_constraint const &o) const {
      return limit == o.limit && flag == o.flag;
    }

    auto operator<=>(absolute_constraint const &) const = default;

    friend auto serialize(auto &ar, absolute_constraint &self, unsigned int const version) {
      if (version == 0) {
        using shyft::core::core_nvp;
        constraint_base super{};
        ar &core_nvp("super", super) & core_nvp("limit", self.limit) & core_nvp("flag", self.flag);
      } else
        shyft::reflection::serialize(ar, self, version);
    }
  };

  /** @brief Data class for a constraint, where violating the constraint incurs a finite cost.
   */
  struct penalty_constraint {
    BOOST_HANA_DEFINE_STRUCT(
      penalty_constraint,
      (time_series::dd::apoint_ts, limit), ///< Time series for the threshold of the constraint (the limit for whether a
                                           ///< value satisfies a constraint or not)
      (time_series::dd::apoint_ts, flag),  ///< Time series specifying when the constraint is active.
      (time_series::dd::apoint_ts, cost),  ///< the cost for violating the constraint [money]
      (time_series::dd::apoint_ts, penalty) ///< the penalty pr. unit violation [money/violation-unit]
    );
    url_fx_t url_fx; // needed to link up py wrapped url paths

    SHYFT_DEFINE_STRUCT(penalty_constraint, (), (limit, flag, cost, penalty));

    bool operator==(penalty_constraint const &o) const {
      return limit == o.limit && flag == o.flag && cost == o.cost && penalty == o.penalty;
    }

    auto operator<=>(penalty_constraint const &) const = default;

    friend auto serialize(auto &ar, penalty_constraint &self, unsigned int const version) {
      if (version == 0) {
        // FIXME: legacy support, find a migration path - jeh
        using shyft::core::core_nvp;
        constraint_base super{};
        ar &core_nvp("super", super) & core_nvp("limit", self.limit) & core_nvp("flag", self.flag)
          & core_nvp("cost", self.cost) & core_nvp("penalty", self.penalty);
      } else
        shyft::reflection::serialize(ar, self, version);
    }
  };
}

// FIXME: legacy support, find a migration path - jeh
BOOST_CLASS_EXPORT_KEY2(
  shyft::energy_market::constraint_base,
  BOOST_PP_STRINGIZE(shyft::energy_market::core::constraint_base));
BOOST_CLASS_EXPORT_KEY2(
  shyft::energy_market::absolute_constraint,
  BOOST_PP_STRINGIZE(shyft::energy_market::core::absolute_constraint));
BOOST_CLASS_VERSION(shyft::energy_market::absolute_constraint, 1);
BOOST_CLASS_EXPORT_KEY2(
  shyft::energy_market::penalty_constraint,
  BOOST_PP_STRINGIZE(shyft::energy_market::core::penalty_constraint));
BOOST_CLASS_VERSION(shyft::energy_market::penalty_constraint, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::absolute_constraint);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::penalty_constraint);
