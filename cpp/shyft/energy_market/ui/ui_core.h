#pragma once

#include <memory>
#include <string>

#include <boost/serialization/export.hpp>
#include <fmt/core.h>

#include <shyft/core/formatters.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>

namespace shyft::energy_market::ui {

  /** @intro layout for user-interface
   *
   * The base-class, specifies only the name, or identifier for the
   * layout. We guess that grid is the popular one, but more can
   * be added, e.g:
   *
   * @ref https://developer.android.com/guide/topics/ui/declaring-layout.html
   *
   * Overall strategy for the implementation in ui_core:
   *
   * (1) keep it simple
   * (2) it's going to be exposed to python, so we can use
   *     builder-class (the one users get to build/modify structure)
   *     to ensure user can not create something wrong.
   * (3) we need to serialize it, store it, even versioning, so we use boost for this
   * (4) we use inheritance/composition as suited.
   * (5) it's going to be rendered and forwareded to front end
   *     as json, using boost.karma, and the json-emitter framework on the
   *     web-api. So use types, not enums cleverly.
   *
   * Current approach: Utilize Qt and PyQt to build sufficient UI-wireframe with data-bindings
   *   challenges:
   *       (1) serialization : replace with cached instances of Qt key-value store
   *       (2) databinding   : use proxy widget in e.g.Button in GridView to represent time-series
   */

  /** @brief layout_info
   * The class to be stored on server. Still need to figure out exactly what it should contain.
   */
  struct layout_info {
    std::int64_t id{0}; ///< unique, in the scope of context (type/level) etc.
    std::string name;   ///< the name of the object
    std::string json;   ///< json, to be used by the python-side

    SHYFT_DEFINE_STRUCT(layout_info, (), (id, name, json));

    auto operator<=>(layout_info const &) const = default;

    SHYFT_DEFINE_SERIALIZE(layout_info);
  };
}

BOOST_CLASS_EXPORT_KEY(shyft::energy_market::ui::layout_info);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::ui::layout_info);

template <typename Char>
struct __attribute__((visibility("hidden")))
fmt::formatter<std::shared_ptr<shyft::energy_market::ui::layout_info>, Char>
  : shyft::ptr_formatter<shyft::energy_market::ui::layout_info, Char> { };
