/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#pragma once

#include <cstdint>
#include <string>

#include <boost/serialization/export.hpp>
#include <boost/preprocessor/stringize.hpp>

#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market {

  using shyft::core::utctime;
  using shyft::core::no_utctime;

  enum class run_state : std::int8_t {
    created,
    prepare_input,
    running,
    finished_run,
    reading_results,
    frozed,
    failed
  };

  /** @brief run info, keeps info about a run
   *
   * The model information contains some mandatory fields
   * like
   * id, a unique id for the model, and some other useful
   * or optional stuff
   *
   * This can be shared between models, and the json part of it
   * is there to provide easy scripting flexibility.
   *
   */
  struct run {
    std::int64_t id{0};                  ///< the unique id of the run
    std::string name{};                  ///< optional, often useful name
    utctime created{no_utctime};         ///< or modified, we might be able to keep track of this
    std::string json{};                  ///< optional, for scripting support
    std::int64_t mid{0};                 ///< model-id attached to the run(could be zero initially)
    run_state state{run_state::created}; ///< for now,

    auto operator<=>(run const &) const = default;

    x_serialize_decl();
  };
}

BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::run, BOOST_PP_STRINGIZE(shyft::energy_market::srv::run));
