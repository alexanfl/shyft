/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

namespace shyft::energy_market::market {


  bool model_area::operator==(model_area const &b) const {
    if (!id_base::operator==(b))
      return false;
    model_area const &a = *this;
    if (!equal_map_content(a.power_modules, b.power_modules))
      return false;
    // either same ptr, including both null, or they are non null and content are equal:
    return (a.detailed_hydro == b.detailed_hydro)
        || (a.detailed_hydro && b.detailed_hydro && (*a.detailed_hydro == *b.detailed_hydro));
  }

  bool model_area::equal_structure(model_area const &b) const {
    model_area const &a = *this;
    for (auto const &kv : power_modules) {
      auto f = b.power_modules.find(kv.first);
      if (f == b.power_modules.end())
        return false;
      if (!kv.second->equal_structure(*(f->second)))
        return false;
    }
    return (a.detailed_hydro == b.detailed_hydro)
        || (a.detailed_hydro && b.detailed_hydro && (a.detailed_hydro->equal_structure(*b.detailed_hydro)));
  }

}
