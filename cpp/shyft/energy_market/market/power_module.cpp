/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/market/power_module.h>

namespace shyft::energy_market::market {

  bool power_module::equal_structure(power_module const & b) const {
    return b.id == id; // only require id to be the same
  }

}
