/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <concepts>
#include <stdexcept>
#include <type_traits>
#include <vector>
#include <string_view>
#include <shyft/mp.h>
#include <shyft/mp/type_set_variant.h>
#include <shyft/mp/parser_utils.h>

#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix.hpp>

#include <boost/hana/ext/boost/fusion/tuple.hpp>
#include <boost/fusion/include/tuple.hpp>
#include <boost/proto/proto.hpp>

namespace shyft::mp::grammar {
  using std::vector;
  namespace qi = boost::spirit::qi;
  namespace phx = boost::phoenix;
  namespace fu = boost::fusion;
  namespace proto = boost::proto;
  using namespace hana::literals;


  /**
   * @brief The goal is to automatically generate parsers for hana-structs as well as boost::variant<...types>,
   * provided that parsers are defined for each type in the struct or types..., respectively.
   */
  // Forward declaration of grammar for a hana-struct:
  template <class Struct, class Iterator, class Skipper>
  struct hana_struct_grammar;

  /** @brief We provide a type -> parser map using templates.
   * Thus, if you need particular parsers for particular types,
   * this template should be specialized. Otherwise it default to
   * the parser for the hana-struct.
   * This behaviour is nice to easily get parsers for nested hana-structs.
   */
  template <class Struct, class Iterator, class Skipper>
  inline auto const type_rule = hana_struct_grammar<Struct, Iterator, Skipper>();

  /** @brief We provide some specializations for type_rules:
   *
   * 1) vector<T>: Parses comma-separated lists of T, where the parser for T is type_rule<T,...>
   * 2) bool: Parses as bool
   * 3) integral: Parses as int or uint
   * 4) enum: Parses the underlying type of the enum
   */
  template <class Struct, class Iterator, class Skipper>
  inline const qi::rule<Iterator, vector<Struct>(), Skipper> type_rule<vector<Struct>, Iterator, Skipper> =
    qi::lit('[') >> -(type_rule<Struct, Iterator, Skipper> % ',') >> qi::lit(']');

  template <class Iterator, class Skipper>
  inline const qi::rule<Iterator, bool(), Skipper> type_rule<bool, Iterator, Skipper> = qi::bool_;

  template <std::integral Integral, class Iterator, class Skipper>
  inline const qi::rule<Iterator, Integral, Skipper> type_rule<Integral, Iterator, Skipper> =
    qi::any_int_parser<Integral>{};

  template <class Enum, class Iterator, class Skipper>
  requires(std::is_enum_v<Enum>)
  inline const qi::rule<Iterator, Enum, Skipper> type_rule<Enum, Iterator, Skipper> =
    qi::any_int_parser<std::underlying_type_t<Enum>>{};

  /** @brief Create a qi parser from an accessor
   *
   * If the accessor is pair("name", V value) (ish)
   * then the result will be a parser that will successfully parse
   *      "name" : value
   *
   * Its attribute type will be V, and on a successfull parse will yield value
   */
  namespace detail {
    template < class A, class Iterator, class Skipper>
    auto make_parser(A accessor) {
      // We get the value type:
      using V = typename decltype(+accessor_ptr_type(accessor))::type;

      return proto::deep_copy(
        qi::lexeme[qi::lit('"') >> qi::lit(hana::to<char const *>(hana::first(accessor))) >> '"'] >> ':'
        >> type_rule<V, Iterator, Skipper>);
    };

  }
  template <class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto make_parser = [](auto accessor) -> decltype(auto) {
    return detail::make_parser<decltype(accessor), Iterator, Skipper>(
      accessor); // ms c++ does not take the body inline, so detail::xx fixes problem
  };

  /** @brief Create a sequence of accessor parsers from a sequence of accessors
   *
   * Basically, make_parser_sequence(accessors) = (make_parser(acc0), ..., make_parser(accN))
   * if accessors = (acc0, acc1,..., accN)
   */
  template <class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto make_accessor_parser_sequence = hana::reverse_partial(hana::transform, make_parser<Iterator, Skipper>);

  /** @brief From a sequence of parser form the sequence parser:
   *
   * If r=(r0,..., rN) is a sequence of parsers
   * make_sequence_parser(r) = r0 >> ',' >> r1 >> ',' >> ... >> ',' >> rN;
   */
  constexpr auto make_sequence_parser =
    hana::reverse_partial(hana::fold_left, [](auto&& p1, auto&& p2) -> decltype(auto) {
      // TODO: Figure out how to avoid copying here.
      return proto::deep_copy(p1 >> ',' >> p2);
    });

  /** @brief Utility function to get sequence parser for all accessors in a hana struct */
  namespace detail {
    template <class Struct, class Iterator, class Skipper >
    auto get_accessor_sequence_parser() {
      constexpr auto attrs = hana::accessors<Struct>();
      auto parsers = make_accessor_parser_sequence<Iterator, Skipper>(attrs);
      return make_sequence_parser(parsers);
    };
  }
  template <class Struct, class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto get_accessor_sequence_parser = []() {
    return detail::get_accessor_sequence_parser<Struct, Iterator, Skipper>();
  };

  /** @brief For a hana struct, get the boost::fusion::tuple<...> for its values
   */
  template <class... x>
  struct as_fusion_tuple {
    using type = fu::tuple<x...>;
  };

  constexpr auto get_tuple_from_types = hana::reverse_partial(hana::unpack, hana::metafunction<as_fusion_tuple>);

  /** @brief very similar to the corresponding boost::variant we did in mp.h.
   *
   * @tparam S : A hana struct.
   * For example, if S = ((int, a), (int, b), (string, name)), then
   * fusion_tuple<S> = hana::type_c< fusion::tuple<int, int, string> >
   */
  template <class S>
  constexpr auto fusion_tuple = get_tuple_from_types(get_accessor_types(hana::accessors<S>()));

  /** @brief From a boost::fusion::tuple<...> we want to insert the values in the right place
   * at an object of type S, where S is a hana-struct.
   *
   * This requires that tuple-type == fusion_tuple<S>, but the compiler will give you an error
   * if that is not the case....
   */
  template <class Struct, class Tuple>
  void set_from_tuple(Struct& s, Tuple const & tup) {
    // TODO: Make a static assert that the sequence of types that represent Tuple are the
    // TODO: same as for Struct...
    // We get the accessors:
    constexpr auto attrs = hana::accessors<Struct>();
    // We go through each attribute in the struct, and
    // for the i'th attribute, we set its value to the i'th element of the tuple.
    hana::for_each(hana::zip(attrs, tup), [&s](auto element) {
      auto acc = element[0_c]; // The accessor
      auto attr_ptr = hana::second(acc);
      auto value = element[1_c]; // The value in the tuple.
      attr_ptr(s) = value;
    });
  }

  /** @brief now we have all the parts we need to make a grammar for a hana-struct
   * automatically...
   */
  template <class Struct, class Iterator, class Skipper = qi::ascii::space_type>
  struct hana_struct_grammar : public qi::grammar<Iterator, Struct(), Skipper> {
    hana_struct_grammar()
      : hana_struct_grammar<Struct, Iterator, Skipper>::base_type(start) {
      using qi::lit;
      using qi::_val;
      using qi::_1;
      using qi::_2;
      using qi::_3;
      using qi::_4;
      using qi::fail;
      using qi::on_error;
      constexpr auto tuple_type_c = fusion_tuple<Struct>;
      using tuple_t = typename decltype(+tuple_type_c)::type;

      // Generate rule as the sequence of accessor parser for Struct:
      values_ = get_accessor_sequence_parser<Struct, Iterator, Skipper>();
      start = lit('{') >> values_[phx::bind(set_from_tuple<Struct, tuple_t>, _val, _1)] >> lit('}');
      on_error<fail>(start, error_handler(_4, _3, _2));
    }

    using tuple_t = typename decltype(+fusion_tuple<Struct>)::type;
    // Member variables:
    qi::rule<Iterator, Struct(), Skipper> start;
    qi::rule<Iterator, tuple_t(), Skipper> values_;
    phx::function<error_handler_> const error_handler = error_handler_{};
  };

  /** +-----------------------------------------------+
   *  |   PART 2: Make a grammar for boost::variant   |
   *  +-----------------------------------------------+
   */
  /** @brief get rule from type: */
  namespace detail {
    template <class Iterator, class Skipper, class A>
    auto get_parser_from_type(A type) {
      using V = typename decltype(+type)::type;
      return proto::deep_copy(type_rule<V, Iterator, Skipper>);
    };

  }
  template <class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto get_parser_from_type = [](auto type) -> decltype(auto) {
    return detail::get_parser_from_type<Iterator, Skipper>(type);
  };

  /** @brief get sequence of parsers from sequence of types */
  template <class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto get_parsers_from_types =
    hana::reverse_partial(hana::transform, get_parser_from_type<Iterator, Skipper>);

  /** @brief parser operator | as a function, which can then be used with e.g. hana::fold */
  constexpr auto parser_or = [](auto p1, auto p2) {
    return proto::deep_copy(p1 | p2);
  };

  /** @brief Create sequential OR parser from a sequence of parsers:
   *
   * IF r = (r0,...,rN), then
   * make_or_parser(r) = r0 | r1 | ... | rN;
   */
  constexpr auto make_or_parser = hana::reverse_partial(hana::fold_left, parser_or);


  /** @brief from template parameter pack, create hana::tuple of types */
  template <class... types>
  constexpr auto get_type_tuple = []() {
    return hana::make_tuple(hana::type_c<types>...);
  };

  /** @brief boost::variant parser: */
  template <class Iterator, class Skipper, class... types>
  struct boost_variant_grammar : public qi::grammar<Iterator, boost::variant<types...>, Skipper> {
    boost_variant_grammar()
      : boost_variant_grammar<Iterator, Skipper, types...>::base_type(start) {
      using qi::_2;
      using qi::_3;
      using qi::_4;
      using qi::fail;
      using qi::on_error;
      // We want types... --> type_seq;
      auto type_seq = get_type_tuple<types...>();
      start = make_or_parser(get_parsers_from_types<Iterator, Skipper>(type_seq));
      on_error<fail>(start, error_handler(_4, _3, _2));
    }

    qi::rule<Iterator, boost::variant<types...>, Skipper> start;
    phx::function<error_handler_> const error_handler = error_handler_{};
  };

  /** @brief We need a metafunction to get from type-sequence to grammar: */
  template <class Iterator, class Skipper, class... types>
  struct get_grammar {
    using type = boost_variant_grammar<Iterator, Skipper, types...>;
  };

  /** @brief Finally, we create a function that takes a sequence of types
   * and returns the grammar type for its corresponding boost::variant
   *
   * @tparam Iterator
   * @tparam Skipper
   */
  namespace detail {
    template <class Iterator, class Skipper, class TS>
    auto get_variant_grammar_from_types(TS type_seq) {
      // We prepend the sequence with Iterator and Skipper classes to enable use of metafunction
      auto prepended_seq = hana::concat(
        hana::make_tuple(hana::type_c<Iterator>, hana::type_c<Skipper>), hana::to<hana::tuple_tag>(type_seq));
      return hana::unpack(prepended_seq, hana::metafunction<get_grammar>);
    };

  }
  template <class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto get_variant_grammar_from_types = [](auto type_seq) {
    return detail::get_variant_grammar_from_types<Iterator, Skipper>(type_seq);
  };

  /** @brief Finally, we create function that takes a sequence of types and
   * returns grammar for its corresponding boost::variant.
   */
  namespace detail {
    template <class Iterator, class Skipper, class TS>
    auto get_variant_grammar_object_from_types(TS type_seq) {
      auto grammar_type = get_variant_grammar_from_types<Iterator, Skipper>(type_seq);
      using Grammar = typename decltype(+grammar_type)::type;
      return Grammar();
    };

  }
  template <class Iterator, class Skipper = qi::ascii::space_type>
  constexpr auto get_variant_grammar_object_from_types = [](auto type_seq) {
    return detail::get_variant_grammar_object_from_types<Iterator, Skipper>(type_seq);
  };


}
