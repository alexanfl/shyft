/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/mp.h>
#include <boost/variant.hpp>

/** @brief this file contains functionality to automatically generate the boost::variant-type from a
 * provided sequence of types.
 *
 * The file also contains functions to generate type-sets/sequences from a hana-struct.
 */
namespace shyft::mp {
  /** @brief Get types from a sequence of accessors.
   *
   * Usage: get_accessor_types(accessors) = hana::tuple of each value type amongst the accessors.
   */
  constexpr auto get_accessor_types = hana::reverse_partial(hana::transform, accessor_ptr_type);

  /** @brief Get types as a set (i.e. removing duplicates) from a sequence of accessors */
  constexpr auto get_accessor_type_set = hana::compose(hana::to<hana::set_tag>, get_accessor_types);

  /** @brief Utility template variable to call get_accessor_type_set
   *
   * if
   * struct X {
   *     BOOST_HANA_DEFINE_STRUCT(X,
   *       (int, a),
   *       (int, b),
   *       (string, name)
   *     );
   * };
   *
   * then accessor_type_set<X> == hana::make_set(hana::type_c<int>, hana::type_c<string>)
   */
  template <class S>
  constexpr auto accessor_type_set = get_accessor_type_set(hana::accessors<S>());


  /** @brief Provided a leaf-accessor, get the corresponding value type:
   *
   *  if la = (acc0, acc1,..., accN) is a leaf-accessor for root type T, and t is an object of type T,
   *  then leaf_accessor_type(la) = decltype(t.acc0.acc1...accN)
   */
  constexpr auto leaf_accessor_type = hana::compose(accessor_ptr_type, hana::back);

  /** @brief From a sequence of leaf_accessors, get a sequence of corresponding types */
  constexpr auto get_leaf_accessor_types = hana::reverse_partial(hana::transform, leaf_accessor_type);

  /** @brief From a sequence of leaf_accessors, get a set of value types */
  constexpr auto get_leaf_accessor_type_set = hana::compose(hana::to<hana::set_tag>, get_leaf_accessor_types);

  /** @brief Generate the leaf_type_set given a type */
  template <class Struct>
  constexpr auto leaf_accessor_type_set = get_leaf_accessor_type_set(leaf_accessors(hana::type_c<Struct>));

  /** @brief We want to generate a boost::variant of all types in a hana-struct.
   * Either through its accessors or leaf_accessors.
   *
   * Roughly what we want:
   * [](auto type_set) {
   *     pack = hana::unpack(set) // to type... pack
   *     // Finally, we map it to boost-variant
   *     mf = hana::metafunction<variant_type>;
   *     return mf(pack);
   * }
   *
   * I.e.: f(type_set) = hana::unpack(type_set, mf)
   */
  // Struct to be used as metafunction
  template <class... x>
  struct variant_type {
    using type = boost::variant<x...>;
  };

  // hana::metafunction<variant_type>(type1, type2,..., typeN) == variant_type<type1,type2,...,typeN>::type
  constexpr auto get_variant_from_type_set = hana::reverse_partial(hana::unpack, hana::metafunction<variant_type>);

  /** @brief Utility function to get variant from a hana struct: */
  template <class Struct>
  constexpr auto types_variant = get_variant_from_type_set(accessor_type_set<Struct>);

  template <class Struct>
  constexpr auto leaf_types_variant = get_variant_from_type_set(leaf_accessor_type_set<Struct>);

}