#include <sstream>
#include <fmt/core.h>
#include <shyft/mp/parser_utils.h>

namespace shyft::mp::grammar {

  template <typename Iterator>
    void error_handler_::operator()(qi::info const & what, Iterator err_pos, Iterator last) const {
      std::stringstream ss;
      ss << what; // utilize the stringstream fmt walker of qi.FIXME: consider a fmt:: of info instead.
      // show max 100 bytes of err location. avoid flooding exception with a lot of stuff
      size_t hint_sz = std::min(static_cast<size_t>(100), static_cast<size_t>(last - err_pos));
      auto msg = fmt::format(
        "syntax error! expecting {} here: \"{}\"\n",
        ss.str(), // what failed?
        std::string_view{err_pos, err_pos + hint_sz});
      throw std::runtime_error(msg);
    }
  template void error_handler_::operator()<char const *>(qi::info const & what, char const *, char const *) const;

}
