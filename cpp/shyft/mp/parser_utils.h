/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <type_traits>
#include <string_view>
#include <sstream>
#include <boost/spirit/include/qi.hpp>

namespace shyft::mp::grammar {
  namespace qi = boost::spirit::qi;

  /**
   * @brief full_match_check
   * @details
   * if require full_match, it means that the entire parse sequence should match the grammar
   * For a phrase_parse, this allows space before/after,
   * and for some reason, some of the strings have double null termination,
   * leaving first != last, thus failing full match when required.
   * Thus we check for null, interpreting that as a string termination,
   * valid as a full matched criteria.
   * @return true if considered a full_match.
   */
  inline bool full_match_check(char const * first, char const * last) noexcept {
    return first == last || (first && *first == '\0');
  }

  /**
   * @brief parse a phrase using any grammar or rule.
   * @details
   * The phrase_parsers do space skip/before after, both parsers require
   * a complete match of the input.
   * Notice that the full match check enforces that garbage at the end of
   * a grammar input is not accepted.
   */

  template <typename P, typename V>
  inline bool phrase_parser(char const * first, char const * last, P const & p, V& v) {
    return qi::phrase_parse(first, last, p, qi::ascii::space, v) && full_match_check(first, last);
  }

  template <typename P, typename V>
  inline bool phrase_parser(std::string_view s, P const & p, V& v) {
    return phrase_parser(s.data(), s.data()+s.size(), p, v);
  }

  /**
   * @brief Commonly used error handler for grammars
   *
   * Error handler, that needs to bound to phx function to be used in qi grammar on_error
   *
   * @note for the compile speed speed/simplicity, only Iterator type supported is cont char*
   * The implementation also limits the abount of trailing parts that goes into error message.
   */
  struct error_handler_ {
    template <typename, typename, typename>
    struct result {
      typedef void type;
    };

    // note: this needs to be specified/specialized, for for compile speed we do that for the const char* iterator type
    // in the .cpp
    //  any other type would require its own
    template <typename Iterator>
    void operator()(qi::info const & what, Iterator err_pos, Iterator last) const;
  };
}
