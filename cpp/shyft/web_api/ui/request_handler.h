/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/web_api/bg_work_result.h>
#include <shyft/core/subscription.h>

namespace shyft::energy_market::ui {
  struct layout_info;
  struct config_server;
}

namespace shyft::web_api::energy_market {
  struct json;    // fwd
  struct request; // fwd
}

namespace shyft::web_api::ui {
  using shyft::web_api::energy_market::request;
  using shyft::web_api::energy_market::json;
  using shyft::energy_market::ui::config_server;
  using shyft::core::subscription::observer_base_;
  using std::string;

  /**
   * @brief Background service that has a pointer to config_server
   *
   * @details
   * The boost beast foreground io-service receives ws messages
   * posts these to the bg thread for processing
   * the bg does the following:
   * parses the request using boost::spirit::qi
   * forwards the request to the server
   * using boost::spirit::karma, generate the json-like response
   * emit the result to the output buffer
   * forwards it to the boost beast io-services
   **/
  struct request_handler : base_request_handler {
    config_server* srv{nullptr};
    // std::atomic_bool running;///<true when starting to serve, false before and after serving

    /** @brief callback function to be used in beast server.
     * parses request into a request struct.
     * On successful parse passes it on to the handler function
     * and passes on response to server
     * On failure passes on error message as string.
     * @param input string to parse and process
     * @return bg_work_result for the server to handle.
     **/
    bg_work_result do_the_work(string const & input) override;
    bg_work_result do_subscription_work(observer_base_ const & o) override;

    /** @brief main dispatch for messages
     *
     * @param req with keyword and json struct of request data
     * @return response string as bg_work_result
     */
    bg_work_result handle_request(request const & req);

   private:
    bg_work_result handle_get_layouts_request(json const & data) const;
    bg_work_result handle_read_layout_request(json const & data) const;
    bg_work_result handle_store_layout_request(json const & data) const;
  };
}
