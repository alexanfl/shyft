/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <algorithm>
#include <string_view>
#include <shyft/web_api/web_api_generator.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/web_api/generators/json_emit.h>

namespace shyft::web_api::generator {
  namespace ka = boost::spirit::karma;
  namespace phx = boost::phoenix;

  using xy_point = shyft::energy_market::hydro_power::point;
  using shyft::energy_market::hydro_power::xy_point_curve;
  using shyft::energy_market::hydro_power::xy_point_curve_with_z;
  using shyft::energy_market::hydro_power::turbine_operating_zone;
  using shyft::energy_market::hydro_power::turbine_description;
  using shyft::energy_market::stm::xy_point_curve_with_z_list;
  using shyft::energy_market::stm::xy_point_curve_;
  using shyft::energy_market::stm::xy_point_curve_with_z_;
  using shyft::energy_market::stm::xy_point_curve_with_z_list_;
  using shyft::energy_market::absolute_constraint;
  using shyft::energy_market::penalty_constraint;
  using shyft::energy_market::stm::turbine_description_;

  using std::vector;
  using std::string;
  using std::string_view;
  using std::pair;

  using boost::spirit::karma::generate;

  using shyft::time_series::ts_point_fx;
  using shyft::time_series::point;

  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::gta_t;

  using namespace shyft::core;

  using std::string_view;
  using ka::int_;
  using ka::long_long;
  using ka::double_;
  using ka::bool_;
  namespace stm = shyft::energy_market::stm;

  template <class OutputIterator>
  void generate_url_level(OutputIterator& oi, string_view type_str, int id) {
    std::copy(begin(type_str), end(type_str), oi);
    generate(oi, int_, id);
  }

  /** @brief grammar for emitting a x,y point
   *   struct xy_point {
   *      double x,y;
   *   }
   */
  template <class OutputIterator>
  struct xy_generator : ka::grammar<OutputIterator, xy_point()> {
    xy_generator()
      : xy_generator::base_type(pg) {
      using ka::double_;
      using ka::_1;
      using ka::_val; //        _1 = _val.x                               _1 = _val.y
      pg = '[' << double_[_1 = phx::bind(&xy_point::x, _val)] << ',' << double_[_1 = phx::bind(&xy_point::y, _val)]
               << ']';
      pg.name("xy-point");
    }

    ka::rule<OutputIterator, xy_point()> pg;
  };

  /** @brief grammar for emitting a x,y point curve
   */
  template <class OutputIterator>
  struct xy_point_curve_generator : ka::grammar<OutputIterator, xy_point_curve()> {
    xy_point_curve_generator()
      : xy_point_curve_generator::base_type(pg) {
      pg = ('[' << -(xy_ % ',') << ']')[ka::_1 = phx::bind(&xy_point_curve::points, ka::_val)];
      pg.name("xy_point_curve");
    }

    ka::rule<OutputIterator, xy_point_curve()> pg;
    xy_generator<OutputIterator> xy_;
  };

  /** @brief grammar for emitting a x,y point curve with z
   */
  template <class OutputIterator>
  struct xy_point_curve_with_z_generator : ka::grammar<OutputIterator, xy_point_curve_with_z()> {
    xy_point_curve_with_z_generator()
      : xy_point_curve_with_z_generator::base_type(pg) {
      pg = ka::lit("{\"z\":")
        << double_[ka::_1 = phx::bind(&xy_point_curve_with_z::z, ka::_val)] << ka::lit(",\"points\":")
        << xy_points_[ka::_1 = phx::bind(&xy_point_curve_with_z::xy_curve, ka::_val)] << '}';
      pg.name("xy_point_curve_with_z");
    }

    ka::rule<OutputIterator, xy_point_curve_with_z()> pg;
    xy_point_curve_generator<OutputIterator> xy_points_;
  };

  /** @brief grammar for emitting a list of x,y point curves with z
   */
  template <class OutputIterator>
  struct xy_point_curve_with_z_list_generator : ka::grammar<OutputIterator, xy_point_curve_with_z_list()> {
    xy_point_curve_with_z_list_generator()
      : xy_point_curve_with_z_list_generator::base_type(pg) {
      pg = ('[' << -(xyz_points_ % ',') << ']');
      pg.name("xy_point_curve_with_z_list");
    }

    ka::rule<OutputIterator, xy_point_curve_with_z_list()> pg;
    xy_point_curve_with_z_generator<OutputIterator> xyz_points_;
  };

  /** @brief grammar for emitting a turbine efficiency
   */
  template <class OutputIterator>
  struct turbine_operating_zone_generator : ka::grammar<OutputIterator, turbine_operating_zone()> {
    turbine_operating_zone_generator()
      : turbine_operating_zone_generator::base_type(pg) {
      pg = ka::lit("{\"production_min\":")
        << double_[ka::_1 = phx::bind(&turbine_operating_zone::production_min, ka::_val)]
        << ka::lit(",\"production_max\":")
        << double_[ka::_1 = phx::bind(&turbine_operating_zone::production_max, ka::_val)]
        << ka::lit(",\"production_nominal\":")
        << double_[ka::_1 = phx::bind(&turbine_operating_zone::production_nominal, ka::_val)]
        << ka::lit(",\"fcr_min\":") << double_[ka::_1 = phx::bind(&turbine_operating_zone::fcr_min, ka::_val)]
        << ka::lit(",\"fcr_max\":") << double_[ka::_1 = phx::bind(&turbine_operating_zone::fcr_max, ka::_val)]
        << ka::lit(",\"efficiency_curves\":[")
        << (-(xyz_points_ % ','))[ka::_1 = phx::bind(&turbine_operating_zone::efficiency_curves, ka::_val)]
        << ka::lit("]}");
      pg.name("turbine_effiency");
    }

    ka::rule<OutputIterator, turbine_operating_zone()> pg;
    xy_point_curve_with_z_generator<OutputIterator> xyz_points_;
  };

  /** @brief grammar for emitting a turbine description.
   */
  template <class OutputIterator>
  struct turbine_description_generator : ka::grammar<OutputIterator, turbine_description()> {
    turbine_description_generator()
      : turbine_description_generator::base_type(pg) {
      pg = ka::lit("{\"turbine_effiencies\":[")
        << (-(t_eff_ % ','))[ka::_1 = phx::bind(&turbine_description::operating_zones, ka::_val)] << ka::lit("]}");
      pg.name("turbine_description");
    }

    ka::rule<OutputIterator, turbine_description()> pg;
    turbine_operating_zone_generator<OutputIterator> t_eff_;
  };

  /** @brief grammar for emitting a message (pair<utctime, string>)
   */
  template <typename OutputIterator>
  struct t_str_generator : ka::grammar<OutputIterator, pair<utctime, string>()> {
    t_str_generator()
      : t_str_generator::base_type(pg) {
      pg = ka::lit("[") << time_ << comma << "\"" << ka::string << "\"]";
    }

    ka::rule<OutputIterator, pair<utctime, string>()> pg;
    utctime_generator<OutputIterator> time_;
  };

  /** @brief grammar for emitting an absolute constraint */
  template <typename OutputIterator>
  struct absolute_constraint_generator : ka::grammar<OutputIterator, absolute_constraint()> {
    absolute_constraint_generator()
      : absolute_constraint_generator::base_type(pg) {
      pg = ka::lit("{")
        << ka::lit("\"limit\":") << ts_[ka::_1 = phx::bind(&absolute_constraint::limit, ka::_val)] << ","
        << ka::lit("\"flag\":") << ts_[ka::_1 = phx::bind(&absolute_constraint::flag, ka::_val)] << ka::lit("}");
    }

    ka::rule<OutputIterator, absolute_constraint()> pg;
    apoint_ts_generator<OutputIterator> ts_;
  };

  /** @brief grammar for emitting a penalty constraint */
  template <typename OutputIterator>
  struct penalty_constraint_generator : ka::grammar<OutputIterator, penalty_constraint()> {
    penalty_constraint_generator()
      : penalty_constraint_generator::base_type(pg) {
      pg = ka::lit("{")
        << ka::lit("\"limit\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::limit, ka::_val)] << ","
        << ka::lit("\"flag\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::flag, ka::_val)] << ","
        << ka::lit("\"cost\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::cost, ka::_val)] << ","
        << ka::lit("\"penalty\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::cost, ka::_val)] << ka::lit("}");
    }

    ka::rule<OutputIterator, penalty_constraint()> pg;
    apoint_ts_generator<OutputIterator> ts_;
  };

  emit_type(utctime, utctime_generator);
  emit_type(utcperiod, utcperiod_generator);

  // new format for ts: emit_type(apoint_ts, apoint_ts_generator);

  template <class OutputIterator>
  struct emit<OutputIterator, apoint_ts> {

    emit(OutputIterator& oi, apoint_ts const & t) {
      static apoint_ts_generator<OutputIterator> t_{true}; // use new format
      generate(oi, t_, t);
    }
  };

  x_emit_vec(apoint_ts); // will use the above emit ts function.

  emit_type(generic_dt, generic_dt_generator);
  //-- xy-type curve-descriptions, karma-generators,  considered basic types in this context:
  emit_type(xy_point_curve, xy_point_curve_generator);
  x_emit_vec(xy_point_curve);
  emit_type(xy_point_curve_with_z, xy_point_curve_with_z_generator);
  x_emit_vec(xy_point_curve_with_z);
  emit_type(turbine_description, turbine_description_generator);
  emit_type(absolute_constraint, absolute_constraint_generator);
  emit_type(penalty_constraint, penalty_constraint_generator);

  using message = pair<utctime, string>;
  emit_type(message, t_str_generator);
  x_emit_vec(message);

  template <typename T>
  using tm = std::map<utctime, T>;
  x_emit_map(utctime, double);
  x_emit_shared_ptr(tm<double>);

  x_emit_map(utctime, xy_point_curve_);
  x_emit_shared_ptr(tm<xy_point_curve_>);

  x_emit_map(utctime, xy_point_curve_with_z_);
  x_emit_shared_ptr(tm<xy_point_curve_with_z_>);

  x_emit_map(utctime, xy_point_curve_with_z_list_);

  x_emit_map(utctime, xy_point_curve_with_z_list);
  x_emit_shared_ptr(tm<xy_point_curve_with_z_list_>);

  x_emit_map(utctime, turbine_description_);
  x_emit_shared_ptr(tm<turbine_description_>);


  /** x_emit_shared_ptr
   *
   *  utility macro to create null or object emitter,
   *  we use a lot of shared-ptr, so we would like null or object almost everywhere
   */
  x_emit_shared_ptr(xy_point_curve);
  x_emit_shared_ptr(xy_point_curve_with_z); // alias xyz_point_curve
  x_emit_shared_ptr(xy_point_curve_with_z_list);
  x_emit_shared_ptr(turbine_description);

  /** @brief generator class for log_entry */
  template <class OutputIterator>
  struct log_entry_generator : ka::grammar<OutputIterator, shyft::energy_market::stm::log_entry()> {
    log_entry_generator()
      : log_entry_generator::base_type(pg) {

      using log_entry = shyft::energy_market::stm::log_entry;
      constexpr char quote = '"';
      pg = ka::lit('{')
        << ka::lit("\"time\":") << time_[ka::_1 = phx::bind(&log_entry::time, ka::_val)] << ka::lit(",\"severity\":")
        << quote << ka::string[ka::_1 = phx::bind(map_severity, phx::bind(&log_entry::severity, ka::_val))] << quote
        << ka::lit(",\"code\":") << ka::int_[ka::_1 = phx::bind(&log_entry::code, ka::_val)] << ka::lit(",\"message\":")
        << quote << esc_str_[ka::_1 = phx::bind(&log_entry::message, ka::_val)] << quote << ka::lit('}');
    }

    static string map_severity(shyft::energy_market::stm::log_severity sev_code) {
      using enum shyft::energy_market::stm::log_severity;
      switch (sev_code) {
      case information:
        return "INFORMATION";
      case warning:
        return "WARNING";
      case error:
        return "ERROR";
      default:
        return "UNDEFINED";
      }
    }

    ka::rule<OutputIterator, shyft::energy_market::stm::log_entry()> pg;
    utctime_generator<OutputIterator> time_;
    escaped_string_generator<OutputIterator> esc_str_;
  };

  /** fwd declare the templates we need for request_iterator_t
   *
   * Current approach is to use a std::back_insert_iterator<string> as output_iterator type,
   *
   * Using extern template ensures that the templates are only
   * expanded once, in their in respective compilation units.
   *
   * Basically, we tell the c++ compiler that somewhere, there
   * is an instantiation of the template with response_iterator.
   * And then we take care in the grammar.cpp files to ensure that
   * we at least instantiate the templates for the reqest_iterator type.
   */
  using em_output_iterator = std::back_insert_iterator<std::string>;
  extern template struct emit_object<em_output_iterator>;
}
