#pragma once
#include <shyft/mp/grammar.h>
// #include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::mp::grammar {
  using namespace shyft::web_api::grammar;
  using shyft::energy_market::stm::t_xy_;
  using shyft::energy_market::stm::t_xyz_;
  using shyft::energy_market::stm::t_xyz_list_;
  using shyft::energy_market::stm::t_turbine_description_;
  using shyft::time_axis::generic_dt;

  /** @brief type_rule specialization for string */
  template <class Iterator, class Skipper>
  inline auto const type_rule<string, Iterator, Skipper> = quoted_string_grammar<Iterator, Skipper>();

  /** @brief type_rule specialization for apoint_ts */
  template <class Iterator, class Skipper>
  inline auto const type_rule<apoint_ts, Iterator, Skipper> = apoint_ts_grammar<Iterator, Skipper>();

  /** @brief type_rule specializaition for generic_dt */
  template <class Iterator, class Skipper>
  inline auto const type_rule<generic_dt, Iterator, Skipper> = time_axis_grammar<Iterator, Skipper>();

  /** @brief type_rule specialization for t_xy_ */
  template <class Iterator, class Skipper>
  inline auto const type_rule<t_xy_, Iterator, Skipper> =
    t_map_grammar<Iterator, xy_point_curve, xy_point_list_grammar<Iterator, Skipper>, Skipper>();

  /** @brief type_rule specialization for t_xyz_list_ */
  template <class Iterator, class Skipper>
  inline auto const type_rule<t_xyz_list_, Iterator, Skipper> =
    t_map_grammar<Iterator, vector<xy_point_curve_with_z>, xyz_list_grammar<Iterator, Skipper>, Skipper>();

  /** @brief type_rule specialization for t_turbine_description_ */
  template <class Iterator, class Skipper>
  inline auto const type_rule<t_turbine_description_, Iterator, Skipper> =
    t_map_grammar<Iterator, turbine_description, turbine_description_grammar<Iterator, Skipper>, Skipper>();

  /** @brief type_rule specialization for pair<utctime, string> */
  template <class Iterator, class Skipper>
  inline auto const type_rule<pair<utctime, string>, Iterator, Skipper> = t_str_grammar<Iterator, Skipper>();

}
