#include <shyft/web_api/energy_market/request_handler.h>

#include <algorithm>
#include <iterator>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/hana.hpp>
#include <boost/variant.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>

#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/mp.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

namespace shyft::web_api::energy_market {

  namespace mp = shyft::mp;
  namespace hana = boost::hana;
  using namespace shyft::energy_market;

  using shyft::core::no_utctime;
  using shyft::web_api::generator::emit_object;
  using shyft::web_api::generator::emit_vector_fx;

  dlib::logger request_handler::log{"dstm-web"};

  bg_work_result request_handler::handle_request(request const &req) {
    if (req.keyword == "read_model") {
      return handle_read_model_request(req.request_data);
    } else if (req.keyword == "read_attributes") {
      return handle_read_attribute_request(req.request_data);
    } else if (req.keyword == "get_model_infos") {
      return handle_get_model_infos_request(req.request_data);
    } else if (req.keyword == "get_hydro_components") {
      return handle_get_hydro_components_request(req.request_data);
    } else if (req.keyword == "set_attributes") {
      return handle_set_attribute_request(req.request_data);
    } else if (req.keyword == "unsubscribe") {
      return handle_unsubscribe_request(req.request_data);
    } else if (req.keyword == "fx") {
      return handle_fx_request(req.request_data);
    } else if (req.keyword == "run_params") {
      return handle_run_params_request(req.request_data);
    } else if (req.keyword == "get_log") {
      return handle_get_log_request(req.request_data);
    } else if (req.keyword == "opt_summary") {
      return handle_opt_summary_request(req.request_data);
    } else if (req.keyword == "get_state") {
      return handle_get_state_request(req.request_data);
    } else if (req.keyword == "get_contract_components") {
      return handle_get_contract_components_request(req.request_data);
    }
    return bg_work_result(fmt::format("Unknown keyword: {}", req.keyword));
  }

  /** called by the web-server timer serving the web-socket connections at regular intervals */
  bg_work_result request_handler::do_subscription_work(observer_base_ const &o) {
    // We support subscription on proxy-type expressions
    // so ts_expression_observer_ , url, looks different for the in-memory em server
    // instead of 'ts-url', we use request_id (assumed to be unique between clients, so that's a possible problem!),
    // ...a map o->request_id -> request_json, (from the original request)
    // and then we can simply do
    //  handle_read_attribute_request(request_json)
    //
    if (o->recalculate()) { // ok, is there a difference(version-number updated, ref. the last sent)
      auto proxy_observer = std::dynamic_pointer_cast<stm::subscription::proxy_attr_observer>(o);
      return proxy_observer->re_emit_response();
    } else {
      return bg_work_result{};
    }
  }

  bg_work_result request_handler::do_the_work(std::string const &input) {
    // 1: Parse into a request
    // 2: Error handling: On success, do a switch on keyword
    // 3: Handle every case, and emit a response string

    bg_work_result b;
    // 1) Parse the request:
    request arequest;
    shyft::web_api::grammar::request_grammar<char const *> request_;
    bool ok_parse = false;
    bg_work_result response;
    try {
      ok_parse = phrase_parser(input, request_, arequest);
      if (ok_parse) {
        response = handle_request(arequest);
      } else {
        response = bg_work_result{fmt::format("not understood: {}", input)};
      }
    } catch (std::runtime_error const &re) {
      response = bg_work_result{fmt::format("request_parse:{}", re.what())};
    }
    return response;
  }

  template <class OutputIterator, class V>
  void emit_system_elements(OutputIterator &oi, V const &elements) {
    emit_vector_fx(oi, elements, [](OutputIterator &oi, auto el) {
      emit_object<OutputIterator> oo(oi);
      oo.def("id", el->id).def("name", el->name);
    });
  }

  /** @brief handle requests of the form:
   *
   * @param data: {
   *          "model_key": <string>
   *      }
   * @return
   */
  bg_work_result request_handler::handle_read_model_request(json const &data) {
    // Get model:
    auto mid = data.required<std::string>("model_key");

    return models
      .observe_or_throw(
        mid,
        [&](auto &&view) {
          auto mdl = view.model;
          // Get request id:
          auto req_id = data.required<std::string>("request_id");
          auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);
          auto sink = std::back_inserter(response);
          // Emit bare-bones model structure:
          {
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
              .def("id", mdl->id)
              .def("name", mdl->name)
              .def("json", mdl->json)
              .def_fx(
                "hps",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->hps);
                })
              .def_fx(
                "unit_groups",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->unit_groups);
                })
              .def_fx(
                "markets",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->market);
                })
              .def_fx(
                "contracts",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->contracts);
                })
              .def_fx(
                "contract_portfolios",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->contract_portfolios);
                })
              .def_fx(
                "networks",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->networks);
                })
              .def_fx(
                "power_modules",
                [&mdl](auto oi) {
                  emit_system_elements(oi, mdl->power_modules);
                })
              .def_fx("wind_farms", [&mdl](auto oi) {
                emit_system_elements(oi, mdl->wind_farms);
              });
          }
          response += "}";
          return bg_work_result{response};
        })
      .get();
  }

  bg_work_result request_handler::handle_get_model_infos_request(json const &data) {

    auto model_infos = models
                         .observe([&](auto const &models) {
                           std::map<std::string, model_info, std::less<>> infos;
                           for (const auto &[key, shared_model] : models) {
                             auto model = shared_model->model;
                             infos[key] = srv::model_info(model->id, model->name, no_utctime, model->json);
                           }
                           return infos;
                         })
                         .get();

    // Prepare response:
    auto req_id = data.required<std::string>("request_id");
    auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);
    auto sink = std::back_inserter(response);
    emit_vector_fx(sink, model_infos, [](auto oi, auto mi) {
      emit_object<decltype(oi)> oo(oi);
      oo.def("model_key", mi.first).def("id", mi.second.id).def("name", mi.second.name);
    });
    response += "}";
    return bg_work_result{response};
  }

  /** @brief Get out a list of IDs for which attributes have values attached.
   *
   * @tparam T : Hana-struct type, e.g. reservoir, powerplant &c.
   * @param t  : instance to check which attributes are set for.
   * @return : vector of attribute-ID's in string format.
   */
  template <class T>
  std::vector<std::string> available_attributes(T const &t) {
    // Accessor functions for every attribute in the struct T (recursive, so gives attributes of nested structs as well)
    auto constexpr attr_paths = mp::leaf_accessors(hana::type_c<T>);

    std::vector<std::string> attr_ids{};
    // Iterate over every attribute:
    hana::for_each(
      attr_paths, // The sequence
      [&attr_ids, &t](auto m) {
        // m is here a pair (attr_id, attr_accessor_function);
        if (attr_traits::exists(mp::leaf_access(t, m))) {
          attr_ids.push_back(std::string(mp::leaf_accessor_id_str(m)));
        }
      });
    return attr_ids;
  }

  template <class OutputIterator>
  void emit_power_plant_skeleton(OutputIterator &oi, stm::power_plant const &pp, bool get_data = false) {
    emit_object<OutputIterator> oo(oi);
    oo.def("id", pp.id).def("name", pp.name).def_fx("units", [&pp](auto oi) {
      *oi++ = generator::arr_begin;
      for (auto it = pp.units.begin(); it != pp.units.end(); ++it) {
        if (it != pp.units.begin())
          *oi++ = generator::comma;
        generator::emit(oi, (*it)->id);
      }
      *oi++ = generator::arr_end;
    });
    if (get_data)
      oo.def("set_attrs", available_attributes(pp));
  }

  template <class OutputIterator>
  void emit_energy_market_area_skeleton(OutputIterator &oi, stm::energy_market_area const &ema, bool get_data = false) {
    emit_object<OutputIterator> oo(oi);
    oo.def("id", ema.id).def("name", ema.name);
    if (get_data)
      oo.def("set_attrs", available_attributes(ema));
  }

  template <class OutputIterator>
  void emit_waterway_skeleton(OutputIterator &oi, stm::waterway const &wr, bool get_data = false) {
    emit_object<OutputIterator> oo(oi);
    oo.def("id", wr.id).def("name", wr.name).def("upstreams", wr.upstreams).def("downstreams", wr.downstreams);
    if (get_data)
      oo.def("set_attrs", available_attributes(wr));
  }

  template <class OutputIterator>
  void emit_hps_reservoirs(OutputIterator &oi, stm::stm_hps const &hps, bool get_data = false) {
    emit_vector_fx(oi, hps.reservoirs, [&get_data](auto oi, auto res_) {
      emit_object<OutputIterator> oo(oi);
      oo.def("id", res_->id).def("name", res_->name);
      if (get_data) {
        oo.def("set_attrs", available_attributes(*std::dynamic_pointer_cast<stm::reservoir>(res_)));
      }
    });
  }

  template <class OutputIterator>
  void emit_hps_units(OutputIterator &oi, stm::stm_hps const &hps, bool get_data = false) {
    emit_vector_fx(oi, hps.units, [&get_data](auto oi, auto unit_) {
      emit_object<OutputIterator> oo(oi);
      oo.def("id", unit_->id).def("name", unit_->name);
      if (get_data) {
        oo.def("set_attrs", available_attributes(*std::dynamic_pointer_cast<stm::unit>(unit_)));
      }
    });
  }

  template <class OutputIterator, class PPVector>
  void emit_hps_power_plants(OutputIterator &oi, PPVector const &ppv, bool get_data = false) {
    emit_vector_fx(oi, ppv, [&get_data](auto oi, auto pp_) {
      emit_power_plant_skeleton(oi, *std::dynamic_pointer_cast<stm::power_plant>(pp_), get_data);
    });
  }

  template <class OutputIterator, class OVector>
  void emit_energy_market_area(OutputIterator &oi, OVector const &ov, bool get_data = false) {
    emit_vector_fx(oi, ov, [&get_data](auto oi, auto o_) {
      emit_energy_market_area_skeleton(oi, *o_, get_data);
    });
  }

  template <class OutputIterator>
  void emit_hps_waterways(OutputIterator &oi, stm::stm_hps const &hps, bool get_data = false) {
    emit_vector_fx(oi, hps.waterways, [&get_data](auto oi, auto wr_) {
      emit_waterway_skeleton(oi, *std::dynamic_pointer_cast<stm::waterway>(wr_), get_data);
    });
  }

  template <class OutputIterator>
  void emit_contract_skeleton(OutputIterator &oi, stm::contract const &o, bool get_data = false) {
    emit_object<OutputIterator> oo(oi);
    oo.def("id", o.id)
      .def("name", o.name)
      .def_fx(
        "power_plants",
        [&o, &get_data](auto oi) {
          emit_hps_power_plants(oi, o.power_plants, get_data);
        })
      .def_fx(
        "energy_market_areas",
        [&o, &get_data](auto oi) {
          emit_energy_market_area(oi, o.get_energy_market_areas(), get_data);
        })
      .def_fx(
        "relations",
        [&o, &get_data](auto oi) {
          emit_vector_fx(oi, o.relations, [](auto oi, auto const rel) {
            emit_object<OutputIterator> e(oi);
            e.def("id", rel->id)
              .def("relation_type", rel->relation_type)
              .def("contract", rel->related ? rel->related->id : -1) // -1 for no relation ref.
              ;
          });
        })
      .def_fx("portfolios", [&o, &get_data](auto oi) {
        emit_vector_fx(oi, o.get_portfolios(), [](auto oi, auto const rel) {
          generator::emit(oi, rel->id);
        });
      });
    if (get_data)
      oo.def("set_attrs", available_attributes(o));
  }

  void emit_contracts(auto &oi, stm::stm_system const &sys, bool get_data = false) {
    emit_vector_fx(oi, sys.contracts, [&get_data](auto oi, auto o_) {
      emit_contract_skeleton(oi, *o_, get_data);
    });
  }

  bg_work_result request_handler::handle_get_hydro_components_request(json const &data) {
    // Get model ID and HPS ID:
    auto mid = data.required<std::string>("model_key");
    auto hps_id = data.required<int>("hps_id");

    // Find if whether to find available data as well:
    auto avail_data_kwarg = data.optional("available_data");
    bool avail_data = false;
    if (avail_data_kwarg) {
      avail_data = boost::get<bool>(*avail_data_kwarg);
    }
    // Get model and search for requested HPS in model:
    return models
      .observe_or_throw(
        mid,
        [&](auto &&view) {
          auto &mdl = view.model;
          auto it = std::ranges::find_if(mdl->hps, [&hps_id](auto ihps) {
            return ihps->id == hps_id;
          });
          if (it == mdl->hps.end()) {
            throw std::runtime_error(fmt::format("Unable to find HPS {} in model '{}'", hps_id, mid));
          }
          auto hps = *it;

          // Prepare response:
          auto req_id = data.required<std::string>("request_id");
          auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);
          auto sink = std::back_inserter(response);
          //---- EMIT DATA: ----//
          { // Has to be in scope so destructor is called appropriately.
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
              .def("hps_id", hps->id)
              .def_fx(
                "reservoirs",
                [&hps, &avail_data](auto oi) {
                  emit_hps_reservoirs(oi, *hps, avail_data);
                })
              .def_fx(
                "units",
                [&hps, &avail_data](auto oi) {
                  emit_hps_units(oi, *hps, avail_data);
                })
              .def_fx(
                "power_plants",
                [&hps, &avail_data](auto oi) {
                  emit_hps_power_plants(oi, hps->power_plants, avail_data);
                })
              .def_fx("waterways", [&hps, &avail_data](auto oi) {
                emit_hps_waterways(oi, *hps, avail_data);
              });
          }
          //---- RETURN RESPONSE: ------//
          response += "}";
          return bg_work_result{response};
        })
      .get();
  }

  bg_work_result request_handler::handle_get_contract_components_request(json const &data) {
    // Get model ID and HPS ID:
    auto mid = data.required<std::string>("model_key");
    // Find if whether to find available data as well:
    auto avail_data_kwarg = data.optional("available_data");
    bool avail_data = false;
    if (avail_data_kwarg) {
      avail_data = boost::get<bool>(*avail_data_kwarg);
    }
    return models
      .observe_or_throw(
        mid,
        [&](auto &&view) {
          auto &mdl = view.model;
          // Prepare response:
          auto req_id = data.required<std::string>("request_id");
          auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);
          {
            auto sink = std::back_inserter(response);
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid).def_fx("contracts", [&mdl, &avail_data](auto oi) {
              emit_contracts(oi, *mdl, avail_data);
            });
          }
          response += "}";
          return bg_work_result{response};
        })
      .get();
  }

  bg_work_result request_handler::handle_unsubscribe_request(json const &data) {
    auto req_id = data.required<std::string>("request_id");
    auto unsub_id = data.required<std::string>("subscription_id");
    std::string response = "";
    auto sink = std::back_inserter(response);
    {
      emit_object<decltype(sink)> oo(sink);
      oo.def("request_id", req_id).def("subscription_id", unsub_id).def("diagnostics", std::string{});
    }

    return bg_work_result{response, unsub_id};
  }

  /** @brief handle fx(mid,fx_arg) request
   *
   * In the first approach, just call the server callback,
   * doing no claim to the model/nor server.
   * Assume the callback will do proper claim on shared resources
   * when needed.
   */
  bg_work_result request_handler::handle_fx_request(json const &data) {
    auto req_id = data.required<std::string>("request_id");
    auto model_key = data.required<std::string>("model_key");
    auto fx_arg = data.required<std::string>("fx_arg");
    auto success = fx_cb ? fx_cb(model_key, fx_arg) : false;

    std::string response = "";
    auto sink = std::back_inserter(response);
    {
      emit_object<decltype(sink)> oo(sink);
      oo.def("request_id", req_id).def("diagnostics", std::string{(success ? "" : "Failed")});
    }
    return bg_work_result{response};
  }


}
