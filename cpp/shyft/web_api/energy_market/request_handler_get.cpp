#include <shyft/web_api/energy_market/request_handler.h>

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <functional>
#include <ranges>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

#include <boost/asio/thread_pool.hpp>
#include <boost/describe/enum_to_string.hpp>
#include <boost/hana.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/variant/static_visitor.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>

#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/mp.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

namespace shyft::web_api::energy_market {

  namespace mp = shyft::mp;
  namespace hana = boost::hana;
  using namespace shyft::energy_market;

  using shyft::core::utcperiod;
  using shyft::core::min_utctime;
  using shyft::core::from_seconds;
  using shyft::web_api::generator::emit_object;
  using shyft::web_api::generator::emit_vector_fx;
  using shyft::web_api::generator::log_entry_generator;
  using shyft::time_series::dd::apoint_ts;

  using shyft::time_axis::generic_dt;

  /** @brief visitor class for dispatching how to read attributes based on type
   *
   */
  struct read_proxy_handler : boost::static_visitor<attribute_value_type> {
    struct extract_period : boost::static_visitor<utcperiod> {
      utcperiod def;

      utcperiod operator()(utcperiod const & p) const {
        return p;
      }

      utcperiod operator()(std::vector<int> const & v) const {
        if (v.size() != 2)
          throw std::runtime_error("parsing read_period failed, needs exact format of [from,to]");

        return utcperiod{from_seconds(v[0]), from_seconds(v[1])};
      }

      template <typename T>
      utcperiod operator()(T const &) const {
        return def;
      }
    };

    read_proxy_handler(json const & data, request_handler& handler, std::function<bg_work_result(json const &)> cb)
      : handler{handler} {
      // Set up for subscription:
      auto osub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
      use_cache = boost::get_optional_value_or(data.optional<bool>("cache"), true);
      if (osub) {
        json sub_data = data;
        sub_data.m.erase("subscribe");
        subscription = std::make_shared<stm::subscription::proxy_attr_observer>(
          handler.models, handler.sm, handler.ts_sm(), data.required<std::string>("request_id"), sub_data, cb);
        subscription->recalculate(); // important: since we emit version 0, set initial value of terminals to 0
      }

      // Setting up read_type, -period and time_axis;
      read_type = boost::get_optional_value_or(data.optional<std::string>("read_type"), "read");
      if (read_type == "percentiles")
        percentiles = data.required<std::vector<int>>("percentiles");
      ta = data.required<generic_dt>("time_axis");
      // read_period = boost::get_optional_value_or(data.optional<utcperiod>("read_period"), ta.total_period());
      auto rp = data.optional("read_period");
      if (rp) {
        extract_period epv; // epv.def=ta.total_period();
        read_period = boost::apply_visitor(epv, *rp);
        if (read_period == epv.def)
          throw std::runtime_error("dstm: failed to parse read_period attribute from the request");
      } else {
        read_period = ta.total_period();
      }
    }

    attribute_value_type operator()(time_series::dd::apoint_ts const & ts) {
      time_series::dd::apoint_ts nts;
      if (ts.needs_bind()) { // Case 1: The series is unbound:
        if (!handler.dtss)
          throw std::runtime_error("Dtss has to be set to read unbound time series.");
        dtss::ts_vector_t tsv;
        auto tsc = ts.clone_expr();
        tsv.emplace_back(tsc);
        // use_cache as pr. request(default true) also update cache
        auto eval_result = evaluate_ts(
          std::bind_front(&dtss::server::do_read, handler.dtss),
          handler.models,
          tsv,
          read_period,
          use_cache,
          true,
          read_period)[0];
        if (auto err = std::get_if<energy_market::stm::evaluate_ts_error>(&eval_result)) {
          request_handler::log << dlib::LERROR << "Failed to evaluate " << tsc.stringify() << " diag:" << err->what;
          return std::string("not found");
        }
        nts = std::get<time_series::dd::apoint_ts>(std::move(eval_result));
      } else {
        nts = ts;
      }
      // TODO: the percentiles, or should we say percentile, ..
      //       could be used, if set, accepting one percentile
      //       combined with time-axis
      if (ta.size() == 0) {
        return nts; // just return the time-series as is, no average, or resample
      }

      if (nts.point_interpretation() == time_series::POINT_AVERAGE_VALUE) {
        // we promise to deliver points to valid for the time-axis
        // stair-case -> use true average, makes a lot of sense
        return nts.average(ta);
      } else {
        // linear, point-in-time state-variables, we use resampling to the exact provided time-axis (hmm. maybe
        // plus one timepoint?? or should user supply resample ta?)
        apoint_ts rts(ta, 1.0, time_series::POINT_INSTANT_VALUE);
        return nts.use_time_axis_from(rts);
      }
    }

    template <typename V>
    attribute_value_type operator()(std::shared_ptr<std::map<utctime, V>> const & tv) {
      // We only read the values that are in the read_period:
      auto res = std::make_shared<std::map<utctime, V>>();
      utctime tx = min_utctime; // keeps track of entries <= read_period.start
      V x;                      // the one entry that was found closest to read_period.start
      for (auto const & kv : *tv) {
        if (kv.first > read_period.start && kv.first < read_period.end) {
          res->insert(res->end(), kv);              // within the period, add it.
        } else if (kv.first <= read_period.start) { // add entry just at start, or first before start
          if (kv.first > tx) {                      // better candidate found
            tx = kv.first;                          // record the time of the candidate
            x = kv.second;                          // and the value, to be inserted when we are done with the loop.
          }
        }
      }
      if (tx != min_utctime) {
        (*res)[tx] = x; // insert the most recent entry before the read period
      }
      return res;
    }

    template <typename V>
    attribute_value_type operator()(V const & v) {
      return v;
    }

    std::shared_ptr<stm::subscription::proxy_attr_observer> subscription;
    std::string read_type;
    std::vector<int> percentiles;
    generic_dt ta;
    utcperiod read_period;
    request_handler& handler;
    bool use_cache{true};
  };

  template <class T>
  std::vector<json>
    get_proxy_attributes(T const & t, std::vector<std::string> const & attr_ids, read_proxy_handler& rph) {
    // Set up result:
    std::vector<json> attr_vals;
    std::vector<bool> found(
      attr_ids.size(), false); // keep track of which attributes we actually found so we an report error
    size_t count = 0;          // count number we find, so we can return quickly
    auto has_id_x = [&attr_ids, &found, &count](char const * id) {
      for (auto i = 0u; i < attr_ids.size(); ++i) {
        if (!strcmp(attr_ids[i].c_str(), id)) {
          found[i] = true; // mark it as found
          ++count;
          return true; // and return
        }
      }
      return false; // not found/wanted.
    };
    // Iterate over each attribute, except ts map
    auto constexpr attribute_paths = mp::leaf_accessors(hana::type_c<T>);
    hana::for_each(
      attribute_paths, // The sequence
      [&](auto m) {
        // 0. Check if current id is in the specified list:
        if (has_id_x(mp::leaf_accessor_id_str(m))) {
          json attr_struct;
          attr_struct["attribute_id"] = std::string(mp::leaf_accessor_id_str(m));
          auto val = mp::leaf_access(t, m);
          if (attr_traits::exists(val)) {
            attr_struct["data"] = rph(mp::leaf_access(t, m));
          } else {
            attr_struct["data"] = std::string("not found");
          }
          if (rph.subscription) {
            rph.subscription->add_subscription(t, m);
          }
          attr_vals.emplace_back(attr_struct);
        }
      });
    // iterate over the .tsm[].. attributes, unless, we already did find all we searched for
    if (count != attr_ids.size()) { // we lack some, let's check the .tsm[].. if they are there.
      if constexpr (
        !std::is_same<stm::unit_group_member, T>::value && !std::is_same<stm::unit_member, T>::value
        && !std::is_same<stm::power_module_member, T>::value
        && !std::is_same<stm::contract_relation, T>::value) { // only if T has .tsm[]
        for (auto i = 0u; i < found.size(); ++i) {
          if (!found[i]) {
            if (!t.tsm.empty() || attr_ids[i].rfind("ts.", 0) == 0) {

              auto f = t.tsm.find(attr_ids[i].substr(3));
              if (f != t.tsm.end()) {
                found[i] = true;
                ++count;

                json attr_struct;
                attr_struct["attribute_id"] = attr_ids[i];
                auto val = f->second;
                if (attr_traits::exists(val)) {
                  attr_struct["data"] = rph(val);
                } else {
                  attr_struct["data"] = std::string("not found");
                }
                if (rph.subscription) {
                  rph.subscription->add_ts_map_subscription(t, attr_ids[i]);
                }
                attr_vals.emplace_back(attr_struct);
              }
            } else if (attr_ids[i] == "json") {
              found[i] = true;
              ++count;
              json attr_struct;
              attr_struct["attribute_id"] = attr_ids[i];
              attr_struct["data"] = t.json;
              attr_vals.emplace_back(attr_struct);
            }
          }
        }
      }
    }

    if (count != attr_ids.size()) { // we got a request that ask for not existent attributes.
      for (auto i = 0u; i < found.size(); ++i) {
        if (!found[i]) {
          json r;
          r["attribute_id"] = attr_ids[i];                // just pretend we have it
          r["data"] = std::string("attribute not found"); //
          attr_vals.emplace_back(r);
        }
      }
    }
    return attr_vals;
  }

  /** @brief Reads a list of proxy attributes from a list of T specified by data.
   *
   * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)

   * @tparam Fx a callable (int cid, std::shared_ptr<Tc> const&el)->bool if el->id matches id
   * @param vecvals range with a series of std::shared_ptr to instances of T.
   * @param data: JSON struct specifying which components, and which attributes to retrieve
   *      Requires keys:
   *          @key component_ids: std::vector<int> of IDs of which containers to retrieve from.
   *          @key attribute_ids: std::vector<int> of IDs of which attributes to retrieve for each container.
   * @param rph: instance of handler, used in innermost loop to handle the different value types.
   * @param fx: the callable that compares component-id, cid, to the 'id' of the Tc.'id' (usually ->id)
   * @return
   */
  template <class T, class Fx>
  std::vector<json>
    get_attribute_value_table_fx(auto&& vecvals, std::vector<json> const & data, read_proxy_handler& rph, Fx&& fx) {
    std::vector<json> result;
    // Get out component_ids and attribute_ids:
    for (auto const & cdata : data) {
      auto cid = cdata.required<int>("component_id");
      auto attr_ids = cdata.required<std::vector<std::string>>("attribute_ids");
      std::shared_ptr<T> comp;
      // For each attribute container:
      {
        json comp_struct;
        comp_struct["component_id"] = cid;
        // Find container in range:
        auto it = std::ranges::find_if(vecvals, [&cid, &fx](auto const & el) {
          return fx(cid, el); //->id == cid;
        });
        if (it == std::ranges::end(vecvals)) {
          comp_struct["component_data"] = std::string("Unable to find component");
        } else {
          comp = std::dynamic_pointer_cast<T>(*it);
          comp_struct["component_data"] = get_proxy_attributes(*comp, attr_ids, rph);
        }
        result.push_back(comp_struct);
      }
    }
    return result;
  }

  /** ref to get_attribute_value_table_fx, this is with a prefabricated fx that compares the id of objects to cid */
  template <class T>
  auto get_attribute_value_table(auto&& vecvals, std::vector<json> const & data, read_proxy_handler& rph) {
    return get_attribute_value_table_fx<T>(vecvals, data, rph, [](int cid, auto const & c) {
      return c->id == cid;
    });
  }

  /** @brief generate a response for a read_attributes request */
  bg_work_result request_handler::handle_read_attribute_request(json const & data) {
    // Get model and HPS:
    auto mid = data.required<std::string>("model_key");
    return models
      .observe_or_throw(
        mid,
        [&](auto&& view) {
          auto& mdl = view.model;

          // Prepare response:
          auto req_id = data.required<std::string>("request_id");

          // We use a read_proxy_handler (visitor) to hold subscription and read data,
          // and pass that along to the innermost loop over components and attributes

          auto cb = [this](json const & data) {
            return handle_read_attribute_request(data);
          };
          read_proxy_handler vis(data, *this, cb);

          auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);
          auto sink = std::back_inserter(response);
          size_t n_sections = 0; // count that the request do contain a section.
          using hana::type_c;
          //---- EMIT DATA: ----//
          {
            emit_object<decltype(sink)> oo(sink);
            // tools for oo emitters,
            auto oo_def =
              [&n_sections, &vis](auto& oo, json const & data, std::string const & name, auto&& dv, auto tp) {
                using d_type = typename decltype(tp)::type;
                // check if there is a json section with the given name
                auto j_data = data.template optional<std::vector<json>>(name);
                if (j_data) {   // if yes, then
                  ++n_sections; // just count so we keep track
                  // does all needed magic rendering json
                  oo.def(name, get_attribute_value_table<d_type>(dv, *j_data, vis));
                }
              };

            auto oo_defx = [&oo_def](auto& oo, json const & data, std::string const & name, auto&& dv) {
              using d_type = typename std::remove_cvref_t<decltype(dv)>::value_type::element_type;
              oo_def(oo, data, name, dv, type_c<d_type>);
            };

            oo.def("model_key", mid);
            auto hps_data = data.optional<std::vector<json>>("hps");

            if (hps_data && (*hps_data).size()) {
              n_sections++;
              oo.def_fx("hps", [&vis, &mdl, mid, &hps_data, &oo_def](auto sink) {
                emit_vector_fx(sink, *hps_data, [&vis, &mdl, mid, &oo_def](auto oi, json const & hpsd) {
                  auto hps_id = hpsd.required<int>("hps_id");
                  auto it = std::ranges::find_if(mdl->hps, [&hps_id](auto ihps) {
                    return ihps->id == hps_id;
                  });
                  if (it == mdl->hps.end()) {
                    throw std::runtime_error(fmt::format("Unable to find HPS {} in model '{}'", hps_id, mid));
                  }
                  auto hps = *it;
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("hps_id", hps->id);
                  oo_def(oo, hpsd, "reservoirs", hps->reservoirs, type_c<stm::reservoir>);
                  oo_def(oo, hpsd, "units", hps->units, type_c<stm::unit>);
                  oo_def(oo, hpsd, "power_plants", hps->power_plants, type_c<stm::power_plant>);
                  oo_def(oo, hpsd, "waterways", hps->waterways, type_c<stm::waterway>);
                  oo_def(oo, hpsd, "gates", hps->gates(), type_c<stm::gate>);
                  oo_def(oo, hpsd, "catchments", hps->catchments, type_c<stm::catchment>);
                });
              });
            }

            oo_defx(oo, data, "markets", mdl->market);

            auto contract_data = data.optional<std::vector<json>>("contracts");
            if (contract_data && (*contract_data).size()) {
              n_sections++;
              oo.def_fx("contracts", [&vis, &mdl, mid, &contract_data](auto sink) {
                emit_vector_fx(sink, *contract_data, [&vis, &mdl, mid](auto oi, json const & cd) {
                  auto contract_id = cd.required<int>("component_id");
                  auto it = std::find_if(mdl->contracts.begin(), mdl->contracts.end(), [&contract_id](auto c) {
                    return c->id == contract_id;
                  });
                  if (it == mdl->contracts.end()) {
                    throw std::runtime_error(fmt::format("Unable to find contract {} in model '{}'", contract_id, mid));
                  }
                  auto contract = *it;

                  // first emit contract attribute (if any)
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("component_id", contract->id); // ensure to identify the contract regardless.

                  if (auto attr_ids = cd.optional<std::vector<std::string>>("attribute_ids");
                      attr_ids) { // any local attribute, like obligation.schedule etc.
                    oo.def("component_data", get_proxy_attributes(*contract, *attr_ids, vis));
                  }
                  if (auto r_attrs = cd.optional<std::vector<json>>("relations"); r_attrs) {
                    oo.def(
                      "relations",
                      get_attribute_value_table_fx<stm::contract_relation>(
                        contract->relations,
                        *r_attrs,
                        vis,
                        [](int id, std::shared_ptr<stm::contract_relation> const & cr) {
                          return id == cr->id;
                        }));
                  }
                  if (auto p_attrs = cd.optional<std::vector<json>>("power_plants"); p_attrs) {
                    oo.def(
                      "power_plants",
                      get_attribute_value_table_fx<stm::power_plant>(
                        contract->power_plants, *p_attrs, vis, [](int id, std::shared_ptr<stm::power_plant> const & p) {
                          return id == p->id;
                        }));
                  }
                });
              });
            }

            oo_defx(oo, data, "contract_portfolios", mdl->contract_portfolios);
            oo_defx(oo, data, "power_modules", mdl->power_modules);

            auto network_data = data.optional<std::vector<json>>(
              "networks"); // check if there is a json section with the given name
            if (network_data && (*network_data).size()) {
              ++n_sections; // just count so we keep track
              oo.def_fx("networks", [&vis, &mdl, mid, &network_data](auto sink) {
                emit_vector_fx(sink, *network_data, [&vis, &mdl, mid](auto oi, json const & networkd) {
                  auto network_id = networkd.required<int>("component_id");
                  auto it = std::find_if(mdl->networks.begin(), mdl->networks.end(), [&network_id](auto inet) {
                    return inet->id == network_id;
                  });
                  if (it == mdl->networks.end()) {
                    throw std::runtime_error(fmt::format("Unable to find Network {} in model '{}'", network_id, mid));
                  }
                  auto network = *it;
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("component_id", network->id);
                  if (auto tl_attrs = networkd.optional<std::vector<json>>("transmission_lines"); tl_attrs) {
                    oo.def(
                      "transmission_lines",
                      get_attribute_value_table_fx<stm::transmission_line>(
                        network->transmission_lines,
                        *tl_attrs,
                        vis,
                        [](int tid, std::shared_ptr<stm::transmission_line> const & tl) {
                          return tid == tl->id;
                        }));
                  }
                  if (auto busbar_data = networkd.optional<std::vector<json>>("busbars");
                      busbar_data && (*busbar_data).size() > 0) {
                    oo.def_fx("busbars", [&vis, &network, network_id, &busbar_data](auto sink) {
                      emit_vector_fx(sink, *busbar_data, [&vis, &network, network_id](auto oi, json const & busbard) {
                        auto busbar_id = busbard.required<int>("component_id");
                        auto it = std::ranges::find_if(network->busbars, [&busbar_id](auto ibus) {
                          return ibus->id == busbar_id;
                        });
                        if (it == network->busbars.end()) {
                          throw std::runtime_error(
                            fmt::format("Unable to find Busbar {} in network '{}'", busbar_id, network_id));
                        }
                        auto busbar = *it;
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("component_id", busbar->id);

                        if (auto attr_ids = busbard.optional<std::vector<std::string>>("attribute_ids"); attr_ids) {
                          oo.def(
                            "component_data",
                            get_proxy_attributes(
                              *busbar, *attr_ids, vis)); // any local attribute, like price.result etc.
                        }
                        if (auto unit_members = busbard.optional<std::vector<json>>("units"); unit_members) {
                          oo.def(
                            "units", // note that we use unit_member->unit->id as member id
                            get_attribute_value_table_fx<stm::unit_member>(
                              busbar->units,
                              *unit_members,
                              vis,
                              [](int cid, std::shared_ptr<stm::unit_member> const & um) {
                                return cid == um->unit->id;
                              }));
                        }
                        if (auto power_module_members = busbard.optional<std::vector<json>>("power_modules");
                            power_module_members) {
                          oo.def(
                            "power_modules", // note that we use power_module_member->power_module->id as member id
                            get_attribute_value_table_fx<stm::power_module_member>(
                              busbar->power_modules,
                              *power_module_members,
                              vis,
                              [](int cid, std::shared_ptr<stm::power_module_member> const & pm) {
                                return cid == pm->power_module->id;
                              }));
                        }
                      });
                    });
                  }
                });
              });
            }

            auto ug_data = data.optional<std::vector<json>>("unit_groups");
            if (ug_data && (*ug_data).size()) {
              n_sections++;
              oo.def_fx("unit_groups", [&vis, &mdl, mid, &ug_data](auto sink) {
                emit_vector_fx(sink, *ug_data, [&vis, &mdl, mid](auto oi, json const & ugd) {
                  auto ug_id = ugd.required<int>("component_id");
                  auto it = std::ranges::find_if(mdl->unit_groups, [&ug_id](auto iug) {
                    return iug->id == ug_id;
                  });
                  if (it == mdl->unit_groups.end()) {
                    throw std::runtime_error(fmt::format("Unable to find unit_groups {} in model '{}'", ug_id, mid));
                  }
                  auto ug = *it;

                  // first emit unit-group attribute (if any)
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("component_id", ug->id); // ensure to identify the unit group regardless.
                  auto attr_ids = ugd.optional<std::vector<std::string>>("attribute_ids");
                  if (attr_ids) { // any local attribute, like obligation.schedule etc.
                    oo.def("component_data", get_proxy_attributes(*ug, *attr_ids, vis));
                  }
                  // then, while in this unit-group, also check if there are member attributes requested: members
                  auto comp_attrs = ugd.optional<std::vector<json>>(
                    "members"); // from members we can retrieve is_active, maybe later some more..
                  if (comp_attrs)
                    oo.def(
                      "members", // notice that we use group-member->unit->id as member id
                      get_attribute_value_table_fx<stm::unit_group_member>(
                        ug->members,
                        *comp_attrs,
                        vis,
                        [](int cid, std::shared_ptr<stm::unit_group_member> const & ugm) {
                          return cid == ugm->unit->id;
                        }));
                });
              });
            }
            if (n_sections == 0) {
              throw std::runtime_error(
                fmt::format("Currently we require the hps or markets sections in the request: request_id={}", req_id));
            }
            oo_defx(oo, data, "wind_farms", mdl->wind_farms);
          }
          //---- RETURN RESPONSE: ------//
          response += "}";
          if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
          else
            return bg_work_result{response};
        })
      .get();
  }

  template <class T>
  std::vector<json> get_attribute_values(T const & t, read_proxy_handler& rph) {
    constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
    std::vector<json> result{};
    result.reserve(mp::leaf_accessor_count(hana::type_c<T>));
    hana::for_each(attr_paths, [&result, &t, &rph](auto m) {
      json attr_struct;
      attr_struct["attribute_id"] = std::string(mp::leaf_accessor_id_str(m));
      attr_struct["data"] = mp::leaf_access(t, m);
      if (rph.subscription) {
        rph.subscription->add_subscription(t, m);
      }
      result.push_back(attr_struct);
    });

    return result;
  }

  /** @brief handle run_params request
   *
   */
  bg_work_result request_handler::handle_run_params_request(json const & data) {
    auto req_id = data.required<std::string>("request_id");
    auto model_key = data.required<std::string>("model_key");
    return models
      .observe_or_throw(
        model_key,
        [&](auto&& view) {
          auto& mdl = view.model;
          // Generate result:
          json ndata = data;
          ndata["time_axis"] = generic_dt(); // Time_axis is required
          auto cb = [this](json const & data) {
            return handle_run_params_request(data);
          };
          read_proxy_handler vis(ndata, *this, cb);
          json result;
          result["model_key"] = model_key;
          result["values"] = get_attribute_values((mdl->run_params), vis);
          // Generate response:
          std::string response = "";
          auto sink = std::back_inserter(response);
          {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id).def("result", result);
          }
          if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
          else
            return bg_work_result{response};
        })
      .get();
  }

  bg_work_result request_handler::handle_opt_summary_request(json const & data) {
    auto req_id = data.required<std::string>("request_id");
    auto model_key = data.required<std::string>("model_key");
    return models
      .observe_or_throw(
        model_key,
        [&](auto&& view) {
          auto& mdl = view.model;
          // Generate result:
          json ndata = data;
          ndata["time_axis"] = generic_dt(); // Time_axis is required
          auto cb = [this](json const & data) {
            return handle_opt_summary_request(data);
          };
          read_proxy_handler vis(ndata, *this, cb);
          json result;
          result["model_key"] = model_key;
          result["values"] = get_attribute_values(*(mdl->summary), vis);
          // Generate response:
          std::string response = "";
          auto sink = std::back_inserter(response);
          {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id).def("result", result);
          }
          if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
          else
            return bg_work_result{response};
        })
      .get();
  }

  /** @brief handle get_log request */
  bg_work_result request_handler::handle_get_log_request(json const & data) {
    auto req_id = data.required<std::string>("request_id");
    auto model_key = data.required<std::string>("model_key");
    auto old_log_version = get_optional_value_or(data.optional<int>("log_version"), 0);
    auto bookmark = get_optional_value_or(data.optional<int>("bookmark"), 0);
    auto severity_level = get_optional_value_or(data.optional<int>("severity_level"), 0);

    auto [log, log_version, new_bookmark] =
      models
        .observe_or_throw(
          model_key,
          [&](auto&& view) {
            auto&& [state, log, log_version] = *view.info;
            // reset bookmark if log_version is new!
            auto n = log_version == old_log_version ? bookmark : 0;
            auto r = std::views::filter(std::views::drop(log, n), [&](auto const & x) {
              return etoi(x.severity) >= severity_level;
            });
            // FIXME: std::ranges::to - jeh
            return std::make_tuple(std::vector(std::ranges::begin(r), std::ranges::end(r)), log_version, log.size());
          })
        .get();
    bookmark = new_bookmark; // notice: as pr current semantic: the new_bookmark need to be at the log

    // Generate response, add log_version and 'bookmark', so that frontend/observer io can work incrementally
    // note: if front/observer changes log-level, then it should also reset the bookmark to 0.
    auto response = fmt::format(
      "{{\"request_id\":\"{}\",\"log_version\":{},\"bookmark\":{},\"result\":", req_id, log_version, bookmark);
    auto sink = std::back_inserter(response);
    log_entry_generator<decltype(sink)> msg_;
    {
      namespace ka = boost::spirit::karma;
      ka::rule<decltype(sink), std::vector<stm::log_entry>()> g = ka::lit('[') << -(msg_ % ',') << ka::lit(']');
      { generate(sink, g, log); }
      *sink++ = '}';
    }
    return bg_work_result{response};
  }

  bg_work_result request_handler::handle_get_state_request(json const & data) {
    auto req_id = data.required<std::string>("request_id");
    auto model_key = data.required<std::string>("model_key");

    auto state =
      models
        .observe_or_throw(
          model_key,
          [](auto&& view) {
            return view.info->state;
          })
        .get();

    // Prepare response:
    auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);
    auto sink = std::back_inserter(response);
    // Emit bare-bones model structure:
    {
      emit_object<decltype(sink)> oo(sink);
      oo.def("state", boost::describe::enum_to_string(state, "unknown"));
    }
    response += "}";
    return bg_work_result{response};
  }

}
