#include <shyft/web_api/energy_market/grammar.h>

namespace shyft::web_api::grammar {
  inline request make_request(string const & keyword, json const & request_data) {
    return request{keyword, request_data};
  }

  template <typename Iterator, typename Skipper>
  request_grammar<Iterator, Skipper>::request_grammar()
    : request_grammar::base_type(request_, "request") {
    keyword_ = lexeme[+(qi::alnum | char_("_"))];
    request_ = (keyword_ >> json_)[_val = phx::bind(make_request, _1, _2)];
    request_.name("request");
    on_error<fail>(request_, error_handler(_4, _3, _2));
  }

  template struct request_grammar<request_iterator_t, request_skipper_t>;
}

/*

*/
