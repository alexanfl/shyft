#include <shyft/web_api/energy_market/grammar.h>

namespace shyft::web_api::grammar {
  template <typename Iterator, typename Skipper>
  integer_list_grammar<Iterator, Skipper>::integer_list_grammar()
    : integer_list_grammar::base_type(start, "integer_list") {
    start = lit('[') >> -(integer_ % ",") >> ']'; // Valid strings are "[1,2,3,4,...]"
    start.name("integer_list");
    integer_.name("integer");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct integer_list_grammar<request_iterator_t, request_skipper_t>;
}
