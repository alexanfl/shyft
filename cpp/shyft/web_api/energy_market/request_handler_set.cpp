#include <shyft/web_api/energy_market/request_handler.h>

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/asio/thread_pool.hpp>
#include <boost/hana.hpp>
#include <boost/variant.hpp>
#include <boost/variant/static_visitor.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>

#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>
#include <shyft/mp.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

namespace shyft::web_api::energy_market {

  namespace mp = shyft::mp;
  namespace hana = boost::hana;
  using namespace shyft::energy_market;

  using shyft::web_api::generator::emit_object;
  using shyft::web_api::generator::emit_vector_fx;
  using shyft::time_series::dd::apoint_ts;

  template <class T1, class... Ts>
  constexpr auto is_one_of() noexcept {
    return (std::is_same_v<T1, Ts> || ...);
  }

  /** @brief handler class for setting attributes on a model.
   *
   */
  class set_attribute_handler {
    request_handler& handler;
    std::map<std::string, std::vector<std::string>> subs; ///< Keep track of what attributes have been changed
    bool merge; ///< Whether to merge values by the "shotgun method", i.e. keep all values that don't directly overlap.
    bool recreate;       ///< Remove the old value entirely
    bool cache_on_write; ///< if true(default), also cache shyft-time-series.
    std::string mkey;    ///< Model key that will need to be prefixed to all subscription updates.
   public:
    set_attribute_handler(
      request_handler& handler,
      bool merge = false,
      bool recreate = false,
      std::string const & mid = "",
      bool cache_on_write = true)
      : handler{handler}
      , merge{merge}
      , recreate{recreate}
      , cache_on_write{cache_on_write}
      , mkey{mid} {
      subs["time-series"] = {};
      subs["other"] = {};
    }

    /** @brief Need a override for attribute_value_type-variant, to safely unpack:
     *
     */
    template <class V>
    std::string apply(V& old_val, attribute_value_type const & new_val) {
      auto f = [this, &old_val](auto arg) {
        return this->apply(old_val, arg);
      };
      return boost::apply_visitor(f, new_val);
    }

    // Apply new_val to old_val based on the state of this, i.e. the values of merge, recreate and the server
    // to communicate with.
    template <class V>
    std::string apply(V& old_val, V const & new_val) {
      // Update with new values:
      std::string res;
      if (attr_traits::exists(old_val)) {
        res = merge_values(old_val, new_val);
      } else {
        old_val = new_val;
        res = "OK";
      }
      return res;
    }

    template <class V, class U>
    std::string apply(V& /*old_val*/, U const & /*new_val*/) {
      return "type mismatch";
    }

    // Notify accrued changes to the subscription managers:
    void notify_changes() {
      // Propagate/notify changes due to write of attributes (e.g. time-series)
      handler.sm->notify_change(subs["other"]);
      handler.dtss->sm->notify_change(subs["time_series"]); // notify the dtss sm about time-series changes
    }

    template <class T>
    std::string make_subscription_url(T const & t, std::string_view attr_id) const {
      std::string sub_id = "dstm://M" + mkey;
      sub_id.reserve(30);
      auto rbi = std::back_inserter(sub_id);
      t.generate_url(rbi);
      *rbi++ = '.';
      sub_id += attr_id;
      return sub_id;
    }

    template <class T>
    void add_tsm_subscription(T const & t, std::string_view attr_id) {
      subs["time_series"].push_back(make_subscription_url(t, attr_id));
    }

    template <class LeafAccessor>
    void add_sub_id(auto const & t, LeafAccessor&& la) {
      auto sub_id = make_subscription_url(t, mp::leaf_accessor_id_str(la));
      // Get value type of leaf_accessor:
      // auto value_type = mp::leaf_accessor_type(la);
      using V = typename decltype(+mp::leaf_accessor_type(std::declval<LeafAccessor>()))::type;
      // Send to correct subscription manager based on type:
      if constexpr (is_one_of<V, apoint_ts, absolute_constraint, penalty_constraint>()) {
        subs["time_series"].push_back(sub_id);
      } else {
        subs["other"].push_back(sub_id);
      }
    }

   private:
    std::string merge_values(std::uint16_t& old_v, const std::uint16_t new_v) {
      old_v = new_v; // just assign it.
      return "OK";
    }

    std::string merge_values(apoint_ts& old_val, apoint_ts const & new_val) {
      switch (stm::srv::dstm::ts_magic_merge_values(handler.dtss, old_val, new_val, merge, recreate, cache_on_write)) {
        using enum stm::srv::dstm::ts_merge_result;
      case saved_to_dtss:
        return "stored to dtss";
      case fail_expression:
        return "Time series is an expression. Cannot be set.";
      case fail_dtss:
        return "Cannot set dtss time series without dtss.";
      default:
        return "OK";
      }
    }

    std::string merge_values(absolute_constraint& old_val, absolute_constraint const & new_val) {
      merge_values(old_val.limit, new_val.limit);
      merge_values(old_val.flag, old_val.flag);
      return "OK";
    }

    std::string merge_values(penalty_constraint& old_val, penalty_constraint const & new_val) {
      merge_values(old_val.limit, new_val.limit);
      merge_values(old_val.flag, new_val.flag);
      merge_values(old_val.cost, new_val.cost);
      merge_values(old_val.penalty, new_val.penalty);
      return "OK";
    }

    template <class T>
    std::string merge_values(
      std::shared_ptr<std::map<utctime, std::shared_ptr<T>>>& old_val,
      std::shared_ptr<std::map<utctime, std::shared_ptr<T>>> const & new_val) {
      // We want to overwrite, so insert doesn't do the trick:
      if (!recreate || merge) {
        for (auto const & kv : *new_val) {
          (*old_val)[kv.first] = kv.second;
        }
      } else {
        old_val = new_val;
      }
      return "OK";
    }

    std::string merge_values(std::string& old_val, std::string const & new_val) {
      old_val = new_val;
      return "OK";
    }

    template <class T>
    std::string merge_values(T& old_val, T const & new_val) {
      old_val = new_val;
      return "NOT OK";
    }
  };

  auto get_attr_struct(std::vector<json> const & attr_vals, std::string const & attr_id) {
    return std::ranges::find_if(attr_vals, [&attr_id](json const & element) -> bool {
      // Here we check if the json structure e contains the key "attribute_id",
      // and if so, if it compares equal to the provided attr_id.
      auto opt = element.optional<std::string>("attribute_id");
      if (opt) {
        return *opt == attr_id;
      } else {
        return false;
      }
    });
  }

  template <class V>
  std::pair<bool, std::string> check_attribute_rules(std::string const & attribute_id) {
    if constexpr (std::is_same_v<V, stm::contract_relation>) {
      if (attribute_id == "relation_type") {
        return {false, "immutable attribute"};
      }
    }
    return {true, ""};
  }

  /** @brief
   *
   * @tparam T : Type whose attributes we want to change. Assumed to be a hana-struct
   * @param t : The instance of type T whose attribute values we want to change
   * @param attr_vals : The values to set or merge in t's attributes. Each entry of the vector requires the
   *   following:
   *      * "attribute_id" : <string> (ID of the attribute to change)
   *      * "value" : The value to be inserted/merged in.
   * @return a list of json. Each entry is the attribute_id that has been processed together with a "status",
   *  stating whether the change was successful or if it  encountered an error.
   */
  template <class T>
  std::vector<json> set_attribute_values(T& t, std::vector<json> const & attr_vals, set_attribute_handler& handler) {
    // Initialize result:
    std::vector<json> result;
    result.reserve(attr_vals.size());

    // Go through each attribute:
    constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
    hana::for_each(attr_paths, [&t, &attr_vals, &result, &handler](auto m) {
      auto attr_id = std::string(mp::leaf_accessor_id_str(m));
      auto it = get_attr_struct(attr_vals, attr_id);
      if (it != attr_vals.end()) { // If we found the current attribute amongst the ones to change
        // Initialize structure to be inserted into result
        json attr_res;
        attr_res["attribute_id"] = attr_id;
        // Get out value to be inserted/merged:
        auto value = it->required("value"); // value is now of type value_type -- a boost::variant
        // Set value
        auto f = [&handler, &t, &m, &attr_id](auto arg) {
          if (const auto [ok, status] = check_attribute_rules<T>(attr_id); !ok)
            return status;

          auto& attr = mp::leaf_access(t, m);
          return handler.apply(attr, arg);
        };
        // Alternatively, we could have used hana::partial like:
        // auto f = hana::partial([&handler](auto& attr, auto arg) { return handler.apply(attr, arg); },
        // mp::leaf_access(t,m));
        attr_res["status"] = boost::apply_visitor(f, value);
        // Add subscription ID to notify change later:
        handler.add_sub_id(t, m);
        // Insert into result vector:
        result.push_back(attr_res);
      }
    });
    if constexpr (
      !std::is_same_v<T, stm::unit_group_member> && !std::is_same_v<T, stm::unit_member>
      && !std::is_same_v<T, stm::power_module_member> && !std::is_same_v<T, stm::contract_relation>) {
      for (auto const & a : attr_vals) {
        auto attr_id = a.required<std::string>("attribute_id");
        if (attr_id.rfind("ts.", 0) == 0) {
          auto value = a.required<attribute_value_type>("value");
          json attr_res;
          attr_res["attribute_id"] = attr_id;
          auto tsi = t.tsm.find(attr_id.substr(3));
          if (tsi == t.tsm.end()) {
            attr_res["status"] = "not found";
          } else {
            attr_res["status"] = handler.apply(tsi->second, value);
            handler.add_tsm_subscription(t, attr_id); // could work.
          }
          result.push_back(attr_res);
        } else if (attr_id == "json") {
          auto value = a.required<std::string>("value");
          json attr_res;
          attr_res["attribute_id"] = attr_id;
          attr_res["status"] = handler.apply(t.json, value);
          result.push_back(attr_res);
        } // else already handled
      }
    }
    return result;
  }

  struct check_empty_visitor : boost::static_visitor<bool> {
    template <typename T>
    bool operator()(std::vector<T> const & t) const {
      return t.size() == 0;
    }

    template <typename T>
    bool operator()(T const &) const {
      return true;
    }
  };

  /** @brief Sets a list of proxy attributes from a list of T specified by data.
   *
   * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
   * @tparam Fx a callable(int,ptr_type const&)->bool used to locate the wanted component
   *
   * @param vecvals the vector with a series of references to instances of T.
   * @param data: JSON struct specifying which components, and which attributes to retrieve
   *      Requires keys:
   *          @key component_ids: std::vector<int> of IDs of which containers to retrieve from.
   *          @key attribute_ids: std::vector<int> of IDs of which attributes to retrieve for each container.
   *          @key values: std::vector<attribute_value_type> of values to set for each component and attribute
   *              Must have length equal to |component_ids| * |attribute_ids|.
   * @param handler: the handler we use to record/repeat the request (incase subscribe)
   * @param fx: The callable(int cid,ptr_type const&c)->bool, that should return true if cid matches id of comp
   * @return vector with json
   */
  template <class T, class Fx>
  std::vector<json> set_attribute_values_vector_fx(
    auto&& vecvals,
    std::vector<json> const & data,
    set_attribute_handler& handler,
    Fx&& fx) {
    std::vector<json> result;

    std::shared_ptr<T> comp;
    // Iterate over each element of the json-list:
    for (auto& comp_data : data) {
      // Each element of the vector is required to have:
      // int component_id
      // std::vector<json> attribute_data
      auto cid = comp_data.required<int>("component_id");
      auto temp = comp_data.required("attribute_data"); // This pattern is required because an empty list would not
                                                        // parse successfully to std::vector<json>
      std::vector<json> attr_vals;
      if (!boost::apply_visitor(check_empty_visitor(), temp)) {
        attr_vals = comp_data.required<std::vector<json>>("attribute_data");
      }
      // Initialize json to store results for current component:
      json comp_struct;
      comp_struct["component_id"] = cid;
      // Get out component:
      auto it = std::ranges::find_if(vecvals, [cid, &fx](auto e) {
        return fx(cid, e);
      });
      if (it != std::ranges::end(vecvals)) {
        // We need to downcast the component because of inheritance.
        if ((comp = dynamic_pointer_cast<T>(*it))) {
          comp_struct["status"] = set_attribute_values(
            *comp, attr_vals, handler); // set_attribute_values(comp, attr_vals....)
        } else {
          comp_struct["status"] = "Unable to cast hydro component";
        }
      } else {
        comp_struct["status"] = "Unable to find component";
      }
      // Add component's json to result list:
      result.push_back(comp_struct);
    }
    return result;
  }

  /** ref set_attribute_values_vector_fx, this one with a ready-shipped fx that compares to c->id */
  template <class T>
  std::vector<json>
    set_attribute_values_vector(auto&& vecvals, std::vector<json> const & data, set_attribute_handler& handler) {
    return set_attribute_values_vector_fx<T>(vecvals, data, handler, [](int cid, auto const & c) {
      return cid == c->id;
    });
  }

  bg_work_result request_handler::handle_set_attribute_request(json const & data) {
    // Get model and HPS:
    auto mid = data.required<std::string>("model_key");

    return models
      .mutate_or_throw(
        mid,
        [&](auto&& view) {
          auto mdl = view.model;

          // Prepare response:
          auto req_id = data.required<std::string>("request_id");
          auto response = fmt::format("{{\"request_id\":\"{}\",\"result\":", req_id);

          auto merge = boost::get_optional_value_or(data.optional<bool>("merge"), false);
          auto recreate = boost::get_optional_value_or(data.optional<bool>("recreate"), false);
          auto cache_on_write = boost::get_optional_value_or(data.optional<bool>("cache_on_write"), true);
          // Set up set_attribute_handler;
          set_attribute_handler handler(*this, merge, recreate, mid, cache_on_write);

          auto sink = std::back_inserter(response);
          //---- EMIT DATA: ----//
          {

            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid);
            auto n_sections = 0u;
            // some tools to auto roll out the types:
            using hana::type_c;
            /**
             * oo_def checks if the json data have the specified key,
             * then if it is there, get the specs from it, and apply them to
             * the corresponding energy market model object.
             */
            auto oo_def =
              [&n_sections, &handler](auto& oo, json const & data, std::string const & name, auto&& dv, auto tp) {
                using d_type =
                  typename decltype(tp)::type; // std::remove_cvref_t<decltype(dv)>::value_type::element_type;
                auto j_data = data.template optional<std::vector<json>>(
                  name);                          // check if there is a json section with the given name
                if (j_data && (*j_data).size()) { // if yes, then just count so we keep track
                  ++n_sections;

                  // does all needed magic rendering json
                  oo.def(name, set_attribute_values_vector<d_type>(dv, *j_data, handler));
                }
              };
            /// short hand when the type to process is the same as vector type (e.g.stm only)
            auto oo_defx = [&oo_def](auto& oo, auto const & data, std::string const & name, auto&& dv) {
              using d_type = typename std::remove_cvref_t< decltype(dv)>::value_type::element_type;
              return oo_def(oo, data, name, dv, type_c<d_type>);
            };

            auto hps_data = data.optional<std::vector<json>>("hps");
            if (hps_data && (*hps_data).size()) {
              n_sections++;
              oo.def_fx("hps", [&mdl, mid, &hps_data, &oo_def](auto sink) {
                emit_vector_fx(sink, *hps_data, [&mdl, mid, &oo_def](auto oi, json const & hpsd) {
                  auto hps_id = hpsd.required<int>("hps_id");
                  auto it = std::ranges::find_if(mdl->hps, [&hps_id](auto ihps) {
                    return ihps->id == hps_id;
                  });
                  if (it == mdl->hps.end()) {
                    throw std::runtime_error(fmt::format("Unable to find hps {} in model '{}'", hps_id, mid));
                  }
                  auto hps = *it;
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("hps_id", hps->id);
                  oo_def(oo, hpsd, "reservoirs", hps->reservoirs, type_c<stm::reservoir>);
                  oo_def(oo, hpsd, "units", hps->units, type_c<stm::unit>);
                  oo_def(oo, hpsd, "power_plants", hps->power_plants, type_c<stm::power_plant>);
                  oo_def(oo, hpsd, "waterways", hps->waterways, type_c<stm::waterway>);
                  oo_def(oo, hpsd, "gates", hps->gates(), type_c<stm::gate>);
                  oo_def(oo, hpsd, "catchments", hps->catchments, type_c<stm::catchment>);
                });
              });
            }


            oo_defx(oo, data, "markets", mdl->market);

            auto contract_data = data.optional<std::vector<json>>("contracts");
            if (contract_data && (*contract_data).size()) {
              n_sections++;
              oo.def_fx("contracts", [&handler, &mdl, mid, &contract_data](auto sink) {
                emit_vector_fx(sink, *contract_data, [&handler, &mdl, mid](auto oi, json const & cd) {
                  auto contract_id = cd.required<int>("component_id");
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("component_id", contract_id); // we can tell which component it is,
                  auto it = std::ranges::find_if(mdl->contracts, [&contract_id](auto c) {
                    return c->id == contract_id;
                  });
                  if (it == mdl->contracts.end()) {
                    oo.def("status", "Unable to find component"); // and we did not find it, just a message, no throw
                  } else {
                    auto contract = *it;
                    // set contract attributes here
                    auto temp = cd.required("attribute_data"); // This pattern is required because an empty list would
                                                               // not parse successfully to std::vector<json>
                    std::vector<json> attr_vals;
                    if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                      attr_vals = cd.required<std::vector<json>>("attribute_data");
                    }

                    oo.def("status", set_attribute_values(*contract, attr_vals, handler));
                    // Set contract relation attributes
                    if (auto comp_attrs = cd.optional<std::vector<json>>("relations"); comp_attrs) {
                      oo.def(
                        "relations",
                        set_attribute_values_vector_fx<stm::contract_relation>(
                          contract->relations,
                          *comp_attrs,
                          handler,
                          [](int id, std::shared_ptr<stm::contract_relation> const & cr) {
                            return cr->id == id;
                          }));
                    }
                    if (auto comp_attrs = cd.optional<std::vector<json>>("power_plants"); comp_attrs) {
                      oo.def(
                        "power_plants",
                        set_attribute_values_vector_fx<stm::power_plant>(
                          contract->power_plants,
                          *comp_attrs,
                          handler,
                          [](int id, std::shared_ptr<stm::power_plant> const & pp) {
                            return pp->id == id;
                          }));
                    }
                  }
                });
              });
            }


            oo_defx(oo, data, "contract_portfolios", mdl->contract_portfolios);
            oo_defx(oo, data, "power_modules", mdl->power_modules);

            auto network_data = data.optional<std::vector<json>>("networks");
            if (network_data && (*network_data).size()) {
              ++n_sections; // just count so we keep track
              oo.def_fx("networks", [&handler, &mdl, mid, &network_data](auto sink) {
                emit_vector_fx(sink, *network_data, [&handler, &mdl, mid](auto oi, json const & networkd) {
                  auto network_id = networkd.required<int>("component_id");
                  auto it = std::ranges::find_if(mdl->networks, [&network_id](auto inet) {
                    return inet->id == network_id;
                  });
                  if (it == mdl->networks.end()) {
                    throw std::runtime_error(fmt::format("Unable to find Network {} in model '{}'", network_id, mid));
                  }
                  auto network = *it;
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("component_id", network->id);
                  if (auto tl_attrs = networkd.optional<std::vector<json>>("transmission_lines"); tl_attrs) {
                    oo.def(
                      "transmission_lines",
                      set_attribute_values_vector_fx<stm::transmission_line>(
                        network->transmission_lines,
                        *tl_attrs,
                        handler,
                        [](int tid, std::shared_ptr<stm::transmission_line> const & tl) {
                          return tid == tl->id;
                        }));
                  }
                  if (auto busbar_data = networkd.optional<std::vector<json>>("busbars");
                      busbar_data && (*busbar_data).size() > 0) {
                    oo.def_fx("busbars", [&handler, &network, network_id, &busbar_data](auto sink) {
                      emit_vector_fx(
                        sink, *busbar_data, [&handler, &network, network_id](auto oi, json const & busbard) {
                          auto busbar_id = busbard.required<int>("component_id");
                          auto it = std::ranges::find_if(network->busbars, [&busbar_id](auto ibus) {
                            return ibus->id == busbar_id;
                          });
                          if (it == network->busbars.end()) {
                            throw std::runtime_error(
                              fmt::format("Unable to find Busbar {} in network '{}'", busbar_id, network_id));
                          }
                          auto busbar = *it;
                          // This pattern is required because an empty list would not parse
                          // successfully to std::vector<json>
                          auto temp = busbard.required("attribute_data");
                          std::vector<json> attr_vals;
                          if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                            attr_vals = busbard.required<std::vector<json>>("attribute_data");
                          }
                          emit_object<decltype(oi)> oo(oi);
                          oo.def("status", set_attribute_values(*busbar, attr_vals, handler));

                          if (auto unit_members = busbard.optional<std::vector<json>>("units"); unit_members) {
                            oo.def(
                              "units",
                              set_attribute_values_vector_fx<stm::unit_member>(
                                busbar->units,
                                *unit_members,
                                handler,
                                [](int cid, std::shared_ptr<stm::unit_member> const & um) {
                                  return um->unit->id == cid;
                                }));
                          }
                          if (auto power_module_members = busbard.optional<std::vector<json>>("power_modules");
                              power_module_members) {
                            oo.def(
                              "power_modules",
                              set_attribute_values_vector_fx<stm::power_module_member>(
                                busbar->power_modules,
                                *power_module_members,
                                handler,
                                [](int cid, std::shared_ptr<stm::power_module_member> const & pm) {
                                  return pm->power_module->id == cid;
                                }));
                          }
                        });
                    });
                  }
                });
              });
            }

            auto ug_data = data.optional<std::vector<json>>("unit_groups");
            if (ug_data && (*ug_data).size()) {
              n_sections++;
              oo.def_fx("unit_groups", [&handler, &mdl, mid, &ug_data](auto sink) {
                emit_vector_fx(sink, *ug_data, [&handler, &mdl, mid](auto oi, json const & ugd) {
                  auto ug_id = ugd.required<int>("component_id");
                  emit_object<decltype(oi)> oo(oi);
                  oo.def("component_id", ug_id); // we can tell which component it is,

                  auto it = std::ranges::find_if(mdl->unit_groups, [&ug_id](auto iug) {
                    return iug->id == ug_id;
                  });
                  if (it == mdl->unit_groups.end()) {
                    oo.def("status", "Unable to find component"); // and we did not find it, just a message, no throw
                  } else {
                    auto ug = *it;
                    // set unit_group attributes here
                    auto temp = ugd.required("attribute_data"); // This pattern is required because an empty list would
                                                                // not parse successfully to std::vector<json>
                    std::vector<json> attr_vals;
                    if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                      attr_vals = ugd.required<std::vector<json>>("attribute_data");
                    }
                    // set_attribute_values(comp, attr_vals....)
                    oo.def("status", set_attribute_values(*ug, attr_vals, handler));
                    // Set unit-group member attributes(is_active, and we could consider unit attribute propagation)
                    auto comp_attrs = ugd.optional<std::vector<json>>("members");
                    if (comp_attrs)
                      oo.def(
                        "members",
                        set_attribute_values_vector_fx<stm::unit_group_member>(
                          ug->members,
                          *comp_attrs,
                          handler,
                          [](int cid, std::shared_ptr<stm::unit_group_member> const & ugm) {
                            return ugm->unit->id == cid;
                          }));
                  }
                });
              });
            }
            oo_defx(oo, data, "wind_farms", mdl->wind_farms);
            if (n_sections == 0) {
              throw std::runtime_error(
                fmt::format("Currently we require the hps or market attribute: request_id={}", req_id));
            }
          }
          handler.notify_changes();
          //---- RETURN RESPONSE: ------//
          response += "}";
          return bg_work_result{response};
        })
      .get();
  }
}
