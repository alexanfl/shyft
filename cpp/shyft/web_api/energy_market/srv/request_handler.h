/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <map>
#include <iosfwd>

#include <shyft/web_api/json_struct.h>    // For proxy_attr_range and json
#include <shyft/web_api/bg_work_result.h> // For bg_work_result

#include <shyft/core/subscription.h>

#include <shyft/srv/db.h>
#include <shyft/srv/server.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>

namespace shyft::energy_market::srv {
  template <class DB>
  struct server;

}

namespace shyft::web_api::energy_market::srv {
  using shyft::web_api::energy_market::request;
  using shyft::web_api::energy_market::json;
  using shyft::core::subscription::observer_base_;
  using shyft::srv::server;
  using shyft::core::utcperiod;

  /** @brief Background service that has a pointer
   * to a shyft::srv::server<DB>.
   * The Boost beast foreground io-service receives
   * websocket messages
   * posts these to the bg thread for processing
   * the bg does the following:
   * 1) Parses the request, using boost::spirit::qi
   * 2) forwards the request to the server
   * 3) using boost::spirit::karma, generate the json-esque response
   * 4) emit the result to the output buffer
   * 5) forwards it to the boost beast io-services
   *
   * @tparam Server: Type of server the request_handler should interface with.
   *      Should be a specialization of shyft::srv::server<DB>,
   *      or derived from it
   *      (See shyft/srv/server.h> for further info).
   */
  template <class Server>
  struct request_handler : base_request_handler {
    Server* srv{nullptr};

    /** @brief callback function to be used in beast server
     * parses request into a request struct.
     * On successful parse, passes it on to the handler function
     * and passes on response to server
     * On failure, passes on error message as string.
     *
     * @param input: string to parse and process
     * @return bg_work_result for the server to handle.
     */
    bg_work_result do_the_work(string const & input) override;
    bg_work_result do_subscription_work(observer_base_ const & o) override;

    string generate_model_infos_response(string const & request_id, vector<int64_t> const & mids, utcperiod per);
    string generate_read_model_response(string const & request_id, int64_t mid);
    /** @brief main dispatch for messages
     *
     * @param req with keword and json struct of request data
     * @return response string as bg_work_result
     */
   protected:
    virtual bool handle_request(request const & req, bg_work_result& resp);

   private:
    // Specific handlers for each type of request:
    bool handle_get_model_infos_request(json const & data, bg_work_result& resp);
    bool handle_read_model_request(json const & data, bg_work_result& resp);
    bool handle_update_model_info_request(json const & data, bg_work_result& resp);
    bool handle_store_model_request(json const & data, bg_work_result& resp);
    bool handle_unsubscribe_request(json const & data, bg_work_result& resp);
    bool handle_remove_model_request(json const & data, bg_work_result& resp);
  };

}
