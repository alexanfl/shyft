#include <shyft/web_api/energy_market/srv/grammar.h>

namespace shyft::web_api::grammar {
  void set_model_ref(shared_ptr<model_ref>& r, model_ref i) {
    r = std::make_shared<model_ref>(i);
  }

  template <class Iterator, class Skipper>
  stm_case_grammar<Iterator, Skipper>::stm_case_grammar()
    : stm_case_grammar::base_type(start, "stm_run") {
    labels_ = lit('[') >> -(string_ % ',') >> lit(']');
    mr_ptr_ = mr_[phx::bind(set_model_ref, _val, _1)];
    mr_list_ = lit('[') >> -(mr_ptr_ % ',') >> lit(']');

    start =
      lit('{') >> lit("\"id\"") >> ':' >> int_[phx::bind(&stm_case::id, _val) = _1] >> ',' >> lit("\"name\"") >> ':'
      >> string_[phx::bind(&stm_case::name, _val) = _1] >> (-(',' >> lit("\"created\"") >> ':' >> time_))
        [phx::bind(&stm_case::created, _val) = phx::bind(
           [](auto const & t) {
             return t ? *t : core::utctime_now();
           },
           _1)]
      >> (-(
        ',' >> lit("\"json\"") >> ':'
        >> string_))[phx::bind(&stm_case::json, _val) = phx::bind(get_value_or<string>, _1, string(""))]
      >> (-(
        ',' >> lit("\"labels\"") >> ':'
        >> labels_))[phx::bind(&stm_case::labels, _val) = phx::bind(get_value_or<vector<string>>, _1, vector<string>{})]
      >> (-(',' >> lit("\"model_refs\"") >> ':' >> mr_list_))
        [phx::bind(&stm_case::model_refs, _val) = phx::bind(get_value_or<vector<model_ref_>>, _1, vector<model_ref_>{})]
      >> lit('}');
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct stm_case_grammar<request_iterator_t, request_skipper_t>;
}
