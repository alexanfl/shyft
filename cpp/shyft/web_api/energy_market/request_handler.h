/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <functional>
#include <iosfwd>
#include <map>
#include <memory>
#include <string>

#include <boost/asio/thread_pool.hpp>
#include <dlib/logger.h>

#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/srv/model_info.h>
#include <shyft/web_api/bg_work_result.h> // For bg_work_result
#include <shyft/web_api/json_struct.h>

namespace shyft::web_api::energy_market {

  struct request; // fwd decl

  // responses

  // background service that
  // have a shared_ptr<stm_server>
  // the boost beast foreground io-service receives ws messages
  // posts those to the bg thread for processing
  // the bg worker does following:
  // parses the request using boost.spirit.qi
  // forward the request to the server
  // using boost.spirit.karma, generate the json-like response
  // emit the result to the output buffer
  // forward it to the boost beast io-services
  struct request_handler : base_request_handler {

    static dlib::logger log;

    shyft::energy_market::stm::shared_models<boost::asio::thread_pool::executor_type> &models;
    std::shared_ptr<core::subscription::manager> sm;
    std::function<bool(std::string, std::string)> &fx_cb;
    dtss::server *dtss;

    request_handler(
      shyft::energy_market::stm::shared_models<boost::asio::thread_pool::executor_type> &models,
      std::shared_ptr<core::subscription::manager> sm,
      std::function<bool(std::string, std::string)> &fx_cb,
      dtss::server *dtss)
      : base_request_handler()
      , models{models}
      , sm{sm}
      , fx_cb{fx_cb}
      , dtss{dtss} {
    }

    auto &ts_sm() {
      if (dtss)
        return dtss->sm;
      return sm;
    }

    auto const &ts_sm() const {
      if (dtss)
        return dtss->sm;
      return sm;
    }

    /** @brief callback function to be used in beast server.
     * Parses request into a request struct.
     * On successful parse passes it on to handler function
     * and passes on response to server.
     * On failure passes on error message as string.
     * @param input string to parse and process.
     * @return bg_work_result for server to handle.
     **/
    bg_work_result do_the_work(std::string const &input) override;
    bg_work_result do_subscription_work(std::shared_ptr<core::subscription::observer_base> const &o) override;

    /** @brief main dispatch for messages
     *
     * @param req with keyword and json struct of request data
     * @return response string
     **/
    bg_work_result handle_request(request const &req);
   private:
    // Various specialized functions for handling each specific request type:
    // The dispatch is done in handle_request
    bg_work_result handle_read_attribute_request(json const &data);
    bg_work_result handle_read_model_request(json const &data);
    bg_work_result handle_get_model_infos_request(json const &data);
    bg_work_result handle_get_hydro_components_request(json const &data);
    bg_work_result handle_get_contract_components_request(json const &data);
    bg_work_result handle_get_attribute_mapping_request(json const &data);
    bg_work_result handle_set_attribute_request(json const &data);
    bg_work_result handle_unsubscribe_request(json const &data);
    bg_work_result handle_fx_request(json const &data);
    bg_work_result handle_run_params_request(json const &data);
    bg_work_result handle_opt_summary_request(json const &data);
    bg_work_result handle_get_log_request(json const &data);
    bg_work_result handle_get_state_request(json const &data);
  };
}
