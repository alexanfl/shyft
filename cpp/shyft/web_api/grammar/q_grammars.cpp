#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  //-- impl. cpp

  template <typename Iterator, typename Skipper>
  q_list_request_grammar<Iterator, Skipper>::q_list_request_grammar()
    : q_list_request_grammar::base_type(start, "q_list_request_grammar") {
    constexpr auto construct = [](string const &a) {
      return q_list_request{a};
    };
    start =
      ((lit("q_list") >> '{') > lit("\"request_id\"") > ':' > quoted_string
       > lit('}'))[_val = phx::bind(construct, _1)];
    start.name("q_get_response");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_list_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_list_response_grammar<Iterator, Skipper>::q_list_response_grammar()
    : q_list_response_grammar::base_type(start, "q_list_response_grammar") {
    static constexpr auto construct = [](string const &req_id, vector<string> const &q_names) -> q_list_response {
      return q_list_response{req_id, q_names};
    };
    start =
      (lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_names\"") > ':' > '['
       > (quoted_string % ',') > ']' > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_list_response");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_list_response_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_info_request_grammar<Iterator, Skipper>::q_info_request_grammar()
    : q_info_request_grammar::base_type(start, "q_info_request_grammar") {
    constexpr auto construct = [](string const &r, string const &q, string const &m) {
      return q_info_request{r, q, m};
    };
    start =
      ((lit("q_info") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > ',' > lit("\"msg_id\"") > ':' > quoted_string
       > lit('}'))[_val = phx::bind(construct, _1, _2, _3)];
    start.name("q_info_request_grammar");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_info_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_done_request_grammar<Iterator, Skipper>::q_done_request_grammar()
    : q_done_request_grammar::base_type(start, "q_done_request_grammar") {
    constexpr auto construct = [](string const &r, string const &q, string const &m, string const &d) {
      return q_done_request{r, q, m, d};
    };
    start =
      ((lit("q_done") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > ',' > lit("\"msg_id\"") > ':' > quoted_string > ',' > lit("\"diagnostics\"") > ':'
       > quoted_string > lit('}'))[_val = phx::bind(construct, _1, _2, _3, _4)];
    start.name("q_done_request_grammar");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_done_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_info_response_grammar<Iterator, Skipper>::q_info_response_grammar()
    : q_info_response_grammar::base_type(start, "q_info_response_grammar") {
    static constexpr auto construct = [](string const &req_id, queue::msg_info const &info) {
      return q_info_response{req_id, info};
    };
    start =
      (lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"info\"") > ':' > msg_info_
       > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_list_response");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_info_response_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_infos_request_grammar<Iterator, Skipper>::q_infos_request_grammar()
    : q_infos_request_grammar::base_type(start, "q_infos_request_grammar") {
    constexpr auto construct = [](string const &r, string const &q) {
      return q_infos_request{r, q};
    };
    start =
      ((lit("q_infos") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_info_request_grammar");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_infos_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_size_response_grammar<Iterator, Skipper>::q_size_response_grammar()
    : q_size_response_grammar::base_type(start, "q_size_response_grammar") {
    static constexpr auto construct = [](string const &req_id, int count) {
      return q_size_response{req_id, count};
    };
    start =
      (lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"count\"") > ':' > qi::uint_
       > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_size_response");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_size_response_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_size_request_grammar<Iterator, Skipper>::q_size_request_grammar()
    : q_size_request_grammar::base_type(start, "q_size_request_grammar") {
    constexpr auto construct = [](string const &r, string const &q) {
      return q_size_request{r, q};
    };
    start =
      ((lit("q_size") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_size_request_grammar");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_size_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_maintain_request_grammar<Iterator, Skipper>::q_maintain_request_grammar()
    : q_maintain_request_grammar::base_type(start, "q_maintain_request_grammar") {
    constexpr auto construct = [](string const &r, string const &q, bool k, bool f) {
      return q_maintain_request{r, q, k, f};
    };
    start =
      ((lit("q_maintain") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > ',' > lit("\"keep_ttl_items\"") > ':' > bool_ > ',' > lit("\"flush_all\"") > ':' > bool_ >

       lit('}'))[_val = phx::bind(construct, _1, _2, _3, _4)];
    start.name("q_maintain_request_grammar");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_maintain_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_infos_response_grammar<Iterator, Skipper>::q_infos_response_grammar()
    : q_infos_response_grammar::base_type(start, "q_infos_response_grammar") {
    static constexpr auto construct = [](string const &req_id, boost::optional<vector<queue::msg_info>> const &infos) {
      return infos ? q_infos_response{req_id, *infos} : q_infos_response{req_id, vector<queue::msg_info>{}};
    };
    start =
      (lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"infos\"") > ':' > '[' > -(msg_info_ % ',')
       > ']' > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_list_response");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_infos_response_grammar<request_iterator_t, request_skipper_t>;


  using null_or_tsv_msg =
    boost::optional<queue::tsv_msg>; // using an or parser ->variant(string,tsv_msg) for dealing with opt shared_ptr

  inline q_get_response mk_q_get_response(string const &req_id, null_or_tsv_msg const &msg) {
    queue::tsv_msg_ tsv_msg_ptr;
    if (msg) {
      tsv_msg_ptr = std::make_shared<queue::tsv_msg>(msg.value()); // default null
    }
    return q_get_response{req_id, tsv_msg_ptr};
  }

  template <typename Iterator, typename Skipper>
  q_get_response_grammar<Iterator, Skipper>::q_get_response_grammar()
    : q_get_response_grammar::base_type(start, "q_get_response_grammar") {

    start =
      (lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"msg\"") > ':' > (q_tsv_msg_ | lit("null"))
       > // response is msg:null if no message
       lit('}'))[_val = phx::bind(mk_q_get_response, _1, _2)];
    start.name("q_get_response");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct q_get_response_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_tsv_msg_grammar<Iterator, Skipper>::q_tsv_msg_grammar()
    : q_tsv_msg_grammar::base_type(start, "q_tsv_msg_grammar") {
    static constexpr auto construct = [](queue::msg_info const &mi, ats_vector const &tsv) {
      return queue::tsv_msg{mi, tsv};
    };
    start =
      (lit('{') > lit("\"info\"") > ':' > info_ > ',' > lit("\"tsv\"") > ':' > tsv_
       > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_tsv_msg");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct q_tsv_msg_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_msg_info_grammar<Iterator, Skipper>::q_msg_info_grammar()
    : q_msg_info_grammar::base_type(start, "q_msg_info_grammar") {
    static constexpr auto construct =
      [](
        std::string const &msg_id,
        string const &description,
        utctime ttl,
        utctime created,
        utctime fetched,
        utctime done,
        string const &diag) {
        return queue::msg_info{msg_id, description, ttl, created, fetched, done, diag};
      };
    start =
      (lit('{') > lit("\"msg_id\"") > ':' > quoted_string > ',' > lit("\"description\"") > ':' > quoted_string > ','
       > lit("\"ttl\"") > ':' > t_ > ',' > lit("\"created\"") > ':' > t_ > ',' > lit("\"fetched\"") > ':' > t_ > ','
       > lit("\"done\"") > ':' > t_ > ',' > lit("\"diagnostics\"") > ':' > quoted_string
       > lit('}'))[_val = phx::bind(construct, _1, _2, _3, _4, _5, _6, _7)];
    start.name("q_msg_info");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct q_msg_info_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_put_request_grammar<Iterator, Skipper>::q_put_request_grammar()
    : q_put_request_grammar::base_type(start, "q_put_request_request") {
    static constexpr auto construct =
      [](
        std::string const &req_id,
        string const &q_name,
        string const &msg_id,
        string const &description,
        utctime ttl,
        ats_vector const &tsv) {
        return q_put_request{req_id, q_name, msg_id, description, ttl, tsv};
      };
    start =
      ((lit("q_put") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > ',' > lit("\"msg_id\"") > ':' > quoted_string > ',' > lit("\"description\"") > ':'
       > quoted_string > ',' > lit("\"ttl\"") > ':' > t_ > ',' > lit("\"tsv\"") > ':' > tsv_
       > lit('}'))[_val = phx::bind(construct, _1, _2, _3, _4, _5, _6)];
    start.name("q_put_request");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct q_put_request_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper>
  q_get_request_grammar<Iterator, Skipper>::q_get_request_grammar()
    : q_get_request_grammar::base_type(start, "q_get_request") {
    static constexpr auto construct = [](std::string const &req_id, string const &q_name) {
      return q_get_request{req_id, q_name};
    };
    start =
      ((lit("q_get") >> lit('{')) > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"q_name\"") > ':'
       > quoted_string > lit('}'))[_val = phx::bind(construct, _1, _2)];
    start.name("q_put_request");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct q_get_request_grammar<request_iterator_t, request_skipper_t>;

}
