#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline percentile_ts_request mk_percentile_ts_request(
    string const & request_id,
    utcperiod const & r,
    shyft::time_axis::generic_dt const & ta,
    std::vector<int> const & pct,
    bool cache,
    vector<string> const & ids,
    boost::optional<bool> opt_sub,
    boost::optional<bool> ts_fmt) {
    return percentile_ts_request{
      request_id, r, ta, pct, cache, ids, opt_sub.get_value_or(false), ts_fmt.get_value_or(false)};
  }

  template <typename Iterator, typename Skipper>
  percentile_ts_request_grammar<Iterator, Skipper>::percentile_ts_request_grammar()
    : percentile_ts_request_grammar::base_type(start, "percentile_ts_request") {

    start =
      (lit("percentiles") > lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"read_period\"") > ':'
       > p_ > ',' > lit("\"time_axis\"") > ':' > time_axis_ > ',' > lit("\"percentiles\"") > ':' > lit('[')
       > (int_ % ',') > lit(']') >> ',' > lit("\"cache\"") > ':' > qi::bool_ > ',' > lit("\"ts_ids\"") > ':' > lit('[')
       > (quoted_string % ',') > lit(']') > -(',' > lit("\"subscribe\"") > ':' > bool_)
       > -(',' > lit("\"ts_fmt\"") > ':' > bool_) >

       lit('}'))[_val = phx::bind(mk_percentile_ts_request, _1, _2, _3, _4, _5, _6, _7, _8)];
    start.name("percentile_request");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct percentile_ts_request_grammar<request_iterator_t, request_skipper_t>;
}
