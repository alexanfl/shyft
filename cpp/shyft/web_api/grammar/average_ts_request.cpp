#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline average_ts_request mk_average_ts_request(
    string const & request_id,
    utcperiod const & r,
    shyft::time_axis::generic_dt const & ta,
    bool cache,
    vector<string> const & ids,
    boost::optional<bool> opt_sub,
    boost::optional<bool> ts_fmt) {
    return average_ts_request{request_id, r, ta, cache, ids, opt_sub.get_value_or(false), ts_fmt.get_value_or(false)};
  }

  template <typename Iterator, typename Skipper>
  average_ts_request_grammar<Iterator, Skipper>::average_ts_request_grammar()
    : average_ts_request_grammar::base_type(start, "average_ts_request") {

    start =
      (lit("average") > lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"read_period\"") > ':' > p_
       > ',' > lit("\"time_axis\"") > ':' > time_axis_ > ',' > lit("\"cache\"") > ':' > qi::bool_ > ','
       > lit("\"ts_ids\"") > ':' > '[' > (quoted_string % ',') > lit(']') > -(',' > lit("\"subscribe\"") > ':' > bool_)
       > -(',' > lit("\"ts_fmt\"") > ':' > bool_) >

       lit('}'))[_val = phx::bind(mk_average_ts_request, _1, _2, _3, _4, _5, _6, _7)];
    start.name("average_request");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct average_ts_request_grammar<request_iterator_t, request_skipper_t>;
}
