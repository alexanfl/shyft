#include <shyft/web_api/web_api_grammar.h>

namespace shyft::hydrology::grammar {
  using namespace shyft::web_api::grammar;

  /** @brief netcdf cf-time parser
   * @details
   * only usage within the shyft::hydrology (not web-api, just located here for convinience)
   * to ensure we can use a simple stable
   * cf-time parser.
   */
  template <typename Iterator, typename Skipper = boost::spirit::qi::ascii::space_type>
  struct cftime_grammar : public qi::grammar<Iterator, utcperiod(), Skipper> {
    cftime_grammar();
    qi::rule<Iterator, utcperiod(), Skipper> start;
    // cf_ref_time_grammar<Iterator> ref_time_;
    qi::rule<Iterator, utctime(), Skipper> ref_time_;
    uint_parser<unsigned, 10, 4, 4> d4_;
    uint_parser<unsigned, 10, 2, 2> d2_;
    qi::symbols<char, int> delta_;
    qi::symbols<char, int> tz_;
    phx::function<error_handler_> const error_handler = error_handler_{};
  };

  extern template struct cftime_grammar<char const *, qi::ascii::space_type>;
  static constexpr const std::size_t max_cf_time_length=50ul;
  utcperiod parse_cf_time(char const * input) {
    using boost::spirit::qi::phrase_parse;
    using boost::spirit::qi::ascii::space;
    static cftime_grammar<char const *> p;
    if(!input)
      return {};
    auto sz=strnlen(input,max_cf_time_length);
    if (sz < 1)
      return {};
    char const * f{input};
    char const * l{f + sz};

    try {
      utcperiod v;
      if (phrase_parse(f, l, p, space, v) && (f == l)) {
        return v;
      }
    } catch (std::exception const &) {
      // we do not provide diag yet.
    }
    return {};
  }

  namespace {
    inline utcperiod mk_cftime(int dt, utctime t0, boost::optional<int> tz) {
      t0 += from_seconds(tz.value_or(0));
      return utcperiod{t0, t0 + from_seconds(dt)};
    }

    inline utctime mk_ref_time(unsigned Y, unsigned M, unsigned D, unsigned h, unsigned m, unsigned s) {
      static calendar utc;
      return utc.time(int(Y), int(M), int(D), int(h), int(m), int(s));
    }
  }

  template <typename Iterator, typename Skipper>
  cftime_grammar<Iterator, Skipper>::cftime_grammar()
    : cftime_grammar::base_type(start, "cftime") {
    start = (delta_ > lit("since") > ref_time_ > -(tz_))[_val = phx::bind(mk_cftime, _1, _2, _3)];

    start.name("cftime");
    ref_time_ = lexeme[(
      d4_ > '-' > d2_ > '-' > d2_ > (lit(' ') | lit('T')) > d2_ > ':' > d2_ > ':'
      > d2_)[_val = phx::bind(mk_ref_time, _1, _2, _3, _4, _5, _6)]]

      ;
    delta_.add("seconds", 1)("minutes", 60)("hours", 3600)("days", 24 * 3600);
    tz_.add("Z", 0)("+00:00", 3600 * 0)("+01:00", 3600 * 1)("+02:00", 3600 * 2)("+03:00", 3600 * 3)("+04:00", 3600 * 4)(
      "+05:00", 3600 * 5)("+06:00", 3600 * 6)("+07:00", 3600 * 7)("+08:00", 3600 * 8)("+09:00", 3600 * 9)(
      "+10:00", 3600 * 10)("+11:00", 3600 * 11)("+12:00", 3600 * 12)("-01:00", -3600 * 1)("-02:00", -3600 * 2)(
      "-03:00", -3600 * 3)("-04:00", -3600 * 4)("-05:00", -3600 * 5)("-06:00", -3600 * 6)("-07:00", -3600 * 7)(
      "-08:00", -3600 * 8)("-09:00", -3600 * 9)("-10:00", -3600 * 10)("-11:00", -3600 * 11)("+00", 3600 * 0)(
      "+01", 3600 * 1)("+02", 3600 * 2)("+03", 3600 * 3)("+04", 3600 * 4)("+05", 3600 * 5)("+06", 3600 * 6)(
      "+07", 3600 * 7)("+08", 3600 * 8)("+09", 3600 * 9)("+10", 3600 * 10)("+11", 3600 * 11)("-01", -3600 * 1)(
      "-02", -3600 * 2)("-03", -3600 * 3)("-04", -3600 * 4)("-05", -3600 * 5)("-06", -3600 * 6)("-07", -3600 * 7)(
      "-08", -3600 * 8)("-09", -3600 * 9)("-10", -3600 * 10)("-11", -3600 * 11);

    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct cftime_grammar<char const *, qi::ascii::space_type>;


}
