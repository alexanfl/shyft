#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
  template <typename Iterator, typename Skipper>
  web_reply_grammar<Iterator, Skipper>::web_reply_grammar()
    : web_reply_grammar::base_type(start, "web_reply") {
    start =
      (request_reply_ | find_ | tsv_reply_ | q_list_ | q_info_ | q_infos_ | q_size_ | q_get_

      ); // one of these constructs are accepted
    start.name("web_reply");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct web_reply_grammar<request_iterator_t, request_skipper_t>;
}
