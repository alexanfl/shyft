#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline static geo_ts_id mk_geo_ts_idx(vector<char> const & geo_db, size_t v, size_t g, size_t e, double t) {
    return geo_ts_id{string(geo_db.begin(), geo_db.end()), v, g, e, shyft::core::from_seconds(t)};
  }

  static std::string geo_prefix{"geo://"};

  template <typename Iterator>
  geo_ts_url_grammar<Iterator>::geo_ts_url_grammar(std::string prefix)
    : geo_ts_url_grammar::base_type(start, "geo_ts_url_grammar")
    , prefix{prefix} {
    start =
      (lit(this->prefix) > (+qi::char_("a-zA-Z_0-9@")) > lit('/') > int_ > lit('/') > int_ > lit('/') > int_ > lit('/')
       > double_)[_val = phx::bind(mk_geo_ts_idx, _1, _2, _3, _4, _5)];
    start.name("geo_ts_url_grammar");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  // instantiate std request_iterator
  template struct geo_ts_url_grammar<request_iterator_t>;


}
