#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline tsv_reply mk_tsv_reply(string const & rid, ats_vector const & v) {
    return tsv_reply{rid, v};
  }

  template <typename Iterator, typename Skipper>
  tsv_reply_grammar<Iterator, Skipper>::tsv_reply_grammar()
    : tsv_reply_grammar::base_type(start, "tsv_reply_grammar") {
    start =
      (lit('{') >> lit("\"request_id\"") >> lit(':') >> quoted_string_ >> lit(',') >> lit("\"tsv\"") >> lit(':') >> tsv_
       >> lit('}'))[_val = phx::bind(mk_tsv_reply, _1, _2)];
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct tsv_reply_grammar<request_iterator_t, request_skipper_t>;


}
