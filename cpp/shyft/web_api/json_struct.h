/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <map>
#include <iosfwd>
#include <type_traits>


#include <boost/variant/recursive_wrapper.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/variant.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/optional.hpp>

#include <shyft/time_series/time_axis.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/srv/model_info.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/energy_market/stm/attribute_types.h>

#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/waterway.h>

#include <shyft/mp.h>
#include <shyft/mp/type_set_variant.h>

namespace shyft::web_api::energy_market {
  using std::vector;
  using std::string;
  using std::map;
  using std::shared_ptr;
  using std::pair;

  namespace hana = boost::hana;

  using shyft::energy_market::stm::reservoir;
  using shyft::energy_market::stm::unit;
  using shyft::energy_market::stm::unit_group;
  using shyft::energy_market::stm::unit_group_member;
  using shyft::energy_market::stm::waterway;
  using shyft::energy_market::stm::power_plant;
  using shyft::energy_market::stm::gate;
  using shyft::energy_market::stm::catchment;
  using shyft::energy_market::stm::energy_market_area;
  using shyft::energy_market::stm::run_parameters;

  using shyft::energy_market::stm::srv::stm_case;
  using shyft::energy_market::stm::srv::stm_task;
  using shyft::energy_market::stm::srv::model_ref;
  using shyft::srv::model_info;
  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using shyft::time_axis::generic_dt;
  using shyft::time_series::dd::apoint_ts;
  using shyft::energy_market::absolute_constraint;
  using shyft::energy_market::penalty_constraint;

  using shyft::mp::leaf_accessor_type_set;
  /** @brief the value types of all attributes in an stm_system contained as a boost-hana set: */
  constexpr auto stm_system_value_types = hana::fold_left(
    hana::make_tuple( // Create a sequence of all the type-sets we want
      leaf_accessor_type_set<reservoir>,
      leaf_accessor_type_set<unit>,
      leaf_accessor_type_set<waterway>,
      leaf_accessor_type_set<power_plant>,
      leaf_accessor_type_set<catchment>,
      leaf_accessor_type_set<gate>,
      leaf_accessor_type_set<energy_market_area>,
      leaf_accessor_type_set<run_parameters>,
      leaf_accessor_type_set<unit_group>,
      leaf_accessor_type_set<unit_group_member>,
      hana::make_set(
        hana::type_c<string>,
        hana::type_c<absolute_constraint>,
        hana::type_c<penalty_constraint>) // We stash in additional types we need here.
      ),
    hana::union_ // Take union of the lot to get a minimal set containing all types.
  );

  constexpr auto stm_system_variant_type = shyft::mp::get_variant_from_type_set(stm_system_value_types);
  using attribute_value_type = typename decltype(+stm_system_variant_type)::type;

  /** @brief visitor for comparing two proxy attributes.
   */
  class attribute_value_compare : public boost::static_visitor<bool> {
   public:
    /** @brief The case of type mismatch */
    template <class LV, class RV>
    bool operator()(LV const &, RV const &) const {
      return false;
    }

    /** @brief Comparison in the case of apoint_ts */
    bool operator()(apoint_ts const & lhs, apoint_ts const & rhs) const {
      if (lhs.needs_bind() || rhs.needs_bind())
        return false;
      return lhs == rhs;
    }

    /** @brief Comparison in the other cases.
     *  That is, both lhs and rhs are shared_ptr's to t-maps, with the same range.
     *
     *  A somewhat complicated implementation as we want to compare by value.
     */
    template <class V>
    bool operator()(
      shared_ptr<map<utctime, shared_ptr<V>>> const & lhs,
      shared_ptr<map<utctime, shared_ptr<V>>> const & rhs) const {
      // Check if both are null:
      if (!(lhs || rhs))
        return true;
      // If at least one is null
      if (!(lhs && rhs))
        return false;
      // There size should be equal:
      if (lhs->size() != rhs->size())
        return false;
      // Iterate over each item in the map
      for (auto const & it : *lhs) {
        auto key = it.first;
        auto val = it.second;
        auto rit = rhs->find(key);
        if (rit == rhs->end())
          return false;
        else if (*(rit->second) != *val)
          return false;
      }
      return true;
    }

    template <class S, class T>
    bool operator()(vector<pair<S, T>> const & lhs, vector<pair<S, T>> const & rhs) {
      if (lhs.size() != rhs.size())
        return false;

      for (auto i = 0u; i < lhs.size(); ++i) {
        if (lhs[i].first != rhs[i].first || lhs[i].second != rhs[i].second)
          return false;
      }
      return true;
    }

    template <class T>
    bool operator()(vector<T> const & lhs, vector<T> const & rhs) {
      if (lhs.size() != rhs.size())
        return false;

      for (auto i = 0u; i < lhs.size(); ++i) {
        if (lhs[i] != rhs[i])
          return false;
      }
      return true;
    }

    /** @brief Default comparison. Assumes equal-operator is overloaded
     */
    template <class T>
    bool operator()(T const & lhs, T const & rhs) const {
      return lhs == rhs;
    }
  };
  /** The json struct is a recursive map with keys that are strings and
   * values in a set here given by the boost variant value_type.
   **/
  /** The range of the json maps, aside from being recursive should be finite. **/
  struct json;
  typedef boost::variant<
    int,
    vector<int>,
    double,
    string,
    vector<string>,
    utcperiod,
    generic_dt,
    bool,
    model_info,
    stm_task,
    stm_case,
    model_ref,
    boost::recursive_wrapper<json>,
    vector< json >,
    vector< vector<json> >,
    attribute_value_type,
    vector<attribute_value_type> >
    value_type;

  /** the json struct is then simply a map from strings with value_type as range. **/
  struct json {
    map<string, value_type> m;

    /** @brief access item in json map. Returns lvalue */
    value_type& operator[](string const & key) {
      return m[key];
    }

    /** @brief Returns item in map. Throws runtime error if provided key is invalid */
    value_type required(string const & key) const {
      auto it = m.find(key);
      if (it != m.end()) {
        return it->second;
      } else {
        throw std::runtime_error("Unable to find required key '" + key + "'");
      }
    }

    /** @brief access value in map, and throw error if unable to retrieve it.
     *  Also throws error if the value of the required key is not of specified type.
     *
     * @tparam ExpType: What type you expect to get back. Will throw a runtime error if unable to convert to this type.
     */
    template <class ExpType>
    ExpType required(string const & key) const {
      auto it = m.find(key);
      if (it != m.end()) {
        try {
          return boost::get<ExpType>(it->second);
        } catch (boost::bad_get const &) {
          throw std::runtime_error("Failed attempt at boost::get with key '" + key + "'");
        }
      } else {
        throw std::runtime_error("Unable to find required key '" + key + "'");
      }
    }

    /** @brief access optional value in map.
     * If provided key is not in map, the optional will be empty.
     */
    boost::optional<value_type> optional(string const & key) const noexcept {
      auto it = m.find(key);
      return (it != m.end()) ? boost::optional<value_type>(it->second) : boost::none;
    }

    /** @brief access optional value in map
     * and return a boost::optional containing the expected type
     *
     * Returns boost::none if either the key is invalid or value is not of expected type.
     * @tparam ExpType: What type to expect to get back.
     */
    template <class ExpType>
    boost::optional<ExpType> optional(string const & key) const noexcept {
      auto it = m.find(key);
      if (it == m.end()) { // Key is not in map
        return boost::none;
      } else {
        try {
          return boost::get<ExpType>(it->second);
        } catch (boost::bad_get const &) {
          // Possibly: Instead throw a warning or error?
          return boost::none;
        }
      }
    }

    json() = default;
  };

  /** @brief the basic request type, containing a keyword and data pertaining
   * to a request held as a json
   */
  struct request {
    string keyword;
    json request_data;
  };


}
