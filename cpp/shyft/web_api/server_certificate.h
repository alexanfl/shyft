//
// Copyright (c) 2016-2017 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//
#pragma once

#include <boost/asio/buffer.hpp>
#include <boost/asio/ssl/context.hpp>
#include <cstddef>
#include <memory>
#include <shyft/core/fs_compat.h>

namespace shyft::web_api {

  /*
   * Load a signed certificate into the ssl context, and configure
   * the context for use.
   * Using recipe on shyft wiki to create your own CA
   * followed by generating these test-certificates.
   * https://gitlab.com/shyft-os/shyft/-/wikis/How-to/Configure-TLS-SSL-for-shyft-services#configure-shyft-web-api-for-tlsssl
   *
   * notice that ca_cert is used to  sign the test certificates, and is needed so that
   * test code can use verify peer certificate.
   */
  inline void load_server_certificate(boost::asio::ssl::context& ctx) {
    std::string const ca_cert =
      R"_(-----BEGIN CERTIFICATE-----
MIIDVzCCAj+gAwIBAgIUYMHbEwE1NBxh2syN3S+iALC0PSgwDQYJKoZIhvcNAQEL
BQAwOzELMAkGA1UEBhMCTk8xDTALBgNVBAgMBE9zbG8xDTALBgNVBAcMBE9zbG8x
DjAMBgNVBAoMBXNoeWZ0MB4XDTIyMDkwMTE1NTgyM1oXDTI3MDgzMTE1NTgyM1ow
OzELMAkGA1UEBhMCTk8xDTALBgNVBAgMBE9zbG8xDTALBgNVBAcMBE9zbG8xDjAM
BgNVBAoMBXNoeWZ0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0ld+
J/tK+p11nNZHAHLbInR5j7Qh3GJvrMDDNvV9pEqHS0kZo8QD9b4xWQn2XKRJWFcG
2ruszSYiBzTtNYGEOKvTIlzjQuusrZVsDYgWm/B0kkjXrH7wC5iHgR7VNNzSYNck
rBitw83pWD3icFJ6Ahdug2iIP9BblTrSu0SHH6iWtG8WkfX/s28D7k4kJvknoUFQ
ZLDufWksLqRz1jnRgXT3DV3p0HuDUJDj9ZpxnlidgBI17vxoxDF5QXkpZrKxk9Oj
oN2GYYHYDRxVdETwFTyj0en8W0tyQHyecVDND832fukGoQMHOeKUF3q5hRuo38h+
uOYDc3hRu3xjrJPbWQIDAQABo1MwUTAdBgNVHQ4EFgQUbyuGY8GpGL/0hHTC0c0G
UD/P8AYwHwYDVR0jBBgwFoAUbyuGY8GpGL/0hHTC0c0GUD/P8AYwDwYDVR0TAQH/
BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAorDv8hijjRPZ1T+kwnj//NMArpvR
nlqRXiZvmsbr74B50cYD1Ni40Fe+C/XsRuschQ9oBtIbi0qKK+PUYNOdOF4hqb+1
A2sa8cmRGzAKb18wVkbtKKZUqrVgadF/zyWrK5LyyKUmYf9+YP5XFlyXA+T+a/Dp
Wd8eB0rpOqS3eBuEpgD/W5NV3YivyzXR/0rqM2aGC+7O1/ABp9LvsLOCyPGL4glT
JMSsD6nYSGQGNj108nCOPJMLpSCH4h5SYd5u+ILAOH/yPcUkstQAHXFxfeE2W3xf
jrtSrq/AcNdUjCsKlrI60sRXiWooM7ygJCbUvueS2ZZPWI01G+SJX8XVgg==
-----END CERTIFICATE-----)_";

    std::string const cert =
      R"_(-----BEGIN CERTIFICATE-----
MIIDfTCCAmWgAwIBAgIUV+LGpjsTLA0HQX4x9iSUd5obcq4wDQYJKoZIhvcNAQEL
BQAwOzELMAkGA1UEBhMCTk8xDTALBgNVBAgMBE9zbG8xDTALBgNVBAcMBE9zbG8x
DjAMBgNVBAoMBXNoeWZ0MB4XDTIyMDkwMTE1NTkwNVoXDTI3MDgzMTE1NTkwNVow
SjELMAkGA1UEBhMCTk8xDTALBgNVBAgMBE9zbG8xDTALBgNVBAcMBE9zbG8xDjAM
BgNVBAoMBXNoeWZ0MQ0wCwYDVQQLDAR0ZXN0MIIBIjANBgkqhkiG9w0BAQEFAAOC
AQ8AMIIBCgKCAQEAor9hpk8HpSD6HqzcoWtbI74sJ+H4nikRs+lFqyqx2wr2wMlW
SfwvRyp15c0WBuwx+RQ45PfbleDcuGIMhhy6W7MQ5cFwKI7mEIvdYLJABTPlfOG8
bD2Rd27WOwRvSxhmaM1IOEfRoue1bg8hc8BCUvTVuz+P4oofpUOxCE78VO/pYR0T
ADmwyyn5pTULpz1sBnrO8xSNtMxJfrUhXnD2cQXptgszpqlmOT/FphRWSA3fiOg1
p1hp59FvmAxuCpYF/5dGBCBJnzKHbgrxhLU1IRcre4lndqvuSlj/tPa4Fcp4ML5b
64bMMkKdlSg9a9hhruNp/O4R4lp5B7YhTl0BnQIDAQABo2owaDAfBgNVHSMEGDAW
gBRvK4ZjwakYv/SEdMLRzQZQP8/wBjAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DAt
BgNVHREEJjAkgg50ZXN0LnNoeWZ0Lm9yZ4ISd3d3LnRlc3Quc2h5ZnQub3JnMA0G
CSqGSIb3DQEBCwUAA4IBAQARKnrkvpKFY7Wm00uketuKnGH/Y7MJh4cttjLYbs5Q
oZT0PiJ2IGXMgwXbz6+YLEM/mSkEzIT8kY58BUNs5Itkymx2JdpqwbQILMLyjLHI
YUdQh70Wk98/os3eaH4akv9BGiDM2WsD6ggG0AyUoKps2cWML219cKqzrQ9mAEI4
RW7y3XqJJUwMWDko1jcvppM4I8Nb+SPqXZ7O9m8rHmvzfnamrE6TGqaLbAuyNS/O
RAsjznBVOFCETeUygshFTm3e6TRiOWm6Pg/m9RdvkNlko3VjNQO1TSv7P5m8yIis
VZGgFg6XBUTYHKz3W11km6BCYcm5uddrmATCaz8KkBPa
-----END CERTIFICATE-----)_";

    std::string const key =
      R"_(-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAor9hpk8HpSD6HqzcoWtbI74sJ+H4nikRs+lFqyqx2wr2wMlW
SfwvRyp15c0WBuwx+RQ45PfbleDcuGIMhhy6W7MQ5cFwKI7mEIvdYLJABTPlfOG8
bD2Rd27WOwRvSxhmaM1IOEfRoue1bg8hc8BCUvTVuz+P4oofpUOxCE78VO/pYR0T
ADmwyyn5pTULpz1sBnrO8xSNtMxJfrUhXnD2cQXptgszpqlmOT/FphRWSA3fiOg1
p1hp59FvmAxuCpYF/5dGBCBJnzKHbgrxhLU1IRcre4lndqvuSlj/tPa4Fcp4ML5b
64bMMkKdlSg9a9hhruNp/O4R4lp5B7YhTl0BnQIDAQABAoIBAQCKhonwjB4+wLYE
/DADuaRm5Q0imWYMAXsO/wSM8BdZEZj8eaNX8C6xzYNyqcEQ/WR/1WWkBxDCHxbg
Xpy4XqWFPgKo/aCg9RkAhnxVPQPfKdAxHtdJ7pHt7cqti1XLvqYUmH7bYM4gckds
ubZIlyIdCEcQTccEFXtgKSD7qHeBNySAvtlCG0NqcUCyI03Mt3+XTo7gJLhE7p/g
320GsOjpGk/j8ZS6i8Xu6B4yKtj4eOQRjnbvlI4LelvBZXUAQFr6UTGWcv/BVFnK
f+tILj2517n1ruIe63d48PBbCP5kB8pT0w0Uj9jrOV8zRfRpwWaI0w2R86Wm0tbk
q9yZEeSFAoGBANFTvNM7C35T0HYf6gTAsHDfkSWrW4BJael1W4WyqewoXsGV8Y3G
HN9mBrAdCSlGxhN0krCwsPWF7dS2jBERr/w2aFXy/ESyQIZjeUVtdeAPoc33+aJW
rX6Ya8F2pLDcNln7TuytuZmgJUH7nAxs8Ibe8Qae74fU/uOfYd39IVObAoGBAMcI
6eSwOBknSaz92NosjZzrA39nrMBeQnZvJq/I9LydrR9sTnUgF+v5CIQaFeLqURHK
xpYMhigyXIG4JtI8tQdrWf6Cxqx55wkJUBtvDhJ27ts88rgKwvHOdedm4zAINius
3Pq90Dg5bnFRTIgCLbu7rZzHNvmA7PUXGxbd1J8nAoGATUa+lLRdf9ttP850VCDW
gjTmnqNt+iQgB2Gd0fOtK1OT959lN1OF4SsHGQj/Es/f29rTNItmKJ2yuS5S8xI2
h8OGLQSxAKNVkscyQ+Eu/PKIZ0M+UyHwlqWqUhd8oRYKsplF/7X6qK4oUv2SFDw7
Z27yR+FwyBJnrpxuQsdqADUCgYAsn1X43H0nPoSpERxeBaEWLiZRmQfE9R0HenH8
9RvRcbRPmDOa1MAoOmPrYgHIJupYB9Mv7szhFDFjL8scVVnsuOerRNyP7KCciQb5
r5NDFoEEjAyKsHb2SWhrociLizvB3PaKDKYzBMU7CJQU5hAkx7XrsUok8/+sDysW
nFM//wKBgHXFiy+dbHjyWsg7ogmqbRDWuZyw09+VaGgKqJXRlJNGsZebRfHXqn5f
6oxESNd0X7YAti5Kk1Xsy8KqzT50qeIfVOtK21XwVNGhxHX6iYu3B11Yw7tehrcC
ZXycCjDhfs2vA5NWwR7+SR1F4ImPSZau5sVPqCqQ4qEnBy9IO5MT
-----END RSA PRIVATE KEY-----)_";


    std::string const dh =
      R"_(-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEAhMpG2EJ0bOtussXKTHFzdjIUgwV+Wzal7hhAhcSoskXkwIAV9Hyc
sU51TG32GXs1Ldz3UvKGMFnuxpe1vNfsXvOtsJ8nFqigqBz8qIu1+bZaUFEE3TEF
5LZh7KayqwJSmseAd7jDZPPdPgCGDWuPurhNzpjohXcVpBVZrOtowZWbSZz20MYX
RwHtz1gTtXqQOGIbgAtzJrru6NIKDwCNS2sSJBuVtvu1J6Um6p/k7fEXSc3OZWdu
1khycbB8Gkc3PImpQkKxG+731Sz4yWwxxumbT01j/Qngeyji9IbEqlEBhmzX1PEN
kXngaxY9wDFirQ27ZOvxLoda/IP7EEQ86wIBAg==
-----END DH PARAMETERS-----)_";

    ctx.set_password_callback([](std::size_t, boost::asio::ssl::context_base::password_purpose) {
      return "test";
    });

    ctx.set_options( // flipp off most unsecured stuff here
      boost::asio::ssl::context::default_workarounds | boost::asio::ssl::context::no_sslv2
      | boost::asio::ssl::context::no_sslv3 | boost::asio::ssl::context::no_tlsv1
      | boost::asio::ssl::context::no_tlsv1_1 | boost::asio::ssl::context::single_dh_use);

    if (char const * cp = getenv("SHYFT_CERT_PATH")) {
      // Set SHYFT_CERT_PATH to point to a directory
      // that contains the cert, key, and dh
      //
      fs::path r = cp;
      fs::path srv_crt = r / "server.crt";
      fs::path srv_key = r / "server.key"; // in pem format
      fs::path dh = r / "dh.pem";
      if (!fs::exists(srv_crt))
        throw std::runtime_error("Missing ssl server cert file :" + srv_crt.string());
      if (!fs::exists(srv_key))
        throw std::runtime_error("Missing ssl server cert pk file :" + srv_key.string());
      if (!fs::exists(dh))
        throw std::runtime_error("Missig ssl dh file :" + dh.string());
      ctx.use_certificate_chain_file(srv_crt.string());
      ctx.use_private_key_file(srv_key.string(), boost::asio::ssl::context::file_format::pem);
      ctx.use_tmp_dh_file(dh.string());
      ctx.add_verify_path(std::string(cp)); // also this as additional verify path
                                            // notice that the path should contain CA files, where the name
                                            // are equal to subject_hash.0
                                            //
    } else {
      ctx.use_certificate_chain(boost::asio::buffer(cert.data(), cert.size()));
      ctx.use_private_key(boost::asio::buffer(key.data(), key.size()), boost::asio::ssl::context::file_format::pem);
      ctx.use_tmp_dh(boost::asio::buffer(dh.data(), dh.size()));
      ctx.add_certificate_authority(boost::asio::buffer(ca_cert.data(), ca_cert.size()));
    }
  }
}
