#include <thread>

#include <shyft/web_api/targetver.h>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/strand.hpp>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/web_api/dtss_client.h>
#include <shyft/web_api/web_api_generator.h>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/server_certificate.h>

namespace shyft::web_api {
  using std::vector;
  using std::string;
  using std::string_view;
  using std::make_shared;

  using namespace shyft::dtss;
  using namespace shyft::core;
  using namespace shyft;

  namespace beast = boost::beast;
  namespace http = beast::http;
  namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
  namespace net = boost::asio;
  namespace ssl = boost::asio::ssl;
  using tcp = boost::asio::ip::tcp; // from <boost/asio/ip/tcp.hpp
  using boost::system::error_code;

  /**
   * @brief ws session for driving tests.
   *
   * @details
   * Sends a WebSocket message and prints the response,
   * from examples made by boost.beast/Vinnie Falco
   *
   */
  class dtss_session : public std::enable_shared_from_this<dtss_session> {
    tcp::resolver resolver_;
    websocket::stream<beast::ssl_stream<beast::tcp_stream>> ws_;
    beast::multi_buffer buffer_;

    string host_;
    string port_;
    string host_hdr_;     // used for handshake on ws
    string auth_;         // used for auth if enabled by .size()>0
    string request_txt_;  ///< keept here in case beast does not copy
    string fail_;         ///< provide latest fail diag
    bool reading_{false}; ///< true when async-read is ongoing(we should not start two async reads!)
    std::function<void(string const &)> response_handler_;
    std::function<bool()> timer_handler_;

    std::shared_ptr<net::steady_timer>
      timer_; // derived().ws().get_executor().context(),std::chrono::steady_clock::time_point_max());
    std::chrono::milliseconds check_timeout{5};

    // Report a failure
    void fail(error_code ec, char const *what) {
      fail_ = string(what) + ": " + ec.message() + "\n";
    }

#define fail_on_error(ec, diag) \
  if ((ec)) { \
    fail((ec), (diag)); \
    throw std::runtime_error(fail_); \
  }
#define warn_on_error(ec, diag) \
  if ((ec)) { \
    fail((ec), (diag)); \
    std::cerr << fail_ << std::endl; \
  }

    void start_timer() {
      if (!timer_)
        timer_ = std::make_shared<net::steady_timer>(ws_.get_executor());
      timer_->expires_after(check_timeout);
      timer_->async_wait([me = shared_from_this()](beast::error_code ec) -> void {
        if (ec == boost::asio::error::operation_aborted)
          return; // ignore this one, it means timer was started with some other timeout.
        me->on_timer_check(ec);
      });
    }

    void stop_timer() {
      if (timer_) {
        timer_->expires_at(std::chrono::steady_clock::time_point::max());
      }
    }

    void on_timer_check(error_code ec) {
      fail_on_error(ec, "timer") // just bail out on cancel
        if (timer_handler_ && timer_handler_()) {
        start_timer();
      }
      else { // at least stop timer, and maybe also stop current ioc??
        stop_timer();
      }
    }

   public:
    // Resolver and socket require an io_context
    explicit dtss_session(net::io_context &ioc, ssl::context &ctx, string_view host, int port, string_view auth)
      : resolver_(net::make_strand(ioc))
      , ws_(net::make_strand(ioc), ctx)
      , host_{host}
      , port_{std::to_string(port)}
      , auth_{auth} {
    }

    string diagnostics() const {
      return fail_;
    }

    // Start the asynchronous operation
    template <class Fx, class Tx>
    void run(string_view request_txt, Fx &&response_handler, Tx &&timer_handler) {
      // Save these for later
      request_txt_ = request_txt;
      response_handler_ = response_handler;
      timer_handler_ = timer_handler;

      start_resolve();
    }

    void send_request(string_view r) {
      request_txt_ = r; // do we need to do this, or will buffer make acopy?
      ws_.async_write(  // Send the message
        boost::asio::buffer(request_txt_),
        [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->on_write(ec, bytes_transferred);
        });
    }

    void start_close() {
      stop_timer();
      ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
        me->on_close(ec);
      });
    }

    void continue_read() {
      start_reading();
    }

   private:
    void start_resolve() {
      resolver_.async_resolve(
        host_,
        port_, // Look up the domain name
        [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
          me->on_resolve(ec, results);
        });
    }

    void on_resolve(error_code ec, tcp::resolver::results_type results) {
      fail_on_error(ec, "resolve") beast::get_lowest_layer(ws_).expires_after(std::chrono::seconds(30));
      beast::get_lowest_layer(ws_).async_connect(
        results, beast::bind_front_handler(&dtss_session::on_connect, shared_from_this()));
    }

    void on_connect(error_code ec, tcp::resolver::results_type::endpoint_type ep) {
      fail_on_error(ec, "connect") beast::get_lowest_layer(ws_).expires_after(
        std::chrono::seconds(30)); // timeout on the next get operation

      host_hdr_ = host_ + ':' + std::to_string(ep.port()); // See https://tools.ietf.org/html/rfc7230#section-5.4
      if (!SSL_set_tlsext_host_name(ws_.next_layer().native_handle(), host_hdr_.c_str())) {
        ec = beast::error_code(static_cast<int>(::ERR_get_error()), net::error::get_ssl_category());
        fail_on_error(ec, "connect SSL_set_tlsext_host_name");
      }
      // Perform the SSL handshake
      ws_.next_layer().async_handshake(
        ssl::stream_base::client, beast::bind_front_handler(&dtss_session::on_ssl_handshake, shared_from_this()));
    }

    void on_ssl_handshake(error_code ec) {
      fail_on_error(ec, "ssl handshake") beast::get_lowest_layer(ws_).expires_never();
      ws_.set_option(websocket::stream_base::timeout::suggested(beast::role_type::client));
      ws_.set_option(websocket::stream_base::decorator([this](websocket::request_type &req) {
        req.set(http::field::user_agent, "dtss-client-ssl");
        if (auth_.size()) // pass on authorization field if specified
          req.set(http::field::authorization, auth_);
      }));

      // Update the host_ string. This will provide the value of the
      // Host HTTP header during the WebSocket handshake.
      // See https://tools.ietf.org/html/rfc7230#section-5.4
      ws_.async_handshake(
        host_hdr_,
        "/", // Perform the websocket handshake
        [me = shared_from_this()](error_code ec) {
          me->on_handshake(ec);
        });
    }

    void on_handshake(error_code ec) {
      fail_on_error(ec, "wss handshake");
      send_request(request_txt_);
      if (timer_handler_)
        start_timer();
    }

    void on_write(error_code ec, std::size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "write");
      start_reading();
    }

    void start_reading() {
      if (!reading_) {
        ws_.async_read(
          buffer_, // Read a message into our buffer
          [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
            me->on_read(ec, bytes_transferred);
          });
        reading_ = true; // flag we are reading so we dont start two operlapping reads(not allowed)
      }
    }

    void on_read(error_code ec, std::size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      reading_ = false; // read operation finished, allow next one
      fail_on_error(ec, "read") auto response_msg = boost::beast::buffers_to_string(buffer_.data());
      buffer_.consume(buffer_.size());
      // std::cout<<"got buffer:"<<response_msg<<std::endl;
      response_handler_(response_msg);
    }

    void on_close(error_code ec) {
      if (ec == net::error::eof || ec == net::error::connection_reset) {
        ec =
          {}; // Rationale:
              // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
      }
      warn_on_error(ec, "close")
    }

#undef fail_on_error
  };

  namespace generator {

    using out_iterator = std::back_insert_iterator<string>; ///< the output iterator is a string for all the cases here

    /**
     * @brief utility to generate a request string for the dtss-web-api
     * @details
     * Given function predicate and parameters, generates request, using karma generators.
     * It uses a locally managed counter to generate new request-identifiers that are
     * kind of read-able using minimal number of bytes.
     *
     */
    struct request_generator {
      size_t request_id{0};                                  ///< the request-id, increasing for each generated request.
      find_ts_request_generator<out_iterator> const f_ts_g_; ///< find genererator
      store_ts_request_generator<out_iterator> const s_ts_g_; ///< store ts generator
      read_ts_request_generator<out_iterator> const r_ts_g_;  ///< read ts generator

      q_get_request_generator<out_iterator> const q_get_;
      q_put_request_generator<out_iterator> const q_put_;
      q_list_request_generator<out_iterator> const q_list_;
      q_info_request_generator<out_iterator> const q_info_;
      q_infos_request_generator<out_iterator> const q_infos_;
      q_size_request_generator<out_iterator> const q_size_;
      q_maintain_request_generator<out_iterator> const q_maintain_;
      q_done_request_generator<out_iterator> const q_done_;

      std::string mk_request_id(std::string const &prefix) {
        return prefix + std::to_string(++request_id);
      }

      string find(std::string pattern) {
        string r;
        generate(std::back_inserter(r), f_ts_g_, find_ts_request{mk_request_id("f-"), pattern});
        return r;
      }

      string info() {
        return "info { \"request_id\":\"" + mk_request_id("i-") + "\"}";
      }

      string store_ts(ats_vector const &tsv, bool recreate_ts, bool cache, bool merge_store = false) {
        string r;
        // TODO: can we avoid copy of tsv here? (e.g. inline emittergrammar will do it)
        generate(
          std::back_inserter(r), s_ts_g_, store_ts_request{mk_request_id("s-"), merge_store, recreate_ts, cache, tsv});
        return r;
      }

      std::tuple<string, string> read_ts(
        vector<string> const ts_ids,
        utcperiod p,
        bool use_ts_cached_read,
        bool subscribe = false,
        utcperiod clip_result = utcperiod{}) {
        string r;
        string rid = mk_request_id("r-");
        string unsub;
        generate(
          std::back_inserter(r),
          r_ts_g_,
          read_ts_request{
            mk_request_id("r-"),
            p,                  // read period
            clip_result,        // period
            use_ts_cached_read, // cache and update really
            ts_ids,
            subscribe, // subscribe
            true,      // use new ts-format
          });

        if (subscribe) {
          unsub = "unsubscribe {\"request_id\":\"" + mk_request_id("u-") + "\",\"subscription_id\":\"" + rid + "\"}";
        }

        return {r, unsub};
      }

      string q_list() {
        string r;
        generate(std::back_inserter(r), q_list_, q_list_request{mk_request_id("ql-")});
        return r;
      }

      string q_msg_info(string const &q_name, string const &msg_id) {
        string r;
        generate(std::back_inserter(r), q_info_, q_info_request{mk_request_id("qmi-"), q_name, msg_id});
        return r;
      }

      string q_msg_infos(string const &q_name) {
        string r;
        generate(std::back_inserter(r), q_infos_, q_infos_request{mk_request_id("qmis-"), q_name});
        return r;
      }

      string
        q_put(string const &q_name, string const &msg_id, string const &descript, utctime ttl, ats_vector const &tsv) {
        string r;
        generate(
          std::back_inserter(r), q_put_, q_put_request{mk_request_id("qp-"), q_name, msg_id, descript, ttl, tsv});
        return r;
      }

      string q_get(string const &q_name) {
        string r;
        generate(std::back_inserter(r), q_get_, q_get_request{mk_request_id("qg-"), q_name});
        return r;
      }

      string q_ack(string const &q_name, string const &msg_id, string const &diag) {
        string r;
        generate(std::back_inserter(r), q_done_, q_done_request{mk_request_id("qd-"), q_name, msg_id, diag});
        return r;
      }

      string q_size(string const &q_name) {
        string r;
        generate(std::back_inserter(r), q_size_, q_size_request{mk_request_id("qs-"), q_name});
        return r;
      }

      // NOT YET: void q_add(string const &q_name);
      // NOT YET: void q_remove(string const &q_name);
      string q_maintain(string const &q_name, bool keep_ttl_items, bool flush_all) { // TODO: route or fix flush_all
        string r;
        generate(
          std::back_inserter(r),
          q_maintain_,
          q_maintain_request{mk_request_id("qm-"), q_name, keep_ttl_items, flush_all});
        return r;
      }
    };

  }

  /**
   * @brief Web api dtss client implementation
   * @details
   * The implementation using the dtss_session asio/async class for it's
   * low level operations.
   * It uses the request_generator, to generate the requests, and then
   * the boost spirit parsers to efficently translate the response back
   * into c++ structures.
   */
  struct dtss_client_impl {
   private:
    string host_ip; ///< the host ip of the dtss server
    int port;       ///< the web-api port of the dtss server
    mutable generator::request_generator
      g_; ///< mutable because it keeps the request-id incremeted(planning for next version with request id-dispatching)
    grammar::request_reply_grammar<char const *> rr_; ///< parser for request replies, common to many requests.
    grammar::find_reply_grammar<char const *> fr_;    ///< parser for the find request
    grammar::tsv_reply_grammar<char const *> tr_;     ///< parser for the read_ts family of requests,
    ssl::context ctx_{ssl::context::tls_client};      ///< the ssl context, loaded with certs at class construction,#(1)
                                                      ///< maybe we should refresh it regularly?
    string auth; ///< if non zero, the auth string to decorate the http communication with.
    using find_reply_type = grammar::request_reply_grammar<char const *>::start_type::attr_type;

    string parse_diag_info(string const &response) const {
      request_reply diag;
      if (!grammar::phrase_parser(response, rr_, diag))
        diag.ex_info = "unknown response:" + response;
      return diag.ex_info;
    }

    template <class RP, class Rx>
    auto do_request_(string const &req, RP const &response_parser, Rx &&extract_result) {
      ///< keep the ssl context, certs etc.
      using rp_type = typename RP::start_type::attr_type; // automagic from RP:  find_reply, tsv_reply etc.
      boost::asio::io_context ioc_; ///< the io-context for this impl class, there is only one, run by calling thread
      auto dtss_ = make_shared<dtss_session>(
        ioc_, ctx_, host_ip, port, auth); ///< the dtss session object for this impl.
      rp_type r;                          // the result of the response parser.
      string diag;                        // capture failure here
      dtss_->run(
        req,
        [&](string const &response) {
          if (!grammar::phrase_parser(response, response_parser, r))
            diag = parse_diag_info(response);
          dtss_->start_close(); // if a single shot, we start clean close operation here
        },
        nullptr // no timer processing in this case,
      );
      ioc_.run();      // will return when session work is done, and is closed/done
      if (diag.size()) // translate failure to exceptions
        throw std::runtime_error("Request failed:" + diag);
      return extract_result(r); // extract the usable result from the xxx_reply type, RVO hopefully eliminates cpy
    }

    template <class RP, class Rx, class Tx>
    void do_subscribe_(string const &req, RP const &response_parser, Rx &&process_result, Tx &&process_timer) {
      using rp_type = typename RP::start_type::attr_type; // automagic from RP:  find_reply, tsv_reply etc.
      boost::asio::io_context ioc_; ///< the io-context for this impl class, there is only one, run by calling thread
      auto dtss_ = make_shared<dtss_session>(
        ioc_, ctx_, host_ip, port, auth); ///< the dtss session object for this impl.
      rp_type r;                          // the result of the response parser.
      string diag;                        // capture failure here
      bool waiting_unsub_response{false}; // set to true when cleaning up
      dtss_->run(
        req,
        [&](string const &response) {
          if (waiting_unsub_response) {
            request_reply unsub_r;
            if (grammar::phrase_parser(response, rr_, unsub_r)) {
              // we could check the request id of the unsub_r, to match with the one generated for the unsub
              //  but that would require us to do the unsub respons generation within this function
              //  like use current request-id, and add 'u' to make it unsubscribe
              dtss_->start_close();
            } else {
              ; // ignore MESSAGE("got unexpected unsub response:"<<response<<"\nwill continue wait for unsub ack");
            }
          } else {
            if (grammar::phrase_parser(response, response_parser, r)) {
              string unsub = process_result(r); // process result, and get response back
              if (unsub.size() == 0) {
                dtss_->continue_read();
              } else {
                waiting_unsub_response = true;
                dtss_->send_request(unsub);
              }
            } else {
              diag = parse_diag_info(response);
              dtss_->start_close(); // fatal, we  close operation here
            }
          }
        },
        [&]() -> bool {
          string unsub = process_timer();                // invoke the timer-handler.
          if (unsub.size() && !waiting_unsub_response) { // stopping by timer-request, (unless already shutting down)
            waiting_unsub_response = true;               // tell we are shutting down
            dtss_->send_request(
              unsub);     // post unsubscribe, that will be handle in the other callback, and then terminate the run
            return false; // tell session to stop the timer.
          }
          return true; // let session continue the timer.
        });
      ioc_.run();      // will return when session work is done, and is closed/done
      if (diag.size()) // translate failure to exceptions
        throw std::runtime_error("Request failed:" + diag);
    }

   public:
    dtss_client_impl(string const &host_ip, int port, string auth, bool cert_validate)
      : host_ip{host_ip}
      , port{port}
      , auth{auth} {
      load_server_certificate(ctx_);
      ctx_.set_default_verify_paths(); // add default paths for ca
      if (cert_validate)
        ctx_.set_verify_mode(boost::asio::ssl::verify_peer);
      // defer until needed: dtss_=make_shared<dtss_session>(ioc_,ctx_,host_ip,port);
    }

    vector<ts_info> find(string const &pattern) {
      return do_request_(g_.find(pattern), fr_, [](auto const &r) {
        return r.ts_infos;
      });
    }

    void store_ts(ats_vector const &tsv, bool overwrite_on_write, bool cache_on_write) {
      auto diag = do_request_(g_.store_ts(tsv, overwrite_on_write, cache_on_write), rr_, [](auto const &r) {
        return r.ex_info;
      });
      if (diag.size())
        throw std::runtime_error(diag);
    }

    ats_vector
      read_ts(vector<string> const tsv, utcperiod p, bool use_ts_cached_read, utcperiod clip_result = utcperiod{}) {
      auto [r, u] = g_.read_ts(tsv, p, use_ts_cached_read, false, clip_result.valid() ? clip_result : p);
      return do_request_(r, tr_, [](auto const &r) {
        return r.tsv;
      });
    }

    /**
     * @brief subscribe to read_ts
     * @param fx callable that takes ats_vector const&, returns bool, if true, continue, if false then unsubscribe and
     * close request
     * @param tx callable that returns true if timer should continue to run
     */
    void subscribe_ts(
      vector<string> const tsv,
      utcperiod p,
      bool use_ts_cached_read,
      utcperiod clip_result,
      std::function<bool(ats_vector const &)> const &fx,
      std::function<bool()> const &ft) {
      auto r_u = g_.read_ts(tsv, p, use_ts_cached_read, true, clip_result.valid() ? clip_result : p);
      string req = std::get<0>(r_u);
      string unsub = std::get<1>(r_u);
      string continue_sub{""};
      do_subscribe_(
        req,
        tr_,
        [&](auto const &r) -> string {
          return fx(r.tsv)
                 ? continue_sub
                 : unsub; // so if fx(), return true, continue subscription, otherwise start unsubscribe/close sequence
        },
        [&]() -> string {
          return ft() ? continue_sub : unsub; // if ft(), then continue, else start close/shutdown sequence.
        });
    }

    vector<string> q_list() {
      grammar::q_list_response_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      return do_request_(g_.q_list(), p_, [](auto const &r) {
        return r.q_names;
      });
    }

    queue::msg_info q_msg_info(string const &q_name, string const &msg_id) {
      grammar::q_info_response_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      return do_request_(g_.q_msg_info(q_name, msg_id), p_, [](auto const &r) {
        return r.info;
      });
    }

    vector<queue::msg_info> q_msg_infos(string const &q_name) {
      grammar::q_infos_response_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      return do_request_(g_.q_msg_infos(q_name), p_, [](auto const &r) {
        return r.infos;
      });
    }

    void q_put(string const &q_name, string const &msg_id, string const &descript, utctime ttl, ats_vector const &tsv) {
      grammar::request_reply_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      auto diag = do_request_(g_.q_put(q_name, msg_id, descript, ttl, tsv), p_, [](auto const &r) {
        return r.ex_info;
      });
      if (diag.size())
        throw std::runtime_error("q_put:" + diag);
    }

    queue::tsv_msg_ q_get(string const &q_name, utctime max_wait) {
      grammar::q_get_response_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      auto r = do_request_(g_.q_get(q_name), p_, [](auto const &r) {
        return r.msg;
      });
      if (!r && max_wait > from_seconds(0.0)) {
        auto t_exit = utctime_now() + max_wait;
        std::chrono::milliseconds poll_rate(10);
        do {
          std::this_thread::sleep_for(poll_rate);
          r = do_request_(g_.q_get(q_name), p_, [](auto const &r) {
            return r.msg;
          });
        } while (!r && utctime_now() < t_exit);
      }
      return r;
    }

    void q_ack(string const &q_name, string const &msg_id, string const &diag) {
      grammar::request_reply_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      auto ex_info = do_request_(g_.q_ack(q_name, msg_id, diag), p_, [](auto const &r) {
        return r.ex_info;
      });
      if (ex_info.size())
        throw std::runtime_error("q_ack:" + ex_info);
    }

    size_t q_size(string const &q_name) {
      grammar::q_size_response_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      return do_request_(g_.q_size(q_name), p_, [](auto const &r) {
        return r.count;
      });
    }

    // NOT YET: void q_add(string const &q_name);
    // NOT YET: void q_remove(string const &q_name);
    void q_maintain(string const &q_name, bool keep_ttl_items, bool flush_all) {
      grammar::request_reply_grammar<grammar::request_iterator_t, grammar::request_skipper_t> p_;
      auto ex_info = do_request_(g_.q_maintain(q_name, keep_ttl_items, flush_all), p_, [](auto const &r) {
        return r.ex_info;
      });
      if (ex_info.size())
        throw std::runtime_error("q_maintain:" + ex_info);
    }
  };

  // impl routing goes here:
  dtss_client::dtss_client(string const &host_ip, int port, string auth, bool cert_validate)
    : impl{std::make_unique<dtss_client_impl>(host_ip, port, auth, cert_validate)} {
  }

  vector<ts_info> dtss_client::find(string const &pattern) const {
    return impl->find(pattern);
  }

  void dtss_client::store_ts(ats_vector const &tsv, bool recreate_ts, bool cache_on_write) const {
    impl->store_ts(tsv, recreate_ts, cache_on_write);
  }

  ats_vector dtss_client::read_ts(
    vector<string> const &ts_ids,
    utcperiod read_period,
    bool use_ts_cached_read,
    utcperiod clip_period) const {
    return impl->read_ts(ts_ids, read_period, use_ts_cached_read, clip_period);
  }

  void dtss_client::subscribe_ts(
    vector<string> const &ts_ids,
    utcperiod read_period,
    bool use_ts_cached_read,
    utcperiod clip_period,
    std::function<bool(ats_vector const &)> const &fx,
    std::function<bool()> const &ft) const {
    return impl->subscribe_ts(ts_ids, read_period, use_ts_cached_read, clip_period, fx, ft);
  }

  vector<string> dtss_client::q_list() {
    return impl->q_list();
  }

  queue::msg_info dtss_client::q_msg_info(string const &q_name, string const &msg_id) {
    return impl->q_msg_info(q_name, msg_id);
  }

  vector<queue::msg_info> dtss_client::q_msg_infos(string const &q_name) {
    return impl->q_msg_infos(q_name);
  }

  void dtss_client::q_put(
    string const &q_name,
    string const &msg_id,
    string const &descript,
    utctime ttl,
    ats_vector const &tsv) {
    impl->q_put(q_name, msg_id, descript, ttl, tsv);
  }

  queue::tsv_msg_ dtss_client::q_get(string const &q_name, utctime max_wait) {
    return impl->q_get(q_name, max_wait);
  }

  void dtss_client::q_ack(string const &q_name, string const &msg_id, string const &diag) {
    impl->q_ack(q_name, msg_id, diag);
  }

  size_t dtss_client::q_size(string const &q_name) {
    return impl->q_size(q_name);
  }

  // NOT YET: void q_add(string const &q_name);
  // NOT YET: void q_remove(string const &q_name);
  void dtss_client::q_maintain(string const &q_name, bool keep_ttl_items, bool flush_all) {
    impl->q_maintain(q_name, keep_ttl_items, flush_all);
  }

  dtss_client::~dtss_client() = default;

}
