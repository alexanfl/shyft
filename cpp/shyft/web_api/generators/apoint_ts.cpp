/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/spirit/include/support_adapt_adt_attributes.hpp>

namespace shyft::time_series::dd {

  /**
   * @brief point proxy
   *
   *  To help creating apoint_ts iterator that could allow iterating
   * over a time-series producing points
   *
   */
  struct point_proxy {
    ipoint_ts* ts; ///< not owning,
    size_t i;

    operator point() const {
      return point(ts->time(i), ts->value(i));
    }

    point_proxy& operator=(point const & p) {
      if (ts->time(i) != p.t)
        throw runtime_error("not supported, change time in an interator of points");
      if (dynamic_cast<gpoint_ts*>(ts))
        dynamic_cast<gpoint_ts*>(ts)->set(i, p.v);
      return *this;
    }

    utctime time() const {
      return ts->time(i);
    }

    double value() const {
      return ts->value(i);
    }
  };

  struct apoint_ts_c {
    using value_type = point_proxy;
    apoint_ts ats;

    apoint_ts_c(apoint_ts const & ats)
      : ats(ats) {
    }

    struct iterator
      : public boost::iterator_facade< iterator, point, boost::random_access_traversal_tag, point_proxy > {
      iterator() {
      }

      iterator(apoint_ts const & a, size_t i = 0)
        : ats{a}
        , i{i} {
      }

      void increment() {
        ++i;
      }

      void decrement() {
        --i;
      }

      void advance(difference_type n) {
        i = static_cast<size_t>(static_cast<difference_type>(i) + n);
      }

      bool equal(iterator const & other) const {
        return ats.ts == other.ats.ts && i == other.i;
      }

      difference_type distance_to(iterator const & other) const {
        return static_cast<difference_type>(other.i) - static_cast<difference_type>(i);
      }

      point_proxy dereference() const {
        return point_proxy{const_cast<ipoint_ts*>(ats.ts.get()), i};
      } // TODO: consider difference between const and non-const dereference

     private:
      apoint_ts ats;
      size_t i{0};
    };

    using const_iterator = iterator;

    iterator begin() const {
      return iterator(ats, 0u);
    }

    iterator end() const {
      return iterator(ats, ats.size());
    }
  };

}

/** @brief adapter for apoint_ts, suited for boost.spirit.karma
 *
 * members of the tuple are:
 *  bool: true if the ts is non-empty
 *  bool: true if stair-case start of step (otherwise linear between points)
 *  apoint_ts_container: a boost.spirit.traits enabled container that provide the points
 */
BOOST_FUSION_ADAPT_ADT(
  shyft::time_series::dd::apoint_ts,
  (bool, bool, obj.size() > 0, val)(
    bool,
    bool,
    obj.size() && obj.point_interpretation() == shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,
    val)(
    shyft::time_series::dd::apoint_ts_c,
    shyft::time_series::dd::apoint_ts_c,
    shyft::time_series::dd::apoint_ts_c(obj),
    val))

namespace shyft::web_api::generator {

  template <class OutputIterator>
  apoint_ts_generator<OutputIterator>::apoint_ts_generator(bool pure_ts)
    : apoint_ts_generator::base_type(tsg) {
    using ka::true_;
    using ka::false_;
    using ka::bool_;
    using ka::omit;
    using ka::eps;
    using ka::lit;
    using ka::_1;
    using ka::_val;
    using ka::string;
    if (pure_ts) {
      tsg =

        (&bool_(true)[_1 = phx::bind(&apoint_ts::size, _val) != phx::val(0)] // only output if ts is non-empty
         << lit("{\"id\":\"") << string[_1 = phx::bind(&apoint_ts::id, _val)] << lit('"') << lit(",\"pfx\":")
         << bool_
              [_1 = (phx::bind(&apoint_ts::point_interpretation, _val) == time_series::ts_point_fx::POINT_AVERAGE_VALUE)]
         << lit(",\"time_axis\":") << ta_[_1 = phx::bind(&apoint_ts::time_axis, _val)] << lit(",\"values\":[")
         << -(d_ % ',')[_1 = phx::bind(&apoint_ts::values, _val)] << lit("]}"))
        | (&true_ << lit("{}"));
    } else {
      tsg = (&true_ << "{\"pfx\":" << bool_ << ",\"data\":" << '[' << -(pt_ % ',') << lit(']') << '}')
          | (omit[bool_] << "{\"pfx\":" << bool_ << lit(",\"data\":") << '[' << -(pt_ % ',') << lit(']') << lit('}'));
    }
    tsg.name("apoint_ts");
    // debug(tsg);
  }

  template <class OutputIterator>
  atsv_generator<OutputIterator>::atsv_generator(bool pure_ts)
    : atsv_generator::base_type(tsg)
    , ats_{pure_ts} {
    tsg = '[' << -(ats_ % ',') << ']';
    tsg.name("atsv");
  }

  template struct apoint_ts_generator<generator_output_iterator>;
  template struct atsv_generator<generator_output_iterator>;
}
