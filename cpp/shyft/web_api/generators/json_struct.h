#pragma once
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/generators/proxy_attr.h>
#include <shyft/web_api/json_struct.h>

#include <algorithm>

namespace shyft::web_api::generator {
  using shyft::web_api::energy_market::json;
  using shyft::web_api::energy_market::value_type;

  template <class OutputIterator>
  struct emit<OutputIterator, value_type> {
    emit(OutputIterator& oi, value_type const & vt) {
      boost::apply_visitor(emit_visitor(&oi), vt);
    }
  };

  template <class OutputIterator>
  struct emit<OutputIterator, json> {
    emit(OutputIterator& oi, json const & j) {
      emit_object<OutputIterator> oo(oi);
      // Iterate over each key-value pair in the map:
      std::for_each(j.m.begin(), j.m.end(), [&oo](auto const el) {
        oo.def(el.first, el.second);
      });
    }
  };

  x_emit_vec(json);
  x_emit_vec(vector<json>); // To also emit vector<vector<json>
}
