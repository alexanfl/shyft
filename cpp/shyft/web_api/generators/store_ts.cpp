#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {
  template <class OutputIterator>
  store_ts_request_generator<OutputIterator>::store_ts_request_generator()
    : store_ts_request_generator::base_type(g_)
    , tsv_{true} {
    g_ = R"_(store_ts {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&store_ts_request::request_id, ka::_val)] << '"' << R"_(,"merge_store":)_"
      << ka::bool_[ka::_1 = phx::bind(&store_ts_request::merge_store, ka::_val)] << R"_(,"recreate_ts":)_"
      << ka::bool_[ka::_1 = phx::bind(&store_ts_request::recreate_ts, ka::_val)] << R"_(,"cache":)_"
      << ka::bool_[ka::_1 = phx::bind(&store_ts_request::cache, ka::_val)] << R"_(,"tsv":)_"
      << tsv_[ka::_1 = phx::bind(&store_ts_request::tsv, ka::_val)] << '}';
    g_.name("store_ts_request");
  }
  template struct store_ts_request_generator<generator_output_iterator>;

}
