#pragma once
#include <string>
#include <boost/spirit/include/karma.hpp>

namespace shyft::web_api::generator {
  namespace ka = boost::spirit::karma;

  /**
   * @brief generator for strings with added escape character at quoation marks
   * @details inspired by
   * @ref http://boost-spirit.com/home/articles/karma-examples/generate-escaped-string-output-using-spirit-karma/
   * @tparam OutputIterator
   */
  template <class OutputIterator>
  struct escaped_string_generator : ka::grammar<OutputIterator, std::string()> {
    escaped_string_generator()
      : escaped_string_generator::base_type(esc_str) {
      esc_char.add('\a', "\\a")('\b', "\\b")('\f', "\\f")('\n', "\\n")('\r', "\\r")('\t', "\\t")('\v', "\\v")(
        '\\', "\\\\")('\'', "\\\'")('"', "\\\"");
      esc_str = *(esc_char | ka::char_);
    }

    ka::rule<OutputIterator, std::string()> esc_str;
    ka::symbols<char, char const *> esc_char;
  };
}
