/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {

  // cpp

  template <class OutputIterator>
  q_get_response_generator<OutputIterator>::q_get_response_generator()
    : q_get_response_generator::base_type(g_) {
    g_ = R"_({"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_get_response::request_id, ka::_val)] << '"' << R"_(,"msg":)_"
      << (
           // notice here how we use if then else generator, checking if has message, deref and emit, else null
           (&ka::bool_(true)[ka::_1 = phx::bind(&q_get_response::has_msg, ka::_val)] // if has_msg,(it matches
                                                                                     // ka::bool_(true)) then
            << tsv_msg_[ka::_1 = phx::bind(&q_get_response::get_msg, ka::_val)]      // do this
            )
           | (&ka::true_ << ka::lit("null")) // else, always  true, emit a null
           )
      << '}';
    g_.name("q_tsv_msg");
  }
  template struct q_get_response_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_tsv_msg_generator<OutputIterator>::q_tsv_msg_generator()
    : q_tsv_msg_generator::base_type(g_)
    , tsv_{true} {
    g_ = R"_({"info":)_" << info_[ka::_1 = phx::bind(&queue::tsv_msg::info, ka::_val)] << R"_(,"tsv":)_"
                         << tsv_[ka::_1 = phx::bind(&queue::tsv_msg::tsv, ka::_val)] << '}';
    g_.name("q_tsv_msg");
  }
  template struct q_tsv_msg_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_msg_info_generator<OutputIterator>::q_msg_info_generator()
    : q_msg_info_generator::base_type(g_) {
    g_ = R"_({"msg_id":")_"
      << ka::string[ka::_1 = phx::bind(&queue::msg_info::msg_id, ka::_val)] << '"' << R"_(,"description":")_"
      << es_[ka::_1 = phx::bind(&queue::msg_info::description, ka::_val)] << '"' << R"_(,"ttl":)_"
      << t_[ka::_1 = phx::bind(&queue::msg_info::ttl, ka::_val)] << R"_(,"created":)_"
      << t_[ka::_1 = phx::bind(&queue::msg_info::created, ka::_val)] << R"_(,"fetched":)_"
      << t_[ka::_1 = phx::bind(&queue::msg_info::fetched, ka::_val)] << R"_(,"done":)_"
      << t_[ka::_1 = phx::bind(&queue::msg_info::done, ka::_val)] << R"_(,"diagnostics":")_"
      << es_[ka::_1 = phx::bind(&queue::msg_info::diagnostics, ka::_val)] << '"' << '}';
    g_.name("q_msg_info");
  }
  template struct q_msg_info_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_put_request_generator<OutputIterator>::q_put_request_generator()
    : q_put_request_generator::base_type(g_)
    , tsv_{true} {
    g_ = R"_(q_put {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_put_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_put_request::q_name, ka::_val)] << '"' << R"_(,"msg_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_put_request::msg_id, ka::_val)] << '"' << R"_(,"description":")_"
      << ka::string[ka::_1 = phx::bind(&q_put_request::descript, ka::_val)] << '"' << R"_(,"ttl":)_"
      << t_[ka::_1 = phx::bind(&q_put_request::ttl, ka::_val)] << R"_(,"tsv":)_"
      << tsv_[ka::_1 = phx::bind(&q_put_request::tsv, ka::_val)] << '}';
    g_.name("q_put_request");
  }
  template struct q_put_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_get_request_generator<OutputIterator>::q_get_request_generator()
    : q_get_request_generator::base_type(g_) {
    g_ = R"_(q_get {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_get_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_get_request::q_name, ka::_val)] << '"' << '}';
    g_.name("q_put_request");
  }
  template struct q_get_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_list_request_generator<OutputIterator>::q_list_request_generator()
    : q_list_request_generator::base_type(g_) {
    g_ = R"_(q_list {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_list_request::request_id, ka::_val)] << '"' << '}';
    g_.name("q_list_request");
  }
  template struct q_list_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_list_response_generator<OutputIterator>::q_list_response_generator()
    : q_list_response_generator::base_type(g_) {
    g_ = R"_({"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_list_response::request_id, ka::_val)]
      << "\","
         R"_("q_names":[)_"
      << -(('"' << ka::string << '"') % ',')[ka::_1 = phx::bind(&q_list_response::q_names, ka::_val)] << "]"

      << '}';
    g_.name("q_list_response");
  }
  template struct q_list_response_generator<generator_output_iterator>;

  //-----
  template <class OutputIterator>
  q_info_request_generator<OutputIterator>::q_info_request_generator()
    : q_info_request_generator::base_type(g_) {
    g_ = R"_(q_info {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_info_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_info_request::q_name, ka::_val)] << '"' << R"_(,"msg_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_info_request::msg_id, ka::_val)] << '"' << '}';
    g_.name("q_info_request");
  }
  template struct q_info_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_done_request_generator<OutputIterator>::q_done_request_generator()
    : q_done_request_generator::base_type(g_) {
    g_ = R"_(q_done {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_done_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_done_request::q_name, ka::_val)] << '"' << R"_(,"msg_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_done_request::msg_id, ka::_val)] << '"' << R"_(,"diagnostics":")_"
      << es_[ka::_1 = phx::bind(&q_done_request::diagnostics, ka::_val)] << '"' << '}';
    g_.name("q_done_request");
  }
  template struct q_done_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_info_response_generator<OutputIterator>::q_info_response_generator()
    : q_info_response_generator::base_type(g_) {
    g_ = R"_({"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_info_response::request_id, ka::_val)]
      << "\","
         R"_("info":)_"
      << info_[ka::_1 = phx::bind(&q_info_response::info, ka::_val)] << '}';
    g_.name("q_info_response");
  }
  template struct q_info_response_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_infos_request_generator<OutputIterator>::q_infos_request_generator()
    : q_infos_request_generator::base_type(g_) {
    g_ = R"_(q_infos {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_infos_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_infos_request::q_name, ka::_val)] << '"' << '}';
    g_.name("q_infos_request");
  }
  template struct q_infos_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_infos_response_generator<OutputIterator>::q_infos_response_generator()
    : q_infos_response_generator::base_type(g_) {
    g_ = R"_({"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_infos_response::request_id, ka::_val)]
      << "\","
         R"_("infos":[)_"
      << -(info_ % ',')[ka::_1 = phx::bind(&q_infos_response::infos, ka::_val)] << "]" << '}';
    g_.name("q_infos_response");
  }
  template struct q_infos_response_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_size_request_generator<OutputIterator>::q_size_request_generator()
    : q_size_request_generator::base_type(g_) {
    g_ = R"_(q_size {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_size_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_size_request::q_name, ka::_val)] << '"' << '}';
    g_.name("q_infos_request");
  }
  template struct q_size_request_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_size_response_generator<OutputIterator>::q_size_response_generator()
    : q_size_response_generator::base_type(g_) {
    g_ = R"_({"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_size_response::request_id, ka::_val)]
      << "\","
         R"_("count":)_"
      << ka::int_[ka::_1 = phx::bind(&q_size_response::count, ka::_val)] << '}';
    g_.name("q_infos_response");
  }
  template struct q_size_response_generator<generator_output_iterator>;

  template <class OutputIterator>
  q_maintain_request_generator<OutputIterator>::q_maintain_request_generator()
    : q_maintain_request_generator::base_type(g_) {
    g_ = R"_(q_maintain {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&q_maintain_request::request_id, ka::_val)] << '"' << R"_(,"q_name":")_"
      << ka::string[ka::_1 = phx::bind(&q_maintain_request::q_name, ka::_val)] << '"' << R"_(,"keep_ttl_items":)_"
      << ka::bool_[ka::_1 = phx::bind(&q_maintain_request::keep_ttl_items, ka::_val)] << R"_(,"flush_all":)_"
      << ka::bool_[ka::_1 = phx::bind(&q_maintain_request::flush_all, ka::_val)] << '}';
    g_.name("q_maintain_request");
  }
  template struct q_maintain_request_generator<generator_output_iterator>;

}
