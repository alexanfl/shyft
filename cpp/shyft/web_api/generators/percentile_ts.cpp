#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {
  template <class OutputIterator>
  percentile_ts_request_generator<OutputIterator>::percentile_ts_request_generator()
    : percentile_ts_request_generator::base_type(g_) {
    g_ = R"_(percentile {"request_id":")_"
      << ka::string[ka::_1 = phx::bind(&percentile_ts_request::request_id, ka::_val)] << '"' << R"_(,"read_period":)_"
      << p_[ka::_1 = phx::bind(&percentile_ts_request::read_period, ka::_val)] << R"_(,"time_axis":)_"
      << ta_[ka::_1 = phx::bind(&percentile_ts_request::ta, ka::_val)] << R"_(,"percentiles":[)_"
      << (ka::int_ % ',')[ka::_1 = phx::bind(&percentile_ts_request::percentiles, ka::_val)] << ']' << R"_(,"cache":)_"
      << ka::bool_[ka::_1 = phx::bind(&percentile_ts_request::cache, ka::_val)] << R"_(,"ts_ids":[)_"
      << (('"' << ka::string << '"') % ',')[ka::_1 = phx::bind(&percentile_ts_request::ts_ids, ka::_val)] << ']'
      << R"_(,"subscribe":)_" << ka::bool_[ka::_1 = phx::bind(&percentile_ts_request::subscribe, ka::_val)]
      << R"_(,"ts_fmt":)_" << ka::bool_[ka::_1 = phx::bind(&percentile_ts_request::ts_fmt, ka::_val)] << '}';
    g_.name("percentile_ts_request");
  }
  template struct percentile_ts_request_generator<generator_output_iterator>;

}
