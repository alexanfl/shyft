/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/spirit/include/support_adapt_adt_attributes.hpp>
BOOST_FUSION_ADAPT_ADT(
  shyft::dtss::ts_info,
  (std::string, std::string, obj.name, val)(
    bool,
    bool,
    obj.point_fx == shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,
    val)(shyft::core::utctime, shyft::core::utctime, obj.delta_t, val)(std::string, std::string, obj.olson_tz_id, val)(
    shyft::core::utcperiod,
    shyft::core::utcperiod,
    obj.data_period,
    val)(shyft::core::utctime, shyft::core::utctime, obj.created, val)(
    shyft::core::utctime,
    shyft::core::utctime,
    obj.modified,
    val))

namespace shyft::web_api::generator {

  template <class OutputIterator>
  ts_info_generator<OutputIterator>::ts_info_generator()
    : ts_info_generator::base_type(pg) {
    using ka::true_;
    using ka::bool_;
    using boost::spirit::ascii::string;
    using ka::omit;
    using ka::real_generator;

    pg = "{\"name\":\""
      << string << "\",\"pfx\":" << bool_ << ",\"delta_t\":" << time_ << ",\"olson_tz_id\":\"" << string
      << "\""
         ",\"data_period\":"
      << period_ << ",\"created\":" << time_ << ",\"modified\":" << time_ << "}";
    pg.name("ts_info");
  }

  template <class OutputIterator>
  ts_info_vector_generator<OutputIterator>::ts_info_vector_generator()
    : ts_info_vector_generator::base_type(tsig) {

    tsig = '[' << -(tsi_ % ',') << ']';
    tsig.name("ts_info_vector");
  }


  template struct ts_info_generator<generator_output_iterator>;
  template struct ts_info_vector_generator<generator_output_iterator>;
}
