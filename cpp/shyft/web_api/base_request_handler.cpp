#include <shyft/web_api/bg_work_result.h>

namespace shyft::web_api {

  bool server_auth::needed() const {
    return auth_tokens.size() > 0;
  }

  bool server_auth::valid(string const &token) const {
    return !needed() || (auth_tokens.find(token) != end(auth_tokens));
  }

  void server_auth::add(vector<string> const &tokens) {
    for (auto const &t : tokens)
      auth_tokens.insert(t);
  }

  void server_auth::remove(vector<string> const &tokens) {
    for (auto const &t : tokens)
      auth_tokens.erase(t);
  }

  vector<string> server_auth::tokens() const {
    vector<string> r;
    r.reserve(auth_tokens.size());
    for (auto const &t : auth_tokens)
      r.emplace_back(t);
    return r;
  }
}
