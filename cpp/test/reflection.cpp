#ifndef WIN32
// TODO: make adjustment so that ms c++ survives
#include <cstdint>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include <boost/describe/enumerators.hpp>
#include <boost/mp11/algorithm.hpp>
#include <doctest/doctest.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/core/reflection/formatters.h>

namespace shyft {

  SHYFT_DEFINE_ENUM(A, std::uint8_t, (a, b, c));

  struct B0 {
    int a0;

    SHYFT_DEFINE_STRUCT(B0, (), (a0));

    auto operator<=>(const B0 &) const = default;

   private:
    SHYFT_DEFINE_SERIALIZE(B0);
  };

  struct B1 {
    int a1;

    SHYFT_DEFINE_STRUCT(B1, (), (a1));

    auto operator<=>(const B1 &) const = default;

   private:
    SHYFT_DEFINE_SERIALIZE(B1);
  };

  struct C0 : B0 {
    int c0;

    SHYFT_DEFINE_STRUCT(C0, (B0), (c0));

    auto operator<=>(const C0 &) const = default;

   private:
    SHYFT_DEFINE_SERIALIZE(C0);
  };

  struct C1
    : B0
    , B1 {
    int c1;

    SHYFT_DEFINE_STRUCT(C1, (B0, B1), (c1));

    auto operator<=>(const C1 &) const = default;

   private:
    SHYFT_DEFINE_SERIALIZE(C1);
  };

  struct reflected_struct_1 {
    int a;
    int b;
    std::vector<int> c;

    SHYFT_DEFINE_STRUCT(reflected_struct_1, (), (a, b, c))
  };

  struct reflected_struct_2 {
    int a;
    int b;
    std::shared_ptr<int> c;
    reflected_struct_1 rs_1;
    std::string s;

    SHYFT_DEFINE_STRUCT(reflected_struct_2, (), (a, b, c, rs_1, s))
  };

  struct reflected_struct_3 {
    std::vector<std::shared_ptr<int>> a;
    std::vector<reflected_struct_1> b;
    std::shared_ptr<reflected_struct_2> c;
    std::string d;

    SHYFT_DEFINE_STRUCT(reflected_struct_3, (), (a, b, c, d))
  };

  struct reflected_struct_4 : reflected_struct_3 {
    using super = reflected_struct_3;
    int a_1;
    int b_2;
    std::vector<std::string> c_3;

    SHYFT_DEFINE_STRUCT(reflected_struct_4, (reflected_struct_3), (a_1, b_2, c_3));
  };

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::A)
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::B0)
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::B1)
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::C0)
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::C1)

namespace shyft {

  TEST_CASE("reflection") {

    auto reblob = []<typename T>(T const &o) {
      std::stringstream stream;
      reflection::write_blob(stream, o);
      T o_also{};
      reflection::read_blob(stream, o_also);
      return o_also;
    };
    auto reserialize = []<typename T>(T const &o) {
      std::stringstream stream;
      {
        shyft::core::core_oarchive archive{stream, shyft::core::core_arch_flags};
        archive << o;
      }
      T o_also;
      {
        shyft::core::core_iarchive archive{stream, shyft::core::core_arch_flags};
        archive >> o_also;
      }
      return o_also;
    };

    SUBCASE("enums") {

      const A invalid_enum{enumerator_count<A> + 1};

      CHECK(enumerator_count<A> == 3);

      CHECK(with_enum(A::b, [](auto e) {
        return e == A::b;
      }));
      CHECK(with_enum_or(
        invalid_enum,
        [](auto) {
          return false;
        },
        [] {
          return true;
        }));

      boost::mp11::mp_for_each<boost::describe::describe_enumerators<A>>([&](auto e) {
        std::string_view e_name = e.name;

        CHECK(e_name == fmt::format("{}", e.value));
        CHECK(reblob(e.value) == e.value);
        CHECK(reserialize(e.value) == e.value);
        if (e_name == "a")
          CHECK(e.value == A::a);
        else if (e_name == "b")
          CHECK(e.value == A::b);
        else if (e_name == "c")
          CHECK(e.value == A::c);
        else
          FAIL("unexpected enum");
      });

      CHECK(fmt::format("{}", invalid_enum) == fmt::format("{}", etoi(invalid_enum)));
    }

    SUBCASE("structs") {

      B0 b0{4};
      C0 c0{B0{1}, 2};
      C1 c1{B0{3}, B1{4}, 5};


      CHECK(reserialize(b0) == b0);
      CHECK(reserialize(c0) == c0);
      CHECK(reserialize(c1) == c1);

      CHECK(fmt::format("{}", b0) == "{ .a0=4 }");
      CHECK(fmt::format("{}", c0) == "{ { .a0=1 }, .c0=2 }");
      CHECK(fmt::format("{}", c1) == "{ { .a0=3 }, { .a1=4 }, .c1=5 }");
    }

    SUBCASE("paths_of") {
      using L0 = paths_of<reflected_struct_1>;
      static_assert(
        std::is_same_v<
          L0,
          std::tuple<
            std::tuple<constant_t<&reflected_struct_1::a>>,
            std::tuple<constant_t<&reflected_struct_1::b>>,
            std::tuple<constant_t<&reflected_struct_1::c>>>>);

      using L1 = paths_of<reflected_struct_2>;
      static_assert(
        std::is_same_v<
          L1,
          std::tuple<
            std::tuple<constant_t<&reflected_struct_2::a>>,
            std::tuple<constant_t<&reflected_struct_2::b>>,
            std::tuple<constant_t<&reflected_struct_2::c>>,
            std::tuple<constant_t<&reflected_struct_2::rs_1>>,
            std::tuple<constant_t<&reflected_struct_2::s>>>>);

      using L2 = paths_of<reflected_struct_3>;
      static_assert(
        std::is_same_v<
          L2,
          std::tuple<
            std::tuple<constant_t<&reflected_struct_3::a>>,
            std::tuple<constant_t<&reflected_struct_3::b>>,
            std::tuple<constant_t<&reflected_struct_3::c>>,
            std::tuple<constant_t<&reflected_struct_3::d>>>>);

      using L3 = paths_of<reflected_struct_4>;
      static_assert(
        std::is_same_v<
          L3,
          std::tuple<
            std::tuple<constant_t<&reflected_struct_3::a>>,
            std::tuple<constant_t<&reflected_struct_3::b>>,
            std::tuple<constant_t<&reflected_struct_3::c>>,
            std::tuple<constant_t<&reflected_struct_3::d>>,
            std::tuple<constant_t<&reflected_struct_4::a_1>>,
            std::tuple<constant_t<&reflected_struct_4::b_2>>,
            std::tuple<constant_t<&reflected_struct_4::c_3>>>>);
    }
  }
}
#endif
