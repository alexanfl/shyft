#pragma once
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/fx_merge.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <shyft/energy_market/stm/stm_system.h>

namespace mocks {
  using std::string;
  using std::vector;
  using std::unique_ptr;
  using namespace shyft;
  using namespace shyft::core;
  using namespace shyft::energy_market;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::ats_vector;

  using shyft::time_series::dd::gta_t;
  using shyft::time_series::ts_point_fx;

  static inline shyft::core::utctime _t(int64_t t1970s) {
    return shyft::core::utctime{shyft::core::seconds(t1970s)};
  }

  vector<apoint_ts> mk_expression(string const &ts_url_base, utctime t, utctimespan dt, int n);

  vector<apoint_ts> mk_expressions(string const &cname);

  /** a slightly modified dstm-server for testing  */
  struct dstm_server {
    string fx_mid() const; ///< set during fx-callbacks, to be used by tester to verify
    string fx_arg() const; ///< callback-resulting in modified server-side data.
    explicit dstm_server();
    explicit dstm_server(string const &root_dir);
    ~dstm_server();
    dstm_server(dstm_server const &) = delete;
    dstm_server(dstm_server &&) = delete;

    void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads);
    bool web_api_running() const;
    void stop_web_api();
    void set_listening_ip(string host_ip);
    int start_server();
    //-- fwds
    void *srv_dtss() const;

    void do_add_model(string name, stm::stm_system_ mdl);
    void dtss_do_store_ts(ats_vector const &tsv, bool cache, bool replace);
    stm::stm_system_ do_get_model(string name);
    void sm_notify_change(string url_id);
    ats_vector dtss_read_callback(vector<string> const &ts_ids, utcperiod p);
   private:
    struct impl;             // p_impl struct
    unique_ptr<impl> p_impl; //
  };
}
