#include "test_pch.h"

#include "test_parser.h"

#include <algorithm>
#include <string>
#include <map>
#include <memory>
#include <vector>

#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/web_api/energy_market/grammar.h>

using std::vector;

using shyft::time_series::dd::apoint_ts;
using shyft::web_api::energy_market::attribute_value_type;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::stm::t_xy_;

TEST_SUITE_BEGIN("web_api");

using shyft::web_api::energy_market::json;
using shyft::web_api::energy_market::request;

TEST_CASE("web_api/json/integer_list_grammar") {
  shyft::web_api::grammar::integer_list_grammar<char const *> grammar;
  vector<int> test;
  auto ok = test::phrase_parser(R"_([1, 2, 3, 4, 5])_", grammar, test);
  CHECK_EQ(true, ok);
  CHECK_EQ(test, vector<int>{1, 2, 3, 4, 5});
}

TEST_CASE("web_api/json/simple") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_(
        {
            "a": [1,2,3,4]
        }
        )_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  vector<int> expected{1, 2, 3, 4};
  // Check that key "a" is present:
  auto it = result.m.find("a");
  CHECK_EQ(true, (it != result.m.end()));
  CHECK_EQ(true, std::ranges::equal(expected, result.required<vector<int>>("a")));
}

TEST_CASE("web_api/json/empty") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(R"_({})_", grammar, result);
  CHECK_EQ(true, ok);
  CHECK_EQ(0, result.m.size());

  ok = test::phrase_parser("null", grammar, result);
  CHECK_EQ(true, ok);
  CHECK_EQ(0, result.m.size());
}

TEST_CASE("web_api/json/boolean") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_(
        {
            "booltrue": true,
            "boolfalse": false
        }
        )_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  // CHECK KEYS:
  CHECK_EQ(true, result.m.find("booltrue") != result.m.end());
  CHECK_EQ(true, result.m.find("boolfalse") != result.m.end());
  // CHECK VALUES:
  CHECK_EQ(true, result.required<bool>("booltrue"));
  CHECK_EQ(false, result.required<bool>("boolfalse"));
}

TEST_CASE("web_api/json/double") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_({"a": 1.05,
                 "b": 2,
                 "c": 2.})_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  CHECK_EQ(result.m.size(), 3);
  CHECK_EQ(result.required<double>("a"), doctest::Approx(1.05));
  CHECK_EQ(result.required<int>("b"), 2);
  CHECK_EQ(result.required<double>("c"), doctest::Approx(2.0));
}

TEST_CASE("web_api/json/multiple") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_(
        {
            "a": [1,2,3,4],
            "b": 3,
            "c3": "a string"
        }
        )_",
    grammar,
    result);
  CHECK_EQ(true, ok); // Checks that we have a full match
  // CHECK KEYS:
  CHECK_EQ(true, result.m.find("a") != result.m.end());
  CHECK_EQ(true, result.m.find("b") != result.m.end());
  CHECK_EQ(true, result.m.find("c3") != result.m.end());
  // CHECK VALUES:
  CHECK_EQ(true, std::ranges::equal(vector<int>{1, 2, 3, 4}, result.required<vector<int>>("a")));
  CHECK_EQ(3, result.required<int>("b"));
  CHECK_EQ("a string", result.required<std::string>("c3"));
}

TEST_CASE("web_api/json/utcperiod") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  using shyft::core::utcperiod;
  using shyft::core::calendar;
  using shyft::core::from_seconds;

  json result;
  auto ok = test::phrase_parser(
    R"_(
            {"t1" : ["2018-02-18T01:02:03Z", "2018-03-01T00:00:00Z"],
             "t2" : [1.0, 2.0]            
            }
        )_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  CHECK_EQ(true, result.m.find("t1") != result.m.end());
  CHECK_EQ(true, result.m.find("t2") != result.m.end());

  calendar cal;
  CHECK_EQ(utcperiod(cal.time(2018, 2, 18, 1, 2, 3), cal.time(2018, 3, 1, 0, 0, 0)), result.required<utcperiod>("t1"));
  CHECK_EQ(utcperiod(from_seconds(1), from_seconds(2)), result.required<utcperiod>("t2"));
}

TEST_CASE("web_api/json/time_axis") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  using shyft::time_axis::generic_dt;
  using shyft::core::calendar;
  using shyft::core::utctime;
  using shyft::core::from_seconds;

  json result;
  CHECK_EQ(
    true,
    test::phrase_parser(
      R"_({"ta1" : {"t0":1.0,"dt": 2.0,"n": 10},
                 "ta2" : {"calendar" : "Europe/Oslo", "t0" : "2018-01-01T00:00:00Z", "dt" : 86400, "n" : 20},
                 "ta3" : { "time_points" : [ 1, 2, 3, 4 ]}
            })_",
      grammar,
      result));
  CHECK_EQ(true, result.m.find("ta1") != result.m.end());
  CHECK_EQ(true, result.m.find("ta2") != result.m.end());
  CHECK_EQ(true, result.m.find("ta3") != result.m.end());

  generic_dt e1{from_seconds(1.0), from_seconds(2.0), 10};
  auto osl = std::make_shared<shyft::core::calendar>("Europe/Oslo");
  generic_dt e2{osl, calendar().time(2018, 1, 1), from_seconds(86400.0), 20};
  generic_dt e3{
    std::vector<utctime>{from_seconds(1.0), from_seconds(2.0), from_seconds(3.0), from_seconds(4.0)}
  };
  CHECK_EQ(e1, result.required<generic_dt>("ta1"));
  CHECK_EQ(e2, result.required<generic_dt>("ta2"));
  CHECK_EQ(e3, result.required<generic_dt>("ta3"));
}

TEST_CASE("web_api/json/recursive") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_(
        {
          "a": [1, 2, 3, 4],
          "subdata": {
            "b": "a string",
            "c": null
          }
        }
        )_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  // CHECK KEYS:
  CHECK_EQ(true, result.m.find("a") != result.m.end());
  CHECK_EQ(true, result.m.find("subdata") != result.m.end());
  // CHECK VALUES:
  CHECK_EQ(true, std::ranges::equal(vector<int>{1, 2, 3, 4}, result.required<vector<int>>("a")));
  auto subdata = result.required<json>("subdata");
  CHECK_EQ(true, subdata.m.find("b") != subdata.m.end());
  CHECK_EQ("a string", subdata.required<std::string>("b"));
  CHECK_EQ(true, subdata.m.find("c") != subdata.m.end());
  auto c = subdata.required<json>("c");
  CHECK_EQ(0, c.m.size());
}

TEST_CASE("web_api/json/recursive/list") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_(
        {
          "a": [1, 2, 3, 4],
          "subdata": [{
            "b": "a string"
          },
          {
            "b": "another string"
          }]
        }
        )_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  // CHECK KEYS:
  CHECK_EQ(true, result.m.find("a") != result.m.end());
  CHECK_EQ(true, result.m.find("subdata") != result.m.end());
  // CHECK VALUES:
  CHECK_EQ(true, std::ranges::equal(vector<int>{1, 2, 3, 4}, result.required<vector<int>>("a")));
  auto subdata = result.required<vector<json>>("subdata");
  CHECK_EQ(true, subdata[0].m.find("b") != subdata[0].m.end());
  CHECK_EQ("a string", subdata[0].required<std::string>("b"));
  CHECK_EQ(true, subdata[1].m.find("b") != subdata[1].m.end());
  CHECK_EQ("another string", subdata[1].required<std::string>("b"));
}

TEST_CASE("web_api/json/recursive/list-of-lists") {
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_({
              "a": [[{},{},{}],[{},{},{}]]
            })_",
    grammar,
    result);
  CHECK_EQ(true, ok);
  CHECK_EQ(result.m.size(), 1);

  auto a = result.required<vector<vector<json>>>("a");
  CHECK_EQ(a.size(), 2);
  CHECK_EQ(a[0].size(), 3);
  CHECK_EQ(a[0][0].m.size(), 0);
  CHECK_EQ(a[0][1].m.size(), 0);
  CHECK_EQ(a[0][2].m.size(), 0);
  CHECK_EQ(a[1].size(), 3);
  CHECK_EQ(a[1][0].m.size(), 0);
  CHECK_EQ(a[1][1].m.size(), 0);
  CHECK_EQ(a[1][2].m.size(), 0);
}

TEST_CASE("web_api/json/attribute_value_type") {
  using namespace shyft::time_series;
  using namespace shyft::time_axis;
  shyft::web_api::grammar::json_grammar<char const *> grammar;
  json result;
  auto ok = test::phrase_parser(
    R"_(
        {
          "a": {
                "id"        : "abcd" ,
                "pfx"       : true,
                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                "values"    : [ 0.1, 0.2,null,0.4 ]
                },
          "b": [
                {
                        "2018-02-18T01:02:03Z": [[1.0, 3.0], [1.5, 2.5], [2.0, 2.0], [2.5, 1.5], [3.0, 1.0]],
                        "2018-03-01T00:00:00Z": [[0.1, 0.0], [0.2, 0.5], [0.3, 1.0], [0.4, 1.5], [0.5, 2.0]]
                },
                {
                "id"        : "abcd" ,
                "pfx"       : true,
                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                "values"    : [ 0.1, 0.2,null,0.4 ]
                }
            ]
        }
        )_",
    grammar,
    result);
  // Set expected values:
  apoint_ts e(
    "abcd",
    apoint_ts(
      generic_dt(vector<utctime>{from_seconds(1), from_seconds(2), from_seconds(3), from_seconds(4), from_seconds(5)}),
      vector<double>{0.1, 0.2, shyft::nan, 0.4},
      POINT_AVERAGE_VALUE));

  xy_point_curve points1(xy_point_curve::make({1.0, 1.5, 2.0, 2.5, 3.0}, {3.0, 2.5, 2.0, 1.5, 1.0}));
  xy_point_curve points2(xy_point_curve::make({0.1, 0.2, 0.3, 0.4, 0.5}, {0.0, 0.5, 1.0, 1.5, 2.0}));
  utctime t1, t2;
  calendar utc;
  t1 = utc.time(2018, 2, 18, 1, 2, 3);
  t2 = utc.time(2018, 3, 1, 0, 0, 0);
  auto a = std::make_shared< std::map<utctime, std::shared_ptr<xy_point_curve> > >();
  (*a)[t1] = std::make_shared<xy_point_curve>(points1);
  (*a)[t2] = std::make_shared<xy_point_curve>(points2);
  CHECK_EQ(ok, true);
  // CHECK KEYS:
  CHECK_NE(result.m.find("a"), result.m.end());
  CHECK_NE(result.m.find("b"), result.m.end());
  // CHECK VALUES:
  auto temp1 = result.required<attribute_value_type>("a");
  CHECK_EQ(boost::get<apoint_ts>(temp1), e);

  auto temp2 = result.required<vector<attribute_value_type>>("b");
  auto b = boost::get<t_xy_>(temp2[0]);
  CHECK_EQ(*((*a)[t1]), *((*b)[t1]));
  CHECK_EQ(*((*a)[t2]), *((*b)[t2]));
  auto c = boost::get<apoint_ts>(temp2[1]);
  CHECK_EQ(e, c);
}

TEST_CASE("web_api/request") {
  shyft::web_api::grammar::request_grammar<char const *> grammar;
  request res;
  auto ok = test::phrase_parser(
    R"_(
        read_ts {
            "a": [1,2,3,4],
            "b": 3,
            "c3": "a string"
        }
        )_",
    grammar,
    res);
  CHECK_EQ(true, ok); // Checks that we have a full match
  // CHECK KEYWORD:
  CHECK_EQ(res.keyword, "read_ts");
  // CHECK KEYS:
  CHECK_EQ(true, res.request_data.m.find("a") != res.request_data.m.end());
  CHECK_EQ(true, res.request_data.m.find("b") != res.request_data.m.end());
  CHECK_EQ(true, res.request_data.m.find("c3") != res.request_data.m.end());
  // CHECK VALUES:
  CHECK_EQ(true, std::ranges::equal(vector<int>{1, 2, 3, 4}, res.request_data.required<vector<int>>("a")));
  CHECK_EQ(3, res.request_data.required<int>("b"));
  CHECK_EQ("a string", res.request_data.required<std::string>("c3"));
}

TEST_CASE("web_api/complex_request") {
  /* notice that this test only verifies what has already been established in the
   * above tests, but targeted to a specific usage-scenario where we
   * expect quite complex requests.
   */
  shyft::web_api::grammar::request_grammar<char const *> grammar;
  request res;
  auto ok = test::phrase_parser(
    R"_(
        read_attributes {
            "request_id": "rid_1",
            "model_id": "mid_2",
            "hps" : [
                {
                    "id": 3,
                    "reservoirs": {
                        "component_ids": [2,9,17],
                        "attribute_ids": ["level.regulation_min","level.regulation_max"]
                    }
                },
                {
                    "id": 4,
                    "reservoirs": {
                        "component_ids": [3,9,2],
                        "attribute_ids": ["level.regulation_min","level.regulation_max"]
                    }
                }
            ],
            "market": [ 
                {  "id": 1,
                   "attribute_ids":["sys_price"],
                   "area":[{"id":3,"attribute_ids":["price","load"]}]
                }
            ],
            "unit_group": [
                {
                    "id":3,
                    "attribute_ids":["production","flow","group_type"],
                    "member":[
                        {
                            "id": 5,
                            "attribute_ids": ["is_active","unit_reference"]
                        }
                    ]
                }
            ]
        }
        )_",
    grammar,
    res);
  CHECK_EQ(true, ok); // Checks that we have a full match
  // CHECK KEYWORD:
  CHECK_EQ(res.keyword, "read_attributes");
  // CHECK 1st order KEYS:
  CHECK_EQ(true, res.request_data.m.find("request_id") != res.request_data.m.end());
  CHECK_EQ(true, res.request_data.m.find("model_id") != res.request_data.m.end());
  CHECK_EQ(true, res.request_data.m.find("hps") != res.request_data.m.end());
  CHECK_EQ(true, res.request_data.m.find("market") != res.request_data.m.end());
  CHECK_EQ(true, res.request_data.m.find("unit_group") != res.request_data.m.end());
  // CHECK nested members and sizes
  auto hps = res.request_data.required<vector<json>>("hps");
  auto market = res.request_data.required<vector<json>>("market");
  auto unit_group = res.request_data.required<vector<json>>("unit_group");
  CHECK_EQ(hps.size(), 2);
  CHECK_EQ(market.size(), 1);
  CHECK_EQ(unit_group.size(), 1);
  /* checkout that we got the hps part */ {
    auto h3 = hps[0];
    CHECK_EQ(h3.required<int>("id"), 3); //.which()==12);
    auto const h3_r = h3.required<json>("reservoirs");
    CHECK_EQ(2, h3_r.m.size());
    CHECK_EQ(vector<int>{2, 9, 17}, h3_r.required<vector<int>>("component_ids"));
    CHECK_EQ(
      vector<std::string>{"level.regulation_min", "level.regulation_max"},
      h3_r.required<vector<std::string>>("attribute_ids"));
  }
  /* checkout that we got the market part */ {
    CHECK_EQ(1, market[0].required<int>("id"));
    CHECK_EQ(vector<std::string>{"sys_price"}, market[0].required<vector<std::string>>("attribute_ids"));
    auto const &m_area = market[0].required<vector<json>>("area");
    CHECK_EQ(1, m_area.size());
    CHECK_EQ(3, m_area[0].required<int>("id"));
    CHECK_EQ(vector<std::string>{"price", "load"}, m_area[0].required<vector<std::string>>("attribute_ids"));
  }
  /* finally the unit_group */ {
    CHECK_EQ(3, unit_group[0].required<int>("id"));
    CHECK_EQ(
      vector<std::string>{"production", "flow", "group_type"},
      unit_group[0].required<vector<std::string>>("attribute_ids"));
    auto const &m = unit_group[0].required<vector<json>>("member");
    REQUIRE_EQ(1, m.size());
    CHECK_EQ(5, m[0].required<int>("id"));
    CHECK_EQ(vector<std::string>{"is_active", "unit_reference"}, m[0].required<vector<std::string>>("attribute_ids"));
  }
}

TEST_SUITE_END();
