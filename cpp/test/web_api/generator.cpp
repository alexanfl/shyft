#include "test_pch.h"
#include <shyft/web_api/web_api_generator.h>
#include <shyft/time/utctime_utilities.h>
#include "test_parser.h"

// play-ground for stuff that will be promoted once it's ok'

using std::vector;
using std::string;
using boost::spirit::karma::generate;

using namespace shyft::core;

using shyft::time_series::ts_point_fx;
using shyft::time_series::point;

using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::gta_t;

using shyft::time_axis::fixed_dt;
using shyft::time_axis::calendar_dt;
using shyft::time_axis::point_dt;
using shyft::time_axis::generic_dt;

namespace shyft::web_api::generator {
}

using namespace shyft::web_api::generator;
using namespace shyft::web_api;
using shyft::dtss::ts_info;

TEST_SUITE_BEGIN("web_api");

TEST_CASE("web_api/utctime_generator") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  auto ok = generate(sink, utctime_generator<decltype(sink)>(), seconds(123456789012));
  CHECK_EQ(true, ok);
  *sink++ = ',';
  auto ok2 = generate(sink, utctime_generator<decltype(sink)>(), shyft::core::no_utctime);
  CHECK_EQ(true, ok2);
  CHECK_EQ("123456789012.0,null", ps);
}

TEST_CASE("web_api/utcperiod_generator") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  auto ok = generate(sink, utcperiod_generator<decltype(sink)>(), utcperiod(seconds(1), seconds(2)));
  CHECK_EQ(true, ok);
  *sink++ = ',';
  auto ok2 = generate(sink, utcperiod_generator<decltype(sink)>(), utcperiod());
  CHECK_EQ(true, ok2);
  CHECK_EQ("[1.0,2.0],null", ps);
}

TEST_CASE("web_api/fixed_dt_generator") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  fixed_dt_generator<decltype(sink)> pg;
  fixed_dt ta(seconds(0), seconds(1), 2);
  auto ok = generate(sink, pg, ta);
  CHECK_EQ(true, ok);
  CHECK_EQ(R"_({"t0":0.0,"dt":1.0,"n":2})_", ps);
}

TEST_CASE("web_api/calendar_dt") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  calendar_dt_generator<decltype(sink)> pg;
  auto cal = std::make_shared<calendar>();
  calendar_dt ta(cal, seconds(0), seconds(1), 2);
  auto ok = generate(sink, pg, ta);
  CHECK_EQ(true, ok);
  CHECK_EQ(R"_({"calendar":"UTC","t0":0.0,"dt":1.0,"n":2})_", ps);
}

TEST_CASE("web_api/point_dt") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  point_dt_generator<decltype(sink)> pg;
  point_dt ta({seconds(0), seconds(1), seconds(2)}, seconds(5));
  auto ok = generate(sink, pg, ta);
  CHECK_EQ(true, ok);
  CHECK_EQ(R"_({"time_points":[0.0,1.0,2.0,5.0]})_", ps);
}

TEST_CASE("web_api/generic_dt") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  generic_dt_generator<decltype(sink)> pg;
  // FIXED:
  generic_dt ta(seconds(0), seconds(1), 2);
  bool ok = generate(sink, pg, ta);
  CHECK_EQ(true, ok);
  CHECK_EQ(R"_({"t0":0.0,"dt":1.0,"n":2})_", ps);
  // CALENDAR:
  ta = generic_dt(
    std::make_shared<calendar>(),
    seconds(0),
    seconds(100000),
    2); //  need some delta t, to avoid simplify activate on creation
  ps = "";
  ok = generate(sink, pg, ta);
  CHECK_EQ(true, ok);
  CHECK_EQ(R"_({"calendar":"UTC","t0":0.0,"dt":100000.0,"n":2})_", ps);
  // POINT:
  ta = generic_dt(vector<utctime>{seconds(0), seconds(1), seconds(2)}, seconds(5));
  ps = "";
  ok = generate(sink, pg, ta);
  CHECK_EQ(true, ok);
  CHECK_EQ(R"_({"time_points":[0.0,1.0,2.0,5.0]})_", ps);
}

TEST_CASE("web_api/ts_info_generator") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  ts_info a{
    string("a"),
    shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,
    seconds(900),
    std::string("Europe/Oslo"),
    utcperiod(seconds(1), seconds(2)),
    seconds(3),
    seconds(4)};
  auto ok = generate(sink, ts_info_generator<decltype(sink)>(), a);
  CHECK_EQ(true, ok);
  *sink++ = ',';
  auto ok2 = generate(sink, ts_info_generator<decltype(sink)>(), ts_info{});
  CHECK_EQ(true, ok2);
  CHECK_EQ(
    R"#({"name":"a","pfx":true,"delta_t":900.0,"olson_tz_id":"Europe/Oslo","data_period":[1.0,2.0],"created":3.0,"modified":4.0},{"name":"","pfx":true,"delta_t":0.0,"olson_tz_id":"","data_period":null,"created":null,"modified":null})#",
    ps);
}

TEST_CASE("web_api/ts_info_vector_generator") {
  std::string ps;
  auto sink = std::back_inserter(ps);
  std::vector<ts_info> a{
    ts_info{
            string("a"),
            shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,
            seconds(1),
            std::string("Europe/Oslo"),
            utcperiod(seconds(1), seconds(2)),
            seconds(3),
            seconds(4)},
    ts_info{}
  };
  auto ok = generate(sink, ts_info_vector_generator<decltype(sink)>(), a);
  CHECK_EQ(true, ok);
  CHECK_EQ(
    R"#([{"name":"a","pfx":true,"delta_t":1.0,"olson_tz_id":"Europe/Oslo","data_period":[1.0,2.0],"created":3.0,"modified":4.0},{"name":"","pfx":true,"delta_t":0.0,"olson_tz_id":"","data_period":null,"created":null,"modified":null}])#",
    ps);
  ps.clear();
  a.clear();
  auto ok2 = generate(sink, ts_info_vector_generator<decltype(sink)>(), a);
  REQUIRE_EQ(true, ok2);
  CHECK_EQ("[]", ps);
}

TEST_CASE("web_api/point_generator") {

  point p1{seconds(123456789012), 2.0};
  std::string ps;
  auto sink = std::back_inserter(ps);

  auto ok = generate(sink, point_generator<decltype(sink)>(), p1);
  CHECK_EQ(true, ok);
  point p2{seconds(1), shyft::nan};
  auto ok2 = generate(sink, point_generator<decltype(sink)>(), p2);
  CHECK_EQ(true, ok2);
  CHECK_EQ("[123456789012.0,2.0][1.0,null]", ps);
  // ref issue 774.
  // ps.clear();
  // point px{seconds(1633341600),468.99950239};

  // generate(sink,point_generator<decltype(sink)>(),px);
  // CHECK_EQ("[1633341600.0,468.999502389999975]",ps);
}

TEST_CASE("web_api/apoint_ts_generator") {
  size_t n = 10;
  gta_t ta(from_seconds(0), from_seconds(1), n);
  vector<double> v(n, 1.2);
  v[1] = shyft::nan;
  apoint_ts a(ta, v, ts_point_fx::POINT_AVERAGE_VALUE);

  std::string ps;
  auto sink = std::back_inserter(ps);
  auto ok = generate(sink, apoint_ts_generator<decltype(sink)>(), a);
  CHECK_EQ(true, ok);
  CHECK_EQ(
    "{\"pfx\":true,\"data\":[[0.0,1.2],[1.0,null],[2.0,1.2],[3.0,1.2],[4.0,1.2],[5.0,1.2],[6.0,1.2],[7.0,1.2],[8.0,1.2]"
    ",[9.0,1.2]]}",
    ps);
  std::string nps;
  auto nsink = std::back_inserter(nps);
  apoint_ts nts(gta_t(utctime(0), from_seconds(1), 0), 0.0);
  auto nok = generate(nsink, apoint_ts_generator<decltype(nsink)>(), nts);
  CHECK_EQ(true, nok);
  CHECK_EQ(nps, string("{\"pfx\":false,\"data\":[]}"));
}

TEST_CASE("web_api/apoint_ts_generator.lin") {
  size_t n = 10;
  gta_t ta(from_seconds(0), from_seconds(1), n);
  vector<double> v(n, 1.2);
  v[1] = shyft::nan;
  apoint_ts a(ta, v, ts_point_fx::POINT_INSTANT_VALUE);

  std::string ps;
  auto sink = std::back_inserter(ps);
  auto ok = generate(sink, apoint_ts_generator<decltype(sink)>(), a);
  CHECK_EQ(true, ok);
  CHECK_EQ(
    "{\"pfx\":false,\"data\":[[0.0,1.2],[1.0,null],[2.0,1.2],[3.0,1.2],[4.0,1.2],[5.0,1.2],[6.0,1.2],[7.0,1.2],[8.0,1."
    "2],[9.0,1.2]]}",
    ps);
  std::string nps;
  auto nsink = std::back_inserter(nps);
  apoint_ts nts(gta_t(utctime(0), from_seconds(1), 0), 0.0);
  auto nok = generate(nsink, apoint_ts_generator<decltype(nsink)>(), nts);
  CHECK_EQ(true, nok);
  CHECK_EQ(nps, string("{\"pfx\":false,\"data\":[]}"));
}

TEST_CASE("web_api/atsv_generator") {
  size_t n = 5;
  gta_t taf(from_seconds(0), from_seconds(1), n);
  vector<utctime> tp;
  for (size_t i = 0; i < taf.size(); ++i)
    tp.push_back(taf.time(i));
  gta_t tax(tp, taf.total_period().end);
  vector<double> v(n, 1.2);
  v[1] = shyft::nan;
  apoint_ts a(taf, v, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(tax, v, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts c;
  vector<apoint_ts> tsv{a, b}; // TODO: handle empty ts,c};

  std::string ps;
  auto sink = std::back_inserter(ps);
  auto ok = generate(sink, atsv_generator<decltype(sink)>(), tsv);
  CHECK_EQ(true, ok);
  std::string nps;
  auto nsink = std::back_inserter(nps);
  vector<apoint_ts> nts;
  auto nok = generate(nsink, atsv_generator<decltype(nsink)>(), nts);
  CHECK_EQ(true, nok);
  CHECK_EQ(nps, string("[]"));
  CHECK_EQ(
    ps,
    string("[{\"pfx\":true,\"data\":[[0.0,1.2],[1.0,null],[2.0,1.2],[3.0,1.2],[4.0,1.2]]},{\"pfx\":true,\"data\":[[0.0,"
           "1.2],[1.0,null],[2.0,1.2],[3.0,1.2],[4.0,1.2]]}]"));
}

TEST_CASE("web_api/atsv_generator_speed") {
  size_t n = 365 * 24;
  gta_t taf(from_seconds(0), from_seconds(1), n);
  vector<double> v(n, 1.234);
  apoint_ts a(taf, v, ts_point_fx::POINT_AVERAGE_VALUE);
  size_t n_ts = 50;
  vector<apoint_ts> tsv(n_ts, a);
  std::string ps;
  auto sink = std::back_inserter(ps);
  auto t0 = utctime_now();
  auto ok = generate(sink, atsv_generator<decltype(sink)>(), tsv);
  auto dt = utctime_now() - t0;
  CHECK_EQ(true, ok);
  WARN_LE(to_seconds(dt), 1.0);
}

TEST_CASE("web_api/average_ts_request_generator") {
  average_ts_request_generator<generator_output_iterator> g_;
  average_ts_request a{
    string{"abc"},
    {from_seconds(1), from_seconds(2)},
    {from_seconds(0), from_seconds(10), 10},
    true,
    {{"a"}, {"b"}},
    true,
    false
  };
  string ps;
  auto sink = std::back_inserter(ps);
  generate(sink, g_, a);
  CHECK_EQ(
    ps,
    R"_(average {"request_id":"abc","read_period":[1.0,2.0],"time_axis":{"t0":0.0,"dt":10.0,"n":10},"cache":true,"ts_ids":["a","b"],"subscribe":true,"ts_fmt":false})_");
}

TEST_CASE("web_api/percentile_ts_request_generator") {
  percentile_ts_request_generator<generator_output_iterator> g_;
  percentile_ts_request a{
    string{"abc"},
    {from_seconds(1), from_seconds(2)},
    {from_seconds(0), from_seconds(10), 10},
    {1, 2, 4},
    true,
    {{"a"}, {"b"}},
    true,
    false
  };
  string ps;
  auto sink = std::back_inserter(ps);
  generate(sink, g_, a);
  CHECK_EQ(
    ps,
    R"_(percentile {"request_id":"abc","read_period":[1.0,2.0],"time_axis":{"t0":0.0,"dt":10.0,"n":10},"percentiles":[1,2,4],"cache":true,"ts_ids":["a","b"],"subscribe":true,"ts_fmt":false})_");
}

TEST_CASE("web_api/read_ts_request_generator") {
  read_ts_request_generator<generator_output_iterator> g_;
  read_ts_request a{
    string{"abc"},
    {from_seconds(1), from_seconds(5)},
    {from_seconds(2), from_seconds(4)},
    true,
    {{"a"}, {"b"}},
    true,
    false
  };
  string ps;
  auto sink = std::back_inserter(ps);
  generate(sink, g_, a);
  CHECK_EQ(
    ps,
    R"_(read {"request_id":"abc","read_period":[1.0,5.0],"clip_period":[2.0,4.0],"cache":true,"ts_ids":["a","b"],"subscribe":true,"ts_fmt":false})_");
}

TEST_SUITE_END();
