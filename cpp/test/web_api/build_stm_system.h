#pragma once
#include <shyft/time/utctime_utilities.h>

// fwds.
namespace shyft::energy_market::stm {
  struct stm_hps;
  struct stm_system;
  using stm_hps_ = std::shared_ptr<stm_hps>;
  using stm_system_ = std::shared_ptr<stm_system>;
}

namespace test::web_api {
  using namespace shyft::energy_market::stm;
  using namespace shyft::core;
  using std::string;

  inline utctime _t(int64_t t1970s) {
    return utctime{seconds(t1970s)};
  }

  stm_hps_ create_stm_hps(int id = 1, string name = "sørland");

  template <class ProxyAttr>
  inline string ts_url(string const & mid, ProxyAttr const & pa) {
    return pa.url("dstm://M" + mid + "/");
  }

  stm_hps_ create_simple_hps(int id = 1, string name = "simple");

  stm_system_ create_stm_system(int id = 1, string name = "stm_system", string json = "");

  stm_system_ create_simple_system(int id = 1, string name = "simple_system");
}
