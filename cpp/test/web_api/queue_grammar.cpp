#include "test_pch.h"
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/web_api_generator.h>
#include <shyft/dtss/queue_msg.h>

#include "test_parser.h"

using namespace shyft::core;
using shyft::time_series::dd::apoint_ts;


using std::vector;
using std::string;
using shyft::core::utctime;
using namespace shyft::web_api;
using namespace shyft;
using namespace shyft::dtss;

namespace qi = boost::spirit::qi;
namespace ka = boost::spirit::karma;
namespace phx = boost::phoenix;

namespace {

  // engine to test round trip:
  // object a -> generate -> string -> parse -> b, and then check a==b
  template <
    typename Generator, // generator is a template type, that takes iterator as arg.
    typename Parser,    // parser, as well a template type ..
    class T >
  void gp_round_trip(T const & a) {
    string ps;
    auto sink = std::back_inserter(ps);
    Generator g_;
    REQUIRE(generate(sink, g_, a)); // verify generation works
    Parser p_;
    T b; // !must be default constructible
    auto ok = test::phrase_parser(ps, p_, b);
    if (!ok) {
      MESSAGE("failed to parse:" << ps);
      REQUIRE(ok); // verify parser is ok
    }
    CHECK(a == b); // and they should compare equal
  }

  static ats_vector mk_tst_tsv() {
    apoint_ts ts1{
      "shyft://p1.g1",
      apoint_ts{
                time_axis::generic_dt{from_seconds(10), from_seconds(1), 3},
                vector<double>{1.0, 2.0, 3.0},
                time_series::POINT_AVERAGE_VALUE}
    };
    ats_vector tsv;
    tsv.push_back(ts1);
    return tsv;
  }

}

using sink_ = std::back_insert_iterator<std::string>;
using src_ = char const *;

TEST_SUITE_BEGIN("web_api");

TEST_CASE("web_api/q_put_grammar") {
  q_put_request a{"req1", "q1", "msg1", "descript1", from_seconds(3), mk_tst_tsv()};
  gp_round_trip<generator::q_put_request_generator<sink_>, grammar::q_put_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_msg_info_grammar") {
  queue::msg_info a{"m1", "d1", from_seconds(3), from_seconds(4), from_seconds(5), no_utctime, "!"};
  gp_round_trip<generator::q_msg_info_generator<sink_>, grammar::q_msg_info_grammar<src_>>(a);
}

TEST_CASE("web_api/q_tsv_msg_grammar") {
  queue::tsv_msg a{
    queue::msg_info{"m1", "d1", from_seconds(3), from_seconds(4), from_seconds(5), no_utctime, "!"},
    mk_tst_tsv()
  };
  gp_round_trip<generator::q_tsv_msg_generator<sink_>, grammar::q_tsv_msg_grammar<src_>>(a);
}

TEST_CASE("web_api/q_get_request_grammar") {
  q_get_request a{"r1", "q1"};
  gp_round_trip<generator::q_get_request_generator<sink_>, grammar::q_get_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_get_response_grammar_null") {
  q_get_response a{"r1", nullptr}; // verify we can
  gp_round_trip<generator::q_get_response_generator<sink_>, grammar::q_get_response_grammar<src_>>(a);
}

TEST_CASE("web_api/q_done_request_grammar") {
  q_done_request a{"r1", "q1", "m1", "diag1"};
  gp_round_trip<generator::q_done_request_generator<sink_>, grammar::q_done_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_get_response_grammar") {
  q_get_response a{
    "r1",
    std::make_shared<queue::tsv_msg>(queue::tsv_msg{
                                                    queue::msg_info{"m1", "d1", from_seconds(3), from_seconds(4), from_seconds(5), no_utctime, "!"},
                                                    mk_tst_tsv()}
    )
  }; // verify we can
  gp_round_trip<generator::q_get_response_generator<sink_>, grammar::q_get_response_grammar<src_>>(a);
}

TEST_CASE("web_api/q_list_request_grammar") {
  q_list_request a{"q1"};
  gp_round_trip<generator::q_list_request_generator<sink_>, grammar::q_list_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_list_response_grammar") {
  q_list_response a{
    "q1", vector<string>{"1", "2"}
  };
  gp_round_trip<generator::q_list_response_generator<sink_>, grammar::q_list_response_grammar<src_>>(a);
}

TEST_CASE("web_api/q_info_request_grammar") {
  q_info_request a{"r1", "q1", "m1"};
  gp_round_trip<generator::q_info_request_generator<sink_>, grammar::q_info_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_info_response_grammar") {
  q_info_response a{
    "q1", queue::msg_info{"m1", "d1", from_seconds(3), from_seconds(4), from_seconds(5), no_utctime, "!"}
  };
  gp_round_trip<generator::q_info_response_generator<sink_>, grammar::q_info_response_grammar<src_>>(a);
}

TEST_CASE("web_api/q_infos_request_grammar") {
  q_infos_request a{"r1", "q1"};
  gp_round_trip<generator::q_infos_request_generator<sink_>, grammar::q_infos_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_infos_response_grammar") {
  q_infos_response a{
    "q1",
    vector<queue::msg_info>{
                            queue::msg_info{"m1", "d1", from_seconds(3), from_seconds(4), from_seconds(5), no_utctime, "!"},
                            queue::msg_info{"m2", "d1", from_seconds(3), from_seconds(4), from_seconds(5), no_utctime, "!"}}
  };
  gp_round_trip<generator::q_infos_response_generator<sink_>, grammar::q_infos_response_grammar<src_>>(a);
}

TEST_CASE("web_api/q_size_request_grammar") {
  q_size_request a{"r1", "q1"};
  gp_round_trip<generator::q_size_request_generator<sink_>, grammar::q_size_request_grammar<src_>>(a);
}

TEST_CASE("web_api/q_size_response_grammar") {
  q_size_response a{"q1", 34};
  gp_round_trip<generator::q_size_response_generator<sink_>, grammar::q_size_response_grammar<src_>>(a);
}

TEST_CASE("web_api/q_maintain_request_grammar") {
  q_maintain_request a{"r1", "q1", true};
  gp_round_trip<generator::q_maintain_request_generator<sink_>, grammar::q_maintain_request_grammar<src_>>(a);
}

TEST_SUITE_END();
