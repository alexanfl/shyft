#include <doctest/doctest.h>

#include <shyft/py/api/doc_builder.h>

//
// old definitions to compare with
//
#define doc_intro(intro) intro "\n"
#define doc_details(dt) "\n" dt "\n"
#define doc_attributes() "\nAttributes:"
#define doc_attribute(name_str, type_str, descr_str) "\n    " name_str " (" type_str "): " descr_str "\n"
#define doc_parameters() "\nArgs:"
#define doc_parameter(name_str, type_str, descr_str) "\n    " name_str " (" type_str "): " descr_str "\n"
#define doc_paramcont(doc_str) "    " doc_str "\n"
#define doc_returns(name_str, type_str, descr_str) "\nReturns:\n    " type_str ": " name_str ". " descr_str "\n"
#define doc_notes() "\nNotes:"
#define doc_note(note_str) "\n    " note_str "\n"
#define doc_see_also(ref) "\nSee also:\n    " ref "\n"
#define doc_reference(ref_name, ref) "\n.. _" ref_name ":\n    " ref "\n"
#define doc_ind(doc_str) "    " doc_str
#define doc_raises() "\nRaises:"
#define doc_raise(type_str, descr_str) "\n    " type_str ": " descr_str
#define doc_retcont(descr_str) descr_str "\n"
// refs to py syms
#define doc_ref_meth(m) " :meth:`" m "` "
#define doc_ref_func(m) " :func:`" m "` "
#define doc_ref_class(m) " :class:`" m "` "
#define doc_ref_mod(m) " :mod:`" m "` "
#define doc_ref_attr(m) " :attr:`" m "` "


TEST_SUITE_BEGIN("doc_builder");

TEST_CASE("doc_builder/intro") {
  // Test that the intro function does the same as the macro:
  CHECK_EQ(doc.intro("my_intro").str(), doc_intro("my_intro"));
}

TEST_CASE("doc_builder/details") {
  // Test that the details function does the same as the macro:
  CHECK_EQ(doc.details("my_details").str(), doc_details("my_details"));
}

TEST_CASE("doc_builder/attributes") {
  // Test that the attributes function does the same as the macro:
  CHECK_EQ(doc.attributes().str(), doc_attributes());
}

TEST_CASE("doc_builder/attribute") {
  // Test that the attribute function does the same as the macro:
  CHECK_EQ(doc.attribute("my_name", "my_type", "my_descr").str(), doc_attribute("my_name", "my_type", "my_descr"));
}

TEST_CASE("doc_builder/parameters") {
  // Test that the parameters function does the same as the macro:
  CHECK_EQ(doc.parameters().str(), doc_parameters());
}

TEST_CASE("doc_builder/parameter") {
  // Test that the parameter function does the same as the macro:
  CHECK_EQ(doc.parameter("my_name", "my_type", "my_descr").str(), doc_parameter("my_name", "my_type", "my_descr"));
}

TEST_CASE("doc_builder/paramcont") {
  // Test that the paramcont function does the same as the macro:
  CHECK_EQ(doc.paramcont("my_doc").str(), doc_paramcont("my_doc"));
}

TEST_CASE("doc_builder/returns") {
  // Test that the returns function does the same as the macros:
  CHECK_EQ(doc.returns("my_name", "my_type", "my_descr").str(), doc_returns("my_name", "my_type", "my_descr"));
}

TEST_CASE("doc_builder/notes") {
  // Test that the notes function does the same as the macros:
  CHECK_EQ(doc.notes().str(), doc_notes());
}

TEST_CASE("doc_builder/see_also") {
  // Test that the see_also function does the same as the macros:
  CHECK_EQ(doc.see_also("my_ref").str(), doc_see_also("my_ref"));
}

TEST_CASE("doc_builder/reference") {
  // Test that the reference function does the same as the macros:
  CHECK_EQ(doc.reference("my_ref_name", "my_ref").str(), doc_reference("my_ref_name", "my_ref"));
}

TEST_CASE("doc_builder/ind") {
  // Test that the ind function does the same as the macros:
  CHECK_EQ(doc.ind("my_doc").str(), doc_ind("my_doc"));
}

TEST_CASE("doc_builder/raises") {
  // Test that the raises function does the same as the macros:
  CHECK_EQ(doc.raises().str(), doc_raises());
}

TEST_CASE("doc_builder/raise") {
  // Test that the raise function does the same as the macros:
  CHECK_EQ(doc.raise("my_type", "my_descr").str(), doc_raise("my_type", "my_descr"));
}

TEST_CASE("doc_builder/retcont") {
  // Test that the retcont function does the same as the macros:
  CHECK_EQ(doc.retcont("my_descr").str(), doc_retcont("my_descr"));
}

TEST_CASE("doc_builder/ref_meth") {
  // Test that the ref_meth function does the same as the macros:
  CHECK_EQ(doc.ref_meth("my_m").str(), doc_ref_meth("my_m"));
}

TEST_CASE("doc_builder/ref_func") {
  // Test that the ref_func function does the same as the macros:
  CHECK_EQ(doc.ref_func("my_m").str(), doc_ref_func("my_m"));
}

TEST_CASE("doc_builder/ref_class") {
  // Test that the ref_class function does the same as the macros:
  CHECK_EQ(doc.ref_class("my_m").str(), doc_ref_class("my_m"));
}

TEST_CASE("doc_builder/ref_mod") {
  // Test that the ref_mod function does the same as the macros:
  CHECK_EQ(doc.ref_mod("my_m").str(), doc_ref_mod("my_m"));
}

TEST_CASE("doc_builder/ref_attr") {
  // Test that the ref_attr function does the same as the macros:
  CHECK_EQ(doc.ref_attr("my_m").str(), doc_ref_attr("my_m"));
}

TEST_CASE("doc_builder/compose") {
  // Test that compositions give the correct results
  CHECK_EQ(
    doc.intro("my_intro").details("my_details").attributes().attribute("some_name", "some_type", "some_descr").str(),
    doc_intro("my_intro") doc_details("my_details") doc_attributes()
      doc_attribute("some_name", "some_type", "some_descr"));
  CHECK_EQ(
    doc.parameters()
      .parameter("some_name", "some_type", "some_descr")
      .paramcont("some_doc")
      .returns("some_name", "some_type", "some_descr")
      .str(),
    doc_parameters() doc_parameter("some_name", "some_type", "some_descr") doc_paramcont("some_doc")
      doc_returns("some_name", "some_type", "some_descr"));
  CHECK_EQ(
    doc.notes().note("some_note").see_also("some_ref").reference("some_refname", "some_ref").str(),
    doc_notes() doc_note("some_note") doc_see_also("some_ref") doc_reference("some_refname", "some_ref"));
  CHECK_EQ(
    doc.ind("my_doc").raises().raise("some_type", "some_descr").retcont("some_descr").str(),
    doc_ind("my_doc") doc_raises() doc_raise("some_type", "some_descr") doc_retcont("some_descr"));
  CHECK_EQ(
    doc.ref_meth("my_m").ref_func("my_m").ref_class("my_m").ref_mod("my_m").ref_attr("my_m").str(),
    doc_ref_meth("my_m") doc_ref_func("my_m") doc_ref_class("my_m") doc_ref_mod("my_m") doc_ref_attr("my_m"));
}

TEST_SUITE_END();