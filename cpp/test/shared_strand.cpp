#include <atomic>
#include <cstdint>
#include <chrono>
#include <ranges>
#include <thread>

#include <boost/asio/post.hpp>
#include <boost/asio/thread_pool.hpp>
#include <boost/asio/use_future.hpp>
#include <doctest/doctest.h>

#include <shyft/core/shared_strand.h>

namespace shyft {

  TEST_SUITE_BEGIN("shared_strand");

  TEST_CASE("shared_strand") {

    boost::asio::thread_pool threads{8};
    auto strand = make_shared_strand(threads.executor());

    SUBCASE("increments") {

      std::size_t n_increments = 4;
      std::atomic<std::size_t> counter = 0;
      std::atomic<bool> sleeping = false;
      bool ok = false;

      auto sleep = [&] {
        sleeping = true;
        sleeping.notify_all();
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
      };
      auto post_incrementers = [&] {
        for ([[maybe_unused]] auto i : std::views::iota(std::size_t{0}, n_increments))
          boost::asio::post(strand, [&] {
            sleeping.wait(false);
            ++counter;
          });
      };

      std::future<void> task{};
      SUBCASE("shared") {
        task = boost::asio::post(strand, boost::asio::use_future([&] {
                                   sleep();
                                   std::size_t count = counter;
                                   ok = (count == n_increments);
                                 }));
      }
      SUBCASE("unique") {
        task = boost::asio::post(strand.unique(), boost::asio::use_future([&] {
                                   sleep();
                                   std::size_t count = counter;
                                   ok = (count == 0);
                                 }));
      }
      post_incrementers();
      task.get();
      CHECK_MESSAGE(ok, counter.load());
    }
  }

  TEST_SUITE_END();

}
