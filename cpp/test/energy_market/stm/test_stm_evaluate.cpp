#include <string>
#include <memory>

#include <doctest/doctest.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>


#include "build_test_system.h"

namespace shyft::energy_market::stm {

  using shyft::core::from_seconds;
  using shyft::core::deltahours;

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/evaluate_ts") {
    auto mdl = test::create_simple_system(1, "0");
    auto mkt = mdl->market[0];

    // In ids:
    auto price_url = fmt::format("dstm://M{}/m{}.price", mdl->name, mkt->id);
    auto max_buy_url = fmt::format("dstm://M{}/m{}.max_buy", mdl->name, mkt->id);
    auto custom_url = fmt::format("dstm://M{}/m{}.ts.custom", mdl->name, mkt->id);
    auto dtss_custom_url = "shyft://test/custom";
    mkt->price = time_series::dd::apoint_ts(dtss_custom_url);
    mkt->tsm["custom"] = time_series::dd::apoint_ts(dtss_custom_url);
    mkt->max_buy = 2.0 + time_series::dd::apoint_ts(price_url);
    boost::asio::thread_pool execution_context;

    auto models = make_shared_models(
      execution_context.executor(),
      {
        {"0", mdl}
    });

    auto get_err = []<typename T>(T const &d) -> std::optional<std::string> {
      if constexpr (time_series::dd::is_apoint_ts_v<T>) {
        return std::nullopt;
      } else {
        static_assert(is_evaluate_ts_error_v<T>);
        return d.what;
      }
    };

    std::string throwing_url = "shyft://test/throws";
    std::string non_returning_url = "shyft://does/not/return";
    auto dtss = [&](auto const &urls, utcperiod read_period, auto &&...) {
      time_series::dd::ats_vector ts_read;
      for (std::string_view url : urls) {
        if (url.starts_with(std::string_view(throwing_url))) {
          throw std::runtime_error("ErrorMessage");
        } else if (url.starts_with(std::string_view(non_returning_url))) {
          continue;
        }
        time_axis::point_dt ta({read_period.start}, read_period.end);
        ts_read.push_back(time_series::dd::apoint_ts(time_axis::generic_dt{ta}, 1.0));
      }
      return ts_read;
    };

    utcperiod bind_period{utctime(0), utctime(7)};

    SUBCASE("cycle_in_expression") {
      auto sale_url = fmt::format("dstm://M{}/m{}.sale", mdl->name, mkt->id);
      auto buy_url = fmt::format("dstm://M{}/m{}.buy", mdl->name, mkt->id);
      mkt->sale = time_series::dd::apoint_ts(buy_url);
      mkt->buy = time_series::dd::apoint_ts(sale_url);
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(buy_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(
        *err == "ts ((dstm://M0/m2.buy + dstm://M0/m2.max_buy) + shyft://test/custom) contains a cyclic expression");
    }

    SUBCASE("url_invalid_in_expression") {
      auto invalid_model_url = fmt::format("dstm://M{}i/m{}.sale", mdl->name, mkt->id);
      auto invalid_attr_url = fmt::format("dstm://M{}/m{}.salei", mdl->name, mkt->id);
      auto invalid_path_url = fmt::format("dstm://M{}/m{}i.sale", mdl->name, mkt->id);
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(invalid_model_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(*err == "dstm timeseries ((dstm://M0i/m2.sale + dstm://M0/m2.max_buy) + shyft://test/custom) did not resolve with error: model with id 0i not found, requested url: dstm://M0i/m2.sale");
      tsv.clear();
      tsv.push_back(
        time_series::dd::apoint_ts(invalid_attr_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url));
      tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(*err == "dstm timeseries ((dstm://M0/m2.salei + dstm://M0/m2.max_buy) + shyft://test/custom) did not resolve with error: attribute salei not found, requested url: dstm://M0/m2.salei");
      tsv.clear();
      tsv.push_back(
        time_series::dd::apoint_ts(invalid_path_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url));
      tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(*err == "dstm timeseries ((dstm://M0/m2i.sale + dstm://M0/m2.max_buy) + shyft://test/custom) did not resolve with error: url dstm://M0/m2i.sale is not parseable: i.sale does not start with an attribute or component delimiter");
    }

    SUBCASE("dtss_throws") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(throwing_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(*err == "dtss get timeseries failed with error: ErrorMessage");
    }

    SUBCASE("dtss_returns_empty") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(non_returning_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(*err == "dtss get timeseries failed with error: dtss did not return same size of ts as requested");
    }

    SUBCASE("multiple_throw_in_dtss") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(throwing_url) + time_series::dd::apoint_ts(throwing_url + "2")
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value() == true);
      CHECK(*err == "dtss get timeseries failed with error: ErrorMessage");
    }

    // FIXME: reenable when dtss returns errors
    /*
    SUBCASE("one_has_err_one_doesn_t") {

      time_series::dd::ats_vector tsv{
        (time_series::dd::apoint_ts(non_returning_url) + time_series::dd::apoint_ts(max_buy_url)
         + time_series::dd::apoint_ts(dtss_custom_url)),
        (time_series::dd::apoint_ts(max_buy_url) + time_series::dd::apoint_ts(dtss_custom_url))};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 2);
      auto err = std::visit(get_err, tsv_eval[0]);
      CHECK(err.has_value() == true);
      CHECK(
        *err
        == "dtss timeseries with url shyft://does/not/return did not resolve with error: dtss did not return or throw");
      auto ts = std::get_if<time_series::dd::apoint_ts>(&tsv_eval[1]);
      CHECK(ts->ts != nullptr);
      // check that the evaluation is correct
      std::ranges::for_each(ts->ts->values(), [](auto &v) {
        CHECK(v - 4.0 < 0.001);
      });
    }
     */
  }

  TEST_SUITE_END;
}