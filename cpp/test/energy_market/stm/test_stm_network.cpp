#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

namespace {
  struct test_sys {
    stm_system_ sys;
    network_ net;
    transmission_line_ t;
    busbar_ b;

    test_sys() {
      sys = make_shared<stm_system>(1, "sys", "{}");
      auto sys_builder = stm_builder(sys);
      auto net = sys_builder.create_network(2, "net", "{}");
      auto net_builder = network_builder(net);
      t = net_builder.create_transmission_line(3, "t", "{}");
      b = net_builder.create_busbar(4, "b", "{}");
    }
  };
}

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/network_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto net = make_shared<network>(2, "net", "{}", sys);
  FAST_CHECK_UNARY(net);
  FAST_CHECK_EQ(net->id, 2);
  FAST_CHECK_EQ(net->name, "net");
  FAST_CHECK_EQ(net->json, "{}");
}

TEST_CASE("stm/network_builder") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");
  auto net_builder = network_builder(net);

  auto t = net_builder.create_transmission_line(3, "t", "{}");
  REQUIRE_EQ(net->transmission_lines[0], t);
  REQUIRE_THROWS(net_builder.create_transmission_line(3, "t2", "{}")); // same id
  REQUIRE_THROWS(net_builder.create_transmission_line(4, "t", "{}"));  // same name
  REQUIRE_EQ(net->transmission_lines.size(), 1);

  auto b = net_builder.create_busbar(4, "b", "{}");
  REQUIRE_EQ(net->busbars[0], b);
  REQUIRE_THROWS(net_builder.create_busbar(4, "b2", "{}")); // same id
  REQUIRE_THROWS(net_builder.create_busbar(5, "b", "{}"));  // same name
  REQUIRE_EQ(net->busbars.size(), 1);
}

TEST_CASE("stm/network_equality") {
  test_sys sys1;
  test_sys sys2;
  CHECK_EQ(*sys1.net, *sys1.net); // equal to self
  CHECK_EQ(*sys1.net, *sys2.net);
}

TEST_SUITE_END();
