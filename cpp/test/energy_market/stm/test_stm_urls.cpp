#include <doctest/doctest.h>

#include <memory>
#include <string>
#include <string_view>

#include <shyft/energy_market/stm/urls.h>
#include <shyft/energy_market/stm/url_parse.h>
#include <shyft/energy_market/stm/model.h>

namespace shyft::energy_market::stm {

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/urls") {

    static_assert(url_max_path_length(identity_v<stm_system>) == 3ul);
    auto model = std::make_shared<stm_system>();

    static_assert(url_step_type<&stm_system::hps>() != url_step_type_null);

    CHECK(url_peek_model_id("a").empty());
    CHECK(url_peek_model_id("aabbccddeeff").empty());
    CHECK(url_peek_model_id("xgbl://Mm4/G0.fdd").empty());
    CHECK(url_peek_model_id("dstm://Mm4") == std::string_view{"m4"});
    CHECK(url_peek_model_id(url_format("m4", {}, "xx")) == std::string_view{"m4"});

    SUBCASE("hps") {

      static_assert(url_max_path_length(identity_v<reservoir>) == 0ul);
      static_assert(url_max_path_length(identity_v<stm_hps>) == 2ul);
      static_assert(url_max_path_length(identity_v<waterway>) == 1ul);

      static_assert(url_step_type<&stm_system::hps>() != url_step_type_null);
      static_assert(url_step_type<&waterway::gates>() != url_step_type_null);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto reservoir = std::make_shared<stm::reservoir>(20, std::string{}, std::string{}, hps);
      auto gate = std::make_shared<stm::gate>(40, std::string{}, std::string{});
      auto waterway = std::make_shared<stm::waterway>(30, std::string{}, std::string{}, hps);
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);
      auto power_plant = std::make_shared<stm::power_plant>(60, std::string{}, std::string{}, hps);
      auto reservoir_aggregate = std::make_shared<stm::reservoir_aggregate>(70, std::string{}, std::string{}, hps);
      auto catchment = std::make_shared<stm::catchment>(80, std::string{}, std::string{}, hps);

      waterway->gates.push_back(gate);

      model->hps.push_back(hps);

      hps->reservoirs.push_back(reservoir);
      hps->waterways.push_back(waterway);
      hps->units.push_back(unit);
      hps->power_plants.push_back(power_plant);
      hps->reservoir_aggregates.push_back(reservoir_aggregate);
      hps->catchments.push_back(catchment);

      auto const H_step = url_make_step<&stm_system::hps>(hps->id);
      auto const H_misstep = url_make_step<&stm_system::hps>(hps->id + 1);
      auto const R_step = url_make_step<&stm_hps::reservoirs>(reservoir->id);
      auto const R_misstep = url_make_step<&stm_hps::reservoirs>(reservoir->id - 1);
      const url_step r_misstep{'r', 0};
      auto const W_step = url_make_step<&stm_hps::waterways>(waterway->id);
      auto const W_misstep = url_make_step<&stm_hps::waterways>(waterway->id - 2);
      auto const G_step = url_make_step<&waterway::gates>(gate->id);
      auto const G_misstep = url_make_step<&waterway::gates>(gate->id - 3);
      auto const G_mislink = url_make_step<&stm_hps::gates>(gate->id - 3);
      auto const U_step = url_make_step<&stm_hps::units>(unit->id);
      auto const U_misstep = url_make_step<&stm_hps::units>(0);
      auto const P_step = url_make_step<&stm_hps::power_plants>(power_plant->id);
      auto const P_misstep = url_make_step<&stm_hps::power_plants>(0);
      auto const C_step = url_make_step<&stm_hps::catchments>(catchment->id);
      auto const C_misstep = url_make_step<&stm_hps::catchments>(catchment->id + 9);
      auto const A_step = url_make_step<&stm_hps::reservoir_aggregates>(reservoir_aggregate->id);
      auto const A_misstep = url_make_step<&stm_hps::reservoir_aggregates>(reservoir_aggregate->id + 1);


      {
        auto was_invalid = url_with_path(
          model,
          {
            {.tp = url_max_path_length(identity_v<stm_system>), .id = hps->id}
        },
          [&](auto const &...) {
            return false;
          },
          [&](auto const &) {
            return true;
          });
        CHECK(was_invalid);
      }

      CHECK(url_check_subcomp<stm_hps>(*model, H_step));
      CHECK(!url_check_subcomp<stm_hps>(*model, H_misstep));
      CHECK(url_check_subcomp<stm::reservoir>(*hps, R_step));
      CHECK(url_check_subcomp<stm::waterway>(*hps, W_step));
      CHECK(url_check_subcomp<const stm::reservoir>((stm_hps const &) *hps, R_step));
      CHECK(!url_check_subcomp<stm::waterway>(*hps, W_misstep));
      CHECK(!url_check_subcomp<stm::unit>(*hps, R_step));
      CHECK(url_check_subcomp<stm::unit>(*hps, U_step));
      CHECK(!url_check_subcomp<stm::unit>(*hps, U_misstep));
      CHECK(!url_check_subcomp<stm::reservoir>(*hps, R_misstep));
      CHECK(!url_check_subcomp<stm::reservoir>(*hps, r_misstep));

      CHECK(url_check_path<stm::reservoir>(*hps, {R_step}));
      CHECK(url_check_path<stm::waterway>(*hps, {W_step}));
      CHECK(url_check_path<stm::gate>(*hps, {W_step, G_step}));
      CHECK(!url_check_path<stm::gate>(*hps, {W_step, G_misstep}));
      CHECK(url_check_path<stm::gate>(*hps, {G_step}));
      CHECK(!url_check_path<stm::gate>(*hps, {G_mislink}));
      CHECK(url_check_path<stm_hps>(*model, {H_step}));
      CHECK(url_check_path<const stm::waterway>((stm_system const &) *model, {H_step, W_step}));
      CHECK(!url_check_path<stm_hps>(*model, {H_step, H_step}));
      CHECK(url_check_path<stm::reservoir>(*model, {H_step, R_step}));
      CHECK(url_check_path<const stm::reservoir>((stm_system const &) *model, {H_step, R_step}));
      CHECK(url_check_path<stm::unit>(*model, {H_step, U_step}));
      CHECK(!url_check_path<stm::unit>(*model, {H_step, U_misstep}));
      CHECK(url_check_path<stm::power_plant>(*model, {H_step, P_step}));
      CHECK(!url_check_path<stm::power_plant>(*model, {H_step, P_misstep}));
      CHECK(url_check_path<stm::catchment>(*model, {H_step, C_step}));
      CHECK(!url_check_path<stm::catchment>(*model, {H_step, C_misstep}));
      CHECK(url_check_path<stm::reservoir_aggregate>(*model, {H_step, A_step}));
      CHECK(!url_check_path<stm::reservoir_aggregate>(*model, {H_step, A_misstep}));

      CHECK(url_check_attr<time_series::dd::apoint_ts>(*reservoir, "level.regulation_min"));
      CHECK(url_check_attr<const time_series::dd::apoint_ts>(
        (stm::reservoir const &) *reservoir, "volume.constraint.tactical.min.penalty"));
      CHECK(url_check_attr<time_series::dd::apoint_ts>(*reservoir, "level.constraint.min"));

      CHECK(url_check_attr<time_series::dd::apoint_ts>(*power_plant, "fees.feeding_fee"));

      reservoir->tsm["constraint.min"] = apoint_ts{};
      reservoir->custom["my.attribute"] = apoint_ts{};
      CHECK(url_check_attr<time_series::dd::apoint_ts>(*reservoir, "ts.constraint.min"));
      CHECK(url_check_attr<any_attr>(*reservoir, "custom.my.attrtibute"));
      CHECK(!url_check_attr<time_series::dd::apoint_ts>(
        *(stm::reservoir const *) reservoir.get(), "ts.constraint.max")); // notice const here
      CHECK(url_check_attr<time_series::dd::apoint_ts>(
        *reservoir, "ts.constraint.max")); // notice non const, will create the entry


      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {H_step, R_step}, "xlevel.schedule"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, R_step}, "volume.slack.lower"));
      CHECK(url_check<const time_series::dd::apoint_ts>(
        (stm_system const &) *model, {H_step, R_step}, "volume.slack.lower"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, U_step}, "discharge.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, G_step}, "discharge.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, W_step, G_step}, "discharge.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, U_step}, "discharge.constraint.min"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, W_step}, "discharge.static_max"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, W_step}, "discharge.result"));
    }

    SUBCASE("unit_group") {

      static_assert(url_max_path_length(identity_v<unit_group>) == 1ul);
      static_assert(url_max_path_length(identity_v<unit_group_member>) == 0ul);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);

      hps->units.push_back(unit);
      model->hps.push_back(hps);

      auto unit_group = std::make_shared<stm::unit_group>(model.get());
      auto unit_group_member = std::make_shared<stm::unit_group_member>(unit_group.get(), unit, apoint_ts{});

      auto const u_step = url_make_step<&stm_system::unit_groups>(unit_group->id);
      auto const u_misstep = url_make_step<&stm_system::unit_groups>(unit_group->id + 1);
      auto const M_step = url_make_step<&unit_group::members>(unit_group_member->id());
      auto const M_misstep = url_make_step<&unit_group::members>(unit_group_member->id() + 1);

      unit_group->members.push_back(unit_group_member);
      model->unit_groups.push_back(unit_group);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {u_step}, "obligation.cost"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {u_step}, "obligation.schedule"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step}, "obligation.scheduxxxle"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_misstep}, "obligation.cost"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {u_step, M_step}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step, M_step}, "actixxxve"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step, M_misstep}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step, M_misstep}, "active"));
    }

    SUBCASE("market_area") {

      static_assert(url_max_path_length(identity_v<energy_market_area>) == 0ul);

      auto market = std::make_shared<energy_market_area>(2, "", "", model);

      model->market.push_back(market);

      auto const m_step = url_make_step<&stm_system::market>(market->id);
      auto const m_misstep = url_make_step<&stm_system::market>(market->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {m_step}, "price"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_step}, "psxxrixce"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_misstep}, "price"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {m_step}, "ts.price-alternative"));
    }
    SUBCASE("contract") {

      static_assert(url_max_path_length(identity_v<contract>) == 0ul);

      auto contract = std::make_shared<stm::contract>(4, "", "", model);

      model->contracts.push_back(contract);

      auto const c_step = url_make_step<&stm_system::contracts>(contract->id);
      auto const c_misstep = url_make_step<&stm_system::contracts>(contract->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {c_step}, "revenue"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {c_step}, "quantity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {c_step}, "rexxevenuex"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {c_misstep}, "revenue"));
    }
    SUBCASE("contract_portfolio") {

      static_assert(url_max_path_length(identity_v<contract_portfolio>) == 0ul);

      auto contract_portfolio = std::make_shared<stm::contract_portfolio>(5, "", "", model);

      model->contract_portfolios.push_back(contract_portfolio);

      auto const p_step = url_make_step<&stm_system::contract_portfolios>(contract_portfolio->id);
      auto const p_misstep = url_make_step<&stm_system::contract_portfolios>(contract_portfolio->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {p_step}, "revenue"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {p_step}, "quantity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {p_step}, "rexxevenuex"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {p_misstep}, "quantity"));
    }
    SUBCASE("network") {

      static_assert(url_max_path_length(identity_v<network>) == 2ul);
      static_assert(url_max_path_length(identity_v<busbar>) == 1ul);
      static_assert(url_max_path_length(identity_v<transmission_line>) == 0ul);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);

      hps->units.push_back(unit);
      model->hps.push_back(hps);

      auto network = std::make_shared<stm::network>(6, "", "", model);

      auto power_module = std::make_shared<stm::power_module>(10, "", "", model);
      auto busbar = std::make_shared<stm::busbar>(8, "", "", network);

      model->power_modules.push_back(power_module);

      auto power_module_member = std::make_shared<stm::power_module_member>(busbar.get(), power_module, apoint_ts{});
      busbar->power_modules.push_back(power_module_member);

      auto unit_member = std::make_shared<stm::unit_member>(busbar.get(), unit, apoint_ts{});
      busbar->units.push_back(unit_member);

      auto transmission_line = std::make_shared<stm::transmission_line>(9, "", "", network);

      network->busbars.push_back(busbar);
      network->transmission_lines.push_back(transmission_line);

      model->networks.push_back(network);

      auto const n_step = url_make_step<&stm_system::networks>(network->id);
      auto const n_misstep = url_make_step<&stm_system::networks>(network->id + 1);
      auto const t_step = url_make_step<&network::transmission_lines>(transmission_line->id);
      auto const t_misstep = url_make_step<&network::transmission_lines>(transmission_line->id + 1);
      auto const b_step = url_make_step<&network::busbars>(busbar->id);
      auto const b_misstep = url_make_step<&network::busbars>(busbar->id + 1);
      auto const M_step = url_make_step<&busbar::units>(unit_member->id());
      auto const M_misstep = url_make_step<&busbar::units>(unit_member->id() + 1);
      auto const P_step = url_make_step<&busbar::power_modules>(power_module->id);
      auto const P_misstep = url_make_step<&busbar::power_modules>(power_module->id + 1);

      CHECK(url_check_path<stm::transmission_line>(*model, {n_step, t_step}));
      CHECK(!url_check_path<stm::transmission_line>(*model, {n_step, t_misstep}));
      CHECK(url_check_path<stm::busbar>(*model, {n_step, b_step}));
      CHECK(!url_check_path<stm::busbar>(*model, {n_step, b_misstep}));
      CHECK(!url_check_path<stm::busbar>(*model, {n_misstep, b_step}));
      CHECK(url_check_path<stm::unit_member>(*model, {n_step, b_step, M_step}));
      CHECK(!url_check_path<stm::unit_member>(*model, {n_step, b_step, M_misstep}));
      CHECK(url_check_path<stm::power_module_member>(*model, {n_step, b_step, P_step}));
      CHECK(!url_check_path<stm::power_module_member>(*model, {n_step, b_step, P_misstep}));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, t_step}, "capacity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, t_misstep}, "capacity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, t_step}, "capsassity"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step}, "price.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step}, "flow.realised"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_misstep}, "flow.realised"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step}, "flab.blub"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, M_step}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_misstep, M_misstep}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, M_step}, "flab.fleb"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, P_step}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_misstep, P_misstep}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, P_step}, "flab.fleb"));
    }
    SUBCASE("power_module") {

      static_assert(url_max_path_length(identity_v<power_module>) == 0ul);

      auto power_module = std::make_shared<stm::power_module>(2, "", "", model);

      model->power_modules.push_back(power_module);

      auto const m_step = url_make_step<&stm_system::power_modules>(power_module->id);
      auto const m_misstep = url_make_step<&stm_system::power_modules>(power_module->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {m_step}, "power.schedule"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_step}, "powsxxxr.erexx"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_misstep}, "power.result"));
    }

    SUBCASE("planning") {

      auto const model_id = fmt::format("{}", model->id);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);

      hps->units.push_back(unit);
      model->hps.push_back(hps);
      auto market = std::make_shared<energy_market_area>(30, "", "", model);
      model->market.push_back(market);


      {
        auto urls = url_planning_inputs(model_id, *model);

        std::vector<std::string> also_urls;
        url_with_planning_inputs(model_id, *model, [&](ignore_t, ignore_t, std::string_view url) {
          also_urls.push_back(std::string(url));
        });

        CHECK(!urls.empty());
        CHECK(std::ranges::equal(urls, also_urls));

        auto url_market_area_price = url_format(model_id, {url_make_step<&stm_system::market>(market->id)}, "price");

        CHECK(std::ranges::count(urls, url_market_area_price) == 1);

        for (auto const &url : urls) {
          auto parse_result = url_parse_to_result(url);
          auto checker = [&]<typename T>(T const &d) {
            if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_tuple>) {
              // success parsing!
              CHECK(std::get<0>(d) == model_id);
            } else if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_parse_error>) {
              CHECK(false); // failed to parse
            }
          };
          std::visit(checker, parse_result);
        }
      }
      {
        auto urls = url_planning_outputs(model_id, *model);

        CHECK(!urls.empty());

        url_parse_buffer buffer;
        for (auto const &url : urls) {
          auto parse_result = url_parse_to_result(url);
          auto checker = [&]<typename T>(T const &d) {
            if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_tuple>) {
              // success parsing!
              CHECK(std::get<0>(d) == model_id);
            } else if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_parse_error>) {
              CHECK(false); // failed to parse
            }
          };
          std::visit(checker, parse_result);
        }
      }
    }
  }

  TEST_SUITE_END();

}
