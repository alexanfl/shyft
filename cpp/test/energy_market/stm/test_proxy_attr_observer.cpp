#include <doctest/doctest.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include "build_test_system.h"

using namespace shyft::energy_market;
using std::shared_ptr, std::make_shared;
using stm::subscription::proxy_attr_observer;
using shyft::web_api::energy_market::json, shyft::web_api::bg_work_result, shyft::time_series::dd::apoint_ts,
  shyft::time_series::dd::ats_vector, shyft::time_axis::generic_dt, shyft::time_series::ts_point_fx,
  shyft::core::utctime;

namespace {
  struct dstm_fixture {

    shared_ptr<stm::srv::dstm::server> dstm;
    stm::stm_system_ sys;
    stm::unit_ u;
    string request_id{"1"};
    string model_key{"sys"};

    dstm_fixture() {
      dstm = make_shared<stm::srv::dstm::server>();
      sys = test::create_simple_system(1, "sys");
      u = std::dynamic_pointer_cast<stm::unit>(sys->hps[0]->units[0]);
      u->production.result = 10.0 + apoint_ts{"dstm://M" + model_key + "/H1/U1.production.schedule"};
      u->production.schedule = apoint_ts{"shyft://stm/H1/U1.production.schedule"};
      dstm->do_add_model(model_key, sys);
    }
  };


}

TEST_SUITE_BEGIN("stm");

TEST_CASE_FIXTURE(dstm_fixture, "stm/ts_expression_observer") {
  json request;
  request["model_key"] = model_key;
  auto bg_callback = [](json const &) -> bg_work_result {
    return {};
  };
  proxy_attr_observer pa_obs(*dstm.get(), request_id, request, bg_callback);
  FAST_REQUIRE_EQ(pa_obs.ts_sm, dstm->dtss->sm);
  FAST_CHECK_EQ(pa_obs.mid, model_key);
  FAST_CHECK_EQ(pa_obs.ts_subs.size(), 0);
  FAST_CHECK_EQ(pa_obs.terminal_version(), 0);
  SUBCASE("add_sub_empty_terminal") {
    apoint_ts empty{};
    string sub_id{"dstm://M" + model_key + "/x"};
    FAST_CHECK_UNARY(pa_obs.add_ts_subscription(sub_id, empty));
    FAST_REQUIRE_EQ(pa_obs.ts_subs.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.front().id(), sub_id);
    FAST_CHECK_UNARY_FALSE(pa_obs.add_ts_subscription(sub_id, empty)); // already added
  }
  SUBCASE("add_sub_gpoint_terminal") {
    apoint_ts ts{
      shyft::time_axis::generic_dt{utctime{0}, utctime{10}, 3},
      {},
      ts_point_fx::POINT_AVERAGE_VALUE
    };
    string sub_id{"dstm://M" + model_key + "/x"};
    pa_obs.add_ts_subscription(sub_id, ts);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.front().id(), sub_id);
  }
  SUBCASE("add_sub_unbound_ref") {
    apoint_ts unbound{"shyft://test/a"};
    string sub_id{"dstm://M" + model_key + "/x"};
    pa_obs.add_ts_subscription(sub_id, unbound);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.front().id(), unbound.id());
  }
  SUBCASE("add_sub_bound_ref") {
    apoint_ts bound{
      "shyft://test/a",
      apoint_ts{shyft::time_axis::generic_dt{utctime{0}, utctime{10}, 3}, {}, ts_point_fx::POINT_AVERAGE_VALUE}
    };
    string sub_id{"dstm://M" + model_key + "/x"};
    pa_obs.add_ts_subscription(sub_id, bound);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.size(), 1);
    FAST_REQUIRE_EQ(
      pa_obs.ts_subs.front()->ts_expression_template.front().id(), sub_id); // treat it as if just simple gpoint
  }
  SUBCASE("add_sub_unbound_ref_to_dstm") {
    apoint_ts unbound{"dstm://Mtest/a.prod.r"};
    string sub_id{"dstm://M" + model_key + "/x"};
    CHECK_THROWS(pa_obs.add_ts_subscription(sub_id, unbound));
    // because it tries to make apoint_ts(sub_id,ts), an that ct require ts to be equivalent of a gpoint_ts.
    // FAST_REQUIRE_EQ(pa_obs.ts_subs.size(),1);
    // FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.size(),1);
    // FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.front().id(),unbound.id());
  }
  SUBCASE("add_sub_expression") {
    apoint_ts{"dstm://M" + model_key + "/H1/U1.production.result"};
    // apoint_ts b{"shyft://test/a.prod.r"};
    // auto expr= a+b;
    string sub_id{"dstm://M" + model_key + "/H1/U1.production.result"};
    pa_obs.add_ts_subscription(
      sub_id, u->production.result); // its 10x production.schedule (alias shyft://something), so an expression, unbound
    FAST_REQUIRE_EQ(pa_obs.ts_subs.size(), 1);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template.size(), 2);
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template[0].stringify(), u->production.result.stringify());
    FAST_REQUIRE_EQ(pa_obs.ts_subs.front()->ts_expression_template[1].stringify(), u->production.schedule.stringify());
    FAST_CHECK_EQ(pa_obs.terminal_version(), 0);
    // notify change on shyft://something
    pa_obs.ts_sm->notify_change(u->production.schedule.id());
    FAST_CHECK_EQ(pa_obs.terminal_version(), 1);
  }
}

TEST_SUITE_END();
