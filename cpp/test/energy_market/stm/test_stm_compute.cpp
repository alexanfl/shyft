#include <doctest/doctest.h>

#include <chrono>
#include <csignal>
#include <cstdint>
#include <memory>
#include <ranges>
#include <span>
#include <sstream>
#include <string_view>
#include <thread>
#include <tuple>
#include <type_traits>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <boost/asio/thread_pool.hpp>
#include <boost/describe/enumerators.hpp>
#include <boost/mp11/algorithm.hpp>

#include <shyft/config.h>
#include <shyft/core/core_archive.h>
#if defined(SHYFT_WITH_SHOP)
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#endif
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/srv/compute/client.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

#include "shop/model_simple.h"

namespace shyft::energy_market::stm::srv {

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/compute_protocol") {

    constexpr shyft::core::utctime time_axis_begin = std::chrono::hours(1);
    constexpr shyft::core::utctime time_axis_end = std::chrono::hours(8);
    constexpr auto time_axis_step = std::chrono::hours(1);
    constexpr std::size_t time_axis_count = (time_axis_end - time_axis_begin) / time_axis_step;
    const time_axis::generic_dt time_axis{time_axis_begin, time_axis_step, time_axis_count};

    auto make_constant_ts = [&](auto value) {
      return time_series::dd::apoint_ts(
        time_axis::generic_dt(time_axis_begin, time_axis_end - time_axis_begin, 1),
        value,
        time_series::POINT_AVERAGE_VALUE);
    };
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
    auto reblob = []<typename T>(T const &o) {
      std::stringstream stream;
      write_blob(stream, o);
      T o_also;
      read_blob(stream, o_also);
      return o_also;
    };

    auto reserialize = []<typename T>(T const &o) {
      std::stringstream stream;
      {
        shyft::core::core_oarchive archive{stream, shyft::core::core_arch_flags};
        archive << o;
      }
      T o_also;
      {
        shyft::core::core_iarchive archive{stream, shyft::core::core_arch_flags};
        archive >> o_also;
      }
      return o_also;
    };
    auto is_same = []<typename T>(T const &l, T const &r) {
      if constexpr (std::is_same_v<T, compute::start_request>) {
        // NOTE: have to serialize shared-ptr, so can't do equality compare. - jeh
        if (!l.model || !r.model)
          return false;
        return std::tie(l.model_id, *l.model) == std::tie(r.model_id, *r.model);
      } else if constexpr (std::is_same_v<T, compute::get_plan_reply>) {
        // NOTE: have to serialize shared-ptr, so can't do equality compare. - jeh
        if (!l.summary || !r.summary)
          return false;
        return *l.summary == *r.summary;
      } else
        return l == r;
    };

    auto make_request_reply = [&]<compute::message_tag t>(std::integral_constant<compute::message_tag, t>)
      -> std::pair<compute::request<t>, compute::reply<t>> {
      using enum compute::message_tag;
      if constexpr (t == start)
        return {
          {.model_id = "something",
           .model =
             [&] {
               auto model = std::make_shared<stm_system>(2, "ok", "json");
               model->run_params.run_time_axis = time_axis;
               return model;
             }()},
          {}
        };
      else if constexpr (t == get_status)
        return {
          {},
          {.state = compute::state::running,
           .log = {
             {.severity = log_severity::warning, .message = "looks fine", .code = 4, .time = std::chrono::hours(3)}}}
        };
      else if constexpr (t == get_attrs)
        return {{.urls{"someurl"}}, {.attrs{make_constant_ts(4.0)}}};
      else if constexpr (t == set_attrs)
        return {{.attrs{{"dstm://M3/hello", stm::any_attr{make_constant_ts(3.0)}}}}, {}};
      else if constexpr (t == stop)
        return {{}, {}};
      else if constexpr (t == plan)
        return {
          {.time_axis = time_axis, .commands = {shop::shop_command::set_merge_on()}},
          {}
        };
      else if constexpr (t == get_plan)
        return {
          {},
          {.summary =
             [] {
               auto o = std::make_shared<optimization_summary>();
               o->total = 45.0;
               return o;
             }()}

        };
      else {
        static_assert(t != t, "Add a non-default request-reply pair above.");
        return {};
      }
    };

    boost::mp11::mp_for_each<boost::describe::describe_enumerators<compute::message_tag>>([&](auto tag) {
      std::string_view tag_name = tag.name;
      CHECK_MESSAGE(tag.value == reblob(tag.value), tag_name);

      auto [request, reply] = make_request_reply(std::integral_constant<compute::message_tag, tag.value>{});
      CHECK_MESSAGE(is_same(request, reserialize(request)), tag_name);
      CHECK_MESSAGE(is_same(reply, reserialize(reply)), tag_name);
    });

    {
      std::stringstream str{};
      CHECK(compute::try_read_error_message(str) == std::nullopt);
    }
    {
      std::stringstream str{};
      std::string message = "too_short";
      std::uint64_t size = 2 * message.size();
      str.write((char const *) &size, sizeof(size));
      str.write(message.data(), message.size());
      CHECK(compute::try_read_error_message(str) == std::nullopt);
    }
    {
      std::stringstream str{};
      std::string message = "correct";
      std::uint64_t size = message.size();
      str.write((char const *) &size, sizeof(size));
      str.write(message.data(), message.size());
      CHECK(compute::try_read_error_message(str) == message);
    }
#if defined(SHYFT_WITH_SHOP)
    {
      auto model = std::make_shared<stm_system>(1, "1", "1");
      model->contracts.push_back(std::make_shared<stm::contract>(2, "2", "2", model));
      auto &contract = *(model->contracts.back());

      {
        time_axis::generic_dt ta{
          time_axis::fixed_dt{core::utctime(0), core::utctimespan(10), 3}
        };
        CHECK(!shop::should_emit({ta}, contract));
        contract.options = std::make_shared<std::map<core::utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
        CHECK(!shop::should_emit({ta}, contract));
        contract.options->insert({core::utctime(3 * 10), std::make_shared<hydro_power::xy_point_curve>()});
        CHECK(!shop::should_emit({ta}, contract));
        {
          auto get_req = compute::get_plan_attrs_request("dstm://wawa", *model, ta);
          CHECK(get_req.urls.empty());
        }
        contract.options->insert({core::utctime(0), std::make_shared<hydro_power::xy_point_curve>()});
        CHECK(shop::should_emit({ta}, contract));
        {
          auto get_req = compute::get_plan_attrs_request("dstm://wawa", *model, ta);
          CHECK(!get_req.urls.empty());
        }
      }
    }
#endif
  }

  TEST_CASE("stress/stm/compute_server") {

    compute::server server{};

    constexpr auto server_ip = "127.0.0.1";
    server.set_listening_ip(server_ip);
    auto server_port = server.start_server();

    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    const std::size_t n_step = 18;
    const time_axis::fixed_dt t_axis{t_begin, t_step, n_step};
    auto const t_end = t_axis._time(n_step);

    auto model = build_simple_model(t_axis.t, t_end, t_axis.dt, false, false);
    model->name = "test";

    auto make_constant_ts = [&](auto value) {
      return time_series::dd::apoint_ts(
        time_axis::generic_dt(t_begin, t_end - t_begin, 1), value, time_series::POINT_AVERAGE_VALUE);
    };

    SUBCASE("single_client") {

      compute::client client{.connection{fmt::format("{}:{}", server_ip, server_port)}};

      {
        auto reply = client.get_status({});
        CHECK(reply.state == compute::state::idle);
        CHECK(reply.log.empty());
      }

      REQUIRE_NOTHROW(client.stop({}));
      compute::start_request invalid_start_request{.model_id = "test", .model = nullptr};
      REQUIRE_THROWS_AS(client.start(invalid_start_request), std::runtime_error const &);

      compute::start_request start_request{.model_id = "test", .model = model};
      REQUIRE_NOTHROW(client.start(start_request));
      REQUIRE(client.get_status({}).state == compute::state::started);

      REQUIRE_THROWS_AS(client.get_plan({}), std::runtime_error const &);
      compute::plan_request plan_request{
        .time_axis = time_axis::generic_dt{t_axis}, .commands{{"set", "method", "primal"}}};

      REQUIRE_NOTHROW(client.plan(plan_request));
      auto t_timeout = shyft::core::utctime_now() + std::chrono::seconds(10);
      while (client.get_status({}).state == compute::state::running) {
        REQUIRE(shyft::core::utctime_now() < t_timeout);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
      }
      REQUIRE(client.get_status({}).state == compute::state::done);

#if !defined(SHYFT_WITH_SHOP)
      REQUIRE_THROWS_AS(client.get_plan({}), std::runtime_error const &);
#else
      REQUIRE_NOTHROW(client.get_plan({}));
      auto urls = [&] {
        auto model_prefix = fmt::format("dstm://M{}", start_request.model_id);
        return dstm::ts_url_generator(model_prefix, *model);
      }();
      REQUIRE(!urls.empty());
      {
        auto result = client.set_attrs({.attrs{{"invalid_url", {make_constant_ts(4.0)}}}});
        REQUIRE(result.attrs.size() == 1);
        CHECK(!result.attrs[0]);
      }
      {
        auto result = client.set_attrs({.attrs{{urls[0], {time_series::dd::apoint_ts{}}}}});
        REQUIRE(result.attrs.size() == 1);
        CHECK(result.attrs[0]);
      }
      {
        auto result = client.get_attrs({.urls{"invalid_url"}});
        REQUIRE(result.attrs.size() == 1);
        CHECK(!result.attrs[0]);
      }
      {
        auto source_ts = make_constant_ts(5.0);
        client.set_attrs({.attrs{{urls[0], {source_ts}}}});
        auto get_attrs = client.get_attrs({.urls{urls[0]}});
        REQUIRE(get_attrs.attrs.size() == 1);
        auto target_ts = get_attrs.attrs.front();
        REQUIRE(target_ts);
        REQUIRE(std::holds_alternative<time_series::dd::apoint_ts>(*target_ts));
        // FIXME: do a more exact compare - jeh
        CHECK(source_ts(t_begin) == std::get<time_series::dd::apoint_ts>(*target_ts)(t_begin));
      }
      REQUIRE(client.get_status({}).state == compute::state::done);
      REQUIRE_NOTHROW(client.get_attrs({}));
#endif
      REQUIRE_NOTHROW(client.stop({}));
      REQUIRE(client.get_status({}).state == compute::state::idle);
      REQUIRE_THROWS_AS(client.get_attrs({}), std::runtime_error const &);
      REQUIRE_THROWS_AS(client.set_attrs({}), std::runtime_error const &);
      REQUIRE_THROWS_AS(client.get_plan({}), std::runtime_error const &);
    }
    SUBCASE("multiple_clients") {

#if defined(SHYFT_WITH_SHOP)
      std::vector<std::jthread> clients;
      for (auto i : std::views::iota(0, 10))
        clients.emplace_back([&, i] {
          compute::client client{.connection{fmt::format("{}:{}", server_ip, server_port)}};

          compute::start_request start_request{.model_id = fmt::format("test{}", i), .model = model};
          compute::plan_request plan_request{.time_axis = time_axis::generic_dt{t_axis}, .commands{}};

          auto t_timeout = shyft::core::utctime_now() + std::chrono::seconds(10);
          while (1) {
            try {
              client.start(start_request);
              client.plan(plan_request);
              break;
            } catch (const std::runtime_error &) {
              std::this_thread::sleep_for(std::chrono::milliseconds(50));
            }
            REQUIRE(shyft::core::utctime_now() < t_timeout);
          }
          while (client.get_status({}).state == compute::state::running) {
            REQUIRE(shyft::core::utctime_now() < t_timeout);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
          }
          REQUIRE(client.get_status({}).state == compute::state::done);
          REQUIRE_NOTHROW(client.stop({}));
        });
#endif
    }
  }

  TEST_CASE("stress/stm/compute_manager") {

    std::optional<compute::server> server0{}, server1{}, server2{};

    server0.emplace();
    server1.emplace();
    server2.emplace();

    constexpr auto server_ip = "127.0.0.1";
    server0->set_listening_ip(server_ip);
    server1->set_listening_ip(server_ip);
    server2->set_listening_ip(server_ip);
    auto server0_port = server0->start_server();
    auto server1_port = server1->start_server();
    auto server2_port = server2->start_server();

    auto address0 = fmt::format("{}:{}", server_ip, server0_port);
    auto address1 = fmt::format("{}:{}", server_ip, server1_port);
    auto address2 = fmt::format("{}:{}", server_ip, server2_port);

    std::shared_ptr<stm_system> model = std::make_shared<stm_system>(0, "whatever", "{}");
    compute::manager manager{};

    SUBCASE("bunk_address") {
      REQUIRE(manager.manage("bunk"));
      CHECK(!manager.assign("model0", model));
      manager.tidy();
      CHECK(manager.servers.empty());
    }
    SUBCASE("server_assignment") {
      REQUIRE(manager.manage(address0));
      REQUIRE(manager.manage(address1));
      {
        auto status = manager.status();
        REQUIRE(status.size() == 2);
        CHECK(status[0].address == address0);
        CHECK(status[1].address == address1);
        CHECK(status[0].state == compute::managed_server_state::idle);
        CHECK(status[1].state == compute::managed_server_state::idle);
      }
      SUBCASE("basic") {
        REQUIRE(!manager.manage(address0));
        auto assigned0 = manager.assign("model0", model);
        REQUIRE(assigned0);
        CHECK(manager.assigned("model0"));
        CHECK(manager.assign("model0", model));
        auto assigned1 = manager.assign("model1", model);
        REQUIRE(assigned1);
        CHECK(manager.assigned("model1"));
        CHECK(!manager.assign("model2", model));
        CHECK(!manager.assigned("model2"));
        assigned0->unassign();
        assigned1->unassign();
      }
      SUBCASE("unexpected_done") {
        server0->state = compute::state::done;
        CHECK(!manager.assign("model1", model));
        auto assigned0 = manager.assign("model1", model);
        assigned0->unassign();
      }
      SUBCASE("unexpected_dead") {
        auto assigned0 = manager.assign("model1", model);
        REQUIRE(assigned0);
        server0 = std::nullopt;
        assigned0->unassign();
        auto assigned1 = manager.assign("model1", model);
        REQUIRE(assigned1);
        CHECK(assigned1->address == address1);
      }
    }
    SUBCASE("janitor") {
      boost::asio::thread_pool threads(4);
      compute::janitor_config j_c{std::chrono::milliseconds(100), std::chrono::milliseconds(500)};
      auto janitor = compute::make_janitor(threads.executor(), j_c);

      SUBCASE("dead_server") {
        manager.manage(address0);
        REQUIRE(manager.servers.size() == 1);
        manager.servers[0]->state = compute::managed_server_state::dead;
        janitor.start(manager);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        janitor.stop();
        threads.join();
        CHECK(manager.servers.empty());
      }
      SUBCASE("stale_server") {
        manager.manage(address0);
        REQUIRE(manager.servers.size() == 1);
        SUBCASE("busy") {
          manager.servers[0]->state = compute::managed_server_state::busy;
          manager.servers[0]->last_send = shyft::core::utctime_now() - 2 * janitor.stale_threshold;
          janitor.start(manager);
          std::this_thread::sleep_for(std::chrono::milliseconds(200));
          janitor.stop();
          threads.join();
          CHECK(manager.servers.empty());
        }
        SUBCASE("assigned") {
          manager.servers[0]->state = compute::managed_server_state::busy;
          manager.servers[0]->last_send = shyft::core::utctime_now();
          janitor.start(manager);
          std::this_thread::sleep_for(std::chrono::milliseconds(200));
          janitor.stop();
          threads.join();
          CHECK(!manager.servers.empty());
        }
        SUBCASE("idle") {
          manager.servers[0]->state = compute::managed_server_state::idle;
          manager.servers[0]->last_send = shyft::core::utctime_now() - 2 * janitor.stale_threshold;
          janitor.start(manager);
          std::this_thread::sleep_for(std::chrono::milliseconds(200));
          janitor.stop();
          threads.join();
          CHECK(!manager.servers.empty());
        }
      }
    }
    SUBCASE("session_tasks") {

      auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
      auto const t_step = shyft::core::deltahours(1);
      const std::size_t n_step = 18;
      const time_axis::fixed_dt t_axis{t_begin, t_step, n_step};
      auto const t_end = t_axis._time(n_step);

      auto model = build_simple_model(t_axis.t, t_end, t_axis.dt, false, false);
      model->name = "test";

      std::vector<compute::task_tag> tags;

      SUBCASE("single_client") {

        SUBCASE("dead_server") {
          REQUIRE(manager.manage(address2));
          auto assigned_server = manager.assign("model0", model);
          REQUIRE(assigned_server);

          server2 = std::nullopt;
          auto started = assigned_server->task([&](auto compute_task) {
            tags.push_back(compute_task.tag);
            if constexpr (compute_task.tag == compute::task_tag::start)
              return shyft::core::from_seconds(1);
            else
              return true;
          });
          REQUIRE(started);
          CHECK(assigned_server->watchdog.wait_for(std::chrono::seconds(2)) == std::future_status::ready);
          std::vector tags_expected{compute::task_tag::start, compute::task_tag::dead};
          CHECK_MESSAGE(tags == tags_expected, fmt::format("{} != {}", tags, tags_expected));
          CHECK(!manager.assigned("test"));
          CHECK(!manager.assign("model0", model));
          manager.tidy();
          CHECK(manager.servers.empty());
        }

        SUBCASE("basic_run") {

          REQUIRE(manager.manage(address0));
          auto assigned_server = manager.assign("model0", model);
          REQUIRE(assigned_server);

          auto started = assigned_server->task([&](auto compute_task) {
            tags.push_back(compute_task.tag);
            if constexpr (compute_task.tag == compute::task_tag::start)
              return shyft::core::from_seconds(1);
            else
              return true;
          });
          REQUIRE(started);
          CHECK(assigned_server->watchdog.wait_for(std::chrono::seconds(2)) == std::future_status::ready);

          std::vector tags_expected{compute::task_tag::start, compute::task_tag::tick, compute::task_tag::done};
          CHECK_MESSAGE(tags == tags_expected, fmt::format("{} != {}", tags, tags_expected));
          CHECK(!manager.assigned("test"));
        }

        SUBCASE("timeout_run") {
          REQUIRE(manager.manage(address0));
          auto assigned_server = manager.assign("model0", model);
          REQUIRE(assigned_server);

          auto started = assigned_server->task([&](auto compute_task) {
            tags.push_back(compute_task.tag);
            if constexpr (compute_task.tag == compute::task_tag::start) {
              server0->state = compute::state::running;
              return std::chrono::milliseconds(100);
            } else
              return true;
          });
          REQUIRE(started);
          CHECK(assigned_server->watchdog.wait_for(std::chrono::seconds(2)) == std::future_status::ready);

          CHECK(tags.size() >= 2);
          CHECK_MESSAGE(
            tags.front() == compute::task_tag::start, fmt::format("{} != {}", tags.front(), compute::task_tag::start));
          CHECK_MESSAGE(
            tags.back() == compute::task_tag::dead, fmt::format("{} != {}", tags.back(), compute::task_tag::dead));
          CHECK(std::ranges::all_of(std::span{tags}.subspan(1ul, tags.size() - 2), [](auto tag) {
            return tag == compute::task_tag::tick;
          }));
          assigned_server = manager.assign("model0", model);
          REQUIRE(!assigned_server);
          manager.tidy();
          REQUIRE(manager.servers.empty());
        }
      }
    }
  }

#if defined(SHYFT_WITH_SHOP)
  TEST_CASE("stm/compute_shop_optimisation") {
    compute::server server{};
    constexpr auto ip = "127.0.0.1";
    server.set_listening_ip(ip);
    auto port = server.start_server();
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = core::deltahours(1);
    constexpr auto n_step = 18;
    time_axis::fixed_dt const t_axis{t_begin, t_step, n_step};
    auto const t_end = t_axis._time(n_step);
    auto const stm_model = build_simple_model_discharge_group(t_begin, t_end, t_step, false, true);
    auto river_ = std::dynamic_pointer_cast<stm::waterway>(
      stm_model->hps.front()->find_waterway_by_name("waterroute river"));

    // Decorate connection with attrs to enforce discharge group
    river_->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    river_->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 1e6);
    river_->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    river_->deviation.realised = make_constant_ts(t_begin, t_end, 1e6);
    stm_model->name = "test";

    auto generic_time_axis = time_axis::generic_dt(t_begin, t_end - t_begin, 1);
    SUBCASE("deviation") {
      compute::client client{.connection{fmt::format("{}:{}", ip, port)}};
      {
        auto reply = client.get_status({});
        CHECK(reply.state == compute::state::idle);
        CHECK(reply.log.empty());
      }

      compute::start_request start_request{.model_id = "test", .model = stm_model};
      REQUIRE_NOTHROW(client.start(start_request));
      REQUIRE(client.get_status({}).state == compute::state::started);

      auto const dtss = std::make_unique<dtss::server>();

      compute::send_input_and_plan(
        *dtss, "test", *stm_model, client, generic_time_axis, optimization_commands(1));

      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      while(client.get_status({}).state == compute::state::started)
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      while(client.get_status({}).state == compute::state::running)
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      REQUIRE(client.get_status({}).state == compute::state::done);
      CHECK(exists(river_->deviation.realised));
      CHECK(!exists(river_->deviation.result));
      get_plan_result("test", dtss->sm.get(), *dtss->sm.get(), *stm_model, client, generic_time_axis);
      CHECK(exists(river_->deviation.result));
    }

  }
#endif
  TEST_SUITE_END();

}
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
