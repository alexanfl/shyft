#include "model_shop_pump_example.h"

#include <memory>
#include <map>
#include <vector>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::energy_market::stm::test_models {

  std::shared_ptr<stm_hps> build_shop_pump_example_hps(shyft::core::utctime t_begin, shyft::core::utctime t_end) {

    auto make_constant_ts = [&](auto value) {
      return time_series::dd::apoint_ts(
        time_series::dd::gta_t(t_begin, t_end - t_begin, 1), value, time_series::POINT_AVERAGE_VALUE);
    };

    int id = 1;

    auto hps = std::make_shared<stm_hps>(id++, "a");

    stm_hps_builder hps_builder(hps);

    auto v0 = [&] {
      auto v0 = hps_builder.create_reservoir(id++, "v0", "");
      v0->level.constraint.min = make_constant_ts(90.0);
      v0->level.constraint.max = make_constant_ts(100.0);
      v0->volume.constraint.max = make_constant_ts(12.0e6);
      v0->volume_level_mapping =
        std::make_shared<std::map<shyft::core::utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
      v0->volume_level_mapping->emplace(
        t_begin,
        std::make_shared<hydro_power::xy_point_curve>(hydro_power::xy_point_curve{
          .points{{.x = 0.0e6, .y = 0.90e2}, {.x = 12.0e6, .y = 1.00e2}, {.x = 14.0e6, .y = 1.01e2}}
      }));
      v0->level.realised = make_constant_ts(92.0);
      v0->inflow.realised = make_constant_ts(10.0);
      v0->water_value.endpoint_desc = make_constant_ts(39.7e-6);
      return v0;
    }();

    auto v1 = [&] {
      auto v1 = hps_builder.create_reservoir(id++, "v1", "");
      v1->level.constraint.min = make_constant_ts(40.0);
      v1->level.constraint.max = make_constant_ts(50.0);
      v1->volume.constraint.max = make_constant_ts(5.0e6);
      v1->volume_level_mapping =
        std::make_shared<std::map<shyft::core::utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
      v1->volume_level_mapping->emplace(
        t_begin,
        std::make_shared<hydro_power::xy_point_curve>(hydro_power::xy_point_curve{
          .points{{.x = 0.0e6, .y = 40.0}, {.x = 5.0e6, .y = 50.0}, {.x = 6.0e6, .y = 51.0}}
      }));
      v1->level.realised = make_constant_ts(43.0);
      v1->inflow.realised = make_constant_ts(0.0);
      v1->water_value.endpoint_desc = make_constant_ts(0.0e6);
      return v1;
    }();

    auto g0 = [&] {
      auto g0 = hps_builder.create_gate(id++, "g0", "");

      g0->flow_description = std::make_shared<
        std::map<shyft::core::utctime, std::shared_ptr<std::vector<hydro_power::xy_point_curve_with_z>>>>();
      g0->flow_description->emplace(
        t_begin,
        std::make_shared<std::vector<hydro_power::xy_point_curve_with_z>>(
          std::vector<hydro_power::xy_point_curve_with_z>{
            {.xy_curve{.points{{.x = 1.00e2, .y = 0.0e3}, {.x = 1.01e2, .y = 1.0e3}}}, .z = 0.0}
      }));
      return g0;
    }();

    auto g1 = [&] {
      auto g1 = hps_builder.create_gate(id++, "g1", "");

      g1->flow_description = std::make_shared<
        std::map<shyft::core::utctime, std::shared_ptr<std::vector<hydro_power::xy_point_curve_with_z>>>>();
      g1->flow_description->emplace(
        t_begin,
        std::make_shared<std::vector<hydro_power::xy_point_curve_with_z>>(
          std::vector<hydro_power::xy_point_curve_with_z>{
            {.xy_curve{.points{{.x = 5.0e1, .y = 0.0e3}, {.x = 5.1e1, .y = 1.0e3}}}, .z = 0.0}
      }));
      return g1;
    }();

    auto u0 = [&] {
      auto u0 = hps_builder.create_unit(id++, "u0", "");
      u0->turbine_description =
        std::make_shared<std::map<shyft::core::utctime, std::shared_ptr<hydro_power::turbine_description>>>();
      u0->turbine_description->emplace(
        t_begin,
        std::make_shared<hydro_power::turbine_description>(
          hydro_power::turbine_description{.operating_zones{{.efficiency_curves{
            {.xy_curve{.points{{.x = -60.0, .y = 1.0e2 * 0.85}}}, .z = -60.0},
            {.xy_curve{.points{{.x = -70.0, .y = 1.0e2 * 0.86}}}, .z = -50.0},
            {.xy_curve{.points{{.x = -80.0, .y = 1.0e2 * 0.87}}}, .z = -40.0},
            {.xy_curve{
               .points{{.x = 25.0, .y = 1.0e2 * 0.8}, {.x = 90.0, .y = 1.0e2 * 0.95}, {.x = 100.0, .y = 1.0e2 * 0.9}}},
             .z = 90.0},
            {.xy_curve{.points{
               {.x = 25.0, .y = 1.0e2 * 0.82}, {.x = 90.0, .y = 1.0e2 * 0.98}, {.x = 100.0, .y = 1.0e2 * 0.92}}},
             .z = 100.0}}}}}));

      u0->generator_description =
        std::make_shared<std::map<shyft::core::utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
      u0->generator_description->emplace(
        t_begin,
        std::make_shared<hydro_power::xy_point_curve>(hydro_power::xy_point_curve{
          .points{{.x = 0.0e8, .y = 1.0e2 * 0.95}, {.x = 1.0e8, .y = 1.0e2 * 0.98}}
      }));
      u0->production.static_min = make_constant_ts(0.25e8);
      u0->production.static_max = make_constant_ts(1.00e8);
      u0->production.nominal = make_constant_ts(1.00e8);

      u0->cost.start = make_constant_ts(5.0e2);
      u0->cost.pump_start = make_constant_ts(5.0e2);
      return u0;
    }();
    auto p = hps_builder.create_power_plant(id++, "p", "");
    power_plant::add_unit(p, u0);
    p->outlet_level = make_constant_ts(4.0e1);

    auto t0 = hps_builder.create_tunnel(id++, "t0", ""), t1 = hps_builder.create_tunnel(id++, "t1", ""),
         t2 = hps_builder.create_tunnel(id++, "t2", "");

    t0->head_loss_coeff = make_constant_ts(3.0e-4);

    auto r0 = hps_builder.create_river(id++, "r0", ""), r1 = hps_builder.create_river(id++, "r1", ""),
         r2 = hps_builder.create_river(id++, "r2", "");

    {
      using ww = hydro_power::waterway;
      ww::add_gate(r0, g0);
      ww::add_gate(r1, g1);
    }
    {
      using hc = hydro_power::hydro_component;
      hc::connect(v0, hydro_power::connection_role::main, t0);
      hc::connect(t0, t1);
      hc::connect(t1, u0);
      hc::connect(u0, t2);
      hc::connect(t2, v1);
      hc::connect(v0, hydro_power::connection_role::flood, r0);
      hc::connect(v1, hydro_power::connection_role::flood, r1);
      hc::connect(r0, r2);
      hc::connect(r1, r2);
    }

    return hps;
  }

  std::shared_ptr<stm_system> build_shop_pump_example(shyft::core::utctime t_begin, shyft::core::utctime t_end) {

    int id = 1000;
    auto make_constant_ts = [&](auto value) {
      return time_series::dd::apoint_ts(
        time_series::dd::gta_t(t_begin, t_end - t_begin, 1), value, time_series::POINT_AVERAGE_VALUE);
    };

    auto hps = build_shop_pump_example_hps(t_begin, t_end);

    auto s = std::make_shared<stm_system>(id++, "x", "");
    s->hps.push_back(hps);
    stm_builder builder(s);

    auto m = builder.create_market_area(id++, "m0", "");

    // be explicit about time_series::dd::ipoint_ts, else we run into a ambiguity.
    // should probably revise the ctors of apoint_ts. - jeh
    m->price = time_series::dd::apoint_ts(std::dynamic_pointer_cast<time_series::dd::ipoint_ts>(
      std::make_shared<time_series::dd::gpoint_ts>(time_series::dd::gpoint_ts{
        time_axis::generic_dt{time_axis::point_dt{{t_begin, t_begin + shyft::core::deltahours(6)}, t_end}},
        {19.99e-6, 39.99e-6},
        shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
    })));
    // shop-example also has a buy-price of [20.01,40.01], but not used in stm.
    // da.buy_price.set(pd.Series([20.01,40.01],index=[starttime,starttime+pd.Timedelta(hours=6)]))
    // - jeh

    m->max_sale = make_constant_ts(9999e6);
    m->max_buy = make_constant_ts(9999e6);

    return s;
  }

}
