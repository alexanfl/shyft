#include <cstdint>

#include <doctest/doctest.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

#include "model_simple.h"

namespace shyft::energy_market::stm {

  TEST_SUITE_BEGIN("stm/shop");

  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
  auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
  auto const dt = shyft::core::deltahours(1);
  const std::size_t n_t = (t_end - t_begin) / dt;
  const time_axis::generic_dt ta(t_begin, dt, n_t);

  TEST_CASE("stm/shop/log" * doctest::description("usage of shop_system::get_log_buffer")) {
    // Set up system to optimize and expected solution
    auto stm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults, 1, true);
    // Create shop system
    shop::shop_system shop(ta);

    // 0. Check that log is empty:
    auto messages = shop.get_log_buffer();
    CHECK_EQ(messages.size(), 0);
    // Do optimization:
    auto const commands = optimization_commands(1, false);
    shop.emit(*stm);
    // 1. Check log after emit:
    auto new_messages = shop.get_log_buffer();
    messages.insert(messages.end(), new_messages.begin(), new_messages.end());
    CHECK_EQ(messages.size(), 0);
    for (auto const &command : commands)
      shop.commander.execute(command);
    // 2. Check log after commands:
    new_messages = shop.get_log_buffer();
    messages.insert(messages.end(), new_messages.begin(), new_messages.end());
    auto N = messages.size();
    CHECK_GE(N, 0);
    CHECK_EQ(shop.get_log_buffer().size(), 0);

    shop.collect(*stm);
    // 3. Check log after collecting results
    CHECK_EQ(shop.get_log_buffer().size(), 0);

    check_results(stm, rstm, t_begin, t_end, dt);
  }

  TEST_SUITE_END();

}
