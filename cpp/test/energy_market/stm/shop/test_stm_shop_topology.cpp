#include <memory>
#include <string>
#include <vector>

#include <doctest/doctest.h>

#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/time/utctime_utilities.h>

#include "model_simple.h"
#include "model_shop_pump_example.h"

static bool const shop_console_output = false;
static bool const shop_log_files = false;

template <class T>
int count_shop_api_objects(shyft::energy_market::stm::shop::shop_api& api) {
  auto const type_name_filter = ShopGetObjectTypeName(api.c, T::t_id);
  int const n = ShopGetObjectCount(api.c);
  int count = 0;
  for (int ix = 0; ix < n; ++ix) {
    auto const type_name = ShopGetObjectType(api.c, ix);
    /*const auto name = */ (void) ShopGetObjectName(api.c, ix);
    if (strcmp(type_name, type_name_filter) == 0)
      ++count;
  }
  return count;
}

using std::size_t;
using std::string;
using std::make_shared;
using namespace std::string_literals;
using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using namespace shyft::energy_market::stm::shop;
using shyft::core::utctime;
using shyft::core::utctimespan;
using shyft::core::to_seconds64;
using shyft::core::deltahours;
using shyft::core::deltaminutes;
using shyft::time_axis::point_dt;
using shyft::time_axis::generic_dt;

TEST_SUITE_BEGIN("stm/shop");

// common for test-cases:
auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
auto const t_step = shyft::core::deltahours(1);
const size_t n_step = 18;
const generic_dt time_axis{t_begin, t_step, n_step};

const shyft::core::utcperiod t_period{time_axis.total_period()};
auto const t_end = t_period.end;

auto create_shop_system = [](stm_hps_ hps) -> shared_ptr<shop_system> {
  // wrap the hps in a dummy stm system
  auto mdl = make_shared<stm_system>(0, "dummy_stm_system"s, ""s);
  auto mkt = make_shared<energy_market_area>(0, "dummy_stm_market"s, ""s, mdl);
  mdl->hps.push_back(hps);
  mdl->market.push_back(mkt);

  // creating the shop system calls ShopInit
  auto s = make_shared<shop_system>(time_axis);
  return s;
};

TEST_CASE("stm/shop/penstock_topology") {

  SUBCASE("penstock_topology_01") {
    // R
    // |
    // | <-- penstock
    // A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    connect(wtr_main).input_from(rsv).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);
    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
  }

  SUBCASE("penstock_topology_02") {
    //   R
    //   |
    //  / \  <-- penstocks
    // A   A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
    connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
    connect(wtr_penstock1).output_to(g1);
    connect(wtr_penstock2).output_to(g2);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
    auto penstock_g2 = shop_emitter::get_penstock(*g2);
    CHECK(penstock_g2 == wtr_penstock2); // g2: penstock 2
  }

  SUBCASE("penstock_topology_03") {
    /*
         R
         |
        /  \  <-- penstocks
      / \  / \
     A   AA   A
    */
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
    auto wtr_inlet1a = builder.create_tunnel(id++, "waterroute inlet 1A"s, ""s);
    auto wtr_inlet1b = builder.create_tunnel(id++, "waterroute inlet 1B"s, ""s);
    auto wtr_inlet2a = builder.create_tunnel(id++, "waterroute inlet 2A"s, ""s);
    auto wtr_inlet2b = builder.create_tunnel(id++, "waterroute inlet 2B"s, ""s);
    connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
    connect(wtr_penstock1).output_to(wtr_inlet1a).output_to(wtr_inlet1b);
    connect(wtr_penstock2).output_to(wtr_inlet2a).output_to(wtr_inlet2b);
    connect(wtr_inlet1a).output_to(g1);
    connect(wtr_inlet1b).output_to(g2);
    connect(wtr_inlet2a).output_to(g3);
    connect(wtr_inlet2b).output_to(g4);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
    auto penstock_g2 = shop_emitter::get_penstock(*g2);
    CHECK(penstock_g2 == wtr_penstock1); // g2: penstock 1
    auto penstock_g3 = shop_emitter::get_penstock(*g3);
    CHECK(penstock_g3 == wtr_penstock2); // g3: penstock 2
    auto penstock_g4 = shop_emitter::get_penstock(*g4);
    CHECK(penstock_g4 == wtr_penstock2); // g4: penstock 2
  }

  SUBCASE("penstock_topology_04a") {
    /* R
       |
      /  \  <-- penstocks
     A  / \
       A   A
    */
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
    auto wtr_inlet2a = builder.create_tunnel(id++, "waterroute inlet 2A"s, ""s);
    auto wtr_inlet2b = builder.create_tunnel(id++, "waterroute inlet 2B"s, ""s);
    connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
    connect(wtr_penstock2).output_to(wtr_inlet2a).output_to(wtr_inlet2b);
    connect(wtr_penstock1).output_to(g1);
    connect(wtr_inlet2a).output_to(g2);
    connect(wtr_inlet2b).output_to(g3);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
    auto penstock_g2 = shop_emitter::get_penstock(*g2);
    CHECK(penstock_g2 == wtr_penstock2); // g2: penstock 2
    auto penstock_g3 = shop_emitter::get_penstock(*g3);
    CHECK(penstock_g3 == wtr_penstock2); // g3: penstock 2
  }

  SUBCASE("penstock_topology_04b") {
    /*   R
         |
        / \  <-- penstocks
      / \  A
     A   A
     */
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
    auto wtr_inlet1a = builder.create_tunnel(id++, "waterroute inlet 1A"s, ""s);
    auto wtr_inlet1b = builder.create_tunnel(id++, "waterroute inlet 1B"s, ""s);
    connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
    connect(wtr_penstock1).output_to(wtr_inlet1a).output_to(wtr_inlet1b);
    connect(wtr_penstock2).output_to(g3);
    connect(wtr_inlet1a).output_to(g1);
    connect(wtr_inlet1b).output_to(g2);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
    auto penstock_g2 = shop_emitter::get_penstock(*g2);
    CHECK(penstock_g2 == wtr_penstock1); // g2: penstock 1
    auto penstock_g3 = shop_emitter::get_penstock(*g3);
    CHECK(penstock_g3 == wtr_penstock2); // g3: penstock 2
  }

  SUBCASE("penstock_topology_05") {
    /*       R
             |
             |
             |
        /  /   \    \  <-- penstocks
      /|\ A   //|\\  A
      AAA     AAAAA
    */
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
    auto g5 = builder.create_unit(id++, "aggregate 5"s, ""s);
    auto g6 = builder.create_unit(id++, "aggregate 6"s, ""s);
    auto g7 = builder.create_unit(id++, "aggregate 7"s, ""s);
    auto g8 = builder.create_unit(id++, "aggregate 8"s, ""s);
    auto g9 = builder.create_unit(id++, "aggregate 9"s, ""s);
    auto g10 = builder.create_unit(id++, "aggregate 10"s, ""s);
    auto wtr_main1 = builder.create_tunnel(id++, "waterroute main tunnel 1"s, ""s);
    auto wtr_main2 = builder.create_tunnel(id++, "waterroute main tunnel 2"s, ""s);
    auto wtr_main3 = builder.create_tunnel(id++, "waterroute main tunnel 3"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
    auto wtr_penstock3 = builder.create_tunnel(id++, "waterroute penstock 3"s, ""s);
    auto wtr_penstock4 = builder.create_tunnel(id++, "waterroute penstock 4"s, ""s);
    auto wtr_inlet1a = builder.create_tunnel(id++, "waterroute inlet 1A"s, ""s);
    auto wtr_inlet1b = builder.create_tunnel(id++, "waterroute inlet 1B"s, ""s);
    auto wtr_inlet1c = builder.create_tunnel(id++, "waterroute inlet 1C"s, ""s);
    auto wtr_inlet3a = builder.create_tunnel(id++, "waterroute inlet 3A"s, ""s);
    auto wtr_inlet3b = builder.create_tunnel(id++, "waterroute inlet 3B"s, ""s);
    auto wtr_inlet3c = builder.create_tunnel(id++, "waterroute inlet 3C"s, ""s);
    auto wtr_inlet3d = builder.create_tunnel(id++, "waterroute inlet 3D"s, ""s);
    auto wtr_inlet3e = builder.create_tunnel(id++, "waterroute inlet 3E"s, ""s);
    connect(wtr_main1).input_from(rsv).output_to(wtr_main2);
    connect(wtr_main2).output_to(wtr_main3);
    connect(wtr_main3)
      .output_to(wtr_penstock1)
      .output_to(wtr_penstock2)
      .output_to(wtr_penstock3)
      .output_to(wtr_penstock4);
    connect(wtr_penstock1).output_to(wtr_inlet1a).output_to(wtr_inlet1b).output_to(wtr_inlet1c);
    connect(wtr_penstock2).output_to(g4);
    connect(wtr_penstock3)
      .output_to(wtr_inlet3a)
      .output_to(wtr_inlet3b)
      .output_to(wtr_inlet3c)
      .output_to(wtr_inlet3d)
      .output_to(wtr_inlet3e);
    connect(wtr_penstock4).output_to(g10);
    connect(wtr_inlet1a).output_to(g1);
    connect(wtr_inlet1b).output_to(g2);
    connect(wtr_inlet1c).output_to(g3);
    connect(wtr_inlet3a).output_to(g5);
    connect(wtr_inlet3b).output_to(g6);
    connect(wtr_inlet3c).output_to(g7);
    connect(wtr_inlet3d).output_to(g8);
    connect(wtr_inlet3e).output_to(g9);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
    auto penstock_g2 = shop_emitter::get_penstock(*g2);
    CHECK(penstock_g2 == wtr_penstock1); // g2: penstock 1
    auto penstock_g3 = shop_emitter::get_penstock(*g3);
    CHECK(penstock_g3 == wtr_penstock1); // g3: penstock 1
    auto penstock_g4 = shop_emitter::get_penstock(*g4);
    CHECK(penstock_g4 == wtr_penstock2); // g4: penstock 2
    auto penstock_g5 = shop_emitter::get_penstock(*g5);
    CHECK(penstock_g5 == wtr_penstock3); // g5: penstock 3
    auto penstock_g6 = shop_emitter::get_penstock(*g6);
    CHECK(penstock_g6 == wtr_penstock3); // g6: penstock 3
    auto penstock_g7 = shop_emitter::get_penstock(*g7);
    CHECK(penstock_g7 == wtr_penstock3); // g7: penstock 3
    auto penstock_g8 = shop_emitter::get_penstock(*g8);
    CHECK(penstock_g8 == wtr_penstock3); // g8: penstock 3
    auto penstock_g9 = shop_emitter::get_penstock(*g9);
    CHECK(penstock_g9 == wtr_penstock3); // g9: penstock 3
    auto penstock_g10 = shop_emitter::get_penstock(*g10);
    CHECK(penstock_g10 == wtr_penstock4); // g10: penstock 4
  }

  SUBCASE("penstock_topology_06") {
    // R
    // | <-- penstock or main tunnel?
    // A
    //
    // This should perhaps be considered an illegal topology, and require at least one penstock and one main tunnel
    // waterroutes, this test shows how the penstock algorithm currently handles it: Returning the single waterroute as
    // the penstock.
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    connect(wtr_main).input_from(rsv).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
  }

  SUBCASE("penstock_topology_07") {
    // R
    // |
    // | <-- (this is never the penstock, with inlet downstream)
    // | <-- penstock (this is never the inlet, with penstock upstream)
    // A
    //
    // This shows the case where we would have ambiguous topology between
    // aggregate-inlet-penstock-main-rsv and aggregate-penstock-main1-main2-rsv,
    // but where the assumption that inlet can only be modelled when necessary (shared penstocks)
    // makes it unambiguous where the penstock is.
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto wtr_main1 = builder.create_tunnel(id++, "waterroute main tunnel 1"s, ""s);
    auto wtr_main2 = builder.create_tunnel(id++, "waterroute main tunnel2 "s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    connect(wtr_main1).input_from(rsv).output_to(wtr_main2);
    connect(wtr_main2).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);

    auto penstock_g1 = shop_emitter::get_penstock(*g1);
    CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
  }
}

TEST_CASE("stm/shop/plant_input_topology") {
  SUBCASE("plant_input_topology_01") {
    // Base case:
    //
    // R
    // | <-- main tunnel (tunnel object in shop, in legacy shop it was part of plant object as a loss factor)
    // | <-- (penstock, part of plant object as a loss factor in shop)
    // A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    connect(wtr_main).input_from(rsv).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);

    // Handle objects between main tunnel and reservoir (should be nothing)
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*rsv, api.adapter.to_shop(*rsv));
    auto plant = api.api.create<shop_power_plant>("plant"); // Just needs a shop object to connect tunnels into
    api.emitter.handle_plant_input(*wtr_main, plant);       // Look upstream from main tunnel

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_main));
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.exists<shop_reservoir>(rsv));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 1);
  }
  SUBCASE("plant_input_topology_02") {
    // One extra tunnel segment:
    //
    // R
    // | <-- tunnel (tunnel object in shop)
    // | <-- main tunnel (tunnel object in shop, in legacy shop it was part of plant object as a loss factor)
    // | <-- (penstock, part of plant object as a loss factor in shop)
    // A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto wtr_tunnel = builder.create_tunnel(id++, "rsv output tunnel"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    connect(wtr_tunnel).input_from(rsv).output_to(wtr_main);
    connect(wtr_main).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);

    // Handle objects between main tunnel and reservoir (should be the tunnel)
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*rsv, api.adapter.to_shop(*rsv));
    auto plant = api.api.create<shop_power_plant>("plant");
    api.emitter.handle_plant_input(*wtr_main, plant);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 2);
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_main));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_tunnel));
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.exists<shop_reservoir>(rsv));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 2);
  }
  SUBCASE("plant_input_topology_03") {
    // One plain junction:
    //
    // R  R
    // \ / <-- tunnels
    //  | <-- main tunnel (with two inputs)
    //  | <-- (penstock)
    //  A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto wtr_r1_main = builder.create_tunnel(id++, "waterroute reservoir 1 main tunnel"s, ""s);
    auto wtr_r2_main = builder.create_tunnel(id++, "waterroute reservoir 2 main tunnel"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);

    connect(wtr_r1_main).input_from(r1).output_to(wtr_main);
    connect(wtr_r2_main).input_from(r2).output_to(wtr_main);
    connect(wtr_main).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);

    wtr_r1_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00011);
    wtr_r2_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00012);
    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00013);

    // Handle objects between main tunnel and reservoir (should be the two tunnels forming a junction into main)
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
    api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
    auto plant = api.api.create<shop_power_plant>("plant");
    api.emitter.handle_plant_input(*wtr_main, plant);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 3);
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_main));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r1_main));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r2_main));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 3);
  }
  SUBCASE("plant_input_topology_03") {
    // Junction into junction (junction gates into junction when gates are added):
    //
    // R  R R  R
    //  \/  \ /
    //    \ /  <-- tunnels  (with two inputs each)
    //     | <-- main tunnel (with two inputs)
    //     | <-- (penstock)
    //     A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
    auto r3 = builder.create_reservoir(id++, "reservoir 3"s, ""s);
    auto r4 = builder.create_reservoir(id++, "reservoir 4"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto wtr_r1_out = builder.create_tunnel(id++, "waterroute reservoir 1 output tunnel"s, ""s);
    auto wtr_r2_out = builder.create_tunnel(id++, "waterroute reservoir 2 output tunnel"s, ""s);
    auto wtr_r3_out = builder.create_tunnel(id++, "waterroute reservoir 3 output tunnel"s, ""s);
    auto wtr_r4_out = builder.create_tunnel(id++, "waterroute reservoir 4 output tunnel"s, ""s);
    auto wtr_r1_r2 = builder.create_tunnel(id++, "waterroute reservoir 1 and 2 junction"s, ""s);
    auto wtr_r3_r4 = builder.create_tunnel(id++, "waterroute reservoir 3 and 4 junction"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);

    connect(wtr_r1_out).input_from(r1).output_to(wtr_r1_r2);
    connect(wtr_r2_out).input_from(r2).output_to(wtr_r1_r2);
    connect(wtr_r3_out).input_from(r3).output_to(wtr_r3_r4);
    connect(wtr_r4_out).input_from(r4).output_to(wtr_r3_r4);

    connect(wtr_main).input_from(wtr_r1_r2);
    connect(wtr_main).input_from(wtr_r3_r4);
    connect(wtr_main).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);

    wtr_r1_out->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00011);
    wtr_r2_out->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00012);
    wtr_r3_out->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00013);
    wtr_r4_out->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00014);
    wtr_r1_r2->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00015);
    wtr_r3_r4->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00016);
    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00017);

    // Handle objects between main tunnel and reservoir
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(r1, api.adapter.to_shop(*r1));
    api.emitter.objects.add(r2, api.adapter.to_shop(*r2));
    api.emitter.objects.add(r3, api.adapter.to_shop(*r3));
    api.emitter.objects.add(r4, api.adapter.to_shop(*r4));
    auto plant = api.api.create<shop_power_plant>("plant");
    api.emitter.handle_plant_input(*wtr_main, plant);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 7);
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_main));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r1_r2));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r3_r4));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r1_out));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r2_out));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r3_out));
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_r4_out));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 4);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 7);
  }
}

TEST_CASE("stm/shop/tailrace_topology") {

  SUBCASE("tailrace_topology_01") {
    // A
    // | <-- tailrace
    // R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
  }

  SUBCASE("tailrace_topology_02") {
    // A A
    //  | <-- tailrace
    //  R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);
    connect(wtr_tailrace).input_from(g2);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
  }

  SUBCASE("tailrace_topology_03") {
    // A
    // \ A <-- draft tube A1
    //   | <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).output_to(wtr_tailrace);
    connect(wtr_tailrace).input_from(g2).output_to(rsv);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
  }

  SUBCASE("tailrace_topology_04") {
    //   A
    // A | <-- draft tube A2
    //   | <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    auto wtr_draft_tube_2 = builder.create_tunnel(id++, "waterroute draft tube 2"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);
    connect(wtr_draft_tube_2).input_from(g2).output_to(wtr_tailrace);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_2));
  }

  SUBCASE("tailrace_topology_05") {
    // A A A
    // \ | / <-- draft tubes
    //   |   <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_draft_tube_2 = builder.create_tunnel(id++, "waterroute draft tube 2"s, ""s);
    auto wtr_draft_tube_3 = builder.create_tunnel(id++, "waterroute draft tube 3"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).output_to(wtr_tailrace);
    connect(wtr_draft_tube_2).input_from(g2).output_to(wtr_tailrace);
    connect(wtr_draft_tube_3).input_from(g3).output_to(wtr_tailrace);
    connect(wtr_tailrace).output_to(rsv);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_2));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_3));
  }

  SUBCASE("tailrace_topology_06") {
    //   A A
    // A | / <-- draft tubes A2 and A3
    //   |   <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_2 = builder.create_tunnel(id++, "waterroute draft tube 2"s, ""s);
    auto wtr_draft_tube_3 = builder.create_tunnel(id++, "waterroute draft tube 3"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);
    connect(wtr_draft_tube_2).input_from(g2).output_to(wtr_tailrace);
    connect(wtr_draft_tube_3).input_from(g3).output_to(wtr_tailrace);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_2));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_3));
  }

  SUBCASE("tailrace_topology_07") {
    // A A
    // \ | A <-- draft tubes A1 and A2
    //   |   <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_draft_tube_2 = builder.create_tunnel(id++, "waterroute draft tube 2"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).output_to(wtr_tailrace);
    connect(wtr_draft_tube_2).input_from(g2).output_to(wtr_tailrace);
    connect(wtr_tailrace).input_from(g3).output_to(rsv);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_2));
  }

  SUBCASE("tailrace_topology_08") {
    //   A
    // A | A <-- draft tubes A2
    //   |   <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_2 = builder.create_tunnel(id++, "waterroute draft tube 2"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);
    connect(wtr_draft_tube_2).input_from(g2).output_to(wtr_tailrace);
    connect(wtr_tailrace).input_from(g3);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_2));
  }

  SUBCASE("tailrace_topology_09") {
    // A   A
    // \ A / <-- draft tubes A1 and A3
    //   |   <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_draft_tube_3 = builder.create_tunnel(id++, "waterroute draft tube 3"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).output_to(wtr_tailrace);
    connect(wtr_tailrace).input_from(g2).output_to(rsv);
    connect(wtr_draft_tube_3).input_from(g3).output_to(wtr_tailrace);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_3));
  }

  SUBCASE("tailrace_topology_10") {
    // A   A
    // \ A / <-- draft tubes A1 and A3
    //   |   <-- tailrace
    //   |
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_draft_tube_3 = builder.create_tunnel(id++, "waterroute draft tube 3"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto wtr_extra = builder.create_tunnel(id++, "waterroute extra"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).output_to(wtr_tailrace);
    connect(wtr_tailrace).input_from(g2).output_to(wtr_extra);
    connect(wtr_draft_tube_3).input_from(g3).output_to(wtr_tailrace);
    connect(wtr_extra).output_to(rsv);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(!shop_emitter::is_plant_outlet(*wtr_extra));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_3));
  }

  SUBCASE("tailrace_topology_11") {
    // A   A
    // \ A /   B   <-- draft tubes A1 and A3
    //    \   /  <-- tailrace
    //      |
    //      R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto ga1 = builder.create_unit(id++, "A1"s, ""s);
    auto ga2 = builder.create_unit(id++, "A2"s, ""s);
    auto ga3 = builder.create_unit(id++, "A3"s, ""s);
    auto psa = builder.create_power_plant(id++, "Plant A"s, ""s);
    power_plant::add_unit(psa, ga1);
    power_plant::add_unit(psa, ga2);
    power_plant::add_unit(psa, ga3);
    auto gb1 = builder.create_unit(id++, "B1"s, ""s);
    auto psb = builder.create_power_plant(id++, "Plant B"s, ""s);
    power_plant::add_unit(psb, gb1);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_draft_tube_3 = builder.create_tunnel(id++, "waterroute draft tube 3"s, ""s);
    auto wtr_tailrace_a = builder.create_tunnel(id++, "waterroute tailrace A"s, ""s);
    auto wtr_tailrace_b = builder.create_tunnel(id++, "waterroute tailrace B"s, ""s);
    auto wtr_extra = builder.create_tunnel(id++, "waterroute extra"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(ga1).output_to(wtr_tailrace_a);
    connect(wtr_tailrace_a).input_from(ga2).output_to(wtr_extra);
    connect(wtr_draft_tube_3).input_from(ga3).output_to(wtr_tailrace_a);
    connect(wtr_tailrace_b).input_from(gb1).output_to(wtr_extra);
    connect(wtr_extra).output_to(rsv);

    auto tailrace_a = shop_emitter::get_tailrace(*psa);
    CHECK(tailrace_a == wtr_tailrace_a);
    auto tailrace_b = shop_emitter::get_tailrace(*psb);
    CHECK(tailrace_b == wtr_tailrace_b);
    CHECK(!shop_emitter::is_plant_outlet(*wtr_extra));
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace_a));
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace_b));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace_a));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace_b));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_3));
  }

  SUBCASE("tailrace_topology_12") {
    // A A
    //  \  A  <-- common draft tube for A1 and A2
    //   |    <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1/2"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).input_from(g2).output_to(wtr_tailrace);
    connect(wtr_tailrace).input_from(g3).output_to(rsv);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
  }

  SUBCASE("tailrace_topology_13") {
    //   A A
    // A  /  <-- common draft tube for A2 and A3
    //   |   <-- tailrace
    //   R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 2/3"s, ""s);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);
    connect(wtr_draft_tube_1).input_from(g2).input_from(g3).output_to(wtr_tailrace);

    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == wtr_tailrace);
    CHECK(shop_emitter::is_plant_tailrace(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_tailrace));
    CHECK(shop_emitter::is_plant_outlet(*wtr_draft_tube_1));
  }

  SUBCASE("tailrace_topology_14") {
    // Illegal topology:
    //
    // A   A  A  A
    //  \  /  \  /
    //    \   /
    //      |
    //      R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
    auto g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
    auto g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    power_plant::add_unit(ps, g2);
    power_plant::add_unit(ps, g3);
    power_plant::add_unit(ps, g4);
    auto wtr_draft_tube_1 = builder.create_tunnel(id++, "waterroute draft tube 1"s, ""s);
    auto wtr_draft_tube_2 = builder.create_tunnel(id++, "waterroute draft tube 2"s, ""s);
    auto wtr_draft_tube_3 = builder.create_tunnel(id++, "waterroute draft tube 3"s, ""s);
    auto wtr_draft_tube_4 = builder.create_tunnel(id++, "waterroute draft tube 4"s, ""s);
    auto wtr_draft_tube_1_2 = builder.create_tunnel(id++, "waterroute draft tube junction 1+2"s, ""s);
    auto wtr_draft_tube_3_4 = builder.create_tunnel(id++, "waterroute draft tube junction 3+4"s, ""s);
    auto wtr_not_really_tailrace = builder.create_tunnel(id++, "waterroute not really tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_draft_tube_1).input_from(g1).output_to(wtr_draft_tube_1_2);
    connect(wtr_draft_tube_2).input_from(g2).output_to(wtr_draft_tube_1_2);
    connect(wtr_draft_tube_3).input_from(g3).output_to(wtr_draft_tube_3_4);
    connect(wtr_draft_tube_4).input_from(g4).output_to(wtr_draft_tube_3_4);

    connect(wtr_not_really_tailrace).input_from(wtr_draft_tube_1_2).input_from(wtr_draft_tube_3_4).output_to(rsv);
    auto tailrace = shop_emitter::get_tailrace(*ps);
    CHECK(tailrace == nullptr);
    CHECK(!shop_emitter::is_plant_tailrace(*wtr_not_really_tailrace));
    CHECK(!shop_emitter::is_plant_outlet(*wtr_not_really_tailrace));
    CHECK(!shop_emitter::is_plant_outlet(*wtr_draft_tube_1_2)); // Currently returns false, could perhaps return true?
    CHECK(!shop_emitter::is_plant_outlet(*wtr_draft_tube_1));   // Currently returns false, could perhaps return true?
  }
}

TEST_CASE("stm/shop/plant_output_topology") {

  SUBCASE("plant_output_topology_01") {
    // A
    // | <-- tailrace
    // R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    connect(wtr_tailrace).input_from(g1).output_to(rsv);

    // Handle objects between tailrace and reservoir (should be nothing)
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*rsv, api.adapter.to_shop(*rsv));
    auto plant = api.api.create<shop_power_plant>("plant"); // Just needs a shop object to connect tunnels into
    api.emitter.handle_plant_output(plant, *wtr_tailrace);  // Look downstream from tailrace

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 0);
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.exists<shop_reservoir>(rsv));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 0);
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 1);
  }

  SUBCASE("plant_output_topology_02") {
    // A
    // | <-- tailrace
    // | <-- river
    // R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto wtr_tunnel_to_rsv = builder.create_tunnel(id++, "waterroute tunnel to reservoir"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

    connect(wtr_tailrace).input_from(g1).output_to(wtr_tunnel_to_rsv);
    connect(wtr_tunnel_to_rsv).output_to(rsv);

    // Handle objects between tailrace and reservoir (should be nothing)
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*rsv, api.adapter.to_shop(*rsv));
    auto plant = api.api.create<shop_power_plant>("plant"); // Just needs a shop object to connect tunnels into
    api.emitter.handle_plant_output(plant, *wtr_tailrace);  // Look downstream from tailrace

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 0);
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.exists<shop_reservoir>(rsv));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 0);
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 1);
  }

  SUBCASE("plant_output_topology_03") {
    // A
    // | <-- tailrace
    // | <-- tunnel
    // R
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    auto wtr_tunnel_to_rsv = builder.create_tunnel(id++, "waterroute tunnel to reservoir"s, ""s);
    auto rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

    wtr_tunnel_to_rsv->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    connect(wtr_tailrace).input_from(g1).output_to(wtr_tunnel_to_rsv);
    connect(wtr_tunnel_to_rsv).output_to(rsv);

    // Handle objects between tailrace and reservoir (should be 1 tunnel)
    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*rsv, api.adapter.to_shop(*rsv));
    auto plant = api.api.create<shop_power_plant>("plant"); // Just needs a shop object to connect tunnels into
    api.emitter.handle_plant_output(plant, *wtr_tailrace);  // Look downstream from tailrace

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.exists<shop_reservoir>(rsv));

    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 1);
  }
}

TEST_CASE("stm/shop/reservoir_output_topology") {

  SUBCASE("reservoir_output_topology_01") {
    //                R1
    //    bypass --> / | <-- main tunnel
    //    (river)   |  | <-- penstock
    //  (1 segment) |  A
    //               \ |
    //                R2
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    auto wtr_bypass = builder.create_tunnel(id++, "waterroute bypass"s, ""s);

    connect(wtr_main).input_from(r1).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);
    connect(g1).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(r2);
    connect(wtr_bypass).input_from(r1, hydro_power::connection_role::bypass).output_to(r2);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
    api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));

    api.emitter.handle_reservoir_output(*r1);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
    CHECK(api.emitter.objects.size<shop_river>() == 1);
    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
    CHECK(count_shop_api_objects<shop_river>(api.api) == 1);
  }

  SUBCASE("reservoir_output_topology_02") {
    //                R1
    //    bypass --> / | <-- main tunnel
    //   (tunnel)   |  | <-- penstock
    //  (1 segment) |  A
    //               \ |
    //                R2
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    auto wtr_bypass = builder.create_tunnel(id++, "waterroute bypass"s, ""s);

    connect(wtr_main).input_from(r1).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);
    connect(g1).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(r2);
    connect(wtr_bypass).input_from(r1, hydro_power::connection_role::bypass).output_to(r2);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);
    wtr_bypass->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
    api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
    api.emitter.handle_reservoir_output(*r1);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass));
    CHECK(api.emitter.objects.size<shop_gate>() == 0);
    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_gate>(api.api) == 0);
  }

  SUBCASE("reservoir_output_topology_03") {
    //                R1
    //    bypass --> / | <-- main tunnel
    //   (tunnel)   |  | <-- penstock
    // (3 segments) |  A
    //               \ |
    //                R2
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    auto wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    auto wtr_bypass1 = builder.create_tunnel(id++, "waterroute bypass 1"s, ""s);
    auto wtr_bypass2 = builder.create_tunnel(id++, "waterroute bypass 2"s, ""s);
    auto wtr_bypass3 = builder.create_tunnel(id++, "waterroute bypass 3"s, ""s);

    connect(wtr_main).input_from(r1).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);
    connect(g1).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(r2);
    connect(wtr_bypass1).input_from(r1, hydro_power::connection_role::bypass).output_to(wtr_bypass2);
    connect(wtr_bypass2).output_to(wtr_bypass3);
    connect(wtr_bypass3).output_to(r2);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);
    wtr_bypass1->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    // No stm gate
    {
      shop_system api{time_axis};
      api.set_logging_to_stdstreams(shop_console_output);
      api.set_logging_to_files(shop_log_files);
      api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
      api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
      api.emitter.handle_reservoir_output(*r1);

      // Check emitted objects (what our code have sent to shop)
      CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
      CHECK(api.emitter.objects.size<shop_tunnel>() == 3);
      CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass1));
      CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass2));
      CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass3));
      CHECK(api.emitter.objects.size<shop_gate>() == 0);
      // Check shop api objects (ask shop what it actually contains)
      CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
      CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 3);
      CHECK(count_shop_api_objects<shop_gate>(api.api) == 0);
    }
    // Add one stm gate, but in shop it is still represented by a tunnel
    {
      auto wtr_bypass1_gt = builder.create_gate(id++, "waterroute bypass 1 gate 1", "");
      waterway::add_gate(wtr_bypass1, wtr_bypass1_gt);
      wtr_bypass1_gt->discharge.schedule = apoint_ts{
        point_dt{{t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_end}},
        {48.0, 40.0, 0.0},
        shyft::time_series::POINT_AVERAGE_VALUE
      };

      shop_system api{time_axis};
      api.set_logging_to_stdstreams(shop_console_output);
      api.set_logging_to_files(shop_log_files);
      api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
      api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
      api.emitter.handle_reservoir_output(*r1);

      // Check emitted objects (what our code have sent to shop)
      CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
      CHECK(api.emitter.objects.size<shop_tunnel>() == 3);
      CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass1));
      CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass2));
      CHECK(api.emitter.objects.exists<shop_tunnel>(wtr_bypass3));
      CHECK(api.emitter.objects.size<shop_gate>() == 0);
      // Check shop api objects (ask shop what it actually contains)
      CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
      CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 3);
      CHECK(count_shop_api_objects<shop_gate>(api.api) == 0);
    }
  }

  SUBCASE("reservoir_output_topology_04") {
    // R1
    // | <-- river with/without gate
    // R2
    // | <-- main
    // | <-- penstock
    // A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_r1_r2 = builder.create_river(id++, "waterroute reservoir 1-2 river"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    connect(wtr_r1_r2).input_from(r1).output_to(r2);
    connect(r2).output_to(wtr_main, hydro_power::connection_role::main);
    connect(wtr_main).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00013);

    // No stm gate, but shop emitter needs to create one representing the r1-r2 connection
    {
      shop_system api{time_axis};
      api.set_logging_to_stdstreams(shop_console_output);
      api.set_logging_to_files(shop_log_files);
      api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
      api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
      api.emitter.handle_reservoir_output(*r1);
      // Check emitted objects (what our code have sent to shop)
      CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
      CHECK(api.emitter.objects.size<shop_river>() == 1);
      // Check shop api objects (ask shop what it actually contains)
      CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
      CHECK(count_shop_api_objects<shop_river>(api.api) == 1);
    }
    // Add one stm gate, that should be used as the shop gate as well
    {
      auto wtr_r1_r2_gt = builder.create_gate(id++, "waterroute reservoir 1-2 river gate 1", "");
      waterway::add_gate(wtr_r1_r2, wtr_r1_r2_gt);
      wtr_r1_r2_gt->discharge.schedule = apoint_ts{
        point_dt{{t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_end}},
        {48.0, 40.0, 0.0},
        shyft::time_series::POINT_AVERAGE_VALUE
      };

      shop_system api{time_axis};
      api.set_logging_to_stdstreams(shop_console_output);
      api.set_logging_to_files(shop_log_files);
      api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
      api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
      api.emitter.handle_reservoir_output(*r1);
      // Check emitted objects (what our code have sent to shop)
      CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
      CHECK(api.emitter.objects.size<shop_river>() == 0);
      CHECK(api.emitter.objects.size<shop_tunnel>() == 0);
      CHECK(api.emitter.objects.size<shop_gate>() == 1);
      // Should this time be a shop_gate representing the actual stm gate, not the water route.
      CHECK(api.emitter.objects.exists<shop_gate>(wtr_r1_r2_gt));
      // Check shop api objects (ask shop what it actually contains)
      CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
      CHECK(count_shop_api_objects<shop_river>(api.api) == 0);
      CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 0);
      CHECK(count_shop_api_objects<shop_gate>(api.api) == 1);
    }
    // Add two more stm gates (parallel to the first)
    {
      auto wtr_r1_r2_gt2 = builder.create_gate(id++, "waterroute reservoir 1-2 river gate 2", "");
      waterway::add_gate(wtr_r1_r2, wtr_r1_r2_gt2);
      wtr_r1_r2_gt2->discharge.schedule = apoint_ts{
        point_dt{{t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_end}},
        {48.0, 40.0, 0.0},
        shyft::time_series::POINT_AVERAGE_VALUE
      };
      auto wtr_r1_r2_gt3 = builder.create_gate(id++, "waterroute reservoir 1-2 river gate 3", "");
      waterway::add_gate(wtr_r1_r2, wtr_r1_r2_gt3);
      wtr_r1_r2_gt3->discharge.schedule = apoint_ts{
        point_dt{{t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_end}},
        {48.0, 40.0, 0.0},
        shyft::time_series::POINT_AVERAGE_VALUE
      };

      shop_system api{time_axis};
      api.set_logging_to_stdstreams(shop_console_output);
      api.set_logging_to_files(shop_log_files);
      api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
      api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
      api.emitter.handle_reservoir_output(*r1);
      // Check emitted objects (what our code have sent to shop)
      CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
      CHECK(api.emitter.objects.size<shop_tunnel>() == 0);
      CHECK(api.emitter.objects.size<shop_gate>() == 3);
      CHECK(api.emitter.objects.exists<shop_gate>(wtr_r1_r2_gt2));
      CHECK(api.emitter.objects.exists<shop_gate>(wtr_r1_r2_gt3));
      // Check shop api objects (ask shop what it actually contains)
      CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
      CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 0);
      CHECK(count_shop_api_objects<shop_gate>(api.api) == 3);
    }
  }

  SUBCASE("reservoir_output_topology_05") {
    // R1
    // | <-- river
    // | <-- "superfluous" segment will be ignored
    // | <-- "superfluous" segment will be ignored
    // R2
    // | <-- main
    // | <-- penstock
    // A
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_r1_out1 = builder.create_river(id++, "waterroute reservoir 1 output segment 1"s, ""s);
    auto wtr_r1_out2 = builder.create_river(id++, "waterroute reservoir 1 output segment 2"s, ""s);
    auto wtr_r1_out3 = builder.create_river(id++, "waterroute reservoir 1 output segment 3"s, ""s);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
    connect(wtr_r1_out1).input_from(r1).output_to(wtr_r1_out2);
    connect(wtr_r1_out2).output_to(wtr_r1_out3);
    connect(wtr_r1_out3).output_to(r2);
    connect(r2).output_to(wtr_main, hydro_power::connection_role::main);
    connect(wtr_main).output_to(wtr_penstock1);
    connect(wtr_penstock1).output_to(g1);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00013);

    {
      shop_system api{time_axis};
      api.set_logging_to_stdstreams(shop_console_output);
      api.set_logging_to_files(shop_log_files);
      api.emitter.objects.add(*r1, api.adapter.to_shop(*r1));
      api.emitter.objects.add(*r2, api.adapter.to_shop(*r2));
      api.emitter.handle_reservoir_output(*r1);
      // Check emitted objects (what our code have sent to shop)
      CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
      CHECK(api.emitter.objects.size<shop_river>() == 1);
      // Check shop api objects (ask shop what it actually contains)
      CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
      CHECK(count_shop_api_objects<shop_river>(api.api) == 1);
    }
  }
}

TEST_CASE("stm/shop/time_delay_topology") {

  auto shop_scale_river = shyft::core::to_seconds(shyft::core::seconds(1));

  auto shop_scale_default = [](auto const & shop_sys) {
    return shyft::core::to_seconds(shop_sys.adapter.time_delay_unit);
  };

  auto compare_time_delay_xy = [](auto const & stm_xy, auto const & shop_xy, auto shop_scale_x) {
    // Compare time delay xy input from stm with output from shop,
    // considering scaling of time delay units performed by adapter
    // on emit (seconds -> minutes).
    if (shop_scale_x == 0)
      shop_scale_x = 1;
    REQUIRE(stm_xy.points.size() == shop_xy.points.size());
    for (size_t i = 0; i < stm_xy.points.size(); ++i) {
      CHECK_EQ(doctest::Approx(stm_xy.points[i].x / shop_scale_x), shop_xy.points[i].x);
      CHECK_EQ(stm_xy.points[i].y, shop_xy.points[i].y);
    }
  };

  // Note: Basic delay attribute handling is tested in shop_adapter tests (shop_gate_delay).
  SUBCASE("time_delay_topology_01") {
    //                R1
    //    flood  --> / | \ <-- bypass
    //   (river)    |  |  |   (river)
    //              |  A  |
    //               \ | /
    //                 | <-- common river with delay
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    auto wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    auto wtr_flood1 = builder.create_river(id++, "waterroute flood segment 1"s, ""s);
    auto wtr_flood2 = builder.create_river(id++, "waterroute flood segment 2"s, ""s);
    auto wtr_flood3 = builder.create_river(id++, "waterroute flood segment 3"s, ""s);
    auto wtr_bypass1 = builder.create_river(id++, "waterroute bypass segment 1"s, ""s);
    auto wtr_bypass2 = builder.create_river(id++, "waterroute bypass segment 2"s, ""s);
    auto wtr_bypass3 = builder.create_river(id++, "waterroute bypass segment 3"s, ""s);
    auto wtr_common_downstream = builder.create_tunnel(id++, "waterroute common downstream river"s, ""s);

    connect(wtr_main).input_from(r1).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);
    connect(g1).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(wtr_common_downstream);
    connect(wtr_flood1).input_from(r1, hydro_power::connection_role::flood).output_to(wtr_flood2);
    connect(wtr_flood2).output_to(wtr_flood3);
    connect(wtr_flood3).output_to(wtr_common_downstream);
    connect(wtr_bypass1).input_from(r1, hydro_power::connection_role::bypass).output_to(wtr_bypass2);
    connect(wtr_bypass2).output_to(wtr_bypass3);
    connect(wtr_bypass3).output_to(wtr_common_downstream);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    auto const txy = make_shared<std::map<utctime, xy_point_curve_>>();
    auto const xy = make_shared<hydro_power::xy_point_curve>(
      hydro_power::xy_point_curve::make({0.0, 1.0}, {60.0, 1.0}));
    txy.get()->emplace(t_begin, xy);
    wtr_common_downstream->delay = txy;

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.emit(*r1);
    api.emitter.emit(*ps);
    api.emitter.handle_reservoir_output(*r1);
    api.emitter.handle_time_delay(*wtr_common_downstream);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.size<shop_river>() == 2);
    CHECK(api.emitter.objects.exists<shop_river>(wtr_flood1));
    CHECK(api.emitter.objects.exists<shop_river>(wtr_bypass1));
    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_river>(api.api) == 2);

    {
      auto it = api.emitter.objects.find<shop_gate>(wtr_flood1.get());
      REQUIRE_EQ(it, api.emitter.objects.end<shop_gate>());
    }
    {
      auto it = api.emitter.objects.find<shop_river>(wtr_flood1.get());
      REQUIRE(it->second.time_delay_curve.exists());
      REQUIRE(it->second.time_delay_curve.get().size() == 1);
      compare_time_delay_xy(*xy, it->second.time_delay_curve.get().back().xy_curve, shop_scale_river);
    }

    CHECK_FALSE(api.emitter.objects.exists<shop_gate>(wtr_flood2.get()));
    CHECK_FALSE(api.emitter.objects.exists<shop_gate>(wtr_flood3.get()));

    {
      auto it = api.emitter.objects.find<shop_gate>(wtr_bypass1.get());
      REQUIRE_EQ(it, api.emitter.objects.end<shop_gate>());
    }
    {
      auto it = api.emitter.objects.find<shop_river>(wtr_bypass1.get());
      REQUIRE(it->second.time_delay_curve.exists());
      REQUIRE(it->second.time_delay_curve.get().size() == 1);
      compare_time_delay_xy(*xy, it->second.time_delay_curve.get().back().xy_curve, shop_scale_river);
    }

    CHECK_FALSE(api.emitter.objects.exists<shop_gate>(wtr_bypass2.get()));
    CHECK_FALSE(api.emitter.objects.exists<shop_gate>(wtr_bypass3.get()));

    {
      auto it2 = api.emitter.objects.find<shop_power_plant>(ps.get());
      REQUIRE_NE(it2, api.emitter.objects.end<shop_power_plant>());
      REQUIRE(it2->second.shape_discharge.exists());
      compare_time_delay_xy(*xy, it2->second.shape_discharge.get().xy_curve, shop_scale_default(api));
    }
  }

  SUBCASE("time_delay_topology_02") {
    //                R1
    //    flood  --> / | \ <-- bypass
    //   (river)    |  |  |
    //              |  A  R2
    //               \ | / <-- main
    //                 | <-- common river with delay
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up 1"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir up 2"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    auto wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    auto wtr_flood1 = builder.create_river(id++, "waterroute flood segment 1"s, ""s);
    auto wtr_flood2 = builder.create_river(id++, "waterroute flood segment 2"s, ""s);
    auto wtr_flood3 = builder.create_river(id++, "waterroute flood segment 3"s, ""s);
    auto wtr_bypass_r1_1 = builder.create_river(id++, "waterroute bypass 1"s, ""s);
    auto wtr_bypass_r1_2 = builder.create_river(id++, "waterroute bypass 2"s, ""s);
    auto wtr_bypass_r2_1 = builder.create_river(id++, "waterroute bypass segment 3"s, ""s);
    auto wtr_common_downstream = builder.create_tunnel(id++, "waterroute common downstream river"s, ""s);

    connect(wtr_main).input_from(r1).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);
    connect(g1).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(wtr_common_downstream);
    connect(wtr_flood1).input_from(r1, hydro_power::connection_role::flood).output_to(wtr_flood2);
    connect(wtr_flood2).output_to(wtr_flood3);
    connect(wtr_flood3).output_to(wtr_common_downstream);
    connect(wtr_bypass_r1_1).input_from(r1, hydro_power::connection_role::bypass).output_to(wtr_bypass_r1_2);
    connect(wtr_bypass_r1_2).output_to(r2);
    connect(wtr_bypass_r2_1).input_from(r2).output_to(wtr_common_downstream);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    auto const txy = make_shared<std::map<utctime, xy_point_curve_>>();
    auto const xy = make_shared<hydro_power::xy_point_curve>(
      hydro_power::xy_point_curve::make({0.0, 1.0}, {60.0, 1.0}));
    txy.get()->emplace(t_begin, xy);
    wtr_common_downstream->delay = txy;

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.emit(*r1);
    api.emitter.emit(*r2);
    api.emitter.emit(*ps);
    api.emitter.handle_reservoir_output(*r1);
    api.emitter.handle_reservoir_output(*r2);
    api.emitter.handle_time_delay(*wtr_common_downstream);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.size<shop_river>() == 3);
    CHECK(api.emitter.objects.size<shop_gate>() == 0);
    CHECK(api.emitter.objects.exists<shop_river>(wtr_flood1));
    CHECK(api.emitter.objects.exists<shop_river>(wtr_bypass_r2_1));
    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_river>(api.api) == 3);
    CHECK(count_shop_api_objects<shop_gate>(api.api) == 0);

    {
      auto it = api.emitter.objects.find<shop_river>(wtr_flood1.get());
      REQUIRE_NE(it, api.emitter.objects.end<shop_river>());
      REQUIRE(it->second.time_delay_curve.exists());
      REQUIRE(it->second.time_delay_curve.get().size() == 1);
      compare_time_delay_xy(*xy, it->second.time_delay_curve.get().back().xy_curve, shop_scale_river);
    }

    CHECK_FALSE(api.emitter.objects.exists<shop_river>(wtr_flood2.get()));
    CHECK_FALSE(api.emitter.objects.exists<shop_river>(wtr_flood3.get()));

    {
      auto it = api.emitter.objects.find<shop_river>(wtr_bypass_r2_1.get());
      REQUIRE_NE(it, api.emitter.objects.end<shop_river>());
      REQUIRE(it->second.time_delay_curve.exists());
      REQUIRE(it->second.time_delay_curve.get().size() == 1);
      compare_time_delay_xy(*xy, it->second.time_delay_curve.get().back().xy_curve, shop_scale_river);
    }

    CHECK_FALSE(api.emitter.objects.exists<shop_river>(wtr_bypass_r1_2.get()));
    {
      auto it = api.emitter.objects.find<shop_power_plant>(ps.get());
      REQUIRE_NE(it, api.emitter.objects.end<shop_power_plant>());
      REQUIRE(it->second.shape_discharge.exists());
      compare_time_delay_xy(*xy, it->second.shape_discharge.get().xy_curve, shop_scale_default(api));
    }
  }

  SUBCASE("time_delay_topology_03") {
    //                R1
    //    flood  --> / | \ <-- bypass
    //   (river)    |  |  |
    // (with delay) |  A  R2
    //               \ | / <-- main
    //                 | <-- common river with delay
    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);
    auto r1 = builder.create_reservoir(id++, "reservoir up 1"s, ""s);
    auto r2 = builder.create_reservoir(id++, "reservoir up 2"s, ""s);
    auto g1 = builder.create_unit(id++, "aggregate"s, ""s);
    auto ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, g1);
    auto wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
    auto wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    auto wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    auto wtr_flood1 = builder.create_river(id++, "waterroute flood segment 1"s, ""s);
    auto wtr_flood2 = builder.create_river(id++, "waterroute flood segment 2"s, ""s);
    auto wtr_flood3 = builder.create_river(id++, "waterroute flood segment 3"s, ""s);
    auto wtr_bypass_r1_1 = builder.create_river(id++, "waterroute bypass 1"s, ""s);
    auto wtr_bypass_r1_2 = builder.create_river(id++, "waterroute bypass 2"s, ""s);
    auto wtr_bypass_r2_1 = builder.create_river(id++, "waterroute bypass segment 3"s, ""s);
    auto wtr_common_downstream = builder.create_tunnel(id++, "waterroute common downstream river"s, ""s);

    connect(wtr_main).input_from(r1).output_to(wtr_penstock);
    connect(wtr_penstock).output_to(g1);
    connect(g1).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(wtr_common_downstream);
    connect(wtr_flood1).input_from(r1, hydro_power::connection_role::flood).output_to(wtr_flood2);
    connect(wtr_flood2).output_to(wtr_flood3);
    connect(wtr_flood3).output_to(wtr_common_downstream);
    connect(wtr_bypass_r1_1).input_from(r1, hydro_power::connection_role::bypass).output_to(wtr_bypass_r1_2);
    connect(wtr_bypass_r1_2).output_to(r2);
    connect(wtr_bypass_r2_1).input_from(r2).output_to(wtr_common_downstream);

    wtr_main->head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030);

    auto const delay_txy_flood1 = make_shared<std::map<utctime, xy_point_curve_>>();
    auto const delay_xy_flood1 = make_shared<hydro_power::xy_point_curve>(
      hydro_power::xy_point_curve::make({0.0, 1.0}, {120.0, 1.0}));
    delay_txy_flood1.get()->emplace(t_begin, delay_xy_flood1);
    wtr_flood1->delay = delay_txy_flood1;

    auto const delay_txy_common = make_shared<std::map<utctime, xy_point_curve_>>();
    auto const delay_xy_common = make_shared<hydro_power::xy_point_curve>(
      hydro_power::xy_point_curve::make({0.0, 1.0}, {60.0, 1.0}));
    delay_txy_common.get()->emplace(t_begin, delay_xy_common);
    wtr_common_downstream->delay = delay_txy_common;

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.emit(*r1);
    api.emitter.emit(*r2);
    api.emitter.emit(*ps);
    api.emitter.handle_reservoir_output(*r1);
    api.emitter.handle_reservoir_output(*r2);
    api.emitter.handle_time_delay(*wtr_common_downstream);

    // Check emitted objects (what our code have sent to shop)
    CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.size<shop_river>() == 3);
    CHECK(api.emitter.objects.size<shop_gate>() == 0);
    CHECK(api.emitter.objects.exists<shop_river>(wtr_flood1));
    CHECK(api.emitter.objects.exists<shop_river>(wtr_bypass_r2_1));
    // Check shop api objects (ask shop what it actually contains)
    CHECK(count_shop_api_objects<shop_reservoir>(api.api) == 2);
    CHECK(count_shop_api_objects<shop_tunnel>(api.api) == 1);
    CHECK(count_shop_api_objects<shop_river>(api.api) == 3);
    CHECK(count_shop_api_objects<shop_gate>(api.api) == 0);

    {
      auto it = api.emitter.objects.find<shop_river>(wtr_flood1.get());
      REQUIRE_NE(it, api.emitter.objects.end<shop_river>());
      REQUIRE(it->second.time_delay_curve.exists());
      REQUIRE(it->second.time_delay_curve.get().size() == 1);
      compare_time_delay_xy(*delay_xy_flood1, it->second.time_delay_curve.get().back().xy_curve, shop_scale_river);
    }

    CHECK_FALSE(api.emitter.objects.exists<shop_river>(wtr_flood2.get()));
    CHECK_FALSE(api.emitter.objects.exists<shop_river>(wtr_flood3.get()));
    {
      auto it = api.emitter.objects.find<shop_river>(wtr_bypass_r2_1.get());
      REQUIRE_NE(it, api.emitter.objects.end<shop_river>());
      REQUIRE(it->second.time_delay_curve.exists());
      REQUIRE(it->second.time_delay_curve.get().size() == 1);
      compare_time_delay_xy(*delay_xy_common, it->second.time_delay_curve.get().back().xy_curve, shop_scale_river);
    }

    CHECK_FALSE(api.emitter.objects.exists<shop_river>(wtr_bypass_r1_2.get()));
    {
      auto it = api.emitter.objects.find<shop_power_plant>(ps.get());
      REQUIRE_NE(it, api.emitter.objects.end<shop_power_plant>());
      REQUIRE(it->second.shape_discharge.exists());
      compare_time_delay_xy(*delay_xy_common, it->second.shape_discharge.get().xy_curve, shop_scale_default(api));
    }
  }
}

TEST_CASE("stm/shop/discharge_group") {
  SUBCASE("handle_discharge_group_simple") {
    using shyft::energy_market::hydro_power::connect;
    using shyft::energy_market::stm::shop::shop_emitter;

    // Check that spill flow description ('flow_descr' in SHOP)  is set correctly
    // on the SHOP reservoir created by the adapter.
    auto hps = make_shared<stm_hps>(0, "test_hps"s);
    auto builder = stm_hps_builder(hps);

    // Create custom stm system
    //   R(0)
    //    |
    //   W(0)
    auto rsv = builder.create_reservoir(0, "reservoir", "");
    auto wtr = builder.create_waterway(0, "waterway", "");
    connect(wtr).input_from(rsv);

    stm_system stm(0, "stm0", "");
    stm.hps.push_back(hps);

    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    const size_t n_step = 18;
    const generic_dt time_axis{t_begin, t_step, n_step};
    const shyft::core::utcperiod t_period{time_axis.total_period()};
    auto const t_end = t_period.end;

    wtr->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    wtr->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    wtr->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);

    // Check that no discharge group will be created
    {
      auto shop_sys = create_shop_system(hps);
      shop_sys->emitter.handle_discharge_group(*wtr);
      auto sd = shop_sys->emitter.objects.get<shyft::energy_market::stm::shop::shop_discharge_group>(wtr.get());
      int nRelations{-999};
      int relatedIndexList[100]{};
      CHECK(ShopGetRelations(shop_sys->api.c, sd.id, "connection_standard", nRelations, &relatedIndexList[0]));
      CHECK(nRelations == 0);
    }

    // Add gate and check that a discharge group with only this gate is created
    {
      auto shop_sys = create_shop_system(hps);
      auto gt = builder.create_gate(0, "gate", "");
      wtr->add_gate(wtr, gt);
      shop_sys->emitter.objects.add(gt, shop_sys->adapter.to_shop_gate(*wtr, gt.get()));
      shop_sys->emitter.handle_discharge_group(*wtr);
      auto sd = shop_sys->emitter.objects.get<shyft::energy_market::stm::shop::shop_discharge_group>(wtr.get());
      auto sg = shop_sys->emitter.objects.get<shyft::energy_market::stm::shop::shop_gate>(gt.get());
      int nRelations{-999};
      int relatedIndexList[100]{};
      CHECK(ShopGetRelations(shop_sys->api.c, sd.id, "connection_standard", nRelations, &relatedIndexList[0]));
      CHECK(nRelations == 1);
      CHECK(relatedIndexList[0] == sg.id);
    }
  }

  SUBCASE("handle_discharge_group_advanced") {
    using shyft::energy_market::hydro_power::connect;
    using shyft::energy_market::stm::shop::shop_emitter;

    // Check that spill flow description ('flow_descr' in SHOP)  is set correctly
    // on the SHOP reservoir created by the adapter.
    auto hps = make_shared<stm_hps>(0, "test_hps"s);
    auto builder = stm_hps_builder(hps);

    // Create custom stm system
    //                      P(0)
    //                       ||
    // R(0) = W(0) = W(1) = U(0) = W(2)
    //  |                          |
    //  |                          W(3) = R(1)
    //  W(4) = = = = = = = = = = = |
    //    W(4) has gate

    // Route 1
    auto unit0 = builder.create_unit(0, "unit0", "");
    auto ppl0 = builder.create_power_plant(0, "powerstation0", "");
    ppl0->add_unit(ppl0, unit0);

    auto rsv0 = builder.create_reservoir(0, "reservoir0", "");
    auto rsv1 = builder.create_reservoir(1, "reservoir1", "");
    auto wtr0 = builder.create_waterway(0, "wtr0", "");
    auto wtr1 = builder.create_waterway(1, "wtr1", "");
    auto wtr2 = builder.create_waterway(2, "wtr2", "");
    auto wtr3 = builder.create_waterway(3, "wtr3", "");

    connect(wtr0).input_from(rsv0).output_to(wtr1);
    connect(wtr1).output_to(unit0);
    connect(wtr2).input_from(unit0).output_to(wtr3);
    connect(wtr3).output_to(rsv1);

    // Route 2
    auto wtr4 = builder.create_waterway(4, "wtr4", "");
    auto gate0 = builder.create_gate(0, "gate0", "");
    wtr4->add_gate(wtr4, gate0);
    connect(wtr4).input_from(rsv0).output_to((wtr3));

    auto shop_sys = create_shop_system(hps);

    stm_system stm(0, "stm0", "");
    stm.hps.push_back(hps);

    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    const size_t n_step = 18;
    const generic_dt time_axis{t_begin, t_step, n_step};
    const shyft::core::utcperiod t_period{time_axis.total_period()};
    auto const t_end = t_period.end;

    wtr3->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    wtr3->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    wtr3->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);

    shop_sys->emitter.objects.add(ppl0, shop_sys->adapter.to_shop(*ppl0));
    shop_sys->emitter.objects.add(gate0, shop_sys->adapter.to_shop_gate(*wtr4, gate0.get()));
    shop_sys->emitter.handle_discharge_group(*wtr3);

    auto sd = shop_sys->emitter.objects.get<shyft::energy_market::stm::shop::shop_discharge_group>(wtr3.get());
    auto sg = shop_sys->emitter.objects.get<shyft::energy_market::stm::shop::shop_gate>(gate0.get());
    auto pp = shop_sys->emitter.objects.get<shyft::energy_market::stm::shop::shop_power_plant>(ppl0.get());
    // Check all shop objects are in the same discharge group.
    int nRelations{-999};
    int relatedIndexList[100]{};
    CHECK(ShopGetRelations(shop_sys->api.c, sd.id, "connection_standard", nRelations, &relatedIndexList[0]));
    CHECK(nRelations == 2);
    CHECK(relatedIndexList[0] == pp.id);
    CHECK(relatedIndexList[1] == sg.id);
  }

  SUBCASE("emit_model_with_discharge_group") {
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    const size_t n_step = 18;
    const generic_dt time_axis{t_begin, t_step, n_step};

    const shyft::core::utcperiod t_period{time_axis.total_period()};
    auto const t_end = t_period.end;

    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    shop_system shop{time_axis, "discharge_group"};

    auto power_plant_ = std::dynamic_pointer_cast<shyft::energy_market::stm::power_plant>(
      stm->hps.front()->find_power_plant_by_name("plant"));
    auto river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute river"));
    river_->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    river_->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    river_->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);

    shop.emit(*stm);

    auto ww_buypass_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute river"));
    auto ppa_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute river"));

    auto bypass_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute bypass"));

    auto sd = shop.emitter.objects.get<shyft::energy_market::stm::shop::shop_discharge_group>(river_.get());
    auto sg = shop.emitter.objects.get<shyft::energy_market::stm::shop::shop_gate>(bypass_river_->gates.front().get());
    auto pp = shop.emitter.objects.get<shyft::energy_market::stm::shop::shop_power_plant>(power_plant_.get());
    // Check all shop objects are in the same discharge group.
    int nRelations{-999};
    int relatedIndexList[100]{};
    CHECK(ShopGetRelations(shop.api.c, sd.id, "connection_standard", nRelations, &relatedIndexList[0]));
    CHECK(nRelations == 2);
    CHECK(relatedIndexList[0] == pp.id);
    CHECK(relatedIndexList[1] == sg.id);
  } // SUBCASE
} // TEST_CASE

TEST_CASE("stm/shop/pump_topologies") {

  auto make_reversible_pump_turbine_description = [&] {
    auto t = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::turbine_description>>>();
    t->emplace(
      t_begin,
      std::make_shared<hydro_power::turbine_description>(
        hydro_power::turbine_description{.operating_zones{{.efficiency_curves{
          {.xy_curve{.points{{.x = -1.0, .y = -80.0}}}, .z = -10.0},
          {.xy_curve{.points{{.x = 1.0, .y = 80.0}}}, .z = 10.0}

        }}}}));
    return t;
  };
  auto make_pump_turbine_description = [&] {
    auto t = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::turbine_description>>>();
    t->emplace(
      t_begin,
      std::make_shared<hydro_power::turbine_description>(hydro_power::turbine_description{
        .operating_zones{{.efficiency_curves{{.xy_curve{.points{{.x = -1.0, .y = -80.0}}}, .z = -10.0}}}}}));
    return t;
  };

  SUBCASE("pumping_plant") {

    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "a");
    stm_hps_builder builder(hps);
    auto v0 = builder.create_reservoir(id++, "b", "");
    auto u0 = [&] {
      auto u0 = builder.create_unit(id++, "c", "");
      u0->turbine_description = make_pump_turbine_description();
      return u0;
    }();

    auto p = builder.create_power_plant(id++, "d", "");
    power_plant::add_unit(p, u0);
    auto t0 = builder.create_tunnel(id++, "e", ""), t1 = builder.create_tunnel(id++, "f", ""),
         t2 = builder.create_tunnel(id++, "g", "");
    auto r0 = builder.create_river(id++, "h", "");

    {
      using hc = hydro_power::hydro_component;
      hc::connect(v0, hydro_power::connection_role::main, t0);
      hc::connect(t0, t1);
      hc::connect(t1, u0);
      hc::connect(u0, t2);
      hc::connect(t2, r0);
    }

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.emit(*v0);
    api.emitter.emit(*p);
    api.emitter.handle_reservoir_output(*v0);
    CHECK(api.emitter.objects.size<shop_reservoir>() == 1);
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.size<shop_gate>() == 0);
    CHECK(api.emitter.objects.size<shop_pump>() == 1);
    CHECK(api.emitter.objects.size<shop_unit>() == 0); // shop 5.2.1 fixes this, 5.2.0 series needed shop_unit to work
  }
  SUBCASE("reversible_pumping_plant") {

    int id = 1;
    auto hps = make_shared<stm_hps>(id++, "a");
    stm_hps_builder builder(hps);
    auto v0 = builder.create_reservoir(id++, "v0", ""), v1 = builder.create_reservoir(id++, "v1", "");
    auto u0 = [&] {
      auto u0 = builder.create_unit(id++, "c", "");
      u0->turbine_description = make_reversible_pump_turbine_description();
      return u0;
    }();
    auto u1 = [&] {
      auto u1 = builder.create_unit(id++, "d", "");
      u1->turbine_description = make_reversible_pump_turbine_description();
      return u1;
    }();

    auto p = builder.create_power_plant(id++, "e", "");
    power_plant::add_unit(p, u0);
    power_plant::add_unit(p, u1);
    auto t0 = builder.create_tunnel(id++, "f", ""), t1 = builder.create_tunnel(id++, "g", ""),
         t2 = builder.create_tunnel(id++, "h", ""), t3 = builder.create_tunnel(id++, "i", "");

    {
      using hc = hydro_power::hydro_component;
      hc::connect(v0, hydro_power::connection_role::main, t0);
      hc::connect(t0, t1);
      hc::connect(t1, u0);
      hc::connect(u0, t3);
      hc::connect(t0, t2);
      hc::connect(t2, u1);
      hc::connect(u1, t3);
      hc::connect(t3, v1);
    }

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.emit(*v0);
    api.emitter.emit(*v1);
    api.emitter.emit(*p);
    api.emitter.handle_reservoir_output(*v0);
    api.emitter.handle_reservoir_output(*v1);
    CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
    CHECK(api.emitter.objects.size<shop_tunnel>() == 1);
    CHECK(api.emitter.objects.size<shop_gate>() == 0);
    CHECK(api.emitter.objects.size<shop_pump>() == 2);
    CHECK(api.emitter.objects.size<shop_unit>() == 2);
  }

  SUBCASE("shop_portal_basic_pump_example") {

    auto hps = test_models::build_shop_pump_example_hps(t_begin, t_end);

    REQUIRE(hps->reservoirs.size() == 2);
    REQUIRE(hps->power_plants.size() == 1);

    auto v0 = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    REQUIRE(v0);
    auto v1 = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[1]);
    REQUIRE(v1);
    auto p0 = std::dynamic_pointer_cast<power_plant>(hps->power_plants[0]);

    shop_system api{time_axis};
    api.set_logging_to_stdstreams(shop_console_output);
    api.set_logging_to_files(shop_log_files);
    api.emitter.emit(*v0);
    api.emitter.emit(*v1);
    api.emitter.emit(static_cast< power_plant const &>(*p0));
    api.emitter.handle_reservoir_output(*v0);
    api.emitter.handle_reservoir_output(*v1);

    CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
    CHECK(api.emitter.objects.size<shop_gate>() == 2);
    CHECK(api.emitter.objects.size<shop_pump>() == 1);
    CHECK(api.emitter.objects.size<shop_unit>() == 1);
  }

} // TEST_CASE

TEST_SUITE_END();
