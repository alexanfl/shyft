#include <csignal>
#include <cstdlib>
#include <thread>

#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <doctest/doctest.h>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/compute/client.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/dstm/server_logger.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/web_api/energy_market/request_handler.h>

#include "model_simple.h"
#include <test/test_utils.h>

namespace shyft::energy_market::stm::srv {

  namespace {

    constexpr bool not_done(model_state state) {
      return state != model_state::finished && state != model_state::failed;
    }

    auto poll_done_for(dstm::client &client, std::string const &model_id, auto max_duration) {
      auto timeout = shyft::core::utctime_now() + max_duration;
      model_state state{};
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      while ((state = client.get_state(model_id)) == model_state::idle && shyft::core::utctime_now() < timeout)
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
      while ((state = client.get_state(model_id)) == model_state::running && shyft::core::utctime_now() < timeout)
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
      return state;
    }

    std::optional<model_state> synch_optimize(
      dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands,
      auto max_duration) {
      if (!client.optimize(model_id, time_axis, commands))
        return std::nullopt;
      return poll_done_for(client, model_id, max_duration);
    }

    auto synch_optimize(
      dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands) {
      return synch_optimize(client, model_id, time_axis, commands, stm::shop::calc_suggested_timelimit(commands));
    }

    std::optional<model_state> synch_tune(
      dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands,
      auto max_duration) {
      if (!client.tune(model_id, time_axis, commands))
        return std::nullopt;
      return poll_done_for(client, model_id, max_duration);
    }

    auto synch_tune(
      dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands) {
      return synch_tune(client, model_id, time_axis, commands, stm::shop::calc_suggested_timelimit(commands));
    }

    struct test_server : dstm::server {
      shyft::web_api::energy_market::request_handler bg_server;
      std::future<int> web_srv; ///< mutex,

      //-- to verify fx-callback
      std::string fx_mid;
      std::string fx_arg;

      bool fx_handler(std::string mid, std::string json_arg) {
        fx_mid = mid;
        fx_arg = json_arg;
        return true;
      }

      explicit test_server()
        : dstm::server()
        , bg_server{models, sm, fx_cb, dtss.get()} {
        this->fx_cb = [this](std::string m, std::string a) -> bool {
          return this->fx_handler(m, a);
        };
      }

      explicit test_server(std::string const &root_dir)
        : dstm::server()
        , bg_server{models, sm, fx_cb, dtss.get()} {
        dtss->add_container("test", root_dir);
      }

      ~test_server() {
        stop_web_api(); // ensure we are rigging us down.
      }

      // FIXME: remove this, not safe... - jeh
      auto do_get_model(string name) {
        return models
          .mutate_or_throw(
            name,
            [](auto view) {
              return view.model;
            })
          .get();
      }

      void start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads) {
        if (!web_srv.valid()) {
          web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads] {
            return shyft::web_api::run_web_server(
              bg_server,
              host_ip,
              static_cast<unsigned short>(port),
              std::make_shared<std::string>(doc_root),
              fg_threads,
              bg_threads);
          });
        }
      }

      bool web_api_running() const {
        return web_srv.valid();
      }

      void stop_web_api() {
        if (web_srv.valid()) {
          std::raise(SIGINT);
          (void) web_srv.get();
        }
      }
    };

    //-- test client
    using tcp = boost::asio::ip::tcp;              // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket; // from <boost/beast/websocket.hpp>
    using boost::system::error_code;

    unsigned short get_free_port() {
      using namespace boost::asio;
      io_service service;
      ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0)); // pass in 0 to get a free port.
      return acceptor.local_endpoint().port();
    }

    /** engine that perform a publish-subscribe against a specified host
     *
     * Same pattern as used in test for the dtss web_api (in cpp/test/web_api/web_server.cpp)
     */
    class run_params_session : public std::enable_shared_from_this<run_params_session> {
      tcp::resolver resolver_;
      websocket::stream<tcp::socket> ws_;
      boost::beast::multi_buffer buffer_;
      std::string host_;
      std::string port_;
      std::string fail_;
      test_server *const srv; ///< Hold the server so we can use its dtss.
      int num_waits = 0;      ///< How many expected releases of subscribed read pattern

      // report a failure
      void fail(error_code ec, char const *what) {
        fail_ = std::string(what) + ": " + ec.message() + "\n";
      }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));
     public:
      // Resolver and socket require an io_context
      explicit run_params_session(boost::asio::io_context &ioc, test_server *const srv)
        : resolver_(ioc)
        , ws_(ioc)
        , srv{srv} {
      }

      std::vector<std::string> responses_;

      // Start the asynchronous operation
      void run(std::string_view host, int port) {
        // Save these for later
        host_ = host;
        port_ = std::to_string(port);
        resolver_.async_resolve(
          host_,
          port_, // Look up the domain name
          [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
            me->on_resolve(ec, results);
          });
      }

      void on_resolve(error_code ec, tcp::resolver::results_type results) {
        fail_on_error(ec, "resolve");
        // Make the connection on the IP address we get from a lookup
        boost::asio::async_connect(
          ws_.next_layer(),
          results.begin(),
          results.end(),
          std::bind(&run_params_session::on_connect, shared_from_this(), std::placeholders::_1));
      }

      void on_connect(error_code ec) {
        fail_on_error(ec, "connect");
        ws_.async_handshake(
          host_,
          "/", // Perform websocket handshake
          [me = shared_from_this()](error_code ec) {
            me->send_initial(ec);
          });
      }

      void send_initial(error_code ec) {
        fail_on_error(ec, "send_initial");
        ws_.async_write(
          boost::asio::buffer(R"_(run_params {"request_id": "initial", "model_key": "simple", "subscribe": true})_"),
          [me = shared_from_this()](error_code ec, std::size_t bytes_transferred) {
            me->start_read(ec, bytes_transferred);
          });
      }

      void start_read(error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        fail_on_error(ec, "start_read");
        ws_.async_read(buffer_, [me = shared_from_this()](error_code ec2, std::size_t bytes_transferred2) {
          me->on_read(ec2, bytes_transferred2);
        });
      }

      void on_read(error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        fail_on_error(ec, "read");
        std::string response = boost::beast::buffers_to_string(buffer_.data());

        responses_.push_back(response);
        buffer_.consume(buffer_.size());
        if (response.find("finale") != std::string::npos) {
          ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
            me->on_close(ec);
          });
        } else {
          if (response.find("initial") != std::string::npos) {
            if (num_waits == 0) { // First time we update a model. Here via a simple notify change
              ++num_waits;
              auto mdl = srv->do_get_model("simple");
              auto pa = proxy_attr(mdl->run_params, "n_inc_runs", mdl->run_params.n_inc_runs);
              pa.a = 33;
              std::string sub_id = "dstm://Msimple";
              auto notify_tag = pa.url(sub_id);
              srv->sm->notify_change(notify_tag);
            } else if (num_waits == 1) { // Update model via optimization
              ++num_waits;
              std::string mdl_id("simple");
              auto host_port = fmt::format("localhost:{}", srv->get_listening_port());
              shyft::energy_market::stm::srv::dstm::client c(host_port);
              auto cmd = optimization_commands(1, false);
              auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
              auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
              auto const t_step = shyft::core::deltahours(1);
              const std::size_t n_steps = (t_end - t_begin) / t_step;
              const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};

              REQUIRE(c.optimize(mdl_id, ta, cmd));

              auto t_exit = shyft::core::utctime_now() + std::chrono::seconds(30); // reasonable limit
              while (not_done(c.get_state(mdl_id)) && shyft::core::utctime_now() < t_exit)
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
              REQUIRE(c.get_state(mdl_id) == model_state::finished);
              auto stm = c.get_model(mdl_id);
              auto rstm = c.get_model("simple_results");
              check_results(stm, rstm, t_begin, t_end, t_step);
            } else {
              ws_.async_write(
                boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"initial"})_"),
                [me = shared_from_this()](error_code ec, std::size_t) {
                  if (!ec)
                    throw std::runtime_error("oops");
                });
            }
          }

          //-- anyway, always continue to read (unless we hit the final request-id sent with the unsubscribe message
          ws_.async_read(buffer_, [me = shared_from_this()](error_code ec, std::size_t bytes_transferred) {
            me->on_read(ec, bytes_transferred);
          });
        }
      }

      void on_close(error_code ec) {
        MESSAGE("closing");
        fail_on_error(ec, "close");
      }

#undef fail_on_error
    };

  }

  TEST_SUITE_BEGIN("stm/shop");

  static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_files==true.

  using namespace shyft::energy_market::stm::srv;
  using namespace shyft::energy_market::stm::srv::dstm;
  using namespace test;
  using shyft::core::utctime_now;

  TEST_CASE(
    "stm/shop/dstm_simple_model_opt/1"
    * doctest::description("building and optmimizing simple model without inlet segment: "
                           "aggregate-penstock-maintunnel-reservoir")) {

    dlib::set_all_logging_output_streams(std::cout);
    dlib::set_all_logging_levels(dlib::LALL);

    // dlib::set_all_logging_levels(dlib::LWARN);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};
    int const mega = 1000000;

    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with results:
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();

    compute::server cs;
    cs.set_listening_ip("127.0.0.1");
    auto cs_port_no = cs.start_server();

    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    auto cs_addr = fmt::format("127.0.0.1:{}", cs_port_no);
    s.do_add_compute_server(cs_addr);

    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here
    try {
      auto host_port = fmt::format("127.0.0.1:{}", port_no);
      client c(host_port);

      {
        auto cs_status = c.compute_server_status();
        REQUIRE(cs_status.size() == 1);
        REQUIRE(cs_status[0].address == cs_addr);
      }

      // get version info
      auto result = c.get_version_info();
      CHECK_EQ(result, s.do_get_version_info());

      // get model ids
      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      char const mdl_id[] = {"test_stm_model"};
      CHECK_EQ(c.add_model(mdl_id, stm), true);
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);
      // Arrange for subscription checks
      // ADD subscription to ALL results for this model, because we would like to see the notification
      auto mdl_prefix = string("dstm://M") + mdl_id;
      auto all_result_ts_urls = ts_url_generator(mdl_prefix, *stm);
      auto subs = s.dtss->sm->add_subscriptions(all_result_ts_urls);
      auto sum_subs = 0;
      for (auto const &sub : subs)
        sum_subs += sub->v;
      // RUN the optimization, that should fire notifications when done(so subs above should increment)
      auto cmd = optimization_commands(run_id);

      SUBCASE("optimize") {
        REQUIRE(synch_optimize(c, mdl_id, ta, cmd) == model_state::finished);
      }
      SUBCASE("tune") {
        REQUIRE(c.start_tune(mdl_id));
        REQUIRE(synch_tune(c, mdl_id, ta, cmd) == model_state::tuning);
        REQUIRE(synch_tune(c, mdl_id, ta, cmd) == model_state::tuning);
        REQUIRE(c.stop_tune(mdl_id));
        REQUIRE(c.get_state(mdl_id) == model_state::finished);
      }

      auto stm2 = c.get_model(mdl_id);
      check_results(stm2, rstm, t_begin, t_end, t_step);

      auto attr = stm2->market.front()->buy;
      REQUIRE_EQ(exists(attr), true);
      auto ts = attr;
      auto values = ts.values();
      REQUIRE_EQ(values[0], 10.0 * mega);

      attr = stm2->market.front()->sale;
      REQUIRE_EQ(exists(attr), true);
      ts = attr;
      values = ts.values();
      REQUIRE_EQ(values[5], -10.0 * mega);

      auto sum_subs_after = 0;
      for (auto const &sub : subs)
        sum_subs_after += sub->v;
      CHECK_GT(sum_subs_after, sum_subs);
      MESSAGE(
        "subs version before:" << sum_subs << ", vs. after:" << sum_subs_after << ", vs. num ts subs: " << subs.size());

      c.close();
      s.clear();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
      s.clear();
    }
  }

  TEST_CASE(
    "stm/shop/dstm_simple_model_opt/2"
    * doctest::description("optmimizing simple model without inlet segment with dstm: "
                           "aggregate-penstock-maintunnel-reservoir")) {
    dlib::set_all_logging_levels(dlib::LWARN);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};
    int const mega = 1000000;

    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with results:
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    auto const n_compute_servers = 1;
    std::vector<compute::server> compute_servers(n_compute_servers);
    std::vector<std::string> cn_addr;
    for (auto &sl : compute_servers) {
      sl.set_listening_ip("127.0.0.1");
      auto ps = sl.start_server();
      cn_addr.push_back(fmt::format("127.0.0.1:{}", ps));
    }

    REQUIRE_GT(port_no, 0);
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      client c(host_port);

      // get version info
      auto result = c.get_version_info();
      CHECK_EQ(result, s.do_get_version_info());

      for (auto const &cn : cn_addr)
        c.add_compute_server(cn); // add compute servers

      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      std::string mdl_id = "test_stm_model";
      CHECK_EQ(c.add_model(mdl_id, stm), true);
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);

      // Arrange for subscription checks
      // ADD subscription to ALL results for this model, because we would like to see the notification
      auto mdl_prefix = fmt::format("dstm://M{}", mdl_id);
      auto all_result_ts_urls = ts_url_generator(mdl_prefix, *stm);
      auto subs = s.dtss->sm->add_subscriptions(all_result_ts_urls);
      auto sum_subs = 0;
      for (auto const &sub : subs)
        sum_subs += sub->v;

      auto cmd = optimization_commands(run_id);
      SUBCASE("optimize") {
        REQUIRE(synch_optimize(c, mdl_id, ta, cmd) == model_state::finished);
      }
      SUBCASE("tune") {
        REQUIRE(c.start_tune(mdl_id));
        REQUIRE(synch_tune(c, mdl_id, ta, cmd) == model_state::tuning);
        REQUIRE(c.stop_tune(mdl_id));
        REQUIRE(c.get_state(mdl_id) == model_state::finished);
      }

      REQUIRE(c.get_state(mdl_id) == model_state::finished);
      auto stm2 = c.get_model(mdl_id);
      check_results(stm2, rstm, t_begin, t_end, t_step);
      CHECK_EQ(stm2->run_params.run_time_axis, ta); // ensure time-axis is updated
      CHECK(stm2->summary != nullptr);
      CHECK_EQ(std::isfinite(stm2->summary->total), true);
      auto attr = stm2->market.front()->buy;
      REQUIRE_EQ(exists(attr), true);
      auto ts = attr;
      auto values = ts.values();
      REQUIRE_EQ(values[0], 10.0 * mega);

      attr = stm2->market.front()->sale;
      REQUIRE_EQ(exists(attr), true);
      ts = attr;
      values = ts.values();
      REQUIRE_EQ(values[5], -10.0 * mega);

      // -- verify that notifications was done (that is: that the version number on the subscribed  servers have ticked
      // up
      auto sum_subs_after = 0;
      for (auto const &sub : subs)
        sum_subs_after += sub->v;
      CHECK_GT(sum_subs_after, sum_subs);

      c.close();
      s.clear();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
      s.clear();
    }
  }

  TEST_CASE("stm/shop/get_log" * doctest::description("getting log from model, before and after running shop")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};

    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);

    test::utils::temp_dir tmpdir{"stm_srv_shop_optimize.test.get_shop_logger."};
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    compute::server cs;
    cs.set_listening_ip("127.0.0.1");
    auto cs_port_no = cs.start_server();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    s.do_add_compute_server(fmt::format("127.0.0.1:{}", cs_port_no));

    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      client c(host_port);

      // get version info
      auto result = c.get_version_info();
      CHECK_EQ(result, s.do_get_version_info());

      // get model ids
      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      const std::string mdl_id = "test_stm_model";
      CHECK_EQ(c.add_model(mdl_id, stm), true);
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);

      // Check log before anything else:
      auto log = c.get_log(mdl_id);
      CHECK_EQ(log.size(), 0);

      auto cmd = optimization_commands(run_id);
      REQUIRE(c.optimize(mdl_id, ta, cmd));                   // Starting optimization
      auto t_exit = utctime_now() + std::chrono::seconds(30); // reasonable limit
      while (not_done(c.get_state(mdl_id)) && utctime_now() < t_exit) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
        log = c.get_log(mdl_id); // just to check that we can get log while optimizing
      }

      auto stm2 = c.get_model(mdl_id);
      check_results(stm2, rstm, t_begin, t_end, t_step);
      log = c.get_log(mdl_id);

      CHECK_GT(log.size(), 15); // We expect a few log messages

      c.close();
      s.clear();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
      s.clear();
    }
  }

  TEST_CASE(
    "stm/shop/optimize_with_unbound_attributes"
    * doctest::description("Testing that dtss handles unbound time series properly before sending to SHOP")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};

    test::utils::temp_dir tmpdir{"stm_srv_shop_optimize.test.optimize_with_unbound_attributes."};
    server s;

    auto port_no = s.start_server();
    s.add_container("test", (tmpdir / "ts").string());

    compute::server cs;
    cs.set_listening_ip("127.0.0.1");
    auto cs_port_no = cs.start_server();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    s.do_add_compute_server(fmt::format("127.0.0.1:{}", cs_port_no));

    auto stm = build_simple_model_with_dtss(
      *(s.dtss), t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto stm2 = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto rstm = build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400, true); // Expected results
    REQUIRE_GT(port_no, 0);
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      client c(host_port);
      c.add_model("dtss_optimize_unbound", stm);
      c.add_model("dtss_optimize_bound", stm2);
      stm = c.get_model("dtss_optimize_unbound");
      // CHECK that stuff is unbound:
      auto hps = stm->hps[0];
      auto market = stm->market[0];
      CHECK_EQ(market->price.needs_bind(), true);
      CHECK_EQ(market->max_buy.needs_bind(), true);
      CHECK_EQ(market->max_sale.needs_bind(), true);
      CHECK_EQ(market->load.needs_bind(), true);
      CHECK_EQ(market->tsm["planned_revenue"].needs_bind(), true);
      auto ug = stm->unit_groups[0];
      CHECK_EQ(ug->obligation.cost.needs_bind(), true);
      CHECK_EQ(ug->obligation.schedule.needs_bind(), true);
      CHECK_EQ(ug->members[0]->active.needs_bind(), true);
      auto rsv = std::dynamic_pointer_cast<stm::reservoir>(hps->find_reservoir_by_name("reservoir"));
      CHECK_EQ(rsv->level.regulation_min.needs_bind(), true);
      CHECK_EQ(rsv->level.regulation_max.needs_bind(), true);
      CHECK_EQ(rsv->volume.static_max.needs_bind(), true);
      CHECK_EQ(rsv->water_value.endpoint_desc.needs_bind(), true);
      CHECK_EQ(rsv->level.realised.needs_bind(), true);
      CHECK_EQ(rsv->inflow.schedule.needs_bind(), true);

      auto wtr_flood = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute flood river"));
      CHECK_EQ(wtr_flood->discharge.static_max.needs_bind(), true);

      auto wtr_tunnel = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute input tunnel"));
      CHECK_EQ(wtr_tunnel->head_loss_coeff.needs_bind(), true);

      auto wtr_penstock = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute penstock"));
      CHECK_EQ(wtr_penstock->head_loss_coeff.needs_bind(), true);

      auto ps = std::dynamic_pointer_cast<stm::power_plant>(hps->find_power_plant_by_name("plant"));
      CHECK_EQ(ps->outlet_level.needs_bind(), true);

      auto gu = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_name("aggregate"));
      CHECK_EQ(gu->production.static_min.needs_bind(), true);
      CHECK_EQ(gu->production.static_max.needs_bind(), true);
      CHECK_EQ(gu->production.nominal.needs_bind(), true);

      // CHECK that we cannot do optimization on unbound model:
      auto cmd = optimization_commands(run_id);
      REQUIRE(synch_optimize(c, "dtss_optimize_unbound", ta, cmd) == std::nullopt);

      // Evaluate all unbound time series in the model:
      CHECK_EQ(true, c.evaluate_model("dtss_optimize_unbound", ta.total_period(), true, false));
      stm = c.get_model("dtss_optimize_unbound");
      market = stm->market[0];
      CHECK_EQ(market->tsm["planned_revenue"].needs_bind(), false);
      auto planned_revenue = market->price * 100.0;
      CHECK_EQ(planned_revenue, market->tsm["planned_revenue"]);
      hps = stm->hps[0];
      rsv = std::dynamic_pointer_cast<stm::reservoir>(hps->find_reservoir_by_name("reservoir"));
      wtr_flood = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute flood river"));
      wtr_tunnel = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute input tunnel"));
      wtr_penstock = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute penstock"));
      ps = std::dynamic_pointer_cast<stm::power_plant>(hps->find_power_plant_by_name("plant"));
      gu = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_name("aggregate"));
      ug = stm->unit_groups[0];
      CHECK_EQ(ug->obligation.cost.needs_bind(), false);
      CHECK_EQ(ug->obligation.schedule.needs_bind(), false);
      CHECK_EQ(ug->members[0]->active.needs_bind(), false);

      auto m2 = stm2->market[0];
      auto hps2 = stm2->hps[0];
      CHECK_EQ(market->price, m2->price);
      CHECK_EQ(market->max_buy, m2->max_buy);
      CHECK_EQ(market->max_sale, m2->max_sale);
      CHECK_EQ(market->load, m2->load);

      auto rsv2 = std::dynamic_pointer_cast<stm::reservoir>(hps2->find_reservoir_by_name("reservoir"));
      CHECK_EQ(rsv->level.regulation_min, rsv2->level.regulation_min);
      CHECK_EQ(rsv->level.regulation_max, rsv2->level.regulation_max);
      CHECK_EQ(rsv->volume.static_max, rsv2->volume.static_max);
      CHECK_EQ(rsv->water_value.endpoint_desc, rsv2->water_value.endpoint_desc);
      CHECK_EQ(rsv->level.realised, rsv2->level.realised);
      CHECK_EQ(rsv->inflow.schedule, rsv2->inflow.schedule);

      auto wtr_flood2 = std::dynamic_pointer_cast<stm::waterway>(hps2->find_waterway_by_name("waterroute flood river"));
      auto wtr_tunnel2 = std::dynamic_pointer_cast<stm::waterway>(
        hps2->find_waterway_by_name("waterroute input tunnel"));
      auto wtr_penstock2 = std::dynamic_pointer_cast<stm::waterway>(hps2->find_waterway_by_name("waterroute penstock"));
      CHECK_EQ(wtr_flood->discharge.static_max, wtr_flood2->discharge.static_max);
      CHECK_EQ(wtr_tunnel->head_loss_coeff, wtr_tunnel2->head_loss_coeff);
      CHECK_EQ(wtr_penstock->head_loss_coeff, wtr_penstock2->head_loss_coeff);

      auto ps2 = std::dynamic_pointer_cast<stm::power_plant>(hps2->find_power_plant_by_name("plant"));
      auto gu2 = std::dynamic_pointer_cast<stm::unit>(hps2->find_unit_by_name("aggregate"));
      CHECK_EQ(ps->outlet_level, ps2->outlet_level);
      CHECK_EQ(gu->production.static_min, gu2->production.static_min);
      CHECK_EQ(gu->production.static_max, gu2->production.static_max);
      CHECK_EQ(gu->production.nominal, gu2->production.nominal);

      SUBCASE("optimize") {
        REQUIRE(synch_optimize(c, "dtss_optimize_unbound", ta, cmd) == model_state::finished);
      }
      SUBCASE("tune") {
        REQUIRE(c.start_tune("dtss_optimize_unbound"));
        REQUIRE(synch_tune(c, "dtss_optimize_unbound", ta, cmd) == model_state::tuning);
        REQUIRE(c.stop_tune("dtss_optimize_unbound"));
        REQUIRE(c.get_state("dtss_optimize_unbound") == model_state::finished);
      }

      stm = c.get_model("dtss_optimize_unbound");
      check_results(stm, rstm, t_begin, t_end, t_step);
      c.close();
      s.clear();
      cs.clear();
    } catch (std::exception const &e) {
      DOCTEST_MESSAGE(e.what());
      CHECK_EQ(true, false);
      s.clear();
      cs.clear();
    }
  }

  TEST_CASE(
    "stress/stm/shop/dstm_optimize"
    * doctest::description("Testing that dtss handles multiple unbound time series properly before sending to SHOP")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};

    test::utils::temp_dir tmpdir{"stm_srv_shop_optimize.test.dstm_stress_optimize."};
    server s;

    auto port_no = s.start_server();
    auto host_port = string("localhost:") + std::to_string(port_no);
    s.add_container("test", (tmpdir / "ts").string());

    compute::server cs;
    cs.set_listening_ip("127.0.0.1");
    auto cs_port_no = cs.start_server();

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    s.do_add_compute_server(fmt::format("127.0.0.1:{}", cs_port_no));

    auto stm = build_simple_model_with_dtss(
      *(s.dtss), t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto stm2 = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto rstm = build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400, true); // Expected results
    REQUIRE_GT(port_no, 0);
    int const n_connects = 20;

    try {
      // Result for each thread:
      auto cmd = optimization_commands(run_id);
      std::vector<std::future<bool>> res;
      res.reserve(n_connects);

      for (std::size_t i = 0; i < n_connects; ++i) {
        res.emplace_back(std::async(std::launch::async, [i, ta, dt = t_step, &stm, &rstm, &cmd, &host_port]() -> bool {
          client c(host_port);
          string mid = "m" + std::to_string(i);
          c.add_model(mid, stm);
          c.evaluate_model(mid, ta.total_period(), false, false); // Evaluate model:
          auto t_exit = utctime_now() + std::chrono::seconds(30); // reasonable limit
          while (!c.optimize(mid, ta, cmd))
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
          poll_done_for(c, mid, stm::shop::calc_suggested_timelimit(cmd) * 2);

          // CHECK results:
          auto stmx = c.get_model(mid);
          c.remove_model(mid);
          auto t0 = ta.total_period().start;
          auto tN = ta.total_period().end;
          check_results(stmx, rstm, t0, tN, dt);
          return true;
        }));
      }
      std::ranges::for_each(res, [](auto &el) {
        CHECK(el.get());
      });

    } catch (std::exception const &e) {
      FAIL(e.what());
      s.clear();
    }
  }

  TEST_CASE(
    "stm/shop/optimize_with_subscription"
    * doctest::description("Optimizing, while holding a subscription to run parameters of the system.")) {
    dlib::set_all_logging_levels(dlib::LALL);
    dlib::set_all_logging_output_streams(std::cout);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    bool const with_results = true;
    auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = shyft::core::deltahours(1);
    auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};

    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, with_results);

    std::string host_ip{"127.0.0.1"};
    test::utils::temp_dir tmp{"shyft.shop.subtst"};
    int port = get_free_port();
    std::string doc_root = (tmp / "doc_root").string();
    test_server srv(doc_root);
    srv.set_listening_ip(host_ip);
    auto port_no = srv.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    compute::server csrv;
    csrv.set_listening_ip("127.0.0.1");
    auto csrv_port_no = csrv.start_server();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    srv.do_add_compute_server(fmt::format("127.0.0.1:{}", csrv_port_no));

    try {
      srv.do_add_model("simple", stm);
      srv.do_add_model("simple_results", rstm);

      srv.start_web_api(host_ip, port, doc_root, 1, 1);
      REQUIRE(srv.web_api_running());
      std::this_thread::sleep_for(std::chrono::milliseconds(700));
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<run_params_session>(ioc, &srv);
      s1->run(host_ip, port);

      ioc.run();
      // Set up expected responses and comparisons.
      std::vector<std::string> expected_responses{
        R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":0},{"attribute_id":"n_full_runs","data":0},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
        R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":33},{"attribute_id":"n_full_runs","data":0},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
        R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":3},{"attribute_id":"n_full_runs","data":3},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":1514768400.0,"dt":3600.0,"n":18}},{"attribute_id":"fx_log","data":[]}]}})_",
        R"_({"request_id":"finale","subscription_id":"initial","diagnostics":""})_"};
      auto responses = s1->responses_;
      s1.reset();

      srv.clear();
      csrv.clear();

      REQUIRE_EQ(responses.size(), expected_responses.size());
      for (std::size_t i = 0; i < responses.size(); ++i) {
        bool found_match = false; // order of responses might differ for the two last
        for (std::size_t j = 0; j < responses.size() && !found_match; ++j) {
          found_match = responses[j] == expected_responses[i];
        }
        CHECK_MESSAGE(!found_match, "failed for the ", i, "th response: ", expected_responses[i], "!=", responses[i]);
      }
    } catch (...) {
    }
    try {
      std::this_thread::sleep_for(std::chrono::milliseconds(700));
      MESSAGE("now cleanup after some sleep");
      srv.stop_web_api();
    } catch (...) {
    }
  }

  TEST_SUITE_END();

}
