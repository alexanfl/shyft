#include <string>
#include <vector>

#include <doctest/doctest.h>

#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/time/utctime_utilities.h>

TEST_SUITE_BEGIN("stm/shop");

TEST_CASE("stm/shop/command_parsing_basic") {
  // Note: Using 6 decimal places for any floating point values in strings,
  // because currently the shop_command is using std::to_string to convert
  // the double value into string and it seems to always use 6 decimals
  // without option to specify anything else.

  using namespace std::string_literals;
  using shyft::energy_market::stm::shop::shop_command;

  SUBCASE("keyword only") {
    // Input string
    char const * const command_string = "quit";
    // Parse string
    shop_command cmd{command_string};
    // Check parse results
    CHECK(cmd.keyword == (std::string) command_string);
    CHECK(cmd.specifier.empty());
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 0);
    // Convert back to string and compare with original input
    // This both tests the string conversion operator and also
    // is an additional verification of the parsing above.
    CHECK((std::string) cmd == (std::string) command_string);
  }
  SUBCASE("keyword and specifier") {
    char const * const command_string = "save tunnelloss";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "save"s);
    CHECK(cmd.specifier == "tunnelloss"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 0);
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::save_tunnelloss();
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier and one option") {
    char const * const command_string = "set method /dual";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "set"s);
    CHECK(cmd.specifier == "method"s);
    CHECK(cmd.options.size() == 1);
    if (cmd.options.size() == 1) {
      CHECK(cmd.options.front() == "dual"s);
    }
    CHECK(cmd.objects.size() == 0);
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::set_method_dual();
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier and string value") {
    char const * const command_string = "log file filename.ext";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "filename.ext"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::log_file("filename.ext");
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier and int value") {
    char const * const command_string = "set max_num_threads 8";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "set"s);
    CHECK(cmd.specifier == "max_num_threads"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "8"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::set_max_num_threads(8);
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier and double value") {
    char const * const command_string = "set fcr_n_band 0.400000";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "set"s);
    CHECK(cmd.specifier == "fcr_n_band"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "0.400000"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::set_fcr_n_band(0.4);
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier option and string value") {
    char const * const command_string = "return simres /gen filename.ext";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "return"s);
    CHECK(cmd.specifier == "simres"s);
    CHECK(cmd.options.size() == 1);
    if (cmd.options.size() == 1) {
      CHECK(cmd.options.front() == "gen"s);
    }
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "filename.ext"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::return_simres_gen("filename.ext");
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier option and double value") {
    char const * const command_string = "set mipgap /relative 0.300000";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "set"s);
    CHECK(cmd.specifier == "mipgap"s);
    CHECK(cmd.options.size() == 1);
    if (cmd.options.size() == 1) {
      CHECK(cmd.options.front() == "relative"s);
    }
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "0.300000"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::set_mipgap(false, 0.3);
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier and three options") {
    char const * const command_string = "penalty flag /on /reservoir /ramping";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "penalty"s);
    CHECK(cmd.specifier == "flag"s);
    CHECK(cmd.options.size() == 3);
    if (cmd.options.size() == 3) {
      CHECK(cmd.options[0] == "on"s);
      CHECK(cmd.options[1] == "reservoir"s);
      CHECK(cmd.options[2] == "ramping"s);
    }
    CHECK(cmd.objects.size() == 0);
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2 = shop_command::penalty_flag_reservoir_ramping(true);
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("keyword specifier two options and double value") {
    char const * const input_command_string = "\t penalty   cost   /reservoir \t /ramping    123.500000   ";
    char const * const output_command_string = "penalty cost /reservoir /ramping 123.500000";
    shop_command cmd{input_command_string};
    CHECK(cmd.keyword == "penalty"s);
    CHECK(cmd.specifier == "cost"s);
    CHECK(cmd.options.size() == 2);
    if (cmd.options.size() == 2) {
      CHECK(cmd.options[0] == "reservoir"s);
      CHECK(cmd.options[1] == "ramping"s);
    }
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects[0] == "123.500000"s);
    }
    CHECK((std::string) cmd == (std::string) output_command_string);
    shop_command cmd2 = shop_command::penalty_cost_reservoir_ramping(123.50);
    CHECK(cmd == cmd2);
    CHECK((std::string) cmd == (std::string) cmd2);
  }
  SUBCASE("multiple objects") {
    char const * const command_string = "the quick brown fox jumps over the lazy dog";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "the"s);
    CHECK(cmd.specifier == "quick"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 7);
    if (cmd.objects.size() == 7) {
      CHECK(cmd.objects[0] == "brown"s);
      CHECK(cmd.objects[1] == "fox"s);
      CHECK(cmd.objects[2] == "jumps"s);
      CHECK(cmd.objects[3] == "over"s);
      CHECK(cmd.objects[4] == "the"s);
      CHECK(cmd.objects[5] == "lazy"s);
      CHECK(cmd.objects[6] == "dog"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
  }
  SUBCASE("options before objects") {
    shop_command cmd{"print mc_curves /up /down mc.xml 12 24"}; // Alternative variant with options before objects
    CHECK(cmd.keyword == "print"s);
    CHECK(cmd.specifier == "mc_curves"s);
    CHECK(cmd.options.size() == 2);
    if (cmd.options.size() == 2) {
      CHECK(cmd.options[0] == "up"s);
      CHECK(cmd.options[1] == "down"s);
    }
    CHECK(cmd.objects.size() == 3);
    if (cmd.objects.size() == 3) {
      CHECK(cmd.objects[0] == "mc.xml"s);
      CHECK(cmd.objects[1] == "12"s);
      CHECK(cmd.objects[2] == "24"s);
    }
  }
}

TEST_CASE("stm/shop_command_parsing_strict_mode") {
  // Explicitely using parse_strict with some argument variants that are
  // typically not supported in this mode, but are handled in the default
  // parse_lenient mode (the same variants will be tested with
  // parse_lenient in next case).

  using namespace std::string_literals;
  using shyft::energy_market::stm::shop::shop_command;

  SUBCASE("simple variants") {
    shop_command cmd;
    CHECK_THROWS(cmd.parse_strict("The /quick"));
    CHECK_THROWS(cmd.parse_strict("The /quick /brown"));
    CHECK_THROWS(cmd.parse_strict("The /quick /brown /fox"));
    CHECK_THROWS(cmd.parse_strict("The quick /brown fox /jumps"));
    CHECK_THROWS(cmd.parse_strict("The quick brown /fox"));
    CHECK_THROWS(cmd.parse_strict("The quick brown /fox /jumps"));
  }
  SUBCASE("objects before options") {
    shop_command cmd;
    CHECK_THROWS(
      cmd.parse_strict("print mc_curves mc.xml 12 24 /up /down")); // From shop documentation: Objects before options
  }
  SUBCASE("options before objects") {
    shop_command cmd;
    cmd.parse_strict("print mc_curves /up /down mc.xml 12 24"); // Alternative variant with options before objects
    CHECK(cmd.keyword == "print"s);
    CHECK(cmd.specifier == "mc_curves"s);
    CHECK(cmd.options.size() == 2);
    if (cmd.options.size() == 2) {
      CHECK(cmd.options[0] == "up"s);
      CHECK(cmd.options[1] == "down"s);
    }
    CHECK(cmd.objects.size() == 3);
    if (cmd.objects.size() == 3) {
      CHECK(cmd.objects[0] == "mc.xml"s);
      CHECK(cmd.objects[1] == "12"s);
      CHECK(cmd.objects[2] == "24"s);
    }
    CHECK(static_cast<std::string>(cmd) == "print mc_curves /up /down mc.xml 12 24"s);
  }
  SUBCASE("mixed order of options before objects") {
    shop_command cmd;
    CHECK_THROWS(cmd.parse_strict(
      "print mc_curves mc.xml /up 12 /down 24")); // Alternative variant with mixed order of options before objects
  }
  SUBCASE("no space before options") {
    shop_command cmd;
    CHECK_THROWS(
      cmd.parse_strict("print mc_curves mc.xml/up/down 12 24")); // Alternative variant with no space before options
  }
}

TEST_CASE("stm/shop_command_parsing_lenient_mode") {
  // Explicitely using parse_lenient with some arguments that are not
  // supported with parse_strict, which was tested above.
  // Note that parse_lenient is typically the default, so the first
  // test case with no specific variant (strict/lenient) tests the
  // most standard cases with parse_lenient!

  using namespace std::string_literals;
  using shyft::energy_market::stm::shop::shop_command;

  SUBCASE("missing specifier and one option") {
    shop_command cmd;
    cmd.parse_lenient("The /quick");
    CHECK(cmd.keyword == "The"s);
    CHECK(cmd.specifier.empty());
    CHECK(cmd.objects.size() == 0);
    CHECK(cmd.options.size() == 1);
    if (cmd.options.size() == 1) {
      CHECK(cmd.options[0] == "quick"s);
    }
  }
  SUBCASE("missing specifier and two options") {
    shop_command cmd;
    cmd.parse_lenient("The /quick /brown");
    CHECK(cmd.keyword == "The"s);
    CHECK(cmd.specifier.empty());
    CHECK(cmd.objects.size() == 0);
    CHECK(cmd.options.size() == 2);
    if (cmd.options.size() == 2) {
      CHECK(cmd.options[0] == "quick"s);
      CHECK(cmd.options[1] == "brown"s);
    }
  }
  SUBCASE("missing specifier and three options") {
    shop_command cmd;
    cmd.parse_lenient("The /quick /brown /fox");
    CHECK(cmd.keyword == "The"s);
    CHECK(cmd.specifier.empty());
    CHECK(cmd.objects.size() == 0);
    CHECK(cmd.options.size() == 3);
    if (cmd.options.size() == 3) {
      CHECK(cmd.options[0] == "quick"s);
      CHECK(cmd.options[1] == "brown"s);
      CHECK(cmd.options[2] == "fox"s);
    }
  }
  SUBCASE("keyword specifier and three options") {
    shop_command cmd;
    cmd.parse_lenient("The quick /brown fox /jumps");
    CHECK(cmd.keyword == "The"s);
    CHECK(cmd.specifier == "quick"s);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects[0] == "fox"s);
    }
    CHECK(cmd.options.size() == 2);
    if (cmd.options.size() == 2) {
      CHECK(cmd.options[0] == "brown"s);
      CHECK(cmd.options[1] == "jumps"s);
    }
  }
  SUBCASE("object before option") {
    shop_command cmd;
    cmd.parse_lenient("The quick brown /fox");
    CHECK(cmd.keyword == "The"s);
    CHECK(cmd.specifier == "quick"s);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects[0] == "brown"s);
    }
    CHECK(cmd.options.size() == 1);
    if (cmd.options.size() == 1) {
      CHECK(cmd.options[0] == "fox"s);
    }
  }
  SUBCASE("object before two option") {
    shop_command cmd;
    cmd.parse_lenient("The quick brown /fox /jumps");
    CHECK(cmd.keyword == "The"s);
    CHECK(cmd.specifier == "quick"s);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects[0] == "brown"s);
    }
    CHECK(cmd.options.size() == 2);
    if (cmd.options.size() == 2) {
      CHECK(cmd.options[0] == "fox"s);
      CHECK(cmd.options[1] == "jumps"s);
    }
  }
  SUBCASE("some special variants") {
    auto check_print_mc_curves = [](shop_command const & cmd) {
      CHECK(cmd.keyword == "print"s);
      CHECK(cmd.specifier == "mc_curves"s);
      CHECK(cmd.options.size() == 2);
      if (cmd.options.size() == 2) {
        CHECK(cmd.options[0] == "up"s);
        CHECK(cmd.options[1] == "down"s);
      }
      CHECK(cmd.objects.size() == 3);
      if (cmd.objects.size() == 3) {
        CHECK(cmd.objects[0] == "mc.xml"s);
        CHECK(cmd.objects[1] == "12"s);
        CHECK(cmd.objects[2] == "24"s);
      }
      CHECK(static_cast<std::string>(cmd) == "print mc_curves /up /down mc.xml 12 24"s);
    };
    {
      // From shop documentation: Objects before options
      shop_command cmd;
      cmd.parse_lenient("print mc_curves mc.xml 12 24 /up /down");
      check_print_mc_curves(cmd);
    }
    {
      // Alternative variant with options before objects
      shop_command cmd;
      cmd.parse_lenient("print mc_curves /up /down mc.xml 12 24");
      check_print_mc_curves(cmd);
    }
    {
      // Alternative variant with mixed order of options before objects
      shop_command cmd;
      cmd.parse_lenient("print mc_curves mc.xml /up 12 /down 24");
      check_print_mc_curves(cmd);
    }
    {
      // Alternative variant with no space before options
      shop_command cmd;
      cmd.parse_lenient("print mc_curves mc.xml/up/down 12 24");
      check_print_mc_curves(cmd);
    }
  }
}

TEST_CASE("stm/shop/commmand_with_file_path_string_values") {
  // Tests with file path object arguments, which are a typical problem area,
  // to specific test handling of path separator, space within strings, escaped
  // characters etc..

  using namespace std::string_literals;
  using shyft::energy_market::stm::shop::shop_command;

  SUBCASE("plain filename") {
    char const * const command_string = "log file filename.ext";
    shop_command cmd;
    cmd.parse_lenient(command_string);
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "filename.ext"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2;
    cmd2.parse_strict(command_string);
    CHECK(cmd == cmd2);
    shop_command cmd3 = shop_command::log_file("filename.ext");
    CHECK(cmd == cmd3);
  }
  SUBCASE("filename with linux path") {
    // NOTE: Linux path separator '/' conflicts with prefix used to separate
    // options from objects in shop command strings, and is therefore not
    // supported when parsing unquoted string. But if explicitely specifying
    // only the object string as with shop_command::log_file then it works,
    // and later subcase we test with quoting which also works.
    char const * const command_string = "log file dir/filename.ext";
    shop_command cmd;
    cmd.parse_lenient(command_string);
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 1);
    if (cmd.options.size() == 1) {
      CHECK(cmd.options.front() == "filename.ext"s);
    }
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "dir"s);
    }
    CHECK((std::string) cmd == "log file /filename.ext dir"s); // Ugh... Not what we intended!
    shop_command cmd2;
    CHECK_THROWS(cmd2.parse_strict(command_string)); // And the strict parser does not even accept it!
    shop_command cmd3 = shop_command::log_file("dir/filename.ext");
    CHECK((std::string) cmd3 == (std::string) command_string); // Yay, that's more like it!
  }
  SUBCASE("filename with windows path") {
    char const * const command_string = R"#(log file dir\filename.ext)#";
    shop_command cmd;
    cmd.parse_lenient(command_string);
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == R"#(dir\filename.ext)#"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2;
    cmd2.parse_strict(command_string);
    CHECK(cmd == cmd2);
    shop_command cmd3 = shop_command::log_file(R"#(dir\filename.ext)#");
    CHECK(cmd == cmd3);
  }
  SUBCASE("quoted filename in quotes with spaces") {
    char const * const command_string = R"#(log file "this is the filename.ext")#";
    shop_command cmd;
    cmd.parse_lenient(command_string);
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == "this is the filename.ext"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2;
    cmd2.parse_strict(command_string);
    CHECK(cmd == cmd2);
    shop_command cmd3 = shop_command::log_file("this is the filename.ext");
    CHECK(cmd == cmd3);
  }
  SUBCASE("quoted filename in quotes with linux path and spaces and escapes") {
    char const * const command_string = R"#(log file "dir/this is \"the\" filename.ext")#";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == R"#(dir/this is "the" filename.ext)#"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2;
    cmd2.parse_strict(command_string);
    CHECK(cmd == cmd2);
    shop_command cmd3 = shop_command::log_file(R"#(dir/this is "the" filename.ext)#");
    CHECK(cmd == cmd3);
  }
  SUBCASE("quoted filename in quotes with windows path and spaces and escapes") {
    char const * const command_string = R"#(log file "dir\\this is \"the\" filename.ext")#";
    shop_command cmd{command_string};
    CHECK(cmd.keyword == "log"s);
    CHECK(cmd.specifier == "file"s);
    CHECK(cmd.options.size() == 0);
    CHECK(cmd.objects.size() == 1);
    if (cmd.objects.size() == 1) {
      CHECK(cmd.objects.front() == R"#(dir\this is "the" filename.ext)#"s);
    }
    CHECK((std::string) cmd == (std::string) command_string);
    shop_command cmd2;
    cmd2.parse_strict(command_string);
    CHECK(cmd == cmd2);
    shop_command cmd3 = shop_command::log_file(R"#(dir\this is "the" filename.ext)#");
    CHECK(cmd == cmd3);
  }
  SUBCASE("nohikkemikk") {
    char const * const command_string = "print bp_curves /nohikkemikk /production 0 24";
    shop_command cmd{command_string};
    REQUIRE(cmd.has_nohikkemikk());
    REQUIRE(cmd.options.size() == 2);
    CHECK(cmd.options[0] == "nohikkemikk");
    CHECK(cmd.options[1] == "production");
    cmd.remove_nohikkemikk();
    REQUIRE(!cmd.has_nohikkemikk());
    CHECK(cmd.keyword == "print");
    CHECK(cmd.specifier == "bp_curves");
    REQUIRE(cmd.options.size() == 1);
    CHECK(cmd.options[0] == "production");
    REQUIRE(cmd.objects.size() == 2);
    CHECK(cmd.objects[0] == "0");
    CHECK(cmd.objects[1] == "24");
  }
}

TEST_CASE("stm/shop/calc_suggested_timelimit") {
  using shyft::energy_market::stm::shop::shop_command;
  using shyft::energy_market::stm::shop::calc_suggested_timelimit;

  auto const default_limit = shyft::core::from_seconds(3600);
  auto const multiplier = 1.5;

  SUBCASE("Sum timelimits as expected") {
    const std::vector<shop_command> cmds{
      shop_command::set_code_full(),
      shop_command::set_timelimit(300), // will be overwritten by next
      shop_command::set_timelimit(500),
      shop_command::start_sim(3),
      shop_command::set_code_incremental(),
      shop_command::set_timelimit(200),
      shop_command::start_sim(2),
      shop_command::set_timelimit(600) // will be ignored
    };
    CHECK_EQ(calc_suggested_timelimit(cmds), shyft::core::from_seconds((500 * 3 + 200 * 2) * multiplier));
  }

  SUBCASE("Ignore timelimits if not provided before a start command") {
    std::vector<shop_command> cmds{
      shop_command::set_code_full(),
      shop_command::set_timelimit(500),
      shop_command::start_sim(3),
      shop_command::set_code_incremental(),
      shop_command::start_sim(3) // no timelimit set since last start
    };
    CHECK_EQ(calc_suggested_timelimit(cmds), default_limit);

    cmds = {
      shop_command::set_code_full(),
      shop_command::start_sim(3), // no timelimit before start
      shop_command::set_timelimit(500),
      shop_command::set_code_incremental(),
      shop_command::start_sim(3)};
    CHECK_EQ(calc_suggested_timelimit(cmds), default_limit);
  }

  SUBCASE("No timelimits provided") {
    const std::vector<shop_command> cmds{
      shop_command::set_code_full(),
      shop_command::start_sim(3),
      shop_command::set_code_incremental(),
      shop_command::start_sim(3)};
    CHECK_EQ(calc_suggested_timelimit(cmds), default_limit);
  }
}

TEST_SUITE_END();
