#include <algorithm>
#include <cstdint>
#include <functional>
#include <ranges>

#include <doctest/doctest.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/shop/shop_system.h>

#include "model_simple.h"
#include "model_shop_pump_example.h"
#include "model_ole_pump.h"

using shyft::time_axis::generic_dt;

TEST_SUITE_BEGIN("stm/shop");

auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
auto const t_step = shyft::core::deltahours(1);
const size_t n_step = 18;
const generic_dt time_axis{t_begin, t_step, n_step};

const shyft::core::utcperiod t_period{time_axis.total_period()};
auto const t_end = t_period.end;

static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_file_commands==true.

TEST_CASE(
  "stm/shop/optimize_simple_model_without_inlet"
  * doctest::description("building and optmimizing simple model without inlet segment: "
                         "aggregate-penstock-maintunnel-reservoir")) {
  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;

  auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
  auto rstm = build_simple_model(
    t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with expected results
  shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
  check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE(
  "stm/shop/optimize_simple_model_with_inlet"
  * doctest::description("building and optmimizing simple model with inlet segment: "
                         "aggregate-inlet-penstock-maintunnel-reservoir")) {
  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
  auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);
  shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
  check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE(
  "stm/shop/optimize_simple_model_with_discharge_group"
  * doctest::description("building and optmimizing simple model without explicit values for "
                         "lrl/hrl/maxvol/p_min/p_max/p_nom")) {
  using namespace shyft::energy_market;
  bool const always_inlet_tunnels = false;
  bool const use_defaults = true;

  // Build models
  auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
  auto rstm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

  // Fetch stm objects connected to shop objects
  auto rsv_ = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(
    stm->hps.front()->find_reservoir_by_name("reservoir"));
  auto unit_ = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(
    stm->hps.front()->find_unit_by_name("aggregate"));
  auto flood_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
    stm->hps.front()->find_waterway_by_name("waterroute flood river"));
  auto bypass_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
    stm->hps.front()->find_waterway_by_name("waterroute bypass"));
  auto bypass_gate_ = std::dynamic_pointer_cast<shyft::energy_market::stm::gate>(bypass_river_->gates.front());
  auto river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
    stm->hps.front()->find_waterway_by_name("waterroute river"));

  // Decorate connection with attrs to enforce discharge group
  river_->discharge.reference = make_constant_ts(t_begin, t_end, 0);
  river_->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 1e6);
  river_->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);
  river_->deviation.realised = make_constant_ts(t_begin, t_end, 1e6);
  //
  // Set buypass schedule, to check if river_ gets input from both production and bypass
  bypass_gate_->discharge.schedule = make_constant_ts(t_begin, t_end, 10);

  generic_dt time_axis{t_begin, t_step, n_step};
  generic_dt time_axis_accumulate{t_begin, t_step, n_step + 1};
  shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
  shyft::energy_market::stm::shop::shop_system::optimize(*rstm, time_axis, optimization_commands(run_id));
  check_results_with_discharge_group(stm, rstm, t_begin, t_end, t_step);
}


TEST_CASE("stm/shop/optimize_contract") {

  using namespace shyft::energy_market;
  bool const always_inlet_tunnels = false;
  bool const use_defaults = true;
  double const mega = 1e6;

  // Build models
  auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
  auto rstm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

  // Fetch stm objects connected to shop objects
  auto mkt_ = stm->market.front();
  auto rsv_ = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(
    stm->hps.front()->find_reservoir_by_name("reservoir"));
  auto unit_ = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(
    stm->hps.front()->find_unit_by_name("aggregate"));
  auto flood_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
    stm->hps.front()->find_waterway_by_name("waterroute flood river"));
  auto bypass_river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
    stm->hps.front()->find_waterway_by_name("waterroute bypass"));
  auto bypass_gate_ = std::dynamic_pointer_cast<shyft::energy_market::stm::gate>(bypass_river_->gates.front());
  auto river_ = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(
    stm->hps.front()->find_waterway_by_name("waterroute river"));

  SUBCASE("valid"){
    auto ctr_ = std::make_shared<shyft::energy_market::stm::contract>(1, "c1", "{}", stm);
    ctr_->options = std::make_shared<std::map<utctime, shyft::energy_market::hydro_power::xy_point_curve_>>();
    // TODO: Not sure what input data is realistic here, have just set something that produced result!
    ctr_->options.get()->emplace(
      time_axis.time(6),
      std::make_shared<shyft::energy_market::hydro_power::xy_point_curve>(
        shyft::energy_market::hydro_power::xy_point_curve::make(
          {-4.4 * mega,
           -3.3 * mega,
           5.5 * mega,
           6.6 * mega}, // X: Energy volume, negative is sale, positive is buy, unit W in STM, unit MW in Shop
          {32.0 / mega, 33.0 / mega, 34.0 / mega, 50.0 / mega}))); // Y: Price, unit NOK/W in STM, unit NOK/MW in Shop
    ctr_->options.get()->emplace(
      time_axis.time(12),
      std::make_shared<shyft::energy_market::hydro_power::xy_point_curve>(
        shyft::energy_market::hydro_power::xy_point_curve::make(
          {-4.0 * mega,
           -2.0 * mega,
           5.0 * mega,
           6.0 * mega}, // X: Energy volume, negative is sale, positive is buy, unit W in STM, unit MW in Shop
          {32.0 / mega, 33.0 / mega, 34.0 / mega, 40.0 / mega}))); // Y: Price, unit NOK/W in STM, unit NOK/MW in Shop
    stm->contracts.push_back(ctr_);
    mkt_->contracts.push_back(ctr_);

    shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id));

    CHECK(exists(ctr_->quantity));
    auto expected_quantity = apoint_ts{
      shyft::time_axis::point_dt{{t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
      {6.6 * mega, 6.6 * mega, 6.0 * mega, 6.0 * mega}, // NOTE: The 6.6 value set at time(6) has retroactive effect!?
      shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
    };
    check_time_series_at_points(ctr_->quantity, expected_quantity, t_begin, t_step, {0, 6, 12, 17});

  }
  SUBCASE("invalid"){

    SUBCASE("0"){
      auto ctr_ = std::make_shared<shyft::energy_market::stm::contract>(1, "c1", "{}", stm);
      ctr_->options = std::make_shared<std::map<utctime, shyft::energy_market::hydro_power::xy_point_curve_>>();
      stm->contracts.push_back(ctr_);
      mkt_->contracts.push_back(ctr_);
      shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
      CHECK(!exists(ctr_->quantity));
    }
    SUBCASE("1"){
      auto ctr_ = std::make_shared<shyft::energy_market::stm::contract>(1, "c1", "{}", stm);
      apoint_ts ctr_ts{
        shyft::time_axis::point_dt{{t_begin,t_begin+1*t_step}},
        {4.0},
        shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      ctr_->quantity = ctr_ts;
      stm->contracts.push_back(ctr_);
      mkt_->contracts.push_back(ctr_);
      shyft::energy_market::stm::shop::shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
      CHECK(exists(ctr_->quantity));
      CHECK(ctr_->quantity.ts == ctr_ts.ts);
    }

  }
}

TEST_CASE(
  "stm/shop/optimize_model_w_3_units_and_reserves"
  * doctest::description("building and optmimizing model with 3 units")) {
  using namespace shyft::energy_market;
  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto stm = build_simple_model_n_units_with_reserves(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);

  shyft::energy_market::stm::shop::shop_system shop{time_axis};
  shop.emit(*stm);
  auto cmds = optimization_commands(run_id);
  cmds.insert(cmds.begin(), stm::shop::shop_command::set_universal_mip_on()); // let shop find the ideal combination
  for (auto const &cmd : cmds)
    shop.commander.execute(cmd);
  shop.collect(*stm);
  shop.complete(*stm);
  for (auto &wx : stm->hps.front()->waterways) {
    auto w = std::dynamic_pointer_cast<stm::waterway>(wx);
    if (w) {
      CHECK_UNARY(
        w->discharge.result.size() > 0); // verify all water way discharges are filled (done by shop.update call above)
    }
  }

  apoint_ts run_ts(time_axis, 1.0, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  auto m = stm->market.front();

  // Verify reserve results: Sum of unit group schedules vs unit results for each reserve type.
  // Assuming the schedule will be followed 100% in correct test, not sure if this will always be true.
  REQUIRE_EQ(stm->unit_groups.size(), 4);
  REQUIRE_EQ(stm->hps.front()->units.size(), 3);
  std::vector reserve_types{stm::unit_group_type::fcr_n_up, stm::unit_group_type::afrr_up};
  for (auto reserve_type : reserve_types) {
    apoint_ts schedule{time_axis, 0.0, shyft::time_series::POINT_AVERAGE_VALUE};
    for (auto const &ug : stm->unit_groups) {
      if (ug->group_type == reserve_type) {
        schedule = schedule + ug->obligation.schedule;
      }
    }
    apoint_ts result{time_axis, 0.0, shyft::time_series::POINT_AVERAGE_VALUE};
    for (auto const &u_ : stm->hps.front()->units) {
      auto u = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
      if (reserve_type == stm::unit_group_type::fcr_n_up) {
        result = result + u->reserve.fcr_n.up.result;
      } else if (reserve_type == stm::unit_group_type::afrr_up) {
        result = result + u->reserve.afrr.up.result;
      }
    }
    CHECK_MESSAGE(schedule == result, fmt::format("Unexpected sum of unit reserves for {}", reserve_type));
  }
}

TEST_CASE("stm/shop/optimize_multi_market" * doctest::description("multimarket demand/supply 3 units")) {
  using namespace shyft::energy_market;
  using xy_point = shyft::energy_market::hydro_power::point;
  using xy_points = shyft::energy_market::hydro_power::xy_point_curve;
  using shyft::energy_market::hydro_power::xy_point_curve_;
  using stm::t_xy_;
  using std::vector;
  using namespace shyft::time_series::dd;

  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto stm = build_simple_model_n_units_with_reserves(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);
  // modify the system to use demand/supply tables:
  double const mega = 1e6;
  auto ema = stm->market[0];
  ema->demand.bids = std::make_shared<t_xy_::element_type>( // shop sale opportunities part
    t_xy_::element_type{
      { time_axis.time(0),std::make_shared<xy_points>(vector<xy_point>{{48.0 / mega, 10 * mega}})                          },
      { time_axis.time(6),           std::make_shared<xy_points>(vector<xy_point>{{40.0 / mega, 220.0 * mega}})},
      { time_axis.time(7),
       std::make_shared<xy_points>(
       vector<xy_point>{{30.0 / mega, 120.0 * mega}, {50.0 / mega, 80.0 * mega}, {55.0 / mega, 20.0 * mega}})  },
      { time_axis.time(8),           std::make_shared<xy_points>(vector<xy_point>{{30.0 / mega, 220.0 * mega}})},
      {time_axis.time(12),           std::make_shared<xy_points>(vector<xy_point>{{48.0 / mega, 220.0 * mega}})},
      {             t_end,           std::make_shared<xy_points>(vector<xy_point>{{35.0 / mega, 220.0 * mega}})},
  });
  double const buy = 1.0;                                   // set 0 to close shop ability to buy,
  ema->supply.bids = std::make_shared<t_xy_::element_type>( // shop buy oppportunities part
    t_xy_::element_type{
      { time_axis.time(0),std::make_shared<xy_points>(vector<xy_point>{{148.0 / mega, buy * 10 * mega}})                          },
      { time_axis.time(1),
       std::make_shared<xy_points>(vector<xy_point>{{10.0 / mega, buy * 5 * mega}, {15.0 / mega, buy * 20 * mega}})},
      { time_axis.time(2),            std::make_shared<xy_points>(vector<xy_point>{{50.0 / mega, buy * 10 * mega}})},
      { time_axis.time(6),          std::make_shared<xy_points>(vector<xy_point>{{45.0 / mega, buy * 30.0 * mega}})},
      { time_axis.time(7),          std::make_shared<xy_points>(vector<xy_point>{{37.0 / mega, buy * 30.0 * mega}})},
      {time_axis.time(12),          std::make_shared<xy_points>(vector<xy_point>{{50.0 / mega, buy * 30.0 * mega}})},
      {             t_end,          std::make_shared<xy_points>(vector<xy_point>{{36.0 / mega, buy * 30.0 * mega}})},
  });
  // close primary market object sale/buy parts. zero price, keep load
  ema->max_buy = apoint_ts{time_axis, 0.0, POINT_AVERAGE_VALUE};
  ema->max_sale = apoint_ts{time_axis, 0.0, POINT_AVERAGE_VALUE};

  for (auto const &u_ : stm->hps.front()->units) {
    auto u = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(u_);
    u->reserve.droop_steps = std::make_shared<t_xy_::element_type>(t_xy_::element_type{
      {time_axis.time(0), std::make_shared<xy_points>(vector<xy_point>{{1, 4}, {2, 6}, {3, 8}, {4, 10}})}
    });
  }
  // modify the market, so that we have
  shyft::energy_market::stm::shop::shop_system shop{time_axis};
  shop.emit(*stm);
  using namespace shyft::energy_market::stm::shop;
  std::vector<shop_command> cmds{
    shop_command::set_universal_mip_on(),
    shop_command::set_droop_discretization_limit(6.0), // verify it works, needs license, otherwise it will fail.
    shop_command::set_method_primal(),
    shop_command::set_code_full(),
    shop_command::start_sim(3),
    shop_command::set_code_incremental(),
    shop_command::start_sim(3),
  };

  shop_global_settings globs = shop.get_global_settings();
  auto lpen = globs.load_penalty_flag.get();
  auto lpenv = globs.load_penalty_cost.get();
  auto g = globs.gravity.get();
  MESSAGE("globs values " << lpen << "," << lpenv << ",g=" << g);
  for (auto const &cmd : cmds)
    shop.commander.execute(cmd);
  shop.collect(*stm);

  apoint_ts run_ts(time_axis, 1.0, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  auto m = stm->market.front();

  // Verify reserve results: Sum of unit group schedules vs unit results for each reserve type.
  // Assuming the schedule will be followed 100% in correct test, not sure if this will always be true.
  REQUIRE_EQ(stm->unit_groups.size(), 4);
  REQUIRE_EQ(stm->hps.front()->units.size(), 3);
  // verify plant instant_max
  ats_vector u_prods;
  for (auto const &u_ : stm->hps.front()->units) {
    u_prods.push_back(std::dynamic_pointer_cast<stm::unit>(u_)->production.result);
  }
  auto pp = std::dynamic_pointer_cast<stm::power_plant>(stm->hps.front()->power_plants.front());
  auto ok_instant_max = (pp->production.instant_max - u_prods.sum()).inside(0.0, 1e9, 0.0, 1.0, 0.0).evaluate();
  CHECK_GE(ok_instant_max.size(), 1);
  for (auto i = 0u; i < ok_instant_max.size(); ++i) {
    CHECK_GT(ok_instant_max.value(i), 0.1);
  }

  std::vector reserve_types{stm::unit_group_type::fcr_n_up, stm::unit_group_type::afrr_up};
  for (auto reserve_type : reserve_types) {
    apoint_ts schedule{time_axis, 0.0, shyft::time_series::POINT_AVERAGE_VALUE};
    for (auto const &ug : stm->unit_groups) {
      if (ug->group_type == reserve_type) {
        schedule = schedule + ug->obligation.schedule;
      }
    }
    apoint_ts result{time_axis, 0.0, shyft::time_series::POINT_AVERAGE_VALUE};
    for (auto const &u_ : stm->hps.front()->units) {
      auto u = std::dynamic_pointer_cast<stm::unit>(u_);
      if (reserve_type == stm::unit_group_type::fcr_n_up) {
        result = result + u->reserve.fcr_n.up.result;
      } else if (reserve_type == stm::unit_group_type::afrr_up) {
        result = result + u->reserve.afrr.up.result;
      }
    }
    CHECK_MESSAGE(schedule == result, fmt::format("Unexpected sum of unit reserves for {}", reserve_type));
  }
}

TEST_CASE("stm/shop/optimize_pumps") {

  SUBCASE("reversible") {

    shyft::energy_market::stm::shop::shop_system shop{time_axis};
    auto system = shyft::energy_market::stm::test_models::build_shop_pump_example(t_begin, t_end);

    CHECK_NOTHROW(shop.emit(*system));
    {
      using sc = shyft::energy_market::stm::shop::shop_command;
      for (
        auto const &cmd : std::vector{
          sc::set_method_primal(), sc::set_code_full(), sc::start_sim(3), sc::set_code_incremental(), sc::start_sim(3)})
        shop.commander.execute(cmd);
    }

    {
      auto log_entries = shop.get_log_buffer();
      auto is_error = [&](auto const &l) {
        return l.severity == shyft::energy_market::stm::log_severity::error;
      };
      auto error_log_entries = std::views::filter(log_entries, is_error);

      for (auto const &log_entry : log_entries)
        INFO(log_entry.message);

      REQUIRE(std::ranges::empty(error_log_entries));
    }
    CHECK_NOTHROW(shop.collect(*system));

    REQUIRE(system->hps.size() == 1);
    auto hps = system->hps[0];

    REQUIRE(hps->power_plants.size() == 1);
    auto p0 = std::dynamic_pointer_cast<shyft::energy_market::stm::power_plant>(hps->power_plants[0]);
    REQUIRE(p0);

    auto const &r = p0->production.result;

    auto production_result = std::views::transform(std::views::iota(std::size_t{0}, n_step), [&](auto i) {
      return r(t_begin + i * t_step);
    });

    auto const n_low_price_steps = 6;

    // FIXME: use bind_back & swap greater / less when we are compiling with C++23.
    //        less confusing to read. - jeh
    auto low_price_production_result = std::views::take(production_result, n_low_price_steps);
    CHECK(std::ranges::all_of(low_price_production_result, std::bind_front(std::ranges::greater_equal{}, 0.0)));
    CHECK(std::ranges::any_of(low_price_production_result, std::bind_front(std::ranges::greater{}, 0.0)));

    auto high_price_production_result = std::views::drop(production_result, n_low_price_steps);
    CHECK(std::ranges::all_of(high_price_production_result, std::bind_front(std::ranges::less_equal{}, 0.0)));
    CHECK(std::ranges::any_of(high_price_production_result, std::bind_front(std::ranges::less{}, 0.0)));
  }

  SUBCASE("ole") {

    const generic_dt time_axis{t_begin, shyft::core::deltahours(1), 23};

    shyft::energy_market::stm::shop::shop_system shop{time_axis};
    auto system = shyft::energy_market::stm::test_models::build_ole_pump(t_begin);

    CHECK_NOTHROW(shop.emit(*system));


    {
      using sc = shyft::energy_market::stm::shop::shop_command;
      for (auto const &cmd : std::vector{
             sc::set_mipgap(true, 300),
             sc::set_nseg_all(25),
             sc::set_dyn_seg_on(),
             sc::set_fcr_n_equality(true),
             sc::set_power_head_optimization(true),
             sc::set_max_num_threads(8),
             sc::start_sim(3),
             sc::set_code_incremental(),
             sc::start_sim(3)})
        shop.commander.execute(cmd);
    }


    {
      auto log_entries = shop.get_log_buffer();
      auto is_error = [&](auto const &l) {
        return l.severity == shyft::energy_market::stm::log_severity::error;
      };
      auto error_log_entries = std::views::filter(log_entries, is_error);

      for (auto const &log_entry : log_entries)
        MESSAGE(log_entry.message);

      REQUIRE(std::ranges::empty(error_log_entries));
    }
    REQUIRE_NOTHROW(shop.collect(*system));
    REQUIRE_NOTHROW(shop.complete(*system));

    REQUIRE(system->hps.size() == 1);
    auto hps = system->hps[0];

    REQUIRE(hps->power_plants.size() == 1);
    auto p0 = std::dynamic_pointer_cast<shyft::energy_market::stm::power_plant>(hps->power_plants[0]);
    REQUIRE(p0);

    REQUIRE(p0->production.result);

    REQUIRE(p0->units.size() == 1);
    auto u0 = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(p0->units[0]);
    REQUIRE(u0);

    REQUIRE(hps->reservoirs.size() == 2);
    auto r0 = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps->reservoirs[0]);
    REQUIRE(r0);
    auto r1 = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps->reservoirs[1]);
    REQUIRE(r1);

    CHECK(u0->production.result);
    CHECK(u0->discharge.result);

    auto ts_view = [&](auto &&ts) {
      return std::views::transform(std::views::iota(std::size_t{0}, n_step), [&](auto i) {
        return ts(t_begin + i * t_step);
      });
    };
    auto production_result = ts_view(u0->production.result);
    auto discharge_result = ts_view(u0->discharge.result);

    // NOTE:
    //     that pumps are running for at least one timestep
    //     - jeh
    CHECK_MESSAGE(
      std::ranges::all_of(production_result, std::bind_front(std::ranges::greater_equal{}, 0.0)),
      fmt::format("{}", production_result));
    CHECK_MESSAGE(
      std::ranges::any_of(production_result, std::bind_front(std::ranges::greater{}, 0.0)),
      fmt::format("{}", production_result));
    CHECK_MESSAGE(
      std::ranges::all_of(discharge_result, std::bind_front(std::ranges::greater_equal{}, 0.0)),
      fmt::format("{}", discharge_result));
    CHECK_MESSAGE(
      std::ranges::any_of(discharge_result, std::bind_front(std::ranges::greater{}, 0.0)),
      fmt::format("{}", discharge_result));
  }
}

TEST_SUITE_END();
