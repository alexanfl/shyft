#include <csignal>
#include <cstdint>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <doctest/doctest.h>
#include <fmt/core.h>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/web_api/energy_market/request_handler.h>

#include "model_simple.h"
#include <test/test_utils.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::core::utctime_now;
  using shyft::core::to_seconds64;
  using shyft::core::from_seconds;

  namespace {

    struct test_server : srv::dstm::server {
      shyft::web_api::energy_market::request_handler bg_server;
      std::future<int> web_srv; ///< mutex,

      //-- to verify fx-callback
      std::string fx_mid;
      std::string fx_arg;

      bool fx_handler(std::string mid, std::string json_arg) {
        fx_mid = mid;
        fx_arg = json_arg;
        return true;
      }

      explicit test_server()
        : srv::dstm::server()
        , bg_server{models, sm, fx_cb, dtss.get()} {
        this->fx_cb = [this](std::string m, std::string a) -> bool {
          return this->fx_handler(m, a);
        };
      }

      explicit test_server(std::string const & root_dir)
        : srv::dstm::server()
        , bg_server{models, sm, fx_cb, dtss.get()} {
        dtss->add_container("test", root_dir);
      }

      void start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads) {
        if (!web_srv.valid()) {
          web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads]() -> int {
            return shyft::web_api::run_web_server(
              bg_server,
              host_ip,
              static_cast<unsigned short>(port),
              std::make_shared<std::string>(doc_root),
              fg_threads,
              bg_threads);
          });
        }
      }

      bool web_api_running() const {
        return web_srv.valid();
      }

      void stop_web_api() {
        if (web_srv.valid()) {
          std::raise(SIGINT);
          (void) web_srv.get();
        }
      }
    };

    //-- test client
    using tcp = boost::asio::ip::tcp;              // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket; // from <boost/beast/websocket.hpp>
    using boost::system::error_code;

    unsigned short get_port() {
      using namespace boost::asio;
      io_service service;
      ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0)); // pass in 0 to get a free port.
      return acceptor.local_endpoint().port();
    }

    // Sends a WebSocket message and prints the response, from examples made by boost.beast/Vinnie Falco
    class session : public std::enable_shared_from_this<session> {
      tcp::resolver resolver_;
      websocket::stream<tcp::socket> ws_;
      boost::beast::multi_buffer buffer_;
      std::string host_;
      std::string port_;
      std::string text_;
      std::string response_;
      std::string fail_;
      std::function<std::string(std::string const &)> report_response;

      // Report a failure
      void fail(error_code ec, char const * what) {
        fail_ = std::string(what) + ": " + ec.message() + "\n";
      }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));
     public:
      // Resolver and socket require an io_context
      explicit session(boost::asio::io_context& ioc)
        : resolver_(ioc)
        , ws_(ioc) {
      }

      std::string response() const {
        return response_;
      }

      std::string diagnostics() const {
        return fail_;
      }

      // Start the asynchronous operation
      template <class Fx>
      void run(std::string_view host, int port, std::string_view text, Fx&& rep_response) {
        // Save these for later
        host_ = host;
        text_ = text;
        port_ = std::to_string(port);
        report_response = rep_response;
        resolver_.async_resolve(
          host_,
          port_, // Look up the domain name
          [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
            me->on_resolve(ec, results);
          });
      }

      void on_resolve(error_code ec, tcp::resolver::results_type results) {
        fail_on_error(ec, "resolve");
        // Make the connection on the IP address we get from a lookup
        boost::asio::async_connect(
          ws_.next_layer(),
          results.begin(),
          results.end(),
          // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
          std::bind(&session::on_connect, shared_from_this(), std::placeholders::_1));
      }

      void on_connect(error_code ec) {
        fail_on_error(ec, "connect");
        ws_.async_handshake(
          host_,
          "/", // Perform the websocket handshake
          [me = shared_from_this()](error_code ec) {
            me->on_handshake(ec);
          });
      }

      void on_handshake(error_code ec) {
        fail_on_error(ec, "handshake");
        if (text_.size()) {
          ws_.async_write( // Send the message
            boost::asio::buffer(text_),
            [me = shared_from_this()](error_code ec, std::size_t bytes_transferred) {
              me->on_write(ec, bytes_transferred);
            });
        } else { // empty text to send means we are done and should close connection
          ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
            me->on_close(ec);
          });
        }
      }

      void on_write(error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        fail_on_error(ec, "write");

        ws_.async_read(
          buffer_, // Read a message into our buffer
          [me = shared_from_this()](error_code ec, std::size_t bytes_transferred) {
            me->on_read(ec, bytes_transferred);
          });
      }

      void on_read(error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        fail_on_error(ec, "read");
        response_ = boost::beast::buffers_to_string(buffer_.data());
        buffer_.consume(buffer_.size());
        text_ = report_response(response_);
        if (text_.size()) { // more messages to send?
          ws_.async_write(  // send the message..
            boost::asio::buffer(text_),
            [me = shared_from_this()](error_code ec, std::size_t bytes_transferred) {
              me->on_write(ec, bytes_transferred);
            });
        } else { // else close it.
          ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
            me->on_close(ec);
          });
        }
      }

      void on_close(error_code ec) {
        fail_on_error(ec, "close");
      }

#undef fail_on_error
    };

  }

  TEST_SUITE_BEGIN("stm/shop");

  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto const t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
  auto const t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
  auto const t_step = shyft::core::deltahours(1);
  auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
  const shyft::time_axis::generic_dt ta{t_begin, t_step, n_steps};

  using shyft::energy_market::stm::log_entry;
  using shyft::energy_market::stm::log_severity;

  TEST_CASE("stm/get_log") {
    dlib::set_all_logging_levels(dlib::LALL);
    //-- keep test directory, unique, and with auto-cleanup.
    auto dirname = fmt::format("shop.web_api.test.{}", to_seconds64(utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    // dir_cleanup wipe{tmpdir}; // note..: ~path raises a SIGSEGV fault when boost is built with different compile
    // flags than shyft.
    //  See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem
    //  However, this directory is required, but never used in this test.

    // Set up test server:
    test_server a;
    std::string host_ip{"127.0.0.1"};
    a.set_listening_ip(host_ip);
    a.start_server();
    // Store some models:
    auto mdl = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    a.do_add_model("simple", mdl);
    // Add some messages:
    auto smdl = a.models.find("simple").get();
    REQUIRE(smdl);
    smdl->info->log = {
      log_entry{log_severity::information, "An informational message", 1145, from_seconds(0)},
      log_entry{    log_severity::warning,        "A warning message", 3301, from_seconds(1)}
    };

    int port = get_port();
    a.start_web_api(host_ip, port, doc_root, 1, 1);

    std::this_thread::sleep_for(std::chrono::milliseconds(700));
    REQUIRE_EQ(true, a.web_api_running());
    std::vector<std::string> requests{
      R"_(get_log {"request_id": "1", "model_key": "simple","log_version":0,"bookmark":1})_",// should pick last msg,
      R"_(get_state {"request_id": "2", "model_key": "simple"})_",
      R"_(get_log {"request_id": "3", "model_key": "simple","log_version":1,"bookmark":2,"severity_level":0})_",//all msg, due to wrong log_version
      R"_(get_log {"request_id": "4", "model_key": "simple","log_version":0,"bookmark":0,"severity_level":1})_",//only warings, 1 msg,
      R"_(get_log {"request_id": "5", "model_key": "simple","log_version":0,"bookmark":2,"severity_level":0})_" // empty, we are at the end
    };
    std::vector<std::string> responses;
    responses.reserve(requests.size());
    std::size_t r = 0;
    {
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<session>(ioc);
      s1->run(host_ip, port, requests[r], [&responses, &r, &requests](std::string const & web_response) -> std::string {
        responses.push_back(web_response);
        ++r;
        return r >= requests.size() ? std::string("") : requests[r];
      });

      ioc.run();
      s1.reset();
    }

    std::vector<std::string> expected{
      R"_({"request_id":"1","log_version":0,"bookmark":2,"result":[{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]})_",
      R"_({"request_id":"2","result":{"state":"idle"}})_",
      R"_({"request_id":"3","log_version":0,"bookmark":2,"result":[{"time":0.0,"severity":"INFORMATION","code":1145,"message":"An informational message"},{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]})_",
      R"_({"request_id":"4","log_version":0,"bookmark":2,"result":[{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]})_",
        //{"request_id":"4","log_version":0,"bookmark":2,"result":[{"time":0.0,"severity":"INFORMATION","code":1145,"message":"An informational message"},{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]}
      R"_({"request_id":"5","log_version":0,"bookmark":2,"result":[]})_"
    };

    for (std::size_t i = 0; i < expected.size(); i++) {
      if (!expected[i].empty()) {
        CHECK_EQ(responses[i], expected[i]);
      } else {
        CHECK_EQ(responses[i].size(), 10);
      }
    }
    a.stop_web_api();
  }

}

TEST_SUITE_END();
