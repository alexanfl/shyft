#include <memory>
#include <vector>
#include <chrono>

#include <doctest/doctest.h>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

TEST_SUITE_BEGIN("stm/shop");

using std::make_shared;
using std::make_unique;
using std::vector;
using shyft::core::utctime;
using shyft::core::utcperiod;
using shyft::core::utctimespan;
using shyft::core::max_utctime;
using shyft::core::deltahours;
using shyft::core::deltaminutes;
using shyft::core::to_seconds64;
using shyft::core::utctime_from_seconds64;
using shyft::core::create_from_iso8601_string;
using shyft::time_axis::generic_dt;
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::POINT_AVERAGE_VALUE;
using shyft::time_series::POINT_INSTANT_VALUE;
using timing = std::chrono::high_resolution_clock;

auto elapsed_ms = [](timing::time_point t0, timing::time_point t1) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
};
auto elapsed_us = [](timing::time_point t0, timing::time_point t1) {
  return std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();
};

template <int scale>
struct unit { // Simple stand-alone version of shop::proxy::unit

  template <typename V>
  static constexpr V to_base(V v) {
    return v * scale;
  };

  template <typename V>
  static constexpr V from_base(V v) {
    return v / scale;
  };
};

TEST_CASE("stm/shop/test_txy_factory") {
  const shyft::core::calendar calendar{"Europe/Oslo"};

  SUBCASE("basic") {
    // Configuration
    int const n{10};
    auto const t_begin = create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = deltahours(1);
    auto const shop_step_resolution = deltaminutes(
      1); // Shop takes timestamps as number of seconds relative to start with minute as finest supported resolution.
    double const v_begin{3.0};
    int const v_scale{1};
    using v_unit = unit<v_scale>;

    // Generate data
    vector<utctime> ta_utc_points;
    ta_utc_points.reserve(n + 1);
    vector<time_t> t_axis;
    t_axis.reserve(n + 1);
    vector<int> t_shop;
    t_shop.reserve(n);
    vector<double> v;
    v.reserve(n);
    for (int i = 0; i < n; ++i) {
      ta_utc_points.push_back(t_begin + i * t_step);
      t_axis.push_back(to_seconds64(t_begin + i * t_step));
      t_shop[i] = i * (t_step / shop_step_resolution);
      v.push_back(v_begin * i);
    }
    t_axis.push_back(to_seconds64(t_begin + n * t_step));
    ta_utc_points.push_back(t_begin + n * t_step);

    // Create shyft ts and shop data txy objects
    auto const expected_ts = apoint_ts(generic_dt{ta_utc_points}, v, POINT_AVERAGE_VALUE);
    auto t = make_unique<int[]>(n);
    auto y = make_unique<double[]>(n);
    for (int i = 0; i < n; ++i) {
      t[i] = t_shop[i];
      y[i] = v_unit::from_base(v[i]);
    }
    auto const expected_txy = shop::data::TXY(
      shop::data::shop_time{(time_t) to_seconds64(t_begin)}, n, std::move(t), std::move(y));

    // Test conversion of shyft ts to shop data txy
    {
      auto txy = shop::data::txy_factory<apoint_ts, v_unit>::convert_to_shop(expected_ts, t_axis);
      CHECK(txy.start == expected_txy.start);
      for (int i = 0; i < n; ++i) {
        CHECK(txy.t[i] == expected_txy.t[i]);
        CHECK(txy.y[i] == expected_txy.y[i]);
      }
    }

    // Test conversion of shop data txy to shyft ts
    {
      t = make_unique<int[]>(n);
      y = make_unique<double[]>(n);
      for (int i = 0; i < n; ++i) {
        t[i] = t_shop[i];
        y[i] = v_unit::from_base(v[i]);
      }
      auto ts = shop::data::txy_factory<apoint_ts, v_unit>::create(t_axis, n, std::move(t), std::move(y));
      CHECK(ts == expected_ts);
    }
  }

  SUBCASE("scaling") {
    // Configuration
    int const n{10};
    auto const t_begin = create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = deltahours(1);
    auto const shop_step_resolution = deltaminutes(
      1); // Shop takes timestamps as number of seconds relative to start with minute as finest supported resolution.
    double const v_begin{3000000.0};
    int const v_scale{1000000}; // Mega, as in MW
    using v_unit = unit<v_scale>;

    // Generate data
    vector<utctime> ta_utc_points;
    ta_utc_points.reserve(n + 1);
    vector<time_t> t_axis;
    t_axis.reserve(n + 1);
    vector<int> t_shop;
    t_shop.reserve(n);
    vector<double> v;
    v.reserve(n);
    for (int i = 0; i < n; ++i) {
      ta_utc_points.push_back(t_begin + i * t_step);
      t_axis.push_back(to_seconds64(t_begin + i * t_step));
      t_shop[i] = i * (t_step / shop_step_resolution);
      v.push_back(v_begin * i);
    }
    t_axis.push_back(to_seconds64(t_begin + n * t_step));
    ta_utc_points.push_back(t_begin + n * t_step);

    // Create shyft ts and shop data txy objects
    auto const expected_ts = apoint_ts(generic_dt{ta_utc_points}, v, POINT_AVERAGE_VALUE);
    auto t = make_unique<int[]>(n);
    auto y = make_unique<double[]>(n);
    for (int i = 0; i < n; ++i) {
      t[i] = t_shop[i];
      y[i] = v_unit::from_base(v[i]);
    }
    auto const expected_txy = shop::data::TXY(
      shop::data::shop_time{(time_t) to_seconds64(t_begin)}, n, std::move(t), std::move(y));

    // Test conversion of shyft ts to shop data txy
    {
      auto txy = shop::data::txy_factory<apoint_ts, v_unit>::convert_to_shop(expected_ts, t_axis);
      CHECK(txy.start == expected_txy.start);
      for (int i = 0; i < n; ++i) {
        CHECK(txy.t[i] == expected_txy.t[i]);
        CHECK(txy.y[i] == expected_txy.y[i]);
      }
    }

    // Test conversion of shop data txy to shyft ts
    {
      t = make_unique<int[]>(n);
      y = make_unique<double[]>(n);
      for (int i = 0; i < n; ++i) {
        t[i] = t_shop[i];
        y[i] = v_unit::from_base(v[i]);
      }
      auto ts = shop::data::txy_factory<apoint_ts, v_unit>::create(t_axis, n, std::move(t), std::move(y));
      CHECK(ts == expected_ts);
    }
  }

  SUBCASE("expression_to_shop") {
    // Configuration
    int const n{10000};
    auto const t_begin = create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = deltahours(1);
    auto const shop_step_resolution = deltaminutes(
      1); // Shop takes timestamps as number of seconds relative to start with minute as finest supported resolution.
    double const v_fill{99.0};
    int const v_scale{1};
    using v_unit = unit<v_scale>;

    // Generate data
    vector<time_t> t_axis;
    t_axis.reserve(n + 1);
    vector<int> t_shop;
    t_shop.reserve(n);
    for (int i = 0; i < n; ++i) {
      t_axis.push_back(to_seconds64(t_begin + i * t_step));
      t_shop[i] = i * (t_step / shop_step_resolution);
    }
    t_axis.push_back(to_seconds64(t_begin + n * t_step));

    // Create shyft ts
    auto work_ts = apoint_ts{
      generic_dt{t_begin, t_step, n},
      shyft::nan, POINT_AVERAGE_VALUE
    };
    auto override_ts = apoint_ts{
      generic_dt{t_begin, t_step, n},
      v_fill, POINT_AVERAGE_VALUE
    };
    auto qac_ts = work_ts.min_max_check_ts_fill(shyft::nan, shyft::nan, max_utctime, override_ts);

    // Test conversion of shyft ts to shop data txy
    auto txy = shop::data::txy_factory<apoint_ts, v_unit>::convert_to_shop(qac_ts, t_axis);
    auto start_time = shop::data::shop_time{(time_t) to_seconds64(t_begin)};
    CHECK(txy.start == start_time);
    for (int i = 0; i < n; ++i) {
      CHECK(txy.t[i] == t_shop[i]);
      CHECK(txy.y[i] == v_unit::from_base(v_fill));
    }
  }

  SUBCASE("complex_expression_to_shop") {
    // Configuration
    int const n{5000};
    int const n_tsv{4};
    auto const t_begin = create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = deltahours(1);
    auto const shop_step_resolution = deltaminutes(
      1); // Shop takes timestamps as number of seconds relative to start with minute as finest supported resolution.
    int const v_scale{1};
    using v_unit = unit<v_scale>;

    // Generate data
    // Making sure to create a point time axis, which gives the more heavy evaluation.
    vector<utctime> ta_utc_points;
    ta_utc_points.reserve(n);
    vector<time_t> t_axis;
    t_axis.reserve(n + 1);
    vector<int> t_shop;
    t_shop.reserve(n);
    for (int i = 0; i < n; ++i) {
      ta_utc_points.push_back(t_begin + i * t_step);
      t_axis.push_back(to_seconds64(t_begin + i * t_step));
      t_shop[i] = i * (t_step / shop_step_resolution);
    }
    t_axis.push_back(to_seconds64(t_begin + n * t_step));
    ta_utc_points.push_back(t_begin + n * t_step);
    auto const ta = generic_dt(ta_utc_points);

    // Create shyft ts (multiple steps)
    // First create a vector of time series, each of them a qac ts,
    // on a source ts which has use_time_axis_from from another qac ts,
    // using replacement values from a third qac ts for replacement.
    auto tsv = ats_vector{};
    tsv.reserve(n_tsv);
    for (auto i = 0; i < n_tsv; ++i) {
      auto work_ts_1 = apoint_ts{ta, shyft::nan, POINT_AVERAGE_VALUE};
      auto override_ts_1 = apoint_ts{ta, i + 0.11, POINT_AVERAGE_VALUE};
      auto qac_ts_1 = work_ts_1.min_max_check_ts_fill(shyft::nan, shyft::nan, max_utctime, override_ts_1);

      auto work_ts_2 = apoint_ts{ta, shyft::nan, POINT_AVERAGE_VALUE};
      auto override_ts_2 = apoint_ts{ta, i + 0.22, POINT_AVERAGE_VALUE};
      auto qac_ts_2 = work_ts_2.min_max_check_ts_fill(shyft::nan, shyft::nan, max_utctime, override_ts_2);

      auto work_ts_3 = apoint_ts{ta, shyft::nan, POINT_AVERAGE_VALUE};
      auto override_ts_3 = apoint_ts{ta, i + 0.33, POINT_AVERAGE_VALUE};
      auto qac_ts_3 = work_ts_2.min_max_check_ts_fill(shyft::nan, shyft::nan, max_utctime, override_ts_3);

      auto qac_ts_123 = qac_ts_3.use_time_axis_from(qac_ts_2).min_max_check_ts_fill(
        shyft::nan, shyft::nan, max_utctime, qac_ts_1);
      auto qac_ts_123_mask = qac_ts_123.inside(0.0, shyft::nan, 0.0, 1.0, 0.0);
      auto ts = qac_ts_123 * qac_ts_123_mask;

      tsv.push_back(ts);
    }

    // Create a sum-expression of the time series in the vector,
    // and then a "mask series" as an inside-expression around that sum.
    auto sum_ts = tsv.sum();
    auto sum_ts_mask = sum_ts.inside(0.0, shyft::nan, 0.0, 1.0, 0.0);

    // Create another qac ts.
    auto work_ts = apoint_ts{ta, shyft::nan, POINT_AVERAGE_VALUE};
    auto override_ts = apoint_ts{ta, 999, POINT_AVERAGE_VALUE};
    auto qac_ts = work_ts.min_max_check_ts_fill(shyft::nan, shyft::nan, max_utctime, override_ts);

    // Create the final ts by "applying" the mask on the above qac ts.
    auto ts = qac_ts * sum_ts_mask;

    // To show the resulting ts expression
    // MESSAGE(ts.stringify());

    // Convert shyft ts to shop data txy.
    // With timing: This should take only 1ms on fast hardware (maybe
    // 40ms on slow hardware), but in an early version of the convert_to_shop
    // implementation the shyft ts was not explicitely evaluated before
    // looking up individual points and this would take 75 seconds on
    // fast hardware!
    auto t0 = timing::now();
    auto txy = shop::data::txy_factory<apoint_ts, v_unit>::convert_to_shop(ts, t_axis);
    auto t1 = timing::now();
    MESSAGE(
      "Converting complex ts expression (qac/inside/use_time_axis_from, n="
      << n << ", n_tsv=" << n_tsv << ") to shop: " << double(elapsed_ms(t0, t1)) << " ms (" << elapsed_us(t0, t1)
      << " us)");

    // Check results
    auto start_time = shop::data::shop_time{(time_t) to_seconds64(t_begin)};
    CHECK(txy.start == start_time);
    for (int i = 0; i < n; ++i) {
      CHECK(txy.t[i] == t_shop[i]);
      CHECK(txy.y[i] == 999.0);
    }
  }
}

TEST_SUITE_END();
