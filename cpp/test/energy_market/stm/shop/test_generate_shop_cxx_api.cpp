#include "fixture.h"
#include <shyft/energy_market/stm/shop/api/shop_cxx_api_generator.h>
#include <string>
#include <memory>
#include <fstream>

using namespace std;
using namespace string_literals;
using namespace shop_cxx_api_generator;

TEST_SUITE_BEGIN("stm/shop");

/** The intenion here is to generate the new api, then compare it with the existing one, and fail if different.
 * currently we just generate the new shop_enums.new.h
 */
TEST_CASE("stm/shop/generate_shop_cxx_api_headers") {
  string generated_file_name = "shop_enums.new.h";
  string proxy_file_name = "shop_api.new.h";
  ofstream ofs(generated_file_name, ofstream::out);
  ofstream pofs(proxy_file_name, ofstream::out);
  REQUIRE(ofs);
  REQUIRE(pofs);
  unique_ptr<ShopSystem, bool (*)(ShopSystem*)> shop_safe(ShopInit(), ShopFree);
  ShopSystem* shop = shop_safe.get(); // for convinience calling the next functions
  ShopSetSilentConsole(shop, true);
  ofs << "#pragma once" << endl << "#include <tuple>" << endl << "namespace shop::enums {" << endl;
  print_object_types(shop, ofs);
  ofs << endl;
  print_attribute_types(shop, ofs, pofs);
  ofs << endl;
  print_relation_types(shop, ofs);
  ofs << endl;
  print_command_types(shop, ofs);
  ofs << endl;
  print_other(shop, ofs);
  ofs << "\n}" << endl;
}

TEST_SUITE_END();
