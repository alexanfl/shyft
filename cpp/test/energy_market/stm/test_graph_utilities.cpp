#include <test/energy_market/build_test_model.h>
#include <doctest/doctest.h>
#include <iostream>
#include <vector>
#include <limits>
#include <memory>
#include <iostream>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <shyft/energy_market/graph/graph_utilities.h>

using namespace shyft::energy_market::hydro_power;
using namespace shyft::energy_market::graph;
using namespace shyft::energy_market::stm;

namespace graph_test {
  /* Create a HPS for graph search tests
   *
   * R0
   *   0 |
   *   1 |
   *   1 R1   R2
   *    2 \   / 4
   *     3 \ / 5
   *        x
   *     6  |
   *    7  / \ 8
   *      U0 U1
   *     9 \ / 10
   *        x
   *        | 11
   *        R4
   */
  inline hydro_power_system_ create_hps() {

    // hydro_power_system_ hps = make_shared<hydro_power_system>(0, "test_system");
    auto hps = std::make_shared<stm_hps>(0, "test_system");
    // auto builder = std::make_unique<hydro_power_system_builder>(hps);
    auto builder = std::make_unique<stm_hps_builder>(hps);

    for (int id = 0; id < 12; ++id) {
      auto ww = builder->create_waterway(id, "test_waterway_" + std::to_string(id), "");
    }
    for (int id = 0; id < 5; ++id) {
      auto ww = builder->create_reservoir(id, "test_reservoir_" + std::to_string(id), "");
    }
    for (int id = 0; id < 2; ++id) {
      auto ww = builder->create_unit(id, "test_unit_" + std::to_string(id), "");
    }
    auto& rsv = hps->reservoirs;
    auto& unit = hps->units;
    auto& wtr = hps->waterways;

    connect(wtr[0]).input_from(rsv[0]).output_to(wtr[1]);
    connect(wtr[1]).output_to(rsv[1]);
    connect(wtr[2]).input_from(rsv[1]).output_to(wtr[3]);
    connect(wtr[3]).output_to(wtr[6]);
    connect(wtr[4]).input_from(rsv[2]).output_to(wtr[5]);
    connect(wtr[5]).output_to(wtr[6]);
    connect(wtr[6]).output_to(wtr[7]).output_to(wtr[8]);
    connect(wtr[7]).output_to(unit[0]);
    connect(wtr[8]).output_to(unit[1]);
    connect(wtr[9]).input_from(unit[0]).output_to(wtr[11]);
    connect(wtr[10]).input_from(unit[1]).output_to(wtr[11]);
    connect(wtr[11]).output_to(rsv[4]);

    return hps;
  }

  inline hydro_power_system_ create_hps_2() {

    auto hps = std::make_shared<hydro_power_system>(0, "test_system");
    auto builder = std::make_unique<hydro_power_system_builder>(hps);

    for (int id = 0; id < 4; ++id) {
      builder->create_waterway(id, "test_waterway_" + std::to_string(id), "");
      builder->create_unit(id, "test_unit_" + std::to_string(id), "");
      builder->create_reservoir(id, "test_reservoir_" + std::to_string(id), "");
    }
    auto& rsv = hps->reservoirs;
    auto& wtr = hps->waterways;

    connect(wtr[0]).input_from(rsv[0]).output_to(rsv[1]);
    connect(wtr[1]).input_from(rsv[1]).output_to(rsv[2]);
    connect(wtr[2]).input_from(rsv[2]);

    return hps;
  }
}

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/upstream_reservoirs") {

  auto hps = graph_test::create_hps();


  SUBCASE("find_immediate") {
    // Test that calling with max_dist=0 returns only immediate upstream reservoirs
    auto src = hps->waterways[7];
    auto us = upstream_reservoirs(src, 0);

    // Should only find reservoirs 1 and 2
    CHECK_EQ(us.size(), 2);
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[1]) != us.end());
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[2]) != us.end());
  }

  SUBCASE("find_one_step_away") {
    // Test that calling with max_dist=1 finds more reservoirs
    auto src = hps->waterways[7];
    auto us = upstream_reservoirs(src, 1);

    // Should find reservoirs 1, 2, 3
    CHECK_EQ(us.size(), 3);
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[0]) != us.end());
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[1]) != us.end());
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[2]) != us.end());
  }

  SUBCASE("find_all") {
    // Test that calling with max_dist=-1 returns all upstream reservoirs
    auto src = hps->waterways[11];
    auto us = upstream_reservoirs(src, -1);

    // Should find reservoirs 1, 2, 3
    CHECK_EQ(us.size(), 3);
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[0]) != us.end());
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[1]) != us.end());
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[2]) != us.end());
  }


  SUBCASE("nearest_from_reservoir") {
    // Test that starting from a reservoir does not include it.

    auto hps = graph_test::create_hps();

    auto src = hps->reservoirs[1];
    auto us = upstream_reservoirs(src, 0);

    // Should find both units
    CHECK_EQ(us.size(), 1);
    CHECK_EQ(us[0], hps->reservoirs[0]);
  }

  SUBCASE("all_from_reservoir") {
    // Test that starting from a reservoir does not include it.

    auto hps = graph_test::create_hps();

    auto src = hps->reservoirs[4];
    auto us = upstream_reservoirs(src, -1);

    // Should all other reservoirs
    CHECK_EQ(us.size(), 3);
    CHECK(std::find(us.begin(), us.end(), hps->reservoirs[4]) == us.end());
  }
}

TEST_CASE("stm/downstream_units") {

  auto hps = graph_test::create_hps();


  SUBCASE("find_immediate") {
    // Test that calling with max_dist=0 returns only immediate downstream units
    auto src = hps->waterways[0];
    auto ds = downstream_units(src, 0);

    // No immediate downstream units
    CHECK_EQ(ds.size(), 0);
  }

  SUBCASE("find_one_step_further") {
    // Test that calling with max_dist=1 finds more units
    auto src = hps->waterways[0];
    auto ds = downstream_units(src, 1);

    // Should find both units
    CHECK_EQ(ds.size(), 2);
    CHECK(std::find(ds.begin(), ds.end(), hps->units[0]) != ds.end());
    CHECK(std::find(ds.begin(), ds.end(), hps->units[1]) != ds.end());
  }

  SUBCASE("find_all") {
    // Test that calling with max_dist=-1 returns all downstream units
    auto src = hps->waterways[0];
    auto ds = downstream_units(src, -1);

    // Should find both units
    CHECK_EQ(ds.size(), 2);
    CHECK(std::find(ds.begin(), ds.end(), hps->units[0]) != ds.end());
    CHECK(std::find(ds.begin(), ds.end(), hps->units[1]) != ds.end());
  }

  SUBCASE("nearest_from_reservoir") {
    // Test that starting from a reservoir with max_dist=0 finds the closest downstream units

    auto hps = graph_test::create_hps();

    auto src = hps->reservoirs[1];
    auto ds = downstream_units(src, 0);

    // Should find both units
    CHECK_EQ(ds.size(), 2);
  }
}

TEST_SUITE_END();
