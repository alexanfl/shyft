#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/time_series/dd/apoint_ts.h>

using namespace shyft::energy_market::stm;
using namespace shyft::time_series::dd;
using std::make_shared;

namespace {
  struct test_sys {
    stm_system_ sys;
    network_ net;
    power_module_ pm;
    busbar_ b;

    test_sys() {
      sys = make_shared<stm_system>(1, "sys", "{}");
      auto sys_builder = stm_builder(sys);
      net = sys_builder.create_network(2, "net", "{}");
      pm = sys_builder.create_power_module(3, "pm", "{}");
      auto net_builder = network_builder(net);
      b = net_builder.create_busbar(4, "b", "{}");
      b->add_power_module(pm, apoint_ts{});
    }
  };
}

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/power_module_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto pm = make_shared<power_module>(1, "pm", "{}", sys);
  FAST_CHECK_UNARY(pm);
  FAST_CHECK_EQ(pm->id, 1);
  FAST_CHECK_EQ(pm->name, "pm");
  FAST_CHECK_EQ(pm->json, "{}");
}

TEST_CASE("stm/power_module_equality") {
  test_sys sys1;
  test_sys sys2;
  CHECK_EQ(*sys1.pm, *sys1.pm); // equal to self
  CHECK_EQ(*sys1.pm, *sys2.pm);
}

TEST_CASE("stm/power_module_generate_url") {
  auto sys = test_sys{};
  std::string s;
  auto rbi = std::back_inserter(s);
  sys.pm->generate_url(rbi);
  FAST_CHECK_EQ(s, "/P3");
  s.clear();
  sys.pm->generate_url(rbi, -1, 0);
  FAST_CHECK_EQ(s, "/P{o_id}");
  s.clear();
  sys.pm->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/P3");
  s.clear();
}

TEST_SUITE_END();
