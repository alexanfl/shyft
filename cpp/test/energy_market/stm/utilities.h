#pragma once

#include <vector>
#include <memory>
#include <limits>
#include <numeric>
#include <exception>

#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

namespace test {

  using namespace shyft::energy_market::stm;
  using shyft::energy_market::hydro_power::point;
  using shyft::energy_market::hydro_power::xy_point_curve;
  using shyft::energy_market::hydro_power::xy_point_curve_with_z;
  using shyft::energy_market::hydro_power::turbine_description;
  using xy_point_curve_with_z_list = std::vector<xy_point_curve_with_z>;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_axis::utctime;
  using shyft::time_axis::point_dt;
  using shyft::time_series::ts_point_fx;


  constexpr auto create_t_double = [](unsigned long t0 = 0, double value = 1.0) -> apoint_ts {
    return apoint_ts{point_dt(vector<utctime>{t0}, utctime::max()), value, ts_point_fx::POINT_AVERAGE_VALUE};
  };

  constexpr auto create_t_xy = [](unsigned long t0 = 0, std::vector<double> const & values = {}) -> t_xy_ {
    if (values.size() % 2)
      throw std::runtime_error("values size must be divisible by 2");

    auto xy = std::make_shared<xy_point_curve>();
    for (auto it = values.begin(); it != values.end();) {
      xy->points.emplace_back(*it++, *it++);
    }
    auto m = std::make_shared<t_xy_::element_type>();
    m->emplace(std::make_pair(utctime(t0), xy));

    return m;
  };

  constexpr auto create_t_xyz = [](unsigned long t0 = 0, std::vector<double> const & values = {}) -> t_xyz_list_ {
    if (values.size() % 3)
      throw std::runtime_error("values size must be divisible by 3");

    auto tm = std::map<double, std::vector<point>>{};
    for (auto it = values.begin(); it != values.end();) {
      double x = *it++, y = *it++, z = *it++;
      if (tm.count(z) == 0)
        tm[z] = std::vector<point>{};
      tm[z].emplace_back(x, y);
    }

    auto xyz = std::make_shared<xy_point_curve_with_z_list>();
    for (auto it = tm.begin(); it != tm.end(); ++it) {
      xyz->emplace_back(xy_point_curve{it->second}, it->first);
    }

    auto m = std::make_shared<t_xyz_list_::element_type>();
    m->emplace(std::make_pair(utctime(t0), xyz));

    return m;
  };

  constexpr auto create_t_turbine_description =
    [](unsigned long t0 = 0, std::vector<double> const & values = {}) -> t_turbine_description_ {
    if (values.size() % 3)
      throw std::runtime_error("values size must be divisible by 3");

    auto tm = std::map<double, std::vector<point>>{};
    for (auto it = values.begin(); it != values.end();) {
      double x = *it++, y = *it++, z = *it++;
      if (tm.count(z) == 0)
        tm[z] = std::vector<point>{};
      tm[z].emplace_back(x, y);
    }

    auto td = std::make_shared<turbine_description>();
    td->operating_zones.emplace_back();
    td->operating_zones.back().fcr_min = td->operating_zones.back().fcr_max = 0.0;
    for (auto it = tm.begin(); it != tm.end(); ++it) {
      td->operating_zones.back().efficiency_curves.emplace_back(xy_point_curve{it->second}, it->first);
    }
    auto m = std::make_shared<t_turbine_description_::element_type>();
    m->emplace(std::make_pair(utctime(t0), td));

    return m;
  };

}
