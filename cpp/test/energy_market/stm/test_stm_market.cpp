#include <doctest/doctest.h>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/unit_group.h>

using namespace shyft::energy_market::stm;
using std::make_shared;
using std::vector;
using std::map;
using std::runtime_error;
using xy_point = shyft::energy_market::hydro_power::point;
using xy_points = shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::hydro_power::xy_point_curve_;
using shyft::energy_market::stm::convert_to_price_delivery_tsv;

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/energy_market_area_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ema = make_shared<energy_market_area>(1, "ema", "{}", sys);
  FAST_CHECK_UNARY(ema);
  FAST_CHECK_EQ(ema->id, 1);
  FAST_CHECK_EQ(ema->name, "ema");
  FAST_CHECK_EQ(ema->json, "{}");
}

TEST_CASE("stm/energy_market_area_set_unit_group") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ema = make_shared<energy_market_area>(1, "ema", "{}", sys);
  auto ug1 = sys->add_unit_group(1, "ug1", "{}", unit_group_type::production);
  ema->set_unit_group(ug1);
  FAST_CHECK_EQ(ema->unit_groups[0], ug1);
  auto ug2 = sys->add_unit_group(2, "ug2", "{}");
  CHECK_THROWS_AS(ema->set_unit_group(ug2), std::runtime_error);
}

TEST_CASE("stm/energy_market_area_get_unit_group") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ema = make_shared<energy_market_area>(1, "ema", "{}", sys);
  auto ug1 = sys->add_unit_group(1, "ug1", "{}", unit_group_type::production);
  auto ug = ema->get_unit_group();
  FAST_CHECK_EQ(ug, nullptr);
  ema->set_unit_group(ug1);
  auto ug2 = ema->get_unit_group();
  FAST_CHECK_EQ(ug2, ug1);
  FAST_CHECK_EQ(ema->unit_groups.size(), 1u);
}

TEST_CASE("stm/energy_market_area_remove_group") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ema = make_shared<energy_market_area>(1, "ema", "{}", sys);
  auto ug1 = sys->add_unit_group(1, "ug1", "{}", unit_group_type::production);
  CHECK_THROWS_AS(ema->remove_unit_group(), std::runtime_error);
  ema->set_unit_group(ug1);
  auto ug = ema->remove_unit_group();
  FAST_CHECK_EQ(ug, ug1);
  FAST_CHECK_EQ(ema->unit_groups.size(), 0u);
}

TEST_CASE("stm/energy_market_area_generate_url") {
  auto sys = make_shared<stm_system>(123, "sys", "{}");
  auto ema = make_shared<energy_market_area>(456, "ema", "{}", sys);
  std::string s;
  auto rbi = std::back_inserter(s);
  ema->generate_url(rbi);
  FAST_CHECK_EQ(s, "/m456");
  s.clear();
  ema->generate_url(rbi, -1, -1);
  FAST_CHECK_EQ(s, "/m456");
  s.clear();
  ema->generate_url(rbi, -1, 0);
  FAST_CHECK_EQ(s, "/m{o_id}");
  s.clear();
  ema->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/m456");
  s.clear();
}

TEST_CASE("stm/transmission_lines_to_market") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(4, "net", "{}");

  auto ema1 = sys_builder.create_market_area(2, "ema1", "{}");
  auto ema2 = sys_builder.create_market_area(3, "ema2", "{}");

  network_builder net_builder(net);
  auto b1 = net_builder.create_busbar(11, "Busbar1", "{}");
  auto b2 = net_builder.create_busbar(12, "Busbar2", "{}");
  auto b3 = net_builder.create_busbar(13, "Busbar3", "{}");
  auto b4 = net_builder.create_busbar(14, "Busbar4", "{}");

  auto t1 = net_builder.create_transmission_line(21, "TransmissionLine1", "{}");
  auto t2 = net_builder.create_transmission_line(22, "TransmissionLine2", "{}");

  /*   ___________________                             ___________________
   *   |  Energy Market 1  |                           | Energy Market 2   |
   *   |                   |                           |                   |
   *   |   |Busbar1| ------>----|TransmissionLine1|---->---- |Busbar2|     |
   *   |                   |                           |                   |
   *   |   |Busbar3| ------<----|TransmissionLine2|----<-----|Busbar4|     |
   *   |___________________|                           |___________________|
   */

  b1->add_to_market_area(ema1);
  b3->add_to_market_area(ema1);
  b1->add_to_start_of_transmission_line(t1);
  b2->add_to_end_of_transmission_line(t1);

  b2->add_to_market_area(ema2);
  b4->add_to_market_area(ema2);
  b4->add_to_start_of_transmission_line(t2);
  b3->add_to_end_of_transmission_line(t2);

  auto t_lines = ema1->transmission_lines_to(ema2);
  REQUIRE_EQ(t_lines.size(), 1);
  REQUIRE_EQ(t_lines.at(0), t1);

  t_lines = ema1->transmission_lines_from(ema2);
  REQUIRE_EQ(t_lines.size(), 1);
  REQUIRE_EQ(t_lines.at(0), t2);

  t_lines = ema2->transmission_lines_to(ema1);
  REQUIRE_EQ(t_lines.size(), 1);
  REQUIRE_EQ(t_lines.at(0), t2);

  t_lines = ema2->transmission_lines_from(ema1);
  REQUIRE_EQ(t_lines.size(), 1);
  REQUIRE_EQ(t_lines.at(0), t1);

  // Add transmission line from b1 to b4
  auto t3 = net_builder.create_transmission_line(23, "TransmissionLine3", "{}");
  b1->add_to_start_of_transmission_line(t3);
  b4->add_to_end_of_transmission_line(t3);
  t_lines = ema1->transmission_lines_to(ema2);
  REQUIRE_EQ(t_lines.size(), 2);
  REQUIRE_EQ(t_lines.at(0), t1);
  REQUIRE_EQ(t_lines.at(1), t3);
}

TEST_CASE("stm/get_production_and_get_consumption") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");

  auto ema = sys_builder.create_market_area(3, "ema1", "{}");

  network_builder net_builder(net);
  auto b1 = net_builder.create_busbar(11, "Busbar1", "{}");
  auto b2 = net_builder.create_busbar(12, "Busbar2", "{}");
  auto b3 = net_builder.create_busbar(13, "Busbar3", "{}");
  b1->add_to_market_area(ema);
  b2->add_to_market_area(ema);
  b3->add_to_market_area(ema);

  auto ug = ema->create_busbar_derived_unit_group(4, "NO1", "{}");
  auto hps = make_shared<stm_hps>(0, "test_system");
  auto builder = stm_hps_builder(hps);
  auto u1 = builder.create_unit(21, "unit1", "");
  auto u2 = builder.create_unit(22, "unit2", "");

  auto pm2 = sys_builder.create_power_module(32, "pm2", "{}");
  auto pm3 = sys_builder.create_power_module(33, "pm3", "{}");

  SUBCASE("production_consumption_no_mask") {
    auto const ta = fixed_dt(0, 1, 5);
    auto const null_mask = apoint_ts{};
    b1->add_unit(u1, null_mask);
    b2->add_unit(u2, null_mask);
    b2->add_power_module(pm2, null_mask);
    b3->add_power_module(pm3, null_mask);

    /** In this test:
     *  Units are in general producing. Except: last value of u2
     *  PowerModules are in general consuming. Except: second-to-last value of pm3
     */
    u1->production.result = apoint_ts(ta, {2, 0, 0, 4, 4});
    u2->production.result = apoint_ts(ta, {0, 3, 0, 5, -2});
    pm2->power.result = apoint_ts(ta, {-2, -2, -1, 0, -2});
    pm3->power.result = apoint_ts(ta, {-5, 0, -2, 1, -3});
    auto const expected_production = vector<double>{2, 3, 0, 10, 4};
    auto const expected_consumption = vector<double>{7, 2, 3, 0, 7};

    auto const prod = ema->get_production();
    CHECK_EQ(prod.result.values(), expected_production);
    CHECK(prod.realised.values().empty());
    CHECK(prod.schedule.values().empty());

    auto const cons = ema->get_consumption();
    CHECK_EQ(cons.result.values(), expected_consumption);
    CHECK(cons.realised.values().empty());
    CHECK(cons.schedule.values().empty());
  }

  SUBCASE("production_consumption_active_mask") {
    auto const ta = fixed_dt(0, 1, 4);
    auto const u1_active = apoint_ts(ta, {1, 1, 0, 0});
    u1->production.result = apoint_ts(ta, {5, 6, 7, 8});
    auto const expected_production = vector<double>{5, 6, 0, 0};
    b1->add_unit(u1, u1_active);

    auto const pm2_active = apoint_ts(ta, {1, 0, 1, 0});
    pm2->power.result = apoint_ts(ta, {-6, -7, -8, -9});
    auto expected_consumption = vector<double>{
      6,
      0,
      8,
      0,
    };
    b2->add_power_module(pm2, pm2_active);

    auto const prod = ema->get_production();
    CHECK_EQ(prod.result.values(), expected_production);
    auto const cons = ema->get_consumption();
    CHECK_EQ(cons.result.values(), expected_consumption);
  }
  SUBCASE("get_production_correct_sum_of_result_realised_schedule") {
    auto const ta = fixed_dt(0, 1, 3);
    u1->production.result = apoint_ts(ta, {1, 2, 3});
    u1->production.schedule = apoint_ts(ta, {4, 5, 6});
    u1->production.realised = apoint_ts(ta, {7, 8, 9});
    auto const expected_result = vector<double>{1, 2, 3};
    auto const expected_schedule = vector<double>{4, 5, 6};
    auto const expected_realised = vector<double>{7, 8, 9};
    b1->add_unit(u1, apoint_ts{});
    auto const prod = ema->get_production();
    CHECK_EQ(prod.result.values(), expected_result);
    CHECK_EQ(prod.schedule.values(), expected_schedule);
    CHECK_EQ(prod.realised.values(), expected_realised);
  }
  SUBCASE("get_consumption_correct_sum_of_result_realised_schedule") {
    auto const ta = fixed_dt(0, 1, 3);
    pm2->power.result = apoint_ts(ta, {-1, -2, -3});
    pm2->power.schedule = apoint_ts(ta, {-4, -5, -6});
    pm2->power.realised = apoint_ts(ta, {-7, -8, -9});
    auto const expected_result = vector<double>{1, 2, 3};
    auto const expected_schedule = vector<double>{4, 5, 6};
    auto const expected_realised = vector<double>{7, 8, 9};
    b2->add_power_module(pm2, apoint_ts{});
    auto const cons = ema->get_consumption();
    CHECK_EQ(cons.result.values(), expected_result);
    CHECK_EQ(cons.schedule.values(), expected_schedule);
    CHECK_EQ(cons.realised.values(), expected_realised);
  }
}

TEST_CASE("stm/get_import_and_get_export") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");
  auto ema = sys_builder.create_market_area(3, "ema1", "{}");
  network_builder net_builder(net);
  auto b1 = net_builder.create_busbar(11, "Busbar1", "{}");
  auto b2 = net_builder.create_busbar(12, "Busbar2", "{}");
  auto b3 = net_builder.create_busbar(13, "Busbar3", "{}");
  b1->add_to_market_area(ema);
  b2->add_to_market_area(ema);
  b3->add_to_market_area(ema);

  // note: positive values == import, negative values == export
  SUBCASE("import_export_result") {
    auto const ta = fixed_dt(0, 1, 5);
    b1->flow.result = apoint_ts(ta, {-1, 0, 0, -2, -2});
    b2->flow.result = apoint_ts(ta, {0, 1, 0, -3, 0});
    b3->flow.result = apoint_ts(ta, {0, 0, 3, 6, -4});
    auto const expected_import = vector<double>{0, 1, 3, 1, 0};
    auto const expected_export = vector<double>{1, 0, 0, 0, 6};

    auto const import = ema->get_import();
    CHECK_EQ(import.result.values(), expected_import);
    CHECK(import.realised.values().empty());
    CHECK(import.schedule.values().empty());
    auto const exp = ema->get_export();
    CHECK_EQ(exp.result.values(), expected_export);
    CHECK(exp.realised.values().empty());
    CHECK(exp.schedule.values().empty());
  }
  SUBCASE("import_export_result_realised_schedule") {
    auto const ta = fixed_dt(0, 1, 5);
    b1->flow.result = apoint_ts(ta, {-1, 0, 0, -2, -2});
    b2->flow.schedule = apoint_ts(ta, {0, 1, 0, -3, 0});
    b3->flow.realised = apoint_ts(ta, {0, 0, 3, 6, -4});
    auto const export_result = vector<double>{1, 0, 0, 2, 2};
    auto const export_schedule = vector<double>{0, 0, 0, 3, 0};
    auto const export_realised = vector<double>{0, 0, 0, 0, 4};
    auto const import_result = vector<double>{0, 0, 0, 0, 0};
    auto const import_schedule = vector<double>{0, 1, 0, 0, 0};
    auto const import_realised = vector<double>{0, 0, 3, 6, 0};

    auto const import = ema->get_import();
    CHECK_EQ(import.result.values(), import_result);
    CHECK_EQ(import.schedule.values(), import_schedule);
    CHECK_EQ(import.realised.values(), import_realised);
    auto const exp = ema->get_export();
    CHECK_EQ(exp.result.values(), export_result);
    CHECK_EQ(exp.schedule.values(), export_schedule);
    CHECK_EQ(exp.realised.values(), export_realised);
  }
}

TEST_SUITE_END();
