#include <test/test_pch.h>
#include <memory>

#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/energy_market/stm/srv/task/client.h>

#include <shyft/web_api/energy_market/stm/task/request_handler.h>
#include <test/test_utils.h>

#include <csignal>
#include <iostream>

using std::make_shared;
using std::vector;
using std::string;
using std::string_view;
using std::to_string;

using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;
using shyft::core::from_seconds;

namespace shyft::energy_market::srv::test {
  using shyft::energy_market::stm::srv::stm_task;
  using shyft::energy_market::stm::srv::stm_case;
  using shyft::energy_market::stm::srv::task::client;
  using shyft::energy_market::stm::srv::task::server;
  using shyft::web_api::energy_market::stm::task::request_handler;
  using shyft::srv::model_info;

  struct test_server : public server {

    request_handler bg_server;
    std::future<int> web_srv;

    explicit test_server(string const & root_dir)
      : server(root_dir) {
      bg_server.srv = this;
    }

    void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads) {
      if (!web_srv.valid()) {
        web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads]() -> int {
          return shyft::web_api::run_web_server(
            bg_server,
            host_ip,
            static_cast<unsigned short>(port),
            make_shared<string>(doc_root),
            fg_threads,
            bg_threads);
        });
      }
    }

    bool web_api_running() const {
      return web_srv.valid();
    }

    void stop_web_api() {
      if (web_srv.valid()) {
        std::raise(SIGINT);
        (void) web_srv.get();
      }
    }
  };

  //-- test client
  using tcp = boost::asio::ip::tcp;
  namespace websocket = boost::beast::websocket;
  using boost::system::error_code;

  class session : public std::enable_shared_from_this<session> {
    tcp::resolver resolver_;
    websocket::stream<tcp::socket> ws_;
    boost::beast::multi_buffer buffer_;
    string host_;
    string port_;
    string text_;
    string response_;
    string fail_;
    std::function<string(string const &)> report_response;

    // Report failure
    void fail(error_code ec, char const * what) {
      fail_ = string(what) + ": " + ec.message() + "\n";
    }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));

   public:
    explicit session(boost::asio::io_context& ioc)
      : resolver_(ioc)
      , ws_(ioc) {
    }

    string response() const {
      return response_;
    }

    string diagnostics() const {
      return fail_;
    }

    // Start the asynchronous operation
    template <class Fx>
    void run(string_view host, int port, string_view text, Fx&& rep_response) {
      host_ = host;
      text_ = text;
      port_ = std::to_string(port);
      report_response = rep_response;
      resolver_.async_resolve(
        host_, port_, [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
          me->on_resolve(ec, results);
        });
    }

    void on_resolve(error_code ec, tcp::resolver::results_type results) {
      fail_on_error(ec, "resolve")

        boost::asio::async_connect(
          ws_.next_layer(),
          results.begin(),
          results.end(),
          std::bind(&session::on_connect, shared_from_this(), std::placeholders::_1));
    }

    void on_connect(error_code ec) {
      fail_on_error(ec, "connect") ws_.async_handshake(host_, "/", [me = shared_from_this()](error_code ec) {
        me->on_handshake(ec);
      });
    }

    void on_handshake(error_code ec) {
      fail_on_error(ec, "handshake")

        if (text_.size()) {
        ws_.async_write(boost::asio::buffer(text_), [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->on_write(ec, bytes_transferred);
        });
      }
      else {
        ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
          me->on_close(ec);
        });
      }
    }

    void on_write(error_code ec, std::size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "write")

        ws_.async_read(buffer_, [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->on_read(ec, bytes_transferred);
        });
    }

    void on_read(error_code ec, size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "read")

        response_ = boost::beast::buffers_to_string(buffer_.data());
      buffer_.consume(buffer_.size());
      text_ = report_response(response_);
      if (text_.size()) {
        ws_.async_write(boost::asio::buffer(text_), [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->on_write(ec, bytes_transferred);
        });
      } else {
        ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
          me->on_close(ec);
        });
      }
    }

    void on_close(error_code ec) {
      fail_on_error(ec, "close")
    }

#undef fail_on_error
  };

  unsigned short get_free_port() {
    using namespace boost::asio;
    io_service service;
    ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));
    return acceptor.local_endpoint().port();
  }

  class pub_sub_session : public std::enable_shared_from_this<pub_sub_session> {
    tcp::resolver resolver_;
    websocket::stream<tcp::socket> ws_;
    boost::beast::multi_buffer buffer_;
    string host_;
    string port_;
    string fail_;
    test_server* const srv; ///< Hold a pointer to server, so we can update models without websocket as well
    int num_waits = 0;      ///< How many expected releases of subscribed read pattern

    // Report failure
    void fail(error_code ec, char const * what) {
      fail_ = string(what) + ": " + ec.message() + "\n";
    }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));
   public:
    explicit pub_sub_session(boost::asio::io_context& ioc, test_server* const srv)
      : resolver_(ioc)
      , ws_(ioc)
      , srv{srv} {
    }

    vector<string> responses_;

    string diagnostics() const {
      return fail_;
    }

    // Start the asynchronous operation
    void run(string_view host, int port) {
      host_ = host;
      port_ = std::to_string(port);
      resolver_.async_resolve(
        host_, port_, [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
          me->on_resolve(ec, results);
        });
    }

    void on_resolve(error_code ec, tcp::resolver::results_type results) {
      fail_on_error(ec, "resolve");
      boost::asio::async_connect(
        ws_.next_layer(),
        results.begin(),
        results.end(),
        std::bind(&pub_sub_session::on_connect, shared_from_this(), std::placeholders::_1));
    }

    void on_connect(error_code ec) {
      fail_on_error(ec, "connect") ws_.async_handshake(host_, "/", [me = shared_from_this()](error_code ec) {
        me->on_handshake(ec);
      });
    }

    void on_handshake(error_code ec) {
      fail_on_error(ec, "handshake") ws_.async_write(
        boost::asio::buffer(R"_(
                    get_model_infos {"request_id":"initial","subscribe":true}
                )_"),
        [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->start_read(ec, bytes_transferred);
        });
    }

    void start_read(error_code ec, size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "start_read")
        ws_.async_read(buffer_, [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->on_read(ec, bytes_transferred);
        });
    }

    void on_read(error_code ec, size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "read");
      string response = boost::beast::buffers_to_string(buffer_.data());
      responses_.push_back(response);
      // std::cout << "Got response: " << response << "\n";
      buffer_.consume(buffer_.size());

      if (response.find("finale") != string::npos) {
        ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
          me->on_close(ec);
        });
      } else {
        if (response.find("initial") != string::npos) {
          if (num_waits == 0) {
            // 0. Directly notifying change to the database's  subscription manager.
            // Should trigger a re-emit of the first response..
            ++num_waits;
            srv->db.sm->notify_change("model_infos");
          } else if (num_waits == 1) {
            // 1. Updating model_info through update_model_info request.
            // This should trigger a re-emit of the first response.
            ++num_waits;
            ws_.async_write(
              boost::asio::buffer(R"_(
                                update_model_info {
                                    "request_id": "first update",
                                    "model_info": {
                                        "id": 1,
                                        "name": "new name",
                                        "created": 11.0
                                    }
                                }
                            )_"),
              [me = shared_from_this()](error_code, size_t) {
                // Nothing to do here...
              });
          } else if (num_waits == 2) {
            // 2. Using a client to update the model_info
            ++num_waits;
            model_info updated(1, "second update", from_seconds(10));
            client c(srv->get_listening_ip() + string(":") + std::to_string(srv->get_listening_port()));
            c.update_model_info(1, updated);
          } else if (num_waits == 3) {
            // 3. If A model is removed.
            ++num_waits;
            client c(srv->get_listening_ip() + string(":") + std::to_string(srv->get_listening_port()));
            c.remove_model(1);
          } else if (num_waits == 4) {
            // 4. Store a new model.
            // This should trigger a re-emit.
            ++num_waits;
            ws_.async_write(
              boost::asio::buffer(R"_(
                                store_model {"request_id": "store_model",
                                    "model": {
                                        "id": 2,
                                        "name": "second run",
                                        "created": 10.0,
                                        "labels": ["test"]
                                    }
                                })_"),
              [me = shared_from_this()](error_code, size_t) {
                // Nothing to do here
              });
          } else if (num_waits == 5) {
            // 5. Make a subscription read of model 2.
            // Should not re-emit a trigger of the first response.
            ++num_waits;
            ws_.async_write(
              boost::asio::buffer(R"_(
                                read_model {
                                    "request_id": "read_subscribe",
                                    "model_id": 2,
                                    "subscribe": true
                                }
                            )_"),
              [](error_code, size_t) {});
          } else {
            ws_.async_write(
              boost::asio::buffer(R"_(unsubscribe {"request_id":"finish_mi", "subscription_id":"initial"})_"),
              [me = shared_from_this()](error_code, size_t) {
                // Nothing to do here.
              });
          }
        } else if (response.find("read_subscribe") != string::npos) {
          if (num_waits == 6) {
            // 6. Do a store_model on model 2:
            // Should trigger update for both
            ++num_waits;
            ws_.async_write(
              boost::asio::buffer(R"_(
                                store_model {"request_id": "2nd model_store",
                                    "model": {
                                        "id": 2,
                                        "name": "2nd run upd",
                                        "created": 15.0,
                                        "labels": ["test", "web"]
                                    }
                                }
                            )_"),
              [](error_code, size_t) {});
          } else {
            ws_.async_write(
              boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"read_subscribe"})_"),
              [](error_code, size_t) {});
          }
        }
        ws_.async_read(buffer_, [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
          me->on_read(ec, bytes_transferred);
        });
      }
    }

    void on_close(error_code ec) {
      fail_on_error(ec, "close");
    }

#undef fail_on_error
  };
}

TEST_SUITE_BEGIN("stm");

using namespace shyft::energy_market::srv;
using namespace shyft::energy_market::srv::test;
using namespace shyft::energy_market::stm::srv;

TEST_CASE("stm/task_web_api/basic_requests") {
  auto dirname = "em.srv.web_api.test" + to_string(to_seconds64(utctime_now()));
  ::test::utils::temp_dir tmpdir(dirname.c_str());
  string doc_root = (tmpdir / "doc_root").string();

  // Set up test server
  test_server a(tmpdir.string());
  string host_ip{"127.0.0.1"};
  a.set_listening_ip(host_ip);
  a.start_server();

  // Store some models:
  auto session1 = make_shared<stm_task>(1, "session1", from_seconds(3600));
  auto run1 = std::make_shared<stm_case>(1, "run1", from_seconds(3600));
  session1->add_case(run1);
  model_info mi1(session1->id, session1->name, session1->created, "");
  a.db.store_model(session1, mi1);

  int port = get_free_port();
  a.start_web_api(host_ip, port, doc_root, 1, 1);
  std::this_thread::sleep_for(std::chrono::milliseconds(700));
  REQUIRE_EQ(true, a.web_api_running());
  vector<string> requests{
    R"_(get_model_infos {"request_id": "1"})_",
    R"_(keyword {"some": "json structure"})_",
    R"_(read_model {"request_id": "2", "model_id": 1})_",
    R"_(update_model_info {
                "request_id": "3",
                "model_info": {
                    "id": 1,
                    "name": "updated model_info",
                    "created": 4100.0,
                    "json": "This mi has been updated"
                }
            })_",
    R"_(get_model_infos {"request_id": "4"})_",
    R"_(store_model {"request_id": "5",
                "model": {
                    "id": 0,
                    "name": "stored1",
                    "created": 4600.0,
                    "labels": ["test", "web API"]
                }
            })_",
    R"_(get_model_infos {"request_id": "6", "mids": [2]})_",
    R"_(read_model {"request_id": "7", "model_id": 2})_",
    R"_(store_model {
                "request_id": "8",
                "model": {
                    "id": 3,
                    "name": "stored2",
                    "created": 10.0,
                    "labels": ["test", "web API"]
                },
                "model_info": {
                    "id": 3,
                    "name": "something else",
                    "json": "misc."
                }
            })_",
    R"_(get_model_infos {"request_id":"9", "mids": [2,3]})_",
    R"_(read_model {"request_id": "10", "model_id": 3})_",
    R"_(remove_model {"request_id":"11", "model_id": 3})_",
    R"_(get_model_infos {"request_id": "12", "mids": [3]})_",
    R"_(get_model_infos {"request_id": "13", "period": [4000.0, 4101.0]})_",
    R"_(get_model_infos {"request_id": "14", "period": [5000.0, 5010.0]})_",
    R"_(add_case {"request_id": "15", "mid": 1,
                    "case": {
                        "id": 2,
                        "name": "testrun",
                        "created": 10.0,
                        "labels": ["test"],
                        "model_refs": []
                    }
            })_",
    R"_(get_case {"request_id": "16", "mid": 1, "cid": 2})_",
    R"_(remove_case {"request_id": "17", "mid": 1, "cid": 2})_",
    R"_(add_model_ref {"request_id": "18", "mid": 1, "cid": 1,
                    "model_ref": {
                        "host": "testhost",
                        "port_num": 12,
                        "api_port_num": 34,
                        "model_key": "mkey"
                    }
            })_",
    R"_(get_model_ref {"request_id": "19", "mid": 1, "cid": 1, "model_key": "mkey"})_",
    R"_(remove_model_ref {"request_id": "20", "mid": 1, "cid": 1, "model_key": "mkey"})_",
  };

  vector<string> responses;
  size_t r = 0;
  {
    boost::asio::io_context ioc;
    auto s1 = std::make_shared<session>(ioc);
    s1->run(host_ip, port, requests[r], [&responses, &r, &requests](string const & web_response) -> string {
      responses.push_back(web_response);
      ++r;
      return r >= requests.size() ? string("") : requests[r];
    });
    ioc.run();
    s1.reset();
  }
  vector<string> expected{
    R"_({"request_id":"1","result":[{"id":1,"name":"session1","created":3600.0,"json":""}]})_",
    R"_(Unknown keyword 'keyword')_",
    R"_({"request_id":"2","result":{"id":1,"name":"session1","created":3600.0,"json":"","labels":[],"cases":[{"id":1,"name":"run1","created":3600.0,"json":"","labels":[],"model_refs":[]}],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":""},"task_name":""}})_",
    R"_({"request_id":"3","result":true})_",
    R"_({"request_id":"4","result":[{"id":1,"name":"updated model_info","created":4100.0,"json":"This mi has been updated"}]})_",
    R"_({"request_id":"5","result":2})_",
    R"_({"request_id":"6","result":[{"id":2,"name":"stored1","created":4600.0,"json":""}]})_",
    R"_({"request_id":"7","result":{"id":2,"name":"stored1","created":4600.0,"json":"","labels":["test","web API"],"cases":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":""},"task_name":""}})_",
    R"_({"request_id":"8","result":3})_",
    R"_({"request_id":"9","result":[{"id":2,"name":"stored1","created":4600.0,"json":""},{"id":3,"name":"something else","created":10.0,"json":"misc."}]})_",
    R"_({"request_id":"10","result":{"id":3,"name":"stored2","created":10.0,"json":"","labels":["test","web API"],"cases":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":""},"task_name":""}})_",
    R"_({"request_id":"11","result":0})_",
    R"_({"request_id":"12","result":[]})_",
    R"_({"request_id":"13","result":[{"id":1,"name":"updated model_info","created":4100.0,"json":"This mi has been updated"}]})_",
    R"_({"request_id":"14","result":[]})_",
    R"_({"request_id":"15","result":true})_",
    R"_({"request_id":"16","result":{"id":2,"name":"testrun","created":10.0,"json":"","labels":["test"],"model_refs":[]}})_",
    R"_({"request_id":"17","result":true})_",
    R"_({"request_id":"18","result":true})_",
    R"_({"request_id":"19","result":{"host":"testhost","port_num":12,"api_port_num":34,"model_key":"mkey"}})_",
    R"_({"request_id":"20","result":true})_",
  };
  REQUIRE_EQ(expected.size(), responses.size());

  for (size_t i = 0; i < expected.size(); ++i) {
    if (!expected[i].empty()) {
      CHECK_EQ(responses[i], expected[i]);
    } else {
      CHECK_EQ(responses[i].size(), -1);
    }
  }
  a.stop_web_api();
}

TEST_CASE("stm/task_web_api/publish_subscribe") {
  auto dirname = "em.srv.web_api.pub_sub.test" + to_string(to_seconds64(utctime_now()));
  ::test::utils::temp_dir tmpdir(dirname.c_str());
  string doc_root = (tmpdir / "web").string();

  // Set up test server:
  test_server a(tmpdir.string());
  string host_ip{"127.0.0.1"};
  a.set_listening_ip(host_ip);
  a.start_server();

  // Store some models:
  auto session1 = make_shared<stm_task>(1, "session1", from_seconds(3600));
  stm_case run1(1, "run1", from_seconds(3600));
  model_info mi1(session1->id, session1->name, session1->created, "");
  a.db.store_model(session1, mi1);

  int port = get_free_port();
  a.start_web_api(host_ip, port, doc_root, 1, 1);
  std::this_thread::sleep_for(std::chrono::milliseconds(700));
  REQUIRE_EQ(true, a.web_api_running());
  try {
    boost::asio::io_context ioc;
    auto s1 = std::make_shared<pub_sub_session>(ioc, &a);
    s1->run(host_ip, port);
    ioc.run();
    // Set up expected responses and comparisons.
    vector<string> expected{
      string(R"_({"request_id":"initial","result":[{"id":1,"name":"session1","created":3600.0,"json":""}]})_"),
      string(R"_({"request_id":"initial","result":[{"id":1,"name":"session1","created":3600.0,"json":""}]})_"),
      string(R"_({"request_id":"first update","result":true})_"),
      string(R"_({"request_id":"initial","result":[{"id":1,"name":"new name","created":11.0,"json":""}]})_"),
      string(R"_({"request_id":"initial","result":[{"id":1,"name":"second update","created":10.0,"json":""}]})_"),
      string(R"_({"request_id":"initial","result":[]})_"),
      string(R"_({"request_id":"store_model","result":2})_"),
      string(R"_({"request_id":"initial","result":[{"id":2,"name":"second run","created":10.0,"json":""}]})_"),
      string(
        R"_({"request_id":"read_subscribe","result":{"id":2,"name":"second run","created":10.0,"json":"","labels":["test"],"cases":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":""},"task_name":""}})_"),
      string(R"_({"request_id":"2nd model_store","result":2})_"),
      string(R"_({"request_id":"initial","result":[{"id":2,"name":"2nd run upd","created":15.0,"json":""}]})_"),
      string(
        R"_({"request_id":"read_subscribe","result":{"id":2,"name":"2nd run upd","created":15.0,"json":"","labels":["test","web"],"cases":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":""},"task_name":""}})_"),
      string(R"_({"request_id":"finish_mi","subscription_id":"initial","diagnostics":""})_"),
      string(R"_({"request_id":"finale","subscription_id":"read_subscribe","diagnostics":""})_")};

    auto responses = s1->responses_;
    s1.reset();

    REQUIRE_EQ(responses.size(), expected.size());
    for (auto i = 0u; i < responses.size(); ++i) {
      bool found_match = false;
      for (auto j = 0u; j < responses.size() && !found_match; ++j) {
        found_match = responses[j] == expected[i];
        if (found_match) {
          CHECK_EQ(responses[j], expected[i]);
        }
      }
      if (!found_match) {
        MESSAGE("failed for the " << i << "th response: " << expected[i] << "!=" << responses[i]);
        CHECK(found_match);
      }
    }
  } catch (std::exception& e) {
    MESSAGE("exception:" << e.what());
  }
  a.stop_web_api();
}

TEST_SUITE_END();
