#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include "build_test_system.h"
#include <fmt/format.h>

namespace test {
  using shyft::time_axis::generic_dt;
  using shyft::time_series::ts_point_fx;
  using shyft::time_series::dd::apoint_ts;
  using namespace shyft::energy_market::stm;
  using shyft::energy_market::hydro_power::connection_role;
  TEST_SUITE_BEGIN("stm");

  struct stm_patch_fixture {
    apoint_ts some_ts{
      generic_dt{utctime_0, deltahours(1),   3},
      {      1.0,           2.0, 3.0},
      ts_point_fx::POINT_AVERAGE_VALUE
    };
    apoint_ts custom_ts{
      generic_dt{utctime_0, deltahours(1),   3},
      {      1.0,           2.0, 3.0},
      ts_point_fx::POINT_AVERAGE_VALUE
    };
  };

  TEST_CASE_FIXTURE(stm_patch_fixture, "stm/patch/add") {

    SUBCASE("nothing") {
      auto s = make_shared<stm_system>();
      stm_system p;
      CHECK_NOTHROW(patch(s, stm_patch_op::add, p));
      auto s_empty = s->hps.empty() && s->contract_portfolios.empty() && s->unit_groups.empty() && s->market.empty()
                  && s->power_modules.empty();
      CHECK(s_empty);
      CHECK_EQ(patch(nullptr, stm_patch_op::add, p), false); // a a null returns false
    }
  }

  TEST_CASE_FIXTURE(stm_patch_fixture, "stm/patch/add/contract") {
    // minimal system
    auto s = make_shared<stm_system>(1, "s1", "{}");
    auto b = stm_hps_builder{make_shared<stm_hps>(1, "hps1")};
    s->hps.push_back(b.s);
    auto pp = b.create_power_plant(1, "pp1", "{}");
    auto m = make_shared<energy_market_area>(1, "NO1", "{}", s);
    s->market.push_back(m);
    auto pc2 = make_shared<contract>(2, "c2-already-on-the-model", "{}", s);
    s->contracts.push_back(pc2);
    // minimal patch
    auto p = make_shared<stm_system>(1, "p1", "{}");            // none of these matters
    auto pb = stm_hps_builder{make_shared<stm_hps>(1, "hps1")}; // object id matters here
    p->hps.push_back(pb.s);
    auto ppp = pb.create_power_plant(1, "pp1-not used", "{}"); // only object id matters
    auto pc = make_shared<contract>(0, "c3", "{}", p);
    auto pcr = make_shared<contract>(2, "cr-not-used", "{}", p); // only object id matters
    pc->active = some_ts;
    pc->tsm["custom"] = custom_ts;
    pc->power_plants.push_back(ppp); // add releation to power-plant in the patch
    p->contracts.push_back(pc);
    p->contracts.push_back(pcr);
    pc->add_relation(1, pcr, 3); // can only add after part of p
    patch(s, stm_patch_op::add, *p);
    REQUIRE(s->contracts.size() == 2); // 1 existing and one new
    CHECK(s->contracts.back()->id == pc->id);
    CHECK(s->contracts.back()->name == pc->name);
    CHECK(s->contracts.back()->json == pc->json);
    CHECK(s->contracts.back()->active == pc->active);
    CHECK(s->contracts.back()->tsm["custom"] == custom_ts);


    REQUIRE(s->contracts.back()->power_plants.size() == 1); // require relation
    CHECK(s->contracts.back()->power_plants.front()->id == pp->id);
    CHECK(s->contracts.back()->power_plants.front()->name == pp->name);
    CHECK(s->contracts.back()->power_plants.front()->json == pp->json);
    REQUIRE(s->contracts.back()->relations.size() == 1);
    CHECK(s->contracts.back()->relations.front()->relation_type == 3);
    CHECK(s->contracts.back()->relations.front()->related->id == 2);
  }

  TEST_CASE_FIXTURE(stm_patch_fixture, "stm/patch/add/hps/components") {
    // about this test:
    //  start with empty stm_system,
    //  then add components as patches
    auto s = make_shared<stm_system>(1, "s1", "{}"); // make an empty system

    /* step "add_r1" */ {
      auto p = make_shared<stm_system>(1, "p1", "{}");            // none of these matters
      auto pb = stm_hps_builder{make_shared<stm_hps>(1, "hps1")}; // new (parent) object
      p->hps.push_back(pb.s);                                     // add hps to the patch
      auto r1 = pb.create_reservoir(
        0, "R1", "the original"); // new object, with id=0, so we expect id to be autocreated to 1.
      patch(s, stm_patch_op::add, *p);
      // prove we can patch from empty to a hps with one r
      REQUIRE_EQ(s->hps.size(), 1);
      REQUIRE_EQ(s->hps.front()->reservoirs.size(), 1);
      r1->id = 1; // we reassign the id, to make the next comparison go straight
      REQUIRE_EQ(*std::dynamic_pointer_cast<reservoir>(s->hps.front()->reservoirs.front()), *r1);
    }
    /* step "add_w1" */ {
      auto p = make_shared<stm_system>(1, "p2", "{}");             // none of these matters
      auto pb = stm_hps_builder{make_shared<stm_hps>(1, "xhps1")}; // object id matters here
      p->hps.push_back(pb.s);                                      // add hps to the patch
      auto r1 = pb.create_reservoir(1, "R1", "just for ref");
      auto w1 = pb.create_tunnel(1, "W1", "original");
      connect(w1).input_from(r1);
      patch(s, stm_patch_op::add, *p);
      // prove we can patch a single waterway with upstream r1
      REQUIRE_EQ(s->hps.size(), 1);
      REQUIRE_EQ(s->hps.front()->reservoirs.size(), 1);
      REQUIRE_EQ(s->hps.front()->reservoirs.front()->json, "the original"); // ensure we get this right
      REQUIRE_EQ(s->hps.front()->waterways.size(), 1);
      CHECK_EQ(*std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways.front()), *w1);
      // ensure it is related to R1 as in our patch:
      REQUIRE_EQ(s->hps.front()->waterways.front()->upstreams.size(), 1);
      REQUIRE(s->hps.front()->waterways.front()->upstreams[0].target);
      auto r1x = std::dynamic_pointer_cast<reservoir>(s->hps.front()->waterways.front()->upstreams[0].target);
      REQUIRE(r1x != nullptr);
      CHECK_EQ(r1x->id, r1->id);
    }
    /* step "add_pp1,u1 and connect w1 u1" */ {
      auto p = make_shared<stm_system>(1, "p3", "{}");             // none of these matters
      auto pb = stm_hps_builder{make_shared<stm_hps>(1, "xhps1")}; // object id matters here
      p->hps.push_back(pb.s);                                      // add hps to the patch
      // -- details of the patch
      auto w1 = pb.create_tunnel(1, "W1", "just for ref");
      auto u1 = pb.create_unit(1, "U1", "original");
      auto p1 = pb.create_power_plant(1, "P1", "original");
      power_plant::add_unit(p1, u1);
      connect(u1).input_from(w1);
      // --
      patch(s, stm_patch_op::add, *p);
      // -- prove the patch worked as expected:
      REQUIRE_EQ(s->hps.size(), 1);
      REQUIRE_EQ(s->hps.front()->units.size(), 1);
      REQUIRE_EQ(s->hps.front()->power_plants.size(), 1);
      REQUIRE_EQ(s->hps.front()->waterways.size(), 1);
      CHECK_EQ(*std::dynamic_pointer_cast<unit>(s->hps.front()->units.front()), *u1);
      CHECK_EQ(*std::dynamic_pointer_cast<power_plant>(s->hps.front()->power_plants.front()), *p1);

      // ensure it is relations added as well:
      REQUIRE_EQ(s->hps.front()->waterways.front()->downstreams.size(), 1);
      REQUIRE(s->hps.front()->waterways.front()->downstreams[0].target);
      auto u1x = std::dynamic_pointer_cast<unit>(s->hps.front()->waterways.front()->downstreams[0].target);
      REQUIRE(u1x != nullptr);
      CHECK_EQ(u1x->id, u1->id);
    }
    /* step "add_ w2 w3 w4 with connections so we have R1-> w1->U1-w2-w3 and R1-w4->w3(bypass)" */ {
      auto p = make_shared<stm_system>(1, "p3", "{}");             // none of these matters
      auto pb = stm_hps_builder{make_shared<stm_hps>(1, "xhps1")}; // object id matters here
      p->hps.push_back(pb.s);                                      // add hps to the patch
      // -- details of the patch
      auto r1 = pb.create_reservoir(1, "R1", "just for ref");
      auto u1 = pb.create_unit(1, "U1", "just for ref");
      auto w1 = pb.create_tunnel(1, "W1", "just for ref");
      //--add a gate to W1
      auto g1 = pb.create_gate(1, "W1G1", "orignal");
      g1->opening.realised = some_ts;
      waterway::add_gate(w1, g1);
      auto w2 = pb.create_tunnel(2, "W2", "from u1");
      auto w3 = pb.create_river(3, "W3", "river downstream");
      auto w4 = pb.create_river(4, "W4", "bypass R1-> river downstream");
      auto g2 = pb.create_gate(
        0, "W4G1", "original"); // set id 0, and let patch auto create the gate id, should be 2 in this case
      waterway::add_gate(w4, g2);
      connect(w2).input_from(u1).output_to(w3);
      connect(w4).input_from(r1, connection_role::bypass).output_to(w3);
      // --
      patch(s, stm_patch_op::add, *p);
      // -- prove the patch worked as expected:
      g2->id = 2; // put into place the anticipated auto-id
      REQUIRE_EQ(s->hps.size(), 1);
      REQUIRE_EQ(s->hps.front()->units.size(), 1);
      REQUIRE_EQ(s->hps.front()->power_plants.size(), 1);
      REQUIRE_EQ(s->hps.front()->waterways.size(), 4);
      REQUIRE_EQ(std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[0])->gates.size(), 1);
      CHECK_EQ(*std::dynamic_pointer_cast<gate>(s->hps.front()->waterways[0]->gates[0]), *g1);

      CHECK_EQ(*std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[1]), *w2);
      CHECK_EQ(*std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[2]), *w3);
      CHECK_EQ(*std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[3]), *w4);
      REQUIRE_EQ(std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[3])->gates.size(), 1);
      CHECK_EQ(*std::dynamic_pointer_cast<gate>(s->hps.front()->waterways[3]->gates[0]), *g2);

      // ensure it is relations added as well:
      REQUIRE_EQ(s->hps.front()->reservoirs.front()->downstreams.size(), 2);
      CHECK_EQ(s->hps.front()->reservoirs.front()->downstreams[1].target->id, 4);
      CHECK_EQ(s->hps.front()->reservoirs.front()->downstreams[1].role, connection_role::bypass);
      CHECK_EQ(
        std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[1])->upstreams[0].target->name, "U1"); // the unit
      CHECK_EQ(
        std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[1])->upstreams[0].role,
        connection_role::input); // the unit

      CHECK_EQ(std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[1])->downstreams[0].target->name, "W3");
      CHECK_EQ(
        std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[1])->downstreams[0].role, connection_role::main);
      CHECK_EQ(
        std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[2])->upstreams.size(),
        2); // two upstreams of W3(W2 and W4)
      CHECK_EQ(std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[2])->upstreams[0].target->name, "W2");
      CHECK_EQ(
        std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[2])->upstreams[0].role, connection_role::input);
      CHECK_EQ(std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[2])->upstreams[1].target->name, "W4");
      CHECK_EQ(
        std::dynamic_pointer_cast<waterway>(s->hps.front()->waterways[2])->upstreams[1].role, connection_role::input);
    }
  }

  TEST_CASE_FIXTURE(stm_patch_fixture, "stm/patch/add/market_unit_groups") {
    // about this test:
    //  start with empty stm_system,
    //  then add components as patches
    auto s = make_shared<stm_system>(1, "s1", "{}"); // make an empty system
    // TODO:
    MESSAGE("stm/patch/add/market_unit_groups is not yet tested");
  }

  TEST_CASE_FIXTURE(stm_patch_fixture, "stm/patch/remove/contracts") {
    // minimal system
    auto s = make_shared<stm_system>(1, "s1", "{}");
    auto b = stm_hps_builder{make_shared<stm_hps>(1, "hps1")};
    s->hps.push_back(b.s);
    auto pp = b.create_power_plant(1, "pp1", "{}");
    auto m = make_shared<energy_market_area>(1, "NO1", "{}", s);
    s->market.push_back(m);
    auto pc2 = make_shared<contract>(2, "c2-already-on-the-model", "{}", s);
    s->contracts.push_back(pc2);
    auto pc3 = make_shared<contract>(3, "c3-already-on-the-model", "{}", s);
    s->contracts.push_back(pc3);
    auto pc4 = make_shared<contract>(4, "c4-already-on-the-model", "{}", s);
    s->contracts.push_back(pc4);
    auto pc5 = make_shared<contract>(5, "c5-already-on-the-model", "{}", s);
    s->contracts.push_back(pc5);
    pc5->add_relation(1, pc2, 22);
    pc5->add_relation(2, pc3, 99);
    auto port = make_shared<contract_portfolio>(5, "contract_portfolio", "", s);
    port->contracts.push_back(pc2);
    port->contracts.push_back(pc3);
    port->contracts.push_back(pc4);
    s->contract_portfolios.push_back(port);

    REQUIRE(s->contracts.size() == 4); // two existing
    REQUIRE(s->contracts.back()->relations.size() == 2);
    REQUIRE(s->contract_portfolios.size() == 1);
    REQUIRE(s->contract_portfolios.back()->contracts.size() == 3);

    // remove two of them:
    auto p = make_shared<stm_system>(1, "p1", "{}");             // none of these matters
    auto pcr = make_shared<contract>(2, "cr-not-used", "{}", p); // only object id matters
    p->contracts.push_back(pcr);
    auto pcr2 = make_shared<contract>(4, "cr2-not-used", "{}", p); // only object id matters
    p->contracts.push_back(pcr2);

    CHECK(patch(s, stm_patch_op::remove_objects, *p));
    REQUIRE(s->contracts.size() == 2); // two removed
    REQUIRE(s->contracts.back()->relations.size() == 1);
    REQUIRE(s->contracts.back()->relations.back()->related->id == 3);
    REQUIRE(s->contract_portfolios.size() == 1);
    REQUIRE(s->contract_portfolios.back()->contracts.size() == 1);
    REQUIRE(s->contract_portfolios.back()->contracts.back()->id == 3);
    // remove the other contracts, but also remove a different contract that does not exist:
    auto p2 = make_shared<stm_system>(99, "p77", "{}");           // none of these matters
    auto pcr3 = make_shared<contract>(3, "cr-not-used", "{}", p); // only object id matters
    auto pcr5 = make_shared<contract>(5, "cr-not-used", "{}", p); // only object id matters
    auto pcr9 = make_shared<contract>(9, "cr-not-used", "{}", p); // only object id matters
    p2->contracts.push_back(pcr3);
    p2->contracts.push_back(pcr5);
    p2->contracts.push_back(pcr9);

    CHECK(!patch(s, stm_patch_op::remove_objects, *p2)); // returns false, not all to remove found
    REQUIRE(s->contracts.size() == 0);                   // other one removed
    REQUIRE(s->contract_portfolios.size() == 1);
    REQUIRE(s->contract_portfolios.back()->contracts.size() == 0);
  }

  TEST_SUITE_END();
}
