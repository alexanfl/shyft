#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/time_series/dd/apoint_ts.h>

using namespace shyft::energy_market::stm;
using namespace shyft::time_series::dd;

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/wind_farm_construct") {
  auto sys = std::make_shared<stm_system>(1, "sys", "{}");
  auto wt = std::make_shared<wind_farm>(3, "wf1", "{}", sys);
  FAST_CHECK_UNARY(wt);
  FAST_CHECK_EQ(wt->id, 3);
  FAST_CHECK_EQ(wt->name, "wf1");
  FAST_CHECK_EQ(wt->json, "{}");
}

TEST_CASE("stm/wind_farm_equality") {
  // Test that stm::wind_turbine_equality::operator== checks member equality:
  auto sys1 = std::make_shared<stm_system>(1, "sys", "{}");
  auto wf_1 = std::make_shared<wind_farm>(1, "wf1", "{}", sys1);
  // same object, address equality:
  CHECK_EQ(*wf_1, *wf_1);
  // add one with different id, ne:
  auto wf_2 = std::make_shared<wind_farm>(2, "wf1", "{}", sys1);
  CHECK_NE(*wf_1, *wf_2);
  // on a different system, same id and name, both empty, equal:
  auto sys2 = std::make_shared<stm_system>(2, "sys", "{}");
  auto wf_3 = std::make_shared<wind_farm>(1, "wf1", "{}", sys2);
  CHECK_EQ(*wf_1, *wf_3);

  // set attr on first to a ts, ne:
  auto ref_ts = apoint_ts{"shyft://test/wf1/production_realised"};
  wf_1->production.realised = ref_ts;
  CHECK_NE(*wf_1, *wf_3);
  // same ts on both, eq:
  wf_3->production.realised = ref_ts;
  CHECK_EQ(*wf_1, *wf_3);
}

TEST_SUITE_END();
