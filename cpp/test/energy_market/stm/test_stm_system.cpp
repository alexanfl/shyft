#include <doctest/doctest.h>

#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/graph/graph_utilities.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/functional.h>
#include <shyft/energy_market/stm/model.h>

#include <test/energy_market/stm/utilities.h>
#include <test/energy_market/stm/build_test_system.h>
#include <test/energy_market/serialize_loop.h>

namespace shyft::energy_market::stm {

  TEST_SUITE_BEGIN("stm");

  using namespace shyft::core;
  using test::serialize_loop;

  TEST_CASE("stm/hydro_power_system_serialization") {
    auto a = test::create_stm_hps();
    // just verify that we got a complete hooked up model:
    auto t = a->find_waterway_by_name("lauvastølsvatn-til-kvilldal");
    CHECK(t != nullptr);
    CHECK(t->gates.size() == 1u); // should be one here..
    CHECK(t->gates[0]->wtr_() == t);
    auto t_stm = std::dynamic_pointer_cast<stm::waterway>(t);
    CHECK(t_stm != nullptr);
    auto t_hps = t->hps_();
    CHECK(t_hps != nullptr);
    auto s_stm = std::dynamic_pointer_cast<stm::stm_hps>(t_hps);
    CHECK(s_stm != nullptr);

    auto b = test::create_stm_hps();
    CHECK(a != nullptr);
    CHECK(b != nullptr);
    CHECK(a->equal_structure(*b) == true);
    SUBCASE("serialization") {
      auto b_as_blob = stm_hps::to_blob(b);
      CHECK(b_as_blob.size() > 0);
      auto b_from_blob = stm_hps::from_blob(b_as_blob);
      CHECK(b_from_blob->equal_structure(*b));
    }
    a->clear();
    b->clear();
  }

  TEST_CASE("stm/hps_equality") {

    using builder = stm::stm_hps_builder;

    auto hps_1 = std::make_shared<stm_hps>(1, "test_hps");
    auto hps_2 = std::make_shared<stm_hps>(1, "test_hps");
    CHECK_EQ(*hps_1, *hps_2); // Both empty, hence equal

    SUBCASE("waterway content") {
      auto wtr_1 = builder(hps_1).create_waterway(2, "test_wtr", "");
      wtr_1->head_loss_coeff = test::create_t_double(100, 0.001);
      CHECK_NE(*hps_1, *hps_2); // hps_1 is non-empty

      auto wtr_2 = builder(hps_2).create_waterway(2, "test_wtr", "");
      CHECK_NE(*hps_1, *hps_2); // since wtr_1 != wtr_2

      wtr_2->head_loss_coeff = test::create_t_double(100, 0.001);
      CHECK_EQ(*hps_1, *hps_2); // since now wtr_1 == wtr_2
    }

    SUBCASE("reservoir content") {
      auto rsv_1 = builder(hps_1).create_reservoir(3, "test_rsv", "");
      rsv_1->level.regulation_min = test::create_t_double(100, 100.0);
      CHECK_NE(*hps_1, *hps_2); // hps_1 is non-empty

      auto rsv_2 = builder(hps_2).create_reservoir(3, "test_rsv", "");
      CHECK_NE(*hps_1, *hps_2); // since rsv_1 != rsv_2

      rsv_2->level.regulation_min = test::create_t_double(100, 100.0);
      CHECK_EQ(*hps_1, *hps_2); // since now rsv_1 == rsv_2
    }

    SUBCASE("unit_content") {
      auto unit_1 = builder(hps_1).create_unit(4, "test_unit", "");
      unit_1->production.static_max = test::create_t_double(100, 10.);
      CHECK_NE(*hps_1, *hps_2); // hps_1 is non-empty

      auto unit_2 = builder(hps_2).create_unit(4, "test_unit", "");
      CHECK_NE(*hps_1, *hps_2); // since unit_1 != unit_2

      unit_2->production.static_max = test::create_t_double(100, 10.);
      CHECK_EQ(*hps_1, *hps_2); // since now unit_1 == unit_2
    }

    SUBCASE("plant_content") {
      auto plant_1 = builder(hps_1).create_power_plant(5, "test_plant", "");
      plant_1->production.constraint_max = test::create_t_double(100, 10.);
      CHECK_NE(*hps_1, *hps_2); // hps_1 is non-empty

      auto plant_2 = builder(hps_2).create_power_plant(5, "test_plant", "");
      CHECK_NE(*hps_1, *hps_2); // since plant_1 != plant_2

      plant_2->production.constraint_max = test::create_t_double(100, 10.);
      CHECK_EQ(*hps_1, *hps_2); // since now plant_1 == plant_2
    }
  }

  TEST_CASE("stm/hydro_power_system_topology") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_stm_hps();
    {
      auto blasjo1 = a->find_reservoir_by_name("blåsjø1");
      CHECK(blasjo1 != nullptr);
      auto blasjo_ds = graph::downstream_units(blasjo1);
      auto blasjo_us = graph::upstream_units(blasjo1);
      CHECK(blasjo_ds.size() == 1);
      CHECK(blasjo_us.size() == 0);
      auto vassbotvatn = a->find_reservoir_by_name("vassbotvatn");
      CHECK(vassbotvatn != nullptr);
      auto vassbotvatn_us = graph::upstream_units(vassbotvatn);
      auto vassbotvatn_ds = graph::downstream_units(vassbotvatn);
      CHECK(vassbotvatn_us.size() == 1);
      CHECK(vassbotvatn_us[0]->name == "stølsdal pumpe");
      CHECK(vassbotvatn_ds.size() == 0);
      SUBCASE("remove connection to water-route") {
        hydro_component::disconnect(vassbotvatn, vassbotvatn->upstreams[0].target);
        CHECK(graph::upstream_units(vassbotvatn).size() == 0);
      }

      // Test reservoir aggregate auxiliary functions
      CHECK(a->find_reservoir_aggregate_by_name("blåsjø")->id == 22);
      CHECK(a->find_reservoir_aggregate_by_id(22)->name == "blåsjø");

      SUBCASE("power_stations up and down streams") {
        auto saurdal = a->find_unit_by_name("saurdal");
        CHECK(saurdal != nullptr);
        auto rsv_us = graph::upstream_reservoirs(saurdal);
        CHECK(rsv_us.size() == 1);
        CHECK(rsv_us[0]->name == "blåsjø1");

        // NOTE, junction is not yet supported auto rsv_ds = saurdal->downstream_reservoir();
        auto kvilldal = a->find_unit_by_name("kvilldal_1");
        auto rsv_ds = graph::downstream_reservoirs(kvilldal);
        CHECK(rsv_ds.size() == 1);
        CHECK(rsv_ds[0]->name == "suldalsvatn");
      }
    }
    a->clear();
    a = nullptr;
  }

  TEST_CASE("stm/system_serialization") {
    auto a = std::make_shared<stm_system>(1, "ull", "aførre");
    auto sys_builder = stm_builder(a);
    auto no1 = sys_builder.create_market_area(1, "NO1", "xx");
    sys_builder.create_market_area(2, "NO2", "xx");
    sys_builder.create_market_area(3, "NO3", "xx");
    sys_builder.create_market_area(4, "NO4", "xx");
    auto hps1=test::create_stm_hps(1, "oslo");
    auto hps2=test::create_stm_hps(2, "sørland");
    hps1->system=a;
    hps2->system=a;
    a->hps.push_back(hps2);
    a->hps.push_back(hps1);
    auto ug = a->add_unit_group(1, "ug1", "{}");                                          // make a unit group
    ug->add_unit(std::dynamic_pointer_cast<stm::unit>(a->hps[0]->units[0]), apoint_ts()); // with one unit
    CHECK_THROWS_AS(a->add_unit_group(1, "ug2", "{}"), std::runtime_error); // verify we can keep it somewhat unique
    CHECK_THROWS_AS(a->add_unit_group(2, "ug1", "{}"), std::runtime_error);
    // add contract, contract-portfolio, plus some relations
    auto c1 = std::make_shared<contract>(5, "C1", "xx", a);
    auto pc1 = std::make_shared<contract_portfolio>(6, "P1", "yy", a);
    c1->power_plants.push_back(std::dynamic_pointer_cast<stm::power_plant>(a->hps[0]->power_plants[0]));
    a->contracts.push_back(c1);
    a->contract_portfolios.push_back(pc1);
    c1->add_to_portfolio(pc1);
    c1->add_to_energy_market_area(no1);
    // add network, transmission_line, busbar, power_module, wind_farm
    auto n = sys_builder.create_network(7, "N1", "xx");
    auto net_builder = network_builder(n);
    auto t = net_builder.create_transmission_line(8, "T1", "xx");
    auto b1 = net_builder.create_busbar(9, "B1", "xx");
    b1->add_to_start_of_transmission_line(t);
    auto p1 = sys_builder.create_power_module(10, "P1", "xx");
    b1->add_power_module(p1, apoint_ts{});
    b1->add_unit(std::dynamic_pointer_cast<stm::unit>(a->hps[0]->units[0]), apoint_ts{});
    auto wp = sys_builder.create_wind_farm(9, "WF1", "xx");
    b1->add_wind_farm(wp, apoint_ts{});

    auto a_blob = stm_system::to_blob(a);
    auto b = stm_system::from_blob(a_blob);
    CHECK(b != nullptr);
    CHECK_EQ(b->name, a->name);
    CHECK_EQ(b->id, a->id);
    CHECK_EQ(b->json, a->json);
    CHECK_EQ(b->market.size(), a->market.size());
    CHECK_EQ(b->hps.size(), a->hps.size());
    CHECK_EQ(b->hps[0]->system_(),b);//verify we keep weakptr over blobbing
    REQUIRE_EQ(b->unit_groups.size(), 1);
    CHECK_EQ(b->unit_groups[0]->members.size(), 1);
    // verify we got it right with the contracts.
    CHECK_EQ(b->contracts.size(), a->contracts.size());
    CHECK_EQ(b->contracts[0]->get_energy_market_areas().size(), 1);
    CHECK_EQ(b->contracts[0]->get_portfolios().size(), 1);
    CHECK_EQ(b->contract_portfolios.size(), a->contract_portfolios.size());
    // verify networks, transmission_lines, busbars, power_modules, wind_farms
    CHECK_EQ(b->networks.size(), a->networks.size());
    CHECK_EQ(b->networks[0]->transmission_lines.size(), a->networks[0]->transmission_lines.size());
    CHECK_EQ(b->networks[0]->busbars.size(), a->networks[0]->busbars.size());
    CHECK_EQ(
      b->networks[0]->busbars[0]->get_transmission_lines_from_busbar().size(),
      a->networks[0]->busbars[0]->get_transmission_lines_from_busbar().size());
    CHECK_EQ(
      b->networks[0]->busbars[0]->get_transmission_lines_to_busbar().size(),
      a->networks[0]->busbars[0]->get_transmission_lines_to_busbar().size());
    CHECK_EQ(b->networks[0]->busbars[0]->power_modules.size(), a->networks[0]->busbars[0]->power_modules.size());
    CHECK_EQ(b->networks[0]->busbars[0]->units.size(), a->networks[0]->busbars[0]->units.size());
    CHECK_EQ(b->networks[0]->busbars[0]->wind_farms.size(), a->networks[0]->busbars[0]->wind_farms.size());
    CHECK_EQ(b->power_modules.size(), a->power_modules.size());
    CHECK_EQ(b->wind_farms.size(), a->wind_farms.size());
  }

  TEST_CASE("stm/system_proxy_attributes") {
    using namespace shyft::energy_market::attr_traits;

    auto a = std::make_shared<stm_system>(1, "ulla", "førre");
    // 1. Check non-existence
    CHECK_EQ(false, exists(a->run_params.run_time_axis));
    CHECK_EQ(false, exists(a->run_params.fx_log));

    // 2. Set attributes, and check
    a->run_params.n_inc_runs = 2;
    a->run_params.n_full_runs = 3;
    a->run_params.head_opt = true;
    a->run_params.fx_log = {
      {from_seconds(0),       "a string"},
      {from_seconds(1), "another string"}
    };
    generic_dt ta(std::vector<utctime>{
      from_seconds(0), from_seconds(1), from_seconds(2), from_seconds(3), from_seconds(4), from_seconds(5)});
    a->run_params.run_time_axis = ta;
    CHECK_EQ(true, exists(a->run_params.run_time_axis));
    CHECK_EQ(true, exists(a->run_params.fx_log));
    CHECK_EQ(2, a->run_params.n_inc_runs);
    CHECK_EQ(3, a->run_params.n_full_runs);
    CHECK_EQ(true, a->run_params.head_opt);
    CHECK_EQ(ta, a->run_params.run_time_axis);
    auto& msgs = a->run_params.fx_log;
    CHECK_EQ(msgs.size(), 2);
    CHECK_EQ(msgs[0].second, "a string");
    CHECK_EQ(msgs[1].second, "another string");

    // 3. Serialize and deserialize, and check that values are persisted
    auto a_blob = stm_system::to_blob(a);
    auto b = stm_system::from_blob(a_blob);
    CHECK(b != nullptr);
    CHECK_EQ(true, exists(b->run_params.run_time_axis));
    CHECK_EQ(true, exists(b->run_params.fx_log));

    CHECK_EQ(2, b->run_params.n_inc_runs);
    CHECK_EQ(3, b->run_params.n_full_runs);
    CHECK_EQ(true, b->run_params.head_opt);
    CHECK_EQ(ta, b->run_params.run_time_axis);
    auto& bmsgs = b->run_params.fx_log;
    CHECK_EQ(bmsgs.size(), 2);
    CHECK_EQ(bmsgs[0].second, "a string");
    CHECK_EQ(bmsgs[1].second, "another string");
  }

  TEST_CASE("stm/functional") {
    auto model = test::create_stm_hps();

    auto inc_visitor = [](int v, ignore_t, ignore_t) {
      return v + 1;
    };
    auto dec_visitor = [](int v, ignore_t, ignore_t) {
      return v - 1;
    };
    auto ign_visitor = [](int v, ignore_t, ignore_t) {
      return v;
    };

    auto pre_order_count = component_traverse(*model, 0, inc_visitor, ign_visitor);
    CHECK(pre_order_count == 29);
    auto post_order_count = component_traverse(*model, 0, ign_visitor, inc_visitor);
    CHECK(post_order_count == 29);
    CHECK(component_traverse(*model, 0, inc_visitor, dec_visitor) == 0);
  }

  TEST_CASE("stm/hps_builder") {

    auto hps = std::make_shared<stm_hps>(0, "test_system");
    auto builder = stm_hps_builder(hps);

    SUBCASE("create_reservoir_unique_id") {
      builder.create_reservoir(0, "rsv_0", "");
      CHECK_THROWS_AS(builder.create_reservoir(0, "rsv_1", ""), stm_rule_exception);
    }

    SUBCASE("create_unit_unique_id") {
      auto hps = std::make_shared<stm_hps>(0, "test_system");
      auto builder = stm_hps_builder(hps);
      builder.create_unit(0, "unit_0", "");
      CHECK_THROWS_AS(builder.create_unit(0, "unit_1", ""), stm_rule_exception);
    }

    SUBCASE("create_power_plant_unique_id") {
      builder.create_power_plant(0, "wtr_0", "");
      CHECK_THROWS_AS(builder.create_power_plant(0, "wtr_1", ""), stm_rule_exception);
    }

    SUBCASE("create_waterway_unique_id") {
      builder.create_waterway(0, "wtr_0", "");
      CHECK_THROWS_AS(builder.create_waterway(0, "wtr_1", ""), stm_rule_exception);
    }

    SUBCASE("create_gate_unique_id") {
      auto w = builder.create_waterway(8, "wtr_xy", "");
      auto g = builder.create_gate(0, "gate_1", "");
      stm::waterway::add_gate(w, g);
      CHECK_THROWS_AS(builder.create_gate(0, "gate_1", ""), stm_rule_exception);
    }

    SUBCASE("create_reservoir_unique_name") {
      builder.create_reservoir(0, "rsv_0", "");
      CHECK_THROWS_AS(builder.create_reservoir(1, "rsv_0", ""), stm_rule_exception);
    }

    SUBCASE("create_unit_unique_name") {
      builder.create_unit(0, "unit_0", "");
      CHECK_THROWS_AS(builder.create_unit(1, "unit_0", ""), stm_rule_exception);
    }

    SUBCASE("create_power_plant_unique_name") {
      builder.create_power_plant(1, "wtr_0", "");
      CHECK_THROWS_AS(builder.create_power_plant(1, "wtr_0", ""), stm_rule_exception);
    }

    SUBCASE("create_waterway_unique_name") {
      builder.create_waterway(0, "wtr_0", "");
      CHECK_THROWS_AS(builder.create_waterway(1, "wtr_0", ""), stm_rule_exception);
    }

    SUBCASE("create_gate_unique_name") {
      auto w = builder.create_waterway(9, "wtr_xx", "");
      auto g = builder.create_gate(7, "gate_0", "");
      stm::waterway::add_gate(w, g);
      CHECK_THROWS_AS(builder.create_gate(1, "gate_0", ""), stm_rule_exception);
    }
  }

  TEST_CASE("stm/builder") {

    auto sys = std::make_shared<stm_system>(1, "sys", "{}");
    auto builder = stm_builder(sys);

    SUBCASE("create_markets") {

      auto mkt = builder.create_market_area(8, "ema", "{}");
      CHECK_THROWS(builder.create_market_area(9, "ema", "{}"));  // same name
      CHECK_THROWS(builder.create_market_area(8, "ema2", "{}")); // same id
      CHECK_EQ(sys->market.size(), 1);

      auto net = builder.create_network(8, "net", "{}");
      CHECK_THROWS(builder.create_network(8, "net2", "{}")); // same id
      CHECK_THROWS(builder.create_network(9, "net", "{}"));  // same name
      CHECK_EQ(sys->networks.size(), 1);

      auto pm = builder.create_power_module(8, "pm", "{}");
      CHECK_THROWS(builder.create_power_module(8, "pm2", "{}")); // same id
      CHECK_THROWS(builder.create_power_module(9, "pm", "{}"));  // same name
      CHECK_EQ(sys->power_modules.size(), 1);
    }

    SUBCASE("create_busbar_derived_unit_group") {
      auto mkt = builder.create_market_area(8, "ema", "{}");
      auto ug = mkt->create_busbar_derived_unit_group(9, "NO1", "{}");

      CHECK_THROWS(
        mkt->create_busbar_derived_unit_group(10, "NO2", "{}")); // A busbar_derived_unit_group is already created
      mkt->remove_unit_group();                                  // Remove "NO1" from market_area
      CHECK_THROWS(
        mkt->create_busbar_derived_unit_group(9, "NO1", "{}")); // The NO1 unit_group is already added to the stm_system
      auto ug2 = mkt->create_busbar_derived_unit_group(11, "NO3", "{}"); // new unit group ok
      mkt->remove_unit_group();
      mkt->set_unit_group(ug); // Set unit group (will not be busbar derived)
      mkt->remove_unit_group();
      // Disallow negative units
      ug->id = -1;
      CHECK_THROWS(mkt->set_unit_group(ug));
    }

    SUBCASE("market_area_create_busbar_derived_unit_group_adds_existing_units_from_busbars") {
      auto mkt = builder.create_market_area(8, "ema", "{}");
      auto net = builder.create_network(9, "Network1", "{}");
      auto net_builder = network_builder(net);
      auto b1 = net_builder.create_busbar(21, "busbar1", "{}");
      auto u1 = std::make_shared<stm::unit>();
      b1->add_unit(u1, apoint_ts{});
      b1->add_to_market_area(mkt);
      auto ug = mkt->create_busbar_derived_unit_group(9, "NO1", "{}");
      CHECK_EQ(ug->members.size(), 1);
      CHECK_EQ(ug->members.at(0)->unit, u1);
    }
  }

  TEST_SUITE_END();
}
