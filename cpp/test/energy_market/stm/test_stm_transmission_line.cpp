#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <string>
#include <iterator>

using namespace shyft::energy_market::stm;
using std::make_shared;

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/transmission_line_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto net = make_shared<network>(2, "net", "{}", sys);
  auto t = make_shared<transmission_line>(3, "t", "{}", net);
  FAST_CHECK_UNARY(t);
  FAST_CHECK_EQ(t->id, 3);
  FAST_CHECK_EQ(t->name, "t");
  FAST_CHECK_EQ(t->json, "{}");
  FAST_CHECK_EQ(*t, *t);
}

TEST_CASE("stm/transmission_line_generate_url") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto net = make_shared<network>(2, "net", "{}", sys);
  auto t = make_shared<transmission_line>(3, "t", "{}", net);
  std::string s;
  auto rbi = std::back_inserter(s);
  t->generate_url(rbi);
  FAST_CHECK_EQ(s, "/n2/t3");
  s.clear();
  t->generate_url(rbi, -1, 0);
  FAST_CHECK_EQ(s, "/n{parent_id}/t{o_id}");
  s.clear();
  t->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/n{parent_id}/t3");
  s.clear();
  t->generate_url(rbi, -1, 2);
  FAST_CHECK_EQ(s, "/n2/t3");
  s.clear();
}

TEST_SUITE_END();
