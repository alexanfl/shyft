#include <doctest/doctest.h>

#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <test/energy_market/serialize_loop.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/contract_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ctr = make_shared<contract>(1, "ctr", "{}", sys);
  auto ctr2 = make_shared<contract>(2, "ctr2", "{}", sys);
  sys->contracts.push_back(ctr);
  sys->contracts.push_back(ctr2);
  auto h = make_shared<stm_hps>(1, "hps");
  sys->hps.push_back(h);
  stm_hps_builder b(h);
  auto u = b.create_unit(1, "u1", "{}");
  auto p = b.create_power_plant(1, "p1", "{}");
  power_plant::add_unit(p, u);
  ctr->power_plants.push_back(p);
  FAST_CHECK_UNARY(ctr);
  FAST_CHECK_EQ(ctr->id, 1);
  FAST_CHECK_EQ(ctr->name, "ctr");
  FAST_CHECK_EQ(ctr->json, "{}");
  FAST_REQUIRE_EQ(ctr->power_plants.size(), 1);
  FAST_CHECK_EQ(*p, *ctr->power_plants[0]);
  auto r1 = ctr->add_relation(0, ctr2, 0);
  CHECK_EQ(ctr->relations[0]->related, ctr2);
  auto related_to_ctr=ctr2->find_related_to_this();
  REQUIRE_EQ(related_to_ctr.size(),1u);
  CHECK_EQ(related_to_ctr[0]->id,1u);

  ctr->tsm["x"] = apoint_ts{};
  auto o = test::serialize_loop(sys);
  FAST_CHECK_EQ(o->contracts[0]->power_plants.size(), 1);
  FAST_CHECK_EQ(o->contracts[0]->relations.size(), 1);
  FAST_CHECK_EQ(*o, *sys);
}

TEST_CASE("stm/contract_market_association") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto mkt = make_shared<energy_market_area>(1, "ema", "{}", sys);
  auto ctr = make_shared<contract>(1, "c1", "{}", sys);
  FAST_CHECK_EQ(sys->market.size(), 0);
  FAST_CHECK_EQ(sys->contracts.size(), 0);
  FAST_CHECK_EQ(mkt->contracts.size(), 0);
  FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 0);
  sys->market.push_back(mkt);
  sys->contracts.push_back(ctr);
  mkt->contracts.push_back(ctr);
  FAST_CHECK_EQ(sys->market.size(), 1);
  FAST_CHECK_EQ(sys->contracts.size(), 1);
  FAST_CHECK_EQ(mkt->contracts.size(), 1);
  FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 1);
  mkt->contracts.clear();
  FAST_CHECK_EQ(mkt->contracts.size(), 0);
  FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 0);
  ctr->add_to_energy_market_area(mkt);
  FAST_CHECK_EQ(mkt->contracts.size(), 1);
  FAST_CHECK_EQ(ctr->get_energy_market_areas().size(), 1);
}

TEST_CASE("stm/contract_portfolio_association") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto cpf = make_shared<contract_portfolio>(1, "cp", "{}", sys);
  auto ctr = make_shared<contract>(1, "c1", "{}", sys);
  FAST_CHECK_EQ(sys->contract_portfolios.size(), 0);
  FAST_CHECK_EQ(sys->contracts.size(), 0);
  FAST_CHECK_EQ(cpf->contracts.size(), 0);
  FAST_CHECK_EQ(ctr->get_portfolios().size(), 0);
  sys->contract_portfolios.push_back(cpf);
  sys->contracts.push_back(ctr);
  cpf->contracts.push_back(ctr);
  FAST_CHECK_EQ(sys->contract_portfolios.size(), 1);
  FAST_CHECK_EQ(sys->contracts.size(), 1);
  FAST_CHECK_EQ(cpf->contracts.size(), 1);
  FAST_CHECK_EQ(ctr->get_portfolios().size(), 1);
  cpf->contracts.clear();
  FAST_CHECK_EQ(cpf->contracts.size(), 0);
  FAST_CHECK_EQ(ctr->get_portfolios().size(), 0);
  ctr->add_to_portfolio(cpf);
  FAST_CHECK_EQ(cpf->contracts.size(), 1);
  FAST_CHECK_EQ(ctr->get_portfolios().size(), 1);
}

TEST_CASE("stm/contract_relation") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto c1 = make_shared<contract>(1, "c1", "{}", sys);
  auto c2 = make_shared<contract>(2, "c2", "{}", sys);
  auto c3 = make_shared<contract>(3, "c3", "{}", sys);
  auto c4 = make_shared<contract>(4, "c4", "{}", sys);
  sys->contracts.push_back(c1);
  sys->contracts.push_back(c2);
  sys->contracts.push_back(c3);
  sys->contracts.push_back(c4);

  auto const rt = 0; // relation_type
  auto r1 = c1->add_relation(1, c2, rt);
  auto r2_1 = c2->add_relation(2, c3, rt);
  auto r2_2 = c2->add_relation(3, c4, rt);

  CHECK_EQ(c1->relations.size(), 1);
  CHECK_EQ(c1->relations[0], r1);
  CHECK_EQ(r1->related, c2);
  CHECK_EQ(r1->relation_type, rt);
  CHECK_EQ(r1->owner_(), c1);

  CHECK_EQ(c2->relations.size(), 2);
  CHECK_EQ(c2->relations[0], r2_1);
  CHECK_EQ(c2->relations[1], r2_2);
  CHECK_EQ(r2_1->related, c3);
  CHECK_EQ(r2_2->related, c4);
  CHECK_EQ(r2_1->owner_(), c2);
  CHECK_EQ(r2_2->owner_(), c2);

  CHECK_THROWS(c1->add_relation(1, c3, rt));  // Id Already exists
  CHECK_THROWS(c2->add_relation(4, c4, rt));  // Relation already exists
  CHECK_THROWS(c4->add_relation(4, c4, rt));  // Not allowed to add self
  CHECK_THROWS(c3->add_relation(4, c1, rt));  // Loop not allowed
  CHECK_THROWS(c3->add_relation(4, c2, rt));  // Loop not allowed
  CHECK_THROWS(c1->remove_relation(nullptr)); // cannot remove non-existing

  auto r3_2 = c3->add_relation(5, c2, rt + 1); // Loop allowed, when relation is of different type

  auto r2_3 = c2->add_relation(6, c4, 9); // Allowed to add parallell relation of a different relation_type
  CHECK_EQ(r2_3->relation_type, 9);
  CHECK_EQ(c2->relations[2], r2_3);

  CHECK_EQ(c1->relations.size(), 1);
  c1->remove_relation(r1);
  CHECK_EQ(c1->relations.size(), 0);

  CHECK_EQ(c2->relations.size(), 3);
  c2->remove_relation(r2_1);
  CHECK_EQ(c2->relations.size(), 2);
  CHECK_EQ(c2->relations[0], r2_2);
}

TEST_SUITE_END();
