#include <doctest/doctest.h>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

using std::vector;
using std::isfinite;
using std::runtime_error;
using namespace shyft::energy_market::hydro_power;

TEST_SUITE_BEGIN("em");

TEST_CASE("xy/point") {
  point a(0.0, 1.0);
  point b(0.0, 1.0);
  CHECK_EQ(a, b);
  CHECK_UNARY_FALSE(a != b);
  b.y = 2.0;
  CHECK_LT(a, b);
}

TEST_CASE("xy/basics") {
  xy_point_curve a;
  xy_point_curve b;
  CHECK_EQ(a, b);
  CHECK_EQ(isfinite(a.calculate_x(1.0)), false);
  CHECK_EQ(isfinite(a.calculate_y(1.0)), false);
  a.points.push_back(point(0.0, 1.0));
  CHECK_NE(a, b);
  CHECK_EQ(isfinite(a.calculate_x(1.0)), false); // Cannot invert constant
  CHECK_EQ(a.calculate_y(0.0), doctest::Approx(1.0));
  CHECK_EQ(false, a.is_xy_mono_increasing());
  a.points.push_back(point(1.0, 2.0));
  CHECK_EQ(true, a.is_xy_mono_increasing());
  a.points.push_back(point(2.0, 1.9));
  CHECK_EQ(false, a.is_xy_mono_increasing());
  xy_point_curve c(vector<point>{point(0.0, 1.0), point(1.0, 2.0)});
  xy_point_curve d(c);
  CHECK_EQ(c, d);
  xy_point_curve e = xy_point_curve::make(vector<double>{0.0, 1.0}, vector<double>{0.0, 2.0});
  CHECK_EQ(e.points.size(), 2u);
  try {
    xy_point_curve e = xy_point_curve::make(vector<double>{0.0, 1.0}, vector<double>{0.0});
    CHECK_UNARY(false);
  } catch (runtime_error const &) {
    CHECK_UNARY(true);
  }
}

TEST_CASE("xy/properties") {
  xy_point_curve a;
  xy_point_curve b;
  xy_point_curve c;

  a.points.emplace_back(0.0, 0.0);
  a.points.emplace_back(1.0, 1.0);
  a.points.emplace_back(2.0, 4.0);

  b.points.emplace_back(0.0, 1.0);
  b.points.emplace_back(1.0, 0.0);
  b.points.emplace_back(2.0, 2.0);

  c.points.emplace_back(0.0, 0.0);
  c.points.emplace_back(1.0, 2.0);
  c.points.emplace_back(2.0, 3.0);

  CHECK_EQ(true, a.is_xy_convex());
  CHECK_EQ(true, a.is_xy_invertible());

  CHECK_EQ(true, b.is_xy_convex());
  CHECK_EQ(false, b.is_xy_invertible());

  CHECK_EQ(false, c.is_xy_convex());
  CHECK_EQ(true, c.is_xy_invertible());
}

TEST_CASE("xy/xyz_basics") {
  xy_point_curve_with_z a;
  xy_point_curve_with_z b;
  CHECK_EQ(a, b);
  b.z = 1.0;
  CHECK_NE(a, b);
  xy_point_curve_with_z c(xy_point_curve(vector<point>{point(0.0, 0.0), point(1.0, 1.0)}), 3.0);
  CHECK_EQ(c.z, doctest::Approx(3.0));
  CHECK_EQ(c.xy_curve.calculate_x(0.5), doctest::Approx(0.5));
  CHECK_EQ(c.xy_curve.calculate_y(0.5), doctest::Approx(0.5));
  CHECK_EQ(c.xy_curve.calculate_y(1.5), doctest::Approx(1.5));
  CHECK_EQ(c.xy_curve.calculate_y(-1.5), doctest::Approx(-1.5));
  CHECK_EQ(c.xy_curve.calculate_x(1.5), doctest::Approx(1.5));
  CHECK_EQ(c.xy_curve.calculate_x(-1.5), doctest::Approx(-1.5));
}

TEST_CASE("em/xy/xyz_point_curve") {
  auto f0 = xy_point_curve(vector<point>{point(1.0, 1.0), point(3.0, 3.0)});
  auto f1 = xy_point_curve(vector<point>{point(2.0, 4.0), point(4.0, 2.0)});
  double z0 = 2.0;
  double z1 = 4.0;
  SUBCASE("set_curve") {
    auto c = xyz_point_curve{};
    CHECK_EQ(c.curves.size(), 0);

    c.set_curve(z0, f0);
    CHECK_EQ(c.curves.size(), 1);

    c.set_curve(z1, f0);
    CHECK_EQ(c.curves.size(), 2);

    c.set_curve(z1, f1);
    CHECK_EQ(c.curves.size(), 2); // curve for z1 already exists

    // check that the curve for z1 was changed
    CHECK_EQ(c.get_curve(z1), f1);
  }

  SUBCASE("constructor") {
    auto l = std::vector<xy_point_curve_with_z>{
      {f0, z0},
      {f0, z1},
      {f1, z1}
    };

    auto c = xyz_point_curve{l};
    CHECK_EQ(c.curves.size(), 2);

    // check that the curve for z1 is the last curve for z1 provided
    CHECK_EQ(c.get_curve(z1), f1);
  }

  SUBCASE("find_interval") {
    auto c = xyz_point_curve{};
    c.set_curve(z0, f0);
    c.set_curve(z1, f1);

    auto z2 = z1 + 2.0;
    c.set_curve(z2, f1);

    // check for first interval
    auto b = c.find_interval(3.0);
    auto a = b - 1;

    CHECK_EQ(a->first, z0);
    CHECK_EQ(b->first, z1);

    // check for second interval
    b = c.find_interval(5.0);
    a = b - 1;
    CHECK_EQ(a->first, z1);
    CHECK_EQ(b->first, z2);

    // check for left of bounds
    b = c.find_interval(0.0);
    a = b - 1;
    CHECK_EQ(a->first, z0);
    CHECK_EQ(b->first, z1);

    // check for right of bounds
    b = c.find_interval(8.0);
    a = b - 1;
    CHECK_EQ(a->first, z1);
    CHECK_EQ(b->first, z2);

    // check for interval boundary
    b = c.find_interval(4.0); // 4.0 is not in [2.0, 4.0)
    a = b - 1;
    CHECK_EQ(a->first, z1);
    CHECK_EQ(b->first, z2);
  }

  SUBCASE("evaluate") {
    double x = 2.5;
    double z = 3.0;

    auto y0 = f0(x);
    auto y1 = f1(x);


    auto c = xyz_point_curve{};

    // checks with one curve
    c.set_curve(z0, f0);
    CHECK_EQ(c.evaluate(x, z), y0);


    // checks with two curves
    c.set_curve(z1, f1);
    CHECK_EQ(c.evaluate(x, z0), y0);
    CHECK_EQ(c.evaluate(x, z1), y1);
    CHECK_EQ(c.evaluate(x, z), (y0 + y1) / (z1 - z0));
  }
}

TEST_CASE("em/xy/turbine_description") {
  SUBCASE("forward") {
    turbine_description t{.operating_zones{{.efficiency_curves{
      {.xy_curve{.points{{.x = 1.0, .y = 0.9}}}, .z = 3.0}, {.xy_curve{.points{{.x = 2.0, .y = 0.8}}}, .z = 5.0}}}}};

    CHECK_EQ(x_min(t.operating_zones[0].efficiency_curves), doctest::Approx(1.0));
    CHECK_EQ(x_max(t.operating_zones[0].efficiency_curves), doctest::Approx(2.0));
    CHECK_EQ(y_min(t.operating_zones[0].efficiency_curves), doctest::Approx(0.8));
    CHECK_EQ(y_max(t.operating_zones[0].efficiency_curves), doctest::Approx(0.9));
    CHECK_EQ(z_min(t.operating_zones[0].efficiency_curves), doctest::Approx(3.0));
    CHECK_EQ(z_max(t.operating_zones[0].efficiency_curves), doctest::Approx(5.0));
    CHECK(has_forward_capability(capability(t)));
    CHECK(!has_backward_capability(capability(t)));
  }
  SUBCASE("backward") {
    turbine_description t{.operating_zones{{.efficiency_curves{

      {.xy_curve{.points{{.x = -1.0, .y = -1.2}}}, .z = -3.0}

    }}}};
    CHECK_EQ(x_min(t.operating_zones[0].efficiency_curves), doctest::Approx(-1.0));
    CHECK_EQ(x_max(t.operating_zones[0].efficiency_curves), doctest::Approx(-1.0));
    CHECK_EQ(y_min(t.operating_zones[0].efficiency_curves), doctest::Approx(-1.2));
    CHECK_EQ(y_max(t.operating_zones[0].efficiency_curves), doctest::Approx(-1.2));
    CHECK_EQ(z_min(t.operating_zones[0].efficiency_curves), doctest::Approx(-3.0));
    CHECK_EQ(z_max(t.operating_zones[0].efficiency_curves), doctest::Approx(-3.0));
    CHECK(!has_forward_capability(capability(t)));
    CHECK(has_backward_capability(capability(t)));
  }
  SUBCASE("reversible") {
    turbine_description t{.operating_zones{{.efficiency_curves{
      {.xy_curve{.points{{.x = -1.0, .y = -1.2}}}, .z = -3.0}, {.xy_curve{.points{{.x = 1.0, .y = 0.9}}}, .z = 3.0}

    }}}};
    CHECK_EQ(x_min(t.operating_zones[0].efficiency_curves), doctest::Approx(-1.0));
    CHECK_EQ(x_max(t.operating_zones[0].efficiency_curves), doctest::Approx(1.0));
    CHECK_EQ(y_min(t.operating_zones[0].efficiency_curves), doctest::Approx(-1.2));
    CHECK_EQ(y_max(t.operating_zones[0].efficiency_curves), doctest::Approx(0.9));
    CHECK_EQ(z_min(t.operating_zones[0].efficiency_curves), doctest::Approx(-3.0));
    CHECK_EQ(z_max(t.operating_zones[0].efficiency_curves), doctest::Approx(3.0));
    CHECK(has_forward_capability(capability(t)));
    CHECK(has_backward_capability(capability(t)));
    CHECK(has_reversible_capability(capability(t)));

    CHECK_EQ(std::ranges::distance(backward_efficiency_curves(t.operating_zones[0])), 1);
    CHECK_EQ(std::ranges::distance(forward_efficiency_curves(t.operating_zones[0])), 1);
  }
}

TEST_SUITE_END();
