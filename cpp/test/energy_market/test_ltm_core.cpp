
#include <doctest/doctest.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/waterway.h>

#include <serialize_loop.h>
using test::serialize_loop;

using std::isnan;

TEST_SUITE_BEGIN("em");

TEST_CASE("em/id_base") {
  using std::string;
  shyft::energy_market::id_base a;
  CHECK_EQ(a.id, 0);
  CHECK_EQ(a.name, string(""));
  CHECK_EQ(a.json, string(""));
  shyft::energy_market::id_base b;
  CHECK_EQ(true, b == a);
  CHECK_EQ(false, b != a);
  CHECK_EQ(false, b < a);
  b.id = 1;
  CHECK_EQ(true, b != a);
  CHECK_EQ(false, b == a);
  CHECK_EQ(false, b < a);
  a.name = "a";
  a.id = 0;
  CHECK_EQ(false, b == a);
  CHECK_EQ(true, b != a);
  CHECK_EQ(true, b < a);
  a.name = "";
  a.json = "python_load { uid:'xtra',stuff:'fluffy'}";
  CHECK_EQ(false, b == a);
  CHECK_EQ(true, b != a);
  CHECK_EQ(false, b < a);
  a.id = 1;
  a.name = "a";
  a.tsm.emplace("dummy", shyft::time_series::dd::apoint_ts{});
  a.custom.emplace("dummy", shyft::time_series::dd::apoint_ts{});
  a.custom.emplace("dummy1", true);
  a.custom.emplace("dummy2", 0.11);
  SUBCASE("serialize") {
    auto a1 = serialize_loop(a);
    fmt::print("Two vectors came in one size {} and other {} \n", a1.custom.size(), a.custom.size());
    CHECK(a1 == a);
  }
}

TEST_CASE("em/model_only") {
  using namespace shyft::energy_market::market;
  model m1;
  CHECK(m1.id == 0);
  CHECK(m1.name.size() == 0);
  CHECK(m1.area.size() == 0);
  CHECK(m1.power_lines.size() == 0);
  SUBCASE("model equality") {
    model m2;
    CHECK(m1 == m2);
    m1.id = 2;
    CHECK(m1 != m2);
    m1.id = 0;
    m1.name = "a";
    CHECK(m1 != m2);
    m1.id = 1;
    CHECK(m1 != m2);
    CHECK(m1.equal_structure(m2) == true);
    CHECK(m1.equal_content(m2) == true);
  }
  SUBCASE("serialize") {
    auto m1x = serialize_loop(m1);
    CHECK(m1x == m1);
    auto m1_as_xml = m1.to_blob();
    auto m1_from_xml = model::from_blob(m1_as_xml);
    CHECK(*m1_from_xml == m1);
  }
}

TEST_CASE("em/model_area") {
  using namespace shyft::energy_market::market;
  model_area a1;
  CHECK(a1.id == 0);
  CHECK(a1.name.size() == 0);
  CHECK(a1.power_modules.size() == 0);
  SUBCASE("model_area equality") {
    model_area a2(nullptr, 0, "", "");
    CHECK(a1 == a2);
    a1.id = 2;
    CHECK(a1 != a2);
    a1.id = 0;
    a1.name = 'a';
    CHECK(a1 != a2);
  }
  SUBCASE("serialize") {
    auto a1x = serialize_loop(a1);
    CHECK(a1x == a1);
  }
}

TEST_CASE("em/power_line") {
  using namespace shyft::energy_market::market;
  power_line l1;
  CHECK(l1.id == 0);
  CHECK(l1.name == "");
  CHECK(l1.get_area_1() == nullptr);
  CHECK(l1.get_area_2() == nullptr);
  SUBCASE("equality") {
    auto a1 = make_shared<model_area>(nullptr, 1, "a1", "json1");
    auto a2 = make_shared<model_area>(nullptr, 2, "a2", "json2");
    power_line l2;
    CHECK(l1 == l2);
    l1.id = 2;
    CHECK(l1 != l2);
    l1.id = 0;
    l1.area_1 = a1;
    l2.area_1 = a1;
    CHECK(l1 == l2);
    l1.area_2 = a2;
    l2.area_2 = a2;
    CHECK(l1 == l2);
    CHECK(l1.equal_structure(l2));
    l1.area_2.reset();
    CHECK(l1 != l2);
    CHECK(!l1.equal_structure(l2));
  }
  SUBCASE("serialize") {
    auto l1x = serialize_loop(l1);
    CHECK(l1x == l1);
  }
}

TEST_CASE("em/power-module") {
  using namespace shyft::energy_market::market;
  power_module pm1;
  CHECK(pm1.id == 0);
  CHECK(pm1.name.size() == 0);
  SUBCASE("equality") {
    power_module pm2(nullptr, 0, "");
    CHECK(pm1 == pm2);
    CHECK(pm1.equal_structure(pm2) == true);
    pm2.id = 1;
    CHECK(pm1 != pm2);
    CHECK(!pm1.equal_structure(pm2) == true);
    pm1.id = 1;
    CHECK(pm1.equal_structure(pm2) == true);
    pm1.name = "different";
    CHECK(pm1.equal_structure(pm2) == true);
    CHECK(pm1 != pm2);
  }
  SUBCASE("serialize") {
    pm1.name = "a";
    pm1.id = 1;
    pm1.json = "metadata";
    auto pm1x = serialize_loop(pm1);
    CHECK(pm1x == pm1);
  }
}

TEST_CASE("em/simple-model_build") {
  using namespace shyft::energy_market::market;
  model_builder m(make_shared<model>(1, "m", ""));
  auto a1 = m.create_model_area(1, "a1", "");
  auto a2 = m.create_model_area(2, "a2", "");
  auto a3 = m.create_model_area(3, "a3", "");

  auto l12 = m.create_power_line(1, "1_a1-a2", "{valid:true}", a1, a2);
  auto l23 = m.create_power_line(2, "2_a2-a3", "", a2, a3);
  auto pm1 = m.create_power_module(1, "solar", "", a1);
  auto m_xml = m.m->to_blob();
  auto mx = model::from_blob(m_xml);
  CHECK(*mx == *m.m);
}

TEST_SUITE_END();
