#include <doctest/doctest.h>
#include <memory>
#include <string>
#include <exception>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_operations.h>

namespace test_waterway {
  TEST_SUITE_BEGIN("em");

  using namespace shyft::energy_market::hydro_power;
  using namespace std;

  TEST_CASE("em_core/gate_default_values_expected") {
    // Test that gate is constructed with expected default values
    auto gt = make_shared<gate>();

    CHECK_EQ(gt->id, 0);
    CHECK_EQ(gt->name, "");
  }

  TEST_CASE("em_core/gate_shared_from_this") {
    auto hps = make_shared<hydro_power_system>(0, "test_system");
    auto ww = make_shared<waterway>(0, "test_waterway", "", hps);

    SUBCASE("gate not in system") {
      CHECK_EQ(ww->shared_from_this(), nullptr);
    }
    ww->hps = hps;
    hps->waterways.push_back(ww);

    SUBCASE("gate in system") {
      CHECK_EQ(ww->shared_from_this(), ww);
    }
  }

  TEST_CASE("emcore/add_gate") {
    // Test that references are correct after adding gate  to a waterway.

    auto hps = make_shared<hydro_power_system>(0, "test_system");
    auto ww = make_shared<waterway>(0, "test_waterway", "", hps);
    auto gt = make_shared<gate>(0, "test_gate", "");
    vector<std::shared_ptr<gate>> gates;

    hps->waterways.push_back(ww);
    gates.push_back(gt);

    waterway::add_gate(ww, gt);

    // check that waterway reference for gate is set
    CHECK_EQ(ww, gt->wtr_());

    // check that gate list contains only gate
    CHECK_EQ(ww->gates.size(), 1);
    CHECK_EQ(ww->gates[0], gt);
  }

  TEST_CASE("em_core/add_std::shared_ptr<gate>twice_throws") {
    // Test that adding the same gate to a waterway twice throws
    auto hps = make_shared<hydro_power_system>(0, "test_system");
    auto ww = make_shared<waterway>(0, "test_waterway", "", hps);
    auto gt = make_shared<gate>(0, "test_gate", "");
    vector<std::shared_ptr<gate>> gates;

    hps->waterways.push_back(ww);
    gates.push_back(gt);

    waterway::add_gate(ww, gt);
    CHECK_THROWS_AS(waterway::add_gate(ww, gt), runtime_error);

    // check that second add changed nothing
    CHECK_EQ(ww, gt->wtr_());
    CHECK_EQ(ww->gates.size(), 1);
    CHECK_EQ(ww->gates[0], gt);
  }

  TEST_CASE("em_core/add_std::shared_ptr<gate>already_added_throws") {
    // Test that adding a gate to a waterway throws when the gate
    // was already added to another waterway
    auto hps = make_shared<hydro_power_system>(0, "test_system");
    auto ww = make_shared<waterway>(0, "test_waterway", "", hps);
    auto other_ww = make_shared<waterway>(1, "other_waterway", "", hps);
    auto gt = make_shared<gate>(0, "test_gate", "");
    vector<std::shared_ptr<gate>> gates;

    hps->waterways.push_back(ww);
    hps->waterways.push_back(other_ww);
    gates.push_back(gt);

    waterway::add_gate(other_ww, gt);
    CHECK_THROWS_AS(waterway::add_gate(ww, gt), runtime_error);

    // check that second add changed nothing
    CHECK_EQ(ww->gates.size(), 0);
    CHECK_EQ(other_ww, gt->wtr_());
    CHECK_EQ(other_ww->gates.size(), 1);
    CHECK_EQ(other_ww->gates[0], gt);
  }

  TEST_SUITE_END();
} // end namespace
