#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/core/core_archive.h>
#include <sstream>
#include <doctest/doctest.h>

TEST_SUITE_BEGIN("em/ui");
using namespace shyft::energy_market::ui;

TEST_CASE("em/ui/layout_info") {
  // 1. Create layout_info object:
  layout_info li; // Using default:
  CHECK_EQ(li.id, 0);
  CHECK_EQ(li.name, "");
  CHECK_EQ(li.json, "");

  // 2. Create from data
  layout_info li2(1, "test", "{misc.}");
  CHECK_EQ(li2.id, 1);
  CHECK_EQ(li2.name, "test");
  CHECK_EQ(li2.json, "{misc.}");

  // 3. move- semantics
  layout_info li3(std::move(li2));
  CHECK_EQ(li3.id, 1);
  CHECK_EQ(li3.name, "test");
  CHECK_EQ(li3.json, "{misc.}");
  CHECK_EQ(li2.id, 1); // id is an int so gets copied in move.
  CHECK_EQ(li2.name, "");
  CHECK_EQ(li2.json, "");

  li3 = std::move(li);
  CHECK_EQ(li3.id, 0);
  CHECK_EQ(li3.name, "");
  CHECK_EQ(li3.json, "");
}

TEST_CASE("em/ui/layout_info_serialization") {
  using shyft::core::core_iarchive;
  using shyft::core::core_oarchive;
  layout_info li(2, "a descriptive name", R"_({"a":[1,2,3],"b":{"c":3}})_");

  // Serialize and deserialize
  std::stringstream ss;
  layout_info li2;
  {
    core_oarchive oa(ss);
    oa << li;
  }
  {
    core_iarchive ia(ss);
    ia >> li2;
  }
  CHECK_EQ(li, li2);
}

TEST_SUITE_END();
