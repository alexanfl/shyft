#include <test/energy_market/ui/ui_test_server.h>

#include <csignal>
#include <future>

#include <shyft/energy_market/ui/server.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/web_api/ui/request_handler.h>

namespace test::ui {

  using namespace shyft::energy_market::ui;

  struct server_impl : config_server {
    shyft::web_api::ui::request_handler bg_server;
    std::future<int> web_srv;

    explicit server_impl(string const & root_dir)
      : config_server(root_dir) {
      bg_server.srv = this;
    }

    void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads) {
      if (!web_srv.valid()) {
        web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads]() -> int {
          return shyft::web_api::run_web_server(
            bg_server,
            host_ip,
            static_cast<unsigned short>(port),
            std::make_shared<string>(doc_root),
            fg_threads,
            bg_threads);
        });
      } else {
        throw std::runtime_error("ui web server already running");
      }
    }

    bool web_api_running() const {
      return web_srv.valid();
    }

    void stop_web_api() {
      if (web_srv.valid()) {
        std::raise(SIGINT);
        (void) web_srv.get();
      }
    }
  };

  server::server(string const & root_dir) {
    impl = std::make_unique<server_impl>(root_dir);
  }

  void server::start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads) {
    impl->start_web_api(host_ip, port, doc_root, fg_threads, bg_threads);
  }

  bool server::web_api_running() const {
    return impl->web_api_running();
  }

  void server::stop_web_api() {
    impl->stop_web_api();
  }

  void server::set_listening_ip(string host_ip) {
    impl->set_listening_ip(host_ip);
  }

  void server::set_read_cb(fx_generate_json_api const & fx) {
    impl->set_read_cb(fx);
  }

  int server::start_server() {
    return impl->start_server();
  }

  void server::db_store_model(std::shared_ptr<layout_info> const & m, model_info const & mi) {
    impl->db.store_model(m, mi);
  }

  std::shared_ptr<layout_info> server::db_read_model(int64_t id) {
    return impl->db.read_model(id);
  }

  server::~server() {
  }

  // test  client part


}
