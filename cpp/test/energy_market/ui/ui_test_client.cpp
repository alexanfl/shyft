#include <test/energy_market/ui/ui_test_server.h>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <boost/beast/websocket.hpp>
#include <boost/beast/core/multi_buffer.hpp>
#include <boost/beast/core/buffers_to_string.hpp>

// #include <iostream>

namespace test::ui {

  using tcp = boost::asio::ip::tcp;
  namespace websocket = boost::beast::websocket;
  using boost::system::error_code;

  namespace impl {
    struct session : public std::enable_shared_from_this<session> {
      tcp::resolver resolver_;
      websocket::stream<tcp::socket> ws_;
      boost::beast::multi_buffer buffer_;
      string host_;
      string port_;
      string text_;
      string response_;
      string fail_;
      std::function<string(string const &)> report_response;

      // Report a failure
      void fail(error_code ec, char const * what) {
        fail_ = string(what) + ": " + ec.message();
        // std::cout<<"fail('"<<fail_<<"')"<<std::endl;
      }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));
     public:
      // Resolver and socket require an io_context
      explicit session(boost::asio::io_context& ioc)
        : resolver_(ioc)
        , ws_(ioc) {
      }

      string response() const {
        return response_;
      }

      string diagnostics() const {
        return fail_;
      }

      // Start the asynchronous operation
      template <class Fx>
      void run(string_view host, int port, string_view text, Fx&& rep_response) {
        // Save these for later
        host_ = host;
        text_ = text;
        port_ = std::to_string(port);
        // std::cout<<"run("<<host<<","<<port<<",'"<<text<<"')"<<std::endl;
        report_response = rep_response;
        resolver_.async_resolve(
          host_,
          port_, // Look up the domain name
          [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
            me->on_resolve(ec, results);
          });
      }

      void on_resolve(error_code ec, tcp::resolver::results_type results) {
        fail_on_error(ec, "resolve")
          // Make the connection on the IP address we get from a lookup
          boost::asio::async_connect(
            ws_.next_layer(),
            results.begin(),
            results.end(),
            std::bind(&session::on_connect, shared_from_this(), std::placeholders::_1));
      }

      void on_connect(error_code ec) {
        fail_on_error(ec, "connect") ws_.async_handshake(
          host_,
          "/", // Perform the websocket handshake
          [me = shared_from_this()](error_code ec) {
            me->on_handshake(ec);
          });
      }

      void on_handshake(error_code ec) {
        fail_on_error(ec, "handshake") if (text_.size()) {
          ws_.async_write(
            boost::asio::buffer(text_), [me = shared_from_this()](error_code ec, size_t bytest_transferred) {
              me->on_write(ec, bytest_transferred);
            });
        }
        else {
          ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
            me->on_close(ec);
          });
        }
      }

      void on_write(error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        fail_on_error(ec, "write")

          ws_.async_read(
            buffer_, // Read a message into our buffer
            [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
              me->on_read(ec, bytes_transferred);
            });
      }

      void on_read(error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        fail_on_error(ec, "read") response_ = boost::beast::buffers_to_string(buffer_.data());
        buffer_.consume(buffer_.size());
        text_ = report_response(response_);
        if (text_.size()) {
          ws_.async_write(
            boost::asio::buffer(text_), [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
              me->on_write(ec, bytes_transferred);
            });
        } else {
          ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
            me->on_close(ec);
          });
        }
      }

      void on_close(error_code ec) {
        fail_on_error(ec, "close")
      }

#undef fail_on_error
    };
  }

  session::session(boost::asio::io_context& ioc) {
    impl = std::make_shared<impl::session>(ioc);
  }

  string session::response() const {
    return impl->response();
  }

  string session::diagnostics() const {
    return impl->diagnostics();
  }

  void session::run(string_view host, int port, string_view text, fx_request_gen const & rep_response) {
    impl->run(host, port, text, rep_response);
  }

  session::~session() {
  }

  unsigned short get_free_port() {
    using namespace boost::asio;
    io_service service;
    ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0)); // pass in 0 to get a free port.
    return acceptor.local_endpoint().port();
  }

}
