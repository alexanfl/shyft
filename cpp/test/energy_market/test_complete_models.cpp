#include <build_test_model.h>
#include <doctest/doctest.h>
#include <serialize_loop.h>
#include <shyft/srv/db.h>
#include <shyft/energy_market/em_utils.h>
#include <fstream>
using test::serialize_loop;

TEST_SUITE_BEGIN("em");

TEST_CASE("em/model_with_no_hps") {
  using namespace shyft::energy_market::market;
  auto m1 = test::build_model();
  auto m2 = test::build_model();
  CHECK(m1->equal_structure(*m2) == true);
  CHECK(m1->equal_content(*m2) == true);
  CHECK(*m1 == *m2);
  m2->power_lines[0]->json = "a minor change";
  CHECK(m1->equal_structure(*m2) == true);
  CHECK(m1->equal_content(*m2) == false);
  CHECK(*m1 != *m2);
  model_builder(m2).create_model_area(4, "extra", "j");
  CHECK(m1->equal_structure(*m2) == false);
  CHECK(m1->area[1]->equal_structure(*m2->area[1])); // these two are still equally structured
  auto m1_xml = m1->to_blob();
  auto m1_s = model::from_blob(m1_xml);
  CHECK(m1->equal_structure(*m1_s));
}

TEST_CASE("em/hydro_power_system/eq-loop") {
  using namespace shyft::energy_market::hydro_power;
  auto hps1 = test::build_hps("sørland");
  auto hps2 = test::build_hps("sørland");
  CHECK(hps1->equal_structure(*hps2) == true);
  CHECK(*hps1 == *hps2);
  auto r1 = hps2->find_reservoir_by_name("r1");
  auto o = hps2->find_reservoir_by_name("ocean");
  CHECK(r1 != nullptr);
  hydro_power_system_builder hpsb(hps2);
  connect(hpsb.create_river(100, "extra bypass", R"(water_route_data(10000, "uid:1234", "r1 to ocean"))"))
    .input_from(r1, connection_role::flood)
    .output_to(o);

  CHECK(hps1->equal_structure(*hps2) == false);
  CHECK(hps2->equal_structure(*hps1) == false);
  auto hps2_s = serialize_loop(hps2);
  CHECK(hps2->equal_structure(*hps2_s));
  CHECK(*hps2 == *hps2_s);
}

TEST_CASE("em/hps_reservoir_json_diff") {
  using namespace shyft::energy_market::hydro_power;
  auto hps1 = test::build_hps("sørland");
  auto hps2 = test::build_hps("sørland");
  CHECK(hps1->equal_structure(*hps2) == true);
  CHECK(*hps1 == *hps2);
  auto r1 = hps2->find_reservoir_by_name("r1");
  r1->json = "this is different";
  CHECK(hps1->equal_content(*hps2) == false);
  CHECK(hps1->equal_structure(*hps2) == true);

  std::vector<std::shared_ptr<unit>> a;
  a.push_back(std::make_shared<unit>(1, "u1", ""));
  a.push_back(std::make_shared<unit>(2, "u2", ""));
  std::vector<std::shared_ptr<unit>> b;
  b.push_back(std::make_shared<unit>(2, "u2", ""));
  b.push_back(std::make_shared<unit>(1, "u1", "xx"));
  CHECK(std::is_permutation(a.begin(), a.end(), b.begin(), b.end(), [](auto const &ca, auto const &cb) -> bool {
    return ca->equal_structure(*cb);
  }));
}

TEST_CASE("em/full model with hps roundtrip") {
  // using namespace shyft::energy_market::core;
  auto m1 = test::build_model();
  m1->area[1]->detailed_hydro = test::build_hps("a1.hydro_power_system");

  auto m1_s = serialize_loop(m1);

  CHECK(m1_s->equal_structure(*m1) == true);
}

TEST_SUITE_END();
