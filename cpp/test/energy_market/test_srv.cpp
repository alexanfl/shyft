#include <doctest/doctest.h>
#include <shyft/srv/db.h>
#include <shyft/srv/db_level.h>
#include <shyft/srv/db_file.h>
#include <shyft/srv/db_rocks.h>

#include <shyft/srv/server.h>
#include <shyft/srv/client.h>
#include <build_test_model.h>
#include <test/test_utils.h>

using std::to_string;
using std::string;
using std::vector;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::utcperiod;
using shyft::core::to_seconds64;
using shyft::core::from_seconds;

TEST_SUITE_BEGIN("em");

//-- this is what we use and test:
using shyft::srv::model_info;
using shyft::srv::db;
using shyft::srv::db_level;
using shyft::srv::db_rocks;
using shyft::srv::db_file;
using shyft::srv::server;
using shyft::srv::client;
using shyft::energy_market::market::model;

//-- keep test directory, unique, and with auto-cleanup.
auto dirname = "srv.db.test." + to_string(to_seconds64(utctime_now()));
test::utils::temp_dir tmpdir(dirname.c_str());

//-- now ready for testing:
TEST_CASE("em/server/serverdb") {
  //
  // 0. test we can create a db
  db<model, db_file> dbm{tmpdir.string()};
  vector<int64_t> mids;
  CHECK_EQ(0u, dbm.get_model_infos(mids).size());
  //
  // 1. test we can store one model into db
  auto m = test::build_model();
  m->id = 0; // create new model, require 0 here
  model_info mi{0, "name", from_seconds(7200), "{a:'key',b:1.23}"};
  auto mid = dbm.store_model(m, mi);
  CHECK_GE(1u, mid);
  // update mi&m to new id: ref todo in db.h, we might change the db-layer to mutable mi and m, to avoid this
  mi.id = mid;
  m->id = mid;
  // verify model-info is there
  auto mis = dbm.get_model_infos(mids);
  CHECK_EQ(1u, mis.size());
  CHECK_EQ(mi, mis[0]);
  vector<int64_t> midv{mid};

  // Get_model_infos with created_in period:
  auto mis2 = dbm.get_model_infos(mids, utcperiod(0, 10000));
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);

  mis2 = dbm.get_model_infos(mids, utcperiod(10000, 20000));
  CHECK_EQ(0u, mis2.size());

  mis = dbm.get_model_infos(midv);
  CHECK_EQ(1u, mis.size());
  CHECK_EQ(mi, mis[0]);
  CHECK_EQ(1u, dbm.find_max_model_id()); // verify we correctly can find max model-id
  // and that we can update it:
  mi.json = string("{Updated info}");
  dbm.update_model_info(mid, mi);

  mis = dbm.get_model_infos(midv);
  CHECK_EQ(mi, mis[0]); // verify we now are aligned with the new stored info

  // and that we can get back the model
  auto mr = dbm.read_model(mid);
  CHECK_UNARY(mr != nullptr);
  CHECK_UNARY(*mr == *m);
  //
  // store another model, and check it can handle more than one:
  auto m2 = test::build_model();
  m2->id = 0;
  model_info mi2 = {0, "name2", utctime_now(), "{b:'vv',b:3.21}"};
  auto mid2 = dbm.store_model(m2, mi2);
  // verify model-infos and models are available
  mi2.id = mid2;
  m2->id = mid2;
  CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id

  mis = dbm.get_model_infos(mids);
  CHECK_EQ(2u, mis.size());
  auto ix2 = mis[0].id == mid2 ? 0 : 1;
  CHECK_EQ(mi2, mis[ix2]);

  auto mr2 = dbm.read_model(mid2);
  CHECK_NE(*mr2, *mr);
  CHECK_EQ(*mr2, *m2);

  //
  // remove model
  dbm.remove_model(mid);

  // verify we got just one model left
  mis = dbm.get_model_infos(mids);
  REQUIRE_EQ(1u, mis.size());
  CHECK_EQ(mi2, mis[0]);                 // and that we got the right model left
  CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 2 since we removed 1'

  // remove last model..
  dbm.remove_model(mid2);
  mis = dbm.get_model_infos(mids);
  REQUIRE_EQ(0u, mis.size());
  CHECK_EQ(0u, dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 2 since we removed 1'
  // simulate we drop in a model file , and verify it discovers it
  db<model, db_file> dbm2{tmpdir.string()};
  auto midx = dbm2.store_model(m2, mi2); //  not known to dbm.
  mids.clear();
  mis = dbm.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1);
  //-- fix for update/store max uid
  // verify that if we store a model with a high model-id, then max model id is maintained.
  mi2.id = 100;
  m2->id = 100;
  auto m2_mid = dbm.store_model(m2, mi2);
  CHECK_EQ(m2_mid, 100);
  CHECK_EQ(dbm.mk_unique_model_id(), 101);
  dbm.remove_model(m2_mid); // clean up
  dbm.remove_model(midx);
}

TEST_CASE("em/server/io") {
  using db_t = db<model, db_file>;
  using srv_t = server<db_t>;
  using client_t = client<model>;
  //
  // 0. arrange with a server and a client running on localhost, tempdir.
  //
  srv_t s0(tmpdir.string());
  string host_ip = "127.0.0.1";
  s0.set_listening_ip(host_ip);
  auto s_port = s0.start_server();
  CHECK_GE(s_port, 1024);
  client_t c{host_ip + ":" + to_string(s_port)};
  vector<int64_t> mids;
  auto mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 0);
  //
  // 1.  arrange with one model
  //
  auto m = test::build_model();
  model_info mi{0, "name2", from_seconds(600), "{b:'vv',b:3.21}"};
  m->id = 0;

  auto mid = c.store_model(m, mi);
  CHECK_EQ(mid, 1u);
  //
  // 2. with one model, verify minfo and m is ok
  //
  m->id = mid;
  mi.id = mid;
  auto mr = c.read_model(mid);
  CHECK_EQ(*mr, *m);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1u);
  CHECK_EQ(mi, mis[0]);

  auto mis2 = c.get_model_infos(mids, utcperiod(0, 1000));
  CHECK_EQ(mis2.size(), 1u);
  CHECK_EQ(mis2[0], mi);
  mis2 = c.get_model_infos(mids, utcperiod(1000, 2000));
  CHECK_EQ(mis2.size(), 0u);
  c.close(); // just to force auto-open on next call
  //
  // 3. check we can update minfo of existing model
  //
  mi.json = string("{something:else}");
  c.update_model_info(mi.id, mi);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1u);
  CHECK_EQ(mi, mis[0]);

  //
  // 4. finally, remove the model, and verify we are back to zero
  //
  c.remove_model(mid);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 0);
  c.close(); // should work here
  s0.clear();
}

TEST_CASE("em/server/leveldb") {
  //
  // 0. test we can create a db
  db<model, db_level> dbm{tmpdir.string() + "level_db"};
  vector<int64_t> mids;

  CHECK_EQ(0u, dbm.get_model_infos(mids).size());

  // 1. test we can store one model into db
  auto m = test::build_model();
  m->id = 0; // create new model, require 0 here
  model_info mi{0, "name", from_seconds(7200), "{a:'key',b:1.23}"};
  auto mid = dbm.store_model(m, mi);
  CHECK_GE(1u, mid);
  // update mi&m to new id: ref todo in db.h, we might change the db-layer to mutable mi and m, to avoid this
  mi.id = mid;
  m->id = mid;
  // verify model-info is there
  auto mis = dbm.get_model_infos(mids);
  CHECK_EQ(1u, mis.size());
  CHECK_EQ(mi, mis[0]);
  vector<int64_t> midv{mid};

  // Get_model_infos with created_in period:
  auto mis2 = dbm.get_model_infos(mids, utcperiod(0, 10000));
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);
  mis2 = dbm.get_model_infos(midv, utcperiod(0, 10000)); // also test with several in vector.
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);
  dbm.io.flush_cache();                                  // evict items, force reread.
  mis2 = dbm.get_model_infos(midv, utcperiod(0, 10000)); // also test with several in empty cache case.
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);

  mis2 = dbm.get_model_infos(mids, utcperiod(10000, 20000));
  CHECK_EQ(0u, mis2.size());

  mis = dbm.get_model_infos(midv);
  CHECK_EQ(1u, mis.size());
  CHECK_EQ(mi, mis[0]);
  CHECK_EQ(1u, dbm.find_max_model_id()); // verify we correctly can find max model-id
  // and that we can update it:
  mi.json = string("{Updated info}");
  dbm.update_model_info(mid, mi);

  mis = dbm.get_model_infos(midv);
  CHECK_EQ(mi, mis[0]); // verify we now are aligned with the new stored info

  // and that we can get back the model
  auto mr = dbm.read_model(mid);
  CHECK_UNARY(mr != nullptr);
  CHECK_UNARY(*mr == *m);
  //
  // store another model, and check it can handle more than one:
  auto m2 = test::build_model();
  m2->id = 0;
  model_info mi2 = {0, "name2", utctime_now(), "{b:'vv',b:3.21}"};
  auto mid2 = dbm.store_model(m2, mi2);
  // verify model-infos and models are available
  mi2.id = mid2;
  m2->id = mid2;
  CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id

  mis = dbm.get_model_infos(mids);
  CHECK_EQ(2u, mis.size());
  auto ix2 = mis[0].id == mid2 ? 0 : 1;
  CHECK_EQ(mi2, mis[ix2]);

  auto mr2 = dbm.read_model(mid2);
  CHECK_NE(*mr2, *mr);
  CHECK_EQ(*mr2, *m2);

  // remove model
  dbm.remove_model(mid);

  // verify we got just one model left
  mis = dbm.get_model_infos(mids);
  REQUIRE_EQ(1u, mis.size());
  CHECK_EQ(mi2, mis[0]);                 // and that we got the right model left
  CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 2 since we removed 1'

  // remove last model..
  dbm.remove_model(mid2);
  mis = dbm.get_model_infos(mids);
  REQUIRE_EQ(0u, mis.size());
  CHECK_EQ(
    0u,
    dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 0 since we removed all models
}

TEST_CASE("em/server/io_leveldb") {
  using db_t = db<model, db_level>;
  using srv_t = server<db_t>;
  using client_t = client<model>;
  //
  // 0. arrange with a server and a client running on localhost, tempdir.
  //
  srv_t s0(tmpdir.string() + "level_db2");
  string host_ip = "127.0.0.1";
  s0.set_listening_ip(host_ip);
  auto s_port = s0.start_server();
  CHECK_GE(s_port, 1024);
  client_t c{host_ip + ":" + to_string(s_port)};
  vector<int64_t> mids;
  auto mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 0);
  //
  // 1.  arrange with one model
  //
  auto m = test::build_model();
  model_info mi{0, "name2", from_seconds(600), "{b:'vv',b:3.21}"};
  m->id = 0;

  auto mid = c.store_model(m, mi);
  CHECK_EQ(mid, 1u);
  //
  // 2. with one model, verify minfo and m is ok
  //
  m->id = mid;
  mi.id = mid;
  auto mr = c.read_model(mid);
  CHECK_EQ(*mr, *m);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1u);
  CHECK_EQ(mi, mis[0]);

  auto mis2 = c.get_model_infos(mids, utcperiod(0, 1000));
  CHECK_EQ(mis2.size(), 1u);
  CHECK_EQ(mis2[0], mi);
  mis2 = c.get_model_infos(mids, utcperiod(1000, 2000));
  CHECK_EQ(mis2.size(), 0u);
  c.close(); // just to force auto-open on next call
  //
  // 3. check we can update minfo of existing model
  //
  mi.json = string("{something:else}");
  c.update_model_info(mi.id, mi);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1u);
  CHECK_EQ(mi, mis[0]);

  //
  // 4. finally, remove the model, and verify we are back to zero
  //
  c.remove_model(mid);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 0);
  c.close(); // should work here
  s0.clear();
}

TEST_CASE("em/server/level_db_migrate") {
  // 1 fill up a db_file db with something,
  //   pretend old db files are in place
  //FIXME: Remove file db, and migration, since we by now should have only level_db ->rocks_db migrated systems
  test::utils::temp_dir tmp_db("level_db_migrate");
  auto root = tmp_db.string();
  shyft::srv::db_file f(root);
  constexpr size_t n = 10;
  for (size_t i = 0; i < n; ++i) {
    string m_blob = std::to_string(i + 1); // minimal
    model_info mi{int64_t(i + 1), "name of " + m_blob, from_seconds(600), "{b:'vv',b:3.21}"};
    f.store_model_blob(m_blob, mi);
  }
  CHECK_EQ(f.find_max_model_id(), n);
  // now predend we are migrating it
  MESSAGE("Migrating it");
  {
    auto t0 = utctime_now();
    shyft::srv::db_level l(root); // construct with root
    auto t1 = utctime_now();
    MESSAGE("done in " << shyft::core::to_seconds(t1 - t0) << "[s], level db now " << l.find_max_model_id());
    // compare!
    auto all_mi = f.get_model_infos({});
    for (auto const & mi : all_mi) {
      auto a = f.read_model_blob(mi.id);
      auto b = l.read_model_blob(mi.id);
      CHECK_EQ(a, b);
      auto ia = f.get_model_infos({mi.id});
      auto ib = l.get_model_infos({mi.id});
      CHECK_EQ(ia, ib);
    }
  }
}

TEST_CASE("em/server/rocksdb") {
  //
  // 0. test we can create a db
  db<model, db_level> dbm{tmpdir.string() + "rocks_db"};
  vector<int64_t> mids;

  CHECK_EQ(0u, dbm.get_model_infos(mids).size());

  // 1. test we can store one model into db
  auto m = test::build_model();
  m->id = 0; // create new model, require 0 here
  model_info mi{0, "name", from_seconds(7200), "{a:'key',b:1.23}"};
  auto mid = dbm.store_model(m, mi);
  CHECK_GE(1u, mid);
  // update mi&m to new id: ref todo in db.h, we might change the db-layer to mutable mi and m, to avoid this
  mi.id = mid;
  m->id = mid;
  // verify model-info is there
  auto mis = dbm.get_model_infos(mids);
  CHECK_EQ(1u, mis.size());
  CHECK_EQ(mi, mis[0]);
  vector<int64_t> midv{mid};

  // Get_model_infos with created_in period:
  auto mis2 = dbm.get_model_infos(mids, utcperiod(0, 10000));
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);
  mis2 = dbm.get_model_infos(midv, utcperiod(0, 10000)); // also test with several in vector.
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);
  dbm.io.flush_cache();                                  // evict items, force reread.
  mis2 = dbm.get_model_infos(midv, utcperiod(0, 10000)); // also test with several in empty cache case.
  CHECK_EQ(1u, mis2.size());
  CHECK_EQ(mi, mis2[0]);

  mis2 = dbm.get_model_infos(mids, utcperiod(10000, 20000));
  CHECK_EQ(0u, mis2.size());

  mis = dbm.get_model_infos(midv);
  CHECK_EQ(1u, mis.size());
  CHECK_EQ(mi, mis[0]);
  CHECK_EQ(1u, dbm.find_max_model_id()); // verify we correctly can find max model-id
  // and that we can update it:
  mi.json = string("{Updated info}");
  dbm.update_model_info(mid, mi);

  mis = dbm.get_model_infos(midv);
  CHECK_EQ(mi, mis[0]); // verify we now are aligned with the new stored info

  // and that we can get back the model
  auto mr = dbm.read_model(mid);
  CHECK_UNARY(mr != nullptr);
  CHECK_UNARY(*mr == *m);
  //
  // store another model, and check it can handle more than one:
  auto m2 = test::build_model();
  m2->id = 0;
  model_info mi2 = {0, "name2", utctime_now(), "{b:'vv',b:3.21}"};
  auto mid2 = dbm.store_model(m2, mi2);
  // verify model-infos and models are available
  mi2.id = mid2;
  m2->id = mid2;
  CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id

  mis = dbm.get_model_infos(mids);
  CHECK_EQ(2u, mis.size());
  auto ix2 = mis[0].id == mid2 ? 0 : 1;
  CHECK_EQ(mi2, mis[ix2]);

  auto mr2 = dbm.read_model(mid2);
  CHECK_NE(*mr2, *mr);
  CHECK_EQ(*mr2, *m2);

  // remove model
  dbm.remove_model(mid);

  // verify we got just one model left
  mis = dbm.get_model_infos(mids);
  REQUIRE_EQ(1u, mis.size());
  CHECK_EQ(mi2, mis[0]);                 // and that we got the right model left
  CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 2 since we removed 1'

  // remove last model..
  dbm.remove_model(mid2);
  mis = dbm.get_model_infos(mids);
  REQUIRE_EQ(0u, mis.size());
  CHECK_EQ(
    0u,
    dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 0 since we removed all models
}

TEST_CASE("em/server/io_rocksdb") {
  using db_t = db<model, db_rocks>;
  using srv_t = server<db_t>;
  using client_t = client<model>;
  //
  // 0. arrange with a server and a client running on localhost, tempdir.
  //
  srv_t s0(tmpdir.string() + "rocks_db2");
  string host_ip = "127.0.0.1";
  s0.set_listening_ip(host_ip);
  auto s_port = s0.start_server();
  CHECK_GE(s_port, 1024);
  client_t c{host_ip + ":" + to_string(s_port)};
  vector<int64_t> mids;
  auto mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 0);
  //
  // 1.  arrange with one model
  //
  auto m = test::build_model();
  model_info mi{0, "name2", from_seconds(600), "{b:'vv',b:3.21}"};
  m->id = 0;

  auto mid = c.store_model(m, mi);
  CHECK_EQ(mid, 1u);
  //
  // 2. with one model, verify minfo and m is ok
  //
  m->id = mid;
  mi.id = mid;
  auto mr = c.read_model(mid);
  CHECK_EQ(*mr, *m);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1u);
  CHECK_EQ(mi, mis[0]);

  auto mis2 = c.get_model_infos(mids, utcperiod(0, 1000));
  CHECK_EQ(mis2.size(), 1u);
  CHECK_EQ(mis2[0], mi);
  mis2 = c.get_model_infos(mids, utcperiod(1000, 2000));
  CHECK_EQ(mis2.size(), 0u);
  c.close(); // just to force auto-open on next call
  //
  // 3. check we can update minfo of existing model
  //
  mi.json = string("{something:else}");
  c.update_model_info(mi.id, mi);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 1u);
  CHECK_EQ(mi, mis[0]);

  //
  // 4. finally, remove the model, and verify we are back to zero
  //
  c.remove_model(mid);
  mis = c.get_model_infos(mids);
  CHECK_EQ(mis.size(), 0);
  c.close(); // should work here
  s0.clear();
}

TEST_CASE("em/server/rocks_db_migrate") {
  // 1 fill up a db_file db with something,
  //   pretend old db files are in place
  // notice: we keep some constants around here to get out
  //         perf. numbers to give indicators for time to migrate on real systems.
  //
  test::utils::temp_dir tmp_db("rocks_db_migrate");
  auto root = tmp_db.string();
  size_t blob_size=100;// real sized nordic em is 36*1000000;//1M
  constexpr size_t n = 100;// real sized number is like 1000, or 3*100;
  MESSAGE("Prepare old level db content");

  /* scope level db, so that it is closed when done*/ {
    shyft::srv::db_level l(root);
    for (size_t i = 0; i < n; ++i) {
      string m_blob = std::to_string(i + 1); // minimal
      model_info mi{int64_t(i + 1), "name of " + m_blob, from_seconds(600), "{b:'vv',b:3.21}"};
      string m_data(blob_size,'A');
      l.store_model_blob(m_data, mi);
    }
    CHECK_EQ(l.find_max_model_id(), n);
  }
  // now predend we are migrating it
  MESSAGE("Migrating it");
  {
    auto t0 = utctime_now();
    shyft::srv::db_rocks r(root); // construct with root
    auto t1 = utctime_now();
    MESSAGE("done in " << shyft::core::to_seconds(t1 - t0) << "[s]");
    MESSAGE("migrated number of models "
        << r.find_max_model_id()
        <<", model size "<<blob_size);

    // compare!
    /*again, use scope  */ {
      shyft::srv::db_level l(root); // the old
      auto all_mi = l.get_model_infos({});
      for (auto const & mi : all_mi) {
        auto a = r.read_model_blob(mi.id);
        auto b = l.read_model_blob(mi.id);
        CHECK_EQ(a, b);
        auto ia = r.get_model_infos({mi.id});
        auto ib = l.get_model_infos({mi.id});
        CHECK_EQ(ia, ib);
      }
    }
    //now prepare last verification:  add one more model to the migrated db.
    // and then shutdown r db, leaving this scope, it should be preserved.
    string m_blob = std::to_string(n + 1); // minimal
    model_info mi{int64_t(n + 1), "name of " + m_blob, from_seconds(600), "{b:'vv',b:3.21}"};
    string m_data(blob_size,'A');
    r.store_model_blob(m_data, mi);
  }
  MESSAGE("After migrate, ignore old/migrated directories");//do not migrate twice!
  { // then re-open the db, and verify it has one more item,
    // e.g.: the proof it did not migrate twice.
    shyft::srv::db_rocks r(root);
    CHECK_EQ(r.find_max_model_id(),n+1);//should be equal to the one we stored
  }
}

TEST_SUITE_END();
