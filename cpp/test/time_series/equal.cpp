#include "test_pch.h"

#include <cstdint>
#include <ranges>
#include <vector>

#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::time_series::dd {

  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/equal") {
    const core::utctime t0{0}, dt{1};
    const std::size_t n = 4;
    apoint_ts a(
      time_axis::generic_dt{t0, dt, n}, std::vector{-2.0, 1.0, 2.0, shyft::nan}, time_series::POINT_AVERAGE_VALUE);
    apoint_ts b(
      time_axis::generic_dt{t0, dt, n}, std::vector{-2.0, 1.0, 2.0, shyft::nan}, time_series::POINT_INSTANT_VALUE);
    apoint_ts c(
      time_axis::generic_dt{t0, dt, n}, std::vector{-2.0, 1.0, 2.0, shyft::nan}, time_series::POINT_AVERAGE_VALUE);
    apoint_ts d(
      time_axis::generic_dt{t0, dt, n}, std::vector{-2.1, 1.0, 2.0, shyft::nan}, time_series::POINT_AVERAGE_VALUE);
    apoint_ts e(
      time_axis::generic_dt{t0, core::utctime{2}, n},
      std::vector{-2.0, 1.0, 2.0, shyft::nan},
      time_series::POINT_AVERAGE_VALUE);
    CHECK(a == c);
    CHECK(a == a);
    CHECK(a != d);
    CHECK(a != e);
    CHECK(a != b);
  }

  TEST_SUITE_END();

}
