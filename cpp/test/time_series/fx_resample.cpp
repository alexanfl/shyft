#include "test_pch.h"

#include <shyft/time_series/fx_resample.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::time_series {
  TEST_SUITE_BEGIN("ts");

  using shyft::core::utctime;
  using gta_t = time_axis::generic_dt;
  using gts_t = point_ts<gta_t>;

  struct resample_fixture {
    utctime t0{0};
    utctime dt{10};
    size_t n{3};
    std::vector<double> vv{1.0, 2.0, 3.0};
    gts_t ts{
      gta_t{t0, dt, n},
      vv,
      POINT_AVERAGE_VALUE
    };
    gts_t ts_lin{
      gta_t{t0, dt, n},
      vv,
      POINT_AVERAGE_VALUE
    };
    gts_t ts_null{};
  };

  TEST_CASE_FIXTURE(resample_fixture, "ts/fx_resample/linear") {
    SUBCASE("zero_time_axis") {
      gta_t ta;
      FAST_CHECK_EQ(fx_resample(ts_lin, ta).size(), 0u);
    }
    SUBCASE("zero_ts") {
      gta_t ta{t0, dt, n};
      auto r = fx_resample(ts_null, ta);
      FAST_CHECK_EQ(r.size(), n);
      for (auto x : r)
        FAST_CHECK_EQ(std::isfinite(x), false);
    }
    SUBCASE("exact") {
      auto r = fx_resample(ts_lin, ts.time_axis());
      FAST_CHECK_EQ(r, ts.v);
    }
    SUBCASE("before") {
      gta_t ta_before{t0 - dt, dt, 1};
      auto r = fx_resample(ts_lin, ta_before);
      FAST_REQUIRE_EQ(r.size(), 1u);
      FAST_CHECK_EQ(std::isfinite(r[0]), false);
    }
    SUBCASE("after") {
      gta_t ta_after{t0 + n * dt, dt, 1};
      auto r = fx_resample(ts_lin, ta_after);
      FAST_REQUIRE_EQ(r.size(), 1u);
      FAST_CHECK_EQ(std::isfinite(r[0]), false);
    }
    SUBCASE("interior") {
      gta_t ta{t0 + dt / 2, dt, n};
      auto r = fx_resample(ts_lin, ta);
      FAST_REQUIRE_EQ(r.size(), ta.size());
      for (size_t i = 0; i < ta.size(); ++i) { // notice we check agains ts f(t) that does lin interpolate
        FAST_CHECK_EQ(nan_equal(r[i], ts_lin(ta.time(i))), true);
      }
    }
  }

  TEST_CASE_FIXTURE(resample_fixture, "ts/fx_resample/stair_case") {
    SUBCASE("zero_time_axis") {
      gta_t ta;
      FAST_CHECK_EQ(fx_resample(ts, ta).size(), 0u);
    }
    SUBCASE("zero_ts") {
      gta_t ta{t0, dt, n};
      auto r = fx_resample(ts_null, ta);
      FAST_CHECK_EQ(r.size(), n);
      for (auto x : r)
        FAST_CHECK_EQ(std::isfinite(x), false);
    }
    SUBCASE("exact") {
      auto r = fx_resample(ts, ts.time_axis());
      FAST_CHECK_EQ(r, ts.v);
    }
    SUBCASE("before") {
      gta_t ta_before{t0 - dt, dt, 1};
      auto r = fx_resample(ts, ta_before);
      FAST_REQUIRE_EQ(r.size(), 1u);
      FAST_CHECK_EQ(std::isfinite(r[0]), false);
    }
    SUBCASE("after") {
      gta_t ta_after{t0 + n * dt, dt, 1};
      auto r = fx_resample(ts, ta_after);
      FAST_REQUIRE_EQ(r.size(), 1u);
      FAST_CHECK_EQ(std::isfinite(r[0]), false);
    }
    SUBCASE("interior") {
      gta_t ta{t0 + dt / 2, dt, n};
      FAST_CHECK_EQ(fx_resample(ts, ta), ts.v);
    }
  }

  TEST_SUITE_END();

}
