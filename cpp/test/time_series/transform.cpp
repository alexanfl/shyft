#include "test_pch.h"
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/transform_spline_ts.h>

using std::make_shared;
using std::vector;
using std::array;
using std::string;
using namespace shyft;
using namespace shyft::core;
using namespace shyft::time_series;
using namespace shyft::time_axis;
using namespace shyft::time_series::dd;

namespace {

  fixed_dt ta(seconds(0), seconds(10), 6);
  apoint_ts x(ta, vector<double>{-1, 0, 1, 2, 3, 4.0}, ts_point_fx::POINT_INSTANT_VALUE);
  apoint_ts a("foo");

  vector<array<double, 2>> f{
    {0.0, 0.0},
    {1.0, 1.0},
    {3.0, 5.0}
  };

  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/method_linear") {
    auto p = spline_interpolator::interpolate(f, interpolation_scheme::SCHEME_LINEAR);
    SUBCASE("method_linear_parameter") {
      CHECK_EQ(p.knots.size(), 5);
      CHECK_EQ(p.coeff.size(), 3);
      CHECK_EQ(p.degree, 1);

      CHECK_EQ(p.coeff[0], 0.0);
      CHECK_EQ(p.coeff[1], 1.0);
      CHECK_EQ(p.coeff[2], 5.0);
    }
    auto ts_t = transform_spline_ts(x, p);
    auto v = ts_t.values();
    SUBCASE("method_linear_interpolate") {
      CHECK_EQ(v.size(), x.size());
      for (size_t i = 0; i < v.size(); ++i)
        CHECK_EQ(v[i], ts_t.value(i));

      CHECK_EQ(v[1], 0.0);
      CHECK_EQ(v[2], 1.0);
      CHECK_EQ(v[3], 3.0);
      CHECK_EQ(v[4], 5.0);
    }
    SUBCASE("method_linear_extrapolate") {
      CHECK_EQ(v[0], -1.0);
      CHECK_EQ(v[5], 7.0);
    }
  }

  TEST_CASE("ts/method_polynomial") {
    auto p = spline_interpolator::interpolate(f, interpolation_scheme::SCHEME_POLYNOMIAL);
    SUBCASE("method_polynomial_parameter") {
      CHECK_EQ(p.knots.size(), 6);
      CHECK_EQ(p.coeff.size(), 3);
      CHECK_EQ(p.degree, 2);

      CHECK_EQ(p.coeff[0], doctest::Approx(0.0));
      CHECK_EQ(p.coeff[1], doctest::Approx(1.0));
      CHECK_EQ(p.coeff[2], doctest::Approx(5.0));
    }
    auto ts_t = transform_spline_ts(x, p);
    auto v = ts_t.values();
    SUBCASE("method_polynomial_interpolate") {
      CHECK_EQ(v.size(), x.size());
      for (size_t i = 0; i < v.size(); ++i)
        CHECK_EQ(v[i], ts_t.value(i));

      CHECK_EQ(v[1], doctest::Approx(0.0));
      CHECK_EQ(v[2], doctest::Approx(1.0));
      CHECK_EQ(v[3], doctest::Approx(8.0 / 3.0));
      CHECK_EQ(v[4], doctest::Approx(5.0));
    }
    SUBCASE("method_polynomial_extrapolate") {
      CHECK_EQ(v[0], doctest::Approx(-1.0 / 3.0));
      CHECK_EQ(v[5], doctest::Approx(8.0));
    }
  }

  TEST_CASE("ts/method_catmull-rom") {
    auto p = spline_interpolator::interpolate(f, interpolation_scheme::SCHEME_CATMULL_ROM);
    SUBCASE("method_catmull-rom_parameter") {
      CHECK_EQ(p.knots.size(), 10);
      CHECK_EQ(p.coeff.size(), 6);
      CHECK_EQ(p.degree, 3);

      CHECK_EQ(p.coeff[0], doctest::Approx(0.0 - 0.0 / 3.0));
      CHECK_EQ(p.coeff[1], doctest::Approx(0.0 + 1.0 / 3.0));
      CHECK_EQ(p.coeff[2], doctest::Approx(1.0 - 1.5 / 3.0));
      CHECK_EQ(p.coeff[3], doctest::Approx(1.0 + 3.0 / 3.0));
      CHECK_EQ(p.coeff[4], doctest::Approx(5.0 - 4.0 / 3.0));
      CHECK_EQ(p.coeff[5], doctest::Approx(5.0 + 0.0 / 3.0));
    }
    auto ts_t = transform_spline_ts(x, p);
    auto v = ts_t.values();
    SUBCASE("method_catmull-rom_interpolate") {
      CHECK_EQ(v.size(), x.size());
      for (size_t i = 0; i < v.size(); ++i)
        CHECK_EQ(v[i], ts_t.value(i));

      CHECK_EQ(v[1], doctest::Approx(0.0));
      CHECK_EQ(v[2], doctest::Approx(1.0));
      CHECK_EQ(v[3], doctest::Approx(23.0 / 8.0));
      CHECK_EQ(v[4], doctest::Approx(5.0));
    }
    SUBCASE("method_catmull-rom_extrapolate") {
      CHECK_EQ(v[0], doctest::Approx(-2.0));
      CHECK_EQ(v[5], doctest::Approx(53.0 / 8.0));
    }
  }

  auto y = x.transform(f, interpolation_scheme::SCHEME_LINEAR);

  TEST_CASE("ts/transformed_ts_equal_time_axis") {
    CHECK_EQ(y.time_axis(), x.time_axis());
  }

  auto b = a.transform(f, interpolation_scheme::SCHEME_LINEAR);

  TEST_CASE("ts/transformed_ts_needs_bind") {
    CHECK_EQ(b.needs_bind(), true);
  }

  TEST_CASE("ts/transformed_ts_stringify") {
    auto bs = b.stringify();
    FAST_CHECK_EQ(bs, "transform_spline_ts(foo,..params..)");
  }

  TEST_CASE("ts/transform_ts_bind_info") {
    auto bi = a.find_ts_bind_info();
    CHECK_EQ(bi.size(), 1);
    CHECK_EQ(bi[0].reference, "foo");
    bi[0].ts.bind(x);
    a.do_bind();
    CHECK_EQ(b.needs_bind(), false);
    CHECK_EQ(b.time_axis(), x.time_axis());
  }

  TEST_SUITE_END();

} // end namespace ts_transform_test
