#include <doctest/doctest.h>

#include <shyft/time_series/fx_make_aligned.h>


namespace shyft::time_series {


  // workbench for working out the make_ts_aligned_ts algorithm and semantics.
  // later to be moved into shyft/dtss/fx_make_aligned when done/satisfied.
  //

  using namespace shyft::time_series;
  using namespace shyft::core;
  using namespace shyft;


  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/fx_make_aligned/linear") {
    // targt ts is a fixed ts, linear interpretation
    // main semantics:
    // resample(as in f(t)) incoming fragment with the target time-points covered
    // by the incoming fragment.
    //
    auto utc = std::make_shared<calendar>();
    time_axis_calendar_dt c(utc);
    SUBCASE("frag/stair_case/exact/calendar") {
      // we need at least one test to stress calendar arithmetic
      auto t0 = utc->time(2023, 1, 1);
      auto dt1 = utc->time(2023, 4, 1) - t0;
      gts_t src{
        time_axis::generic_dt{t0, dt1, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, t0, calendar::MONTH, POINT_INSTANT_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utc, t0, calendar::MONTH, 3},
        vector<double>{1.0, 1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      FAST_CHECK_EQ(r.time_axis(), expected.time_axis());
      FAST_CHECK_EQ(r, expected);
    }

    SUBCASE("frag/stair_case/exact") {
      gts_t src{
        time_axis::generic_dt{utctime{0}, utctime{4}, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{1}, POINT_INSTANT_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{1}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      FAST_CHECK_EQ(r.time_axis(), expected.time_axis());
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/stair_case/to_short") {
      gts_t src{
        time_axis::generic_dt{utctime{1}, utctime{2 * 3}, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_INSTANT_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{2}, utctime{2}, 2},
        vector<double>{1.0, 1.0}, // notice: it shrinks to cover target time-points
        POINT_INSTANT_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/stair_case/interior_only") {
      gts_t src{
        time_axis::generic_dt{utctime{1}, utctime{1}, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_INSTANT_VALUE, src);
      FAST_CHECK_EQ(r.size(), 0u);
    }
    SUBCASE("frag/linear/exact_w_one_point") {
      gts_t src{
        time_axis::generic_dt{utctime{0}, utctime{4}, 1},
        1.0, POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{1}, POINT_INSTANT_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{1}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/linear/exact_w_multiple_points") {
      gts_t src{
        time_axis::generic_dt(vector<utctime>{utctime{0}, utctime{3}},
        utctime{4}
        ),
        vector<double>{1.0, 4.0},
        POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{1}, POINT_INSTANT_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{1}, 4},
        vector<double>{1.0, 2.0, 3.0, 4.0},
        POINT_INSTANT_VALUE
      };
      FAST_CHECK_EQ(r.time_axis(), expected.time_axis());
      FAST_CHECK_EQ(r, expected);
    }

    SUBCASE("frag/linear/to_short") {
      gts_t src{
        time_axis::generic_dt{vector<utctime>{utctime{1}, utctime{3}}, utctime{4}},
        vector<double>{                                    0.0,        2.0},
        POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_INSTANT_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{2}, utctime{2}, 1},
        vector<double>{1.0}, // covering just one
        POINT_INSTANT_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }

    SUBCASE("frag/linear/interior_only") {
      gts_t src{
        time_axis::generic_dt(vector<utctime>{utctime{1}, utctime{3}},
        utctime{4}
        ),
        vector<double>{1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{10}, POINT_INSTANT_VALUE, src);
      FAST_CHECK_EQ(r.size(), 0);
    }
  }

  TEST_CASE("ts/fx_make_aligned/stair_case") {
    // main semantics:
    // project the true-average of the incoming fragment to the target time-axis
    //  .. expand incoming fragment range to cover any partially overlapped
    time_axis_fixed_dt c;
    SUBCASE("frag/stair_case/exact/calendar") {
      auto utc = std::make_shared<calendar>();
      time_axis_calendar_dt cc(utc);
      auto t0 = utc->time(2023, 1, 1);
      auto dt1 = utc->time(2023, 4, 1) - t0;
      gts_t src{
        time_axis::generic_dt{t0, dt1, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(cc, t0, calendar::MONTH, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utc, t0, calendar::MONTH, 3},
        vector<double>{1.0, 1.0, 1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/stair_case/exact") {
      gts_t src{
        time_axis::generic_dt{utctime{0}, utctime{4}, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{1}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{1}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/stair_case/to_short") {
      gts_t src{
        time_axis::generic_dt{utctime{1}, utctime{2 * 3}, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{2}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0}, // notice: it bleeds out to fill stair-case
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/stair_case/interior_only") {
      gts_t src{
        time_axis::generic_dt{utctime{1}, utctime{1}, 1},
        1.0, POINT_AVERAGE_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{2}, 1},
        vector<double>{1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/linear/exact_w_one_point") {
      gts_t src{
        time_axis::generic_dt{utctime{0}, utctime{4}, 1},
        1.0, POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{1}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{1}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/linear/exact_w_multiple_points") {
      gts_t src{
        time_axis::generic_dt(vector<utctime>{utctime{0}, utctime{4}},
        utctime{5}
        ),
        vector<double>{1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{1}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{1}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r.time_axis(), expected.time_axis());
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/linear/to_short") {
      gts_t src{
        time_axis::generic_dt{utctime{1}, utctime{2 * 3}, 1},
        1.0, POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{2}, 4},
        vector<double>{1.0, 1.0, 1.0, 1.0}, // notice: it bleeds out to fill stair-case
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/linear/to_short_w_multiple_points") {
      gts_t src{
        time_axis::generic_dt(vector<utctime>{utctime{1}, utctime{3}},
        utctime{5}
        ),
        vector<double>{1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{2}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{2}, 2},
        vector<double>{1.0, 1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
    SUBCASE("frag/linear/interior_only") {
      gts_t src{
        time_axis::generic_dt(vector<utctime>{utctime{1}, utctime{3}},
        utctime{4}
        ),
        vector<double>{1.0, 1.0},
        POINT_INSTANT_VALUE
      };
      auto r = fx_make_aligned(c, utctime{0}, utctime{10}, POINT_AVERAGE_VALUE, src);
      gts_t expected{
        time_axis::generic_dt{utctime{0}, utctime{10}, 1},
        vector<double>{1.0},
        POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, expected);
    }
  }

  TEST_SUITE_END();
}
