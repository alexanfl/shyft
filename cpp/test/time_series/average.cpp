#include "test_pch.h"
#define _USE_MATH_DEFINES
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/average.h>
#include <shyft/time_series/point_ts.h>

#include <shyft/time_series/fx_average.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/average_ts.h>
#include <shyft/time_series/accessor.h>

using namespace shyft::core;
using namespace shyft::time_series;
using namespace shyft::time_axis;
using namespace std;

namespace shyft::time_series {

  //-- new factorised functions:
  template < class TA, class TS>
  vector<double> fx_accumulate_linear(const TA& ta, const TS& ts, bool avg) {
    vector<double> r(ta.size(), shyft::nan);
    _accumulate_linear(
      ta,
      ts,
      avg,
      [](TS const & ts, utctime t) {
        return ts.index_of(t);
      },
      [&r](size_t i, double v) noexcept {
        r[i] = v;
      });
    return r;
  }

  template < class TA, class TS>
  vector<double> fx_accumulate_stair_case(const TA& ta, const TS& ts, bool avg) {
    vector<double> r(ta.size(), shyft::nan);
    _accumulate_stair_case(
      ta,
      ts,
      avg,
      [](TS const & ts, utctime t) {
        return ts.index_of(t);
      },
      [&r](size_t i, double v) noexcept {
        r[i] = v;
      });
    return r;
  }

  //-- for test; run via the python exposed apoint_ts interface
  template < class TA, class TS>
  vector<double> apoint_ts_accumulate_linear(const TA& ta, const TS& ts, bool avg) {
    using dd::apoint_ts;
    using dd::gta_t;
    apoint_ts src{ts};
    src.set_point_interpretation(POINT_INSTANT_VALUE);
    return avg ? src.average(gta_t{ta}).values() : src.integral(gta_t{ta}).values();
  }

  template < class TA, class TS>
  vector<double> apoint_ts_accumulate_stair_case(const TA& ta, const TS& ts, bool avg) {
    using dd::apoint_ts;
    using dd::gta_t;
    apoint_ts src{ts};
    src.set_point_interpretation(POINT_AVERAGE_VALUE);
    return avg ? src.average(gta_t{ta}).values() : src.integral(gta_t{ta}).values();
  }

}

// helper
static inline utctime _t(int64_t t1970s) {
  return utctime{seconds(t1970s)};
}

template <class F>
void test_linear_fx(F&& acc_fn) {

  utctimespan dt{deltahours(1)};
  fixed_dt ta{_t(0), dt, 6};
  ts_point_fx linear{POINT_INSTANT_VALUE};
  point_ts<decltype(ta)> ts{
    ta, vector<double>{1.0, 2.0, shyft::nan, 4.0, 3.0, 6.0},
     linear
  };
  // own_axis_average
  {
    auto r = acc_fn(ta, ts, true);
    REQUIRE_EQ(r.size(), ta.size());
    CHECK(r[0] == doctest::Approx(1.5));
    CHECK_UNARY(!isfinite(r[1]));
    CHECK_UNARY(!isfinite(r[2]));
    CHECK(r[3] == doctest::Approx(3.5));
    CHECK(r[4] == doctest::Approx(4.5));
    CHECK_UNARY(!isfinite(r[5]));
  }
  // own_axis_integral
  {
    auto r = acc_fn(ta, ts, false);
    REQUIRE_EQ(r.size(), ta.size());
    CHECK(r[0] == doctest::Approx(1.5 * to_seconds(dt)));
    CHECK_UNARY(!isfinite(r[1]));
    CHECK_UNARY(!isfinite(r[2]));
    CHECK(r[3] == doctest::Approx(3.5 * to_seconds(dt)));
    CHECK(r[4] == doctest::Approx(4.5 * to_seconds(dt)));
    CHECK_UNARY(!isfinite(r[5]));
  }
  // zero_axis
  {
    fixed_dt zta{_t(0), dt, 0};
    auto r = acc_fn(zta, ts, true);
    CHECK_EQ(r.size(), 0);
  }
  // zero_ts
  {
    point_ts<decltype(ta)> zts(fixed_dt{_t(0), dt, 0}, 1.0, linear);
    auto r = acc_fn(ta, zts, true);
    CHECK_EQ(r.size(), ta.size());
    for (auto const & v : r)
      CHECK_UNARY(!isfinite(v));
  }
  // axis_before
  {
    fixed_dt bta{_t(-10000), dt, 1};
    auto r = acc_fn(bta, ts, true);
    CHECK_EQ(r.size(), bta.size());
    for (auto const & v : r)
      CHECK_UNARY(!isfinite(v));
  }
  SUBCASE("axis_after") {
    fixed_dt ata{ta.total_period().end, dt, 10};
    auto r = acc_fn(ata, ts, true);
    CHECK_EQ(r.size(), ata.size());
    for (auto const & v : r)
      CHECK_UNARY(!isfinite(v));
  }
  // aligned_x2_axis
  {
    fixed_dt ta2(_t(0), 2 * dt, ta.size() / 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.5));
    CHECK(r[1] == doctest::Approx(3.5));
    CHECK(r[2] == doctest::Approx(4.5));
  }
  // aligned_/2_axis
  {
    fixed_dt ta2(_t(0), dt / 2, ta.size() * 2 - 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.25));
    CHECK(r[1] == doctest::Approx(1.75));
    CHECK_UNARY(!isfinite(r[2]));
    CHECK_UNARY(!isfinite(r[3]));
    CHECK_UNARY(!isfinite(r[4]));
    CHECK_UNARY(!isfinite(r[5]));
    CHECK(r[6] == doctest::Approx(3.75));
    CHECK(r[7] == doctest::Approx(3.25));
    CHECK(r[8] == doctest::Approx(0.5 * (3.0 + 4.5)));
    CHECK(r[9] == doctest::Approx(0.5 * (4.5 + 6.0)));
  }
  // aligned_one_interval
  {
    fixed_dt ta2{_t(0), ta.total_period().timespan(), 1};
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx((1.5 + 3.5 + 4.5) / 3.0));
  }
  // un_aligned_/2_axis_begins_in_interval
  {
    fixed_dt ta2(utctime(+dt / 4), dt / 2, ta.size() * 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.500));
    CHECK(r[1] == doctest::Approx(1.875));
    CHECK_UNARY(!isfinite(r[2]));
    CHECK_UNARY(!isfinite(r[3]));
    CHECK_UNARY(!isfinite(r[4]));
    CHECK(r[5] == doctest::Approx(3.875));
    CHECK(r[6] == doctest::Approx(3.500));
    CHECK(r[7] == doctest::Approx(3.250));
    CHECK(r[8] == doctest::Approx(4.500));
    CHECK(r[9] == doctest::Approx(5.625));
    CHECK_UNARY(!isfinite(r[10]));
    CHECK_UNARY(!isfinite(r[11]));
  }
  // un_aligned_/2_axis_begins_before_interval
  {
    fixed_dt ta2(utctime(-dt / 4), dt / 2, ta.size() * 2 - 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.125));
    CHECK(r[1] == doctest::Approx(1.500));
    CHECK(r[2] == doctest::Approx(1.875));
    CHECK_UNARY(!isfinite(r[3]));
    CHECK_UNARY(!isfinite(r[4]));
    CHECK_UNARY(!isfinite(r[5]));
    CHECK(r[6] == doctest::Approx(3.875));
    CHECK(r[7] == doctest::Approx(3.500));
    CHECK(r[8] == doctest::Approx(3.250));
    CHECK(r[9] == doctest::Approx(4.500));
  }
  // out_of_points_searching_for_start
  {
    point_ts<fixed_dt> nts(
      fixed_dt{_t(0), dt, 5}, vector<double>{shyft::nan, 1.0, 1.0, shyft::nan, shyft::nan}, linear);
    fixed_dt ta2{_t(0), dt * 4, 2};
    auto r = acc_fn(ta2, nts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.0));
    CHECK_UNARY(!isfinite(r[1]));
  }
  // just_nans
  {
    point_ts<fixed_dt> nts(
      fixed_dt{_t(0), dt, 5}, vector<double>{shyft::nan, shyft::nan, shyft::nan, shyft::nan, shyft::nan}, linear);
    fixed_dt ta2{_t(0), dt * 4, 2};
    auto r = acc_fn(ta2, nts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK_UNARY(!isfinite(r[0]));
    CHECK_UNARY(!isfinite(r[1]));
  }
  // just_one_value_in_source
  {
    point_ts<fixed_dt> nts(fixed_dt{_t(0), dt, 1}, vector<double>{1.0}, linear);
    fixed_dt ta2{_t(0), dt * 4, 2};
    auto r = acc_fn(ta2, nts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK_UNARY(!isfinite(r[0]));
    CHECK_UNARY(!isfinite(r[1]));
  }
}

template <class F>
void test_stair_case_fx(F&& acc_fn) {
  utctimespan dt{deltahours(1)};
  fixed_dt ta{_t(0), dt, 6};
  ts_point_fx stair_case{POINT_AVERAGE_VALUE};
  point_ts<decltype(ta)> ts{
    ta, vector<double>{1.0, 2.0, shyft::nan, 4.0, 3.0, 6.0},
     stair_case
  };
  // own_axis_average
  {
    auto r = acc_fn(ta, ts, true);
    REQUIRE_EQ(r.size(), ta.size());
    CHECK(r[0] == doctest::Approx(1.0));
    CHECK(r[1] == doctest::Approx(2.0));
    CHECK_UNARY(!isfinite(r[2]));
    CHECK(r[3] == doctest::Approx(4.0));
    CHECK(r[4] == doctest::Approx(3.0));
    CHECK(r[5] == doctest::Approx(6.0));
  }
  // own_axis_integral
  {
    auto r = acc_fn(ta, ts, false);
    REQUIRE_EQ(r.size(), ta.size());
    CHECK(r[0] == doctest::Approx(1.0 * to_seconds(dt)));
    CHECK(r[1] == doctest::Approx(2.0 * to_seconds(dt)));
    CHECK_UNARY(!isfinite(r[2]));
    CHECK(r[3] == doctest::Approx(4.0 * to_seconds(dt)));
    CHECK(r[4] == doctest::Approx(3.0 * to_seconds(dt)));
    CHECK(r[5] == doctest::Approx(6.0 * to_seconds(dt)));
  }
  // zero_axis
  {
    fixed_dt zta{_t(0), dt, 0};
    auto r = acc_fn(zta, ts, true);
    CHECK_EQ(r.size(), 0);
  }
  // zero_ts
  {
    point_ts<decltype(ta)> zts(fixed_dt{_t(0), dt, 0}, 1.0, stair_case);
    auto r = acc_fn(ta, zts, true);
    CHECK_EQ(r.size(), ta.size());
    for (auto const & v : r)
      CHECK_UNARY(!isfinite(v));
  }
  // one_point_ts
  {
    point_ts<decltype(ta)> zts(fixed_dt{_t(0), dt, 1}, 1.0, stair_case);
    auto r = acc_fn(ta, zts, true);
    CHECK_EQ(r.size(), ta.size());
    CHECK(r[0] == doctest::Approx(1.0));
    for (size_t i = 1; i < r.size(); ++i)
      CHECK_UNARY(!isfinite(r[i]));
  }
  // last_interval_handling
  {
    point_ts<decltype(ta)> ots(fixed_dt{_t(0), dt * ta.size(), 1}, 1.0, stair_case);
    auto r = acc_fn(ta, ots, false);
    CHECK_EQ(r.size(), ta.size());
    for (size_t i = 0; i < ta.size(); ++i) {
      CHECK_EQ(r[i], doctest::Approx(to_seconds(dt)));
    }
  }
  // axis_before
  {
    fixed_dt bta{_t(-10000), dt, 1};
    auto r = acc_fn(bta, ts, true);
    CHECK_EQ(r.size(), bta.size());
    for (auto const & v : r)
      CHECK_UNARY(!isfinite(v));
  }
  // axis_after
  {
    fixed_dt ata{ta.total_period().end, dt, 10};
    auto r = acc_fn(ata, ts, true);
    CHECK_EQ(r.size(), ata.size());
    for (auto const & v : r)
      CHECK_UNARY(!isfinite(v));
  }
  // aligned_x2_axis
  {
    fixed_dt ta2(_t(0), 2 * dt, ta.size() / 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.5));
    CHECK(r[1] == doctest::Approx(4.0));
    CHECK(r[2] == doctest::Approx(4.5));
  }
  // aligned_/2_axis
  {
    fixed_dt ta2(_t(0), dt / 2, ta.size() * 2 - 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.00));
    CHECK(r[1] == doctest::Approx(1.00));
    CHECK(r[2] == doctest::Approx(2.00));
    CHECK(r[3] == doctest::Approx(2.00));
    CHECK_UNARY(!isfinite(r[4]));
    CHECK_UNARY(!isfinite(r[5]));
    CHECK(r[6] == doctest::Approx(4.00));
    CHECK(r[7] == doctest::Approx(4.00));
    CHECK(r[8] == doctest::Approx(3.00));
    CHECK(r[9] == doctest::Approx(3.00));
  }
  // aligned_one_interval
  {
    fixed_dt ta2{_t(0), ta.total_period().timespan(), 1};
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx((1 + 2 + 4 + 3 + 6) / 5.0));
  }
  // un_aligned_/2_axis_begins_in_interval
  {
    fixed_dt ta2(utctime(+dt / 4), dt / 2, ta.size() * 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.000));
    CHECK(r[1] == doctest::Approx(1.500));
    CHECK(r[2] == doctest::Approx(2.000));
    CHECK(r[3] == doctest::Approx(2.000));
    CHECK_UNARY(!isfinite(r[4]));
    CHECK(r[5] == doctest::Approx(4.000));
    CHECK(r[6] == doctest::Approx(4.000));
    CHECK(r[7] == doctest::Approx(3.500));
    CHECK(r[8] == doctest::Approx(3.000));
    CHECK(r[9] == doctest::Approx(4.500));
    CHECK(r[10] == doctest::Approx(6.000));
    CHECK(r[11] == doctest::Approx(6.000));
  }
  // un_aligned_/2_axis_begins_before_interval
  {
    fixed_dt ta2(utctime(-dt / 4), dt / 2, ta.size() * 2 - 2);
    auto r = acc_fn(ta2, ts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.000));
    CHECK(r[1] == doctest::Approx(1.000));
    CHECK(r[2] == doctest::Approx(1.500));
    CHECK(r[3] == doctest::Approx(2.000));
    CHECK(r[4] == doctest::Approx(2.000));
    CHECK_UNARY(!isfinite(r[5]));
    CHECK(r[6] == doctest::Approx(4.000));
    CHECK(r[7] == doctest::Approx(4.000));
    CHECK(r[8] == doctest::Approx(3.500));
    CHECK(r[9] == doctest::Approx(3.000));
  }
  // out_of_points_searching_for_start
  {
    point_ts<fixed_dt> nts(
      fixed_dt{_t(0), dt, 5}, vector<double>{shyft::nan, 1.0, 1.0, shyft::nan, shyft::nan}, stair_case);
    fixed_dt ta2{_t(0), dt * 4, 2};
    auto r = acc_fn(ta2, nts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.0));
    CHECK_UNARY(!isfinite(r[1]));
  }
  // just_nans
  {
    point_ts<fixed_dt> nts(
      fixed_dt{_t(0), dt, 5}, vector<double>{shyft::nan, shyft::nan, shyft::nan, shyft::nan, shyft::nan}, stair_case);
    fixed_dt ta2{_t(0), dt * 4, 2};
    auto r = acc_fn(ta2, nts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK_UNARY(!isfinite(r[0]));
    CHECK_UNARY(!isfinite(r[1]));
  }
  // just_one_value_in_source
  {
    point_ts<fixed_dt> nts(fixed_dt{_t(0), dt, 1}, vector<double>{1.0}, stair_case);
    fixed_dt ta2{_t(0), dt * 4, 2};
    auto r = acc_fn(ta2, nts, true);
    REQUIRE_EQ(r.size(), ta2.size());
    CHECK(r[0] == doctest::Approx(1.0));
    CHECK_UNARY(!isfinite(r[1]));
  }
}

TEST_SUITE_BEGIN("ts");

TEST_CASE("ts/avg_linear") {
  auto f_lin = accumulate_linear<fixed_dt, point_ts<fixed_dt>>;
  test_linear_fx(f_lin);
}

TEST_CASE("ts/apoint_ts_avg_linear") {
  auto f_lin = apoint_ts_accumulate_linear<fixed_dt, point_ts<fixed_dt>>;
  test_linear_fx(f_lin);
}

TEST_CASE("ts/avg_stair_case") {
  auto f_stair_case = accumulate_stair_case<fixed_dt, point_ts<fixed_dt>>;
  test_stair_case_fx(f_stair_case);
}

TEST_CASE("ts/apoint_ts_avg_stair_case") {
  auto f_stair_case = apoint_ts_accumulate_stair_case<fixed_dt, point_ts<fixed_dt>>;
  test_stair_case_fx(f_stair_case);
}

TEST_CASE("perf/ts/avg") {
  using ts_t = point_ts<fixed_dt>;
  size_t n = 5 * 365 * 8; // 0.1 MB pr ts.
  size_t n_ts = 1000;     //~100 MB for each 1000 ts.
  utctimespan dt_h{deltahours(1)};
  utctimespan dt_h24{24 * dt_h};
  fixed_dt ta_h{_t(0), dt_h, n};
  ts_point_fx linear{POINT_INSTANT_VALUE};
  vector<ts_t> tsv;
  for (size_t i = 0; i < n_ts; ++i)
    tsv.emplace_back(ta_h, double(i), linear);

  // time ts->vector
  fixed_dt ta_h24(_t(0), dt_h24, n / 24);
  double s = 0.0;
  auto t0 = timing::now();
  for (size_t i = 0; i < n_ts; ++i) {
    auto r = accumulate_stair_case(ta_h24, tsv[i], true);
    s += r[0];
  }
  auto t1 = timing::now();
  for (size_t i = 0; i < n_ts; ++i) {
    auto r = accumulate_linear(ta_h24, tsv[i], true);
    s += r[0];
  }
  auto t2 = timing::now();
  for (size_t i = 0; i < n_ts; ++i) {
    average_accessor<ts_t, fixed_dt> avg(tsv[i], ta_h24);
    vector<double> r;
    r.reserve(ta_h24.size());
    for (size_t t = 0; t < ta_h24.size(); ++t) {
      r.push_back(avg.value(t));
    }
    s += r[0];
  }
  auto t3 = timing::now();
  auto t10 = timing::now();
  for (size_t i = 0; i < n_ts; ++i) {
    auto r = fx_accumulate_stair_case(ta_h24, tsv[i], true);
    s += r[0];
  }
  auto t11 = timing::now();
  for (size_t i = 0; i < n_ts; ++i) {
    auto r = fx_accumulate_linear(ta_h24, tsv[i], true);
    s += r[0];
  }
  auto t12 = timing::now();
  auto us_l = elapsed_us(t0, t1);
  auto us_s = elapsed_us(t1, t2);
  auto us_l1 = elapsed_us(t10, t11);
  auto us_s1 = elapsed_us(t11, t12);
  auto us_o = elapsed_us(t2, t3);
  CHECK_GE(s, 0.0);
  double mpts_s = n_ts * n / 1e6; // mill pts in source dim
  MESSAGE("Stair  mill pts/sec " << mpts_s / (us_l / 1e6) << " -> " << mpts_s / (us_l1 / 1e6));
  MESSAGE("Linear mill pts/sec " << mpts_s / (us_s / 1e6) << " -> " << mpts_s / (us_s1 / 1e6));
  MESSAGE("Old    mill pts/sec " << mpts_s / (us_o / 1e6));
}

TEST_CASE("ts/core_ts_last_interval_case") {
  using ts_t = point_ts<fixed_dt>;
  calendar utc{};
  fixed_dt ta_s{utc.time(2017, 10, 16), deltahours(24 * 7), 219};
  fixed_dt ta{utc.time(2017, 10, 16), deltahours(3), 12264};
  ts_t src(ta_s, 1.0, POINT_AVERAGE_VALUE);

  average_ts<ts_t, fixed_dt> i_src{src, ta};
  for (size_t i = 0; i < ta.size(); ++i)
    CHECK(i_src.value(i) == doctest::Approx(1.0));
}

TEST_SUITE_END();
