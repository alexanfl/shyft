#include "test_pch.h"

#include <cmath>
#include <vector>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>


using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::dd::deflate_ts_vector;

TEST_SUITE_BEGIN("ts");

TEST_CASE("ts/compute_ts_vector") {
  // Test that deflate_ts_vector returns an empty apoint_ts when given one, no segfault
  ats_vector tsv;
  tsv.push_back(apoint_ts());
  auto res = deflate_ts_vector<apoint_ts>(tsv);
  CHECK_EQ(res[0].ts, nullptr);
}

TEST_SUITE_END();
