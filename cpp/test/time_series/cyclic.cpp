#include "test_pch.h"

#include <memory>
#include <map>
#include <string>
#include <string_view>

#include <shyft/time_series/time_series_dd.h> //get all of them
#include <shyft/time_series/dd/is_cyclic.h>
#include <fmt/ranges.h>

namespace shyft::time_series::dd {

  TEST_SUITE_BEGIN("ts");

  inline constexpr auto terminal_schema = "shyft://";

  TEST_CASE("ts/cyclicity") {
    auto get_map_resolver = [](auto const & map) {
      return [=]([[maybe_unused]] const std::shared_ptr<const ipoint_ts>& source, std::string_view url)
               -> std::shared_ptr<const ipoint_ts> {
        auto it = map.find(url);
        if (it == map.end() || url.starts_with(terminal_schema)) {
          return nullptr;
        }
        return it->second;
      };
    };

    std::shared_ptr<ipoint_ts const> ts0{
      new gpoint_ts{time_axis::generic_dt{time_axis::fixed_dt{0, 1, 5}}, 2.0}
    };

    SUBCASE("Terminal") {
      constexpr auto url_terminal = "shyft://terminal_url";
      std::shared_ptr<ipoint_ts const> termref{new aref_ts{url_terminal}};
      apoint_ts cts = apoint_ts{termref};
      std::map<std::string, std::shared_ptr<ipoint_ts const>, std::less<>> map{
        {url_terminal, termref}
      };
      CHECK(!is_cyclic(get_map_resolver(map), cts));
    }

    SUBCASE("self reference") {
      constexpr auto url_self_ref_a = "test://self_referencinga";
      constexpr auto url_self_ref_b = "test://self_referencingb";
      std::shared_ptr<ipoint_ts const> cyclea{new aref_ts{url_self_ref_b}};
      std::shared_ptr<ipoint_ts const> cycleb{new aref_ts{url_self_ref_a}};
      std::map<std::string, std::shared_ptr<ipoint_ts const>, std::less<>> map{
        {url_self_ref_a, cyclea},
        {url_self_ref_b, cycleb},
      };
      apoint_ts cts{cyclea};
      CHECK(is_cyclic(get_map_resolver(map), cts));
    }

    SUBCASE("long chain") {
      constexpr auto url_self_ref_a = "test://self_referencinga";
      constexpr auto url_self_ref_b = "test://self_referencingb";
      constexpr auto url_self_ref_c = "test://self_referencingc";
      constexpr auto url_self_ref_d = "test://self_referencingd";
      constexpr auto url_self_ref_e = "test://self_referencinge";
      constexpr auto url_self_ref_f = "test://self_referencingf";
      std::shared_ptr<ipoint_ts const> cyclea{new aref_ts{url_self_ref_b}};
      std::shared_ptr<ipoint_ts const> cycleb{new aref_ts{url_self_ref_c}};
      std::shared_ptr<ipoint_ts const> cyclec{new aref_ts{url_self_ref_d}};
      std::shared_ptr<ipoint_ts const> cycled{new aref_ts{url_self_ref_e}};
      std::shared_ptr<ipoint_ts const> cyclee{new aref_ts{url_self_ref_f}};
      std::shared_ptr<ipoint_ts const> cyclef{new aref_ts{url_self_ref_a}};
      std::map<std::string, std::shared_ptr<ipoint_ts const>, std::less<>> map{
        {url_self_ref_a, cyclea},
        {url_self_ref_b, cycleb},
        {url_self_ref_c, cyclec},
        {url_self_ref_d, cycled},
        {url_self_ref_e, cyclee},
        {url_self_ref_f, cyclef},
      };
      apoint_ts cts{cyclea};
      CHECK(is_cyclic(get_map_resolver(map), cts));
    }

    SUBCASE("two branches, one ts") {
      std::shared_ptr<ipoint_ts const> sum{
        new abin_op_ts{apoint_ts{ts0}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      std::map<std::string, std::shared_ptr<ipoint_ts const>, std::less<>> map;
      apoint_ts cts{sum};
      CHECK(!is_cyclic(get_map_resolver(map), cts));
    }

    SUBCASE("deep cycle") {
      constexpr auto url_a = "test://a";
      std::shared_ptr<ipoint_ts const> ref_to_a{new aref_ts{url_a}};
      // sum:
      std::shared_ptr<ipoint_ts const> sum_of_ref_and_ts0{
        new abin_op_ts{apoint_ts{ts0}, iop_t::OP_ADD, apoint_ts{ref_to_a}}
      };
      // another sum:
      std::shared_ptr<ipoint_ts const> a{
        new abin_op_ts{apoint_ts{sum_of_ref_and_ts0}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      std::map<std::string, std::shared_ptr<ipoint_ts const>, std::less<>> map{
        {url_a, a}
      };
      apoint_ts cts{a};
      CHECK(is_cyclic(get_map_resolver(map), cts));
    }

    SUBCASE("two branches with a single cycle") {
      constexpr auto url_a = "test://a";
      std::shared_ptr<ipoint_ts const> ref_to_a{new aref_ts{url_a}};
      // long non-cyclic chain:
      // sum:
      std::shared_ptr<ipoint_ts const> sum_ts0{
        new abin_op_ts{apoint_ts{ts0}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      // sum o sum:
      std::shared_ptr<ipoint_ts const> sum_ts0_1{
        new abin_op_ts{apoint_ts{sum_ts0}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      // sum o sum o sum:
      std::shared_ptr<ipoint_ts const> sum_ts0_2{
        new abin_op_ts{apoint_ts{sum_ts0_1}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      // sum o sum o sum o sum:
      std::shared_ptr<ipoint_ts const> sum_ts0_3{
        new abin_op_ts{apoint_ts{sum_ts0_2}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      // sum o sum o sum o sum o sum:
      std::shared_ptr<ipoint_ts const> sum_ts0_4{
        new abin_op_ts{apoint_ts{sum_ts0_3}, iop_t::OP_ADD, apoint_ts{ts0}}
      };
      // sum together last sum and ref, ref lhs:
      std::shared_ptr<ipoint_ts const> lhs_ref{
        new abin_op_ts{apoint_ts{ref_to_a}, iop_t::OP_ADD, apoint_ts{sum_ts0_4}}
      };
      // other way
      std::shared_ptr<ipoint_ts const> rhs_ref{
        new abin_op_ts{apoint_ts{sum_ts0_4}, iop_t::OP_ADD, apoint_ts{ref_to_a}}
      };
      std::map<std::string, std::shared_ptr<ipoint_ts const>, std::less<>> map{
        {url_a, lhs_ref}
      };
      apoint_ts cts_lhs{lhs_ref};
      CHECK(is_cyclic(get_map_resolver(map), cts_lhs));
      map[url_a] = rhs_ref;
      apoint_ts cts_rhs{rhs_ref};
      CHECK(is_cyclic(get_map_resolver(map), cts_rhs));
    }
  }
}
