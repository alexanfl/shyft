#include "test_pch.h"
#include <shyft/time_series/time_axis.h>

using namespace shyft;
using namespace shyft::core;
using namespace std;

// helper
static inline utctime _t(int64_t t1970s) {
  return utctime{seconds(t1970s)};
}

/** \brief Utility function to verify one time-axis are conceptually equal to another */
template <class TA, class TB>
static bool test_if_equal(const TA& e, const TB& t) {
  using namespace std;
  if (e.size() != t.size())
    return false;
  if (e.total_period() != t.total_period())
    return false;
  if (e.index_of(e.total_period().end) != t.index_of(e.total_period().end))
    return false;
  if (e.open_range_index_of(e.total_period().end) != t.open_range_index_of(e.total_period().end))
    return false;

  for (size_t i = 0; i < e.size(); ++i) {
    if (e.time(i) != t.time(i))
      return false;
    if (e.period(i) != t.period(i))
      return false;
    if (e.index_of(e.time(i) + deltaminutes(30)) != t.index_of(e.time(i) + deltaminutes(30)))
      return false;
    if (e.index_of(e.time(i) - deltaminutes(30)) != t.index_of(e.time(i) - deltaminutes(30)))
      return false;
    if (e.open_range_index_of(e.time(i) + deltaminutes(30)) != t.open_range_index_of(e.time(i) + deltaminutes(30)))
      return false;
    utctime tx = e.time(i) - deltaminutes(30);
    size_t ei = e.open_range_index_of(tx);
    size_t ti = t.open_range_index_of(tx);
    TS_ASSERT_EQUALS(ei, ti);
    if (ei != ti)
      return false;
  }

  if (!equivalent_time_axis(e, t)) // verify e and t produces the same periods
    return false;
  // now create a time-axis different from e and t, just to verify that equivalent_time_axis states it's false
  time_axis::fixed_dt u(utctime(seconds(1234)), deltahours(1), 21);

  return !equivalent_time_axis(u, e) && !equivalent_time_axis(u, t);
}

TEST_SUITE_BEGIN("ts");

TEST_CASE("ts/time_axis_merge_fixed_dt") {
  using namespace shyft::time_axis;
  utctime t0{seconds(0)}, t2{seconds(2)}, t3{seconds(3)}, t4{seconds(4)};
  utctimespan dt{seconds(1)}, dt2{seconds(2)};
  CHECK_EQ(merge(fixed_dt{t0, dt, 4}, fixed_dt{t4, dt, 4}), fixed_dt{t0, dt, 8});
  CHECK_EQ(merge(fixed_dt{t0, dt, 4}, fixed_dt{t3, dt, 4}), fixed_dt{t0, dt, 7});
  CHECK_EQ(merge(fixed_dt{t4, dt, 4}, fixed_dt{t0, dt, 4}), fixed_dt{t0, dt, 8});
  CHECK_EQ(merge(fixed_dt{t3, dt, 4}, fixed_dt{t0, dt, 4}), fixed_dt{t0, dt, 7});
  CHECK_UNARY(!can_merge(fixed_dt{t0, dt, 4}, fixed_dt{t2, dt2, 4}));
  CHECK_UNARY(!can_merge(fixed_dt{t0, dt, 0}, fixed_dt{t0, dt, 4}));
  CHECK_UNARY(!can_merge(fixed_dt{t0, dt, 3}, fixed_dt{t4, dt, 1}));
}

TEST_CASE("ts/time_axis_merge_calendar_dt") {
  using namespace shyft::time_axis;
  auto c = make_shared<calendar>("Europe/Oslo");
  auto u = make_shared<calendar>(deltahours(1));
  utctime t0{seconds(0)}, t3{3 * calendar::DAY}, t4{4 * calendar::DAY};
  utctimespan dt{calendar::DAY};
  CHECK_EQ(merge(calendar_dt{c, t0, dt, 4}, calendar_dt{c, t4, dt, 4}), calendar_dt{c, t0, dt, 8});
  CHECK_EQ(merge(calendar_dt{c, t0, dt, 4}, calendar_dt{c, t3, dt, 4}), calendar_dt{c, t0, dt, 7});
  CHECK_EQ(merge(calendar_dt{c, t4, dt, 4}, calendar_dt{c, t0, dt, 4}), calendar_dt{c, t0, dt, 8});
  CHECK_EQ(merge(calendar_dt{c, t3, dt, 4}, calendar_dt{c, t0, dt, 4}), calendar_dt{c, t0, dt, 7});
  CHECK_UNARY(!can_merge(calendar_dt{c, t3, dt, 4}, calendar_dt{u, t0, dt, 4}));     // different calendars
  CHECK_UNARY(!can_merge(calendar_dt{c, t3, dt * 2, 4}, calendar_dt{c, t0, dt, 4})); // different time-step
  CHECK_UNARY(!can_merge(calendar_dt{c, t3, dt, 4}, calendar_dt{c, t0, dt, 1}));     // disjoint periods
}

TEST_CASE("ts/time_axis_merge_point_dt") {
  using namespace shyft::time_axis;
  utctime t0{seconds(0)}, t1{seconds(1)}, t2{seconds(2)}, t3{seconds(3)}, t4{seconds(4)}, t5{seconds(5)},
    t6{seconds(6)}, t7{seconds(7)}, t10{seconds(10)};
  test_if_equal(
    merge(
      point_dt{
        {t0, t1, t2},
        t3
  },
      point_dt{{t3, t4, t5}, t6}),
    point_dt{{t0, t1, t2, t3, t4, t5}, t6}); // a-b perfect
  test_if_equal(
    merge(
      point_dt{
        {t0, t1, t2},
        t3
  },
      point_dt{{t2, t4, t5}, t6}),
    point_dt{{t0, t1, t2, t4, t5}, t6}); // a b overlap
  test_if_equal(
    merge(
      point_dt{
        {t3, t4, t5},
        t6
  },
      point_dt{{t0, t1, t2}, t3}),
    point_dt{{t0, t1, t2, t3, t4, t5}, t6}); // a after b, perfect
  test_if_equal(
    merge(
      point_dt{
        {t3, t4, t5},
        t6
  },
      point_dt{{t0, t1, t2}, t10}),
    point_dt{{t0, t1, t2, t3, t4, t5}, t10}); // b-a-b-extend t_end
  test_if_equal(
    merge(
      point_dt{
        {t3, t4, t5},
        t6
  },
      point_dt{{t0, t1, t2, t6}, t10}),
    point_dt{{t0, t1, t2, t3, t4, t5, t6}, t10}); // b-a-b
  test_if_equal(
    merge(
      point_dt{
        {t3, t4, t5},
        t6
  },
      point_dt{{t3}, t10}),
    point_dt{{t3, t4, t5, t6}, t10}); // no points,just end
  test_if_equal(
    merge(
      point_dt{
        {t3, t4, t5},
        t6
  },
      point_dt{{t3}, t4}),
    point_dt{{t3, t4, t5, t6}, t10}); // b with no contrib
  CHECK_UNARY(!can_merge(
    point_dt{
      {t3, t4, t5},
      t6
  },
    point_dt{{t7}, t10})); // not mergable
  CHECK_UNARY(!can_merge(
    point_dt{
      {t3, t4, t5},
      t6
  },
    point_dt{})); // not mergable
}

TEST_CASE("ts/time_axis_merge_generic_dt") {
  using namespace shyft::time_axis;
  auto c = make_shared<calendar>("Europe/Oslo");
  utctime t0{seconds(0)}, t4x{4 * calendar::DAY};
  utctimespan dt{calendar::DAY};
  utctimespan dx{calendar::HOUR * 7};
  utctime t1{seconds(1)}, t2{seconds(2)}, t3{seconds(3)}, t4{seconds(4)}, t5{seconds(5)}, t6{seconds(6)};
  CHECK_EQ(
    merge(
      generic_dt{
        fixed_dt{t0, dt, 4}
  },
      generic_dt{fixed_dt{t4x, dt, 4}}),
    generic_dt{fixed_dt{t0, dt, 8}});
  test_if_equal(
    merge(
      generic_dt{
        point_dt{{t0, t1, t2}, t3}
  },
      generic_dt{point_dt{{t3, t4, t5}, t6}}),
    generic_dt{point_dt{{t0, t1, t2, t3, t4, t5}, t6}}); // a-b perfect
  CHECK_EQ(
    merge(
      generic_dt{
        calendar_dt{c, t0, dt, 4}
  },
      generic_dt{calendar_dt{c, t4x, dt, 4}}),
    generic_dt{calendar_dt{c, t0, dt, 8}});
  test_if_equal(
    merge(
      generic_dt{
        calendar_dt{c, t0, dt, 4}
  },
      generic_dt{fixed_dt{t4x, dt, 1}}),
    generic_dt{point_dt{{t0, t0 + dt, t0 + dt * 2, t0 + dt * 3, t0 + dt * 4}, t0 + dt * 5}});
  auto a = combine(
    generic_dt{
      fixed_dt{t0, dt, 2}
  },
    generic_dt{fixed_dt{t0, dx, 2}});
  auto b = generic_dt{
    vector<utctime>{t0, t0 + dx},
    t0 + 2 * dx
  };
  CHECK_EQ(a, b);
  // for(size_t i=0;i<b.size();++i)
  //    MESSAGE("t"<<i<<":"<<c->to_string(b.time(i))<<"=="<<c->to_string(a.time(i)));
}

TEST_CASE("ts/time_axis_combine_month_week") {
  using namespace shyft::time_axis;
  auto c = make_shared<calendar>();
  generic_dt a{c->time(2019, 1, 1), calendar::MONTH, 1};
  generic_dt b{c->time_from_week(2019, 1, 1), calendar::WEEK, 1};
  auto x = combine(a, b);
  generic_dt e{c->time(2019, 1, 1), calendar::DAY * 6, 1};
  CHECK_EQ(e, x);
  CHECK_EQ(e, combine(b, a));
}

TEST_CASE("ts/all") {
  // Verify that if all types of time-axis are setup up to have the same periods
  // they all have the same properties.
  // test-strategy: Have one fixed time-axis that the other should equal

  auto utc = make_shared<calendar>();
  utctime start = utc->time(YMDhms(2016, 3, 8));
  auto dt = deltahours(3);
  int n = 9 * 3;
  time_axis::fixed_dt expected(start, dt, n); // this is the simplest possible time axis
  //
  // STEP 0: verify that the expected time-axis is correct
  //
  TS_ASSERT_EQUALS(n, (int) expected.size());
  TS_ASSERT_EQUALS(utcperiod(start, start + n * dt), expected.total_period());
  TS_ASSERT_EQUALS(string::npos, expected.index_of(start - seconds(1)));
  TS_ASSERT_EQUALS(string::npos, expected.open_range_index_of(start - seconds(1)));
  TS_ASSERT_EQUALS(string::npos, expected.index_of(start + n * dt));
  TS_ASSERT_EQUALS(n - 1, (int) expected.open_range_index_of(start + n * dt));
  for (int i = 0; i < n; ++i) {
    TS_ASSERT_EQUALS(start + i * dt, expected.time(i));
    TS_ASSERT_EQUALS(utcperiod(start + i * dt, start + (i + 1) * dt), expected.period(i));
    TS_ASSERT_EQUALS(i, (int) expected.index_of(start + i * dt));
    TS_ASSERT_EQUALS(i, (int) expected.index_of(start + i * dt + dt - seconds(1)));
    TS_ASSERT_EQUALS(i, (int) expected.open_range_index_of(start + i * dt));
    TS_ASSERT_EQUALS(i, (int) expected.open_range_index_of(start + i * dt + dt - seconds(1)));
  }
  //
  // STEP 1: construct all the other types of time-axis, with equal content, but represented differently
  //
  time_axis::calendar_dt c_dt(utc, start, dt, n);
  vector<utctime> tp;
  for (int i = 0; i < n; ++i)
    tp.push_back(start + i * dt);
  time_axis::point_dt p_dt(tp, start + n * dt);
  vector<utcperiod> sub_period;
  //
  // STEP 2: Verify that all the other types are equal to the now verified correct expected time_axis
  //
  TS_ASSERT(test_if_equal(expected, c_dt));
  TS_ASSERT(test_if_equal(expected, p_dt));
  TS_ASSERT(test_if_equal(expected, time_axis::generic_dt(expected)));
  TS_ASSERT(test_if_equal(expected, time_axis::generic_dt(p_dt)));
  TS_ASSERT(test_if_equal(expected, time_axis::generic_dt(c_dt)));
  //
  // STEP 3: Verify the time_axis::combine algorithm when equal time-axis are combined
  //
  TS_ASSERT(test_if_equal(expected, time_axis::combine(expected, expected)));
  TS_ASSERT(test_if_equal(expected, time_axis::combine(c_dt, expected)));
  TS_ASSERT(test_if_equal(expected, time_axis::combine(c_dt, p_dt)));
  TS_ASSERT(test_if_equal(expected, time_axis::combine(c_dt, p_dt)));

  //
  // STEP 4: Verify the time_axis::combine algorithm for non-overlapping timeaxis(should give null-timeaxis)
  //
  time_axis::fixed_dt f_dt_null = time_axis::fixed_dt::null_range();
  time_axis::point_dt p_dt_x({start + n * dt, start + (n + 1) * dt}, start + (n + 2) * dt);
  TS_ASSERT(test_if_equal(f_dt_null, time_axis::combine(c_dt, p_dt_x)));
  TS_ASSERT(test_if_equal(f_dt_null, time_axis::combine(expected, p_dt_x)));
  TS_ASSERT(test_if_equal(f_dt_null, time_axis::combine(p_dt, p_dt_x)));


  //
  // STEP 5: Verify the time_axis::combine algorithm for overlapping time-axis
  //
  time_axis::fixed_dt overlap1(start + dt, dt, n);
  time_axis::fixed_dt expected_combine1(start + dt, dt, n - 1);
  TS_ASSERT(test_if_equal(expected_combine1, time_axis::combine(expected, overlap1)));
  TS_ASSERT(test_if_equal(expected_combine1, time_axis::combine(c_dt, overlap1)));
  TS_ASSERT(test_if_equal(expected_combine1, time_axis::combine(p_dt, overlap1)));
  TS_ASSERT(test_if_equal(expected_combine1, time_axis::combine(overlap1, time_axis::generic_dt(c_dt))));
  TS_ASSERT(test_if_equal(expected_combine1, time_axis::combine(overlap1, time_axis::generic_dt(p_dt))));
  TS_ASSERT(test_if_equal(expected_combine1, time_axis::combine(overlap1, time_axis::generic_dt(expected))));
}

TEST_CASE("ts/time_axis_extend") {

  namespace ta = shyft::time_axis;
  namespace core = shyft::core;

  core::calendar utc;

  SUBCASE("directly sequential fixed_dt") {
    core::utctime t0 = utc.time(2017, 1, 1);
    core::utctimespan dt = core::deltahours(1);
    size_t n = 512;

    ta::fixed_dt axis(t0, dt, 2 * n);
    ta::fixed_dt ext(t0 + 2 * n * dt, dt, 2 * n);

    ta::generic_dt res = ta::extend(axis, ext, t0 + 2 * n * dt);
    ta::fixed_dt expected(t0, dt, 4 * n);

    REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED);
    CHECK_EQ(res.f(), expected);
  }

  SUBCASE("fixed_dt with fixed_dt") {

    core::utctime t0 = utc.time(2017, 1, 1);
    core::utctimespan dt = deltahours(1);
    size_t n = 24u;

    ta::fixed_dt axis(t0, dt, n), empty = ta::fixed_dt::null_range();

    SUBCASE("empty time-axes") {
      SUBCASE("both empty") {
        ta::generic_dt res = ta::extend(empty, empty, empty.t + empty.dt * empty.n);
        REQUIRE_EQ(res.f(), empty);
      }
      SUBCASE("last empty") {
        SUBCASE("split after") {
          ta::generic_dt res = ta::extend(axis, empty, t0 + dt * n);
          REQUIRE_EQ(res.f(), axis);
        }
        SUBCASE("split inside") {
          size_t split_after = n / 2;
          ta::generic_dt res = ta::extend(axis, empty, t0 + dt * split_after);
          REQUIRE_EQ(res.f(), ta::fixed_dt(t0, dt, split_after));
        }
        SUBCASE("split before") {
          ta::generic_dt res = ta::extend(axis, empty, t0 - seconds(1));
          REQUIRE_EQ(res.f(), empty);
        }
      }
      SUBCASE("first empty") {
        SUBCASE("split after") {
          ta::generic_dt res = ta::extend(empty, axis, t0 + dt * n);
          REQUIRE_EQ(res.f(), empty);
        }
        SUBCASE("split inside") {
          size_t split_after = n / 2;
          ta::generic_dt res = ta::extend(empty, axis, t0 + dt * split_after);
          REQUIRE_EQ(res.f(), ta::fixed_dt(t0 + dt * split_after, dt, n - split_after));
        }
        SUBCASE("split before") {
          ta::generic_dt res = ta::extend(empty, axis, t0 - seconds(1));
          REQUIRE_EQ(res.f(), axis);
        }
      }
    }
    SUBCASE("aligned") {
      SUBCASE("rhs fully before lhs") { // branch 4
        ta::fixed_dt extension(t0 - 24u * dt, dt, 12u);
        ta::generic_dt res = ta::extend(axis, extension, t0 - 6 * dt);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::FIXED);
        CHECK_EQ(res.f(), empty);
      }
      SUBCASE("rhs start before lhs and end inside") { // branch 3
        ta::fixed_dt extension(t0 - 12u * dt, dt, n);
        ta::fixed_dt expected(t0, dt, 12u);

        ta::generic_dt res = ta::extend(axis, extension, t0);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::FIXED);
        CHECK_EQ(res.f(), expected);
      }
      SUBCASE("rhs start before and end after lhs") { // branch 1
        ta::fixed_dt extension(t0 - 12u * dt, dt, n + 24u);
        ta::fixed_dt expected(t0, dt, n + 12u);

        ta::generic_dt res = ta::extend(axis, extension, t0 + 12u * dt);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::FIXED);
        CHECK_EQ(res.f(), expected);
      }
      SUBCASE("rhs matches exactly lhs") { // branch 2
        ta::fixed_dt extension(t0, dt, n);
        ta::generic_dt res = ta::extend(axis, extension, t0 + n * dt);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::FIXED);
        CHECK_EQ(res.f(), axis);
      }
      SUBCASE("rhs start inside lhs and end after") { // branch 2
        ta::fixed_dt extension(t0 + (n / 2u) * dt, dt, n + 12u);
        ta::generic_dt res = ta::extend(axis, extension, t0 + (n / 2u + n + 12u) * dt);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::FIXED);
        CHECK_EQ(res.f(), axis);
      }
      SUBCASE("rhs start and end inside lhs") { // degenerate to point_dt
        ta::fixed_dt extension(t0 + 6u * dt, dt, 12u);
        ta::generic_dt res = ta::extend(axis, extension, t0 + 3u * dt);

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= 3u; ++i)
          expected_points.push_back(t0 + i * dt);
        for (size_t i = 0u; i <= 12u; ++i)
          expected_points.push_back(t0 + (i + 6u) * dt);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
      SUBCASE("rhs fully after lhs") { // degenerate to point_dt
        ta::fixed_dt extension(t0 + (n + 2) * dt, dt, 12u);
        ta::generic_dt res = ta::extend(axis, extension, t0 + (n + 1) * dt);

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(t0 + i * dt);
        for (size_t i = 0u; i <= 12u; ++i)
          expected_points.push_back(t0 + (i + n + 2) * dt);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
    }
    SUBCASE("unaligned") {
      SUBCASE("equal dt, unaligned boundaries") {
        utctime t0_ext = t0 + deltaminutes(30);

        ta::fixed_dt extension(t0_ext, dt, 2 * n);
        ta::generic_dt res = ta::extend(axis, extension, t0 + n * dt);

        // construct time points
        std::vector<utctime> expected_points;
        for (utctime t = t0; t <= t0 + utctimespan(dt * n); t += dt)
          expected_points.push_back(t);
        for (utctime t = t0_ext + n * dt; t <= t0_ext + utctimespan(2 * n * dt); t += dt)
          expected_points.push_back(t);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
      SUBCASE("unequal dt, aligned boundaries") {
        utctimespan dt_ext = deltaminutes(30);
        size_t n_ext = 4 * n;

        ta::fixed_dt extension(t0, dt_ext, n_ext);
        ta::generic_dt res = ta::extend(axis, extension, t0 + dt * n);

        // construct time points
        std::vector<utctime> expected_points;
        for (utctime t = t0; t <= t0 + utctimespan(dt * n); t += dt)
          expected_points.push_back(t);

        for (utctime t = t0 + n * dt + dt_ext; t <= t0 + utctimespan(4 * n * dt_ext); t += dt_ext)
          expected_points.push_back(t);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
    }
  }
  SUBCASE("calendar_dt with calendar_dt") {

    std::shared_ptr<core::calendar> cal = std::make_shared<core::calendar>(utc);
    core::utctime t0 = cal->time(2017, 4, 1);
    core::utctime t0x{seconds(0)};
    core::utctimespan dt = core::calendar::DAY;
    size_t n = 30;

    ta::calendar_dt axis(cal, t0, dt, n), empty = ta::calendar_dt::null_range();

    SUBCASE("empty time-axes") {
      SUBCASE("both empty") {
        ta::generic_dt res = ta::extend(empty, empty, t0x);

        REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED); // Because we simplify extensions to fixed
        REQUIRE_EQ(res.f(), ta::fixed_dt::null_range());
      }
      SUBCASE("last empty") {
        SUBCASE("split after") {
          ta::generic_dt res = ta::extend(axis, empty, cal->add(t0, dt, n + 1));

          REQUIRE_EQ(res.gt(), ta::generic_dt::CALENDAR);
          REQUIRE_EQ(res.c(), axis);
        }
        SUBCASE("split inside") {
          size_t split_after = n / 2; // 15
          ta::calendar_dt expected(cal, t0, dt, split_after);
          ta::generic_dt res = ta::extend(axis, empty, cal->add(t0, dt, split_after));

          REQUIRE_EQ(res.gt(), ta::generic_dt::CALENDAR);
          REQUIRE_EQ(res.c(), expected);
        }
        SUBCASE("split before") {
          ta::generic_dt res = ta::extend(axis, empty, cal->add(t0, dt, -1));

          REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED);
          REQUIRE_EQ(res.f(), ta::fixed_dt::null_range());
        }
      }
      SUBCASE("first empty") {
        SUBCASE("split after") {
          ta::generic_dt res = ta::extend(empty, axis, cal->add(t0, dt, n + 1));

          REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED);
          REQUIRE_EQ(res.f(), ta::fixed_dt::null_range());
        }
        SUBCASE("split inside") {
          size_t split_after = n / 2; // 15

          ta::calendar_dt expected(cal, cal->time(2017, 4, split_after + 1), dt, n - split_after);
          ta::generic_dt res = ta::extend(empty, axis, cal->add(t0, dt, split_after));

          REQUIRE_EQ(res.gt(), ta::generic_dt::CALENDAR);
          CHECK_EQ(res.c(), expected);
        }
        SUBCASE("split before") {
          ta::generic_dt res = ta::extend(empty, axis, cal->add(t0, dt, -1));

          REQUIRE_EQ(res.gt(), ta::generic_dt::CALENDAR);
          REQUIRE_EQ(res.c(), axis);
        }
      }
    }
    SUBCASE("aligned") {
      SUBCASE("rhs fully before lhs") { // branch 4
        ta::calendar_dt extension(cal, cal->add(t0, dt, -15), dt, 10);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, -5));

        REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED);
        REQUIRE_EQ(res.f(), ta::fixed_dt::null_range());
      }
      SUBCASE("rhs start before lhs and end inside") { // branch 3
        ta::calendar_dt extension(cal, cal->add(t0, dt, -15), dt, n);
        ta::calendar_dt expected(cal, t0, dt, 15u);

        ta::generic_dt res = ta::extend(axis, extension, t0);

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::CALENDAR);
        CHECK_EQ(res.c(), expected);
      }
      SUBCASE("rhs start before and end after lhs") { // branch 1
        ta::calendar_dt extension(cal, cal->add(t0, dt, -10), dt, n + 20);
        ta::calendar_dt expected(cal, t0, dt, n + 10u);

        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, 15));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::CALENDAR);
        CHECK_EQ(res.c(), expected);
      }
      SUBCASE("rhs matches exactly lhs") { // branch 2
        ta::calendar_dt extension(cal, t0, dt, n);

        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, n));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::CALENDAR);
        CHECK_EQ(res.c(), axis);
      }
      SUBCASE("rhs start inside lhs and end after") { // branch 2
        ta::calendar_dt extension(cal, cal->add(t0, dt, 15), dt, n);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, n + 15));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::CALENDAR);
        CHECK_EQ(res.c(), axis);
      }
      SUBCASE("rhs start and end inside lhs") { // degenerate to point_dt
        ta::calendar_dt extension(cal, cal->add(t0, dt, 10), dt, n - 20);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, 5));

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= 5u; ++i)
          expected_points.push_back(cal->add(t0, dt, i));
        for (size_t i = 0u; i <= 10u; ++i)
          expected_points.push_back(cal->add(t0, dt, 10 + i));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
      SUBCASE("rhs fully after lhs") { // degenerate to point_dt
        ta::calendar_dt extension(cal, cal->add(t0, dt, n + 10), dt, n);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, n + 5));

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(cal->add(t0, dt, i));
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(cal->add(t0, dt, n + 10 + i));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
    }
    SUBCASE("unaligned") {
      SUBCASE("equal dt, unaligned boundaries") {
        utctime t0_ext = cal->add(t0, core::calendar::HOUR, 12);

        ta::calendar_dt extension(cal, t0_ext, dt, 2 * n);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, n));

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(cal->add(t0, dt, i));
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(cal->add(t0_ext, dt, n + i));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
      SUBCASE("unequal dt, aligned boundaries") {
        core::utctimespan dt_ext = core::calendar::HOUR;
        size_t n_ext = 2 * 24 * n;

        ta::calendar_dt extension(cal, t0, dt_ext, n_ext);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, n));

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(cal->add(t0, dt, i));
        //
        core::utctime end = cal->add(t0, dt, n);
        for (size_t i = 1u; i <= n_ext / 2; ++i)
          expected_points.push_back(cal->add(end, dt_ext, i));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
      SUBCASE("different calendars") {
        std::shared_ptr<core::calendar> other_cal = std::make_shared<core::calendar>(2 * core::calendar::HOUR);

        ta::calendar_dt extension(other_cal, t0, dt, n);
        ta::generic_dt res = ta::extend(axis, extension, cal->add(t0, dt, n / 2));

        // construct time points
        std::vector<utctime> expected_points;
        for (size_t i = 0u; i <= n; ++i)
          expected_points.push_back(cal->add(t0, dt, i));

        REQUIRE_EQ(res.gt(), ta::generic_dt::generic_type::POINT);
        CHECK_EQ(res.p(), ta::point_dt(expected_points));
      }
    }
  }
  SUBCASE("continuous with different continuous") {

    std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>(utc);

    core::utctime t0 = utc.time(2017, 1, 1);
    core::utctime t0x{seconds(0)};
    core::utctimespan dt_30m = 30 * core::calendar::MINUTE;
    core::utctimespan dt_h = core::calendar::HOUR;
    size_t n = 50;

    SUBCASE("empty cases") {

      ta::fixed_dt empty_fdt = ta::fixed_dt::null_range();
      ta::point_dt empty_pdt = ta::point_dt::null_range();
      ta::calendar_dt non_empty(utc_ptr, t0, dt_h, n);

      SUBCASE("empty with empty") {
        ta::generic_dt res = ta::extend(empty_fdt, empty_pdt, t0x);
        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p().size(), 0);
      }
      SUBCASE("empty with non-empty (split_at == middle of non_empty)") {
        ta::calendar_dt expected(utc_ptr, utc.add(t0, dt_h, n / 2), dt_h, n / 2);

        ta::generic_dt res = ta::extend(empty_fdt, non_empty, utc.add(t0, dt_h, n / 2));

        REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED);
        CHECK_EQ(res.f(), expected.simplify());
      }
      SUBCASE("non-empty with empty (split_at == non_empty.end)") {
        ta::generic_dt res = ta::extend(non_empty, empty_pdt, utc.add(t0, dt_h, n));

        REQUIRE_EQ(res.gt(), ta::generic_dt::FIXED);
        CHECK_EQ(res.f(), non_empty.simplify());
      }
    }
    SUBCASE("non-empty cases") {
      SUBCASE("fully before (split between)") {
        ta::fixed_dt ax_fdt(t0, dt_h, n);
        ta::calendar_dt ext_cdt(utc_ptr, utc.add(t0, dt_h, -2 * ((long) n)), dt_h, n);

        ta::generic_dt res = ta::extend(ax_fdt, ext_cdt, utc.add(t0, dt_h, -(long) n / 2));

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), ta::point_dt::null_range());
      }
      SUBCASE("overlapping starting before, ending inside (split at axis start)") {
        std::vector<core::utctime> ext_points;
        core::utctime t0_ext = utc.add(t0, dt_30m, -(long) n / 2);
        for (size_t i = 0; i <= n; ++i) {
          ext_points.push_back(utc.add(t0_ext, dt_30m, i));
        }

        ta::calendar_dt ax_cdt(utc_ptr, t0, dt_h, n);
        ta::point_dt ext_pdt(ext_points);

        ta::generic_dt res = ta::extend(ax_cdt, ext_pdt, t0);

        std::vector<core::utctime> exp_points;
        for (size_t i = 0; i <= n / 2; ++i) {
          exp_points.push_back(utc.add(t0, dt_30m, i));
        }
        ta::point_dt expected_pdt(exp_points);

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), expected_pdt);
      }
      SUBCASE("overlapping starting before, ending after (split at middle of axis)") {
        std::vector<core::utctime> ax_points;
        for (size_t i = 0; i <= n; ++i) {
          ax_points.push_back(t0 + i * dt_h);
        }

        ta::point_dt ax_pdt(ax_points);
        ta::fixed_dt ext_fdt(t0 - n * dt_h / 2, dt_h, 2 * n);

        ta::generic_dt res = extend(ax_pdt, ext_fdt, t0 + n * dt_h / 2);

        std::vector<core::utctime> exp_points;
        for (size_t i = 0; i <= 3 * n / 2; ++i) {
          exp_points.push_back(utc.add(t0, dt_h, i));
        }
        ta::point_dt expected_pdt(exp_points);

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), expected_pdt);
      }
      SUBCASE("overlapping exactly (split in the middle)") {
        std::vector<core::utctime> ext_points;
        for (size_t i = 0; i <= 2 * n; ++i) {
          ext_points.push_back(t0 + i * dt_h / 2);
        }

        ta::fixed_dt ax_fdt(t0, dt_h, n);
        ta::point_dt ext_pdt(ext_points);

        ta::generic_dt res = ta::extend(ax_fdt, ext_pdt, t0 + n * dt_h / 2);

        std::vector<core::utctime> exp_points;
        for (size_t i = 0; i <= n / 2; ++i) {
          exp_points.push_back(t0 + i * dt_h);
        }
        for (size_t i = 1; i <= n; ++i) {
          exp_points.push_back((t0 + n * dt_h / 2) + i * dt_h / 2);
        }
        ta::point_dt expected_pdt(exp_points);

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), expected_pdt);
      }
      SUBCASE("overlapping fully inside (split mid between axis start and extend start)") {
        ta::calendar_dt ax_cdt(utc_ptr, t0, dt_h, n);
        ta::fixed_dt ext_fdt(utc.add(t0, dt_h, n / 5), dt_h, 3 * n / 5);

        ta::generic_dt res = ta::extend(ax_cdt, ext_fdt, utc.add(t0, dt_h, n / 10));

        std::vector<core::utctime> exp_points;
        for (size_t i = 0; i <= n / 10; ++i) {
          exp_points.push_back(utc.add(t0, dt_h, i));
        }
        for (size_t i = 0; i <= 3 * n / 5; ++i) {
          exp_points.push_back(utc.add(t0, dt_h, n / 5) + i * dt_h);
        }
        ta::point_dt expected(exp_points);

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), expected);
      }
      SUBCASE("overlapping starting inside, ending after (split at end of extend)") {
        std::vector<core::utctime> ax_points;
        for (size_t i = 0; i <= n; ++i) {
          ax_points.push_back(utc.add(t0, dt_h, i));
        }

        ta::point_dt ax_pdt(ax_points);
        ta::calendar_dt ext_cdt(utc_ptr, utc.add(t0, dt_h, n / 2), dt_h, n);

        ta::generic_dt res = ta::extend(ax_pdt, ext_cdt, utc.add(t0, dt_h, 3 * n / 2));

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), ax_pdt);
      }
      SUBCASE("fully after (split before axis)") {
        std::vector<core::utctime> ax_points;
        for (size_t i = 0; i <= n; ++i) {
          ax_points.push_back(t0 + i * dt_h);
        }

        std::vector<core::utctime> ext_points;
        for (size_t i = 0; i <= n; ++i) {
          ext_points.push_back((t0 + 2 * n * dt_h) + i * dt_30m);
        }

        ta::point_dt ax_pdt(ax_points);
        ta::point_dt ext_pdt(ext_points);

        ta::generic_dt res = ta::extend(ax_pdt, ext_pdt, t0 - n * dt_h);

        REQUIRE_EQ(res.gt(), ta::generic_dt::POINT);
        CHECK_EQ(res.p(), ext_pdt);
      }
    }
  }
}

TEST_CASE("ts/ta_extend_fixed_with_point_overlap") {
  namespace ta = shyft::time_axis;
  auto t0 = seconds(0);
  auto dt = seconds(1);

  ta::generic_dt a(t0, dt, 2);
  ta::generic_dt b(vector<utctime>{from_seconds(1.5), from_seconds(2.5)}, from_seconds(3.5));
  auto r = ta::extend(a, b, from_seconds(1.5));
  ta::generic_dt expected(
    vector<utctime>{from_seconds(0), from_seconds(1), from_seconds(1.5), from_seconds(2.5)}, from_seconds(3.5));

  CHECK_EQ(r, expected);
}

TEST_CASE("ts/time_shift") {
  calendar utc;
  utctime t0 = utc.time(2015, 1, 1);
  utctime t1 = utc.time(2016, 1, 1);
  auto dt = deltahours(1);
  size_t n = 24;
  time_axis::fixed_dt ta0(t0, dt, n);
  time_axis::fixed_dt ta1(time_shift(ta0, t1 - t0));
  TS_ASSERT(test_if_equal(time_axis::fixed_dt(t1, dt, n), ta1));
}

TEST_CASE("ts/time_axis_f_slice") {
  time_axis::fixed_dt a(utctime{seconds(0)}, seconds(1), 10);
  auto b = a.slice(1, 2);
  CHECK_EQ(b.time(0), utctime{seconds(1)});
  CHECK_EQ(b.time(1), utctime{seconds(2)});
  CHECK_EQ(b.size(), 2);
}

TEST_CASE("ts/time_axis_c_slice") {
  time_axis::calendar_dt a(make_shared<calendar>(), utctime{seconds(0)}, seconds(1), 10);
  auto b = a.slice(1, 2);
  CHECK_EQ(b.time(0), utctime{seconds(1)});
  CHECK_EQ(b.time(1), utctime{seconds(2)});
  CHECK_EQ(b.size(), 2);
}

TEST_CASE("ts/time_axis_p_slice") {
  vector<utctime> tp;
  for (int64_t t = 0; t < 10; ++t)
    tp.push_back(_t(t));
  time_axis::point_dt a(tp, _t(10));
  auto b = a.slice(1, 2);
  CHECK_EQ(b.time(0), utctime{seconds(1)});
  CHECK_EQ(b.time(1), utctime{seconds(2)});
  CHECK_EQ(b.size(), 2);
  auto c = a.slice(8, 2);
  CHECK_EQ(c.size(), 2);
  CHECK_EQ(c.time(0), utctime{seconds(8)});
  CHECK_EQ(c.time(1), utctime{seconds(9)});
}

TEST_CASE("ts/time_axis_g_slice") {
  /*fixed */ {
    time_axis::generic_dt a(time_axis::fixed_dt(_t(0), seconds(1), 10));
    auto b = a.slice(1, 2);
    CHECK_EQ(b.time(0), _t(1));
    CHECK_EQ(b.time(1), _t(2));
    CHECK_EQ(b.size(), 2);
  }
  /* calendar*/ {
    time_axis::generic_dt a(time_axis::calendar_dt(make_shared<calendar>(), _t(0), seconds(1), 10));
    auto b = a.slice(1, 2);
    CHECK_EQ(b.time(0), _t(1));
    CHECK_EQ(b.time(1), _t(2));
    CHECK_EQ(b.size(), 2);
  }
  /* point */ {
    time_axis::generic_dt a(time_axis::point_dt(
      vector<utctime>{_t(0), _t(1), _t(2), _t(3), _t(4), _t(5), _t(6), _t(7), _t(8), _t(9)}, _t(10)));
    auto b = a.slice(1, 2);
    CHECK_EQ(b.time(0), _t(1));
    CHECK_EQ(b.time(1), _t(2));
    CHECK_EQ(b.size(), 2);
    auto c = a.slice(8, 2);
    CHECK_EQ(c.size(), 2);
    CHECK_EQ(c.time(0), _t(8));
    CHECK_EQ(c.time(1), _t(9));
  }
}

TEST_CASE("ts/ct_point_dt") {
  CHECK_THROWS_AS(time_axis::point_dt(vector<utctime>{_t(0)}, _t(0)), std::runtime_error);
  CHECK_THROWS_AS(time_axis::point_dt(vector<utctime>{_t(0), _t(0)}), std::runtime_error);
}
TEST_CASE("ts/ct_calendar_dt") {
  time_axis::calendar_dt a;
  time_axis::calendar_dt b;
  CHECK_EQ(a,b);
  CHECK_EQ(a.cal->get_tz_name(),"UTC");
  a.cal=nullptr;// avoid hitting core dump here..
  CHECK_NE(a,b);
}
TEST_CASE("ts/ct_generic_w_a_tweak") {
  time_axis::generic_dt a;
  CHECK_EQ(a.gt(),time_axis::generic_dt::FIXED);
  a.set_gt(time_axis::generic_dt::CALENDAR);
  CHECK_EQ(a.gt(),time_axis::generic_dt::CALENDAR);
  CHECK_EQ(a.c().cal->get_tz_name(),"UTC");
}
TEST_CASE("ts/time_axis/empty"){
  SUBCASE("ct") {
    time_axis::fixed_dt f;
    time_axis::calendar_dt c;
    time_axis::point_dt p;
    time_axis::generic_dt g;
    CHECK(f.empty());
    CHECK(c.empty());
    CHECK(p.empty());
    CHECK(g.empty());
    CHECK_EQ(f.index_of(utctime{4}),std::string::npos);
    CHECK_EQ(c.index_of(utctime{4}),std::string::npos);
    CHECK_EQ(p.index_of(utctime{4}),std::string::npos);
    CHECK_EQ(g.index_of(utctime{4}),std::string::npos);
  }
  SUBCASE("ct_n_0") {
    time_axis::fixed_dt f{utctime{0},utctime{10},0};
    time_axis::calendar_dt c{calendar::utc(),utctime{0},utctime{10},0};
    time_axis::point_dt p{{}};
    time_axis::generic_dt g{c};
    CHECK(f.empty());
    CHECK(c.empty());
    CHECK(p.empty());
    CHECK(g.empty());
  }
}
TEST_CASE("ts/is_fixed_dt") {
  auto is_fixed = [](time_axis::generic_dt const & ta, utctime dt) {
    switch (ta.gt()) {
    case time_axis::generic_dt::POINT:
      return false;
    case time_axis::generic_dt::FIXED:
      return ta.f().delta() == dt;
    case time_axis::generic_dt::CALENDAR:
      return ta.c().dt == dt;
    }
    return false;
  };
  utctime t0 = seconds(0);
  utctime dt = seconds(3600);
  auto utc = make_shared<calendar>();
  vector<utctime> tp{seconds(0), seconds(3600), seconds(7200)};
  time_axis::generic_dt a(t0, dt, 10);
  time_axis::generic_dt b(utc, t0, dt, 10);
  time_axis::generic_dt c(tp);
  CHECK_EQ(is_fixed(a, seconds(3600)), true);
  CHECK_EQ(is_fixed(b, seconds(3600)), true);
  CHECK_EQ(is_fixed(a, seconds(3601)), false);
  CHECK_EQ(is_fixed(c, seconds(3600)), false);
}

TEST_CASE("ts/make_dt_axis") {
  // given ta and dt (assume ta.dt != dt), create a new time-axis
  // where the resulting time-axis starts at floor(total_period.start,dt)
  // and the last interval covers the original time-axis.
  auto make_dt_axis = [](time_axis::generic_dt const & ta, utctimespan dt) -> time_axis::generic_dt {
    auto t0 = floor(ta.total_period().start, dt);
    auto tn = floor(ta.total_period().end + dt - utctime(1ul), dt);
    auto n = static_cast<size_t>((tn - t0) / dt);

    return time_axis::generic_dt(t0, dt, n);
  };
  utctime t0 = seconds(0);
  utctime dt = seconds(3600);
  auto utc = make_shared<calendar>();
  time_axis::generic_dt a(t0, dt / 4, 24 * 4);
  auto b = make_dt_axis(a, seconds(3600));
  time_axis::generic_dt e(t0, seconds(3600), 24);
  CHECK_EQ(b, e);
  time_axis::generic_dt a2(t0 + seconds(1), dt / 4, 24 * 4 - 1);
  auto b2 = make_dt_axis(a, seconds(3600));
  CHECK_EQ(b2, e);
  time_axis::generic_dt a3(t0, dt * 24, 1);
  auto b3 = make_dt_axis(a, seconds(3600));
  CHECK_EQ(b3, e);
}

TEST_CASE("ts/point_dt_index_of_w_hint") {
  time_axis::point_dt ta{
    vector<utctime>{
                    utctime(0),
                    utctime(1),
                    utctime(2),
                    utctime(3),
                    utctime(4),
                    utctime(5),
                    utctime(6),
                    utctime(7),
                    utctime(8),
                    utctime(9),
                    utctime(10)},
    utctime(12)
  };
  CHECK_EQ(ta.index_of(utctime(0), 0), 0);
  CHECK_EQ(ta.index_of(utctime(0), 3), 0);
  CHECK_EQ(ta.index_of(utctime(0), 11), 0);
  CHECK_EQ(ta.index_of(utctime(1), 0), 1);
  CHECK_EQ(ta.index_of(utctime(2), 0), 2);
  CHECK_EQ(ta.index_of(utctime(3), 0), 3);
  CHECK_EQ(ta.index_of(utctime(3), 3), 3);
  CHECK_EQ(ta.index_of(utctime(11), 0), 10);
  CHECK_EQ(ta.index_of(utctime(11), 5), 10);
}

TEST_SUITE_END();
