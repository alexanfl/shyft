#include "test_pch.h"

#include <cstdint>
#include <vector>

#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/ice_packing_recession_ts.h>
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/time_series/dd/transform_spline_ts.h>

namespace shyft::time_series::dd {

  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/null_ts/equal") {
    const core::utctime t0{0}, dt{1};
    const std::size_t n = 4;
    apoint_ts a(
      time_axis::generic_dt{t0, dt, n}, std::vector{-2.0, 1.0, 2.0, shyft::nan}, time_series::POINT_AVERAGE_VALUE);
    apoint_ts b,c;
    CHECK( b==b);
    CHECK( b==c);
    CHECK( b!=a);
  }
  namespace {
    template <class Op>
    void verify_binop_throws_on_null(Op&& op) {
      const core::utctime t0{0}, dt{1};
      time_axis::generic_dt ta{t0, dt, 4};
      apoint_ts a(ta, std::vector{-2.0, 1.0, 2.0, shyft::nan}, time_series::POINT_AVERAGE_VALUE);
      apoint_ts n;
      double d{3.0};
      CHECK_THROWS_AS(op(n,a),std::runtime_error);
      CHECK_THROWS_AS(op(a,n),std::runtime_error);
      CHECK_THROWS_AS(op(n,d),std::runtime_error);
      CHECK_THROWS_AS(op(d,n),std::runtime_error);
    }
  }
  TEST_CASE("ts/null_ts/expr_throws/bin_op") {
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return a-b;});
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return a+b;});
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return a/b;});
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return a*b;});
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return max(a,b);});
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return min(a,b);});
    verify_binop_throws_on_null([](auto const&a, auto const&b)->apoint_ts{ return pow(a,b);});
  }
  TEST_CASE("ts/null_ts/expr_throws/fx") {
    time_axis::generic_dt ta{utctime{0},utctime{1}, 4};
    apoint_ts n;
    CHECK_THROWS_AS(n.average(ta),std::runtime_error);
    CHECK_THROWS_AS(n.integral(ta),std::runtime_error);
    CHECK_THROWS_AS(n.accumulate(ta),std::runtime_error);
    CHECK_THROWS_AS(n.time_shift(utctime{30000}),std::runtime_error);
    CHECK_THROWS_AS(n.derivative(),std::runtime_error);
    CHECK_THROWS_AS(n.log(),std::runtime_error);
    CHECK_THROWS_AS(n.statistics(ta,50u),std::runtime_error);
    CHECK_THROWS_AS(n.abs(),std::runtime_error);
    CHECK_THROWS_AS(n.stack_ts(calendar{},utctime{0},1u,utctime{0},2u,utctime{0},utctime{0}),std::runtime_error);
    CHECK_THROWS_AS(n.convolve_w(std::vector<double>{1.0,2.0,3.0},convolve_policy::CENTER),std::runtime_error);
    CHECK_THROWS_AS(n.rating_curve({}),std::runtime_error);
    CHECK_THROWS_AS(n.ice_packing({},ice_packing_temperature_policy::ALLOW_INITIAL_MISSING),std::runtime_error);
    CHECK_THROWS_AS(n.ice_packing_recession(n,{}),std::runtime_error);
    CHECK_THROWS_AS(n.bucket_to_hourly(1,230.0),std::runtime_error);
    CHECK_THROWS_AS(n.krls_interpolation(utctime{},1.0,1.0,10u),std::runtime_error);
    CHECK_THROWS_AS(n.min_max_check_linear_fill(1.0,10.0,utctime{1}),std::runtime_error);
    CHECK_THROWS_AS(n.quality_and_self_correction({}),std::runtime_error);
    CHECK_THROWS_AS(n.quality_and_ts_correction({},n),std::runtime_error);
    CHECK_THROWS_AS(n.inside(1.0,10.0,3.0,1.0,0.0),std::runtime_error);
    CHECK_THROWS_AS(n.transform_spline({}),std::runtime_error);
    CHECK_THROWS_AS(n.transform_spline(std::vector<double>{1.0,2.0},std::vector<double>{3.0,4.0},12),std::runtime_error);
    CHECK_THROWS_AS(n.transform({},interpolation_scheme::SCHEME_LINEAR),std::runtime_error);
    CHECK_THROWS_AS(n.decode(0,1),std::runtime_error);
    CHECK_THROWS_AS(n.use_time_axis_from(n),std::runtime_error);
    CHECK_THROWS_AS(n.repeat(ta),std::runtime_error);
    CHECK_THROWS_AS(n.compress(1.0),std::runtime_error);
    CHECK_THROWS_AS(n.compress_size(1.0),std::runtime_error);
    CHECK_THROWS_AS(n.bind(n),std::runtime_error);
  }
  TEST_SUITE_END();

}
