#include "test_pch.h"
#include <string>
#include <mutex>
#include <queue>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/queue.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>


using namespace shyft::dtss;
using namespace shyft::dtss::queue;
using namespace shyft::core;
using namespace shyft;
using namespace shyft::time_series::dd;

TEST_SUITE_BEGIN("dtss");

TEST_CASE("dtss/queue_basics") {
  tsv_queue q;
  q.name = "q1";
  CHECK_EQ(q.size(), 0u);
  CHECK_EQ(q.name, "q1");
  CHECK(q.try_get() == nullptr);
  CHECK(q.flush_done_items(false) == 0);
  auto t_now = utctime_now();
  ats_vector tsv;
  auto ttl = from_seconds(10.0); // need to allow some room here
  auto msg_id = "a";
  auto descr = "{}";
  q.put(msg_id, descr, ttl, tsv);
  CHECK_THROWS_AS(q.put(msg_id, "unique ids are required", ttl, tsv), std::runtime_error const &);
  CHECK_THROWS_AS(q.done(msg_id, "not allowed, need to fetch first"), std::runtime_error const &);
  CHECK_THROWS_AS(q.get_msg_info("no-msg-id"), std::runtime_error const &);
  CHECK(q.size() == 1u);
  auto qi = q.get_msg_infos();
  CHECK(qi.size() == 1u);
  CHECK(qi.front().msg_id == msg_id);
  CHECK((qi.front().created - t_now) < from_seconds(10.0));
  CHECK(qi.front().ttl == ttl);
  auto m1 = q.try_get();
  CHECK(m1 != nullptr);
  CHECK_EQ((m1->info.fetched - t_now) < from_seconds(10.0) && m1->info.fetched >= t_now, true);
  CHECK(q.size() == 0u);                 // because element is out of the queue.
  CHECK(q.flush_done_items(false) == 0); // ensure we can not remove elements that are only fetched
  string diag{"OK"};
  q.done(msg_id, diag);                                                      // now q element is done.
  auto mi = q.get_msg_info(msg_id);                                          // verify!
  CHECK_EQ(true, mi.done >= t_now && mi.done <= t_now + from_seconds(10.0)); //
  CHECK_EQ(mi.diagnostics, diag);
  CHECK_EQ(0u, q.flush_done_items(true)); // keep items with ttl

  CHECK_EQ(1u, q.flush_done_items(false)); // get rid of done items
}

TEST_CASE("dtss/q_mgr_basics") {
  q_mgr m;
  CHECK_EQ(m.size(), 0u);
  CHECK_EQ(m.queue_names().size(), 0u);
  m.add("a");
  CHECK_EQ(m.size(), 1u);
  CHECK_EQ(m.queue_names(), vector<string>{"a"});
  auto a = m("a");
  REQUIRE(a != nullptr);
  CHECK_EQ(a->name, "a");
  CHECK_THROWS(m.add("a"));
  m.add("b");
  auto q = m.queue_names();
  REQUIRE_EQ(q.size(), 2u);
  CHECK_EQ(q, vector<string>{"a", "b"});
  CHECK_THROWS(m.remove("c"));
  CHECK_THROWS(m("c"));
  auto b = m("b");
  REQUIRE(b != nullptr);
  CHECK_EQ(b->name, "b");
  m.remove("a");
  CHECK_EQ(m.size(), 1u);
  CHECK_THROWS(m("a")); // because it's removed
  b = m("b");
  REQUIRE(b != nullptr);
  m.remove("b");
  CHECK(m.size() == 0);
}

TEST_CASE("dtss/queue_srv_client") {
  server s;
  string ip{"127.0.0.1"};
  s.set_listening_ip(ip);
  auto port = s.start_server();
  auto host_port = ip + ":" + std::to_string(port);
  client c{host_port};
  auto q_names = c.q_list();
  CHECK_EQ(q_names.size(), 0);
  string q1_name{"q1"};
  c.q_add(q1_name);
  q_names = c.q_list();
  CHECK_EQ(q_names, vector<string>{q1_name});
  auto q_msgs = c.q_msg_infos(q1_name);
  CHECK_EQ(q_msgs.size(), 0);
  CHECK_EQ(c.q_size(q1_name), 0);
  ats_vector tsv;
  time_axis::generic_dt ta{from_seconds(0.0), from_seconds(10), 10};
  tsv.push_back(apoint_ts(ta, 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE));
  string descript{"{'plan_change':'activate now'}"};
  string msg_id1{"m1"};
  string msg_id2{"m2"};
  auto ttl = from_seconds(0.2);
  c.q_put(q1_name, msg_id1, descript, ttl, tsv);
  CHECK_EQ(c.q_size(q1_name), 1u);
  c.q_put(q1_name, msg_id2, descript, ttl, tsv);
  CHECK_EQ(c.q_size(q1_name), 2u);
  q_msgs = c.q_msg_infos(q1_name);
  CHECK_EQ(q_msgs.size(), 2);
  auto m1_info = c.q_msg_info(q1_name, msg_id1);
  CHECK_EQ(m1_info.msg_id, msg_id1);
  auto m2_info = c.q_msg_info(q1_name, msg_id2);
  CHECK_EQ(m2_info.msg_id, msg_id2);
  auto m1 = c.q_get(q1_name, from_seconds(0));
  CHECK(m1 != nullptr);
  CHECK_EQ(c.q_size(q1_name), 1u);
  CHECK_EQ(m1->tsv, tsv);
  CHECK_EQ(m1->info.msg_id, msg_id1);
  CHECK_EQ(m1->info.description, descript);
  c.q_ack(q1_name, m1->info.msg_id, "OK");
  CHECK_EQ(c.q_size(q1_name), 1u);
  auto m2 = c.q_get(q1_name, from_seconds(0));
  CHECK(m2 != nullptr);
  CHECK_EQ(c.q_size(q1_name), 0u);
  c.q_ack(q1_name, m2->info.msg_id, "OK");
  auto m3 = c.q_get(q1_name, from_seconds(0.010));
  CHECK(m3 == nullptr);
  q_msgs = c.q_msg_infos(q1_name);
  for (auto const &qi : q_msgs)
    CHECK(qi.diagnostics == "OK");
  // auto x_now=utctime_now()-from_seconds(3);// make it the past
  c.q_maintain(q1_name, false);

  auto q_x = c.q_msg_infos(q1_name);
  CHECK(q_x.size() == 0u);
  c.q_remove(q1_name);
  CHECK(c.q_list().size() == 0u); // no more queues left

  c.close(100);
}

TEST_SUITE_END();
