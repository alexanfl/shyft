#include "test_pch.h"
#include <shyft/dtss/dtss_db.h>
#include <cstdio>
#include <test/test_utils.h>
#include <shyft/dtss/dtss_db_time_io.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>

using namespace shyft::dtss;
using namespace shyft::core;
using namespace shyft;
using shyft::time_series::POINT_AVERAGE_VALUE;
using std::exception;
using std::fopen;
using std::fclose;

static inline shyft::core::utctime _t(int64_t t1970s) {
  return shyft::core::utctime{shyft::core::seconds(t1970s)};
}

namespace {
  template <class T>
  static void test_time_io(char const *nm) {
    test::utils::temp_dir fpath(nm);
    string cp = fpath.string();
    auto *fp = fopen(cp.c_str(), "wb");
    vector<utctime> tv;
    tv.push_back(seconds(1));
    tv.push_back(seconds(2));
    utctime t{seconds(4)};
    T::write(fp, tv);
    T::write(fp, t);
    fclose(fp);
    fp = fopen(cp.c_str(), "rb");
    vector<utctime> tvr;
    tvr.resize(tv.size());
    utctime tr;
    T::read(fp, tvr);
    T::read(fp, tr);
    fclose(fp);
    CHECK_EQ(tv, tvr);
    CHECK_EQ(t, tr);
  }
}

void test_dtss_db_basics(bool time_in_micro_seconds) {
  using namespace shyft::dtss;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;

  std::shared_ptr<core::calendar> utc = std::make_shared<core::calendar>();
  std::shared_ptr<core::calendar> osl = std::make_shared<core::calendar>("Europe/Oslo");

  core::utctime t = utc->time(1932, 1, 1);
  core::utctimespan dt = core::deltahours(24);
  core::utctimespan dt_half = core::deltahours(12);
  std::size_t n = 24 * 365 * 2; // 24*365*5;

  // construct time-axis that we want to test.
  time_axis::fixed_dt fta(t, dt, n);
  time_axis::calendar_dt cta1(utc, t, dt, n);
  time_axis::calendar_dt cta2(osl, t, dt, n);

  vector<utctime> tp;
  for (std::size_t i = 0; i < fta.size(); ++i)
    tp.push_back(fta.time(i));
  time_axis::point_dt pta(tp, fta.total_period().end);
  test::utils::temp_dir tmpdir(time_in_micro_seconds ? "shyft.ts.db.s." : "shyft.ts.db.us.");
  ts_db db(tmpdir.string());
  db.time_format_micro_seconds(time_in_micro_seconds);
  SUBCASE("store_fixed_dt") {
    gts_t o(gta_t(fta), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    o.set(0, 100.);
    o.set(o.size() - 1, 100.);
    std::string fn("measurements/tssf.db"); // verify we can have path-parts
    db.save(fn, o, true);

    // read all
    auto r1 = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r1.point_interpretation());
    CHECK_EQ(o.time_axis(), r1.time_axis());

    // read inner slice
    core::utctime tb = t + 3u * dt / 2u;
    core::utctime te = t + (2u * n - 3u) * dt / 2u;
    auto r2 = db.read(fn, utcperiod{tb, te});
    CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
    CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t + dt, dt, n - 2u + 1));
    CHECK_EQ(r2.value(0), o.value(1));                        // dropped first value of o
    CHECK_EQ(r2.value(r2.size() - 1), o.value(o.size() - 1)); // dropped last value of o

    // read slice inbetween,require both points to be returned
    core::utcperiod rp{fta.time(0) + dt_half, fta.time(0) + dt_half + core::seconds(1)};
    auto r3 = db.read(fn, rp);
    CHECK_EQ(r3.size(), 2);
    CHECK_EQ(r3.value(0), doctest::Approx(o.value(0)));
    if (r3.size() > 1)
      CHECK_EQ(r3.value(1), doctest::Approx(o.value(1)));


    auto fr = db.find(string("measurements/.*\\.db")); // should match our ts.
    CHECK_EQ(fr.size(), 1);

    db.remove(fn);
    fr = db.find(string("measurements/.*\\.db")); // should match our ts.
    CHECK_EQ(fr.size(), 0);
  }

  SUBCASE("store_calendar_utc_dt") {
    gts_t o(gta_t(cta1), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    o.set(0, 100.);
    o.set(o.size() - 1, 100.);
    std::string fn("tssf1.db");
    std::string fnx("measurements/tssf1.db");

    db.save(fn, o, true);
    db.save(fnx, o, true); // for the find operation to show we discriminate directories

    // read all
    auto r = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r.point_interpretation());
    CHECK_EQ(o.time_axis(), r.time_axis());

    // read inner slice
    core::utctime tb = cta1.cal->add(t, dt_half, 3);
    core::utctime te = cta1.cal->add(t, dt_half, static_cast<int64_t>(2 * n - 3));
    auto r2 = db.read(fn, utcperiod{tb, te});

    CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
    CHECK_EQ(r2.time_axis(), time_axis::generic_dt{cta1.cal, cta1.cal->add(t, dt, 1), dt, n - 2u + 1});
    CHECK_EQ(r2.value(0), o.value(1));                        // dropped first value of o
    CHECK_EQ(r2.value(r2.size() - 1), o.value(o.size() - 1)); // dropped last value of o

    // read slice inbetween,require both points to be returned
    core::utcperiod rp{cta1.time(0) + dt_half, cta1.time(0) + dt_half + core::seconds(1)};
    auto r3 = db.read(fn, rp);
    CHECK_EQ(r3.size(), 2);
    CHECK_EQ(r3.value(0), doctest::Approx(o.value(0)));
    if (r3.size() > 1)
      CHECK_EQ(r3.value(1), doctest::Approx(o.value(1)));

    auto i = db.get_ts_info(fn);
    CHECK_EQ(i.name, fn);
    CHECK_EQ(i.data_period, o.total_period());
    CHECK_EQ(i.point_fx, o.point_interpretation());
    CHECK_LE(i.modified, utctime_now());
    CHECK_EQ(i.delta_t, cta1.dt);
    SUBCASE("verify_find_and_paths") {
      auto fr = db.find(string(".ss.1\\.db")); // should match our ts.
      REQUIRE_EQ(fr.size(), 1);
      CHECK_EQ(fr[0].name, fn);
      auto fr2 = db.find(string("measurements/.ss.1\\.db")); // should match other
      CHECK_EQ(fr2.size(), 1);
      auto fr3 = db.find(string("(.*\\/)?.ss.1\\.db")); // should match both, notice directory match
      CHECK_EQ(fr3.size(), 2);
    }

    db.remove(fn);
    try {
      auto rx = db.read(fn + ".not.there", utcperiod{});
      CHECK_UNARY(rx.size() == 3);
    } catch (exception const &) {
      CHECK_UNARY(true);
    }
  }

  SUBCASE("store_calendar_osl_dt") {
    gts_t o(gta_t(cta2), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    string fn("tssf2.db");
    db.save(fn, o, true);
    auto r = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r.point_interpretation());
    CHECK_EQ(o.time_axis(), r.time_axis());
    SUBCASE("read-null-frag") {
      utcperiod rp{cta2.cal->time(2000,1,1),cta2.cal->time(2000,1,2)};
      auto rn=db.read(fn,rp);
      CHECK_EQ(rn.size(),0u);
      CHECK_EQ(rn.time_axis().gt(),time_axis::generic_dt::CALENDAR);
      REQUIRE_NE(rn.time_axis().c().cal,nullptr);
      CHECK_EQ(rn.time_axis().c().cal->get_tz_name(),cta2.cal->get_tz_name());
      CHECK_EQ(rn.time_axis().c().dt,cta2.dt);//ensure we get dt filled in
    }
    db.remove(fn);
  }
  SUBCASE("get_ts_info_with_tz") {
    gts_t o(gta_t(osl, osl->time(2020, 3, 3), calendar::DAY, 10), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    string fn("tssf2_tz.db");
    db.save(fn, o, true);
    auto i = db.get_ts_info(fn);
    CHECK_EQ(i.name, fn);
    CHECK_EQ(i.data_period, o.total_period());
    CHECK_EQ(i.point_fx, o.point_interpretation());
    CHECK_LE(i.modified, utctime_now());
    CHECK_EQ(i.delta_t, o.ta.c().dt);
    CHECK_EQ(i.olson_tz_id, osl->get_tz_name());
    db.remove(fn);
  }
  SUBCASE("store_point_dt") {
    gts_t o(gta_t(pta), 10.0, time_series::ts_point_fx::POINT_INSTANT_VALUE);
    o.set(0, 100.);
    o.set(o.size() - 1, 100.);
    string fn("tssf3.db");
    db.save(fn, o, true);
    // read all
    auto r = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r.point_interpretation());
    CHECK_EQ(o.time_axis(), r.time_axis());
    // read slice
    auto tb = _t((to_seconds64(pta.time(2)) + to_seconds64(pta.time(1))) / 2);
    auto te = _t((to_seconds64(pta.time(pta.size() - 1)) + to_seconds64(pta.time(pta.size() - 2))) / 2);
    auto r2 = db.read(fn, utcperiod{tb, te});
    CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
    time_axis::point_dt exp{std::vector<core::utctime>(&o.ta.p().t[1], &o.ta.p().t[o.ta.p().t.size()]), o.ta.p().t_end};
    CHECK_EQ(r2.time_axis(), gta_t(exp));
    CHECK_EQ(r2.value(0), o.value(1));                        // dropped first value of o
    CHECK_EQ(r2.value(r2.size() - 1), o.value(o.size() - 1)); // dropped last value of o


    core::utcperiod rp{pta.time(0) + dt_half, pta.time(0) + dt_half + core::seconds(1)};
    auto r3 = db.read(fn, rp);
    CHECK_EQ(r3.size(), 2);
    CHECK_EQ(r3.value(0), doctest::Approx(o.value(0)));
    if (r3.size() > 1)
      CHECK_EQ(r3.value(1), doctest::Approx(o.value(1)));

    auto i = db.get_ts_info(fn);
    // calendar utc;
    // MESSAGE(" modified time :"<<utc.to_string(i.modified));
    CHECK_EQ(i.name, fn);
    CHECK_EQ(i.data_period, o.total_period());
    CHECK_EQ(i.point_fx, o.point_interpretation());
    CHECK_LE(i.modified, utctime_now() + _t(2));
    CHECK_GE(i.modified, utctime_now() - _t(300));
    // ensure we can store another fixed dt fragment on to the break point resolution
    // ref issue reported by Nito, a breakpoint ts SHALL be able to extend with any kind of time-axis, as basic
    // functionality
    gts_t o2(gta_t(fta.slice(1, 2)), 10.0, time_series::ts_point_fx::POINT_INSTANT_VALUE);
    db.save(fn, o2, false);
    db.remove(fn);
  }

  SUBCASE("dtss_db_speed") {
    size_t n_ts = 120;
    vector<gts_t> tsv;
    tsv.reserve(n_ts);
    double fv = 1.0;
    for (size_t i = 0; i < n_ts; ++i)
      tsv.emplace_back(gta_t(fta), fv += 1.0, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    CHECK_EQ(n_ts, tsv.size());

    auto t0 = timing::now();
    for (std::size_t i = 0; i < tsv.size(); ++i) {
      std::string fn("ts." + std::to_string(i) + ".db");
      db.save(fn, tsv[i], true);
    }
    auto t1 = timing::now();
    vector<gts_t> rv;
    for (std::size_t i = 0; i < tsv.size(); ++i) {
      std::string fn("ts." + std::to_string(i) + ".db");
      rv.push_back(db.read(fn, utcperiod{}));
    }
    auto t2 = timing::now();
    auto w_mb_s = n_ts * n / double(elapsed_ms(t0, t1)) / 1000.0;
    auto r_mb_s = n_ts * n / double(elapsed_ms(t1, t2)) / 1000.0;
    // on windows(before workaround): ~ 6 mpts/sec write, 162 mpts/sec read (slow close->workaround with thread?)
    // on linux: ~ 120 mpts/sec write, 180 mpts/sec read
    MESSAGE(
      "file_db: n_ts = " << n_ts << " n = " << n << " write Mpts/s = " << w_mb_s << ", read Mpts/s = " << r_mb_s
                         << " pts = " << n_ts * n << ", roundtrip ms=" << double(elapsed_ms(t0, t2)));
    // std::cout << "open_ms:" << db.t_open << ", write_ms:" << db.t_write << ", t_close_ms:" << db.t_close << std::endl;
    CHECK_EQ(rv.size(), tsv.size());
    // fs::remove_all("*.db");
  }
}

void test_dtss_db_store_merge_write(bool time_in_micro_seconds) {
  namespace core = shyft::core;
  namespace dtss = shyft::dtss;
  namespace ta = shyft::time_axis;
  namespace ts = shyft::time_series;
  using shyft::time_series::dd::gta_t;
  // setup db
  test::utils::temp_dir tmpdir(time_in_micro_seconds ? "ts.db.test.us" : "ts.db.test.s");
  dtss::ts_db db(tmpdir.string());
  db.time_format_micro_seconds(time_in_micro_seconds);
  SUBCASE("error_handling") {
    SUBCASE("extending with different ta") {
      // data
      std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>();
      const core::utctime t0 = core::utctime_now();
      const core::utctimespan dt_h = core::calendar::HOUR;
      const core::utctimespan dt_d = core::calendar::DAY;
      const std::size_t n = 1000;
      // -----
      ta::fixed_dt f_ta_h{t0, dt_h, n};
      ta::calendar_dt c_ta_d{utc_ptr, t0, dt_d, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(c_ta_d), 0.};
      // -----
      std::string fn("dtss_save_merge/ext_diff_ta.db");

      // save initital data
      db.save(fn, pts_d, false);
      auto find_res = db.find(string("dtss_save_merge/ext_diff_ta\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        db.save(fn, pts_h, false), std::runtime_error, "dtss_store: cannot merge with different ta type");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_diff_ta\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("extending with different point interpretation") {
      // data
      std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>();
      const core::utctime t0 = core::utctime_now();
      const core::utctimespan dt_h = core::calendar::HOUR;
      const std::size_t n = 1000;
      // -----
      ta::fixed_dt f_ta_h_1{t0, dt_h, n};
      ta::fixed_dt f_ta_h_2{t0 + n * dt_h, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h_1{gta_t(f_ta_h_1), 0., ts::POINT_INSTANT_VALUE};
      ts::point_ts<ta::generic_dt> pts_h_2{gta_t(f_ta_h_2), 0., ts::POINT_AVERAGE_VALUE};
      // -----
      std::string fn("dtss_save_merge/ext_diff_fx.db");

      // save initital data
      db.save(fn, pts_h_1, false);
      auto find_res = db.find(string("dtss_save_merge/ext_diff_fx\\.db"));
      REQUIRE_EQ(find_res.size(), 1);

      // add data to the same path
      if (time_in_micro_seconds) {
        db.save(fn, pts_h_2, false);
      } else {
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h_2, false),
          std::runtime_error,
          "dtss_store: cannot merge microseconds to old seconds based storage ts-file");
      }

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_diff_fx\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("fixed_dt old_dt != new_dt") {
      // data
      const core::utctime t0 = core::utctime_now();
      const core::utctimespan dt_h = core::calendar::HOUR;
      const core::utctimespan dt_d = core::calendar::DAY;
      const std::size_t n = 1000;
      // -----
      ta::fixed_dt f_ta_h{t0, dt_h, n};
      ta::fixed_dt f_ta_d{t0, dt_d, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      std::string fn("dtss_save_merge/ext_fixed_diff_dt.db"); // verify we can have path-parts

      // save initital data
      db.save(fn, pts_d, false);
      auto find_res = db.find(string("dtss_save_merge/ext_fixed_diff_dt\\.db"));
      CHECK_EQ(find_res.size(), 1);

      if (time_in_micro_seconds) {
        // add data to the same path
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false), std::runtime_error, "dtss_store: cannot merge unaligned fixed_dt");
      } else {
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false),
          std::runtime_error,
          "dtss_store: cannot merge microseconds to old seconds based storage ts-file");
      }

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_fixed_diff_dt\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("fixed_dt unaligned axes") {
      // data
      core::calendar utc{};
      const core::utctime t0_1 = utc.time(2000, 1, 1, 0, 0);
      const core::utctime t0_2 = utc.time(2000, 1, 1, 0, 13); // 13 minutes shifted
      const core::utctimespan dt_h = core::calendar::HOUR;
      const std::size_t n = 1000;
      // -----
      ta::fixed_dt f_ta_h{t0_1, dt_h, n};
      ta::fixed_dt f_ta_d{t0_2, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      std::string fn("dtss_save_merge/ext_fixed_unaligned.db"); // verify we can have path-parts

      // save initital data
      db.save(fn, pts_d, false);
      auto find_res = db.find(string("dtss_save_merge/ext_fixed_unaligned\\.db"));
      CHECK_EQ(find_res.size(), 1);

      if (time_in_micro_seconds) {
        // add data to the same path
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false), std::runtime_error, "dtss_store: cannot merge unaligned fixed_dt");
      } else {
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false),
          std::runtime_error,
          "dtss_store: cannot merge microseconds to old seconds based storage ts-file");
      }

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_fixed_unaligned\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("calendar_dt old_dt != new_dt") {
      // data
      std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>();
      const core::utctime t0 = core::utctime_now();
      const core::utctimespan dt_h = core::calendar::HOUR;
      const core::utctimespan dt_d = core::calendar::DAY;
      const std::size_t n = 1000;
      // -----
      ta::calendar_dt f_ta_h{utc_ptr, t0, dt_h, n};
      ta::calendar_dt f_ta_d{utc_ptr, t0, dt_d, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      std::string fn("dtss_save_merge/ext_cal_diff_dt.db"); // verify we can have path-parts

      // save initital data
      db.save(fn, pts_d, false);
      auto find_res = db.find(string("dtss_save_merge/ext_cal_diff_dt\\.db"));
      CHECK_EQ(find_res.size(), 1);

      if (time_in_micro_seconds) {
        // add data to the same path
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false), std::runtime_error, "dtss_store: cannot merge unaligned calendar_dt");
      } else {
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false),
          std::runtime_error,
          "dtss_store: cannot merge microseconds to old seconds based storage ts-file");
      }

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_cal_diff_dt\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("calendar_dt unaligned axes") {
      // data
      std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>();
      const core::utctime t0_1 = utc_ptr->time(2000, 1, 1, 0, 0);
      const core::utctime t0_2 = utc_ptr->time(2000, 1, 1, 0, 13); // 13 minutes shifted
      const core::utctimespan dt_h = core::calendar::HOUR;
      const std::size_t n = 1000;
      // -----
      ta::calendar_dt f_ta_h{utc_ptr, t0_1, dt_h, n};
      ta::calendar_dt f_ta_d{utc_ptr, t0_2, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      std::string fn("dtss_save_merge/ext_cal_unaligned.db"); // verify we can have path-parts

      // save initital data
      db.save(fn, pts_d, false);
      auto find_res = db.find(string("dtss_save_merge/ext_cal_unaligned\\.db"));
      CHECK_EQ(find_res.size(), 1);

      if (time_in_micro_seconds) {
        // add data to the same path
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false), std::runtime_error, "dtss_store: cannot merge unaligned calendar_dt");
      } else {
        CHECK_THROWS_AS_MESSAGE(
          db.save(fn, pts_h, false),
          std::runtime_error,
          "dtss_store: cannot merge microseconds to old seconds based storage ts-file");
      }

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_cal_unaligned\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("calendar_dt different calendars") {
      // data
      std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>();
      std::shared_ptr<core::calendar> osl_ptr = std::make_shared<core::calendar>("Europe/Oslo");
      const core::utctime t0_1 = utc_ptr->time(2000, 1, 1, 0, 0);
      const core::utctime t0_2 = osl_ptr->time(2000, 1, 1, 0, 13); // 13 minutes shifted
      const core::utctimespan dt_h = core::calendar::HOUR;
      const std::size_t n = 1000;
      // -----
      ta::calendar_dt f_ta_h{utc_ptr, t0_1, dt_h, n};
      ta::calendar_dt f_ta_d{osl_ptr, t0_2, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      std::string fn("dtss_save_merge/ext_cal_diff_cal.db"); // verify we can have path-parts

      // save initital data
      db.save(fn, pts_d, false);
      auto find_res = db.find(string("dtss_save_merge/ext_cal_diff_cal\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        db.save(fn, pts_h, false), std::runtime_error, "dtss_store: cannot merge calendar_dt with different calendars");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_cal_diff_cal\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
  }
  SUBCASE("merging time-series") {
    std::shared_ptr<core::calendar> utc_ptr = std::make_shared<core::calendar>();
    std::shared_ptr<core::calendar> osl_ptr = std::make_shared<core::calendar>("Europe/Oslo");

    SUBCASE("fixed_dt") {
      SUBCASE("exact") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0 = utc_ptr->time(2016, 1, 1);
        // -----
        ta::fixed_dt f_ta{t0, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_exact.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_exact\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_exact.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());
        CHECK_UNARY(i.delta_t == dt);

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_exact\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old") {
        // data
        const std::size_t drop = 10u; // points to drop from start/end of old
        // -----
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + dt * drop;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n - 2 * drop};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_new_in_old.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_new_in_old\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_new_in_old.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(drop - 1), 1.);
        CHECK_EQ(res.v.at(drop), 10.);
        CHECK_EQ(res.v.at(n - drop - 1), 10.);
        CHECK_EQ(res.v.at(n - drop), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new") {
        // data
        const std::size_t extra = 10u; // points to drop from start/end of old
        // -----
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old - dt * extra;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n + extra};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_old_in_new.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_old_in_new.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap start of old") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old - dt * n / 2;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_new_over_start.db");

        REQUIRE_LT(t0_new, t0_old);

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_new_over_start\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_new_over_start.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_EQ(res.v.at(n), 1.);
        CHECK_EQ(res.v.at(n + n / 2 - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_new_over_start\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap end of old") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + dt * n / 2;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_new_over_end.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_new_over_end\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_new_over_end.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n / 2 - 1), 1.);
        CHECK_EQ(res.v.at(n / 2), 10.);
        CHECK_EQ(res.v.at(n + n / 2 - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_new_over_end\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("consecutive without gap") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + dt * n;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_consec.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_consec\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_consec.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_EQ(res.v.at(n), 10.);
        CHECK_EQ(res.v.at(2 * n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_consec\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new after old with gap") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + 2 * n * dt;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_gap_after.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_gap_after\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_gap_after.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_UNARY(std::isnan(res.v.at(2 * n - 1)));
        CHECK_EQ(res.v.at(2 * n), 10.);
        CHECK_EQ(res.v.at(3 * n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_gap_after\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new before old with gap") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old - 2 * n * dt;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/fixed_gap_before.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/fixed_gap_before\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/fixed_gap_before.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_UNARY(std::isnan(res.v.at(2 * n - 1)));
        CHECK_EQ(res.v.at(2 * n), 1.);
        CHECK_EQ(res.v.at(3 * n - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_gap_before\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
    }
    SUBCASE("calendar_dt") {
      SUBCASE("exact") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0 = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta{utc_ptr, t0, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_exact.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_exact\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_exact.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_exact\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old") {
        // data
        const std::size_t drop = 10u; // points to drop from start/end of old
        // -----
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + dt * drop;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n - 2 * drop};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_new_in_old.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_new_in_old\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_new_in_old.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(drop - 1), 1.);
        CHECK_EQ(res.v.at(drop), 10.);
        CHECK_EQ(res.v.at(n - drop - 1), 10.);
        CHECK_EQ(res.v.at(n - drop), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new") {
        // data
        const std::size_t extra = 10u; // points to drop from start/end of old
        // -----
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old - dt * extra;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n + extra};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_old_in_new.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_old_in_new.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap start of old") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old - dt * n / 2;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_new_over_start.db");

        REQUIRE_LT(t0_new, t0_old);

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_new_over_start\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_new_over_start.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_EQ(res.v.at(n), 1.);
        CHECK_EQ(res.v.at(n + n / 2 - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_new_over_start\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap end of old") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + dt * n / 2;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_new_over_end.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_new_over_end\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_new_over_end.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n / 2 - 1), 1.);
        CHECK_EQ(res.v.at(n / 2), 10.);
        CHECK_EQ(res.v.at(n + n / 2 - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_new_over_end\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("consecutive without gap") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + dt * n;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_consec.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_consec\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_consec.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_EQ(res.v.at(n), 10.);
        CHECK_EQ(res.v.at(2 * n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_consec\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new after old with gap") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old + 2 * n * dt;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_gap_after.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_gap_after\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_gap_after.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_UNARY(std::isnan(res.v.at(2 * n - 1)));
        CHECK_EQ(res.v.at(2 * n), 10.);
        CHECK_EQ(res.v.at(3 * n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_gap_after\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new before old with gap") {
        // data
        const core::utctimespan dt = core::calendar::DAY;
        const std::size_t n = 100;
        const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
        const core::utctime t0_new = t0_old - 2 * n * dt;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        std::string fn("dtss_save_merge/calendar_gap_before.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/calendar_gap_before\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/calendar_gap_before.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, c_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_UNARY(std::isnan(res.v.at(2 * n - 1)));
        CHECK_EQ(res.v.at(2 * n), 1.);
        CHECK_EQ(res.v.at(3 * n - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_gap_before\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
    }
    SUBCASE("point_dt") {
      // data
      const core::utctimespan dt = core::calendar::DAY;
      const std::size_t n = 100;
      const core::utctime t0_old = utc_ptr->time(2016, 1, 1, 12, 30, 15);
      const core::utctimespan offset = core::deltaminutes(25); // to misalign old and new timepoints
      // -----
      std::vector<core::utctime> old_timepoints{};
      const std::vector<double> old_values(n, 1.);
      // -----
      std::vector<core::utctime> new_timepoints{};
      std::vector<double> new_values(n, 10.);
      // -----
      old_timepoints.reserve(n + 1);
      for (std::size_t i = 0; i <= n; ++i) {
        old_timepoints.emplace_back(t0_old + dt * i);
      }

      SUBCASE("exact") {
        // data
        ta::point_dt p_ta{old_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta), new_values};
        // -----
        std::string fn("dtss_save_merge/point_exact.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_exact\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_exact.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_exact\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old") {
        // data
        const std::size_t drop = 10u; // points to drop from start/end of old
        const core::utctime t0_new = t0_old + offset + dt * drop;
        // -----
        new_timepoints.reserve(n - 2 * drop);
        for (std::size_t i = 0; i <= n - 2 * drop; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        new_values.resize(new_timepoints.size() - 1);
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_new_in_old.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_new_in_old\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_new_in_old.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_old.total_period().end);
        // values
        //  - NB! (n+1) because the point_dt's are unaligned, so a new point is introduced
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(drop), 1.); // this is the extra new point
        CHECK_EQ(res.v.at(drop + 1), 10.);
        CHECK_EQ(res.v.at((n + 1) - drop - 1), 10.);
        CHECK_EQ(res.v.at((n + 1) - drop), 1.);
        CHECK_EQ(res.v.at((n + 1) - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new") {
        // data
        const std::size_t extra = 10u; // points to drop from start/end of old
        const core::utctime t0_new = t0_old + offset - dt * extra;
        // -----
        new_timepoints.reserve(n + 2 * extra);
        for (std::size_t i = 0; i <= n + 2 * extra; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        new_values.resize(new_timepoints.size() - 1, 10.);
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_old_in_new.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_old_in_new.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap start of old") {
        // data
        const core::utctime t0_new = t0_old + offset - dt * n / 2;
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_new_over_start.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_new_over_start\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_new_over_start.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_EQ(res.v.at(n), 1.);
        CHECK_EQ(res.v.at(n + n / 2 - 1), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_over_start\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap end of old") {
        // data
        const core::utctime t0_new = t0_old + offset + dt * n / 2;
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_new_over_end.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_new_over_end\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_new_over_end.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n / 2), 1.);
        CHECK_EQ(res.v.at(n / 2 + 1), 10.);
        CHECK_EQ(res.v.at(n + n / 2), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_over_end\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("consecutive without gap") {
        // data
        const core::utctime t0_new = t0_old + n * dt;
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_consec.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_consec\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_consec.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_EQ(res.v.at(n), 10.);
        CHECK_EQ(res.v.at(2 * n - 1), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_consec\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new after old with gap") {
        // data
        const core::utctime t0_new = t0_old + n * dt + offset;
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_gap_after.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_gap_after\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_gap_after.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.ta.size(), 2 * n + 1);
        CHECK_EQ(res.total_period().start, p_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_new.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_EQ(res.v.at(n + 1), 10.);
        CHECK_EQ(res.v.at(2 * n), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_gap_after\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new before old with gap") {
        // data
        const core::utctime t0_new = t0_old - n * dt - offset;
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dt);
        }
        // -----
        ta::point_dt p_ta_old{old_timepoints};
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        std::string fn("dtss_save_merge/point_gap_before.db");

        // save initital data
        db.save(fn, pts_old, false);
        auto find_res = db.find("dtss_save_merge/point_gap_before\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        db.save(fn, pts_new, false);

        // check merged data
        ts::point_ts<ta::generic_dt> res = db.read("dtss_save_merge/point_gap_before.db", core::utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.ta.size(), 2 * n + 1);
        CHECK_EQ(res.total_period().start, p_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_EQ(res.v.at(n + 1), 1.);
        CHECK_EQ(res.v.at(2 * n), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_gap_before\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
    }
    SUBCASE("force overwrite") {
      // data
      const std::size_t drop = 10u; // points to drop from start/end of old
      // -----
      const core::utctimespan dt = core::calendar::DAY;
      const std::size_t n = 100;
      const core::utctime t0_old = utc_ptr->time(2016, 1, 1);
      const core::utctime t0_new = t0_old + dt * drop;
      // -----
      ta::fixed_dt f_ta_old{t0_old, dt, n};
      ta::fixed_dt f_ta_new{t0_new, dt, n - 2 * drop};
      ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
      ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
      // -----
      std::string fn("dtss_save_merge/force_overwrite.db");

      // save initital data
      db.save(fn, pts_old, false);
      auto find_res = db.find("dtss_save_merge/force_overwrite\\.db");
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path with overwrite
      db.save(fn, pts_new, true);

      // check merged data
      ts::point_ts<ta::generic_dt> res = db.read(fn, core::utcperiod{});
      // time-axis
      CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
      CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
      CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
      // values
      CHECK_EQ(res.v.at(0), 10.);
      CHECK_EQ(res.v.at(n - 2 * drop - 1), 10.);
      auto i = db.get_ts_info(fn);
      CHECK_UNARY(i.data_period == res.total_period());
      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/force_overwrite\\.db"));
      CHECK_EQ(find_res.size(), 0);
    }
  }
}

TEST_SUITE_BEGIN("dtss");

TEST_CASE("dtss/db_seconds_time_io") {
  test_time_io<seconds_time_io>("sec");
}

TEST_CASE("dtss/db_native_time_io") {
  test_time_io<native_time_io>("usec");
}

TEST_CASE("dtss/db_basics_micro_seconds") {
  test_dtss_db_basics(true);
}

TEST_CASE("dtss/db_basics_seconds") {
  test_dtss_db_basics(false);
}

TEST_CASE("dtss/db_merge_write_micro_seconds") {
  test_dtss_db_store_merge_write(true);
}

TEST_CASE("dtss/db_merge_write_seconds") {
  test_dtss_db_store_merge_write(false);
}

TEST_CASE("dtss/db_merge_us_to_seconds_throws") {
  using namespace shyft::dtss;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;

  std::shared_ptr<core::calendar> utc = std::make_shared<core::calendar>();
  std::shared_ptr<core::calendar> osl = std::make_shared<core::calendar>("Europe/Oslo");

  core::utctime t = utc->time(2016, 1, 1);
  core::utctimespan dt = core::deltahours(1);
  std::size_t n = 24 * 365 * 2; // 24*365*5;

  // construct time-axis that we want to test.
  time_axis::fixed_dt fta(t, dt, n);
  constexpr bool time_in_micro_seconds = false; // use seconds when writing startts.
  vector<utctime> tp;
  for (std::size_t i = 0; i < fta.size(); ++i)
    tp.push_back(fta.time(i));
  time_axis::point_dt pta(tp, fta.total_period().end);
  test::utils::temp_dir tmpdir(time_in_micro_seconds ? "ts.db.us" : "ts.db.s");
  ts_db db(tmpdir.string());
  db.time_format_micro_seconds(time_in_micro_seconds);
  // 1. store a break-point ts, with seconds resolution
  gts_t o(gta_t(pta), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  o.set(0, 100.);
  o.set(o.size() - 1, 100.);
  std::string fn("measurements/tssf.db");
  db.save(fn, o, true);

  // 2. try to merge another ts, with seconds resolution (should work)
  vector<utctime> tp2;
  tp2.push_back(tp.back());
  time_axis::point_dt pta2(tp2, tp2.back() + dt);
  gts_t x(gta_t(pta2), 9.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  db.save(fn, x, false); // merge store
  // 3. try to merge another ts, with us resolution (should throw).
  tp2[0] += utctime{5000}; // add some microseconds
  gts_t us(gta_t(tp2, tp2.back() + dt), 7.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  try {
    db.save(fn, us, false);
    CHECK_UNARY(false);
  } catch (runtime_error const &) {
    CHECK_UNARY(true);
  }
  // 4. read back the merged ts.
  utcperiod rp(o.total_period().start, pta2.total_period().end);
  auto rts = db.read(fn, rp);
  CHECK_EQ(rts.total_period(), rp);
  auto tsi = db.get_ts_info(fn);
  CHECK_UNARY(tsi.data_period == rp);
  db.remove(fn);
}

TEST_CASE("dtss/db_write_large_fixed_dt" * doctest::skip()) {
  // test case that cover windows 32 limitations
  // as well as that we now avoid using uint32_t n-field of the ts-header
  // thus allowing time-series with number of points limited only by 64bit ints
  using namespace shyft::dtss;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;

  std::shared_ptr<core::calendar> utc = std::make_shared<core::calendar>();
  core::utctime t = utc->time(2016, 1, 1);
  core::utctimespan dt = from_seconds(0.000025);
  size_t n = (std::numeric_limits<std::uint32_t>::max() + 1000UL) / 8UL;
  // construct time-axis that we want to test.
  time_axis::fixed_dt fta(t, dt, n);

  gts_t o(gta_t(fta), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  std::string fn("measurements/audio_40khz.db");
  test::utils::temp_dir tmpdir("ts.db.us.xlarge");
  ts_db db(tmpdir.string());
  db.save(fn, o, true);
  auto rts = db.read(fn, utcperiod{});
  FAST_CHECK_EQ(rts, o); // make sure this still works
  auto i = db.get_ts_info(fn);
  FAST_CHECK_EQ(fta.size(), size_t(i.data_period.timespan() / i.delta_t));
  gts_t e(
    gta_t(time_axis::fixed_dt(fta.total_period().end, dt, 10 * 1000)),
    3.0,
    time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  db.save(fn, e, false);               // make sure we extend the ts.
  auto ets = db.read(fn, utcperiod{}); // read all, including extension
  FAST_CHECK_EQ(ets.size(), e.size() + o.size());
  FAST_CHECK_EQ(ets.time_axis().f().dt, dt);
}

TEST_CASE("dtss/db_write_large_point_dt" * doctest::skip() * doctest::description("this test needs 128GB+ to run ")) {
  // test case that cover 32bit limitations in the current db-header
  // We throw exception for attempt to exceed this limit
  // Testing it require a *LOT* of memory, so don't try to run this unless
  // you have like 128GB available (as in free before you start the test)
  using namespace shyft::dtss;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;

  std::shared_ptr<core::calendar> utc = std::make_shared<core::calendar>();
  core::utctime t = utc->time(2016, 1, 1);
  core::utctimespan dt = from_seconds(0.000025);
  size_t n = (std::numeric_limits<std::uint32_t>::max() + 1000UL);
  // construct time-axis that we want to test.
  time_axis::fixed_dt fta(t, dt, n);
  time_axis::point_dt pta;
  pta.t_end = fta.total_period().end;
  pta.t.reserve(fta.size());
  for (size_t i = 0; i < fta.size(); ++i)
    pta.t.emplace_back(fta._time(i));

  gts_t o(gta_t(pta), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  std::string fn("measurements/audio_40khz.db");
  test::utils::temp_dir tmpdir("ts.db.p().us.xlarge");
  ts_db db(tmpdir.string());
  try {
    db.save(fn, o, true);
  } catch (std::runtime_error const &) {
    // re; // Silence warning C4101: 're': unreferenced local variable
    FAST_CHECK_EQ(true, true);
  }
}

TEST_CASE("dtss/db/minimal_point_merge") {
  using namespace shyft::dtss;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;

  time_axis::point_dt pta{vector<utctime>{utctime{0}}, utctime{10}};
  time_axis::point_dt ptb{vector<utctime>{utctime{2}}, utctime{4}};
  time_axis::point_dt ptr{
    vector<utctime>{utctime{0}, utctime{2}, utctime{4}},
    utctime{10}
  };

  gts_t a(gta_t(pta), 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  gts_t b(gta_t(ptb), 0.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  gts_t r(gta_t(ptr), vector<double>{1.0, 0.0, 1.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  std::string fn("p1.db");
  test::utils::temp_dir tmpdir("ts.db.p1");
  ts_db db(tmpdir.string());
  db.save(fn, a, true);
  db.save(fn, b, false);
  auto rx = db.read(fn, utcperiod{});
  FAST_CHECK_EQ(rx, r);
}

TEST_CASE("dtss/db/header/valid") {
  ts_db_header h;
  FAST_CHECK_EQ(true, h.is_valid());
  h.signature[2] = '1';
  FAST_CHECK_EQ(true, h.is_valid());
  h.point_fx = time_series::POINT_INSTANT_VALUE;
  FAST_CHECK_EQ(true, h.is_valid());
  FAST_CHECK_EQ(true, h.is_seconds());
  h.ta_type = time_axis::generic_dt::CALENDAR;
  FAST_CHECK_EQ(true, h.is_valid());
  h.ta_type = time_axis::generic_dt::POINT;
  FAST_CHECK_EQ(true, h.is_valid());
  for (auto i = 0; i < 4; i++) {
    auto hh{h};
    hh.signature[i] = 'x'; // destroy signature
    FAST_CHECK_EQ(false, hh.is_valid());
  }
}

TEST_CASE("dtss/db/handle_bad_files") {
  using namespace shyft::dtss;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;

  time_axis::point_dt pta{vector<utctime>{utctime{0}}, utctime{10}};

  gts_t a(gta_t(pta), 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  std::string fn("p1.db");
  test::utils::temp_dir tmpdir("ts.db.p1");
  ts_db db(tmpdir.string());
  db.save(fn, a, true);
  auto rx = db.read(fn, utcperiod{});
  FAST_CHECK_EQ(rx, a);
  auto full_path = (tmpdir / fn).string();
  auto fp = std::fopen(full_path.c_str(), "r+b");
  char baad_food[] = "xy";
  std::fwrite(baad_food, 1, 2, fp);
  std::fclose(fp);
  DOCTEST_CHECK_THROWS(db.read(fn, utcperiod{}));
  DOCTEST_CHECK_THROWS(db.find(fn));
  DOCTEST_CHECK_THROWS(db.save(fn, a, false));
  db.remove(fn);
}

TEST_SUITE_END();
