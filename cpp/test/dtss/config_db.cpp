#include <map>
#include "test_pch.h"
#include <test/test_utils.h>
#include <shyft/dtss/container_config.h>


TEST_SUITE_BEGIN("dtss");
using namespace shyft::dtss;

TEST_CASE("dtss/container_config") {
  container_config a, b;
  a.container_cfg["a"] = "/root/a";
  a.container_cfg[""] = "/root";
  CHECK(a != b);
  test::utils::temp_dir tmpdir{"dtss-cfg"};
  fs::create_directories(tmpdir.string());
  auto db_path = (tmpdir / ".container_config").string();
  container_config::store(db_path, a);
  auto c = container_config::read(db_path);
  CHECK(a == c);
}

TEST_SUITE_END();
