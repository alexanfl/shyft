#include "test_pch.h"

#include <shyft/dtss/dtss_mutex.h>

TEST_SUITE_BEGIN("dtss");

TEST_CASE("dtss/mutex") {
  using namespace shyft::dtss;
  file_lock_manager m;
  string a("a");
  string b("b");
  {
    reader_file_lock f1(m, a);
    CHECK_EQ(m.f_locks.size(), 1);
  } //
  CHECK_EQ(m.f_locks.size(), 0);
  {
    reader_file_lock f2(m, a);
    reader_file_lock f3(m, b);
    CHECK_EQ(m.f_locks.size(), 2);
  }
  CHECK_EQ(m.f_locks.size(), 0);
  {
    writer_file_lock f4(m, a);
    reader_file_lock f5(m, b);
    CHECK_EQ(m.f_locks.size(), 2);
  }
  CHECK_EQ(m.f_locks.size(), 0);
}

TEST_SUITE_END();
