#include "test_pch.h"
#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/hydrology/stacks/pt_st_k_cell_model.h>

#include <iostream>
#include <tuple>

static inline shyft::core::utctime _t(int64_t t1970s) {
  return shyft::core::utctime{shyft::core::seconds(t1970s)};
}

using namespace shyft::core;
using namespace shyft::core::snow_tiles;
using namespace std;
using snow_tiles_model = calculator;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/snow_tiles/normalize_vector") {
  parameter p;
  vector<double> vec = {1, 2, 3};
  p.normalize_vector_mean(vec);
  double sum = 0;
  for (auto n : vec)
    sum += n;
  TS_ASSERT_DELTA(sum / vec.size(), 1, 1e-8);
}

TEST_CASE("hydrology/snow_tiles/parameter_equal") {
  parameter a;
  parameter b;
  CHECK_EQ(true, a == b);
  CHECK_EQ(false, a != b);
  a.set_shape(0.7);
  CHECK_EQ(false, a == b);
  CHECK_EQ(true, a != b);
  a = b;
  REQUIRE_EQ(a, b);
  a.ts += 0.1;
  CHECK_EQ(false, a == b);
  a = b;
  a.tx += 0.1;
  CHECK_EQ(false, a == b);
  a = b;
  a.cfr += 0.1;
  CHECK_EQ(false, a == b);
  a = b;
  a.lwmax += 0.1;
  CHECK_EQ(false, a == b);
  a = b;
  a.cx += 0.1;
  CHECK_EQ(false, a == b);
  a = b;
}

TEST_CASE("hydrology/snow_tiles/compute_inverse_gamma") {
  parameter p;
  vector<double> multiply;
  double shape_min = 0.2;
  double const shape_max = 10;
  double const shape_step = 0.5;
  double mean_diff_max = 0.0;
  for (double shape = shape_min; shape < shape_max; shape += shape_step) {
    vector<double> multiply;
    multiply = p.compute_inverse_gamma(shape);
    vector<double> multiply_normalized;
    for (auto i = 0u; i < multiply.size(); i++)
      multiply_normalized.push_back(multiply[i]);
    p.normalize_vector_mean(multiply_normalized);
    double mean = 0;
    double mean_normalized = 0;
    for (auto i = 0u; i < multiply.size(); i++) {
      if (i > 0) {
        TS_ASSERT_LESS_THAN(multiply[i - 1], multiply[i]);
      }
      mean += multiply[i];
      mean_normalized += multiply_normalized[i];
    }
    mean /= multiply.size();
    mean_normalized /= multiply.size();
    mean_diff_max = max(mean_diff_max, abs(mean - 1));
    TS_ASSERT_DELTA(mean_normalized, 1.0, 1e-8);
  }
  TS_ASSERT_DELTA(mean_diff_max, 0.0, 1e-2);
}

TEST_CASE("hydrology/snow_tiles/snow_distr_from_gamma") {
  parameter p;
  vector<double> multiply;
  vector<double> area_fractions;
  multiply = p.get_multiply();
  area_fractions = p.get_area_fractions();
  double mean = 0.0;
  for (auto i = 0u; i < area_fractions.size(); i++) {
    if (i > 0) {
      TS_ASSERT_LESS_THAN(multiply[i - 1], multiply[i]);
    }
    mean += double(multiply[i]);
  }
  mean /= multiply.size();
  TS_ASSERT_DELTA(mean, 1.0, 1e-8);
}

TEST_CASE("hydrology/snow_tiles/set_shape") {
  double shape = 1.0;
  parameter p;
  vector<double> multiply_before;
  multiply_before = p.get_multiply();
  p.set_shape(shape);
  TS_ASSERT_DELTA(p.get_shape(), shape, 1e-8);
  vector<double> multiply_after;
  multiply_after = p.get_multiply();
  double mean_before = 0.0;
  double mean_after = 0.0;
  for (auto i = 0u; i < multiply_after.size(); i++) {
    mean_before += double(multiply_before[i]);
    mean_after += double(multiply_after[i]);
  }

  mean_before /= multiply_before.size();
  mean_after /= multiply_after.size();
  TS_ASSERT_DELTA(mean_before, 1.0, 1e-8);
  TS_ASSERT_DELTA(mean_after, 1.0, 1e-8);
  TS_ASSERT_LESS_THAN(multiply_after[0], multiply_before[0]);
  TS_ASSERT_LESS_THAN(multiply_before[9], multiply_after[9]);
}

TEST_CASE("hydrology/snow_tiles/set_area_fractions") {
  parameter p;
  vector<double> multiply;
  vector<double> area_fractions;
  vector<double> new_area_fractions = {0.5, 0.6}; // sum > 1
  TS_ASSERT_THROWS_ANYTHING(p.set_area_fractions(new_area_fractions));

  new_area_fractions = {0.009, 0.991}; // element < 0.01
  TS_ASSERT_THROWS_ANYTHING(p.set_area_fractions(new_area_fractions));

  new_area_fractions = {0.4, 0.3, 0.2, 0.1};
  p.set_area_fractions(new_area_fractions);
  area_fractions = p.get_area_fractions();
  multiply = p.get_multiply();
  double sum = 0.0;
  double mean = 0.0;
  for (auto i = 0u; i < area_fractions.size(); i++) {
    sum += area_fractions[i];
    mean += multiply[i];
  }
  mean /= multiply.size();
  TS_ASSERT_DELTA(sum, 1.0, 1e-8); // area fractions normalized to sum=1
  TS_ASSERT_DELTA(area_fractions.size(), double(new_area_fractions.size()), 1e-8);
  TS_ASSERT_DELTA(mean, 1.0, 1e-8);
  TS_ASSERT_DELTA(multiply.size(), double(new_area_fractions.size()), 1e-8);
  new_area_fractions = {0.1, 0.2, 0.3, 0.4};
  p.set_area_fractions(new_area_fractions);
  new_area_fractions = {0.01, 0.99};
  p.set_area_fractions(new_area_fractions);
  new_area_fractions = {0.99, 0.01};
  p.set_area_fractions(new_area_fractions);
}

TEST_CASE("hydrology/snow_tiles/precipitation_phase_calculation") {
  parameter p;
  snow_tiles_model snow_model(p);

  double psolid, pliquid;

  tie(psolid, pliquid) = snow_model.split_precipitation(1.0, -10.0, 0.0);

  TS_ASSERT_DELTA(psolid, 0.9999999979388463, 1.0e-8);
  TS_ASSERT_DELTA(pliquid, 1 - 0.9999999979388463, 1.0e-8);
  TS_ASSERT_DELTA(psolid + pliquid, 1.0, 1.0e-8);

  tie(psolid, pliquid) = snow_model.split_precipitation(1.0, 0.0, 0.0);

  TS_ASSERT_DELTA(psolid, 0.5, 1.0e-8);
  TS_ASSERT_DELTA(pliquid, 0.5, 1.0e-8);
  TS_ASSERT_DELTA(psolid + pliquid, 1.0, 1.0e-8);

  tie(psolid, pliquid) = snow_model.split_precipitation(1.0, 10.0, 0.0);

  TS_ASSERT_DELTA(psolid, 2.0611536181902037e-9, 1.0e-8);
  TS_ASSERT_DELTA(pliquid, 1 - 2.0611536181902037e-9, 1.0e-8);
  TS_ASSERT_DELTA(psolid + pliquid, 1.0, 1.0e-8);
}

TEST_CASE("hydrology/snow_tiles/potential_melt_calculation") {
  parameter p;
  snow_tiles_model snow_model(p);

  double pot_melt;

  pot_melt = snow_model.compute_potmelt(-10.0, 5.0, 0.0, 1.0);
  TS_ASSERT_DELTA(pot_melt, 5.1528825650848376e-9, 1.0e-8);

  pot_melt = snow_model.compute_potmelt(0.0, 5.0, 0.0, 1.0);
  TS_ASSERT_DELTA(pot_melt, 1.7328679513998633, 1.0e-8);

  pot_melt = snow_model.compute_potmelt(10.0, 5.0, 0.0, 1.0);
  TS_ASSERT_DELTA(pot_melt, 50.000000005152884, 1.0e-8);
}

TEST_CASE("hydrology/snow_tiles/potential_refreeze_calculation") {
  parameter p;
  snow_tiles_model snow_model(p);

  double pot_refreeze;

  pot_refreeze = snow_model.compute_potrefreeze(-10.0, 5.0, 0.1, 0.0, 1.0);
  TS_ASSERT_DELTA(pot_refreeze, 5.0000000005152884, 1.0e-8);

  pot_refreeze = snow_model.compute_potrefreeze(0.0, 5.0, 0.1, 0.0, 1.0);
  TS_ASSERT_DELTA(pot_refreeze, 0.17328679513998633, 1.0e-8);

  pot_refreeze = snow_model.compute_potrefreeze(10.0, 5.0, 0.1, 0.0, 1.0);
  TS_ASSERT_DELTA(pot_refreeze, 5.1528825650848376e-10, 1.0e-8);
}

TEST_CASE("hydrology/snow_tiles/limit_fluxes") {
  parameter p;
  snow_tiles_model snow_model(p);

  double act_flux = 50;

  act_flux = snow_model.limit_fluxes(act_flux, 100);
  TS_ASSERT_DELTA(act_flux, 50, 1.0e-8);

  act_flux = 50;

  act_flux = snow_model.limit_fluxes(act_flux, 30);
  TS_ASSERT_DELTA(act_flux, 30, 1.0e-8);
}

TEST_CASE("hydrology/snow_tiles/update_states") {
  parameter p;
  snow_tiles_model snow_model(p);

  double outflow;

  // Outflow == 0

  double fw = 90.0, lw = 5.0;
  double psolid = 10.0, pliquid = 5.0, act_melt = 5.0, act_refreeze = 5.0;
  double lwmax = 0.1;

  tie(fw, lw, outflow) = snow_model.update_states(fw, lw, psolid, pliquid, act_melt, act_refreeze, lwmax);

  TS_ASSERT_DELTA(fw, 100.0, 1.0e-8);
  TS_ASSERT_DELTA(lw, 10.0, 1.0e-8);
  TS_ASSERT_DELTA(outflow, 0.0, 1.0e-8);

  // Outflow == 10

  fw = 90.0, lw = 15.0;
  psolid = 10.0, pliquid = 5.0, act_melt = 5.0, act_refreeze = 5.0;
  lwmax = 0.1;

  tie(fw, lw, outflow) = snow_model.update_states(fw, lw, psolid, pliquid, act_melt, act_refreeze, lwmax);

  TS_ASSERT_DELTA(fw, 100.0, 1.0e-8);
  TS_ASSERT_DELTA(lw, 10.0, 1.0e-8);
  TS_ASSERT_DELTA(outflow, 10.0, 1.0e-8);
}

TEST_CASE("hydrology/snow_tiles/mass_balance") {
  parameter p;
  p.get_multiply(); // force init.
  state state;
  response r;
  utctime t0 = _t(0), t1 = _t(86400);

  auto area_fractions{p.get_area_fractions()};
  for (double prec = 0; prec < 10; prec++) {
    for (double temp = -20; temp < 21; temp += 2.0) {
      for (double lw = 0; lw < 101; lw += 10.0) {
        for (double fw = 0; fw < 101; fw += 10.0) {
          r.swe = 0;
          for (auto i = 0u; i < state.fw.size(); i++) {
            state.fw[i] = fw;
            state.lw[i] = lw;
            r.swe += area_fractions[i] * (state.fw[i] + state.lw[i]);
          }

          double total_water_before = r.swe; // this is the state , in mm when we start.
          snow_tiles_model snow_model(p);
          snow_model.step(state, r, t0, t1, prec, temp);
          double total_water_after = 0.0;
          for (size_t i = 0; i < state.fw.size(); ++i) // sum up what is stored into the snow_model
            total_water_after += area_fractions[i] * ((state.fw[i] + state.lw[i]));
          // swe.before plus precip 24h should equal swe_after + outflow 24h for the entire period
          double mass_balance = total_water_before + 24 * prec
                              - (total_water_after + r.outflow * 24.0); // don't forget, it responds in mm/h!'
          REQUIRE_EQ(mass_balance, doctest::Approx(0.0).epsilon(1e-8));
        }
      }
    }
  }
}

TEST_CASE("hydrology/snow_tiles/state_swe_sca_calc") {
  // we would like this computation to be checked as close to the core code as possible,
  // so we arrange a minimum mockup using the classes (easy)
  parameter p;
  p.set_area_fractions(vector<double>{0.5, 0.5});
  pt_st_k::state_collector sc;
  // just to check, what happens on access to blank structure
  auto z_swe = sc.swe(p);
  auto z_sca = sc.sca(p);
  REQUIRE_EQ(z_swe.size(), 0); // should be null ts.
  REQUIRE_EQ(z_sca.size(), 0); // should be null ts.
  sc.time_axis.t = utctime_0;
  sc.time_axis.dt = from_seconds(3600);
  sc.time_axis.n = 2;
  sc.fw.emplace_back(sc.time_axis, vector<double>{0.0, 1.0}, ts_point_fx::POINT_INSTANT_VALUE);
  sc.fw.emplace_back(sc.time_axis, vector<double>{2.0, 2.0}, ts_point_fx::POINT_INSTANT_VALUE);
  sc.lw.emplace_back(sc.time_axis, vector<double>{0.0, 10.0}, ts_point_fx::POINT_INSTANT_VALUE);
  sc.lw.emplace_back(sc.time_axis, vector<double>{20.0, 20.0}, ts_point_fx::POINT_INSTANT_VALUE);
  // then we act:
  auto swe = sc.swe(p);
  auto sca = sc.sca(p);
  // and assert:
  REQUIRE_EQ(sc.time_axis, swe.ta);
  REQUIRE_EQ(sc.time_axis, sca.ta);
  for (size_t t = 0; t < sc.time_axis.size(); ++t) {
    CHECK_EQ(
      swe.v[t],
      doctest::Approx(
        p.area_fraction(0) * (sc.fw[0].v[t] + sc.lw[0].v[t]) + p.area_fraction(1) * (sc.fw[1].v[t] + sc.lw[1].v[t])));
  }
  CHECK_EQ(sca.v[0], doctest::Approx(0.5)); // 0.0 in the first interval, give only 0.1 contrib
  CHECK_EQ(sca.v[1], doctest::Approx(1.0)); // in the second interval, frozen snow available so 0.2 since we 'know' that
}

TEST_SUITE_END();
