#include <vector>
#include <string>
#include "test_pch.h"
#include <shyft/hydrology/stacks/pt_st_hbv.h>
#include <shyft/hydrology/cell_model.h>
#include <shyft/hydrology/stacks/pt_st_hbv_cell_model.h>
#include "hydro_mocks.h"
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time/utctime_utilities.h>
#include <energy_market/serialize_loop.h>
// Some typedefs for clarity
using namespace shyft::core;
using namespace shyft::time_series;
using namespace shyft::core::pt_st_hbv;

using namespace shyfttest::mock;
using namespace shyfttest;

namespace pt = shyft::core::priestley_taylor;
namespace st = shyft::core::snow_tiles;
namespace gm = shyft::core::glacier_melt;
namespace pc = shyft::core::precipitation_correction;
namespace ta = shyft::time_axis;
typedef TSPointTarget<ta::point_dt> catchment_t;

namespace shyfttest::mock {
  // need specialization for ptstk_response_t above
  template <>
  template <>
  void ResponseCollector<ta::fixed_dt>::collect<response>(size_t idx, response const & response) {
    _snow_output.set(idx, response.snow.outflow);
  }

  template <>
  template <>
  void DischargeCollector<ta::fixed_dt>::collect<response>(size_t idx, response const & response) {
    // q_avg is given in mm, so compute the totals
    avg_discharge.set(idx, destination_area * response.tank.q() / 1000.0 / 3600.0);
  }
}

#define DOCTEST_VALUE_PARAMETERIZED_DATA(data, data_container) \
  static size_t _doctest_subcase_idx = 0; \
  std::for_each(data_container.begin(), data_container.end(), [&](const auto& in) { \
    DOCTEST_SUBCASE((std::string(#data_container "[") + std::to_string(_doctest_subcase_idx++) + "]").c_str()) { \
      data = in; \
    } \
  }); \
  _doctest_subcase_idx = 0


TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/pt_st_hbv/call_stack") {
  xpts_t temp;
  xpts_t prec;
  xpts_t rel_hum;
  xpts_t wind_speed;
  xpts_t radiation;

  calendar cal;
  utctime t0 = cal.time(YMDhms(2014, 8, 1, 0, 0, 0));
  size_t n_ts_points = 3 * 24;
  utctimespan dt = deltahours(1);
  utctime t1 = t0 + n_ts_points * dt;
  shyfttest::create_time_series(temp, prec, rel_hum, wind_speed, radiation, t0, dt, n_ts_points);

  auto model_dt = deltahours(24);
  vector<utctime> times;
  for (utctime i = t0; i <= t1; i += model_dt)
    times.emplace_back(i);
  ta::fixed_dt time_axis(t0, dt, n_ts_points);
  ta::fixed_dt state_time_axis(t0, dt, n_ts_points + 1);
  // Initialize parameters
  // std::vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0}; // Zero cv distribution of snow (i.e. even)
  // std::vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  pt::parameter pt_param;
  st::parameter snow_param;
  hbv_soil::parameter soil_param;
  hbv_tank::parameter tank_param;
  pc::parameter p_corr_param;

  // Initialize the state vectors
  hbv_soil::state soil_state{};
  hbv_tank::state tank_state{};
  st::state snow_state;


  // Initialize collectors
  shyfttest::mock::ResponseCollector<ta::fixed_dt> response_collector(1000 * 1000, time_axis);
  // shyfttest::mock::StateCollector<ta::fixed_dt>
  state_collector state_collector{state_time_axis};

  state state{snow_state, soil_state, tank_state};
  parameter parameter(pt_param, snow_param, soil_param, tank_param, p_corr_param);
  geo_cell_data geo_cell_data;
  pt_st_hbv::run<direct_accessor, response>(
    geo_cell_data,
    parameter,
    time_axis,
    0,
    0,
    temp,
    prec,
    wind_speed,
    rel_hum,
    radiation,
    state,
    state_collector,
    response_collector);

  auto snow_swe = response_collector.snow_swe();
  for (size_t i = 0; i < snow_swe.size(); ++i)
    TS_ASSERT(std::isfinite(snow_swe.get(i).v) && snow_swe.get(i).v >= 0);
}

double integral_tot(dd::apoint_ts ts, ta::fixed_dt ta) {
  utctimespan total_period = ta.total_period().timespan();
  dd::gta_t ta_full{ta.start(), total_period, 1};
  return ts.integral(ta_full).value(0) / to_seconds(ts.time_axis().dt());
}

double integral_tot(pts_t ts, ta::fixed_dt ta) {
  return integral_tot(dd::apoint_ts(ts), ta);
}

dd::apoint_ts calc_hbv_lake_evap(hbv_tank::calculator calc, dd::apoint_ts temp_ts) {
  std::vector<double> v;
  v.reserve(temp_ts.size());
  for (double t : temp_ts.values()) {
    v.push_back(calc.calc_elake(t));
  }
  return dd::apoint_ts{temp_ts.time_axis(), v, time_series::ts_point_fx::POINT_AVERAGE_VALUE};
}

TEST_CASE("hydrology/pt_st_hbv/different_forcing_data") {
  double temp_air;
  double precip;
  SUBCASE("cold, no precip") {
    temp_air = -10;
    precip = 0;
  }
  SUBCASE("cold, + precip") {
    temp_air = -10;
    precip = 3;
  }
  SUBCASE("warm, no precip") {
    temp_air = 10;
    precip = 0;
  }
  SUBCASE("cold, + precip") {
    temp_air = 10;
    precip = 3;
  }

  CAPTURE(temp_air);
  CAPTURE(precip);

  // most general test: take a fraction of all important land types
  double lake_fraction = 0.1;
  double glacier_fraction = 0.2;
  double reservoir_fraction = 0.3;

  for (auto const & gm_direct_response_f : std::vector<double>{0, 0.2, 1}) {
    for (auto const & res_direct_response_f : std::vector<double>{0, 0.3, 1}) {
      CAPTURE(gm_direct_response_f);
      CAPTURE(res_direct_response_f);
      calendar cal;
      utctime t0 = cal.time(2014, 8, 1, 0, 0, 0);
      utctimespan dt = deltahours(1);
      int const n =
        1; // actually, water balance should be fullfilled for any one timestep, so need to calculate only one
      ta::fixed_dt tax(t0, dt, n);
      ta::fixed_dt tax_state(t0, dt, n + 1);
      pt::parameter pt_param;
      st::parameter st_param;
      hbv_soil::parameter soil_param;
      hbv_tank::parameter tank_param;
      pc::parameter p_corr_param;
      st_param.lwmax = 0.0; // ensure everything melts..
      parameter parameter{pt_param, st_param, soil_param, tank_param, p_corr_param};

      dd::apoint_ts temp(tax, temp_air, POINT_AVERAGE_VALUE);
      dd::apoint_ts prec(tax, precip, POINT_AVERAGE_VALUE);
      dd::apoint_ts rel_hum(tax, 0.8, POINT_AVERAGE_VALUE);
      dd::apoint_ts wind_speed(tax, 2.0, POINT_AVERAGE_VALUE);
      dd::apoint_ts radiation(tax, 300.0, POINT_AVERAGE_VALUE);

      hbv_soil::state soil_state{};
      hbv_tank::state tank_state{};
      st::state gs_state;

      state s0{gs_state, soil_state, tank_state};

      geo_cell_data gcd(geo_point(1000, 1000, 100));
      land_type_fractions ltf(
        glacier_fraction,
        lake_fraction,
        reservoir_fraction,
        0,
        1 - lake_fraction - glacier_fraction - reservoir_fraction); // glac; lake; reservoir; unspec
      gcd.set_land_type_fractions(ltf);

      pt_st_hbv::state_collector sc;
      pt_st_hbv::all_response_collector rc;
      double const cell_area = 1000 * 1000;
      sc.collect_state = true;
      sc.initialize(parameter, tax_state, 0, 0, cell_area, gcd.land_type_fractions_info().snow_storage());
      rc.initialize(tax, 0, 0, cell_area);

      parameter.msp.reservoir_direct_response_fraction = res_direct_response_f;
      parameter.gm.direct_response = gm_direct_response_f;
      pt_st_hbv::run<direct_accessor, pt_st_hbv::response>(
        gcd, parameter, tax, 0, 0, temp, prec, wind_speed, rel_hum, radiation, s0, sc, rc);

      double snow_coverable_fraction = gcd.land_type_fractions_info().snow_storage();
      auto snow_swe = sc.swe(parameter.st, snow_coverable_fraction);

      // calculate here, as it is not capture in response
      dd::apoint_ts hbv_evap_raw = calc_hbv_lake_evap(hbv_tank::calculator{tank_param, lake_fraction}, temp);
      auto hbv_evap_res_direct_corr = dd::min(hbv_evap_raw, prec) * res_direct_response_f * reservoir_fraction;

      auto m3s_to_mmh = [cell_area](double v) {
        return shyft::m3s_to_mmh(v, cell_area);
      };

      // 1) check total water balance
      double wb_total =
        (integral_tot(prec, tax)
         + m3s_to_mmh(integral_tot(rc.glacier_melt, tax)) // "extra source", since glacier state not taken into account
         - (snow_swe.value(n) - snow_swe.value(0)) - integral_tot(rc.soil_ae, tax) - integral_tot(rc.elake, tax)
         - (sc.soil_sm.value(n) - sc.soil_sm.value(0)) - (sc.tank_uz.value(n) - sc.tank_uz.value(0))
         - (sc.tank_lz.value(n) - sc.tank_lz.value(0)) - m3s_to_mmh(integral_tot(rc.avg_discharge, tax))
         - integral_tot(hbv_evap_res_direct_corr, tax));
      CHECK_EQ(doctest::Approx(wb_total), 0);

      // 2) check snow tiles water balance
      double wb_snow =
        (integral_tot(prec, tax) * snow_coverable_fraction - m3s_to_mmh(integral_tot(rc.snow_outflow, tax))
         - (snow_swe.value(n) - snow_swe.value(0)));
      CHECK_EQ(doctest::Approx(wb_snow), 0);

      // 3) water balance for soil moisture & ground water
      std::vector<double> snow_out_rescaled_v;
      snow_out_rescaled_v.reserve(rc.snow_outflow.size());
      for (double v : rc.snow_outflow.values()) {
        snow_out_rescaled_v.push_back(snow_coverable_fraction > 0 ? v / snow_coverable_fraction : 0);
      }
      dd::apoint_ts snow_out_rescaled =
        dd::apoint_ts{rc.snow_outflow.time_axis(), snow_out_rescaled_v, time_series::ts_point_fx::POINT_AVERAGE_VALUE};

      double land_fraction = 1 - lake_fraction - glacier_fraction - reservoir_fraction;
      double wb_hbvr =
        (m3s_to_mmh(integral_tot(snow_out_rescaled, tax)) * land_fraction
         + integral_tot(prec, tax) * (lake_fraction + reservoir_fraction)
         + m3s_to_mmh(integral_tot(rc.glacier_melt, tax))
         + m3s_to_mmh(integral_tot(snow_out_rescaled, tax)) * glacier_fraction - integral_tot(rc.soil_ae, tax)
         - integral_tot(rc.elake, tax) - (sc.soil_sm.value(n) - sc.soil_sm.value(0))
         - (sc.tank_uz.value(n) - sc.tank_uz.value(0)) - (sc.tank_lz.value(n) - sc.tank_lz.value(0))
         - m3s_to_mmh(integral_tot(rc.avg_discharge, tax)) - integral_tot(hbv_evap_res_direct_corr, tax));
      CHECK_EQ(doctest::Approx(wb_hbvr), 0);
    }
  }
}

std::tuple<pt_st_hbv::state_collector, pt_st_hbv::all_response_collector, ta::fixed_dt, int> prepare_tests(
  double temp_air,
  double precip,
  double res_direct_response_f,
  double gm_direct_response_f,
  land_type_fractions ltf) {
  calendar cal;
  utctime t0 = cal.time(2014, 8, 1, 0, 0, 0);
  utctimespan dt = deltahours(1);
  int const n = 1; // actually, should be fullfilled for any one timestep, so need to calculate only one
  ta::fixed_dt tax(t0, dt, n);
  ta::fixed_dt tax_state(t0, dt, n + 1);
  pt::parameter pt_param;
  st::parameter st_param;
  hbv_soil::parameter soil_param;
  hbv_tank::parameter tank_param;
  pc::parameter p_corr_param;
  st_param.lwmax = 0.0; // ensure everything melts..
  parameter parameter{pt_param, st_param, soil_param, tank_param, p_corr_param};

  dd::apoint_ts temp(tax, temp_air, POINT_AVERAGE_VALUE);
  dd::apoint_ts prec(tax, precip, POINT_AVERAGE_VALUE);
  dd::apoint_ts rel_hum(tax, 0.8, POINT_AVERAGE_VALUE);
  dd::apoint_ts wind_speed(tax, 2.0, POINT_AVERAGE_VALUE);
  dd::apoint_ts radiation(tax, 300.0, POINT_AVERAGE_VALUE);

  hbv_soil::state soil_state{};
  hbv_tank::state tank_state{};
  st::state gs_state;

  state s0{gs_state, soil_state, tank_state};

  geo_cell_data gcd(geo_point(1000, 1000, 100));
  gcd.set_land_type_fractions(ltf);

  pt_st_hbv::state_collector sc;
  pt_st_hbv::all_response_collector rc;
  double const cell_area = 1000 * 1000;
  sc.collect_state = true;
  sc.initialize(parameter, tax_state, 0, 0, cell_area, gcd.land_type_fractions_info().snow_storage());
  rc.initialize(tax, 0, 0, cell_area);

  parameter.msp.reservoir_direct_response_fraction = res_direct_response_f;
  parameter.gm.direct_response = gm_direct_response_f;
  pt_st_hbv::run<direct_accessor, pt_st_hbv::response>(
    gcd, parameter, tax, 0, 0, temp, prec, wind_speed, rel_hum, radiation, s0, sc, rc);
  return std::make_tuple(sc, rc, tax, n);
}

TEST_CASE("hydrology/pt_st_hbv/gm_direct_response_f") {
  // no storage in tank if everything is directly routed
  double gm_direct_response_f;
  double res_direct_response_f = 1;
  SUBCASE("gm all routed") {
    gm_direct_response_f = 0;
  }
  SUBCASE("gm all direct") {
    gm_direct_response_f = 1;
  }

  CAPTURE(gm_direct_response_f);

  // most general test: take a fraction of all important land types
  double temp_air = 10;
  double precip = 0;
  double lake_fraction = 0.;
  double glacier_fraction = 1;
  double reservoir_fraction = 0;
  land_type_fractions ltf(
    glacier_fraction, lake_fraction, reservoir_fraction, 0, 1 - lake_fraction - glacier_fraction - reservoir_fraction);
  auto [sc, rc, tax, n] = prepare_tests(temp_air, precip, res_direct_response_f, gm_direct_response_f, ltf);

  // 1) check total water balance
  double delta_sm = sc.soil_sm.value(n) - sc.soil_sm.value(0);
  double delta_uz = sc.tank_uz.value(n) - sc.tank_uz.value(0);
  double delta_lz = sc.tank_lz.value(n) - sc.tank_lz.value(0);

  double total_discharge = integral_tot(rc.avg_discharge, tax);

  if (gm_direct_response_f == 1) {
    CHECK_EQ(delta_sm, 0);
    CHECK_EQ(delta_uz, 0);
    CHECK_EQ(delta_lz, 0);
    CHECK_GT(total_discharge, 0); // discharge due to gm
  } else {
    CHECK_GT(delta_uz + delta_lz, 0);
  }
}

TEST_CASE("hydrology/pt_st_hbv/res_direct_response_f") {
  // no storage in tank if everything is directly routed
  double gm_direct_response_f = 1;
  double res_direct_response_f;
  SUBCASE("res all routed") {
    res_direct_response_f = 0;
  }
  SUBCASE("res all direct") {
    res_direct_response_f = 1;
  }

  CAPTURE(res_direct_response_f);

  // most general test: take a fraction of all important land types
  double temp_air = 10;
  double precip = 10;
  double lake_fraction = 0.;
  double glacier_fraction = 0;
  double reservoir_fraction = 1;
  land_type_fractions ltf(
    glacier_fraction, lake_fraction, reservoir_fraction, 0, 1 - lake_fraction - glacier_fraction - reservoir_fraction);
  auto [sc, rc, tax, n] = prepare_tests(temp_air, precip, res_direct_response_f, gm_direct_response_f, ltf);

  // 1) check total water balance
  double delta_sm = sc.soil_sm.value(n) - sc.soil_sm.value(0);
  double delta_uz = sc.tank_uz.value(n) - sc.tank_uz.value(0);
  double delta_lz = sc.tank_lz.value(n) - sc.tank_lz.value(0);

  double total_discharge = integral_tot(rc.avg_discharge, tax);

  if (res_direct_response_f == 1) {
    CHECK_EQ(delta_sm, 0);
    CHECK_EQ(delta_uz, 0);
    CHECK_EQ(delta_lz, 0);
    CHECK_GT(total_discharge, 0); // discharge due to rain
  } else {
    CHECK_GT(delta_lz, 0);
  }
}

TEST_CASE("hydrology/pt_st_hbv/serialization") {
  pt::parameter pt_param{0.21, 1.33};
  st::parameter gs_param{
    1.999, 0.1, 0.9, 1.1, 0.12, 0.51, {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.09, 0.11, 0.09, 0.1}
  };
  hbv_soil::parameter s_param{};
  hbv_tank::parameter t_param{};
  pc::parameter p_corr_param{1.5};
  parameter a{pt_param, gs_param, s_param, t_param, p_corr_param};
  auto b = test::serialize_loop(a);
  FAST_CHECK_EQ(b, a);
}

TEST_SUITE_END();
