#include "test_pch.h"
#include <shyft/hydrology/methods/skaugen.h>

using namespace shyft::core::skaugen;
using namespace shyft::core;
typedef calculator SkaugenModel;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/skaugen/equal_operator") {
  double const d_range = 113.0;
  double const unit_size = 0.1;
  double const alpha_0 = 40.77;
  double const max_water_fraction = 0.1;
  double const tx = 0.16;
  double const cx = 2.50;
  double const ts = 0.14;
  double const cfr = 0.01;

  parameter p(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr);
  parameter p1(alpha_0 + 1.0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr);
  parameter p2(alpha_0, d_range + 1.0, unit_size, max_water_fraction, tx, cx, ts, cfr);
  parameter p3(alpha_0, d_range, unit_size + 1.0, max_water_fraction, tx, cx, ts, cfr);
  parameter p4(alpha_0, d_range, unit_size, max_water_fraction + 1.0, tx, cx, ts, cfr);
  parameter p5(alpha_0, d_range, unit_size, max_water_fraction, tx + 1.0, cx, ts, cfr);
  parameter p6(alpha_0, d_range, unit_size, max_water_fraction, tx, cx + 1.0, ts, cfr);
  parameter p7(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts + 1.0, cfr);
  parameter p8(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr + 1.0);


  TS_ASSERT(p != p1);
  TS_ASSERT(p != p2);
  TS_ASSERT(p != p3);
  TS_ASSERT(p != p4);
  TS_ASSERT(p != p5);
  TS_ASSERT(p != p6);
  TS_ASSERT(p != p7);
  TS_ASSERT(p != p8);

  p1.alpha_0 = alpha_0;
  p2.d_range = d_range;
  p3.unit_size = unit_size;
  p4.max_water_fraction = max_water_fraction;
  p5.tx = tx;
  p6.cx = cx;
  p7.ts = ts;
  p8.cfr = cfr;

  TS_ASSERT(p == p1);
  TS_ASSERT(p == p2);
  TS_ASSERT(p == p3);
  TS_ASSERT(p == p4);
  TS_ASSERT(p == p5);
  TS_ASSERT(p == p6);
  TS_ASSERT(p == p7);
  TS_ASSERT(p == p8);
}

TEST_CASE("hydrology/skaugen/accumulation") {
  // Model parameters
  double const d_range = 113.0;
  double const unit_size = 0.1;
  double const alpha_0 = 40.77;
  double const max_water_fraction = 0.1;
  double const tx = 0.16;
  double const cx = 2.50;
  double const ts = 0.14;
  double const cfr = 0.01;
  parameter p(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr);

  // Model state variables
  double const alpha = alpha_0;
  double const nu = alpha_0 * unit_size;
  double const sca = 0.0;
  double const swe = 0.0;
  double const free_water = 0.0;
  double const residual = 0.0;
  unsigned long const nnn = 0;
  state s(nu, alpha, sca, swe, free_water, residual, nnn);

  // Model input
  utctimespan dt = seconds(60 * 60);
  double temp = -10.0;
  double prec = 10.0;
  double radiation = 0.0;
  double wind_speed = 0.0;
  std::vector<std::pair<double, double>> tp(10, std::pair<double, double>(temp, prec));

  // Accumulate snow
  SkaugenModel model;
  response r;
  for_each(tp.begin(), tp.end(), [&dt, &p, &model, &radiation, &wind_speed, &s, &r](std::pair<double, double> pair) {
    model.step(dt, p, pair.first, pair.second, radiation, wind_speed, s, r);
  });
  TS_ASSERT_DELTA(s.swe * s.sca, prec * tp.size(), 1.0e-6);
  TS_ASSERT_DELTA(s.sca, 1.0, 1.0e-6);
  TS_ASSERT(s.nu < alpha_0 * unit_size);
}

TEST_CASE("hydrology/skaugen/melt") {
  // Model parameters
  double const d_range = 113.0;
  double const unit_size = 0.1;
  double const alpha_0 = 40.77;
  double const max_water_fraction = 0.1;
  double const tx = 0.16;
  double const cx = 2.50;
  double const ts = 0.14;
  double const cfr = 0.01;
  parameter p(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr);

  // Model state variables
  double const alpha = alpha_0;
  double const nu = alpha_0 * unit_size;
  double const sca = 0.0;
  double const swe = 0.0;
  double const free_water = 0.0;
  double const residual = 0.0;
  unsigned long const nnn = 0;
  state s(nu, alpha, sca, swe, free_water, residual, nnn);

  // Model input
  utctimespan dt = seconds(24 * 60 * 60);
  double temp = -10.0;
  double prec = 10.0 / 24.0;
  double radiation = 0.0;
  double wind_speed = 0.0;
  std::vector<std::pair<double, double>> tp(10, std::pair<double, double>(temp, prec));

  // Accumulate snow
  SkaugenModel model;
  response r;
  for_each(tp.begin(), tp.end(), [&dt, &p, &model, &radiation, &wind_speed, &s, &r](std::pair<double, double> pair) {
    model.step(dt, p, pair.first, pair.second, radiation, wind_speed, s, r);
  });

  double const total_water = s.swe * s.sca;
  double agg_outflow{0.0}; // For checking mass balance

  // Single melt event
  tp = std::vector<std::pair<double, double>>(
    1, std::pair<double, double>{10.0, 0.0}); // No precip, but 10.0 degrees for one day
  for_each(
    tp.begin(),
    tp.end(),
    [&dt, &p, &model, &radiation, &wind_speed, &s, &r, &agg_outflow](std::pair<double, double> pair) {
      model.step(dt, p, pair.first, pair.second, radiation, wind_speed, s, r);
      agg_outflow += r.outflow * 24.0; // dt=24h, r.outflow is in mm/h
    });
  double const total_water_after_melt = s.sca * (s.swe + s.free_water);
  TS_ASSERT_LESS_THAN(total_water_after_melt, total_water); // Less water after melt due to runoff
  TS_ASSERT(r.outflow * 24.0 + s.free_water >= 1.0);        // Some runoff or free water in snow
  TS_ASSERT_DELTA(r.outflow * 24.0 + s.sca * (s.free_water + s.swe), total_water, 1.0e-6);

  // One hundred melt events, that should melt everything
  tp = std::vector<std::pair<double, double>>(100, std::pair<double, double>(10.0, 0.0));
  for_each(
    tp.begin(),
    tp.end(),
    [&dt, &p, &model, &radiation, &wind_speed, &s, &r, &agg_outflow](std::pair<double, double> pair) {
      model.step(dt, p, pair.first, pair.second, radiation, wind_speed, s, r);
      agg_outflow += r.outflow * 24.0; // mm/h *24h-> mm
    });

  TS_ASSERT_DELTA(s.sca, 0.0, 1.0e-6);
  TS_ASSERT_DELTA(s.swe, 0.0, 1.0e-6);
  TS_ASSERT_DELTA(agg_outflow, total_water, 1.0e-10);
  TS_ASSERT_DELTA(s.alpha, alpha_0, 1.0e-6);
  TS_ASSERT_DELTA(s.nu, alpha_0 * unit_size, 1.0e-6);
}

TEST_CASE("hydrology/skaugen/lwc") {
  // Model parameters
  double const d_range = 113.0;
  double const unit_size = 0.1;
  double const alpha_0 = 40.77;
  double const max_water_fraction = 0.1;
  double const tx = 0.16;
  double const cx = 2.50;
  double const ts = 0.14;
  double const cfr = 0.01;
  parameter p(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr);

  // Model state variables
  double const alpha = alpha_0;
  double const nu = alpha_0 * unit_size;
  double const sca = 0.0;
  double const swe = 0.0;
  double const free_water = 0.0;
  double const residual = 0.0;
  unsigned long const nnn = 0;
  state s(nu, alpha, sca, swe, free_water, residual, nnn);

  // Model input
  utctimespan dt = seconds(24 * 60 * 60);
  double temp = -10.0;
  double prec = 10.0 / 24.0;
  double radiation = 0.0;
  double wind_speed = 0.0;
  std::vector<std::pair<double, double>> tp(10, std::pair<double, double>(temp, prec));

  // Accumulate snow
  SkaugenModel model;
  response r;
  for_each(tp.begin(), tp.end(), [&dt, &p, &model, &radiation, &wind_speed, &s, &r](std::pair<double, double> pair) {
    model.step(dt, p, pair.first, pair.second, radiation, wind_speed, s, r);
  });

  TS_ASSERT_DELTA(s.free_water, 0.0, 1.0e-6); // No free water when dry snow precip
  model.step(dt, p, 10.0, 0.0, radiation, wind_speed, s, r);
  TS_ASSERT(
    s.free_water
    <= s.swe * max_water_fraction); // Can not have more free water in the snow than the capacity of the snowpack
  tp = std::vector<std::pair<double, double>>(5, std::pair<double, double>(2.0, 0.0));
  for_each(tp.begin(), tp.end(), [&dt, &p, &model, &radiation, &wind_speed, &s, &r](std::pair<double, double> pair) {
    model.step(dt, p, pair.first, pair.second, radiation, wind_speed, s, r);
  });
  TS_ASSERT_DELTA(s.free_water, s.swe * max_water_fraction, 1.0e-6); // Test that snow is saturated with free water
}

TEST_CASE("hydrology/skaugen/meltdown") {

  // Model parameters
  double const d_range = 113.0;
  double const unit_size = 0.1;
  double const alpha_0 = 40.77;
  double const max_water_fraction = 0.1;
  double const tx = 0.16;
  double const cx = 2.50;
  double const ts = 0.14;
  double const cfr = 0.01;
  parameter p(alpha_0, d_range, unit_size, max_water_fraction, tx, cx, ts, cfr);

  // Model state variables before melt-down
  double const alpha = 0.127852277312898;
  double const nu = 0.012785227731289801;
  double const sca = 0.005033599471562574;
  double const swe = 32.1;
  double const free_water = 3.21;
  double const residual = 0.0;
  unsigned long const nnn = 321;
  state s(nu, alpha, sca, swe, free_water, residual, nnn);

  // Model input
  utctimespan dt = seconds(3 * 3600);
  double temp = 4.891358376624782;
  double prec = 0.0010356738461072955;
  double radiation = 0.0042811137986584116;
  double wind_speed = 6.411016839448434;
  std::vector<std::pair<double, double>> tp(2, std::pair<double, double>(temp, prec));

  // melt it
  SkaugenModel model;
  response r;
  model.step(dt, p, temp, prec, radiation, wind_speed, s, r);
  TS_ASSERT(true);
}

TEST_SUITE_END();
