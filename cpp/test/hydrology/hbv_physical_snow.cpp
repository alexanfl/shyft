
#include "test_pch.h"
#include <shyft/hydrology/methods/hbv_physical_snow.h>


using namespace shyft::core;
using namespace shyft::core::hbv_physical_snow;
using namespace std;

using SnowModel = calculator;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/hbv_physical_snow_equal_operator") {
  vector<double> s = {1.0, 1.0, 1.0};
  vector<double> i = {0, 0.5, 1.0};
  double tx = 0.0;
  double lw = 0.1;
  double cfr = 0.5;
  double wind_scale = 2.0;
  double wind_const = 1.0;
  double surface_magnitude = 30.0;
  double max_albedo = 0.9;
  double min_albedo = 0.6;
  double fast_albedo_decay_rate = 5.0;
  double slow_albedo_decay_rate = 5.0;
  double snowfall_reset_depth = 5.0;
  bool calculate_iso_pot_energy = false;
  vector<double> s2 = {2.0, 2.0, 2.0, 2.0};
  vector<double> i2 = {0, 1.0};

  parameter p(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p1(
    s2,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p2(
    s,
    i2,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p3(
    s,
    i,
    tx + 1.0,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p4(
    s,
    i,
    tx,
    lw + 1.0,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p5(
    s,
    i,
    tx,
    lw,
    cfr + 1.0,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p6(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale + 1.0,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p7(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const + 1.0,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p8(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude + 1.0,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p9(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo + 1.0,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p10(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo + 1.0,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p11(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate + 1.0,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p12(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate + 1.0,
    snowfall_reset_depth,
    calculate_iso_pot_energy);
  parameter p13(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth + 1.0,
    calculate_iso_pot_energy);
  parameter p14(
    s,
    i,
    tx,
    lw,
    cfr,
    wind_scale,
    wind_const,
    surface_magnitude,
    max_albedo,
    min_albedo,
    fast_albedo_decay_rate,
    slow_albedo_decay_rate,
    snowfall_reset_depth,
    calculate_iso_pot_energy + 1.0);

  TS_ASSERT(p != p1);
  TS_ASSERT(p != p2);
  TS_ASSERT(p != p3);
  TS_ASSERT(p != p4);
  TS_ASSERT(p != p5);
  TS_ASSERT(p != p6);
  TS_ASSERT(p != p7);
  TS_ASSERT(p != p8);
  TS_ASSERT(p != p9);
  TS_ASSERT(p != p10);
  TS_ASSERT(p != p11);
  TS_ASSERT(p != p12);
  TS_ASSERT(p != p13);
  TS_ASSERT(p != p14);

  p1.s = s;
  p2.intervals = i;
  p3.tx = tx;
  p4.lw = lw;
  p5.cfr = cfr;
  p6.wind_scale = wind_scale;
  p7.wind_const = wind_const;
  p8.surface_magnitude = surface_magnitude;
  p9.max_albedo = max_albedo;
  p10.min_albedo = min_albedo;
  p11.fast_albedo_decay_rate = fast_albedo_decay_rate;
  p12.slow_albedo_decay_rate = slow_albedo_decay_rate;
  p13.snowfall_reset_depth = snowfall_reset_depth;
  p14.calculate_iso_pot_energy = false;

  TS_ASSERT(p == p1);
  TS_ASSERT(p == p2);
  TS_ASSERT(p == p3);
  TS_ASSERT(p == p4);
  TS_ASSERT(p == p5);
  TS_ASSERT(p == p6);
  TS_ASSERT(p == p7);
  TS_ASSERT(p == p8);
  TS_ASSERT(p == p9);
  TS_ASSERT(p == p10);
  TS_ASSERT(p == p11);
  TS_ASSERT(p == p12);
  TS_ASSERT(p == p13);
  TS_ASSERT(p == p14);
}

TEST_CASE("hydrology/hbv_physical_snow_integral_calculations") {
  vector<double> f = {0.0, 0.5, 1.0};
  vector<double> x = {0.0, 0.5, 1.0};

  double pt0 = 0.25;
  double pt1 = 0.75;
  TS_ASSERT_DELTA(integrate(f, x, x.size(), 0.0, 1.0), 0.5, 1.0e-12);
  TS_ASSERT_DELTA(
    integrate(f, x, x.size(), 0.0, pt0) + integrate(f, x, x.size(), pt0, 1.0),
    integrate(f, x, x.size(), 0.0, 1.0),
    1.0e-12);

  TS_ASSERT_DELTA(
    integrate(f, x, x.size(), pt0, pt1),
    integrate(f, x, x.size(), 0.0, 1.0) - (integrate(f, x, x.size(), 0.0, pt0) + integrate(f, x, x.size(), pt1, 1.0)),
    1.0e-12);
}

TEST_CASE("hydrology/hbv_physical_snow_mass_balance_at_snowpack_reset") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;
  double precipitation = 0.04;
  double temperature = 1.0;
  double sca = 1.0;
  double swe = 0.05;
  state.swe = swe;
  state.sca = sca;

  vector<double> albedo(5, 0.6);
  vector<double> iso_pot_energy(5, 1752.56396484375);
  state.albedo = albedo;
  state.iso_pot_energy = iso_pot_energy;
  state.surface_heat = 0.0;

  auto dt = shyft::core::deltahours(1);
  double rad = 10.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;

  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);

  state.distribute(p);

  snow_model.step(state, r, utctime(dt * 24 * 320), dt, temperature, rad, precipitation, wind_speed, rel_hum);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
}

TEST_CASE("hydrology/hbv_physical_snow_mass_balance_at_snowpack_buildup") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  double precipitation = 0.15;
  double temperature = -1.0;
  double sca = 0.6;
  double swe = 0.2;
  state.swe = swe;
  state.sca = sca;
  vector<double> albedo(5, 0.6);
  vector<double> iso_pot_energy(5, 1752.56396484375);
  state.albedo = albedo;
  state.iso_pot_energy = iso_pot_energy;
  state.surface_heat = 0.0;

  auto dt = shyft::core::deltahours(1);
  double rad = 10.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;

  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);
  state.distribute(p);
  snow_model.step(state, r, utctime(dt * 24 * 320), dt, temperature, rad, precipitation, wind_speed, rel_hum);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
  state.swe = 0.2;
  state.sca = 0.6;
  temperature = p.tx; // special check fo tx
  SnowModel snow_model2(p);
  state.distribute(p);
  snow_model2.step(state, r, utctime(dt * 24 * 320), dt, temperature, rad, precipitation, wind_speed, rel_hum);
  TS_ASSERT_DELTA(total_water_before, state.swe + r.outflow, 1.0e-8);
}

TEST_CASE("hydrology/hbv_physical_snow_mass_balance_rain_no_snow") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  double precipitation = 0.15;
  double temperature = p.tx;
  double sca = 0.0;
  double swe = 0.0;
  state.swe = swe;
  state.sca = sca;

  vector<double> albedo(5, 0.6);
  vector<double> iso_pot_energy(5, 1752.56396484375);
  state.albedo = albedo;
  state.iso_pot_energy = iso_pot_energy;
  state.surface_heat = 0.0;

  auto dt = shyft::core::deltahours(1);
  double rad = 10.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;

  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);
  state.distribute(p);
  snow_model.step(state, r, utctime(dt * 24 * 320), dt, temperature, rad, precipitation, wind_speed, rel_hum);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
  TS_ASSERT_DELTA(state.sca, 0.0, 1.0e-8);
  TS_ASSERT_DELTA(state.swe, 0.0, 1.0e-8);
}

TEST_CASE("hydrology/hbv_physical_snow_mass_balance_melt_no_precip") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  double precipitation = 0.0;
  double temperature = 3.0;
  double sca = 0.5;
  double swe = 10.0;
  state.swe = swe;
  state.sca = sca;
  vector<double> albedo(5, 0.6);
  vector<double> iso_pot_energy(5, 1752.56396484375);
  state.albedo = albedo;
  state.iso_pot_energy = iso_pot_energy;
  state.surface_heat = 0.0;

  auto dt = shyft::core::deltahours(1);
  double rad = 10.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;


  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);
  state.distribute(p);

  snow_model.step(state, r, utctime(dt * 24 * 320), dt, temperature, rad, precipitation, wind_speed, rel_hum);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
}

TEST_SUITE_END();
