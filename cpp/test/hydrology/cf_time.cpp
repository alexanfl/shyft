#include "test_pch.h"
#include <shyft/time/utctime_utilities.h>

using shyft::core::utcperiod;
using shyft::core::from_seconds;

namespace shyft::hydrology::grammar {
  utcperiod parse_cf_time(char const *);
}

using shyft::hydrology::grammar::parse_cf_time;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/cf_time_no_z") {
  auto a = parse_cf_time("hours since 1970-01-01 00:00:00");
  CHECK(a.valid());
  CHECK_EQ(a, utcperiod(from_seconds(0.0), from_seconds(3600)));
}

TEST_CASE("hydrology/cf_time_with_z") {
  auto a = parse_cf_time("minutes since 1970-01-01 00:00:00Z");
  CHECK(a.valid());
  CHECK_EQ(a, utcperiod(from_seconds(0.0), from_seconds(60)));
}

TEST_CASE("hydrology/cf_time_with_plus_1h") {
  auto a = parse_cf_time("hours since 1970-01-01 00:00:00+01:00");
  CHECK(a.valid());
  CHECK_EQ(a, utcperiod(from_seconds(3600), from_seconds(2 * 3600)));
}

TEST_CASE("hydrology/cf_time_with_minus_1h_only") {
  auto a = parse_cf_time("days since 1970-01-01 00:00:00-01");
  CHECK(a.valid());
  CHECK_EQ(a, utcperiod(from_seconds(-3600), from_seconds(-3600 + 24 * 3600)));
}

TEST_CASE("hydrology/cf_time_with_space_before_tz") {
  auto a = parse_cf_time("days since 1970-01-01 00:00:00 -01:00");
  CHECK(a.valid());
  CHECK_EQ(a, utcperiod(from_seconds(-3600), from_seconds(-3600 + 24 * 3600)));
}

TEST_CASE("hydrology/cf_time_with_T_separator") {
  auto a = parse_cf_time("days since 1970-01-01T00:00:00 -01:00");
  CHECK(a.valid());
  CHECK_EQ(a, utcperiod(from_seconds(-3600), from_seconds(-3600 + 24 * 3600)));
}

TEST_CASE("hydrology/cf_time_invalid_nullptr") {
  auto a = parse_cf_time(nullptr);
  CHECK(!a.valid());
}

TEST_CASE("hydrology/cf_time_invalid_empty") {
  auto a = parse_cf_time("");
  CHECK(!a.valid());
}

TEST_CASE("hydrology/cf_time_invalid_format_1") {
  auto a = parse_cf_time("hours 1970-01-01");
  CHECK(!a.valid());
}

TEST_CASE("hydrology/cf_time_invalid_format_2") {
  auto a = parse_cf_time("hours since 1970-01-01");
  CHECK(!a.valid());
}

TEST_CASE("hydrology/cf_time_invalid_format_3") {
  auto a = parse_cf_time("ticks since 1970-01-01");
  CHECK(!a.valid());
}

TEST_CASE("hydrology/cf_old_time") {
  auto a = parse_cf_time("days since 0851-01-01 00:00:00");
  CHECK(a.valid());
}

TEST_SUITE_END();
