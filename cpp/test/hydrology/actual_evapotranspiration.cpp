#include "test_pch.h"
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/time/utctime_utilities.h>

namespace shyfttest {
  double const EPS = 1.0e-8;
}

using namespace shyft::core;
using namespace shyft::core::actual_evapotranspiration;
TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/aevapo/equal_operator") {
  parameter ae1;
  parameter ae2{1.5};
  parameter ae3{2.0};

  TS_ASSERT(ae1 == ae2);
  TS_ASSERT(ae1 != ae3);
  TS_ASSERT(ae2 != ae3);

  ae2.ae_scale_factor = 2.0;

  TS_ASSERT(ae1 != ae2);
  TS_ASSERT(ae1 != ae3);
  TS_ASSERT(ae2 == ae3);
}

TEST_CASE("hydrology/aevapo/water") {
  double const sca = 0.0;
  double const pot_evap = 5.0; // [mm/h]
  double const scale_factor = 1.0;
  auto const dt = deltahours(3);
  double act_evap;
  act_evap = calculate_step(0.0, pot_evap, scale_factor, sca, dt);
  TS_ASSERT_DELTA(act_evap, 0.0, shyfttest::EPS);

  act_evap = calculate_step(1.0e8, pot_evap, scale_factor, sca, dt);
  TS_ASSERT_DELTA(act_evap, pot_evap, shyfttest::EPS);

  TS_ASSERT_DELTA((1 - exp(-3 * 1.0 / 1.0)), calculate_step(1.0, 1.0, 1.0, 0.0, dt), shyfttest::EPS);
}

TEST_CASE("hydrology/aevapo/snow") {
  double const water = 5.0;
  double const pot_evap = 5.0; // [mm/h]
  double const scale_factor = 1.0;
  auto const dt = deltahours(1);
  double act_evap_no_snow = calculate_step(water, pot_evap, scale_factor, 0.0, dt);
  double act_evap_some_snow = calculate_step(water, pot_evap, scale_factor, 0.1, dt);

  TS_ASSERT(act_evap_no_snow > act_evap_some_snow);
}

TEST_CASE("hydrology/aevapo/scale_factor") {
  double const water = 5.0;
  double const sca = 0.5;
  double const pot_evap = 5.0; // [mm/h]
  auto const dt = deltahours(1);
  double act_evap_small_scale = calculate_step(water, pot_evap, 0.5, sca, dt);
  double act_evap_large_scale = calculate_step(water, pot_evap, 1.5, sca, dt);

  TS_ASSERT(act_evap_small_scale > act_evap_large_scale);
}

TEST_SUITE_END();
