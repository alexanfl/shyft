#include "test_pch.h"
#include <shyft/hydrology/optimizers/sceua_optimizer.h>
// #include <iosfwd>
using namespace std;
using namespace shyft::core::optimizer;

struct x2_fx : public ifx {
  size_t n_eval = 0;

  double evaluate(vector<double> const & xv) {
    double y = 0;
    for (auto x : xv)
      y += x * x;
    n_eval++;
    return y;
  }
};

struct fx_complex : public ifx {
  size_t n_eval = 0;

  double evaluate(vector<double> const & xv) {
    double r2 = 0.0;
    for (auto x : xv)
      r2 += x * x;
    double r = sqrt(r2);
    ++n_eval;
    double y = 1.0 + pow(r / 3.1415, 3) + cos(r); /// range 0.702.. +oo, minimum r=2.5,
    return y;
  }
};

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/sceua/basic") {
  sceua opt;
  const size_t n = 2;
  double x[2] = {-1.0, 2.5};
  double x_min[2] = {-10, -10};
  double x_max[2] = {10, 10.0};
  double const eps = 1e-6;
  double x_eps[2] = {eps, eps};
  double y = -1;
  x2_fx f_basic;
  const size_t max_iterations = 20000;
  auto r = opt.find_min(n, x_min, x_max, x, y, f_basic, eps, -1, -2, x_eps, max_iterations);
  TS_ASSERT_LESS_THAN(f_basic.n_eval, max_iterations);
  TS_ASSERT_EQUALS(r, OptimizerState::FinishedXconvergence);
  TS_ASSERT_DELTA(y, 0.0, 1e-5);
  TS_ASSERT_DELTA(x[0], 0.0, 1e-5);
  TS_ASSERT_DELTA(x[1], 0.0, 1e-5);
}

TEST_CASE("hydrology/sceua/complex") {
  sceua opt;
  const size_t n = 2;
  double x[2] = {-4.01, -7.5};
  double x_min[2] = {-10, -10};
  double x_max[2] = {10, 10.0};
  double const eps = 1e-5;
  double x_eps[2] = {eps, eps};
  double y = -1;
  double y_eps = 1e-3;
  fx_complex f_complex;
  const size_t max_iterations = 150000;
  auto r = opt.find_min(n, x_min, x_max, x, y, f_complex, y_eps, -1, -2, x_eps, max_iterations);
  TS_ASSERT_LESS_THAN(f_complex.n_eval, max_iterations + 1000);
  TS_ASSERT_EQUALS(r, OptimizerState::FinishedFxConvergence);
  TS_ASSERT_DELTA(y, 0.7028, 1e-3);
  double rr = sqrt(x[0] * x[0] + x[1] * x[1]);
  TS_ASSERT_DELTA(rr, 2.5, 1e-2);
  f_complex.n_eval = 0;
  x[0] = -4.01;
  x[1] = -7.5;
  r = opt.find_min(n, x_min, x_max, x, y, f_complex, eps, -1, -2, x_eps, 100);
  TS_ASSERT_EQUALS(r, OptimizerState::FinishedMaxIterations);
}

TEST_SUITE_END();
