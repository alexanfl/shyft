#include <doctest/doctest.h>
#include <shyft/core/epsg_fast.h>
#include <boost/geometry.hpp>
#include <boost/geometry/srs/epsg.hpp>
#include <boost/geometry/algorithms/distance.hpp>

namespace bg = boost::geometry;

namespace {
  namespace bg = boost::geometry;
  using point_ll =
    bg::model::point<double, 2, bg::cs::geographic<bg::degree> >; ///< point (latitude,longitude),  epsg 4326 ,wgs 84
  using point_xy =
    bg::model::point<double, 2, bg::cs::cartesian>; /// < x and y of geo_point, in some metric epsg zone, utm.. etc.

  template <class pxy>
  pxy fast_epsg_map(pxy a, int from_epsg, int to_epsg) {
    if (from_epsg == to_epsg) {
      return a;
    } else {
      bg::srs::projection<> a_prj{bg::srs::epsg_fast{from_epsg}};
      point_ll a_ll;
      a_prj.inverse(a, a_ll);
      bg::srs::projection<> r_prj{bg::srs::epsg_fast{to_epsg}};
      pxy r;
      r_prj.forward(a_ll, r);
      return r;
    }
  }

  template <class pxy, int from_epsg, int to_epsg>
  pxy static_epsg_map(pxy a) {
    if constexpr (from_epsg == to_epsg) {
      return a;
    } else {
      bg::srs::projection<bg::srs::static_epsg<from_epsg>> a_prj{};
      point_ll a_ll;
      a_prj.inverse(a, a_ll);
      bg::srs::projection<bg::srs::static_epsg<to_epsg>> r_prj{};
      pxy r;
      r_prj.forward(a_ll, r);
      return r;
    }
  }

}

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/epsg_fast_all") {
  auto all_epsg_codes = bg::projections::detail::all_epsg_codes();
  for (auto code : all_epsg_codes) {
    try {
      auto p = bg::projections::detail::epsg_to_parameters_fast(code);
      CHECK(!p.empty());
    } catch (std::runtime_error const & re) {
      throw std::runtime_error("While processing code " + std::to_string(code) + ", got ex" + re.what());
    }
  }
}

TEST_CASE("hydrology/epsg_fast_regression_vs_boost") {
  point_xy p32633{254807.03, 6784125.07};
  point_xy p32632{578006.66, 6776452.38};
  auto a1 = fast_epsg_map(p32633, 32633, 32632);
  auto a2 = static_epsg_map<point_xy, 32633, 32632>(p32633);
  CHECK(bg::distance(a1, a2) == doctest::Approx(0.0));
}

TEST_SUITE_END();
