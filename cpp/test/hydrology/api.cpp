#include "test_pch.h"

// from core
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/cell_model.h>
#include <shyft/hydrology/stacks/pt_gs_k_cell_model.h>


#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/api/api_state.h>
#include <shyft/hydrology/api/geo_cell_data_io.h>

#include "../energy_market/serialize_loop.h"

using namespace std;
using namespace shyft::core;
using namespace shyft::time_series;

using namespace shyft::api;
using test::serialize_loop;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/geo_cell_data_io") {
  geo_cell_data gcd(geo_point(1, 2, 3), 4, 5, 0.6, land_type_fractions(2, 4, 6, 8, 10));

  auto gcd_s = geo_cell_data_io::to_vector(gcd);
  TS_ASSERT(gcd_s.size() > 0);
  geo_cell_data gcd2 = geo_cell_data_io::from_vector(gcd_s);
  double eps = 1e-12;
  TS_ASSERT_DELTA(gcd2.area(), gcd.area(), eps);
  TS_ASSERT_EQUALS(gcd2.mid_point(), gcd.mid_point());
  TS_ASSERT_EQUALS(gcd2.catchment_id(), gcd.catchment_id());
  TS_ASSERT_DELTA(gcd2.radiation_slope_factor(), gcd.radiation_slope_factor(), eps);
  TS_ASSERT_DELTA(gcd2.land_type_fractions_info().glacier(), gcd.land_type_fractions_info().glacier(), eps);
  TS_ASSERT_DELTA(gcd2.land_type_fractions_info().lake(), gcd.land_type_fractions_info().lake(), eps);
  TS_ASSERT_DELTA(gcd2.land_type_fractions_info().reservoir(), gcd.land_type_fractions_info().reservoir(), eps);
  TS_ASSERT_DELTA(gcd2.land_type_fractions_info().forest(), gcd.land_type_fractions_info().forest(), eps);
  TS_ASSERT_DELTA(gcd2.land_type_fractions_info().unspecified(), gcd.land_type_fractions_info().unspecified(), eps);
}

/** Here we try to build a test-story from start to end that covers state-io-extract-restore*/
TEST_CASE("hydrology/state_with_id_functionality") {
  typedef shyft::core::pt_gs_k::cell_discharge_response_t xcell_t;
  cell_state_id a{1, 2, 3, 10};
  cell_state_id b{1, 2, 4, 20};
  cell_state_id c{2, 2, 4, 20};
  TS_ASSERT_DIFFERS(a, b);
  std::map<cell_state_id, int64_t> smap;
  smap[a] = a.area;
  smap[b] = b.area;
  TS_ASSERT_EQUALS(smap.find(c), smap.end());
  TS_ASSERT_DIFFERS(smap.find(a), smap.end());
  // ------------------------------- (x,y,z),area,cid)
  xcell_t c1{geo_cell_data(geo_point(1, 1, 1), 10.0, 1), nullptr, {}, {}, {}, {}};
  c1.state.kirchner.q = 1.1;
  xcell_t c2{geo_cell_data(geo_point(1, 2, 1), 10.0, 1), nullptr, {}, {}, {}, {}};
  c2.state.kirchner.q = 1.2;
  xcell_t c3{geo_cell_data(geo_point(2, 1, 1), 10.0, 2), nullptr, {}, {}, {}, {}};
  c3.state.kirchner.q = 2.1;
  xcell_t c4{geo_cell_data(geo_point(2, 2, 1), 10.0, 2), nullptr, {}, {}, {}, {}};
  c4.state.kirchner.q = 2.2;
  auto cv = make_shared<vector<xcell_t>>();
  cv->push_back(c1);
  cv->push_back(c2);
  cv->push_back(c3);
  cv->push_back(c4);
  state_io_handler<xcell_t> xh(cv);
  auto s0 = xh.extract_state(cids_t());
  TS_ASSERT_EQUALS(s0->size(), cv->size());
  for (size_t i = 0; i < cv->size(); ++i)
    TS_ASSERT_EQUALS((*s0)[i].id, cell_state_id_of((*cv)[i].geo)); // ensure we got correct id's out.
  //-- while at it, also verify serialization support
  auto bytes = serialize_to_bytes(s0);
  TS_ASSERT(bytes.size() > 10);
  shared_ptr<vector<cell_state_with_id<xcell_t::state_t>>> s0_x;
  deserialize_from_bytes(bytes, s0_x);
  TS_ASSERT_EQUALS(s0_x->size(), s0->size());
  for (size_t i = 0; i < s0->size(); ++i) {
    TS_ASSERT_EQUALS((*s0_x)[i].id, (*s0)[i].id); // equality by identity only check
    TS_ASSERT_DELTA((*s0_x)[i].state.kirchner.q, (*s0)[i].state.kirchner.q, 0.01);
  }
  auto s1 = xh.extract_state(cids_t{2}); // ok, now specify cids, 2, only two cells match
  TS_ASSERT_EQUALS(s1->size(), size_t(2));
  for (size_t i = 0; i < s1->size(); ++i)
    TS_ASSERT_EQUALS((*s1)[i].id, cell_state_id_of((*cv)[i + 2].geo)); // ensure we got correct id's out.

  auto s_missing = xh.extract_state(cids_t{3});
  TS_ASSERT_EQUALS(s_missing->size(), 0u);

  auto m0 = xh.apply_state(s0, cids_t());
  TS_ASSERT_EQUALS(m0.size(), 0u);

  auto m0_x = xh.apply_state(s0, cids_t{4});
  TS_ASSERT_EQUALS(m0_x.size(), 0u); // because we passed in states not containing 4
  (*s0)[0].id.cid = 4;               // stuff in a 4, and get one missing below
  auto m0_y = xh.apply_state(s0, cids_t{4});
  TS_ASSERT_EQUALS(m0_y.size(), 1u);
  TS_ASSERT_EQUALS(m0_y[0], 0);
}

TEST_CASE("hydrology/serialize_geo_point_ts_etc") {
  calendar utc;
  apoint_ts ts(time_axis::fixed_dt(utc.time(2019, 6, 1), deltahours(1), 4), 4.0, time_series::POINT_AVERAGE_VALUE);
  apoint_ts ts_b(time_axis::fixed_dt(utc.time(2019, 6, 1), deltahours(1), 4), 4.0, time_series::POINT_AVERAGE_VALUE);
  geo_point p1(1, 2, 3);
  SUBCASE("geo_point_source_equality") {
    GeoPointSource a(p1, ts);
    a.uid = "123";
    GeoPointSource b(p1, ts_b);
    b.uid = "123";
    // just to ensure we are on the right page here, verify the == operator of geo-point source
    REQUIRE_EQ(a, b);
    b.uid = "abc";
    REQUIRE_EQ(false, a == b);
    b.uid = "123";
    b.mid_point_.x = 9;
    REQUIRE_EQ(false, a == b);
    b.mid_point_.x = 1;
    b.ts.set(0, 0.0);
    REQUIRE_EQ(false, a == b);
  }
  SUBCASE("geo_point_source_serialize") {
    GeoPointSource a(p1, ts);
    a.uid = "123";
    auto sa = serialize_loop(a);
    CHECK_EQ(sa, a);
  }
  SUBCASE("geo_temperature_serialize") {
    TemperatureSource a(p1, ts);
    auto sa = serialize_loop(a);
    CHECK_EQ(sa, a);
  }
  SUBCASE("geo_precipitation_serialize") {
    PrecipitationSource a(p1, ts);
    auto sa = serialize_loop(a);
    CHECK_EQ(sa, a);
  }
  SUBCASE("geo_wind_speed_serialize") {
    WindSpeedSource a(p1, ts);
    auto sa = serialize_loop(a);
    CHECK_EQ(sa, a);
  }
  SUBCASE("geo_rel_hum_serialize") {
    RelHumSource a(p1, ts);
    auto sa = serialize_loop(a);
    CHECK_EQ(sa, a);
  }
  SUBCASE("geo_radiation_serialize") {
    RadiationSource a(p1, ts);
    auto sa = serialize_loop(a);
    CHECK_EQ(sa, a);
  }
}

TEST_CASE("hydrology/a_region_env_serialize") {
  calendar utc;
  apoint_ts ts(time_axis::fixed_dt(utc.time(2019, 6, 1), deltahours(1), 4), 4.0, time_series::POINT_AVERAGE_VALUE);
  geo_point p1(1, 2, 3);
  a_region_environment a;
  a.temperature->push_back(TemperatureSource(p1, ts));
  a.precipitation->push_back(PrecipitationSource(p1, ts));
  a.radiation->push_back(RadiationSource(p1, ts));
  a.wind_speed->push_back(WindSpeedSource(p1, ts));
  a.rel_hum->push_back(RelHumSource(p1, ts));
  auto sa = serialize_loop(a);
  REQUIRE_EQ(false, sa.temperature == nullptr);
  REQUIRE_EQ(false, sa.precipitation == nullptr);
  REQUIRE_EQ(false, sa.wind_speed == nullptr);
  REQUIRE_EQ(false, sa.radiation == nullptr);
  REQUIRE_EQ(false, sa.rel_hum == nullptr);
  REQUIRE_EQ(*sa.temperature, *a.temperature);
  REQUIRE_EQ(*sa.precipitation, *a.precipitation);
  REQUIRE_EQ(*sa.wind_speed, *a.wind_speed);
  REQUIRE_EQ(*sa.rel_hum, *a.rel_hum);
  REQUIRE_EQ(*sa.radiation, *a.radiation);
}

TEST_SUITE_END();
