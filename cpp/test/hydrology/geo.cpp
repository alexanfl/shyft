#include "test_pch.h"
#include <vector>
#include <algorithm>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/geo_point.h>
using shyft::core::geo_cell_data;
using shyft::core::land_type_fractions;
using shyft::core::geo_point;
using shyft::core::land_type;

namespace shyft::test {
  struct geo_cell_data {
    geo_point v1;
    geo_point v2;
    geo_point v3;

    arma::vec normal() const {
      const arma::vec::fixed<3> vv0 = {v2.x - v1.x, v2.y - v1.y, v2.z - v1.z};
      const arma::vec::fixed<3> vv1 = {v3.x - v1.x, v3.y - v1.y, v3.z - v1.z};
      arma::vec::fixed<3> n = arma::cross(vv0, vv1);
      n /= arma::norm(n);
      return n[2] >= 0.0 ? arma::vec{n[0], n[1], n[2]} : arma::vec{-n[0], -n[1], -n[2]};
    }

    double slope(arma::vec normal) {
      return std::atan2(pow(pow(normal[0], 2) + pow(normal[1], 2), 0.5), normal[2]);
    }

    double aspect(arma::vec normal) {
      return std::atan2(normal[0], normal[1]);
    }
  };
}

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/geo/land_type_fractions") {
  land_type_fractions a;
  FAST_CHECK_EQ(a.glacier(), doctest::Approx(0.0));
  FAST_CHECK_EQ(a.lake(), doctest::Approx(0.0));
  FAST_CHECK_EQ(a.reservoir(), doctest::Approx(0.0));
  FAST_CHECK_EQ(a.forest(), doctest::Approx(0.0));
  FAST_CHECK_EQ(a.unspecified(), doctest::Approx(1.0));
  FAST_CHECK_EQ(a.snow_storage(), doctest::Approx(1.0));
  FAST_CHECK_EQ(a.is_monocell(), true);
  land_type_fractions b;
  FAST_CHECK_EQ(a, b);
  land_type_fractions c(0.1, 0.11, 0.123, 0.1234, 0.5436);
  FAST_CHECK_EQ(c.glacier(), doctest::Approx(0.1));
  FAST_CHECK_EQ(c.lake(), doctest::Approx(0.11));
  FAST_CHECK_EQ(c.reservoir(), doctest::Approx(0.123));
  FAST_CHECK_EQ(c.forest(), doctest::Approx(0.1234));
  FAST_CHECK_EQ(c.unspecified(), doctest::Approx(0.5436));
  FAST_CHECK_EQ(c.snow_storage(), doctest::Approx(1 - (0.11 + 0.123)));
  FAST_CHECK_EQ(c.is_monocell(), false);
  CHECK(a != c);
  land_type_fractions bg(land_type::bare_ground);
  FAST_CHECK_EQ(bg.is_monocell(), true);
  FAST_CHECK_EQ(bg.unspecified(), doctest::Approx(1.0));
  land_type_fractions f(land_type::forest);
  FAST_CHECK_EQ(f.forest(), doctest::Approx(1.0));
  land_type_fractions r(land_type::reservoir);
  FAST_CHECK_EQ(r.reservoir(), doctest::Approx(1.0));
  land_type_fractions l(land_type::lake);
  FAST_CHECK_EQ(l.lake(), doctest::Approx(1.0));
  land_type_fractions g(land_type::glacier);
  FAST_CHECK_EQ(g.glacier(), doctest::Approx(1.0));
}

TEST_CASE("hydrology/geo/point_as_long_lat") {
  geo_cell_data gcd{
    geo_point{591520.915557, 6643097.688005, 40.0}
  };
  gcd.epsg_id = 32632;
  auto p = gcd.mid_point_as_long_lat();
  FAST_CHECK_EQ(p.longitude, doctest::Approx(10.6367397));
  FAST_CHECK_EQ(p.latitude, doctest::Approx(59.9151928));
}

TEST_CASE("hydrology/geo/point_epsg_map") {
  // 32633 -> 32632 (two nearby epsg) that splits norway
  geo_point p32633{254807.03, 6784125.07, 100};
  geo_point p32632{578006.66, 6776452.38, 100};
  auto a = epsg_map(p32633, 32633, 32632);
  FAST_CHECK_EQ(geo_point::xy_distance(a, p32632), doctest::Approx(0).epsilon(0.1));
  FAST_CHECK_EQ(a.z, doctest::Approx(p32633.z));
  auto b = epsg_map(p32633, 32633, 32633);
  FAST_CHECK_EQ(b, p32633);
}

TEST_CASE("hydrology/geo/cell_tin_area") {
  geo_cell_data gcd(
    geo_point(0, 500, 100),
    geo_point(2000, 500, 100),
    geo_point(
      1000, 1500, 1000)); // tin data should give same area as previous test, so we can check that things are not broken
  FAST_CHECK_EQ(gcd.area(), doctest::Approx(1000 * 1000).epsilon(0.0001));
}

TEST_CASE("hydrology/geo/cell_tin_flat") {
  geo_cell_data a(geo_point(0, 500, 100), geo_point(2000, 500, 100), geo_point(1000, 2000, 100));
  FAST_CHECK_EQ(a.is_monocell(), true);
  FAST_CHECK_EQ(a.mid_point().x, doctest::Approx(1000.0));
  FAST_CHECK_EQ(a.mid_point().y, doctest::Approx(1000.0));
  FAST_CHECK_EQ(a.mid_point().z, doctest::Approx(100.0));
  FAST_CHECK_EQ(a.area(), doctest::Approx(1500000.0));
  FAST_CHECK_EQ(a.slope(), doctest::Approx(0.0));
  FAST_CHECK_EQ(a.aspect(), doctest::Approx(0.0));
  FAST_CHECK_EQ(a.rarea(), doctest::Approx(a.area()));
}

TEST_CASE("hydrology/geo/cell_tin_sloped") {
  geo_cell_data a(geo_point(0, 500, 100), geo_point(2000, 500, 400), geo_point(1000, 2000, 100));
  FAST_CHECK_EQ(a.is_monocell(), true);
  FAST_CHECK_EQ(a.mid_point().x, doctest::Approx(1000.0));
  FAST_CHECK_EQ(a.mid_point().y, doctest::Approx(1000.0));
  FAST_CHECK_EQ(a.mid_point().z, doctest::Approx(200.0));
  FAST_CHECK_EQ(a.area(), doctest::Approx(1500000.0));
  shyft::test::geo_cell_data gcd;
  gcd.v1 = geo_point(0, 500, 100);
  gcd.v2 = geo_point(2000, 500, 400);
  gcd.v3 = geo_point(1000, 2000, 100);

  FAST_CHECK_EQ(a.slope(), doctest::Approx(gcd.slope(gcd.normal())));
  FAST_CHECK_EQ(a.aspect(), doctest::Approx(gcd.aspect(gcd.normal())));
}

TEST_CASE("hydrology/geo/cell_equal") {
  geo_cell_data a(geo_point(0, 500, 100), geo_point(2000, 500, 400), geo_point(1000, 2000, 100));
  geo_cell_data b{a};
  FAST_CHECK_EQ(a, b);
  b.set_catchment_id(3);
  FAST_CHECK_NE(a, b);
}

TEST_SUITE_END();
