#include "test_pch.h"
#include "hydro_mocks.h"
#include <shyft/hydrology/methods/gamma_snow.h>

namespace shyfttest {

  double const EPS = 1.0e-10;

};

using namespace shyft::core;
namespace gs = shyft::core::gamma_snow;

class gamma_snow_test {
 public:
  template <class GS_CALC, class P>
  void reset_snow_pack(
    GS_CALC& gsc,
    double& sca,
    double& lwc,
    double& alpha,
    double& sdc_melt_mean,
    double& acc_melt,
    double& temp_swe,
    double const storage,
    P const & p) const {
    gsc.reset_snow_pack(sca, lwc, alpha, sdc_melt_mean, acc_melt, temp_swe, storage, p);
  }

  template <class GS_CALC>
  void calc_snow_state(
    GS_CALC& gsc,
    double const shape,
    double const scale,
    double const y0,
    double const lambda,
    double const lwd,
    double const max_water_frac,
    double const temp_swe,
    double& swe,
    double& sca) const {
    gsc.calc_snow_state(shape, scale, y0, lambda, lwd, max_water_frac, temp_swe, swe, sca);
  }

  template <class GS_CALC>
  double corr_lwc(
    GS_CALC& gsc,
    double const z1,
    double const a1,
    double const b1,
    double z2,
    double const a2,
    double const b2) const {
    return gsc.corr_lwc(z1, a1, b1, z2, a2, b2);
  }
};

static gamma_snow_test fix; // access to private scope

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/gs/equal_operator") {
  gs::parameter gs1;
  gs::parameter gs2{99, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs3{100, 99.0, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs4{100, 0.04, 99.0, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs5{100, 0.04, 0.4, 99.0, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs6{100, 0.04, 0.4, -0.5, 99.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs7{100, 0.04, 0.4, -0.5, 2.0, 99.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs8{100, 0.04, 0.4, -0.5, 2.0, 1.0, 99.0, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs9{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 99.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs10{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 99.0, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs11{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 99.0, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs12{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 99.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs13{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 99.0, 5.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs14{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 99.0, 0.4, false, 0.0, 0.0, 221};
  gs::parameter gs15{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 99.0, false, 0.0, 0.0, 221};
  gs::parameter gs16{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, true, 0.0, 0.0, 221};
  gs::parameter gs17{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 99.0, 0.0, 221};
  gs::parameter gs18{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 99.0, 221};
  gs::parameter gs19{100, 0.04, 0.4, -0.5, 2.0, 1.0, 0.1, 30.0, 0.9, 0.6, 5.0, 5.0, 5.0, 0.4, false, 0.0, 0.0, 99};

  TS_ASSERT(gs1 != gs2);
  TS_ASSERT(gs1 != gs3);
  TS_ASSERT(gs1 != gs4);
  TS_ASSERT(gs1 != gs5);
  TS_ASSERT(gs1 != gs6);
  TS_ASSERT(gs1 != gs7);
  TS_ASSERT(gs1 != gs8);
  TS_ASSERT(gs1 != gs9);
  TS_ASSERT(gs1 != gs10);
  TS_ASSERT(gs1 != gs11);
  TS_ASSERT(gs1 != gs12);
  TS_ASSERT(gs1 != gs13);
  TS_ASSERT(gs1 != gs14);
  TS_ASSERT(gs1 != gs15);
  TS_ASSERT(gs1 != gs16);
  TS_ASSERT(gs1 != gs17);
  TS_ASSERT(gs1 != gs18);
  TS_ASSERT(gs1 != gs19);

  gs2.winter_end_day_of_year = 100;
  gs3.initial_bare_ground_fraction = 0.04;
  gs4.snow_cv = 0.4;
  gs5.tx = -0.5;
  gs6.wind_scale = 2.0;
  gs7.wind_const = 1.0;
  gs8.max_water = 0.1;
  gs9.surface_magnitude = 30.0;
  gs10.max_albedo = 0.9;
  gs11.min_albedo = 0.6;
  gs12.fast_albedo_decay_rate = 5.0;
  gs13.slow_albedo_decay_rate = 5.0;
  gs14.snowfall_reset_depth = 5.0;
  gs15.glacier_albedo = 0.4;
  gs16.calculate_iso_pot_energy = false;
  gs17.snow_cv_forest_factor = 0.0;
  gs18.snow_cv_altitude_factor = 0.0;
  gs19.n_winter_days = 221;

  TS_ASSERT(gs1 == gs2);
  TS_ASSERT(gs1 == gs3);
  TS_ASSERT(gs1 == gs4);
  TS_ASSERT(gs1 == gs5);
  TS_ASSERT(gs1 == gs6);
  TS_ASSERT(gs1 == gs7);
  TS_ASSERT(gs1 == gs8);
  TS_ASSERT(gs1 == gs9);
  TS_ASSERT(gs1 == gs10);
  TS_ASSERT(gs1 == gs11);
  TS_ASSERT(gs1 == gs12);
  TS_ASSERT(gs1 == gs13);
  TS_ASSERT(gs1 == gs14);
  TS_ASSERT(gs1 == gs15);
  TS_ASSERT(gs1 == gs16);
  TS_ASSERT(gs1 == gs17);
  TS_ASSERT(gs1 == gs18);
  TS_ASSERT(gs1 == gs19);
}

TEST_CASE("hydrology/gs/reset_snow_pack_zero_storage") {

  gs::calculator gs;
  gs::parameter p;
  double sca;
  double lwc;
  double alpha;
  double sdc_melt_mean;
  double acc_melt;
  double temp_swe;
  double const storage = 0.0;
  fix.reset_snow_pack(gs, sca, lwc, alpha, sdc_melt_mean, acc_melt, temp_swe, storage, p);
  TS_ASSERT_DELTA(sca, 0.0, shyfttest::EPS);
  TS_ASSERT_DELTA(lwc, 0.0, shyfttest::EPS);
  TS_ASSERT_DELTA(alpha, 1.0 / (p.snow_cv * p.snow_cv), shyfttest::EPS);
  TS_ASSERT_DELTA(sdc_melt_mean, 0.0, shyfttest::EPS);
  TS_ASSERT_DELTA(acc_melt, -1.0, shyfttest::EPS);
  TS_ASSERT_DELTA(temp_swe, 0.0, shyfttest::EPS);
}

TEST_CASE("hydrology/gs/reset_snow_pack_with_storage") {

  gs::calculator gs;
  gs::parameter p;
  double sca;
  double lwc;
  double alpha;
  double sdc_melt_mean;
  double acc_melt;
  double temp_swe;
  double const storage = 1.0;

  fix.reset_snow_pack(gs, sca, lwc, alpha, sdc_melt_mean, acc_melt, temp_swe, storage, p);
  TS_ASSERT_DELTA(sca, 1.0 - p.initial_bare_ground_fraction, shyfttest::EPS);
  TS_ASSERT_DELTA(lwc, 0.0, shyfttest::EPS);
  TS_ASSERT_DELTA(alpha, 1.0 / (p.snow_cv * p.snow_cv), shyfttest::EPS);
  TS_ASSERT_DELTA(sdc_melt_mean, storage / sca, shyfttest::EPS);
  TS_ASSERT_DELTA(acc_melt, -1.0, shyfttest::EPS);
  TS_ASSERT_DELTA(temp_swe, 0.0, shyfttest::EPS);
}

TEST_CASE("hydrology/gs/calculate_snow_state") {
  // This is a pure regression test, based on some standard values and recorded response

  gs::calculator gs;

  double const shape = 1.0 / (0.4 * 0.4);
  double const scale = 0.4 / shape;
  double const y0 = 0.04;
  double const lambda = 0.0;
  double const lwd = 0.0;
  double const max_water_frac = 0.1;
  double const temp_swe = 0.0;
  double swe; // Output from gs.calc_snow_state(...)
  double sca; // Output from gs.calc_snow_state(...)
  fix.calc_snow_state(gs, shape, scale, y0, lambda, lwd, max_water_frac, temp_swe, swe, sca);
  TS_ASSERT_DELTA(swe, 0.384, shyfttest::EPS);
  TS_ASSERT_DELTA(sca, 0.96, shyfttest::EPS);
}

TEST_CASE("hydrology/gs/correct_lwc") {
  // This is a pure regression test, based on a set of input values and recorded response

  gs::calculator gs;
  double const z1 = 4.0;
  double const a1 = 6.0;
  double const b1 = 1.0;
  double const z2 = 5.0;
  double const a2 = 5.0;
  double const b2 = 2.0;

  double const result = fix.corr_lwc(gs, z1, a1, b1, z2, a2, b2);
  // as a result of using lower resolution, less accuracy
  TS_ASSERT_DELTA(result, 3.8411, 0.0001);
  // would throw in previous versions
  fix.corr_lwc(gs, 1.0, 6.25, 0.005358, 0.5, 6.25, 0.005358);

  fix.corr_lwc(gs, 0.0, a1, b1, z2, a2, b2);
  fix.corr_lwc(gs, z1, a1, b1, 0.0, a2, b2);
}

TEST_CASE("hydrology/gs/warm_winter_effect") {
  gs::calculator gs;
  double tx = -0.5;
  double wind_scale = 2.0;
  double wind_const = 1.0;
  double max_lwc = 0.1;
  double surf_mag = 30.0;
  double max_alb = 0.9;
  double min_alb = 0.6;
  double fadr = 5.0;
  double sadr = 5.0;
  double srd = 5.0;
  double glac_alb = 0.4;
  gs::parameter param(100, tx, wind_scale, wind_const, max_lwc, surf_mag, max_alb, min_alb, fadr, sadr, srd, glac_alb);
  param.initial_bare_ground_fraction = 0.04;
  param.snow_cv = 0.5;
  // Value for node 94
  // param.set_glacier_fraction(0.12);
  // double albedo = 0.6;
  // double lwc = 3133.46;
  // double surface_heat = 0.0;
  // double alpha = 3.9589693546295166;
  // double sdc_melt_mean = 1474.5162353515625;
  // double acc_melt = 2669.209716796875;
  // double iso_pot_energy = 1698.521484375;
  // double temp_swe = 0.0;

  // Value for node 231
  double albedo = 0.6;
  double lwc = 3148.9609375;
  double surface_heat = 0.0;
  double alpha = 3.960848093032837;
  double sdc_melt_mean = 1525.66064453125;
  double acc_melt = 2753.03076171875;
  double iso_pot_energy = 1752.56396484375;
  double temp_swe = 0.0;
  gs::state states(albedo, lwc, surface_heat, alpha, sdc_melt_mean, acc_melt, iso_pot_energy, temp_swe);
  gs::response response;

  double temp = 7.99117956; // With this temperature, the GammaSnow method fails at winter end
  double rad = 0;
  double prec = 0.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;
  auto dt = shyft::core::deltahours(1);
  size_t num_days = 100;

  for (size_t i = 0; i < 24 * num_days; ++i) {
    gs.step(
      states, response, utctime(dt * 24 * 232 + i * dt), dt, param, temp, rad, prec, wind_speed, rel_hum, 0.0, 0.0);
  }
}

TEST_CASE("hydrology/gs/step") {

  gs::calculator gs;
  gs::parameter param;
  auto dt = shyft::core::deltahours(1);

  gs::state states(0.0, 1.0, 0.0, 1.0 / (param.snow_cv * param.snow_cv), 10.0, -1.0, 0.0, 0.0);
  gs::response response;

  double temp = 1.0;
  double rad = 10.0;
  double prec = 5.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;

  for (size_t i = 0; i < 365; ++i) {
    gs.step(
      states, response, utctime(i * dt), dt, param, i & 1 ? temp : param.tx, rad, prec, wind_speed, rel_hum, 0.0, 0.0);
  }
}

TEST_CASE("hydrology/gs/output_independent_of_timestep") {
  gs::calculator gs;
  gs::parameter param;
  auto dt = shyft::core::deltahours(1);
  auto dt3h = shyft::core::deltahours(3);

  gs::state initial_state(0.0, 0.0, 0.0, 1.0 / (param.snow_cv * param.snow_cv), 0.0, -1.0, 0.0, 0.0);
  double rad = 10.0;
  double prec = 5.0;
  double wind_speed = 2.0;
  double rel_hum = 0.70;

  for (size_t j = 0; j < 2; j++) {

    double temp = j == 0 ? 10.0 : -10.0;
    // first test with hot scenario, no snow, to get equal output (response is nonlinear)
    // then with cold,
    gs::state state1h(initial_state);
    gs::response response1h;
    double output_hour = 0;
    for (size_t i = 0; i < 3; ++i) {
      gs.step(state1h, response1h, utctime(i * dt), dt, param, temp, rad, prec, wind_speed, rel_hum, 0.0, 0.0);
      output_hour += response1h.outflow; // mm_h
    }
    output_hour /= 3.0; // avg hour output
    gs::state state3h(initial_state);
    gs::response response3h;
    gs.step(state3h, response3h, utctime(0 * dt3h), dt3h, param, temp, rad, prec, wind_speed, rel_hum, 0.0, 0.0);
    // require same output on the average, and same lwc
    TS_ASSERT_DELTA(output_hour, response3h.outflow, 0.00001);
    TS_ASSERT_DELTA(state1h.lwc, state3h.lwc, 0.000001);
    TS_ASSERT_DELTA(response1h.storage, response3h.storage, 0.00001);
  }
}

TEST_CASE("hydrology/gs/forest_altitude_dependent_snow_cv") {
  gs::parameter p;
  TS_ASSERT_DELTA(
    p.effective_snow_cv(1.0, 1000), p.snow_cv, 0.0000001); // assume default no effect, backward compatible
  p.snow_cv_forest_factor = 0.1;                           // forest fraction 1.0 should add 0.1 to the snow_cv
  p.snow_cv_altitude_factor = 0.0001;                      // at 1000m, add 0.1 to the factor
  TS_ASSERT_DELTA(p.effective_snow_cv(0.0, 0.0), p.snow_cv, 0.0000001); // verify no increase of forest=0 and altitude=0
  TS_ASSERT_DELTA(p.effective_snow_cv(1.0, 0.0), p.snow_cv + 0.1, 0.0000001); // verify increase in forest direction
  TS_ASSERT_DELTA(
    p.effective_snow_cv(0.0, 1000.0), p.snow_cv + 0.1, 0.0000001); // verify increase in altitude direction
}

TEST_SUITE_END();
