|                        |                                                                                                                                                     |
|------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| Shyft Documentation    | [![Documentation Status](https://readthedocs.org/projects/shyft/badge/?version=latest)](https://shyft-os.gitlab.io/shyft-doc/index.html)                    |
| Shyft Google Group     | [![Shyft Google Group](https://img.shields.io/badge/Shyft%20Google%20Group-active-blue.svg)](https://groups.google.com/forum/#!forum/shyft)         |
| License                | [![Gnu GPL license](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gitlab.com/shyft-os/shyft/blob/master/LICENSE)                   |


# ABOUT Shyft and the Energy Market Model


![alt text](./doc/image/shyft_energy_market_model.jpg "The Energy Market Model")

_Figure crafted by Christine Schei Liland_
 



**Shyft** is a cross-platform, open source toolbox developed at [Statkraft](http://www.statkraft.com) in cooperation with the [Department of Geosciences](http://geo.uio.no) at the University of Oslo.

The goal of Shyft is to **facilitate collaboration** among system providers, users, and research communities solving **energy market-related problems**.

We perceive that the industry today is fragmented and inflexible, with specialized communities that do not collaborate closely enough and with systems that are not compatible.

We believe that the entire industry would benefit from increased **transparency and standardization** concerning data management, data types, and system interfaces.

By sharing fundamental components, we believe it is possible to achieve **faster development, improved solutions, and closer collaboration** among professional and academic communities in the power market.

**Shyft is an open source toolbox** that includes data types, interfaces, and methods for data management that we believe can contribute to more standardization and collaboration around energy market-related issues, if other actors wish to use them and contribute to their improvement.

Shyft is in active maintenance and development, used in **24x7 operations at Statkraft** and forms the basis for much of the data management and analysis work in Statkraft's Energy Management division.

Shyft has **rolling-releases** - improvements are shipped as soon as testing proves stability and quality.

## Contents
The toolbox provides python-enabled high performance components with **operational quality**:

  * Well-designed components and algorithms aiming at speed, robustness and scalability.
  * Using well known, high performance, open-source, 3rd party libraries.
  * C++ is the core language, with python as an interface for interaction and orchestration.
  * The toolbox comes with Unit-tests, integration tests, examples, and demo data, with high test coverage.
  * Continuous build-test-deploy on many platforms (linux, windows, even raspberry pi/arm is possible).
  * Integrated issue tracking and merge-requests, allowing everyone to contribute.
  * Transparent and openly available source code.
  * Supports in-house build-systems for those that would like to extend and integrate with in-house software products.

The format allows model experts in the business domain, scientists at institutes/universities together with professional 
programmers and IT support to cooperate efficiently in the energy-market domain.

If your primary interest is in the generic __high performance time-series engine__, you would benefit from reading the 
intro presented here.

If you are interested in the __energy-market model__, power-market, pure energy or hydro-power, then the energy-market model
section would be useful, as well as the time-series section.

The shyft.dashboard contains a major contribution providing components suitable for creating bokeh based applications and dashboards.

If your primary interest is __Hydrology forecasting models and algorithms__, skip to the __Hydrology__ section.

shyft.viz contains a matplotlib based presentation tool for working efficiently with hydrological models.

Some of our tools and libraries will work well for other domains, like the shyft.time-series package which is relatively generic for time-series problems. 

## About security

It is of major importance that we can trust the software that we used.

This also applies to users of shyft, since the intended usage is operational.

This includes both the source-code, which is maintained here on GitLab, and the complete surface of dependencies to the 3rd party packages and libraries that we use for building shyft. We try to use packages from linux distro maintainers, and if these are not available, we compile from source to make the supply chain as transparent as possible.

All the libraries and packages we use are completely open and maintained through the dependencies which again are documented in the checked in scripts of build_support, as well as the ine the GitLab wiki.

We secure the repository with 2FA, and from 2021 we also only accept verified commits (GPG signed) from the contributers to the project.

To get an overview of the supply-chain security, as well as the recommended practices, we recommend reading through documents like

[linux foundation: Developing(and Evaluating) Secure Open source Software(OSS)](https://events.linuxfoundation.org/wp-content/uploads/2021/07/Developing-Secure-Open-Source-Software-OSS.pdf)

The wiki [Open Source Security Foundation](https://en.wikipedia.org/wiki/Open_Source_Security_Foundation), also gives a good starting point.

Our plan is to incorporate more supply chain security related features into the build-pipeline of shyft.
 
## High performance generic time-series handling services

Shyft allows you work with time series easily, with tools for everything from storage to advanced, distributed server-side 
evaluation of large time series expressions.

Shyft allows you to write time series expressions, as you would in numpy, using scalars, time-series, or vectors of time-series.
The differences from other python frameworks, like numpy, pandas, xarray, is that it provides
 
 * strongly typed time series, (not arrays/vectors) with mathematical expression power 'as-if' it was f(t).
 * lazy evaluation, using multicore parallel execution.
 * symbolic expressions in natural python (or C++), that can be persisted and retrieved.
 * time series classes, including unbound time series-expression, that can be attached to models as attributes.
 * a backend server (dtss) for distributed scalable high performance computing, with caching.
 * a geo-location aware time series database for working with geo-time series.
 * a backend server (dtss) with advanced web-socket based api with subscription for direct integration with web-front-ends technologies

### Expressions

You can express your self in natural python, and get scalable high performance expressions like:
```python

a = TimeSeries(time_axis,values,point_interpretation..)
b = TimeSeries(time_axis,values,point_interpretation..)
c = a*2.5 + b.pow(a) -1000.0  # this works, lazy eval, takes care of diff. time-resolution etc. etc.

my_plot(c.values.to_numpy())  # you can extract numpy values from the expression.

e = TimeSeries('shyft:/prod/price/no_1')
p = TimeSeries('shyft:/prod/total_mw')

ta_2018 = TimeAxis(time('2018-01-01T00:00:00Z'),deltahours(24),365)
m = e*p.accumulate(ta_2018)  # this also works, expression

dtsc= DtsClient('dtss_host:20000')
mr = dtsc.evaluate(TsVector([m,e,p]),ta_2018.total_period()) # vector eval, get back server-side evaluated expressions


```

You can create, store and update server-side time-series, and use those time-series in your expressions.

```python
from shyft.time_series import DtsServer
from time import sleep
# On the server side ! 
def start_the_dtss_server(dtss_root:str, port:int=20000)->DtsServer:
    """ These 4 lines starts a HPC ts server on port 20000 (it could be your laptop!) """
    dtss = DtsServer()
    dtss.set_container('prod',dtss_root)
    dtss.set_listening_port(port)
    dtss.start_async()
    return dtss
dtss=start_the_dtss_server(dtss_root='/tmp/dtss',port=20000) # you might want to tune the root and port number to your configuration here
while True:
  sleep(0.5)  # just keep this python process alive until control-C.

# Anywhere on you network (ensure to open firewall for port 20000 etc.
from shyft.time_series import DtsClient,TimeSeries,TsVector,shyft_url,POINT_AVERAGE_VALUE as stair_case,TimeAxis,time
dtsc = DtsClient(f'localhost:20000')  # adjust this accordig to your server setup 
ta=TimeAxis(time('2021-01-01T00:00:00Z'),3600,3)  # time-axis that defines when
values=[1.0,2.0,4.0] # values to use in our little example

tsv_to_store = TsVector([
    TimeSeries(shyft_url('prod','price/no1'),TimeSeries(ta,values,stair_case)), # a ts with url and payload data
    TimeSeries(shyft_url('prod','total_mw'),TimeSeries(ta,values,stair_case)) # a ts with url and payload data
    ])
dtsc.store_ts(tsv_to_store)  # Done ! 

# now have fun, ref example above, you can use symbolic expressions referencing time-series for server evaluation
price=TimeSeries(shyft_url('prod','price/no1')) # notice, just the url here! we now got a symbolic ts-ref (like math symbol)
prod=TimeSeries(shyft_url('prod','total_mw'))
income= price*prod*0.80 # an expression, we have to pay tax, so only 0.8 :-) 
result=dtsc.evaluate(TsVector([price,prod,income]),ta.total_period()) # evaluate the expressions, if needed read data from storage covering the timeaxis period
print(result[-1].values) 
# outputs: [ 0.8  3.2 12.8]

```

The DTSS is easily extensible, by python!

On the server-side, you can register your own methods to do the read, write and find
time-series methods. Based on the pattern of the Shyft-time-series url.
Those that starts with shyft://.. is handled internally, using local high-performance store.
The other ts-urls, are grouped together and forwarded to your python code.
Most likely, you already have a legacy system with python-api, so it's easy to do.

This allow you to integrate with any backend, legacy system or computational system that you
might have. 

Do you have a slow performing legacy time-series database ?

Bring your time-series data to life using python and Shyft DTSS!

Typical read/write speeds at server side is close to system-performance,
typically 100..1000 GBytes/sec, for typical SSD and NVME drives.
The computational speed is comparable to matrix library speeds, multicore.

The DTSS supports caching of time-series, giving you in-memory speed for computations, production-servers would typically
keep 250 GB of cache (that is 25 Giga points of time-series float data!). Time is valuable, -memory is cheap! 

In most scenario, with single-writer multiple readers, Shyft DTSS supports `cache on write`, so your client will always
get fresh data, evaluated at multicore in-memory performance.

One of the success-stories in Statkraft is that we are using a model-driven architecture,
 and deriving the expressions from the models.

   
## Hydrological forecasting models.

The hydrological forecasting models follows the paradigm of distributed, lumped parameter models -- with recent developments introducing more physically based / process-level methods.

Users interested in the application of Inflow modeling are encouraged to see:

["Shyft v4.8: A Framework for Uncertainty Assessment and Distributed Hydrologic
Modelling for Operational Hydrology"](https://doi.org/10.5194/gmd-2020-47)


## The energy-market model

The energy-market model framework provides fundamental tools for building/storing and maintaining energy-market models. The artwork at top of page illustrates the conceptual scope.

The energy-market model is a high performance python enabled framework for handling all information needed to digitally represent a given energy market. The detailing of the model will depend on the use case you need to apply it to.

When the model is defined and populated with information, the idea is to feed it into the __proprietary algorithms__ that are the foundation for most existing energy market simulation or optimization tools. This allows IT-vendors, users and researchers to have a the same standardized interface for their work in energy market simulation or optimization. This in turn gives the community the possibility of testing different models on their data,evaluate their performance, and in general lay the ground for a much a higher rate of improvement in the energy market domain.

The model-driven approach, combining fundamental models with various algorithms is a key factor for business driven development, along with python-enabled architecture on both the server and client-side.

The energy-market model at a birds-eye view contains the electrical grid with consumers and producers within defined areas. The areas are typically partitioned due to power-grid transmission line capacity, or political/country strategies.

At a more detailed level, within a model-area, the user can define details for each producer/consumer and for power-modules.

Between the areas, there are power-lines with capacity and regulations. For areas that have hydropower, and which may also be hydropower dominated, the user may define a quite detailed description of each hydropower system with its reservoirs, tunnels/rivers, aggregates and power-stations.

For hydropower systems, you may also define a detailed model suitable for day-to-day planning, bid-process, optimization, and daily operation and intraday balancing. 

The detail level of this hydropower model also allows for estimation of inflow from catchments surrounding the hydropower system.

The energy-market model __does not currently__ provide algorithms for optimization, simulation or historical inflow estimates based on metered production, gate-flow and reservoir levels.

We hope that highly competent and skilled companies and research institutions can focus on their specialized algorithms and let the cunsumer side handle the necessary data data.

We would like to __cooperate closely with the vendors__ of algorithms regarding the integration of an energy market model, so that we can provide the best possible product to the end users, researchers and analysts. 

Contributions that allows end-users to test the algorithms, using the energy-market model to harvest data for the algorithms, is very welcome.

Any other contributions and integrations (e.g. presentation-layer), are also welcome. 

# VERSION

Shyft is configured to automatically run complete pipelines in Gitlab ending up in the publication of python-packages.

The root directory of shyft contains a manually maintained file VERSION that contains 3 numbers that denotes the current version X.Y.Z.

Z: For bugfix and additional features (usually this day-to-day releases).

Y: For possible breaking changes that might involve user-interaction for those using the python code, and c++ code (would have to recompile etc.).

X: For changes that causes severe format-changes and would require manual upgrade, e.g. let's say we changed the format of the storage for time-series, where the upgrade 

To publish new versions, it is enforced on anaconda.org/shyft-os that each new version must have distinct version.


# DOCUMENTATION

Shyft's primary **end-user** documentation is at [Shyft docs](https://shyft-os.gitlab.io/shyft-doc/index.html), where you will find instructions for installing Shyft and getting up and running with the tools it provides.

We also maintain this [README](README.md) file with basic instructions for building Shyft from a **developer** perspective.


# AUTHORS

Shyft is developed by Statkraft, and the two main initial authors to the C++ core were Sigbjørn Helset <Sigbjorn.Helset@statkraft.com> and Ola Skavhaug <ola@xal.no>.

Orchestration and the Python wrappers were originally developed by John F. Burkhart <john.burkhart@geo.uio.no> with later contributions from Yisak Sultan Abdella <yisaksultan.abdella@statkraft.com>

Copyright (C) Sigbjørn Helset (SiH), John F. Burkhart (JFB), Ola Skavhaug (OS), Yisak Sultan Abdella (YAS), Statkraft AS


# THANKS

Contributors and current project participants include:
 * Sigbjørn Helset <Sigbjorn.Helset@statkraft.com>
 * Ola Skavhaug <ola@xal.no>
 * John Burkhart <John.Burkhart@geo.uio.no>
 * Yisak Sultan Abdella <YisakSultan.Abdella@statkraft.com>
 * Felix Matt <f.n.matt@geo.uio.no>
 * Olga Silantyeva <olga.silantyeva@geo.uio.no>
 * Francesc Alted <faltet@gmail.com>
 * Ludovic Pochon-Guérin <ludovic.pochon-guerin@statkraft.com>
 * Eivind Lycke Melvær <eivindlyche.melvaer@statkraft.com>
 * Cecilie Granerød <Cecilie.Granerod@statkraft.com>
 * Andreas Slyngstad <andreas@xal.no>
 * Bernardo de Olivera <bernardo@xal.no>
 * Diako Darian <diako@xal.no>
 * Eivind Aarnæs <eivind@xal.no>
 * Vinzenz Gregor Eck <vinzenz@xal.no>
 * Mindaugas Pivoras <mindaugas.pivoras@statkraft.com>
 * Roar Emaus <roar@xal.no>
 * Trygve Bærland <trygve@xal.no>
 * Albert O. Nybø <Albert.Overskeid.Nybo@statkraft.com>
 * Gry M. Tveten <gry@xal.no>
 * Magne Nordaas <magne@xal.no>
 * Christine Schei Liland <ChristineShei.Liland@statkraft.com>
 * Jens Askgaard <jens.askgaard@statkraft.com>
 * Ibrahim Rahmani <ibrahim@xala.no>
 * Anders Kjeldsen <anders.kjeldsen@statkraft.com>
 * Alexander Becker <alexander.becker@statkraft.com>
 * Sarah Dahmen <sarah.dahmen21@gmail.com>
 * Fabio Zeiser <fabio.zeiser@statkraft.com>
 * Stian Angelsen <stian.angelsen@statkraft.com>
 * Joseph Bradshaw <joseph.bradshaw@statkraft.com>
 * Ole Andreas Grønn Ramsdal <oleandreasgronn.ramsdal@statkraft.com>
 * John Eivind Rømma Helset <https://gitlab.com/jehelset>
 * Benedikt Reinartz <https://gitlab.com/filmor>
 * Paul Christoph Bätzing-Rosenvinge <paulchristoph.batzing-rosenvinge@statkraft.com>
 * Eilidh Troup <https://gitlab.com/eilidht>
 * Petar Kutlesic <https://gitlab.com/petar.kutlesic>
 

# COPYING / LICENSE

Shyft is released under LGPL V.3
See LICENCE

# Citing

Please cite:

**Burkhart, J. F., Matt, F. N., Helset, S., Sultan Abdella, Y., Skavhaug, O., and Silantyeva, O.**: *Shyft v4.8: a framework for uncertainty assessment and distributed hydrologic modeling for operational hydrology*, Geosci. Model Dev., 14, 821–842, https://doi.org/10.5194/gmd-14-821-2021, 2021.

## Container images for development and pre-built shyft

To make it all easy, - we provide dockers, Refer to the [shyft dockers](https://gitlab.com/shyft-os/dockers) that provides both developer-oriented containers and ready-built containers for production, including the recipes so everything is completely transparent.


## Installing for python

Conda packages for linux and even windows are available at anaconda.org, channel shyft-os
```bash
conda install -c shyft-os shyft
# or to get only the shyft time-series package
conda install -c shyft-os shyft.time_series
```

Similar python pip packages are also available on pypi.org:

```bash
python -v venv shyft-py --system-site-packages
source shyft-py/bin/activate
pip install shyft
```

## DEVELOPER DOCUMENTATION

First time users and those are interested in learning how to use Shyft for hydrological simulation are strongly encouraged to see [Shyft at readthedocs](http://shyft.readthedocs.io/en/latest/).


### CLONING

Shyft is distributed in three separate code repositories. This repository, `shyft` provides the main code base. A second repository (required for tests) is located at [shyft-data](https://gitlab.com/shyft-os/shyft-data). A third repository [shyft-doc](https://gitlab.com/shyft-os/shyft-doc) is available containing example notebooks and tutorials. The three repositories assume they have been checked out in parallel into a `shyft_workspace` directory:

```bash
mkdir shyft_workspace && cd shyft_workspace
export SHYFT_WORKSPACE=`pwd`
git clone https://gitlab.com/shyft-os/shyft.git
git clone https://gitlab.com/shyft-os/shyft-data.git
git clone https://gitlab.com/shyft-os/shyft-doc.git
```

### REQUIREMENTS


For compiling and running Shyft, you will need:

* A C++2y compiler (gcc-12 or higher)
* The BLAS and LAPACK libraries (development packages)
* A Python3 (3.8 or higher) interpreter
* The NumPy package (>= 1.15.0)
* The netCDF4 package (>= 1.2.1)
* The CMake building tool (3.19 or higher)
* 3rd party dependencies for c++ extensions and tests
  boost(>=1.78), dlib(>=19.6), armadillo(>=9.300.2), doctest, openssl, leveldb, rocksdb

In addition, a series of Python packages are needed mainly for running the tests.

### BUILDING

Please refer to the [Gitlab Wiki](https://gitlab.com/shyft-os/shyft/-/wikis/home) for building instructions.

