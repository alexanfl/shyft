#
# Function find_shop_license_file
#
# Find the Shop license file to be used.
#
# On successful return, exports the following variables to parent scope:
# - shop_license_file (exported when license configuration is found, even if file does not exist or is not valid)
# - shop_license_statement (exported only when valid license file was found)
# - shop_license_date (exported only when valid license file was found)
#
# The following conditions apply, in prioritized order:
# - If CMake variable SHOP_LICENSE is set, and not function parameter read_only
#   is specified, then write contents to a file in current build directory and
#   return its path.
# - If CMake variable SHOP_LICENSE_FILE is set, then return its value.
# - If environment variable SHOP_LICENSE is set, and not function parameter
#   read_only is specified, then write contents to a file in current build
#   directory and return its path.
# - If environment variable SHOP_LICENSE_FILE is set, then return its value.
# - If variable shop_api_LICENSE_FILE is set, which is set by the shop_api
#   package when it includes a license file, then return its value.
#
function(find_shop_license_file read_only)
    if(NOT read_only AND DEFINED SHOP_LICENSE)
        message(VERBOSE "Shop license from CMake variable will be used in a temporary file")
        set(shop_license_file ${CMAKE_CURRENT_BINARY_DIR}/SHOP_license.dat)
        file(WRITE ${shop_license_file} "${SHOP_LICENSE}")
    elseif(DEFINED SHOP_LICENSE_FILE)
        message(VERBOSE "Shop license file from CMake variable will be used:${SHOP_LICENSE_FILE}")
        set(shop_license_file ${SHOP_LICENSE_FILE})
    elseif(NOT read_only AND DEFINED ENV{SHOP_LICENSE})
        message(VERBOSE "Shop license from environment variable will be used in a temporary file")
        set(shop_license_file ${CMAKE_CURRENT_BINARY_DIR}/SHOP_license.dat)
        file(WRITE ${shop_license_file} "$ENV{SHOP_LICENSE}")
    elseif(DEFINED ENV{SHOP_LICENSE_FILE})
        message(VERBOSE "Shop license file from environment variable will be used")
        set(shop_license_file $ENV{SHOP_LICENSE_FILE})
    elseif(DEFINED shop_api_LICENSE_FILE)
        message(VERBOSE "Shop license file from shop_api package will be used")
        set(shop_license_file ${shop_api_LICENSE_FILE})
    else()
        set(shop_license_file "")
    endif()
    set(shop_license_statement "")
    set(shop_license_date "")
    if(NOT "${shop_license_file}" STREQUAL "")
        message(VERBOSE "Shop license file path: ${shop_license_file}")
        if (EXISTS ${shop_license_file})
            file(READ ${shop_license_file} shop_license_data LIMIT 64) # Reading just enough to expect finding "<datetime>#Licensed to <owner>#"
            string(FIND "${shop_license_data}" "#" pos)
            if (pos GREATER 0)
                string(SUBSTRING "${shop_license_data}" 0 ${pos} shop_license_date)
                math(EXPR pos "${pos} + 1")
                string(SUBSTRING "${shop_license_data}" ${pos} -1 shop_license_data)
                string(FIND "${shop_license_data}" "#" pos)
                if (pos GREATER 0)
                    string(SUBSTRING "${shop_license_data}" 0 ${pos} shop_license_statement)
                    message(VERBOSE "Shop license statement: ${shop_license_statement}")
                    message(VERBOSE "Shop license date: ${shop_license_date}")
                    set(shop_license_statement "${shop_license_statement}")
                    set(shop_license_date "${shop_license_date}")
                else()
                    message(WARNING "Shop license file \"${shop_license_file}\" is not valid: ${SHOP_LICENSE_DATE}#${shop_license_data}")
                endif()
            else()
                message(WARNING "Shop license file \"${shop_license_file}\" is not valid: ${shop_license_data}")
            endif()
        else()
            message(WARNING "Shop license file \"${shop_license_file}\" does not exist")
        endif()
    else()
        message(VERBOSE "No Shop license: Only unlicensed features will be supported")
    endif()
    set(shop_license_file "${shop_license_file}" PARENT_SCOPE)
    set(shop_license_statement "${shop_license_statement}" PARENT_SCOPE)
    set(shop_license_date "${shop_license_date}" PARENT_SCOPE)
endfunction()
