import os
from pathlib import Path
import pytest

from shyft.time_series import YMDhms, DtsServer, DtssCfg, Calendar, TimeAxis, TimeSeries, POINT_AVERAGE_VALUE


class NetcdfData:
    def __init__(self):
        if "SHYFT_DATA" in os.environ:
            shyft_data_dir = Path(os.environ["SHYFT_DATA"])
        else:
            shyft_data_dir = Path(__file__).parents[3] / "shyft-data"
        if not shyft_data_dir.is_dir():
            raise RuntimeError(f"SHYFT_DATA directory not found: {shyft_data_dir}")
        self.repo_path = Path(shyft_data_dir) / "repository"

    @property
    def ecmwf_op_merged_vik(self) -> Path:
        return self.repo_path / "ecmwf_op_merged" / "ecmwf_op_merged_vik_red.nc"

    @property
    def ecmwf_ens_merged_vik_red(self) -> Path:
        return self.repo_path / "ecmwf_ens_merged" / "ecmwf_ens_merged_vik_red.nc"

    @staticmethod
    def arome_date_str() -> str:
        t0 = YMDhms(2015, 8, 24, 0)
        return f"{t0.year}{t0.month:02}{t0.day:02}_{t0.hour:02}"

    @property
    def arome_default2(self) -> Path:
        return self.repo_path / "arome_data_repository" / \
            f"arome_metcoop_red_default2_5km_{self.arome_date_str()}_diff_time_unit.nc"

    @property
    def arome_test2(self) -> Path:
        return self.repo_path / "arome_data_repository" / f"arome_metcoop_red_test2_5km_{self.arome_date_str()}.nc"

    @property
    def senorge_test(self) -> Path:
        return self.repo_path / "senorge_data_repository" / "senorge_test.nc"


@pytest.fixture(scope="module")
def netcdf_data_path():
    return NetcdfData()


@pytest.fixture(scope="module")
def dts_server(tmp_path_factory):
    m = 1024 * 1024
    cfg = DtssCfg(ppf=10 * m, compress=True, max_file_size=600 * m, write_buffer_size=10 * m)
    dtss = DtsServer()
    dtss.set_listening_port(0)
    dtss.default_geo_db_config = cfg
    dtss.set_container("", str(tmp_path_factory.mktemp('test')), container_type='ts_ldb')
    dtss.start_async()
    yield dtss
    dtss.clear()
    del dtss


@pytest.fixture(scope='module')
def mock_ta() -> TimeAxis:
    return TimeAxis(Calendar().time(2020, 1, 1), Calendar.DAY, 365)


@pytest.fixture(scope='module')
def mock_ts(mock_ta) -> TimeSeries:
    return TimeSeries(mock_ta, [i for i in range(len(mock_ta))], POINT_AVERAGE_VALUE)


@pytest.fixture(scope='module')
def mock_ts_exp() -> TimeSeries:
    return TimeSeries('abc')

