from pathlib import Path
import pytest
import numpy as np

from shyft.hydrology import shyftdata_dir
from shyft.hydrology.repository.netcdf.concat_data_repository import ConcatDataRepository, ConcatDataRepositoryError
from shyft.hydrology.repository.netcdf.utils import (fixup_init, calc_P, calc_RH, utm_proj,
                                                     _clip_ensemble_of_geo_timeseries, calculate_pressure_tsv,
                                                     calculate_relative_humidity_tsv)
from shyft.time_series import (Calendar, UtcPeriod, deltahours, DoubleVector, TsVector, TimeAxis, TimeSeries,
                               POINT_AVERAGE_VALUE)

utc = Calendar()


def test_fixup_init():
    """ verify pyproj backward fwd compat function to handle just our cases """
    assert fixup_init('EPSG:1234', '1.9.4') == '+init=EPSG:1234', 'add +init= when less than 2.0 and start with EPSG'
    assert fixup_init('+proj=EPSG:1234', '1.9.4') == '+proj=EPSG:1234', 'do not tukle +init= when less than 2.0 and start not with EPSG'
    assert fixup_init('EPSG:1234', '2.0.0') == 'EPSG:1234', 'do not add +init after '
    assert fixup_init('+init=EPSG:1234', '2.0.0') == 'EPSG:1234', 'strip off +init= to avoid warning if version >=2.0'


def test_calc_pressure():
    p = calc_P(elev=100)
    assert abs(p - 100129.45855) < 0.001


def single_point_tsv(v: float) -> TsVector:
    ta = TimeAxis(Calendar().time(2020, 1, 1), Calendar.DAY, 1)
    return TsVector([TimeSeries(ta, v, POINT_AVERAGE_VALUE)])


def test_calc_pressure_tsv():
    p = 101325 + 1000
    elev = 100
    np.testing.assert_almost_equal(calculate_pressure_tsv(DoubleVector([elev]), single_point_tsv(p))[0].v[0],
                                   calc_P(elev, p))


def test_calc_rel_hum():
    rh = calc_RH(T=np.array([273.16 + 15.0]), Td=279.0, p=100129)
    assert abs(rh[0] - 0.54) < 0.1


def test_calc_rel_hum_tsv():
    temp = 273.16 + 15.0
    dew_temp = 279.0
    pressure = 100129
    rh = calculate_relative_humidity_tsv(single_point_tsv(temp), single_point_tsv(dew_temp), single_point_tsv(pressure))
    np.testing.assert_almost_equal(rh[0].v[0], calc_RH(np.array([temp]), dew_temp, pressure)[0])


def test_utm_proj():
    assert utm_proj('EPSG:32633')
    assert not utm_proj('EPSG:4326')


def test_clip_ensemble_of_geo_timeseries() -> None:
    f_ens = Path(shyftdata_dir) / "repository" / "ecmwf_ens_merged" / "ecmwf_ens_merged_box_red.nc"
    ec_ens = ConcatDataRepository(32633, filename=f_ens)
    source_names = ['precipitation', 'temperature']
    t0 = int(utc.time(2019, 9, 1, 0))
    ec_ens_fcst = ec_ens.get_forecast_ensemble(source_names, None, t0, geo_location_criteria=None)

    ens_clipped = _clip_ensemble_of_geo_timeseries(ec_ens_fcst, None, ConcatDataRepositoryError)
    assert ens_clipped == ec_ens_fcst

    period = UtcPeriod(t0, t0 + deltahours(24))
    ens_clipped = _clip_ensemble_of_geo_timeseries(ec_ens_fcst, period, ConcatDataRepositoryError)
    for e in ens_clipped:
        assert e['precipitation'][0].ts.total_period() == period
        assert e['temperature'][0].ts.total_period() == UtcPeriod(t0, t0 + deltahours(27))
    ens_is_opt = _clip_ensemble_of_geo_timeseries(ens_clipped, period, ConcatDataRepositoryError)
    assert ens_is_opt == ens_clipped

    period = UtcPeriod(t0, t0 + deltahours(2400))
    ens_clipped = _clip_ensemble_of_geo_timeseries(ec_ens_fcst, period, ConcatDataRepositoryError,
                                                   allow_shorter_period=True)
    assert ens_clipped == ec_ens_fcst
    with pytest.raises(ConcatDataRepositoryError, match="Found time axis that does not cover utc_period."):
        _clip_ensemble_of_geo_timeseries(ec_ens_fcst, period, ConcatDataRepositoryError, allow_shorter_period=False)

    assert ens_clipped == ec_ens_fcst
    with pytest.raises(Exception, match="Found time axis that does not cover utc_period."):
        _clip_ensemble_of_geo_timeseries(ec_ens_fcst, period, allow_shorter_period=False)

    with pytest.raises(ConcatDataRepositoryError, match="Found time axis that does not intersect utc_period."):
        period = UtcPeriod(t0 + deltahours(2400), t0 + deltahours(2424))
        _clip_ensemble_of_geo_timeseries(ec_ens_fcst, period, ConcatDataRepositoryError, allow_shorter_period=False)
