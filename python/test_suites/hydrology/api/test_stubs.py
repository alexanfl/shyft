import io
from shyft.utilities.stub_generation import BoostModuleInspect

def test_boost_module_inspect_pt_gs_k():
    from shyft.hydrology.pt_gs_k import _pt_gs_k
    i = BoostModuleInspect(_pt_gs_k)
    assert i.name
    assert i.doc
    assert i.functions
    assert i.classes
    assert not i.enums
    assert not i.variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s