from shyft.hydrology import GlacierMeltParameter
from shyft.hydrology import glacier_melt_step
from shyft.time_series import deltahours
from shyft.time_series import Calendar

from shyft.time_series import TimeAxis
from shyft.time_series import TimeSeries
from shyft.time_series import point_interpretation_policy as fx_policy
from shyft.time_series import create_glacier_melt_ts_m3s
from shyft.time_series import DoubleVector as dv
from numpy.testing import assert_array_almost_equal
import numpy as np

"""Verify and illustrate GlacierMelt routine and GlacierMeltTs exposure to python
"""


def test_glacier_melt_parameter():
    p = GlacierMeltParameter(5.0)
    assert round(abs(p.dtf - 5.0), 7) == 0
    assert round(abs(p.direct_response - 0.0), 7) == 0
    p = GlacierMeltParameter(5.0, 1.0)
    assert round(abs(p.dtf - 5.0), 7) == 0
    assert round(abs(p.direct_response - 1.0), 7) == 0
    p.direct_response = 0.5
    assert round(abs(p.direct_response - 0.5), 7) == 0


def test_glacier_melt_step_function():
    dtf = 6.0
    temperature = 10.0
    area_m2 = 3600.0/0.001
    sca = 0.5*area_m2
    gf = 1.0*area_m2
    m = glacier_melt_step(dtf, temperature, sca, gf)
    assert round(abs(1.25 - m), 7) == 0  # mm/h
    assert round(abs(0.0 - glacier_melt_step(dtf, 0.0, sca, gf)), 5) == 0, 'no melt at 0.0 deg C'
    assert round(abs(0.0 - glacier_melt_step(dtf, 10.0, 0.7, 0.6)), 5) == 0, 'no melt when glacier is covered'


def test_glacier_melt_ts_m3s():
    utc = Calendar()
    t0 = utc.time(2016, 10, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    area_m2 = 487*1000*1000  # Jostedalsbreen, largest i Europe
    temperature = TimeSeries(ta=ta, fill_value=10.0, point_fx=fx_policy.POINT_AVERAGE_VALUE)
    sca_values = dv.from_numpy(np.linspace(area_m2*1.0, 0.0, num=n))
    sca = TimeSeries(ta=ta, values=sca_values, point_fx=fx_policy.POINT_AVERAGE_VALUE)
    gf = 1.0*area_m2
    dtf = 6.0
    melt_m3s = create_glacier_melt_ts_m3s(temperature, sca, gf, dtf)  # Here we get back a melt_ts, that we can do ts-stuff with
    assert melt_m3s is not None
    full_melt_m3s = glacier_melt_step(dtf, 10.0, 0.0, gf)
    expected_melt_m3s = np.linspace(0.0, full_melt_m3s, num=n)
    assert_array_almost_equal(expected_melt_m3s, melt_m3s.values.to_numpy(), 4)
    # Just to check we can work with the result as a ts in all aspects
    mx2 = melt_m3s*2.0
    emx2 = expected_melt_m3s*2.0
    assert_array_almost_equal(emx2, mx2.values.to_numpy(), 4)
