import numpy as np
from shyft.time_series import (time, GeoTsMatrix, IntVector, GeoTimeSeries, GeoTimeSeriesVector, TsVector, TimeSeries, TimeAxis, point_interpretation_policy as ts_point_fx, deltahours)
from shyft.hydrology import GeoPointVector
from shyft.hydrology import GeoPoint
from shyft.hydrology import TemperatureSourceVector, TemperatureSource
from shyft.hydrology import PrecipitationSourceVector, PrecipitationSource
from shyft.hydrology import RelHumSourceVector
from shyft.hydrology import RadiationSourceVector
from shyft.hydrology import create_temperature_source_vector_from_np_array
from shyft.hydrology import create_precipitation_source_vector_from_np_array
from shyft.hydrology import create_rel_hum_source_vector_from_np_array
from shyft.hydrology import create_radiation_source_vector_from_np_array
from shyft.hydrology import ARegionEnvironment


def test_create_xx_source_vector():
    # arrange the setup
    a = np.array([[1.1, 1.2, 1.3], [2.1, 2.2, 2.3]], dtype=np.float64)
    ta = TimeAxis(deltahours(0), deltahours(1), 3)
    gpv = GeoPointVector()
    gpv[:] = [GeoPoint(1, 2, 3), GeoPoint(4, 5, 6)]
    cfs = [(create_precipitation_source_vector_from_np_array, PrecipitationSourceVector),
           (create_temperature_source_vector_from_np_array, TemperatureSourceVector),
           (create_radiation_source_vector_from_np_array, RadiationSourceVector),
           (create_rel_hum_source_vector_from_np_array, RelHumSourceVector),
           (create_radiation_source_vector_from_np_array, RadiationSourceVector)]
    # test all creation types:
    for cf in cfs:
        r = cf[0](ta, gpv, a, ts_point_fx.POINT_AVERAGE_VALUE)  # act here
        assert isinstance(r, cf[1])  # then the asserts
        assert len(r) == len(gpv)
        for i in range(len(gpv)):
            assert r[i].mid_point() == gpv[i]
            assert np.allclose(r[i].ts.values.to_numpy(), a[i])
            assert r[i].ts.point_interpretation() == ts_point_fx.POINT_AVERAGE_VALUE


def test_create_xx_vector_from_list():
    """ verify we can construct from list of xxSource"""
    ts = TemperatureSource(
        GeoPoint(1.0, 2.0, 3.0),
        TimeSeries(TimeAxis(0, 3600, 10), fill_value=1.0, point_fx=ts_point_fx.POINT_AVERAGE_VALUE)
    )
    tsv = TemperatureSourceVector([ts])
    assert len(tsv) == 1
    assert len(TemperatureSourceVector([])) == 0
    assert len(TemperatureSourceVector([ts, ts])) == 2

    a = TemperatureSourceVector([ts.geo_ts])  # verify we can create from list of geo_ts type
    assert len(a) == 1
    assert a[0].geo_ts == ts.geo_ts  # ok, equal.
    # test and demonstrate how we  easiliy construct from the new GeoTimeSeries, or GeoTimeSeriesVector
    # also shows the geo_tsv property
    b = TemperatureSourceVector(GeoTimeSeriesVector([ts.geo_ts, ts.geo_ts]))  # verify we can create directly from GeoTimeSeriesVector
    assert len(b) == 2
    assert b[0].geo_ts == ts.geo_ts
    b_geo_tsv = b.geo_tsvector
    assert len(b_geo_tsv) == 2


def test_create_xx_source_from_geo_ts():
    p0 = GeoPoint(1, 2, 3)
    ts0 = TimeSeries(TimeAxis(0, 3600, 10), fill_value=1.0, point_fx=ts_point_fx.POINT_AVERAGE_VALUE)
    gts0 = GeoTimeSeries(p0, ts0)
    for src_type in [TemperatureSource, PrecipitationSource]:  # test for more than one..
        t_ts = src_type(gts0)
        assert t_ts
        assert t_ts.mid_point() == gts0.mid_point
        assert t_ts.ts == gts0.ts
        assert t_ts.geo_ts == gts0  # we can easily get back the geo_ts, it aliases the underlying time-series, but copies the geo-point


def test_create_tsvector_from_ts_list():
    ts_list = [TimeSeries(TimeAxis(0, 3600, 10), fill_value=float(i), point_fx=ts_point_fx.POINT_AVERAGE_VALUE) for i in range(3)]
    tsv = TsVector(ts_list)
    assert tsv
    assert len(tsv) == 3
    assert tsv[0].value(0) == 0.0
    assert tsv[1].value(0) == 1.0
    assert tsv[2].value(0) == 2.0


def test_create_a_region_env_from_geo_ts_matrix():
    """ Demo and test how to create a complete ARegionEnvironment from a GeoTsMatrix - in one line! """
    # arrange test data
    gs = GeoTsMatrix(n_t0=2, n_v=5, n_e=2, n_g=2)
    for t in range(gs.shape.n_t0):
        ta = TimeAxis(time('2000-01-01T03:00:00Z') + t*time(3600)*3, time(3600), 3)
        for v in range(gs.shape.n_v):
            for e in range(gs.shape.n_e):
                for g in range(gs.shape.n_g):
                    gs.set_geo_point(t, v, e, g, GeoPoint(g, 0.0, 0.0))
                    gs.set_ts(t, v, e, g, TimeSeries(ta=ta, fill_value=t*1000 + v*100 + e*10 + g, point_fx=ts_point_fx.POINT_AVERAGE_VALUE))

    def verify_env(a: ARegionEnvironment, t: int, e: int) -> bool:
        assert a
        for n, v in a.variables:
            ix_of = {'temperature': 0, 'precipitation': 1, 'radiation': 2, 'wind_speed': 3, 'rel_hum': 4}
            vix = ix_of[n]
            v_a = v.geo_tsvector  # this is the geo_tsvector of xxx source vectors,
            v_b = gs.extract_geo_ts_vector(t, vix, e)  # this is what we expect of geo midpoint and time-series
            assert v_a == v_b

    for t in range(gs.shape.n_t0):
        for e in range(gs.shape.n_e):
            a = ARegionEnvironment.create_from_geo_ts_matrix(m=gs, t=t, e=e, v=IntVector())  # act: pay attention to this line, all there is to make it
            verify_env(a, t, e)  # asserts
