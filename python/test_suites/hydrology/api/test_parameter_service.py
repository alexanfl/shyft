from shyft.hydrology import (ParameterServer,ParameterClient,ParameterModel,KirchnerParameter,GammaSnowParameter)
from shyft.hydrology.pt_gs_k import PTGSKParameter, PTGSKParameterMap
from shyft.time_series import time, ModelInfo, Int64Vector


def mk_parameter_data_example(n: int = 10)->PTGSKParameterMap:
    m=PTGSKParameterMap()
    for i in range(n):
        m[i]=PTGSKParameter()
        m[i].kirchner.c1 = 1.0 + float(i)/float(n)
    return m

def test_parameter_map_eq():
    a=mk_parameter_data_example(n=10)
    b=mk_parameter_data_example(n=10)
    assert a==b
    a[0].kirchner.c1 = 2.123
    assert a!=b
def test_param_map():
    p=PTGSKParameterMap()
    p[0]=PTGSKParameter()
    p[1]=PTGSKParameter()
    assert p[0] == p[1]
    assert len(p)==2
    p[0].kirchner.c1=1.4
    assert p[0].kirchner.c1 == 1.4
    kv = [ (d.key(),d.data()) for d in p]
    assert len(kv) ==2
    assert p[0] != p[1]
    q=PTGSKParameterMap()
    q[0]=PTGSKParameter()
    q[1]=PTGSKParameter()
    q[0].kirchner.c1=1.4
    assert q[0] == p[0]
    assert q[1] == p[1]
    assert q == p


def test_parameter_data_server_basics(tmpdir):
    model_dir = (tmpdir/"t_gparameter_srv_basics")
    if not model_dir.exists():
        model_dir.mkdir()
    s = ParameterServer(root_dir=str(model_dir))  # just tell the Server where to store it's model.
    try :
        #s.set_listening_port(10000) # you can specify the server port in real life, but in test, we autoallocate ports to avoid conflicts
        port = s.start_server()   # if you do not tell  the server, it will find a free port
        assert port > 0
        all_infos = Int64Vector()  # specify empty to get all infos
        c = ParameterClient(host_port=f'localhost:{port}',timeout_ms=1000)
        m_infos=c.get_model_infos(all_infos)
        assert len(m_infos)==0
        m=ParameterModel()
        m.parameters=mk_parameter_data_example(n=100)
        m_info=ModelInfo(id=0,name='example model',created=time.now(), json='{"descript":"any thing usefule for filtering goes here"}')
        mid=c.store_model(m,m_info)  # here we store the model, with the decorated m_info, that we can later use for filtering
        m_infos=c.get_model_infos(all_infos)  # we can specify spesific infos, or empty typed Int64Vector will give all
        assert len(m_infos) == 1
        assert m_infos[0].id == mid  # because that's the only model we have stored so far
        mx= c.read_model(mid=mid)
        mxv = c.read_models([mid])  # you can read many models in one go(save time!)
        assert len(mxv) == 1
        mx1 = mxv[0]
        assert mx.id == mx1.id  # should give same model in this case
        assert mx.parameters == mx1.parameters  # can compare the model, but not the .parameters, which is a variant, unexposed
        assert mx == mx1
        # update the model info (not the model, but associated info)
        m_info.json='{"labels":["operative","pt_gs_k","nea-nidelv"]}'
        m_info.id=mid  # required, extra check, the mi.id must equal the mid=mid, below
        c.update_model_info(mid=mid,mi=m_info)
        m_infos=c.get_model_infos(all_infos)
        assert m_info.json == m_infos[0].json
        c.remove_model(mid=mid)
        assert len(c.get_model_infos(all_infos)) == 0  # now there should be zero left.
        c.close()
    finally:
        s.stop_server()

if __name__ == '__main__':
    from pathlib import Path
    
    test_parameter_data_server_basics(tmpdir=Path("/tmp"))