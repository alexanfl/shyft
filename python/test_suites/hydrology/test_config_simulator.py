from os import path

from shyft.hydrology import shyftdata_dir
from shyft.hydrology.repository.default_state_repository import DefaultStateRepository
from shyft.hydrology.repository.netcdf.cf_ts_repository import CFTsRepository
from shyft.hydrology.repository.netcdf.cf_region_model_repository import CFRegionModelRepository
from shyft.hydrology.repository.netcdf.cf_geo_ts_repository import CFDataRepository
from shyft.hydrology.orchestration.configuration.yaml_configs import YAMLSimConfig
from shyft.hydrology.orchestration.configuration.yaml_configs import YAMLCalibConfig
from shyft.hydrology.orchestration.simulators.config_simulator import ConfigSimulator
from shyft.hydrology.orchestration.simulators.config_simulator import ConfigCalibrator

from shyft.time_series import IntVector, Calendar, DoubleVector, TimeSeries
from shyft.hydrology.orchestration.configuration.yaml_configs import utctime_from_datetime
import datetime as dt


def test_utctime_from_datetime():
    utc = Calendar()
    dt1 = dt.datetime(2015, 6, 1, 2, 3, 4)
    t1 = utctime_from_datetime(dt1)
    assert t1 == utc.time(2015, 6, 1, 2, 3, 4)


def test_run_geo_ts_data_config_simulator():
    # These config files are versioned in shyft-data git
    config_dir = path.join(shyftdata_dir, "neanidelv", "yaml_config")
    config_file = path.join(config_dir, "neanidelva_simulation.yaml")
    config_section = "neanidelva"
    cfg = YAMLSimConfig(config_file, config_section, overrides={'config': {'number_of_steps': 168}})

    # These config files are versioned in shyft-data git. Read from ${SHYFTDATA}/netcdf/orchestration-testdata/
    simulator = ConfigSimulator(cfg)

    # Regression tests on interpolation parameters
    assert round(abs(simulator.region_model.interpolation_parameter.precipitation.scale_factor - 1.01), 3) == 0

    # n_cells = simulator.region_model.size()
    state_repos = DefaultStateRepository(simulator.region_model)
    simulator.run(cfg.time_axis, state_repos.get_state(0))
    cids = IntVector()
    discharge = simulator.region_model.statistics.discharge(cids)
    snow_sca = simulator.region_model.statistics.snow_sca(cids)  # time-series average for cids
    snow_sca_v = simulator.region_model.statistics.snow_sca(cids, 2)  # raster at ix 2
    snow_sca_f = simulator.region_model.statistics.snow_sca_value(cids, 2)  # average value at ix 2
    snow_swe = simulator.region_model.statistics.snow_swe(cids)
    snow_swe_v = simulator.region_model.statistics.snow_swe(cids, 2)
    snow_swe_f = simulator.region_model.statistics.snow_swe_value(cids, 2)
    assert snow_sca is not None
    assert snow_sca_v is not None
    assert isinstance(snow_sca_f, float)
    assert isinstance(snow_swe, TimeSeries)
    assert isinstance(snow_swe_v, DoubleVector)
    assert isinstance(snow_swe_f, float)

    # Regression tests on discharge values
    assert round(abs(discharge.values[0] - 80.23843199 - 3.498), 3) == 0
    assert round(abs(discharge.values[3] - 82.50344985 - 3.415), 3) == 0
    # Regression tests on geo fractions
    assert round(abs(simulator.region_model.cells[0].geo.land_type_fractions_info().unspecified() - 1.0), 3) == 0
    assert round(abs(simulator.region_model.cells[2].geo.land_type_fractions_info().unspecified() - 0.1433), 3) == 0
    assert round(abs(simulator.region_model.cells[2].geo.land_type_fractions_info().forest() - 0.0), 3) == 0
    assert round(abs(simulator.region_model.cells[2].geo.land_type_fractions_info().reservoir() - 0.8566), 3) == 0


def test_run_geo_ts_data_config_calibrator():
    # These config files are versioned in shyft git
    config_dir = path.join(shyftdata_dir, "neanidelv", "yaml_config")
    config_file = path.join(config_dir, "neanidelva_calibration.yaml")
    print(config_file)
    config_section = "neanidelva"
    cfg = YAMLCalibConfig(config_file, config_section)
    calibrator = ConfigCalibrator(cfg)

    # Regression tests on interpolation parameters
    assert round(abs(calibrator.region_model.interpolation_parameter.precipitation.scale_factor - 1.01), 3) == 0
