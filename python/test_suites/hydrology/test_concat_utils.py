import pytest

from shyft.hydrology import GeoPointSource, GeoPointSourceVector, TemperatureSource, TemperatureSourceVector
from shyft.hydrology.repository.netcdf.concat_utils import (Variable, Get, TemperatureShift, PrecipitationDerivative,
                                                            RadiationDerivative, WindSpeed, RelativeHumidity,
                                                            RelativeHumiditySea, ConcatSourceVariables)
from shyft.time_series import (TimeAxis, Calendar, TimeSeries, POINT_AVERAGE_VALUE, GeoPoint, POINT_INSTANT_VALUE,
                               UtcPeriod)


def test_variable(mock_ts, mock_ts_exp):
    assert Variable('test', '1').get_timeseries() == TimeSeries()

    assert Variable('test', '1', time_series=mock_ts_exp).get_timeseries() == mock_ts_exp

    assert Variable('test', '1', time_series=mock_ts).get_timeseries() == mock_ts


def test_get_method(mock_ts, mock_ts_exp):
    for ts in [mock_ts, mock_ts_exp]:
        variable = Variable('test', '1', ts)

        method = Get(variable)
        assert method.variable_names == [variable.name]
        assert method.get_timeseries() == ts


def test_get_variable_create_source(mock_ts):
    method = Get(Variable('test', '1', time_series=mock_ts))
    gp = GeoPoint(0, 0, 0)

    method.set_geo_point(gp)
    assert method.geo_point == gp

    method.set_source(POINT_INSTANT_VALUE, TemperatureSource, TemperatureSourceVector)

    src = method.create_source()
    assert src.mid_point() == gp
    assert list(src.ts.v) == list(mock_ts.v)
    assert src.ts.point_interpretation() == POINT_INSTANT_VALUE

    larger_utc_period = UtcPeriod(Calendar().time(2019, 1, 1), Calendar().time(2021, 1, 1))
    src = method.create_source(larger_utc_period)
    assert src.ts.total_period() == mock_ts.total_period()

    shorter_utc_period = UtcPeriod(Calendar().time(2020, 3, 1, 2), Calendar().time(2020, 4, 1, 2))
    src = method.create_source(shorter_utc_period)
    assert src.ts.total_period().contains(shorter_utc_period)
    assert not src.ts.total_period().contains(mock_ts.total_period())


def test_get_variable_create_source_expression(mock_ts_exp):
    method = Get(Variable('test', '1', time_series=mock_ts_exp))
    method.set_geo_point(GeoPoint(0, 0, 0))
    method.set_source(POINT_INSTANT_VALUE, GeoPointSource, GeoPointSourceVector)

    src = method.create_source()
    assert src.ts == mock_ts_exp


def test_temperature_shift_method_values(mock_ts):
    variable = Variable('test', 'K', mock_ts + 300)
    method = TemperatureShift(variable)
    assert method.get_timeseries() is not None


def test_temperature_shift_method_exp(mock_ts_exp):
    variable = Variable('test', 'K', mock_ts_exp + 300)
    method = TemperatureShift(variable)
    assert method.get_timeseries().needs_bind()


def test_temperature_shift_method_unit(mock_ts):
    variable_c = Variable('test', 'degrees_C', mock_ts)
    with pytest.raises(RuntimeError):
        TemperatureShift(variable_c)


def test_precip_derivative_method_values():
    ta = TimeAxis(Calendar().time(2020, 1, 1), Calendar.HOUR, 5)
    ts = TimeSeries(ta, [0, 0, 1, -1, 1000], POINT_AVERAGE_VALUE)
    variable = Variable('test', 'kg/m^2', ts)
    method = PrecipitationDerivative(variable)
    assert list(method.get_timeseries().v) == [0, 1, 0, 1000, 0]


def test_precip_derivative_method_exp(mock_ts_exp):
    variable = Variable('test', 'kg/m^2', mock_ts_exp)
    method = PrecipitationDerivative(variable)
    assert method.get_timeseries().needs_bind()


def test_radiation_derivative_method_values():
    ta = TimeAxis(Calendar().time(2020, 1, 1), Calendar.SECOND, 5)
    ts = TimeSeries(ta, [0, 0, 1, -1, 10000], POINT_AVERAGE_VALUE)
    variable = Variable('test', 'kg/m^2', ts)
    method = RadiationDerivative(variable)
    assert list(method.get_timeseries().v) == [0, 1, 0, 5000, 0.]


def test_radiation_derivative_method_exp(mock_ts_exp):
    variable = Variable('test', 'kg/m^2', mock_ts_exp)
    method = RadiationDerivative(variable)
    assert method.get_timeseries().needs_bind()


def test_wind_speed_method_values(mock_ta):
    x_var = Variable('x_ws', 'm/s', TimeSeries(mock_ta, 3., POINT_AVERAGE_VALUE))
    y_var = Variable('y_ws', 'm/s', TimeSeries(mock_ta, 4., POINT_AVERAGE_VALUE))
    method = WindSpeed(x_var, y_var, 'ws')
    assert method.get_timeseries().v[0] == 5.0


def test_wind_speed_method_exp(mock_ts_exp):
    x_var = Variable('x_ws', 'm/s', mock_ts_exp)
    y_var = Variable('y_ws', 'm/s', mock_ts_exp)
    method = WindSpeed(x_var, y_var, 'ws')
    assert method.get_timeseries().needs_bind()


def test_rel_hum_method_values(mock_ta):
    temp = Variable('temp', 'K', TimeSeries(mock_ta, 1., POINT_AVERAGE_VALUE))
    dew_temp = Variable('dew_temp', 'K', TimeSeries(mock_ta, 1., POINT_AVERAGE_VALUE))
    pressure = Variable('pressure', 'Pa', TimeSeries(mock_ta, 1., POINT_AVERAGE_VALUE))
    method = RelativeHumidity(temp, dew_temp, pressure, 'rel_hum')
    assert method.get_timeseries().v[0] > 0.


def test_rel_hum_method_exp(mock_ts_exp):
    temp = Variable('temp', 'K', mock_ts_exp)
    dew_temp = Variable('dew_temp', 'K', mock_ts_exp)
    pressure = Variable('pressure', 'Pa', mock_ts_exp)
    method = RelativeHumidity(temp, dew_temp, pressure, 'rel_hum')
    assert method.get_timeseries().needs_bind()


def test_rel_hum_sea_method_values(mock_ta):
    temp = Variable('temp', 'K', TimeSeries(mock_ta, 1., POINT_AVERAGE_VALUE))
    dew_temp = Variable('dew_temp', 'K', TimeSeries(mock_ta, 1., POINT_AVERAGE_VALUE))
    pressure = Variable('pressure', 'Pa', TimeSeries(mock_ta, 1., POINT_AVERAGE_VALUE))
    method = RelativeHumiditySea(temp, dew_temp, pressure, 'rel_hum')
    method.set_geo_point(GeoPoint(0, 0, 100.))
    assert method.get_timeseries().v[0] > 0.


def test_rel_hum_sea_method_exp(mock_ts_exp):
    temp = Variable('temp', 'K', mock_ts_exp)
    dew_temp = Variable('dew_temp', 'K', mock_ts_exp)
    pressure = Variable('pressure', 'Pa', mock_ts_exp)
    method = RelativeHumiditySea(temp, dew_temp, pressure, 'rel_hum')
    method.set_geo_point(GeoPoint(0, 0, 100.))
    assert method.get_timeseries().needs_bind()


def test_concat_source_variables():
    source_method = ConcatSourceVariables()
    assert source_method.get_methods([]) == []
    assert source_method.get_methods(['other_variable']) == []


def test_temperature_sources():
    source_method = ConcatSourceVariables()

    methods = source_method.get_methods(['temperature'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'temperature'

    methods = source_method.get_methods(['air_temperature_2m'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'temperature'


def test_precipitation_sources():
    sources = ConcatSourceVariables()

    methods = sources.get_methods(['precipitation'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'precipitation'

    methods = sources.get_methods(['precipitation_amount_acc'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'precipitation'


def test_radiation_sources():
    sources = ConcatSourceVariables()

    methods = sources.get_methods(['radiation'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'radiation'

    methods = sources.get_methods(['integral_of_surface_downwelling_shortwave_flux_in_air_wrt_time'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'radiation'


def test_wind_speed_sources():
    sources = ConcatSourceVariables()

    methods = sources.get_methods(['windspeed'])
    assert len(methods) == 1
    assert methods[0].variable_names == ['windspeed']
    assert methods[0].source_input_name == 'wind_speed'

    methods = sources.get_methods(['x_wind', 'y_wind'])
    assert len(methods) == 3
    assert'wind_speed' in [m.source_input_name for m in methods]


def test_relative_humidity_sources():
    sources = ConcatSourceVariables()

    methods = sources.get_methods(['relative_humidity'])
    assert len(methods) == 1
    assert methods[0].source_input_name == 'relative_humidity'

    methods = sources.get_methods(['air_temperature_2m', 'dew_point_temperature_2m', 'surface_air_pressure'])
    assert len(methods) == 3
    assert 'relative_humidity' in [m.source_input_name for m in methods]
