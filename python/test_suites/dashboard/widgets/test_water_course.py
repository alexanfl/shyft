import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.dashboard.apps.water_route_app.example_hps import get_example_hps
from shyft.dashboard.apps.water_route_app.simplified_water_route import SimplifiedWaterRouteGraph

from shyft.dashboard.widgets.water_route_widget import WaterRouteWidget


def test_water_route_widget():
    hps = get_example_hps()
    water_route_graph = SimplifiedWaterRouteGraph()
    widget = WaterRouteWidget(water_route_graph=water_route_graph)
    widget.receive_selected_water_route([hps])

    # Assert that the figure has been updated
    assert widget.fig.x_range.start != 0
    assert widget.fig.y_range.start != 0

    assert widget.reservoirs_data_dict['tags'] == [r.tag for r in hps.reservoirs.values()]
    assert widget.power_station_data_dict['tags'] == [ps.tag for ps in hps.power_stations.values()]

    # Check for ocenas. 2 beziers per ocean
    assert len(widget.ds_oceans.data['sx']) == 2
    assert len(widget.ds_oceans_spillage.data['sx']) == 4
    assert len(widget.ds_oceans_bypass.data['sx']) == 4

    all_main_tags = [ww.tag for ww in hps.waterways if ww.upstream_role.name =='main']
    for tag in widget.ds_main_water.data['tags']:
        assert tag in all_main_tags
    for swr in filter(lambda x: x.upstream_role.name == 'flood', hps.waterways):
        assert swr.tag in widget.ds_spillage.data['tags']
    for bpwr in filter(lambda x: x.upstream_role.name == 'bypass', hps.waterways):
        assert bpwr.tag in widget.ds_bypass.data['tags']
