from shyft.dashboard.widgets.sliders import SliderSelect, RangeSliderSelect, SliderData, RangeSliderData
from shyft.dashboard.base.ports import Receiver, Sender, connect_ports
from typing import (Dict, List, Any, Union, Tuple, Optional, Callable)

def test_slider_selector():
    # Create a slider
    slider = SliderSelect(width=300, title="Test slider", start=0, step=1, end=50, value=10)

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _recieve_10(number: int) -> None:
        assert number == 10
        call_count[0] += 1

    # set up port to both receive and send data to/from the slider
    receive_assert_port_recieve = Receiver(parent=_recieve_10, name='_recieve_10', func=_recieve_10, signal_type=int)
    send_assert_port_send = Sender(parent='parent', name='parameters', signal_type=SliderData)
    # connect the ports
    connect_ports(slider.send_slider_value, receive_assert_port_recieve)
    connect_ports(send_assert_port_send, slider.receive_param)
    # test data that will not trigger a callback
    test_data = SliderData(start=0, end=50, step=1, value=10, callback=False)
    assert call_count[0] == 0
    # test data that will trigger a callback
    test_data = SliderData(start=0, end=50, step=1, value=10, callback=True)
    send_assert_port_send(test_data)
    assert call_count[0] == 1


def test_rangeslider_selector():
    # Create a slider
    import bokeh
    print(bokeh.__version__)
    import bokeh.models as bm
    bm.RangeSlider(start=0, step=1, end=20, value=(1,5))
    rangeslider = RangeSliderSelect(width=300, title="Test range slider", start=0, step=1, end=20)

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _receive_number_assert(number: Tuple[float, float]) -> None:
        assert number == (0, 10)
        call_count[0] += 1

    # set up port to both receive and send data to/from the slider
    receive_number_assert = Receiver(parent='_recieve_10', name='_recieve_10', func=_receive_number_assert,
                                     signal_type=Tuple[float, float])
    send_number_assert = Sender(parent='parent', name='parameters', signal_type=RangeSliderData)
    # connect the ports
    connect_ports(rangeslider.send_slider_value, receive_number_assert)
    connect_ports(send_number_assert, rangeslider.receive_param)
    # test data that will not trigger a callback
    test_data = RangeSliderData(start=0, end=20, step=1, range=(0, 20), callback=False)
    assert call_count[0] == 0
    # test data that will trigger a callback
    test_data = RangeSliderData(start=0, end=20, step=1, range=(0, 10), callback=True)
    send_number_assert(test_data)
    assert call_count[0] == 1
