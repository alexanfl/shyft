from typing import List

#from shyft.dashboard.test.time_series.test_time_series_fixtures import mock_ds_view_handle_class

from shyft.dashboard.base.ports import Receiver, connect_ports
from bokeh import events
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle
from shyft.dashboard.time_series.ds_view_handle_registry import (DsViewHandleRegistry, DsViewHandleRegistryApp,
                                                                 DsvhRegistryPolicy)


def test_dsvh_registry(mock_ds_view_handle_class):

    d1 = mock_ds_view_handle_class(tag=1)
    d2 = mock_ds_view_handle_class(tag=2)
    d22 = mock_ds_view_handle_class(tag=2)  # a second one with tag 2
    d3 = mock_ds_view_handle_class(tag=3)
    d4 = mock_ds_view_handle_class(tag=4)
    d42 = mock_ds_view_handle_class(tag=4)  # a second one with tag 4
    d5 = mock_ds_view_handle_class(tag=5)

    # create dsvh registry
    dsvh_reg = DsViewHandleRegistry()
    # check len obj
    assert len(dsvh_reg) == 0
    assert not dsvh_reg

    # initial add
    dsvh = [d1, d2]
    dsvh_reg.add(dsvh)
    assert len(dsvh_reg) == 2
    assert set(dsvh_reg.ds_view_handles) == set(dsvh)

    #  test add
    dsvh = [d22, d3, d4]
    # should add d3 and d4 but not d2 since d2 is already in registry
    dsvh_reg.add(dsvh)
    assert len(dsvh_reg) == 4
    assert set(dsvh_reg.ds_view_handles) == {d1, d2, d3, d4}

    # test registry function
    reg_d = dsvh_reg.registry
    assert reg_d == {1: d1, 2: d2, 3: d3, 4: d4}

    # test replace method
    dsvh = [d42, d5]
    # should add d5 not d4 since d4 is already in registry remove the rest
    dsvh_reg.replace(dsvh)
    assert len(dsvh_reg) == 2
    assert set(dsvh_reg.ds_view_handles) == {d4, d5}

    # test remove dsvh method
    dsvh = [d42]
    # should remove d4 even if the value d42 is not the same obj as d4 but has same tag
    dsvh_reg.remove(dsvh)
    assert len(dsvh_reg) == 1
    assert set(dsvh_reg.ds_view_handles) == {d5}

    # test empty registry
    dsvh_reg.empty_registry()
    assert len(dsvh_reg) == 0
    assert not dsvh_reg


def test_dsvh_registry_app(mock_ds_view_handle_class):

    class TestReceiver:

        def __init__(self):
            self.values_to_add = None
            self.add_count = 0
            self.values_to_remove = None
            self.remove_count = 0
            self.receive_ds_view_handles_to_add = Receiver(parent=self, name="Receive List[DsViewHandles] to add",
                                                           signal_type=List[DsViewHandle],
                                                           func=self._on_receive_add)
            self.receive_ds_view_handles_to_remove = Receiver(parent=self, name="Receive List[DsViewHandles] to remove",
                                                              signal_type=List[DsViewHandle],
                                                              func=self._on_receive_remove)

        def _on_receive_add(self, dsvh: List[DsViewHandle]):
            self.values_to_add = dsvh
            self.add_count += 1

        def _on_receive_remove(self, dsvh: List[DsViewHandle]):
            self.values_to_remove = dsvh
            self.remove_count += 1

    d1 = mock_ds_view_handle_class(tag=1)
    d2 = mock_ds_view_handle_class(tag=2)
    d22 = mock_ds_view_handle_class(tag=2)  # a second one with tag 2
    d3 = mock_ds_view_handle_class(tag=3)
    d4 = mock_ds_view_handle_class(tag=4)
    d42 = mock_ds_view_handle_class(tag=4)  # a second one with tag 4
    d5 = mock_ds_view_handle_class(tag=5)

    test_receiver = TestReceiver()

    reg_app = DsViewHandleRegistryApp()
    connect_ports(reg_app.send_ds_view_handles_to_add, test_receiver.receive_ds_view_handles_to_add)
    connect_ports(reg_app.send_ds_view_handles_to_remove, test_receiver.receive_ds_view_handles_to_remove)

    reg_app.layout
    reg_app.layout_components

    # set policy to ADD
    reg_app.policy_buttons.active = 1

    # initial add
    dsvh = [d1, d2]
    reg_app.receive_ds_view_handles_to_register(dsvh)
    assert len(reg_app.dsvh_registry) == 2
    assert set(reg_app.dsvh_registry.ds_view_handles) == set(dsvh)
    assert set(test_receiver.values_to_add) == set(dsvh)
    assert test_receiver.add_count == 1

    #  test add
    dsvh = [d22, d3, d4]
    # should add 3 and 4 but not 2 since 2 is already in registry
    reg_app.receive_ds_view_handles_to_register(dsvh)
    assert len(reg_app.dsvh_registry) == 4
    assert set(reg_app.dsvh_registry.ds_view_handles) == {d1, d2, d3, d4}
    assert set(test_receiver.values_to_add) == {d3, d4}
    assert test_receiver.add_count == 2

    # test add same things again nothing should happen
    dsvh = [d22, d3, d4]
    # should add 3 and 4 but not 2 since 2 is already in registry
    reg_app.receive_ds_view_handles_to_register(dsvh)
    assert len(reg_app.dsvh_registry) == 4
    assert set(reg_app.dsvh_registry.ds_view_handles) == {d1, d2, d3, d4}
    assert set(test_receiver.values_to_add) == {d3, d4}
    assert test_receiver.add_count == 2

    # set policy to REPLACE
    reg_app.policy_buttons.active = 0
    assert reg_app.policy == DsvhRegistryPolicy.REPLACE

    # test replace method
    dsvh = [d42, d5]
    # should add 5 not 4 since 4 is already in registry remove the rest
    reg_app.receive_ds_view_handles_to_register(dsvh)
    assert len(reg_app.dsvh_registry) == 2
    assert set(reg_app.dsvh_registry.ds_view_handles) == {d4, d5}
    assert set(test_receiver.values_to_remove) == {d1, d2, d3}
    assert set(test_receiver.values_to_add) == {d5}
    assert test_receiver.add_count == 3
    assert test_receiver.remove_count == 1

    # test replace method with same values, nothing should happen now
    dsvh = [d42, d5]
    # should add 5 not 4 since 4 is already in registry remove the rest
    reg_app.receive_ds_view_handles_to_register(dsvh)
    assert len(reg_app.dsvh_registry) == 2
    assert set(reg_app.dsvh_registry.ds_view_handles) == {d4, d5}
    assert set(test_receiver.values_to_remove) == {d1, d2, d3}
    assert set(test_receiver.values_to_add) == {d5}
    assert test_receiver.add_count == 3
    assert test_receiver.remove_count == 1

    # test that nothing happens if policy is wrong
    dsvh = [d22, d3, d4]
    current_policy = reg_app.policy
    reg_app.policy = "nonsense"
    dsvh = [d22, d3, d4]
    # nothing should happen
    reg_app.receive_ds_view_handles_to_register(dsvh)
    assert len(reg_app.dsvh_registry) == 2
    assert set(reg_app.dsvh_registry.ds_view_handles) == {d4, d5}
    assert set(test_receiver.values_to_remove) == {d1, d2, d3}
    assert set(test_receiver.values_to_add) == {d5}
    assert test_receiver.add_count == 3
    assert test_receiver.remove_count == 1
    reg_app.policy = current_policy

    # test empty registry
    # TODO: verify that test works with new bokeh version, button.clicks not propagating changes
    #reg_app.clear_button.click() #s = 1
    reg_app.clear_button._trigger_event(events.ButtonClick(reg_app.clear_button))

    assert len(reg_app.dsvh_registry) == 0
    assert set(test_receiver.values_to_remove) == {d4, d5}
    assert test_receiver.remove_count == 2

    # remove all once more nothing should happen
    reg_app.receive_ds_view_handles_to_remove({d4, d5})
    assert test_receiver.remove_count == 2

    # test if wrong policy is set
    current_policy = reg_app.policy
    reg_app._change_policy("bla", 0, 5)
    assert reg_app.policy == current_policy
