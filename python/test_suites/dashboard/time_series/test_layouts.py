import pytest
from bokeh import __version__ as bokeh_version
if bokeh_version < '3.0.0':
    from bokeh.models import Panel
else:
    from bokeh.models import TabPanel as Panel

from bokeh.layouts import row

from shyft.dashboard.time_series.layouts import ViewContainerTabs, ViewContainerTabsError


def test_view_container_tabs():

    class ViewContainer:
        def __init__(self, visible):
            self.visible = visible
        @property
        def layout(self):
            return row()

    figure = ViewContainer(False)
    legend = ViewContainer(True)
    table = ViewContainer(True)
    always_invisible = ViewContainer(False)

    panels_price = [Panel(child=row(figure.layout, legend.layout), title="Figure"),
                    Panel(child=row(table.layout), title="Data Table"),
                    Panel(child=row(always_invisible.layout), title="Data figure")]
    viewer_table_price = ViewContainerTabs(panels=panels_price,
                                           visibility_view_containers=[[figure, legend], [table], []],
                                           active=0, width=1200, height=500)
    assert figure.visible
    assert legend.visible
    assert not table.visible
    assert not always_invisible.visible
    # set active to table
    viewer_table_price.viewer_table.active = 1
    assert not figure.visible
    assert not legend.visible
    assert table.visible
    assert not always_invisible.visible
    # set active to invisible figure
    viewer_table_price.viewer_table.active = 2
    assert not figure.visible
    assert not legend.visible
    assert not table.visible
    assert not always_invisible.visible

    row(viewer_table_price.layout)

    # test errors
    # len visibility_view_containers is wrong
    with pytest.raises(ViewContainerTabsError):
        panels_price = [Panel(child=row(figure.layout, legend.layout), title="Figure"),
                        Panel(child=row(table.layout), title="Data Table"),
                        Panel(child=row(always_invisible.layout), title="Data figure")]
        viewer_table_price = ViewContainerTabs(panels=panels_price,
                                               visibility_view_containers=[[figure, legend], [table]],
                                               active=0, width=1200, height=500)

    # active higher than panels
    with pytest.raises(ViewContainerTabsError):
        panels_price = [Panel(child=row(figure.layout, legend.layout), title="Figure"),
                        Panel(child=row(table.layout), title="Data Table"),
                        Panel(child=row(always_invisible.layout), title="Data figure")]
        viewer_table_price = ViewContainerTabs(panels=panels_price,
                                               visibility_view_containers=[[figure, legend], [table], []],
                                               active=10, width=1200, height=500)

