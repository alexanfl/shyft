import numpy as np
import pytest

import shyft.time_series as sa
from shyft.dashboard.time_series.renderer import BaseFigureRenderer
from shyft.dashboard.time_series.state import State

cal = sa.Calendar()


@pytest.fixture
def mock_tsv():
    t0 = cal.time(2019, 1, 1)
    ta = sa.TimeAxis(t0, cal.HOUR, 5)
    ts = sa.TimeSeries(ta, [1.0, 2, 3, 4, np.nan], sa.POINT_AVERAGE_VALUE)

    ta2 = sa.TimeAxis(t0 + cal.HOUR, cal.HOUR, 5)
    ts2 = sa.TimeSeries(ta2, [2., 1, 3, 2, 0.1], sa.POINT_AVERAGE_VALUE)
    tsv = sa.TsVector([ts, ts2])
    return State.Quantity(tsv, "W")


@pytest.fixture
def mock2_tsv():
    t0 = cal.time(2019, 1, 1)
    ta = sa.TimeAxis(t0, cal.HOUR, 5)
    ts = sa.TimeSeries(ta, [np.nan, np.nan, np.nan, np.nan, np.nan], sa.POINT_AVERAGE_VALUE)
    tsv = sa.TsVector([ts])
    return State.Quantity(tsv, "W")


class TestBaseFigureRenderer:
    def test_y_range(self, monkeypatch, mock_tsv):
        view_range = sa.UtcPeriod(cal.time(2019, 1, 1, 1), cal.time(2019, 1, 1, 4))
        view_range_all = sa.UtcPeriod(cal.time(2019, 1, 1), cal.time(2019, 1, 2))

        class MockView:
            indices = 0
            no_y_rescaling = False

        renderer = BaseFigureRenderer(unit_registry=None, notify_figure_y_range_update=False)
        renderer.view = MockView

        # check part of the validity checks
        setattr(BaseFigureRenderer, 'ts_vector', property(lambda self: None))
        np.testing.assert_equal(renderer.y_range(view_range), np.array([np.nan, np.nan]))

        # check normal operation
        # only 1. ts in tsv
        setattr(BaseFigureRenderer, 'ts_vector', property(lambda self: mock_tsv))
        renderer.view.indices = [0]
        np.testing.assert_equal(renderer.y_range(view_range), np.array([2., 4.]))

        # several ts
        setattr(BaseFigureRenderer, 'ts_vector', property(lambda self: mock_tsv))
        renderer.view.indices = [0, 1]
        np.testing.assert_equal(renderer.y_range(view_range), np.array([1., 4.]))

        # view-period included nans
        setattr(BaseFigureRenderer, 'ts_vector', property(lambda self: mock_tsv))
        renderer.view.indices = [0, 1]
        np.testing.assert_equal(renderer.y_range(view_range_all), np.array([0.1, 4.]))

        # empty ts
        setattr(BaseFigureRenderer, 'ts_vector', property(lambda self: sa.TsVector()))
        renderer.view.indices = [0, 1]
        np.testing.assert_equal(renderer.y_range(view_range_all), np.array([np.nan, np.nan]))
