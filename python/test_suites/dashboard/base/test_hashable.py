from shyft.dashboard.base.hashable import Hashable


def test_hashable():

    hashable1 = Hashable()
    hashable2 = Hashable()

    assert hashable1 != hashable2
    assert hashable1 == hashable1
    assert hashable2 == hashable2

    d = {hashable1: 2} # will not work as dict literal
    d[hashable2] = 3

    d[hashable1] = 10

    # test hashable against other objects
    assert hashable1 != 10
    assert hashable1 != None
    assert hashable1 is not None


def test_hashable_instances():

    class Foo(Hashable):

        def __len__(self):
            super().__init__()

    class Bar(Hashable):

        def __len__(self):
            super().__init__()

    f = Foo()
    f2 = Foo()
    b = Bar()

    assert f == f
    assert b == b
    assert f != f2
    assert f != b

    d = {f: 2, b: 10}  # will not work as dict literal
    d[f2] = 3
    d[f2] = 10
