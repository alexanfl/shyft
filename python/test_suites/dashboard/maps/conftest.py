import pytest
from shyft.dashboard.maps.base_map import BBoxTileBaseMap


@pytest.fixture
def base_map():
    service = 'https://services.geodataonline.no/arcgis/services/Geocache_UTM33_WGS84/GeocacheBasis/MapServer/WMSServer?'
    arguments = {'service': 'WMS',
                 'styles': 'default',
                 'request': 'GetMap',
                 'version': '1.3.0',
                 'BGCOLOR': '0xFFFFFF',
                 'format': 'image/png',
                 'crs': 'EPSG:32633',
                 'layers': '0',
                 'width': 256,
                 'height': 256,
                 # 'bbox': ['{XMIN}','{YMIN}','{XMAX}','{YMAX}']
                 }
    return BBoxTileBaseMap(service=service, arguments=arguments)
