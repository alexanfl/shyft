import pytest

from .utility import create_model, create_model_with_hydro
from shyft.energy_market.core import Model
from shyft.energy_market.core import IntStringDict
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE


def test_can_create_and_modify():
    m = Model(123, 'model name')
    assert m.name == 'model name'
    assert m.id == 123
    m.created = time(12345678)
    m.name = 'a'
    m.id = 456
    assert m.name == 'a'
    assert m.id == 456
    assert m.created == 12345678
    assert len(m.area) == 0
    m.create_model_area(12, 'a1')
    assert len(m.area) == 1


def test_equality():
    a = Model(1, 'a')
    b = Model(2, 'b')
    c = a
    assert a == c
    assert not a == b
    assert a != b
    assert a == a
    assert not a != a


def add_custom_attrs(o) -> TimeSeries:
    ts = TimeSeries(TimeAxis(0, 10, 3), fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    o.ts['custom'] = ts
    o.custom['any_ts'] = ts*2.0
    return ts


def verify_custom_attrs(o, ts: TimeSeries) -> None:
    assert o.ts['custom'] == ts
    assert o.custom['any_ts'] == ts*2.0


def test_can_serialize():
    m = Model(2, 'a')
    ts = add_custom_attrs(m)
    verify_custom_attrs(m, ts)
    blob_string = m.to_blob()
    m_blob = Model.from_blob(blob_string)
    verify_custom_attrs(m_blob, ts)
    assert m == m_blob


def test_can_add_model_area():
    m = Model(1, 'm')
    xjson: str = "{'quite':'floppy'}"
    a1 = m.create_model_area(uid=1, name='a1', json=xjson)
    assert a1.json == xjson
    assert a1.id == 1
    assert a1.name == 'a1'
    ts = add_custom_attrs(a1)
    verify_custom_attrs(a1, ts)
    a2 = m.create_model_area(2, 'a2')
    a3 = m.create_model_area(3, 'a3')
    assert len(m.area) == 3
    assert m.area[1] == a1
    assert m.area[2] == a2
    assert m.area[3] == a3
    assert a1.model.name == m.name
    m_blob = m.to_blob()
    mx = Model.from_blob(m_blob)
    assert mx == m
    assert mx.area[1].ts['custom'] == ts


def test_can_add_power_line():
    m = Model(1, 'm')
    a1 = m.create_model_area(1, 'a1')
    a2 = m.create_model_area(2, 'a2')
    a3 = m.create_model_area(3, 'a3')

    l12 = m.create_power_line(a1, a2, 1, "1_a1-a2")
    l23 = m.create_power_line(a2, a3, 2, "2_a2-a3")
    ts = add_custom_attrs(l12)
    verify_custom_attrs(l12, ts)
    assert len(m.power_lines) == 2
    assert m.power_lines[0].area_1 == a1
    assert l12.model.name == m.name


def test_can_add_power_modules():
    m = Model(1, 'm')
    a1 = m.create_model_area(1, 'a1')
    a2 = m.create_model_area(2, 'a2')
    a3 = m.create_model_area(3, 'a3')

    l12 = m.create_power_line(a1, a2, 1, "1_a1-a2")
    l23 = m.create_power_line(a2, a3, 2, "2_a2-a3")
    pm1 = a1.create_power_module(1, "solar")
    assert pm1.area.name == a1.name
    assert pm1.area.model.name == m.name
    ts = add_custom_attrs(pm1)
    verify_custom_attrs(pm1, ts)
    m_blob = m.to_blob()
    mx = Model.from_blob(m_blob)
    assert m == mx
    # now verify that internal weak-references are in place (so that .model .area works)
    assert mx.area[1].power_modules[1].area.name == a1.name
    assert mx.area[1].model.name == m.name
    assert mx.power_lines[0].model.name == m.name
    verify_custom_attrs(mx.area[1].power_modules[1], ts)


def test_int_string_dict():
    m = IntStringDict()
    assert len(m) == 0
    m[1] = 'a'
    m[2] = 'b'
    assert m[1] == 'a'
    assert m[2] == 'b'


def test_complex_model_serialization():
    a = create_model()
    blob_of_a = a.to_blob()
    b = Model.from_blob(blob_of_a)
    assert a == b


def test_structural_and_content_equality():
    a = create_model_with_hydro('m1')
    b = create_model_with_hydro('m1')
    # just to be sure, we start out equal
    assert a == b
    assert a.equal_content(b)
    assert a.equal_structure(b)
    # 1. modify content somewhere in 'a'
    a.area[1].power_modules[11].json = '{"a":"difference"}'
    assert not a.equal_content(b)
    assert a.equal_structure(b)
    # 2. modify something that is structural
    a.area[1].create_power_module(110, "pm changes structure", "json goes here")
    assert not a.equal_structure(b)
    assert a.area[2].equal_structure(b.area[2])  # we can compare details on structural equality
    assert not a.area[1].equal_structure(b.area[1])  # so here is the detailed diff
    assert a.area[2] == b.area[2]  # this includes attributes, and neighbor relations
