import math
from shyft.energy_market.core import (
    Point, PointList, XyPointCurve, XyPointCurveList,
    XyPointCurveWithZ, XyPointCurveWithZList, XyzPointCurve,
    TurbineOperatingZone, TurbineOperatingZoneList, TurbineDescription, TurbineCapability,
    has_forward_capability, has_backward_capability, has_reversible_capability,
)
import pytest


def verify_repr(a):
    """verify that __repr__ works, producing some string representation of the object"""
    b = repr(a)
    assert type(b) == str


def test_repr():
    verify_repr(Point(20.0, 96.0))
    verify_repr([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])
    verify_repr(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)]))
    verify_repr(XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])))
    verify_repr([XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])), XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0)]))])
    verify_repr(XyPointCurveList(
        [XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])), XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0)]))]))
    verify_repr(XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0))
    verify_repr([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
                 XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])
    verify_repr(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
                                       XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))
    verify_repr(TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
                                                            XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])))
    verify_repr([TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
                                                             XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])),
                                                                               72.0)])), TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))])
    verify_repr(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])), TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    verify_repr(TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]), 41, 92, 92, 40, 93), TurbineOperatingZone(
        XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
                               XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))])))
    verify_repr(XyzPointCurve(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
                                                     XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])))


def test_point():
    p1 = Point()
    assert p1.x == 0.0 and p1.y == 0.0
    p2 = Point(1.0, 2.0)
    assert p2.x == 1.0 and p2.y == 2.0
    assert p1 != p2
    p3 = Point(p2)
    assert p2 == p3
    p3.x = p3.x + 0.001
    assert p2 != p3


def test_point_list():
    p = PointList()
    assert len(p) == 0
    p = PointList([Point(0.0, 0.0), Point(1.0, 1.0)])
    assert len(p) == 2
    assert p[0] == Point()
    assert p[1] == Point(1.0, 1.0)
    p.append(Point(2.0, 2.0))
    assert len(p) == 3
    q = PointList(p)
    assert len(q) == 3
    assert q == p
    q.append(Point(4.0, 4.0))
    assert q != p


def test_xy_point_curve():
    a = XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)]))
    assert len(a.points) == 2
    assert a.calculate_y(5.0) == 5.0
    assert a.calculate_x(1.0) == 1.0
    assert a.is_mono_increasing()
    b = XyPointCurve(PointList([Point(0.0, 0.0), Point(1.0, 1.0), Point(2.0, 1.5)]))
    assert b.is_mono_increasing()
    assert not b.is_convex()
    c = XyPointCurve(PointList([Point(0.0, 0.0), Point(1.0, 1.0), Point(2.0, 5.0)]))
    assert c.is_mono_increasing()
    assert c.is_convex()


def test_xy_point_curve_with_z():
    a = XyPointCurveWithZ(XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)])), z=1.0)
    assert a.z == 1.0
    assert len(a.xy_point_curve.points) == 2
    b = XyPointCurveWithZ(a)
    assert a == b
    a.z = 2.0
    assert a != b


def test_xyz_point_curve():
    # a = XyPointCurveWithZ(XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)])), z=1.0)
    # b = XyPointCurveWithZ(XyPointCurve(PointList([Point(5.0, 0.0), Point(15.0, 40.0)])), z=5.0)
    a = XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)]))
    b = XyPointCurve(PointList([Point(5.0, 0.0), Point(15.0, 40.0)]))

    # test set_curve
    c = XyzPointCurve()
    c.set_curve(z=0.0, xy=a)
    c.set_curve(z=2.0, xy=a)
    assert c.get_curve(z=0.0) == a
    assert c.get_curve(z=2.0) == a

    # test that value is replaced
    c.set_curve(z=2.0, xy=b)
    assert c.get_curve(z=2.0) == b

    assert c.evaluate(x=10.0, z=1.0) == 15.0

    # check also the evaluation function for the curves directly
    assert c.curves.evaluate(x=10.0, z=1.0) == 15.0

    # check curves property
    assert c == XyzPointCurve(curves=c.curves)


def test_turbine_operating_zone():
    """turbine efficiency, for a set of net heads"""
    xyz = XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])), 100)
    b = TurbineOperatingZone([xyz], 10, 20, 20, 9, 21)
    assert b.production_min == pytest.approx(10.0)
    assert b.production_max == pytest.approx(20.0)
    assert b.production_nominal == pytest.approx(20.0)
    assert b.fcr_min == pytest.approx(9.0)
    assert b.fcr_max == pytest.approx(21.0)
    assert b.efficiency_curves[0] == xyz
    a = TurbineOperatingZone()
    assert len(a.efficiency_curves) == 0
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])), 100))
    assert a.production_min == pytest.approx(0.0)
    assert a.production_max == pytest.approx(0.0)
    assert a.production_nominal == pytest.approx(0.0)
    assert math.isnan(a.fcr_min)
    assert math.isnan(a.fcr_max)

    assert len(a.efficiency_curves) == 1
    assert abs(a.efficiency_curves[0].z - 100.0) < 1e-8
    # append one more net head (z)
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(11.0, 0.7), Point(12.0, 0.65)])), 110))
    assert len(a.efficiency_curves) == 2
    assert abs(a.efficiency_curves[0].z - 100.0) < 1e-8
    assert abs(a.efficiency_curves[1].z - 110.0) < 1e-8

    # check evaluation
    assert a.evaluate(x=12.0, z=110.0) == a.efficiency_curves.evaluate(x=12.0, z=110.0)


def test_turbine_description():
    """turbine description, with a list of turbine efficiency curves, one for each needle combination"""
    d = TurbineDescription()
    assert d
    assert len(d.operating_zones) == 0
    for z in range(0, 6, 1):
        a = TurbineOperatingZone()
        a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])), 100))
        a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6), Point(11.0, 0.7), Point(12.0, 0.65)])), 110))
        a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(11.0, 0.6), Point(13.0, 0.8), Point(14.0, 0.65)])), 120))
        a.production_min = z + 1
        a.production_max = z + 2
        d.operating_zones.append(a)
    assert len(d.operating_zones) == 6

    # check that get_operating_zone(p) returns the zone containing p
    assert d.get_operating_zone(p=0).production_min == 1  # p=0 should map to first zone
    assert d.get_operating_zone(p=1).production_min == 1
    assert d.get_operating_zone(p=2).production_min == 2
    assert d.get_operating_zone(p=6).production_min == 6
    assert d.get_operating_zone(p=7).production_min == 6  # p=7 should map to last zone

    assert has_forward_capability(d.capability())
    assert not has_reversible_capability(d.capability())


def test_backward_turbine_description1():
    d = TurbineDescription()

    a = TurbineOperatingZone()
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(-10.0, 0.6)])), -100))
    d.operating_zones.append(a)
    assert len(d.operating_zones) == 1
    assert not has_forward_capability(d.capability())
    assert has_backward_capability(d.capability())


def test_backward_turbine_description2():
    d = TurbineDescription()

    a = TurbineOperatingZone()
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(-10.0, 0.6)])), -100))
    a.efficiency_curves.append(XyPointCurveWithZ(XyPointCurve(PointList([Point(10.0, 0.6)])), 100))
    d.operating_zones.append(a)
    assert len(d.operating_zones) == 1
    assert has_forward_capability(d.capability())
    assert has_backward_capability(d.capability())
    assert has_reversible_capability(d.capability())
