from pathlib import Path
from shyft.time_series import no_utctime, time, Int64Vector, utctime_now, UtcPeriod
from shyft.energy_market.core import ModelInfo, ModelInfoVector, Client, Server
from .utility import create_model_with_hydro


def test_model_info():
    a = ModelInfo()
    assert a
    assert a.id == 0
    assert len(a.name) == 0
    assert len(a.json) == 0
    assert a.created == no_utctime
    t0 = time('2018-10-11T01:02:03Z')

    b = ModelInfo(id=1, name='xtra', created=t0, json='{abc:1}')
    assert a != b
    assert b.id == 1
    assert b.name == 'xtra'
    assert b.created == t0
    assert b.json == '{abc:1}'

    v = ModelInfoVector()
    v[:] = [a, b]
    assert len(v) == 2
    assert v[0] == a
    assert v[1] == b


def test_client_server(tmpdir):
    root_dir = (tmpdir/"t_cs2")
    s = Server(str(root_dir))
    port = s.start_server()
    c = Client(host_port=f'localhost:{port}', timeout_ms=1000)
    assert s
    assert c
    mids = Int64Vector()
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
    m = create_model_with_hydro(name='m1')
    m.id = 0
    mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
    mid = c.store_model(m=m, mi=mi)
    m.id = mid
    mr = c.read_model(mid=mid)
    mrs = c.read_models(Int64Vector([mid]))
    assert len(mrs) == 1
    assert mr == mrs[0]
    mi.name = 'Hello world'
    mi.id = mid
    c.update_model_info(mid=mid, mi=mi)
    assert mr
    assert mr == m
    c.close()  # just to illustrate we can disconnect, and reconnect automagigally
    mis = c.get_model_infos(mids)
    assert len(mis) == 1
    assert mis[0].name == mi.name

    mis = c.get_model_infos(mids, UtcPeriod(0, utctime_now() + 3600))
    assert len(mis) == 1
    mis = c.get_model_infos(mids, UtcPeriod(0, 1))
    assert len(mis) == 0
    c.remove_model(mid)
    mis = c.get_model_infos(mids)
    assert len(mis) == 0


def test_client_server_read_old_data():
    s = Server(str(Path(__file__).parent/"model_data"))
    port = s.start_server()
    c = Client(host_port=f'localhost:{port}', timeout_ms=1000)
    assert s
    assert c
    mid = 1
    mr = c.read_model(mid)
    m = create_model_with_hydro(name='m1')
    m.id = mid
    assert mr.id == 1
    assert mr.equal_structure(m)
