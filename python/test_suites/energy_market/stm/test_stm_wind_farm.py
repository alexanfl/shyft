import pytest

from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from functools import reduce
from shyft.energy_market.stm import StmSystem
from shyft.time_series import TimeSeries

def test_wind_farm_attributes():
    sys = StmSystem(1, "A", "{}")
    wf = sys.create_wind_farm(1, "Wind Park #1")
    # generics, from id_base
    assert hasattr(wf, "id")
    assert hasattr(wf, "name")
    assert hasattr(wf, "json")

    #specifics
    assert hasattr(wf, "production")

    assert hasattr(wf.production, "forecast")
    assert hasattr(wf.production, "realised")
    assert hasattr(wf.production, "result")


    assert str(wf)
    assert repr(wf)
    assert wf.tag == "/W1"


def test_wind_farm_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    wf = sys.create_wind_farm(1, "Wind Farm #1")
    assert wf.flattened_attributes()
    for (path, val) in wf.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), wf)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()

def test_wind_farm_equality():
    sys1 = StmSystem(1, "A", "{}")
    wt1 = sys1.create_wind_farm(1, "Wind Farm #1")
    sys2 = StmSystem(2, "B", "{}")
    wt2 = sys2.create_wind_farm(1, "Wind Farm #1")
    wt3 = sys2.create_wind_farm(99, "other Wind Farm #1")

    assert len(sys2.wind_farms) == 2
    assert wt1 == wt1
    assert wt1 == wt2
    assert wt1 != wt3
    wt1.production.forecast = TimeSeries('shyft://stm/windfarm_forecast')
    assert wt1 != wt2
    wt2.production.forecast = wt1.production.forecast
    assert wt1 == wt2
    
    

