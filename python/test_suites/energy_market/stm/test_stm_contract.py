import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from functools import reduce
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, Contract


def test_contract_attributes():
    a = StmSystem(1, "A", "{}")
    # ma = MarketArea(2, 'NO1', '{}', a)
    ca = Contract(3, 'Contract #1', '{}', a)
    # generics, from id_base
    assert hasattr(ca, "id")
    assert hasattr(ca, "name")
    assert hasattr(ca, "json")
    # specifics
    assert hasattr(ca, "quantity")
    assert hasattr(ca, "price")
    assert hasattr(ca, "fee")
    assert hasattr(ca, "revenue")
    assert hasattr(ca, "parent_id")
    assert hasattr(ca, "buyer")
    assert hasattr(ca, "seller")
    assert hasattr(ca, "active")
    assert hasattr(ca, "validation")
    assert ca.tag == "/c3"


def test_contract_attributes_flattened():
    a = StmSystem(1, "A", "{}")
    # ma = MarketArea(2, 'NO1', '{}', a)
    ca = Contract(3, 'Contract #1', '{}', a)
    assert ca.flattened_attributes()
    for (path, val) in ca.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), ca)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()


def test_contract_market_association():
    sys = StmSystem(1, "A", "{}")
    mkt = MarketArea(2, 'NO1', '{}', sys)
    ctr = Contract(3, 'Contract #1', '{}', sys)
    assert hasattr(ctr, 'ts')
    assert len(sys.market_areas) == 0
    assert len(sys.contracts) == 0
    assert len(mkt.contracts) == 0
    assert len(ctr.get_market_areas()) == 0
    sys.market_areas.append(mkt)
    sys.contracts.append(ctr)
    mkt.contracts.append(ctr)
    assert len(sys.contracts) == 1
    assert len(sys.market_areas) == 1
    assert len(mkt.contracts) == 1
    assert len(ctr.get_market_areas()) == 1
    del mkt.contracts[0]
    assert len(mkt.contracts) == 0
    assert len(ctr.get_market_areas()) == 0
    ctr.add_to_market_area(mkt)
    assert len(mkt.contracts) == 1
    assert len(ctr.get_market_areas()) == 1


def test_contract_power_plant_association():
    sys = StmSystem(1, "A", "{}")
    hps = HydroPowerSystem(2, ' test hps')
    plt = hps.create_power_plant(3, 'test plant')
    sys.hydro_power_systems.append(hps)
    ctr = Contract(5, 'Contract #1', '{}', sys)
    assert len(ctr.power_plants) == 0
    ctr.power_plants.append(plt)
    assert len(ctr.power_plants) == 1


def test_contract_relations():
    sys = StmSystem(1, "A", "{}")
    c1 = Contract(1, 'Contract #1', '{}', sys)
    c2 = Contract(2, 'Contract #2', '{}', sys)
    c3 = Contract(3, 'Contract #3', '{}', sys)
    c4 = Contract(4, 'Contract #4', '{}', sys)
    sys.contracts.extend([c1, c2, c3, c4])

    relation_type1 = 1
    relation_type2 = 2

    r1 = c1.add_relation(1, c2, relation_type1)
    assert r1.related == c2
    assert len(c1.relations) == 1

    # notice how we can find the reverse relation(s)
    related_to_c1 = c2.find_related_to_this()
    assert len(related_to_c1)
    assert related_to_c1[0].id == c1.id

    r2_1 = c2.add_relation(2, c3, relation_type1)
    r2_2 = c2.add_relation(3, c3, relation_type2)
    assert len(c2.relations) == 2
    assert c2.relations[0] == r2_1
    assert c2.relations[1] == r2_2
    assert r2_1.related == c3
    assert r2_2.related == c3
    c2.remove_relation(r2_1)
    assert c2.relations[0] == r2_2
    assert str(c2)
    assert repr(c2)


def test_contract_equal():
    sys = StmSystem(1, "A", "{}")
    a = Contract(3, 'Contract #1', '{}', sys)
    b = Contract(3, 'Contract #1', '{}', sys)
    c = Contract(4, 'hmm', '{}', sys)
    sys.contracts.extend([a, b, c])
    assert len(sys.contracts) == 3
    assert a == a
    assert a == b
    assert a != c
    a.name = 'Another name'
    assert a != b
