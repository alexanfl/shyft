import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.energy_market.stm import Contract, StmPatchOperation
from shyft.energy_market.stm import HydroPowerSystem, StmSystem
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE
from .models import create_complete_stm_system
from shyft.energy_market import stm
from time import sleep


def _create_patch(a: StmSystem):
    patch = StmSystem(1, 'patch', '')  # it starts with a system
    hps = HydroPowerSystem(a.hydro_power_systems[0].id, "hps_patch")  # we add a hps, where we would like to
    pp = hps.create_power_plant(a.hydro_power_systems[0].power_plants[0].id, "patch")  # add the plant ref. for our contract

    patch.hydro_power_systems.append(hps)  # add the hps with the empty plant (but notice, the id's are the one we want to relate)
    c4 = Contract(4, 'existing', '{}', patch)  # add ref to existing contract in our model, similar to plant, only id matters
    c5 = Contract(5, 'new-patched-added', '{}', patch)  # a new contract to be added
    c5.buyer = 'optimist'
    c5.seller = 'also_optimist'
    c5.json = '{"kind":"win-win"}'
    ta = TimeAxis(time('2023-06-08T23:06:00Z'), time(3600), 1)
    MW = 1e6
    EUR_MW = 1.0/MW
    c5.quantity.value = TimeSeries(ta, [250.0*MW], POINT_AVERAGE_VALUE)
    c5.price.value = TimeSeries(ta, [36.0*EUR_MW], POINT_AVERAGE_VALUE)
    c5.power_plants.append(pp)  # we want the contract to have relation to pp
    patch.contracts.extend([c4, c5])  # must add contracts prior to relating them!
    r1 = c5.add_relation(1, c4, 2)  # also let the patch relate to another existing contract
    return pp, r1, c4, c5, patch


def test_can_patch_contract():
    """
    Test and demo we can patch a stm system with a new contract
    that have
     * its own preset attributes
     * relations to existing power plant
     * relation of specific type to another existing contract

    """

    a = create_complete_stm_system()
    b = StmSystem.from_blob(a.to_blob())  # make a copy so we can compare
    assert a == b  # ensure we are equal to begin with.
    pp, r1, c4, c5, patch = _create_patch(a)  # create a patch, get some vars back so we can test..
    c5.id = 0  # set the id to 0 means let the system auto-assign a new contract-id,it should be 5
    a.patch(StmPatchOperation.ADD, patch)
    assert a != b  # should be at least different
    assert c5.id == 5  # as noted below, the patch is assigned with auto-id
    # A side effect on auto-id and a.patch(..), is that the patch is
    # updated with the new id.
    # now fine tune test to ensure we have the wanted behaviour
    a_c5 = a.contracts[-1]  # we just know its added at the end, so we pick it here to verify its content
    assert a_c5.id == c5.id  #  c5.id
    assert a_c5.relations[0].related.id == c4.id  # verify we got our relation
    assert a_c5.relations[0].relation_type == r1.relation_type
    assert a_c5.power_plants[0].id == pp.id
    assert a_c5.buyer == c5.buyer
    assert a_c5.quantity.value == c5.quantity.value
    assert a_c5.price.value == c5.price.value


def test_dstm_client_server_patch(port_no, web_api_port, simple_stm_system, tmpdir):
    """
    This test and demonstrates patching a dstm model with a new contract,
    Exactly equal to the above test.
    """
    assert port_no != web_api_port

    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    doc_root = (tmpdir/"c_dstm_client")
    doc_root.mkdir()
    host = "127.0.0.1"
    srv.start_server()
    srv.start_web_api(f"{host}", web_api_port, str(doc_root), 1, 1)
    assert srv.get_web_api_port() == web_api_port
    assert srv.get_web_api_ip() == "127.0.0.1"
    sleep(0.2)  # todo:.. start_web api is not immediately ready ,need a sleep.
    client = stm.DStmClient(f"{host}:{port_no}", 1000)
    try:
        b = create_complete_stm_system()
        a_name = "a"
        client.add_model(a_name, b)
        pp, r1, c4, c5, patch = _create_patch(b)  # create a patch, get some vars back so we can test..
        assert client.patch(a_name, StmPatchOperation.ADD, patch)
        a = client.get_model(a_name)  # notice! we can always retreive the entire model, as is, from dstm
        assert a != b
        # same check: now fine tune test to ensure we have the wanted behaviour
        a_c5 = a.contracts[-1]  # we just know its added at the end, so we pick it here to verify its content
        assert a_c5.id == c5.id
        assert a_c5.relations[0].related.id == c4.id  # verify we got our relation
        assert a_c5.relations[0].relation_type == r1.relation_type
        assert a_c5.power_plants[0].id == pp.id
        assert a_c5.buyer == c5.buyer
        assert a_c5.quantity.value == c5.quantity.value
        assert a_c5.price.value == c5.price.value
    finally:
        client.close()
        srv.stop_web_api()
        srv.close()
