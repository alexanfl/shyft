import io
import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)

from shyft.utilities.stub_generation import BoostClassInspect, BoostModuleInspect


def test_boost_module_inspect_stm():
    from shyft.energy_market.stm import _stm
    i = BoostModuleInspect(_stm)
    assert i.name
    assert i.doc
    assert i.functions
    assert i.classes
    assert i.enums
    assert i.variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s


def test_boost_class_inspect_Reservoir():
    from shyft.energy_market.stm._stm import Reservoir
    i = BoostClassInspect(Reservoir)
    assert i.name == 'Reservoir'
    assert len(i.doc) > 10
    assert i.base_class
    assert len(i.cls_props) == 0
    p = i.props
    assert i.props
    assert i.methods
    assert i.nested_classes
    assert not i.type_variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s


def test_boost_class_inspect_Waterway():
    from shyft.energy_market.stm._stm import Waterway
    i = BoostClassInspect(Waterway._Discharge)
    assert i.name == '_Discharge'
    assert len(i.cls_props) == 0
    p = i.props
    assert i.props
    # assert i.methods
    assert i.nested_classes
    assert not i.type_variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s


def test_boost_class_inspect_UnitGroup():
    from shyft.energy_market.stm._stm import UnitGroup
    i = BoostClassInspect(UnitGroup)
    assert len(i.doc) > 10
    assert i.base_class
    assert len(i.cls_props) >= 0
    assert i.props
    assert i.methods
    assert i.nested_classes
    assert not i.enums
    assert not i.type_variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s


def test_boost_class_inspect_Reservoir2():
    from shyft.energy_market.core._core import Reservoir
    i = BoostClassInspect(Reservoir)
    assert i.name == 'Reservoir'
    assert i.base_class.__name__ == 'HydroComponent'
    assert len(i.cls_props) == 0
    assert i.props
    assert i.methods
    assert not i.nested_classes
    assert not i.type_variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s


def test_boost_class_inspect_StmCase():
    from shyft.energy_market.stm._stm import StmCase
    i = BoostClassInspect(StmCase)
    assert i.name == 'StmCase'
    assert len(i.doc) > 10
    assert len(i.cls_props) == 0
    assert i.props
    assert i.methods
    assert not i.nested_classes
    assert not i.type_variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s
