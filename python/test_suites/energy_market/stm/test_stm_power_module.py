import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from functools import reduce
from shyft.energy_market.stm import StmSystem, PowerModule


def test_power_module_attributes():
    sys = StmSystem(1, "A", "{}")
    pm = sys.create_power_module(2, 'PowerModule #1', '{}')
    # generics, from id_base
    assert hasattr(pm, "id")
    assert hasattr(pm, "name")
    assert hasattr(pm, "json")
    # specifics
    assert hasattr(pm, "power")
    assert pm.tag == "/P2"


def test_power_module_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    pm = sys.create_power_module(2, 'PowerModule #1', '{}')
    assert pm.flattened_attributes()
    for (path, val) in pm.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), pm)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()


def test_power_module_equal():
    sys = StmSystem(1, "A", "{}")
    a = PowerModule(3, 'PowerModule #1', '{}', sys)
    b = PowerModule(3, 'PowerModule #1', '{}', sys)
    c = PowerModule(4, 'hmm', '{}', sys)
    sys.power_modules.extend([a, b, c])
    assert len(sys.power_modules) == 3
    assert a == a
    assert a == b
    assert a != c
