import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.time_series import time, deltahours, deltaminutes, TimeSeries, Calendar, TimeAxis, POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ, TurbineOperatingZone, TurbineDescription
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea
# from shyft.energy_market.stm import ShopCommand, ShopCommander, ShopSystem, ShopCommandList
# from shyft.energy_market.stm import t_double, t_xy, t_turbine_description
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list


# if '__file__' in locals():
#    current_path = dirname(realpath(__file__))  # Directory of this script
# else:
#    current_path = getcwd()  # Working directory when run interactively
# stm_source_path = rf"{current_path}\..\..\..\..\ec\stm"
# print(f"Extending PATH with Shop library directories according to STM source path {stm_source_path}")
# environ["PATH"] = rf"{stm_source_path}\shop\api\bin;{stm_source_path}\shop\api\bin\windows;{stm_source_path}\shop\api\lib\windows\debug;{environ['PATH']}"

# def test_stm_shop_command():
#    """verify shop command"""
#    s1 = ShopCommand.penalty_flag_all(True)
#    s2 = ShopCommand('penalty flag /all /on')
#    assert s1 == s2

def create_mock_hydro_power_system(*, hps_id: int = 1, name: str) -> HydroPowerSystem:
    """ helper to create a test system """
    hps = HydroPowerSystem(hps_id, name)
    t0 = time('2000-01-01T00:00:00Z')

    rsv = hps.create_reservoir(1, 'rsv')
    rsv.volume_level_mapping.value = create_t_xy(t0, XyPointCurve(PointList([Point(0.0, 80.0), Point(2.0, 90.0), Point(3.0, 95.0), Point(5.0, 100.0), Point(16.0, 105.0)])))
    rsv.level.regulation_max.value = create_t_double(t0, 100.0)
    rsv.level.regulation_min.value = create_t_double(t0, 80.0)
    rsv.water_value.endpoint_desc.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=36.5, point_fx=POINT_AVERAGE_VALUE)
    rsv.level.realised.value = TimeSeries(TimeAxis(time('2018-10-17T09:00:00Z'), time(3600), 241), fill_value=90.0, point_fx=POINT_AVERAGE_VALUE)
    rsv.inflow.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=55.0, point_fx=POINT_AVERAGE_VALUE)

    gen = hps.create_aggregate(1, 'agg')
    gen.generator_description.value = create_t_xy(t0, XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])))
    gen.turbine_description.value = create_t_turbine_description(t0,
                                                                 [XyPointCurveWithZ(
                                                                     XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),
                                                                     70.0)
                                                                 ])
    plant = hps.create_power_station(1, 'plant')
    plant.add_unit(gen)
    plant.outlet_level.value = create_t_double(t0, 10.0)

    wtr_flood = hps.create_river(1, 'wtr rsv_flood')
    wtr_flood.discharge.static_max.value = create_t_double(t0, 150.0)
    gt = wtr_flood.add_gate(1, 'wtr.gate')
    gt.flow_description.value = create_t_xyz_list(t0, XyPointCurve(PointList([Point(100.0, 0.0), Point(101.5, 25.0), Point(103.0, 80.0), Point(104.0, 150.0)])), 0.0)

    wtr_main = hps.create_tunnel(2, 'wtr rsv_main')
    wtr_main.head_loss_coeff.value = create_t_double(t0, 0.00030)

    wtr_penstock = hps.create_tunnel(3, 'wtr penstock')
    wtr_penstock.head_loss_coeff.value = create_t_double(t0, 0.00005)

    wtr_outlet = hps.create_tunnel(4, "wtr outlet")

    wtr_tailrace = hps.create_tunnel(5, "wtr tailrace")

    wtr_river = hps.create_river(6, "wtr outlet_river")

    wtr_flood.input_from(rsv, ConnectionRole.flood).output_to(wtr_river)
    wtr_main.input_from(rsv).output_to(wtr_penstock)
    wtr_penstock.output_to(gen)
    gen.output_to(wtr_outlet)
    wtr_outlet.output_to(wtr_tailrace)
    wtr_tailrace.output_to(wtr_river)
    return hps


def create_mock_stm_system(id: int, name: str) -> StmSystem:
    """ helper to create a complete system with market and stm hps"""
    a = StmSystem(id, name, '{}')
    a.hydro_power_systems.append(create_mock_hydro_power_system(hps_id=1, name='test hps'))
    no_1 = MarketArea(1, 'NO1', '{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=40.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=9999.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_sale.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=9999.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=5.0, point_fx=POINT_AVERAGE_VALUE)
    a.market_areas.append(no_1)
    return a


def test_create_hps_system():
    assert create_mock_hydro_power_system(hps_id=1, name='test')


def test_create_stm_system():
    assert create_mock_stm_system(1, 'test')

# def create_test_optimization_commands(run_id: int, write_files: bool) -> ShopCommandList:
#    r = ShopCommandList()
#    if write_files: r.append(
#        ShopCommand.log_file(f"shop_log_{run_id}.txt")
#    )
#    r.extend([
#        ShopCommand.set_method_primal(),
#        ShopCommand.set_code_full(),
#        ShopCommand.start_sim(3),
#        ShopCommand.set_code_incremental(),
#        ShopCommand.start_sim(3)
#    ])
#    if write_files: r.extend([
#        ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
#        ShopCommand.save_series(f"shop_series_{run_id}.txt"),
#        ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
#        ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
#    ])
#    return r


# def test_stm_system_optimize():
#    """verify shop optimization"""
#    stm_system = create_test_stm_system(id=1, name='test stm system')
#    commands = create_test_optimization_commands(run_id=1, write_files=False)

#   agg = stm_system.hydro_power_systems[0].aggregates[0]

# Optimize with fixed step arguments
#    utc = Calendar()
#    t_begin = time('2018-10-17T10:00:00Z')
#    t_mid = time('2018-10-17T20:00:00Z')
#    t_end = time('2018-10-18T10:00:00Z')
#    t_step = deltahours(1)
#    n_steps = (t_end - t_begin) / t_step
#    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps.seconds)
#    ShopSystem.optimize(stm_system, ta, commands)
#    assert agg.production.exists
#    assert agg.production.value.values[0] > 56 and agg.production.value.values[0] < 57

# Optimize with fixed step time axis
#    ShopSystem.optimize(stm_system, ta, commands)
#    assert agg.production.value.values[0] > 56 and agg.production.value.values[0] < 57

# Optimize with point time axis but still fixed steps
#    t_points = [t for t in range(t_begin.seconds, t_end.seconds, deltahours(1).seconds)]
# [utc.to_string(t) for t in t_points]
#    ta = TimeAxis(t_points, t_end)
#    ShopSystem.optimize(stm_system, ta, commands)
#    assert agg.production.value.values[0] > 56 and agg.production.value.values[0] < 57

# Optimize with point time axis with change in steps
# 15 minutes first period, then change to 1 hour resolution.
#    t_points = [t for t in range(t_begin.seconds, t_mid.seconds, deltaminutes(15).seconds)] \
#               + [t for t in range(t_mid.seconds, t_end.seconds, deltahours(1).seconds)]
# [utc.to_string(t) for t in t_points]
#    ta = TimeAxis(t_points, t_end)
#    ShopSystem.optimize(stm_system, ta, commands)
#    assert agg.production.value.values[0] > 56 and agg.production.value.values[0] < 57
