import pytest
import shyft.energy_market.stm as stm

if not stm.shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
else:
    import shyft.energy_market.stm.compute as compute


def test_compute_client_server():
    server = compute.Server()
    port = server.start_server()
    client = compute.Client(host_port=f'localhost:{port}', timeout_ms=1000)
    assert server
    assert client
    assert client.send(compute.GetStatusRequest()).state == compute.State.IDLE
    server.close()
    del server
