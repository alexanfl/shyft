import pytest
# this needs to go first,  *and* no other stm imports here
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.time_series import Int64Vector, utctime_now, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE, time, UtcPeriod, \
    StringVector, DtsServer, CacheStats, TsVector
from shyft.energy_market.core import ModelInfo
from shyft.energy_market.stm import compute, HpsClient, HpsServer, StmClient, StmServer, StmTaskServer, StmTaskClient
from shyft.energy_market.stm import StmSystem, MarketArea, StmCase, ModelRefList, StmModelRef, StmTask
from .models import create_test_hydro_power_system, create_test_hydro_power_system_for_regression_old_data_test
from shyft.energy_market import stm
from shyft.energy_market.stm import UrlResolveError
from shyft.energy_market.stm.utilities import create_t_turbine_description
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ
from time import sleep


def test_hps_client_server(tmpdir):
    root_dir = (tmpdir/"t_hps_cs")
    s = HpsServer(str(root_dir))
    port = s.start_server()
    c = HpsClient(host_port=f'localhost:{port}', timeout_ms=1000)
    assert s
    assert c
    mids = Int64Vector()
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
    m = create_test_hydro_power_system(hps_id=0, name='hps m1')
    m.id = 0
    mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
    mid = c.store_model(m=m, mi=mi)
    m.id = mid
    mr = c.read_model(mid=mid)
    mi.name = 'Hello world'
    mi.id = mid
    c.update_model_info(mid=mid, mi=mi)
    assert mr
    # equality not yet impl: assert mr == m
    c.close()  # just to illustrate we can disconnect, and reconnect automagigally
    mis = c.get_model_infos(mids)
    assert len(mis) == 1
    assert mis[0].name == mi.name
    c.remove_model(mid)
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
    c.close()
    del s


def test_throw_on_conflict_db_dir(tmpdir):
    """
    ref https://gitlab.com/shyft-os/shyft/-/issues/944
    lets ensure we get exception if we try to open
    several instances at once
    """

    root_dir = str(tmpdir/"a")
    s1 = StmServer(root_dir)
    assert s1
    with pytest.raises(RuntimeError):
        s2 = StmServer(root_dir)
    del s1


def create_stm_sys(stm_id: int, name: str, json: str) -> StmSystem:
    a = StmSystem(uid=stm_id, name=name, json=json)
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', '{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    return a


def create_stm_sys_for_regression_test(stm_id: int, name: str, json: str) -> StmSystem:
    a = StmSystem(uid=stm_id, name=name, json=json)
    a.hydro_power_systems.append(create_test_hydro_power_system_for_regression_old_data_test(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', '{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    return a


def test_stm_client_server(tmpdir):
    root_dir = (tmpdir/"t_stm_cs")
    s = StmServer(str(root_dir))
    port = s.start_server()
    c = StmClient(host_port=f'localhost:{port}', timeout_ms=1000)
    assert s
    assert c
    mids = Int64Vector()
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
    m = create_stm_sys(stm_id=0, name='stm m1', json="{}")
    m.id = 0
    mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
    mid = c.store_model(m=m, mi=mi)
    m.id = mid
    mr = c.read_model(mid=mid)
    mi.name = 'Hello world'
    mi.id = mid
    c.update_model_info(mid=mid, mi=mi)
    assert mr
    # equality not yet impl: assert mr == m
    c.close()  # just to illustrate we can disconnect, and reconnect automagigally
    mis = c.get_model_infos(mids)
    assert len(mis) == 1
    assert mis[0].name == mi.name
    c.remove_model(mid)
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
    c.close()
    del s  # ensure to close down precise


def test_stm_system_ts_result_urls(simple_stm_system):
    prefix = "dstm://Mx"
    ts_urls = simple_stm_system.result_ts_urls(prefix)
    assert len(ts_urls) > 10, "expect some at urls generated"
    for url in ts_urls:
        assert prefix in url


def test_dstm_server(simple_stm_system, tmpdir):
    """ just test the server side object, no client/io interaction """
    log_file = tmpdir/"dstm.log"
    config = stm.LogConfig(str(log_file))
    stm.configure_logger(config, stm.LALL)
    srv = stm.DStmServer()
    assert srv.shared_lock_timeout == time(0.200)
    doc_root = (tmpdir/"www_dstm")
    srv.do_add_model("simple", simple_stm_system)
    assert not srv.get_listening_port()
    assert not srv.get_listening_ip()
    assert len(srv.do_get_model_ids()) == 1
    # Add model:
    srv.do_add_model(simple_stm_system.name, simple_stm_system)
    assert len(srv.do_get_model_ids()) == 2
    # Get model infos:
    mifs = srv.do_get_model_infos()
    assert len(mifs) == 2
    mif = mifs["simple"]
    assert mif.id == 1
    assert mif.json == ""
    assert mif.name == "Test STM system"

    # Create model server side:
    assert srv.do_create_model("new model")
    assert len(srv.do_get_model_ids()) == 3

    result = srv.apply("new model", lambda model: 2)
    assert result == 2

    # Rename
    srv.do_rename_model("new model", "new renamed model")
    # Evaluate model
    period = UtcPeriod(time('2018-01-01T10:00:00Z'), time('2018-02-01T10:00:00Z'))
    assert not srv.do_evaluate_model("new renamed model", period)
    # And remove
    srv.do_remove_model("new renamed model")
    assert len(srv.do_get_model_ids()) == 2

    assert srv.get_web_api_port() == -1  # Web API is not running
    assert srv.get_web_api_ip() == ""  # Web API is not running
    del srv
    stats = log_file.stat()  # check that we log to the file specified
    assert stats.size > 0


def test_dstm_client(port_no, compute_port_no, web_api_port, simple_stm_system, tmpdir):
    assert port_no != web_api_port
    assert compute_port_no != web_api_port

    # server side fx_callback feature goes here:
    fx_events = []
    srv = stm.DStmServer()  # we need to create srv here, so we can use it in callback fx

    def my_server_side_fx(mid: str, fx_arg: str) -> bool:
        if "raise" in mid and "exception" in fx_arg:
            raise RuntimeError("Here it is")
        fx_events.append([mid, fx_arg])  # just to ensure we got the fx call here
        # TODO: add something that changes some attributes, collect urls and then:
        changed_urls = StringVector([f'dstm://M{mid}/path1', f'dstm://M{mid}/path1'])
        srv.notify_change(changed_urls)  # this will notify any subscribers about changes
        return True

    log_config = stm.LogConfig()
    stm.configure_logger(log_config, stm.LALL)
    srv = stm.DStmServer()
    mem_tgt: int = 10*1000*1000  #  MB is very small size
    ts_estimate: int = 10*1000  # 10 kB, approx 1000 points
    assert srv.cache_memory_target != mem_tgt  # we know it is different, so ok
    assert srv.cache_ts_initial_size_estimate != ts_estimate  # same here, the initial ones are different
    srv.cache_memory_target = mem_tgt  # usual purpose would be to control cache size
    srv.cache_ts_initial_size_estimate = ts_estimate
    assert srv.cache_memory_target == mem_tgt
    assert srv.cache_ts_initial_size_estimate == ts_estimate
    cs: CacheStats = srv.cache_stats  # demo/ensure we can get ut cache statistics
    assert cs.id_count == 0
    assert cs.hits == 0
    assert cs.misses == 0
    srv.clear_cache_stats()  # ensure we can invoke this function
    srv.flush_cache(["shyft://stm/silent_ignore_items_not_there"])  # Evict specific items from cache
    srv.flush_cache_all()  # will empty internal dtss cache

    srv.fx = my_server_side_fx  # hook up server side callback here.
    srv.set_listening_port(port_no)
    doc_root = (tmpdir/"t_dstm_client")
    doc_root.mkdir()
    srv.start_server()

    csrv = compute.Server()
    csrv.set_listening_port(compute_port_no)
    csrv.start_server()

    simple = "simple"
    srv.do_add_model(simple, simple_stm_system)
    assert srv.get_listening_port() == port_no
    host = "127.0.0.1"
    srv.start_web_api(f"{host}", web_api_port, str(doc_root), 1, 1)
    assert srv.get_web_api_port() == web_api_port
    assert srv.get_web_api_ip() == "127.0.0.1"
    sleep(0.2)  # todo:.. start_web api is not immediately ready ,need a sleep.
    client = stm.DStmClient(f"{host}:{port_no}", 1000)
    try:
        assert len(client.get_model_ids()) == 1
        # Add model:
        assert client.create_model("new model")
        assert len(client.get_model_ids()) == 2
        assert len(client.get_model_infos()) == 2
        assert client.get_model_infos()["new model"].id == 0
        assert len(client.get_model_ids()) == len(srv.do_get_model_ids())
        # Read model:
        sys2 = client.get_model("new model")
        assert isinstance(sys2, StmSystem)
        # Rename model:
        new_name = "m2"
        client.rename_model("new model", new_name)
        assert client.get_model(new_name)
        # Invoke server-side fx
        assert client.fx(new_name, "optimize_this")
        assert len(fx_events)
        assert fx_events[0][0] == new_name and fx_events[0][1] == "optimize_this"
        # invoke server-side fx, that raise exception
        with pytest.raises(RuntimeError) as r:  # verify that we do get an exception (minimum)
            client.fx("raise", "exception")

        # Evaluate model:
        period = UtcPeriod(time('2018-01-01T10:00:00Z'), time('2018-02-01T10:00:00Z'))
        assert not client.evaluate_model(new_name, period), "this model should only contain bound time series."
        assert client.reset_model(new_name), "this model should be able to be reset."

        # Get result time-series from the model
        result_urls = simple_stm_system.result_ts_urls(f"dstm://M{simple}")
        assert len(result_urls)
        rts = client.get_ts(simple, result_urls)
        assert len(rts)
        # Remove model:
        client.remove_model(new_name)
        assert len(client.get_model_ids()) == 1
        assert len(client.get_model_ids()) == len(srv.do_get_model_ids())
        # Version info:
        assert client.get_server_version() == srv.do_get_version_info()

        assert client.add_compute_server(f'localhost:{compute_port_no}')
        cinfo = client.compute_server_status()
        assert len(cinfo) == 1
        assert cinfo[0].state == compute.ManagedServerState.IDLE
        assert cinfo[0].address == f'localhost:{compute_port_no}'
    finally:
        client.close()
        srv.stop_web_api()
        srv.close()


def test_dstm_set_get_attrs(port_no, web_api_port, simple_stm_system, tmpdir):
    assert port_no != web_api_port

    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    simple = "simple"
    doc_root = (tmpdir/"t_dstm_client")
    doc_root.mkdir()

    some_custom_ts_url = f'dstm://M{simple}/H1/R1.ts.custom'
    some_custom_ts2 = TimeSeries(some_custom_ts_url) + 1
    r = simple_stm_system.hydro_power_systems[0].reservoirs[0]
    r.ts['custom.2'] = some_custom_ts2
    some_custom_ts_url2 = f'dstm://M{simple}{r.get_tsm_object("custom.2").url()}'

    srv.do_add_model(simple, simple_stm_system)
    host = "127.0.0.1"
    srv.start_server()
    srv.start_web_api(f"{host}", web_api_port, str(doc_root), 1, 1)
    assert srv.get_web_api_port() == web_api_port
    assert srv.get_web_api_ip() == "127.0.0.1"
    sleep(0.2)  # todo:.. start_web api is not immediately ready ,need a sleep.
    client = stm.DStmClient(f"{host}:{port_no}", 1000)

    try:
        assert len(client.get_model_ids()) == 1
        assert len(client.get_attrs([])) == 0
        result_urls = simple_stm_system.result_ts_urls(f"dstm://M{simple}")
        result_attrs = client.get_attrs(result_urls)
        assert len(result_urls) == len(result_attrs)

        assert not isinstance(client.get_attrs([some_custom_ts_url2])[0], UrlResolveError)

        # demo how to set time-series on the model, using remote attribute set
        t0 = time('2018-10-17T10:00:00Z')
        ta = TimeAxis(t0, time(3600), 240)
        some_ts = TimeSeries(ta, fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
        some_ts_url = f'dstm://M{simple}/H1/R1.level.constraint.max'

        some_custom_ts = TimeSeries(some_ts_url)*10 + TimeSeries(some_ts_url)
        set_result = client.set_attrs([(some_ts_url, some_ts), (some_custom_ts_url, some_custom_ts)])
        assert len(set_result) == 2
        assert set_result[0] is None
        assert set_result[1] is None
        assert client.get_model(simple).hydro_power_systems[0].reservoirs[0].level.constraint.max.exists
        assert client.get_model(simple).hydro_power_systems[0].reservoirs[0].get_tsm_object('custom').exists
        get_some_ts_result = client.get_attrs([some_ts_url, some_custom_ts_url, some_custom_ts_url2])
        assert len(get_some_ts_result) == 3
        assert get_some_ts_result[0] == some_ts

        tsv = client.evaluate_ts(TsVector([get_some_ts_result[1], some_custom_ts]), ta.total_period())
        assert tsv[0] == tsv[1]
        tsv2 = client.evaluate_ts(TsVector([get_some_ts_result[2], some_custom_ts2]), ta.total_period())
        assert tsv2[0] == tsv2[1]

        # test that we get error:
        tsv3 = client.evaluate_ts(TsVector([TimeSeries("dstm://MInvalid/U1.turbine_description")]), ta.total_period())
        assert hasattr(tsv3[0], 'what')

        # demo how to set turbine description on a unit
        u0_turb_descr_url = f'dstm://M{simple}/H1/U1.turbine_description'
        turbine_description = create_t_turbine_description(t0,
                                                           [XyPointCurveWithZ(
                                                               XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])),
                                                               400.0)
                                                           ])
        td_set_result = client.set_attrs([(u0_turb_descr_url, turbine_description)])
        assert len(td_set_result) == 1
        assert td_set_result[0] == None
        # demo how to read both t_xy curve and ts in one go:
        not_yet_created_url = f'dstm://M{simple}/H1/R1.ts.custom-not-there-yet'
        multi_result = client.get_attrs([u0_turb_descr_url, some_ts_url, not_yet_created_url])
        assert len(multi_result) == 3
        assert str(multi_result[0]) == str(turbine_description)  # equal operator not yet exposed, so use str
        assert multi_result[1] == some_ts
        assert hasattr(multi_result[2], "what")
        assert multi_result[2].what == "attribute ts.custom-not-there-yet not found, requested url: dstm://Msimple/H1/R1.ts.custom-not-there-yet"

        check_still_missing_result = client.get_attrs([not_yet_created_url])  # ensure that asking for ts.attribute, does not create one
        assert len(check_still_missing_result) == 1
        assert hasattr(check_still_missing_result[0],"what")
        assert check_still_missing_result[0].what == "attribute ts.custom-not-there-yet not found, requested url: dstm://Msimple/H1/R1.ts.custom-not-there-yet"

        #check that we get diagnostics when sending a malformed request on get_attrs
        malformed_url = f'dstm://M{simple}/H1/U1.turbine_descxxxription'
        error_result = client.get_attrs([malformed_url])
        assert len(error_result) == 1
        assert hasattr(error_result[0], "what")
        assert error_result[0].what == "attribute turbine_descxxxription not found, requested url: dstm://Msimple/H1/U1.turbine_descxxxription"

        #check that we get diagnostics when sending a malformed request on set_attrs
        td_set_err_result = client.set_attrs([(malformed_url, turbine_description)])
        assert len(td_set_err_result) == 1
        assert hasattr(td_set_err_result[0], "what")
        assert td_set_err_result[0].what == "attribute turbine_descxxxription not found, requested url: dstm://Msimple/H1/U1.turbine_descxxxription"


    finally:
        client.close()
        srv.stop_web_api()
        srv.close()


def test_case_server_client(tmpdir):
    root_dir = (tmpdir/"t_case_sc")
    root_dir.mkdir()
    s = StmTaskServer(str(root_dir))
    port = s.start_server()
    c = StmTaskClient(host_port=f"localhost:{port}", timeout_ms=1000)
    assert s
    assert c
    mids = Int64Vector()
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
    # Create a task:
    task = StmTask(0, "s1", utctime_now())
    case = StmCase(0, "r1", utctime_now())
    case.model_refs.append(StmModelRef("host", 123, 456, "key"))
    task.labels.append("test")
    task.labels.append("dstm")
    task.add_case(case)
    mi = ModelInfo(id=0, name="task s1", created=utctime_now())
    mid = c.store_model(task, mi)
    task.id = mid
    rsess = c.read_model(mid=mid)
    rsess_vec = c.read_models(Int64Vector([mid]))
    assert len(rsess_vec) == 1
    assert rsess_vec[0] == rsess
    # Update model info:
    mi.name = 'Hello world'
    mi.id = mid
    c.update_model_info(mid=mid, mi=mi)
    assert rsess == task
    # Add run:
    run2 = StmCase(2, "r2", utctime_now())
    c.add_case(task.id, run2)
    task2 = c.read_model(task.id)
    assert len(task2.cases) == 2

    # Get runs:
    ## By ID:
    assert c.get_case(task.id, case.id) == case
    assert c.get_case(task.id, -1) is None
    ## By name:
    assert c.get_case(task.id, run2.name)
    assert c.get_case(task.id, "norun") is None

    # Model reference stuff:
    c.add_model_ref(task.id, case.id, StmModelRef("host", 12, 34, "testkey"))
    assert c.get_model_ref(task.id, case.id, "testkey") == StmModelRef("host", 12, 34, "testkey")
    assert c.get_model_ref(task.id, case.id, "nonkey") is None
    with pytest.raises(RuntimeError):
        c.get_model_ref(-1, case.id, "testkey")
    assert len(c.get_case(task.id, case.id).model_refs) == 2
    assert c.remove_model_ref(task.id, case.id, "testkey")
    assert not c.remove_model_ref(task.id, case.id, "nonkey")
    assert len(c.get_case(task.id, case.id).model_refs) == 1
    # Remove runs:
    ## By id:
    assert not c.remove_case(task.id, 3)
    assert c.remove_case(task.id, 2)
    assert not c.remove_case(task.id, 2)
    task2 = c.read_model(task.id)
    assert len(task2.cases) == 1
    ## By name:
    assert not c.remove_case(task.id, "norun")
    assert c.remove_case(task.id, "r1")
    assert not c.remove_case(task.id, "r1")
    task2 = c.read_model(task.id)
    assert len(task2.cases) == 0

    c.close()
    mis = c.get_model_infos(mids)
    assert len(mis) == 1
    assert mis[0].name == mi.name
    c.remove_model(mid)
    mis = c.get_model_infos(mids)
    assert len(mis) == 0
