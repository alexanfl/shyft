from shyft.time_series import time, deltahours, TimeAxis

import pytest

shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def test_stm_shop_pq_curves(system_to_optimize):
    """verify pq curves creation by shop optimization """
    plant = system_to_optimize.hydro_power_systems[0].power_plants[0]
    agg = plant.units[0]

    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps =int((t_end - t_begin)/t_step)
    ta = TimeAxis(t_begin, t_step, n_steps)

    # Optimize with save_pq_curves command and verify that pq results are produced,
    # and take a closer look at agg.production_discharge_relation.final.
    # Can send print_pqcurves_all command to dump XML file in working directory,
    # for manual inspection. Note that this command only have affect for the single
    # next iteration, so to dump files for more iterations the command must be
    # repeated before each of them.
    shop_system = shop.ShopSystem(ta)
    shop_system.emit(system_to_optimize)
    shop_system.commander.set_method_primal()
    shop_system.commander.save_pq_curves(True)  # This is important, without it Shop does not fill the pq_curves attributes. Also it only have effect for following iterations, so adding it early.
    # First run one full iteration and check results.
    shop_system.commander.set_code_full()
    # shop_system.commander.print_pqcurves_all() # To dump XML file "pq_curves_full_mode_iter1.xml" in working directory, for manual inspection!
    shop_system.commander.start_sim(1)
    shop_system.collect(system_to_optimize)
    print(agg.production_discharge_relation.final.value)  # Debug output if test fails
    assert len(agg.production_discharge_relation.original.value) == n_steps
    assert len(agg.production_discharge_relation.convex.value) == n_steps
    assert len(agg.production_discharge_relation.final.value) == n_steps
    assert [i.key() for i in agg.production_discharge_relation.final.value] == [t.start for t in ta]
    first, *_, last = [i.data().points for i in agg.production_discharge_relation.final.value]
    assert pytest.approx([0.0, 80.0, 90.0, 100.0, 110.0]) == [i.x for i in first]
    assert pytest.approx([0.0, 80.0, 90.0, 100.0, 110.0]) == [i.x for i in last]
    # First run one full iteration and check results.
    shop_system.commander.set_code_incremental()
    # shop_system.commander.print_pqcurves_all() # To dump XML file "pq_curves_incr_mode_iter1.xml" in working directory, for manual inspection!
    shop_system.commander.start_sim(1)
    shop_system.collect(system_to_optimize)
    print(agg.production_discharge_relation.final.value)  # Debug output if test fails
    assert len(agg.production_discharge_relation.original.value) == n_steps
    assert len(agg.production_discharge_relation.convex.value) == n_steps
    assert len(agg.production_discharge_relation.final.value) == n_steps
    assert [i.key() for i in agg.production_discharge_relation.final.value] == [t.start for t in ta]
    first, *_, last = [i.data().points for i in agg.production_discharge_relation.final.value]
    assert pytest.approx([0.0, 33.342010, 64.447337, 80.0, 90.0, 100.0, 110.0]) == [i.x for i in first]
    assert pytest.approx([0.0, 36.935880, 55.0, 65.645293, 80.0, 90.0, 100.0, 110.0]) == [i.x for i in last]
