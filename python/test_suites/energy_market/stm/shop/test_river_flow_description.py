import numpy as np
import pytest
# this needs to go first !
shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-related test for non-shop build', allow_module_level=True)
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ, XyPointCurveWithZList
from shyft.energy_market.stm.utilities import create_t_xy
from shyft.energy_market import stm
from shyft.energy_market.stm import t_xyz_list
from shyft import time_series as sts




def create_t_double(t0: sts.time, v: float) -> sts.TimeSeries:
    return sts.TimeSeries(sts.TimeAxis([t0, sts.max_utctime]), v, sts.POINT_AVERAGE_VALUE)


def create_connected_reservoirs() -> stm.HydroPowerSystem:
    hps = stm.HydroPowerSystem(1, "two-reservoirs")
    t0 = sts.time("2000-01-01T00:00:00Z")

    rsv_up = hps.create_reservoir(1, "Upper reservoir")
    rsv_vol_head = stm.t_xy()
    rsv_vol_head[t0] = XyPointCurve(PointList([
        Point(0.0e6,    500.0),
        Point(30.0e6,   510.0),
        Point(65.0e6,   520.0),
        Point(105.0e6,  530.0),
        Point(155.0e6,  540.0),
        Point(210.0e6,  550.0),
        Point(250.0e6,  552.0)]))
    rsv_up.volume_level_mapping.value = rsv_vol_head
    rsv_up.volume.static_max.value = create_t_double(t0, 210.0e6)
    rsv_up.level.regulation_min.value = create_t_double(t0, 500.0)
    rsv_up.level.regulation_max.value = create_t_double(t0, 550.0)

    rsv_down = hps.create_reservoir(2, "Lower reservoir")
    rsv_down_vol_head = stm.t_xy()
    rsv_down_vol_head[t0] = XyPointCurve(PointList([
        Point(0.0e6,    475.0),
        Point(30.0e6,   485.0),
        Point(65.0e6,   495.0),
        Point(105.0e6,  505.0),
        Point(155.0e6,  515.0),
        Point(210.0e6,  525.0),
        Point(230.0e6,  527.0)]))
    rsv_down.volume_level_mapping.value = rsv_down_vol_head
    rsv_down.volume.static_max.value = create_t_double(t0, 210.0e6)
    rsv_down.level.regulation_min.value = create_t_double(t0, 475.0)
    rsv_down.level.regulation_max.value = create_t_double(t0, 525.0)

    spillway_1 = hps.create_waterway(1, "spillway 1")
    spillway_1.geometry.z0.value = create_t_double(t0, 550.)
    spillway_1.geometry.z1.value = create_t_double(t0, 475.)

    spillway_1.flow_description.upstream_ref.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(550.,   0.0),
        Point(550.25, 25.0),
        Point(550.50, 60.0),
        Point(551.,   150.0),
        Point(552.,   400.0),
    ])))

    rsv_up.output_to(spillway_1)
    spillway_1.output_to(rsv_down)

    return hps


def add_2nd_flood_way(hps: stm.HydroPowerSystem):
    t0 = sts.time("2000-01-01T00:00:00Z")

    spillway_2 = hps.create_waterway(2, "spillway 2")
    spillway_2.geometry.z0.value = create_t_double(t0, 550.25)
    spillway_2.geometry.z1.value = create_t_double(t0, 527.)
    spillway_2.flow_description.upstream_ref.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(550.25, 0.0),
        Point(550.50, 10.0),
        Point(551.00, 25.0),
        Point(552.00, 60.0),
    ])))

    rsv_up = hps.find_reservoir_by_name("Upper reservoir")
    rsv_up.output_to(spillway_2)
    # spillway_2.output_to()  # Lost water


def add_main_river(hps: stm.HydroPowerSystem):
    t0 = sts.time("2000-01-01T00:00:00Z")
    rsv_up, rsv_down = hps.reservoirs
    main = hps.create_waterway(3, "main")
    main.geometry.z0.value = create_t_double(t0, 500)
    main.geometry.z1.value = create_t_double(t0, 475.)
    rsv_up.output_to(main)
    main.output_to(rsv_down)


def add_dummy_market_area(stm_sys: stm.StmSystem):
    m = stm.MarketArea(1, "dummy-area", "", stm_sys)
    m.max_sale.value = create_t_double(sts.time(0), 9999e6)
    m.max_buy.value = create_t_double(sts.time(0), 0.)
    m.price.value = create_t_double(sts.time(0), 1e-6)
    stm_sys.market_areas.append(m)


def optimization_cmds() -> shop.ShopCommandList:
    return shop.ShopCommandList([
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])


def run_cmds(stm_sys: stm.StmSystem, run_ta: sts.TimeAxis, cmds: shop.ShopCommandList):
    shop_sys = shop.ShopSystem(run_ta)
    shop_sys.set_logging_to_stdstreams(True)
    shop_sys.set_logging_to_files(False)
    shop_sys.emit(stm_sys)
    for cmd in cmds:
        shop_sys.commander.execute(cmd)
    shop_sys.collect(stm_sys)
    shop_sys.complete(stm_sys)

    with open(f"./debug.yml", "w") as f:
        f.write(shop_sys.export_yaml_string(input_only=False, compress_txy=True, compress_connection=True))


def test_reservoir_basic_overflow():
    t0 = sts.time("2023-09-09T00:00:00Z")
    dt = 15*sts.Calendar.MINUTE
    n_steps = 4*24*7
    ta_real = sts.TimeAxis(t0-dt, dt, 2)
    ta = sts.TimeAxis(t0, dt, n_steps)

    s = stm.StmSystem(1, "two reservoirs", "")
    hps = create_connected_reservoirs()
    s.hydro_power_systems.append(hps)
    add_dummy_market_area(s)  # Needed for optimization in SHOP
    rsv_up, rsv_down = hps.reservoirs
    spill1 = hps.find_waterway_by_name("spillway 1")

    rsv_up.level.realised.value = sts.TimeSeries(ta_real, 549.9, sts.POINT_INSTANT_VALUE)
    rsv_up.inflow.schedule.value = sts.TimeSeries(ta, 60, sts.POINT_AVERAGE_VALUE)
    rsv_down.level.realised.value = sts.TimeSeries(ta_real, 475., sts.POINT_INSTANT_VALUE)
    spill1.discharge.penalty.cost.rate.value = sts.TimeSeries(ta, 1, sts.POINT_AVERAGE_VALUE)

    run_cmds(s, ta, optimization_cmds())

    # At 60 m3/s inflow the reservoir level should converge (strictly increasing) to 550.5 according to the
    # flow description:
    level_result = rsv_up.level.result.value.values.to_numpy()
    assert np.isclose(max(level_result), 550.5, atol=0.01)
    assert np.all(np.diff(level_result) > 0)


def test_reservoir_multiple_overflow():
    t0 = sts.time("2023-09-09T00:00:00Z")
    dt = 60*sts.Calendar.MINUTE
    n_steps = 24*14
    ta_real = sts.TimeAxis(t0-dt, dt, 2)
    ta = sts.TimeAxis(t0, dt, n_steps)

    s = stm.StmSystem(1, "two reservoir", "")
    hps = create_connected_reservoirs()
    s.hydro_power_systems.append(hps)
    rsv_up, rsv_down = hps.reservoirs
    add_dummy_market_area(s)  # Needed for optimization in SHOP

    add_2nd_flood_way(hps)

    spill1 = hps.find_waterway_by_name("spillway 1")
    spill2 = hps.find_waterway_by_name("spillway 2")

    rsv_up.level.realised.value = sts.TimeSeries(ta_real, 549., sts.POINT_INSTANT_VALUE)
    rsv_up.inflow.schedule.value = sts.TimeSeries(ta, 70., sts.POINT_AVERAGE_VALUE)
    rsv_down.level.realised.value = sts.TimeSeries(ta_real, 475., sts.POINT_INSTANT_VALUE)
    spill1.discharge.penalty.cost.rate.value = sts.TimeSeries(ta, 1., sts.POINT_AVERAGE_VALUE)
    spill2.discharge.penalty.cost.rate.value = sts.TimeSeries(ta, 1., sts.POINT_AVERAGE_VALUE)

    run_cmds(s, ta, optimization_cmds())

    level_result = rsv_up.level.result.value.values.to_numpy()
    assert np.isclose(max(level_result), 550.5, atol=0.001)
    assert np.all(np.diff(level_result) > 0)
    spill1_result = spill1.discharge.result.value.values.to_numpy()
    assert np.isclose(max(spill1_result), 60., atol=0.01)
    assert np.all(np.diff(spill1_result) >= 0)
    spill2_result = spill2.discharge.result.value.values.to_numpy()
    assert np.isclose(max(spill2_result), 10., atol=0.01)
    assert np.all(np.diff(spill2_result) >= 0)


def test_reservoir_overflow_simulation():
    t0 = sts.time("2023-09-09T00:00:00Z")
    dt = 15*sts.Calendar.MINUTE
    n_steps = 24*14
    ta_real = sts.TimeAxis(t0-dt, dt, 2)
    ta = sts.TimeAxis(t0, dt, n_steps)

    hps = create_connected_reservoirs()
    rsv_up, rsv_down = hps.reservoirs
    s = stm.StmSystem(1, "two reservoir", "")
    s.hydro_power_systems.append(rsv_up.hps)

    add_2nd_flood_way(hps)

    spill1 = hps.find_waterway_by_name("spillway 1")
    spill2 = hps.find_waterway_by_name("spillway 2")

    rsv_up.level.realised.value = sts.TimeSeries(ta_real, 549., sts.POINT_INSTANT_VALUE)
    rsv_up.inflow.schedule.value = sts.TimeSeries(ta, 70, sts.POINT_AVERAGE_VALUE)

    rsv_down.level.realised.value = sts.TimeSeries(ta_real, 475., sts.POINT_INSTANT_VALUE)

    sim_cmd = shop.ShopCommand.start_shopsim()
    sim_cmd.objects = [str(dt.seconds)]
    run_cmds(s, ta, shop.ShopCommandList([sim_cmd]))

    assert spill1.discharge.result.exists
    assert spill2.discharge.result.exists
    spill1_result = spill1.discharge.result.value.values.to_numpy()
    spill2_result = spill2.discharge.result.value.values.to_numpy()
    assert len(spill1_result)
    assert len(spill2_result)


def test_river_delta_meter():
    t0 = sts.time("2023-09-09T00:00:00Z")
    dt = 15*sts.Calendar.MINUTE
    n_steps = 4*24*7
    ta_real = sts.TimeAxis(t0-dt, dt, 2)
    ta = sts.TimeAxis(t0, dt, n_steps)

    s = stm.StmSystem(1, "two reservoirs", "")
    hps = create_connected_reservoirs()
    s.hydro_power_systems.append(hps)
    add_dummy_market_area(s)  # Needed for optimization in SHOP
    add_main_river(hps)
    rsv_up, rsv_down = hps.reservoirs
    rsv_up.level.realised.value = sts.TimeSeries(ta_real, 512, sts.POINT_INSTANT_VALUE)
    rsv_down.level.realised.value = sts.TimeSeries(ta_real, 510., sts.POINT_INSTANT_VALUE)

    main = hps.find_waterway_by_name("main")
    dm_descr = t_xyz_list()
    dm_descr[t0] = XyPointCurveWithZList([
        XyPointCurveWithZ(
            XyPointCurve(PointList([
                Point(-2, -120),
                Point(-1., -50),
                Point(0., 0),
                Point(1., 50),
                Point(2, 120)])),
            z=512.),
        XyPointCurveWithZ(
            XyPointCurve(PointList([
                Point(-2, -90),
                Point(-1., -40),
                Point(0., 0),
                Point(1., 40),
                Point(2, 90)])),
            z=510.)
    ])
    for dm_attr in [main.flow_description.delta_meter.upstream_ref, main.flow_description.delta_meter.downstream_ref]:
        dm_attr.value = dm_descr
        run_cmds(s, ta, optimization_cmds())

        upstream_level_result = rsv_up.level.result.value.values.to_numpy()
        downstream_level_result = rsv_down.level.result.value.values.to_numpy()
        delta_level_result = upstream_level_result - downstream_level_result
        river_discharge_result = main.discharge.result.value.values.to_numpy()

        assert np.isclose(delta_level_result[-1], 0., atol=0.001)
        assert np.all(np.diff(river_discharge_result) < 0)
