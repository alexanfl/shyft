from shyft.time_series import time, deltahours, deltaminutes, Calendar, TimeAxis, TimeSeries, POINT_AVERAGE_VALUE as stair_case
import pytest

shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def test_stm_shop_command():
    """verify shop command"""
    s1 = shop.ShopCommand.penalty_flag_all(True)
    s2 = shop.ShopCommand('penalty flag /all /on')
    assert s1 == s2


def create_test_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    r = shop.ShopCommandList()
    if write_files: r.append(
        shop.ShopCommand.log_file(f"shop_log_{run_id}.txt")
    )
    r.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])
    if write_files: r.extend([
        shop.ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
        shop.ShopCommand.save_series(f"shop_series_{run_id}.txt"),
        shop.ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
        shop.ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
    ])
    return r


def test_stm_system_optimize(system_to_optimize):
    """verify shop optimization"""
    mega = 1000000
    stm_system = system_to_optimize
    commands = create_test_optimization_commands(1, False)

    agg = stm_system.hydro_power_systems[0].units[0]

    # Optimize with fixed step arguments
    utc = Calendar()
    t_begin = time('2018-10-17T10:00:00Z')
    t_mid = time('2018-10-17T20:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = int((t_end - t_begin)/t_step)
    ta = TimeAxis(t_begin, t_step, n_steps)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert agg.production.result.exists
    assert 19*mega < agg.production.result.value.values[0] < 21*mega
    # verify .export utility functions
    ss: shop.ShopSystem = shop.ShopSystem(ta)
    ss.emit(stm_system)
    assert ss.export_topology_string()
    assert ss.export_data_string()
    # Optimize with fixed step time axis
    # demo as requested from Ole Andreas : let schedule be partial ts, with nans
    nan = float('nan')
    agg.production.schedule = TimeSeries(ta.slice(3, 15), fill_value=nan, point_fx=stair_case)
    agg.production.schedule.value.set(1, 51*mega)  # illustrate of to set specific schedule directly
    agg.production.schedule.value.set(2, 52*mega)  # on specific time-steps
    agg.production.schedule.value.set(5, 55*mega)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert 19*mega < agg.production.result.value.values[0] < 21*mega

    # Optimize with point time axis but still fixed steps
    t_points = [time(t) for t in range(int(t_begin), int(t_end), int(deltahours(1)))]
    # [utc.to_string(t) for t in t_points]
    ta = TimeAxis(t_points, t_end)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert 19*mega < agg.production.result.value.values[0] < 21*mega

    # Optimize with point time axis with change in steps
    # 15 minutes first period, then change to 1 hour resolution.
    t_points = [time(t) for t in range(t_begin.seconds, t_mid.seconds, deltaminutes(15).seconds)] \
               + [time(t) for t in range(t_mid.seconds, t_end.seconds, deltahours(1).seconds)]
    # [utc.to_string(t) for t in t_points]
    ta = TimeAxis(t_points, t_end)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert 19*mega < agg.production.result.value.values[4] < 21*mega
