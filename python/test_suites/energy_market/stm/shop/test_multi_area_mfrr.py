import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

from shyft.energy_market.core import XyPointCurve, PointList, Point, XyPointCurveWithZ, XyPointCurveWithZList
from shyft.energy_market.core import ConnectionRole
from shyft.energy_market import stm
from shyft.energy_market.stm.utilities import create_t_turbine_description, create_t_xy, create_t_xyz_list
import shyft.time_series as sts
from shyft.energy_market.stm import shop
import numpy as np

def create_t_double(t0: sts.time, v: float) -> sts.TimeSeries:
    return sts.TimeSeries(sts.TimeAxis([t0, sts.max_utctime]), v, sts.POINT_AVERAGE_VALUE)


def create_simple_hps(hps_id: int = 1, name: str = "Hydro_power_system") -> stm.HydroPowerSystem:
    M = 1e6
    hps = stm.HydroPowerSystem(hps_id, name)
    t0 = sts.time("2000-01-01T00:00:00Z")

    # ---------- Reservoir ----------
    rsv = hps.create_reservoir(1, "Reservoir_1")

    # Lowest regulated water level
    lrl = create_t_double(t0, 200.0)
    rsv.level.regulation_min.value = lrl
    # Highest regulated water level
    hrl = create_t_double(t0, 250.0)
    rsv.level.regulation_max.value = hrl

    # Volume vs. head:
    rsv_vol_head = stm.t_xy()
    rsv_vol_head[t0] = XyPointCurve(PointList([
        Point(0.0*M, 200.0),
        Point(30.0*M, 210.0),
        Point(65.0*M, 220.0),
        Point(105.0*M, 230.0),
        Point(155.0*M, 240.0),
        Point(210.0*M, 250.0),
        Point(230.0*M, 252.0)]))
    rsv.volume_level_mapping.value = rsv_vol_head
    rsv.volume.static_max.value = create_t_double(t0, 210.0*M)

    # --------- Spill ---------
    spill = hps.create_river(1, "Flood river")
    spill_gt = spill.add_gate(1, "Spill")

    spill_desc_xyz = XyPointCurveWithZ()
    spill_desc_xyz.xy_point_curve = XyPointCurve(PointList([
        Point(250.0, 0.0),
        Point(251.0, 100.0),
        Point(252.0, 225.0)]))
    spill_desc_xyz.z = 0.0  # Not in use for spill description
    spill_desc = stm.t_xyz_list()
    spill_desc[t0] = XyPointCurveWithZList([spill_desc_xyz])
    spill_gt.flow_description.value = spill_desc

    # ------------- Units -------------
    unit1 = hps.create_unit(1, 'Unit_1')

    unit1.turbine_description.value = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.0, 90.0),
            Point(20.0, 92.25),
            Point(25.0, 93.75),
            Point(30.0, 95.0)
        ])), 225.00),

        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.0, 90.50),
            Point(20.0, 92.25),
            Point(25.0, 93.50),
            Point(30.0, 94.60)
        ])), 240.00),
    ])

    unit1.production.static_max.value = create_t_double(t0, 70.0*M)
    unit1.production.static_min.value = create_t_double(t0, 35.0*M)

    unit1.cost.start.value = sts.TimeSeries(
        sts.TimeAxis(sts.time('2010-01-01T00:00:00Z'), sts.deltahours(8760), 25),
        fill_value=200.0, point_fx=sts.POINT_AVERAGE_VALUE)
    unit1.cost.stop.value = sts.TimeSeries(
        sts.TimeAxis(sts.time('2010-01-01T00:00:00Z'), sts.deltahours(8760), 25),
        fill_value=200.0, point_fx=sts.POINT_AVERAGE_VALUE)

    gen_eff = stm.t_xy()
    gen_eff[t0] = XyPointCurve(PointList([
        Point(30.0*M, 98.10),
        Point(40.0*M, 98.30),
        Point(50.0*M, 98.47),
        Point(60.0*M, 98.60)
    ]))

    unit1.generator_description.value = gen_eff

    # ----------- Power plants -----------
    pp = hps.create_power_plant(1, 'Plant_1')
    pp.add_unit(unit1)

    pp.outlet_level.value = create_t_double(t0, 5.5)  # Outlet level might to be moved to waterway

    # ------------- Waterways -------------
    wr1 = hps.create_waterway(2, 'w_Reservoir1_to_Unit1')
    pen1 = hps.create_waterway(3, 'Penstock 1 Unit1')
    tr1 = hps.create_waterway(4, 'Tail race for Unit1')

    wr1.input_from(rsv).output_to(pen1)
    pen1.input_from(wr1).output_to(unit1)
    # tr1.input_from(unit1)

    wr1.head_loss_coeff.value = create_t_double(t0, 0.0001)
    pen1.head_loss_coeff.value = create_t_double(t0, 0.00075)

    wr1.add_gate(2, 'w_Reservoir1_to_Unit1_gate', '{}')

    hps.create_river(5, 'f_Reservoir1_Sea') \
        .input_from(rsv, ConnectionRole.flood)

    hps.create_river(6, 'b_Reservoir1_Sea') \
        .input_from(rsv, ConnectionRole.bypass)

    return hps


def create_dual_plant_hps(hps_id: int = 2, name: str = "Hydro_power_system") -> stm.HydroPowerSystem:
    # Expands on simple HPS
    hps = create_simple_hps(hps_id, name)
    M = 1e6
    t0 = sts.time("2000-01-01T00:00:00Z")

    # Reservoir
    rsv = hps.create_reservoir(2, "Reservoir_2")
    rsv.level.regulation_min.value = create_t_double(t0, 300.0)
    rsv.level.regulation_max.value = create_t_double(t0, 310.0)

    rsv.volume_level_mapping.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(0.0 * M, 300.0),
        Point(300.0 * M, 310.0),
        Point(370.0 * M, 312.0)])))
    rsv.volume.static_max.value = create_t_double(t0, 300.0 * M)

    # Spill
    spill = hps.create_river(10, "Flood river 2")
    spill_gt = spill.add_gate(10, "Spill2")

    spill_gt.flow_description.value = create_t_xyz_list(t0, XyPointCurve(PointList([
        Point(310.0, 0.0),
        Point(311.0, 100.0),
        Point(312.0, 225.0)])), 0.0)

    # Unit
    unit2 = hps.create_unit(2, 'Unit_2')

    unit2.turbine_description.value = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(65.00, 90.0),
            Point(90.00, 92.25),
            Point(112.50, 93.75),
            Point(135.00, 95.0)
        ])), 50.0),

    ])

    unit2.production.static_max.value = create_t_double(t0, 60.0*M)
    unit2.production.static_min.value = create_t_double(t0, 30.0*M)

    unit2.cost.start.value = create_t_double(t0, 100)
    unit2.cost.stop.value = create_t_double(t0, 150)

    unit2.generator_description.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(30.0 * M, 98.10),
        Point(40.0 * M, 98.30),
        Point(50.0 * M, 98.47),
        Point(60.0 * M, 98.60)
    ])))

    pp = hps.create_power_plant(2, 'Plant_2')
    pp.add_unit(unit2)

    pp.outlet_level.value = create_t_double(t0, 225.0)

    wr = hps.create_waterway(12, 'w_Reservoir2_to_Unit2')
    pen = hps.create_waterway(13, 'Penstock 2 Unit2')
    tr = hps.create_waterway(14, 'Tail race for Unit2')

    wr.input_from(rsv).output_to(pen)
    pen.output_to(unit2)
    tr.input_from(unit2).output_to(hps.find_reservoir_by_name("Reservoir_1"))

    wr.head_loss_coeff.value = create_t_double(t0, 0.0001)
    pen.head_loss_coeff.value = create_t_double(t0, 0.00075)

    hps.create_river(15, 'f_Reservoir2_Reservoir1') \
        .input_from(rsv, ConnectionRole.flood).output_to(hps.find_reservoir_by_name("Reservoir_1"))

    hps.create_river(16, 'b_Reservoir2_Reservoir1') \
        .input_from(rsv, ConnectionRole.bypass).output_to(hps.find_reservoir_by_name("Reservoir_1"))

    return hps


def test_simple_multi_market_area():
    """ ref https://gitlab.com/shyft-os/shyft/-/issues/979 """
    stm_sys = stm.StmSystem(1, "sys", json="")
    hps = create_dual_plant_hps()

    stm_sys.hydro_power_systems.append(hps)

    ma1 = stm.MarketArea(1, "Market-area 1", "", stm_sys)
    stm_sys.market_areas.append(ma1)

    cal = sts.Calendar("Europe/Oslo")
    ta = sts.TimeAxis(cal.time(2022, 2, 22, 0), cal.HOUR, 24)
    t0 = ta(0).start
    t_end = ta(len(ta)-1).end

    ma1_price = np.array([98.28, 97.82, 98.10, 99.96, 104.06, 110.04, 117.68, 139.47, 200.07, 168.31, 129.38, 124.89, 121.04, 117.56, 118.54, 120.90, 124.21, 127.70, 127.59, 121.57, 115.65, 109.32, 104.71, 98.94])/1e6
    ma1.price = sts.TimeSeries(ta, ma1_price, sts.POINT_AVERAGE_VALUE)
    ma1.load = sts.TimeSeries(sts.TimeAxis([t0, ta.time(4), ta.time(10), t_end]), np.array([0, 60, 0])*1e6, sts.POINT_AVERAGE_VALUE)
    ma1.max_sale = create_t_double(t0, 0)  # 9999e6)
    ma1.max_buy = create_t_double(t0, 0)

    ma3 = stm.MarketArea(3, "Market-area 3", "", stm_sys)
    stm_sys.market_areas.append(ma3)

    ma2 = stm.MarketArea(2, "Market-area 2", "", stm_sys)
    stm_sys.market_areas.append(ma2)
    ma2_prices = np.array([104.61, 104.10, 104.27, 105.72, 108.71, 113.58, 117.86, 127.95, 199.40, 150.29, 127.97, 125.12, 121.25, 117.66, 118.87, 121.40, 124.95, 126.07, 126.12, 122.25, 117.66, 110.56, 108.42, 104.94])/1e6
    ma2.price = sts.TimeSeries(ta, ma2_prices, sts.POINT_AVERAGE_VALUE)
    ma2.load = sts.TimeSeries(sts.TimeAxis([t0, ta.time(6), ta.time(12), ta.time(17), ta.time(21), t_end]), np.array([0, 50, 0, 100, 0])*1e6, sts.POINT_AVERAGE_VALUE)
    ma2.max_sale = create_t_double(t0, 0)  # 9999e6)
    ma2.max_buy = create_t_double(t0, 0)

    ta_active1 = sts.TimeAxis([t0, ta.time(16), t_end])
    ts_active1 = sts.TimeSeries(ta_active1, [1, 0], sts.POINT_AVERAGE_VALUE)
    ts_active1_2 = sts.TimeSeries(ta_active1, [0, 1], sts.POINT_AVERAGE_VALUE)
    ts_active = sts.TimeSeries(ta, 1, sts.POINT_AVERAGE_VALUE)

    u1 = hps.find_unit_by_name("Unit_1")
    u2 = hps.find_unit_by_name("Unit_2")

    ug1 = stm_sys.add_unit_group(1, "market-area1-participants", "", stm.UnitGroupType.PRODUCTION)
    ug1.add_unit(u1, ts_active1)
    # ug1.add_unit(u, sts.TimeSeries())

    ug2 = stm_sys.add_unit_group(2, "market-area2-participants", "", stm.UnitGroupType.PRODUCTION)
    ug2.add_unit(u1, ts_active1_2)
    ug2.add_unit(u2, ts_active)

    ug3 = stm_sys.add_unit_group(3, "market-area3-participants", "", stm.UnitGroupType.MFRR_UP)
    ug3.add_unit(u1, ts_active)
    ug3.add_unit(u2, ts_active)

    ma1.unit_group = ug1
    ma2.unit_group = ug2
    #ma3.unit_group = ug3

    r1 = hps.find_reservoir_by_name("Reservoir_1")
    r2 = hps.find_reservoir_by_name("Reservoir_2")

    r1.level.realised = create_t_double(t0, 225.0)
    r1.water_value.endpoint_desc = create_t_double(t0, 100.0e-6)
    r2.level.realised = create_t_double(t0, 305.0)
    r2.water_value.endpoint_desc = create_t_double(t0, 120.0e-6)

    cmds = shop.ShopCommandList([
        # shop.ShopCommand.penalty_cost_load(500),
        shop.ShopCommand.set_max_num_threads(8),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.set_dyn_seg_on(),
        shop.ShopCommand.set_nseg_all(5),
        shop.ShopCommand.start_sim(3)
    ])

    shop.ShopSystem.optimize(stm_sys, ta, cmds, True, False)

    load_unbalance_ma1 = ma1.load.value - ma1.unit_group.delivery.result.value
    assert sum(load_unbalance_ma1.inside(-1e4, 1e4, inside_v=0, outside_v=1).values) == 0

    load_unbalance_ma2 = ma2.load.value - ma2.unit_group.delivery.result.value
    assert sum(load_unbalance_ma2.inside(-1e5, 1e5, inside_v=0, outside_v=1).values) == 0
