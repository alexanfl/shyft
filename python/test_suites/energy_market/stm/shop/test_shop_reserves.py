import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ, XyPointCurveWithZList
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list
from shyft.energy_market import stm
from shyft.energy_market.stm import shop, UnitGroup, UnitGroupType
from shyft import time_series as sts

import numpy as np


def create_t_double(t0: sts.time, v: float) -> sts.TimeSeries:
    return sts.TimeSeries(sts.TimeAxis([t0, sts.max_utctime]), v, sts.POINT_AVERAGE_VALUE)


def create_simple_hps(hps_id: int = 1, name: str = "Hydro_power_system") -> stm.HydroPowerSystem:
    M = 1e6
    hps = stm.HydroPowerSystem(hps_id, name)
    t0 = sts.time("2000-01-01T00:00:00Z")

    # ---------- Reservoir ----------
    rsv = hps.create_reservoir(1, "Reservoir_1")

    # Lowest regulated water level
    lrl = create_t_double(t0, 200.0)
    rsv.level.regulation_min.value = lrl
    # Highest regulated water level
    hrl = create_t_double(t0, 250.0)
    rsv.level.regulation_max.value = hrl

    # Volume vs. head:
    rsv_vol_head = stm.t_xy()
    rsv_vol_head[t0] = XyPointCurve(PointList([
        Point(0.0*M, 200.0),
        Point(30.0*M, 210.0),
        Point(65.0*M, 220.0),
        Point(105.0*M, 230.0),
        Point(155.0*M, 240.0),
        Point(210.0*M, 250.0),
        Point(230.0*M, 252.0)]))
    rsv.volume_level_mapping.value = rsv_vol_head
    rsv.volume.static_max.value = create_t_double(t0, 210.0*M)

    # --------- Spill ---------
    spill = hps.create_river(1, "Flood river")
    spill_gt = spill.add_gate(1, "Spill")

    spill_desc_xyz = XyPointCurveWithZ()
    spill_desc_xyz.xy_point_curve = XyPointCurve(PointList([
        Point(250.0, 0.0),
        Point(251.0, 100.0),
        Point(252.0, 225.0)]))
    spill_desc_xyz.z = 0.0  # Not in use for spill description
    spill_desc = stm.t_xyz_list()
    spill_desc[t0] = XyPointCurveWithZList([spill_desc_xyz])
    spill_gt.flow_description.value = spill_desc

    # ------------- Units -------------
    unit1 = hps.create_unit(1, 'Unit_1')

    unit1.turbine_description.value = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.0, 90.0),
            Point(20.0, 92.25),
            Point(25.0, 93.75),
            Point(30.0, 95.0)
        ])), 225.00),

        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.0, 90.50),
            Point(20.0, 92.25),
            Point(25.0, 93.50),
            Point(30.0, 94.60)
        ])), 240.00),
    ])

    unit1.production.static_max.value = create_t_double(t0, 70.0*M)
    unit1.production.static_min.value = create_t_double(t0, 35.0*M)

    unit1.cost.start.value = sts.TimeSeries(
        sts.TimeAxis(sts.time('2010-01-01T00:00:00Z'), sts.deltahours(8760), 25),
        fill_value=200.0, point_fx=sts.POINT_AVERAGE_VALUE)
    unit1.cost.stop.value = sts.TimeSeries(
        sts.TimeAxis(sts.time('2010-01-01T00:00:00Z'), sts.deltahours(8760), 25),
        fill_value=200.0, point_fx=sts.POINT_AVERAGE_VALUE)

    gen_eff = stm.t_xy()
    gen_eff[t0] = XyPointCurve(PointList([
        Point(30.0*M, 98.10),
        Point(40.0*M, 98.30),
        Point(50.0*M, 98.47),
        Point(60.0*M, 98.60)
    ]))

    unit1.generator_description.value = gen_eff

    # ----------- Power plants -----------
    pp = hps.create_power_plant(1, 'Plant_1')
    pp.add_unit(unit1)

    pp.outlet_level.value = create_t_double(t0, 5.5)  # Outlet level might to be moved to waterway

    # ------------- Waterways -------------
    wr1 = hps.create_waterway(2, 'w_Reservoir1_to_Unit1')
    pen1 = hps.create_waterway(3, 'Penstock 1 Unit1')
    tr1 = hps.create_waterway(4, 'Tail race for Unit1')

    wr1.input_from(rsv).output_to(pen1)
    pen1.input_from(wr1).output_to(unit1)
    # tr1.input_from(unit1)

    wr1.head_loss_coeff.value = create_t_double(t0, 0.0001)
    pen1.head_loss_coeff.value = create_t_double(t0, 0.00075)

    wr1.add_gate(2, 'w_Reservoir1_to_Unit1_gate', '{}')

    hps.create_river(5, 'f_Reservoir1_Sea').input_from(rsv, ConnectionRole.flood)

    hps.create_river(6, 'b_Reservoir1_Sea').input_from(rsv, ConnectionRole.bypass)

    return hps


def create_dual_plant_hps(hps_id: int = 2, name: str = "Hydro_power_system") -> stm.HydroPowerSystem:
    # Expands on simple HPS
    hps = create_simple_hps(hps_id, name)
    M = 1e6
    t0 = sts.time("2000-01-01T00:00:00Z")

    # Reservoir
    rsv = hps.create_reservoir(2, "Reservoir_2")
    rsv.level.regulation_min.value = create_t_double(t0, 300.0)
    rsv.level.regulation_max.value = create_t_double(t0, 310.0)

    rsv.volume_level_mapping.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(0.0 * M, 300.0),
        Point(300.0 * M, 310.0),
        Point(370.0 * M, 312.0)])))
    rsv.volume.static_max.value = create_t_double(t0, 300.0 * M)

    # Spill
    spill = hps.create_river(10, "Flood river 2")
    spill_gt = spill.add_gate(10, "Spill2")

    spill_gt.flow_description.value = create_t_xyz_list(t0, XyPointCurve(PointList([
        Point(310.0, 0.0),
        Point(311.0, 100.0),
        Point(312.0, 225.0)])), 0.0)

    # Unit
    unit2 = hps.create_unit(2, 'Unit_2')

    unit2.turbine_description.value = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(65.00, 90.0),
            Point(90.00, 92.25),
            Point(112.50, 93.75),
            Point(135.00, 95.0)
        ])), 50.0),

    ])

    unit2.production.static_max.value = create_t_double(t0, 60.0*M)
    unit2.production.static_min.value = create_t_double(t0, 40.0*M)
    unit2.reserve.fcr_static_min.value = create_t_double(t0, 38.0*M)

    unit2.cost.start.value = create_t_double(t0, 100)
    unit2.cost.stop.value = create_t_double(t0, 150)

    unit2.generator_description.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(30.0 * M, 98.10),
        Point(40.0 * M, 98.30),
        Point(50.0 * M, 98.47),
        Point(60.0 * M, 98.60)
    ])))

    pp = hps.create_power_plant(2, 'Plant_2')
    pp.add_unit(unit2)

    pp.outlet_level.value = create_t_double(t0, 225.0)

    wr = hps.create_waterway(12, 'w_Reservoir2_to_Unit2')
    pen = hps.create_waterway(13, 'Penstock 2 Unit2')
    tr = hps.create_waterway(14, 'Tail race for Unit2')

    wr.input_from(rsv).output_to(pen)
    pen.output_to(unit2)
    tr.input_from(unit2).output_to(hps.find_reservoir_by_name("Reservoir_1"))

    wr.head_loss_coeff.value = create_t_double(t0, 0.0001)
    pen.head_loss_coeff.value = create_t_double(t0, 0.00075)

    hps.create_river(15, 'f_Reservoir2_Reservoir1') \
        .input_from(rsv, ConnectionRole.flood).output_to(hps.find_reservoir_by_name("Reservoir_1"))

    hps.create_river(16, 'b_Reservoir2_Reservoir1') \
        .input_from(rsv, ConnectionRole.bypass).output_to(hps.find_reservoir_by_name("Reservoir_1"))

    return hps


def add_reserve_droop_steps(u1: stm.Unit, u2: stm.Unit, t0: sts.time):
    u1.production.nominal.value = create_t_double(t0, 75.0e6)
    u1.reserve.droop.fcr_n.cost.value = create_t_double(t0, 100)
    fcr_n_droop_steps = create_t_xy(t0, XyPointCurve(PointList([
        Point(0, 0.0),
        Point(1, 3.0),
        Point(2, 5.0),
        Point(3, 7.0)])))
    u1.reserve.droop.fcr_n.steps.value = fcr_n_droop_steps
    u1.reserve.droop.fcr_d_up.steps.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(0, 0.0),
        Point(1, 4.0),
        Point(2, 5.0)])))

    u2.production.nominal.value = create_t_double(t0, 60.0e6)
    fcr_n_droop_steps_u2 = create_t_xy(t0, XyPointCurve(PointList([
        Point(0, 0.0),
        Point(1, 1.75),
        Point(2, 3.0),
        Point(3, 8.0)])))
    u2.reserve.droop.fcr_n.steps.value = fcr_n_droop_steps_u2
    u2.reserve.droop.fcr_d_down.steps.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(0, 0.0),
        Point(1, 2.5),
        Point(2, 4.0),
        Point(3, 5.0)])))


def add_reserve_unit_groups(sys: stm.StmSystem, run_ta: sts.TimeAxis):
    def create_group(gid: int, name: str, gtype: UnitGroupType, obl: sts.TimeSeries) -> UnitGroup:
        g = sys.add_unit_group(gid, name, "{}")
        g.group_type = gtype
        g.obligation.schedule = obl
        return g

    hps = sys.hydro_power_systems[0]
    u1 = hps.find_unit_by_id(1)
    u2 = hps.find_unit_by_id(2)

    n = len(run_ta)
    fcr_n_group = create_group(1, "fcr-n", stm.UnitGroupType.FCR_N_UP,
                               obl=sts.TimeSeries(run_ta, np.round(np.linspace(6e6, 9e6, n), -6), point_fx=sts.POINT_AVERAGE_VALUE))
    fcr_n_group.add_unit(u1, sts.TimeSeries())
    fcr_n_group.add_unit(u2, sts.TimeSeries())

    fcr_n_down_group = create_group(4, "fcr-n-down", stm.UnitGroupType.FCR_N_DOWN,
                                    obl=sts.TimeSeries(run_ta, np.round(np.linspace(6e6, 9e6, n), -6), point_fx=sts.POINT_AVERAGE_VALUE))
    fcr_n_down_group.add_unit(u1, sts.TimeSeries())
    fcr_n_down_group.add_unit(u2, sts.TimeSeries())

    fcr_d_up_group = create_group(2, "fcr-d up", stm.UnitGroupType.FCR_D_UP,
                                  obl=sts.TimeSeries(run_ta, np.round(np.linspace(0.5e6, 0.5e6, n), -5), point_fx=sts.POINT_AVERAGE_VALUE))
    fcr_d_up_group.add_unit(u1, sts.TimeSeries())

    fcr_d_down_group = create_group(3, "fcr-d down", stm.UnitGroupType.FCR_D_DOWN,
                                    obl=sts.TimeSeries(run_ta, np.round(np.linspace(1e6, 1e6, n), -6), point_fx=sts.POINT_AVERAGE_VALUE))
    fcr_d_down_group.add_unit(u2, sts.TimeSeries())


def add_energy_market_area(stm_sys: stm.StmSystem, run_ta: sts.TimeAxis):
    t0 = run_ta(0).start
    ma1 = stm.MarketArea(1, "Market-area 1", "", stm_sys)
    stm_sys.market_areas.append(ma1)
    prices = [98.28, 97.82, 98.10, 99.96, 104.06, 110.04, 117.68, 139.47, 200.07, 168.31, 129.38, 124.89, 121.04, 117.56, 118.54, 120.90, 124.21, 127.70, 127.59, 121.57, 115.65, 109.32, 104.71, 98.94]
    prices = prices + prices[::-1]
    prices = prices[:len(run_ta)]
    ma1_price = np.array(prices)/1e6
    ma1.price = sts.TimeSeries(run_ta, ma1_price, sts.POINT_AVERAGE_VALUE)
    ma1.load.value = create_t_double(t0, 0)
    ma1.max_sale = create_t_double(t0, 9999e6)
    ma1.max_buy = create_t_double(t0, 0)


def add_reserve_market_areas(stm_sys: stm.StmSystem, run_ta: sts.TimeAxis):
    t0 = run_ta(0).start
    fcr_n_ma = stm.MarketArea(2, "FCR-N market area", "", stm_sys)
    fcr_n_ma.unit_group = next(ug for ug in stm_sys.unit_groups if ug.group_type == UnitGroupType.FCR_N_UP)
    stm_sys.market_areas.append(fcr_n_ma)
    fcr_n_ma.price = sts.TimeSeries(run_ta, 10e-6, sts.POINT_AVERAGE_VALUE)

    fcr_d_up_ma = stm.MarketArea(2, "FCR-D Up market area", "", stm_sys)
    fcr_d_up_ma.unit_group = next(ug for ug in stm_sys.unit_groups if ug.group_type == UnitGroupType.FCR_D_UP)
    stm_sys.market_areas.append(fcr_d_up_ma)
    fcr_d_up_ma.price = sts.TimeSeries(run_ta, 8e-6, sts.POINT_AVERAGE_VALUE)

    fcr_d_down_ma = stm.MarketArea(3, "FCR-D Down market area", "", stm_sys)
    fcr_d_down_ma.unit_group = next(ug for ug in stm_sys.unit_groups if ug.group_type == UnitGroupType.FCR_D_DOWN)
    stm_sys.market_areas.append(fcr_d_down_ma)
    fcr_d_down_ma.price = sts.TimeSeries(run_ta, 9e-6, sts.POINT_AVERAGE_VALUE)

    for ma in [fcr_n_ma, fcr_d_up_ma, fcr_d_down_ma]:
        ma.max_sale = create_t_double(t0, 0.0e6)
        ma.max_buy = create_t_double(t0, 0.0)


def decorate_system(s: stm.StmSystem, run_ta: sts.TimeAxis):
    hps = s.hydro_power_systems[0]
    t0 = run_ta.time(0)
    u1 = hps.units[0]
    u2 = hps.units[1]

    add_energy_market_area(s, run_ta)
    add_reserve_droop_steps(u1, u2, t0)
    add_reserve_unit_groups(s, run_ta)

    r1 = hps.find_reservoir_by_name("Reservoir_1")
    r2 = hps.find_reservoir_by_name("Reservoir_2")
    r2.level.realised = create_t_double(t0, 305.0)
    r2.water_value.endpoint_desc = create_t_double(t0, 140.0e-6)
    r1.level.realised = create_t_double(t0, 225.0)
    r1.water_value.endpoint_desc = create_t_double(t0, 130.0e-6)


def run_shop(stm_sys: stm.StmSystem, run_ta: sts.TimeAxis):
    shop_cmd = shop.ShopCommandList([
        shop.ShopCommand.set_fcr_n_equality(True),
        shop.ShopCommand.set_max_num_threads(8),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.set_dyn_seg_on(),
        shop.ShopCommand.set_nseg_all(5),
        shop.ShopCommand(f"set droop_discretization_limit /all"),
        shop.ShopCommand.start_sim(3)
    ])

    shop_sys = shop.ShopSystem(run_ta)
    shop_sys.set_logging_to_stdstreams(False)
    shop_sys.set_logging_to_files(False)
    shop_sys.emit(stm_sys)
    for cmd in shop_cmd:
        shop_sys.commander.execute(cmd)
    shop_sys.collect(stm_sys)
    shop_sys.complete(stm_sys)


def is_close(a: sts.TimeSeries, b: sts.TimeSeries, atol=1e5) -> int:
    # Returns the number of values where the difference between a and b is greater than atol, in other words, returns
    # 0 if difference is less than atol.
    return sum((a - b).inside(-atol, atol, inside_v=0, outside_v=1).values)


def test_fcr_n_equality():
    cal = sts.Calendar("Europe/Oslo")
    run_ta = sts.TimeAxis(cal.time(2022, 2, 22, 0), cal.HOUR, 24)

    stm_sys = stm.StmSystem(1, "sys", json="")
    hps = create_dual_plant_hps()
    stm_sys.hydro_power_systems.append(hps)
    decorate_system(stm_sys, run_ta)

    run_shop(stm_sys, run_ta)

    for u in hps.units:
        if u.reserve.fcr_static_min.exists:
            prod_at_max_fcr_down = (u.production.result.value - u.reserve.fcr_n.down.result.value - u.reserve.fcr_d.down.result.value)
            assert sum((prod_at_max_fcr_down - u.reserve.fcr_static_min.value).inside(0, float('inf'), inside_v=0, outside_v=1).values) == 0
        assert is_close(u.reserve.fcr_n.up.result.value, u.reserve.fcr_n.down.result.value) == 0


def test_droop_steps():
    cal = sts.Calendar("Europe/Oslo")
    run_ta = sts.TimeAxis(cal.time(2022, 2, 22, 0), cal.HOUR, 24)
    t0 = run_ta(0).start

    stm_sys = stm.StmSystem(1, "sys", json="")
    hps = create_dual_plant_hps()
    stm_sys.hydro_power_systems.append(hps)
    decorate_system(stm_sys, run_ta)

    run_shop(stm_sys, run_ta)

    assert stm_sys.summary.sum_penalties.value <= 1.0

    for ug in stm_sys.unit_groups:
        assert is_close(ug.obligation.schedule.value, ug.delivery.result.value, 1e4) == 0

    for u in hps.units:
        fcr_droops = [u.reserve.droop.fcr_n, u.reserve.droop.fcr_d_up, u.reserve.droop.fcr_d_down]
        for droop in fcr_droops:
            if not droop.steps.exists:
                continue
            input_droop_steps = set((p.y for p in droop.steps.value[t0].points))
            result_droop_steps = set(droop.result.value.values)
            assert result_droop_steps
            assert result_droop_steps.issubset(input_droop_steps)


def test_reserve_schedule_penalty():
    cal = sts.Calendar("Europe/Oslo")
    run_ta = sts.TimeAxis(cal.time(2022, 2, 22, 0), cal.HOUR, 24)

    stm_sys = stm.StmSystem(1, "sys", json="")
    hps = create_dual_plant_hps()
    stm_sys.hydro_power_systems.append(hps)
    decorate_system(stm_sys, run_ta)

    u1, u2 = hps.units
    u1.reserve.mfrr.up.schedule = sts.TimeSeries(run_ta, 100e6, sts.POINT_AVERAGE_VALUE)
    u2.reserve.afrr.up.schedule = sts.TimeSeries(run_ta, 100e6, sts.POINT_AVERAGE_VALUE)

    run_shop(stm_sys, run_ta)

    assert u1.reserve.mfrr.up.penalty.exists
    assert u2.reserve.afrr.up.penalty.exists


def test_reserve_markets():
    cal = sts.Calendar("Europe/Oslo")
    n = 48
    run_ta = sts.TimeAxis(cal.time(2022, 2, 22, 0), cal.HOUR, n)
    oblig_mask_ta = sts.TimeAxis([run_ta.time(0), run_ta.time(n//2), run_ta.time(n-1)])
    oblig_mask_ts = sts.TimeSeries(oblig_mask_ta, [0, 1], sts.POINT_AVERAGE_VALUE)
    t0 = run_ta(0).start

    stm_sys = stm.StmSystem(1, "sys", json="")
    hps = create_dual_plant_hps()
    stm_sys.hydro_power_systems.append(hps)
    decorate_system(stm_sys, run_ta)
    add_reserve_market_areas(stm_sys, run_ta)

    # Limit obligation period to half the run_ta
    energy_market_area = next(m for m in stm_sys.market_areas if m.name == "Market-area 1")
    fcr_n_market = next(m for m in stm_sys.market_areas if m.name == "FCR-N market area")
    fcr_n_market.unit_group.obligation.schedule.value *= oblig_mask_ts

    stm_sys.unit_groups[1].obligation.schedule.value *= oblig_mask_ts

    fcr_d_up_market = next(m for m in stm_sys.market_areas if m.name == "FCR-D Up market area")
    fcr_d_up_market.unit_group.obligation.schedule.value *= oblig_mask_ts

    fcr_d_down_market = next(m for m in stm_sys.market_areas if m.name == "FCR-D Down market area")
    fcr_d_down_market.unit_group.obligation.schedule.value *= oblig_mask_ts

    run_shop(stm_sys, run_ta)
    assert stm_sys.summary.sum_penalties.value <= 1.0
    objective_pre_ma_access = stm_sys.summary.total.value

    # Add market access
    fcr_n_market.max_sale = sts.TimeSeries(oblig_mask_ta, [0, 5e6], sts.POINT_AVERAGE_VALUE)
    fcr_d_up_market.max_sale = sts.TimeSeries(oblig_mask_ta, [0, 10e6], sts.POINT_AVERAGE_VALUE)
    fcr_d_down_market.max_sale = sts.TimeSeries(oblig_mask_ta, [0, 15e6], sts.POINT_AVERAGE_VALUE)

    run_shop(stm_sys, run_ta)
    assert stm_sys.summary.sum_penalties.value <= 1.0
    assert abs(objective_pre_ma_access) < abs(stm_sys.summary.total.value)

    for rma in [fcr_n_market, fcr_d_up_market, fcr_d_down_market]:
        ug = rma.unit_group
        additional_sale = ug.delivery.result.value - ug.obligation.schedule.value
        assert sum((rma.max_sale.value - additional_sale).inside(0, float('inf'), inside_v=0, outside_v=1).values) == 0

    # All units part of energy market since it has no unit group
    sum_all_unit_prod = sts.TsVector([u.production.result.value for u in hps.units]).sum()
    additional_sale = sum_all_unit_prod - energy_market_area.load.value
    assert is_close(energy_market_area.sale.value.abs(), additional_sale) == 0
