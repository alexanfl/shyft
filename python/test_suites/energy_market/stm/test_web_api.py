import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from tempfile import TemporaryDirectory
from typing import Dict, Optional
from time import sleep

websockets = pytest.importorskip("websockets")  # Required for tests in this module

import asyncio
from shyft.energy_market import stm
from shyft.energy_market.stm._stm import _ts
from shyft.time_series import TimeAxis, TimeSeries, POINT_AVERAGE_VALUE
def uri(port_num):
    return f"ws://127.0.0.1:{port_num}"


def get_response(req: str, port_no: int, auth: str = ""):
    async def wrap(ws_req: str, x_hdr: Optional[Dict[str, str]]):
        async with websockets.connect(uri(port_no), extra_headers=x_hdr) as websocket:
            await websocket.send(ws_req)
            response = await websocket.recv()
            return response

    extra_header = {"Authorization": auth} if auth else None
    return asyncio.get_event_loop().run_until_complete(wrap(req, extra_header))

async def get_response_within(socket: any, timeout: int):
    try:
        async with asyncio.timeout(timeout):
            return await socket.recv()
    except:
        assert False , "timeout while waiting for socket to recieve "
def dstm_url(mdl_key: str, attr: _ts, attr_template_levels=-1) -> str:
    return attr.url(f"dstm://M{mdl_key}", template_levels=attr_template_levels)

def test_web_api(simple_stm_system, port_no, web_api_port):
    assert port_no != web_api_port
    import time
    #  ensure there is event loop for this thread
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        srv.do_add_model("simple", simple_stm_system)
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)
        time.sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        qa_tuples = [("""get_model_infos {"request_id": "1"}""",
                      """{"request_id":"1","result":[{"model_key":"simple","id":1,"name":"Test STM system"}]}"""),
                     ("""get_hydro_components {
                                    "request_id": "2",
                                    "model_key": "simple",
                                    "hps_id": 1
                                  }""",
                      """{"request_id":"2","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"id":1,"name":"simple_res"}],"units":[{"id":1,"name":"simple_unit"}],"power_plants":[{"id":2,"name":"simple_pp","units":[1]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]}]}}"""),
                     ("""read_model {"request_id": "1", "model_key": "simple"}""",
                      """{"request_id":"1","result":{"model_key":"simple","id":1,"name":"Test STM system","json":"","hps":[{"id":1,"name":"simple hps"}],"unit_groups":[],"markets":[],"contracts":[],"contract_portfolios":[],"networks":[],"power_modules":[],"wind_farms":[]}}""")]
        try:
            for qa in qa_tuples:
                response = get_response(qa[0], web_api_port)
                assert response == qa[1], 'expected result'
        finally:
            srv.stop_web_api()
            srv.close()
    del srv

def test_web_api_subscription(simple_stm_system, port_no, web_api_port):
    assert port_no != web_api_port
    import json

    def web_read_sub(rid: str, attr: str):
        read_req_json = {"request_id": rid,
                        "model_key": mdl_key,
                        "time_axis": {"t0": read_ta.fixed_dt.start.seconds, "dt": read_ta.fixed_dt.delta_t.seconds,
                                    "n": read_ta.fixed_dt.n},
                        "subscribe": True,
                        "hps": [
                            {"hps_id": 1,
                            "power_plants": [
                                {
                                    "component_id": 2,
                                    "attribute_ids": [attr]
                                }
                            ],
                            },
                        ]}
        return f"read_attributes {json.dumps(read_req_json, ensure_ascii=False)}"
    def web_write_req(rid: str, attr: str, v: float):
        change_json = {
            "request_id": rid,
            "model_key": mdl_key,
            "cache": True,
            "hps": [
                        {"hps_id": 1,
                        "power_plants": [
                            {
                                "component_id": 2,
                                "attribute_data": [{
                                    "attribute_id":attr,
                                    "value": {
                                        "id":"someId",
                                        "pfx":True,
                                        "time_axis":{
                                            "t0":0.0,
                                            "dt":3600.0,
                                            "n":10},
                                        "values":[v,v,v,v,v,v,v,v,v,v]

                                    }
                                }]
                            }
                        ],
                        },
                    ],
            "market": [],
            "contracts": [],
            "contract_portfolios": [],
            "unit_groups": []
        }
        return f"set_attributes {json.dumps(change_json, ensure_ascii=False)}"
    def web_read_rep(rid: str, attr: str, v: float):
        return f"{{\"request_id\":\"{rid}\",\"result\":{{\"model_key\":\"simple\",\"hps\":[{{\"hps_id\":1,\"power_plants\":[{{\"component_data\":[{{\"attribute_id\":\"{attr}\",\"data\":{{\"id\":\"\",\"pfx\":true,\"time_axis\":{{\"t0\":0.0,\"dt\":3600.0,\"n\":10}},\"values\":[{v},{v},{v},{v},{v},{v},{v},{v},{v},{v}]}}}}],\"component_id\":2}}]}}]}}}}"
    mdl_key="simple"
    p=simple_stm_system.hydro_power_systems[0].power_plants[0]
    read_ta=TimeAxis(0,3600,10)
    p.discharge.result.value = TimeSeries(read_ta, float(22), POINT_AVERAGE_VALUE)
    #  ensure there is event loop for this thread
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)  # note (1)
        client = stm.DStmClient(f"{host}:{port_no}", 1000)
        mdl_key = "simple"
        client.remove_model(mdl_key)
        client.add_model(mdl_key, simple_stm_system)
        # we have to wait a reasonable time for the web-api to start,ref (1), its a thread to!
        # or alternatively the websock connect call would need to spin/delay wait until success/reasonable time
        sleep(0.7)  # ref(1), sufficient wait
        try:
            async def test_wrapper():
                async with websockets.connect(uri(web_api_port)) as websocket:
                    await websocket.send(web_read_sub("rr1", "discharge.result"))
                    assert await get_response_within(websocket, 10) == web_read_rep("rr1","discharge.result",22.0)
                    #change by websocket:
                    await websocket.send(web_write_req("sample write request","discharge.result",100.0))
                    #expected response:
                    write_resp = """{"request_id":"sample write request","result":{"model_key":"simple","hps":[{"hps_id":1,"power_plants":[{"component_id":2,"status":[{"attribute_id":"discharge.result","status":"OK"}]}]}]}}"""
                    assert await get_response_within(websocket, 10) == write_resp
                    #expect new response with changed values, due to sub:
                    assert await get_response_within(websocket, 10) == web_read_rep("rr1","discharge.result",100.0)
                    #change by set_attrs:
                    client.set_attrs([(dstm_url(mdl_key, p.discharge.result), TimeSeries(read_ta, float(99), POINT_AVERAGE_VALUE))])
                    #should notify websocket:
                    assert await get_response_within(websocket, 10) == web_read_rep("rr1","discharge.result",99.0)

                    #request never set attr:
                    await websocket.send(web_read_sub("rr2", "outlet_level"))
                    #expect response:
                    #never set response:
                    read_res_never_set="""{"request_id":"rr2","result":{"model_key":"simple","hps":[{"hps_id":1,"power_plants":[{"component_data":[{"attribute_id":"outlet_level","data":"not found"}],"component_id":2}]}]}}"""
                    assert await get_response_within(websocket, 10) == read_res_never_set
                    #send setattr:
                    client.set_attrs([(dstm_url(mdl_key, p.outlet_level), TimeSeries(read_ta, float(333), POINT_AVERAGE_VALUE))])
                    #we do get a reply from sub:
                    assert await get_response_within(websocket, 10) == web_read_rep("rr2","outlet_level",333.0)
            asyncio.get_event_loop().run_until_complete(test_wrapper())
        finally:
            client.close()
            srv.stop_web_api()
            srv.close()
    del srv

def test_web_api_dstm_ref(simple_stm_system, port_no, web_api_port):
    """
        Test that we can resolve TS that are pure ref to dstm url, also when subscribed
    """
    assert port_no != web_api_port
    mdl_key="simple"
    p=simple_stm_system.hydro_power_systems[0].power_plants[0]
    u = simple_stm_system.hydro_power_systems[0].units[0]
    read_ta=TimeAxis(0,3600,10)
    dstm_url_discharge = dstm_url(mdl_key, p.discharge.result)
    p.discharge.result.value = TimeSeries(read_ta, float(22), POINT_AVERAGE_VALUE)
    #this attr is just a ref to the prev one
    u.discharge.result.value = TimeSeries(dstm_url_discharge)
    #  ensure there is event loop for this thread
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)  # note (1) the listen operation starts in bg, it might not be finished here!
        try:
            client = stm.DStmClient(f"{host}:{port_no}", 1000)
            mdl_key = "simple"
            client.remove_model(mdl_key)
            client.add_model(mdl_key, simple_stm_system)
            client.close()
            del client
            sleep(0.7)  # ref(1), sufficient wait
            async def test_wrapper():
                async with websockets.connect(uri(web_api_port)) as websocket:
                    read_req =  """read_attributes {"request_id": "some req", "model_key": "simple", "time_axis": {"t0": 0, "dt": 3600, "n": 10}, "subscribe": true, "hps": [{"hps_id": 1, "units": [{"component_id": 1, "attribute_ids": ["discharge.result"]}]}]}"""
                    await websocket.send(read_req)
                    assert await get_response_within(websocket, 10) == """{"request_id":"some req","result":{"model_key":"simple","hps":[{"hps_id":1,"units":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":10},"values":[22.0,22.0,22.0,22.0,22.0,22.0,22.0,22.0,22.0,22.0]}}],"component_id":1}]}]}}"""
            asyncio.get_event_loop().run_until_complete(test_wrapper())
        finally:
            srv.stop_web_api()
            srv.close()
    del srv

def test_web_api_multi_model_expressions(simple_stm_system, port_no, web_api_port):
    """
    Test that we can resolve expressions refering to multiple models
    """
    import time
    assert port_no != web_api_port
    mdl1_key="simple"
    mdl2_key="other"
    #  ensure there is event loop for this thread
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)
        time.sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        client = stm.DStmClient(f"{host}:{port_no}", 1000)
        p=simple_stm_system.hydro_power_systems[0].power_plants[0]
        read_ta=TimeAxis(0,3600,10)
        dstm_url_discharge = dstm_url(mdl1_key, p.discharge.result)
        p.discharge.result.value = TimeSeries(read_ta, float(1), POINT_AVERAGE_VALUE)

        client.remove_model(mdl1_key)
        client.add_model(mdl1_key, simple_stm_system)
        #use the same model, and readd with a diff
        u = simple_stm_system.hydro_power_systems[0].units[0]
        u.discharge.result.value = TimeSeries(read_ta, float(2), POINT_AVERAGE_VALUE)
        dstm_url_discharge_mdl2 = dstm_url(mdl2_key, u.discharge.result)

        p.discharge.result.value = TimeSeries(dstm_url_discharge)+TimeSeries(dstm_url_discharge_mdl2)
        client.add_model(mdl2_key, simple_stm_system)

        try:
            async def test_wrapper():
                async with websockets.connect(uri(web_api_port)) as websocket:
                    read_req =  """read_attributes {"request_id": "some req", "model_key": "other", "time_axis": {"t0": 0, "dt": 3600, "n": 10}, "subscribe": true, "hps": [{"hps_id": 1, "power_plants": [{"component_id": 2, "attribute_ids": ["discharge.result"]}]}]}"""
                    await websocket.send(read_req)
                    a = await get_response_within(websocket, 10)
                    assert a == """{"request_id":"some req","result":{"model_key":"other","hps":[{"hps_id":1,"power_plants":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":10},"values":[3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0]}}],"component_id":2}]}]}}"""
            asyncio.get_event_loop().run_until_complete(test_wrapper())
        finally:
            srv.stop_web_api()
            srv.close()
            client.close()
    del srv


def test_stm_task_update_web_api(port_no, web_api_port):
    assert port_no != web_api_port
    from time import sleep
    with TemporaryDirectory() as tmp:
        srv = stm.StmTaskServer(tmp)
        #  to verify that interface can be setup with auth
        token = "Basic dXNlcjpwd2Q="
        srv.add_auth_tokens([token])  # echo -n 'user:pwd' | base64
        print("Starting webapi")
        socket_port = srv.start_server()
        srv.start_web_api("127.0.0.1",
                          web_api_port,
                          doc_root=tmp,
                          fg_threads=1,
                          bg_threads=1)

        sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        # First store a model
        client = stm.StmTaskClient(f"127.0.0.1:{socket_port}", 9999)
        store_req = """store_model {"request_id": "example-store", 
        "model": {"id": 4, "name": "new stm run","labels": ["example", "wiki"]}}"""
        a = get_response(store_req, web_api_port, token)
        mdl = client.read_model(4)

        add_case_req = """add_case {"request_id": "add_case",
        "mid": 4, "case": {"id": 2, "name": "added_case", "created": 0, "json": "", "labels": ["test"], "model_refs": [] } }"""
        a = get_response(add_case_req, web_api_port, token)
        mdl = client.read_model(4)

        update_case_req = """update_case {"request_id": "update_case",
        "mid": 4, "case": {"id": 2, "name": "updated_case", "created": 0, "json": "{'spam': 'egg'}", "labels": ["development"], "model_refs": [] } }"""
        a = get_response(update_case_req, web_api_port, token)
        sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        mdl = client.read_model(4)

        # SOMETHING FISHY with ports/mdl.id combo, try different mids

        mdl_update = client.read_model(4)
        assert len(mdl.cases) == 1
        assert mdl_update.cases[0].id == 2  # Id must be identical when updating case
        assert mdl_update.cases[0].name == "updated_case"  # Verify we have updated
        assert mdl_update.cases[0].json == """{'spam': 'egg'}"""  # Verify we have updated
        srv.stop_web_api()
        client.close()
        del srv

def test_stm_auth():
    with TemporaryDirectory() as tmp:
        s = stm.StmTaskServer(tmp)
        assert not s.auth_needed
        assert len(s.auth_tokens()) == 0
        auth_tokens = ["Basic base64", "Bearer any", "A"]
        s.add_auth_tokens(auth_tokens)
        assert s.auth_needed
        assert len(s.auth_tokens()) == len(auth_tokens)
        assert set(auth_tokens) == set(s.auth_tokens())
        s.remove_auth_tokens(["A", "Bearer any"])
        assert len(s.auth_tokens()) == 1
        assert s.auth_tokens()[0] == "Basic base64"
        assert s.auth_needed
        s.remove_auth_tokens(s.auth_tokens())
        assert not s.auth_needed
