"""dummy c like library to test stub generation"""

foo = "a string"



class Bar():
    """Some class docs"""
    a = 1.5

    def python_add(self, a: int, b: list) -> None:
        """python function docs"""
        return

    def boost_fx(self,*args,**kvargs):
        """
        boost_fx( (Calendar)self) -> float :
            a1
            a2

        boost_fx( (Calendar)arg1, (int)Y [, (int)M=1 [, (int)D=1 ]]) -> object :
            b1
            b2
        """
        pass

    def boost_time(self, *args, **kwargs):
        """
        boost_time( (Bar)self) -> float :
            a1
            a2

        boost_time( (Bar)self, (int)Y [, (int)M=1 [, (int)D=1 ]]) -> object :
            b1
            b2
            b3

        boost_time() -> float :
            c1

        boost_time( (Bar)arg1 [, (derivative_method)method=<shyft.time_series._time_series.derivative_method.DEFAULT object @546684>]) -> TimeSeries :
            d1
            d2
        """
        pass