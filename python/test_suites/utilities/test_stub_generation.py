import io
from shyft.utilities.stub_generation import CStubGenerator, BoostEnumInspect, BoostClassInspect, is_boost_enum, BoostModuleInspect
import test_suites.utilities.dummy_c_like_module as dummy_module

def test_parse_function():
    sg = CStubGenerator()
    signatures, docs = sg.parse_boost_python_function("boost_time", dummy_module.Bar.boost_time)
    expected_sig = [
        "(self) -> float",
        "(self, Y: int, M: int = 1, D: int = 1) -> Any",
        "() -> float",
        "(self, method: derivative_method = shyft.time_series._time_series.derivative_method.DEFAULT) -> TimeSeries"
    ]

    assert signatures == expected_sig
    assert [d.strip("\n") for d in docs] == ["a1\n    a2", "b1\n    b2\n    b3", "c1", "d1\n    d2"]

def test_inspect_boost_Calendar_class():
    from shyft.time_series._time_series import Calendar
    i = BoostClassInspect(Calendar)
    assert i
    cls_props=i.cls_props
    o = io.StringIO()
    i.create_stub(o,0)
    s=o.getvalue()
    assert s

def test_inspect_boost_enum_class():
    from shyft.time_series import point_interpretation_policy
    c = point_interpretation_policy
    assert is_boost_enum(c)
    i = BoostEnumInspect(c)
    assert i.name
    assert i.doc
    assert len(i.enum_members) == 2
    o = io.StringIO()
    i.create_stub(o,1)
    s= o.getvalue()
    assert s


def test_boost_class_inspect_time():
    from shyft.time_series._time_series import time
    i = BoostClassInspect(time)
    assert i.name
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s

def test_boost_class_inspect_time_series():
    from shyft.time_series._time_series import TimeSeries
    i = BoostClassInspect(TimeSeries)
    assert i.name
    m=i.methods

    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s

def test_boost_module_inspect_time_series():
    from shyft.time_series import _time_series
    i = BoostModuleInspect(_time_series)
    assert i.name
    assert i.doc
    assert i.functions
    assert i.classes
    assert i.enums
    assert i.variables
    o = io.StringIO()
    i.create_stub(o, 0)
    s = o.getvalue()
    assert s



