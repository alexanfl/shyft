import numpy as np
import pytest
from shyft.time_series import Calendar, deltahours, TimeAxisFixedDeltaT, TsVector, TimeSeries, point_interpretation_policy, IntVector, statistics_property, TimeAxis


def test_percentiles():
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)
    timeseries = TsVector()

    for i in range(10):
        timeseries.append(
            TimeSeries(ta=ta, fill_value=i, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE))

    wanted_percentiles = IntVector([statistics_property.MIN_EXTREME,
                                    0, 10, 50,
                                    statistics_property.AVERAGE,
                                    70, 100,
                                    statistics_property.MAX_EXTREME])
    ta_day = TimeAxisFixedDeltaT(t0, dt*24, n//24)
    ta_day2 = TimeAxis(t0, dt*24, n//24)
    percentiles = timeseries.percentiles(ta_day, wanted_percentiles)
    percentiles2 = timeseries.percentiles(ta_day2, wanted_percentiles)  # just to verify it works with alt. syntax

    assert len(percentiles2) == len(percentiles)

    for i in range(len(ta_day)):
        assert round(abs(0.0 - percentiles[0].value(i)), 3) == 0, "min-extreme "
        assert round(abs(0.0 - percentiles[1].value(i)), 3) == 0, "  0-percentile"
        assert round(abs(0.9 - percentiles[2].value(i)), 3) == 0, " 10-percentile"
        assert round(abs(4.5 - percentiles[3].value(i)), 3) == 0, " 50-percentile"
        assert round(abs(4.5 - percentiles[4].value(i)), 3) == 0, "   -average"
        assert round(abs(6.3 - percentiles[5].value(i)), 3) == 0, " 70-percentile"
        assert round(abs(9.0 - percentiles[6].value(i)), 3) == 0, "100-percentile"
        assert round(abs(9.0 - percentiles[7].value(i)), 3) == 0, "max-extreme"


def test_percentiles_with_min_max_extremes():
    """ the percentiles function now also supports picking out the min-max peak value
        within each interval.
        Setup test-data so that we have a well known percentile result,
        but also have peak-values within the interval that we can
        verify.
        We let hour ts 0..9 have values 0..9 constant 24*10 days
           then modify ts[1], every day first  value to a peak min value equal to - day_no*1
                              every day second value to a peak max value equal to + day_no*1
                              every day 3rd    value to a nan value
        ts[1] should then have same average value for each day (so same percentile)
                                        but min-max extreme should be equal to +- day_no*1
    """
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    timeseries = TsVector()
    p_fx = point_interpretation_policy.POINT_AVERAGE_VALUE
    for i in range(10):
        timeseries.append(TimeSeries(ta=ta, fill_value=i, point_fx=p_fx))

    ts = timeseries[1]  # pick this one to insert min/max extremes
    for i in range(0, 240, 24):
        ts.set(i + 0, 1.0 - 100*i/24.0)
        ts.set(i + 1, 1.0 + 100*i/24.0)  # notice that when i==0, this gives 1.0
        ts.set(i + 2, float('nan'))  # also put in a nan, just to verify it is ignored during average processing

    wanted_percentiles = IntVector([statistics_property.MIN_EXTREME,
                                    0, 10, 50,
                                    statistics_property.AVERAGE,
                                    70, 100,
                                    statistics_property.MAX_EXTREME])
    ta_day = TimeAxis(t0, dt*24, n//24)
    percentiles = timeseries.percentiles(ta_day, wanted_percentiles)
    for i in range(len(ta_day)):
        if i == 0:  # first timestep, the min/max extremes are picked from 0'th and 9'th ts.
            assert round(abs(0.0 - percentiles[0].value(i)), 3) == 0, "min-extreme "
            assert round(abs(9.0 - percentiles[7].value(i)), 3) == 0, "min-extreme "
        else:
            assert round(abs(1.0 - 100.0*i*24.0/24.0 - percentiles[0].value(i)), 3) == 0, "min-extreme "
            assert round(abs(1.0 + 100.0*i*24.0/24.0 - percentiles[7].value(i)), 3) == 0, "max-extreme"
        assert round(abs(0.0 - percentiles[1].value(i)), 3) == 0, "  0-percentile"
        assert round(abs(0.9 - percentiles[2].value(i)), 3) == 0, " 10-percentile"
        assert round(abs(4.5 - percentiles[3].value(i)), 3) == 0, " 50-percentile"
        assert round(abs(4.5 - percentiles[4].value(i)), 3) == 0, "   -average"
        assert round(abs(6.3 - percentiles[5].value(i)), 3) == 0, " 70-percentile"
        assert round(abs(9.0 - percentiles[6].value(i)), 3) == 0, "100-percentile"


def test_min_max_extreme_1st_leftside_issue():
    """"
    This code adapted from  snippet provided by Fabio Zeiser
    revealed a issue with 1st left-side interval of
    the statistics min-max extreme finder. (E.g. first interval was dt aligned with original ta, causing next source interval value to appear)
    Added here for reference (c++ tests covers the algorithms)
    """
    cal = Calendar()
    t0 = cal.time(2019, 1, 1)
    ts = TimeSeries(TimeAxis(t0, cal.HOUR, 5), [1.0, 2, 3, 4, np.nan], point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsv = TsVector([ts])
    min_ts, max_ts = tsv.percentiles(TimeAxis(t0 + deltahours(1), deltahours(1)*3, 1), [statistics_property.MIN_EXTREME, statistics_property.MAX_EXTREME])

    assert min_ts.value(0) == pytest.approx(2.0)
    assert max_ts.value(0) == pytest.approx(4.0)

    # shift view_range-start by dt/2
    min_ts, max_ts = tsv.percentiles(TimeAxis(t0 + 1800.0, deltahours(1)*3, 1), [statistics_property.MIN_EXTREME, statistics_property.MAX_EXTREME])

    # print(min_ts.values.to_numpy(), max_ts.values.to_numpy())
    # before fix: was [3], [4]
    # wanted result: [2], [4], (after fix),
    # see below
    assert min_ts.value(0) == pytest.approx(2.0)  # because 2.0 is the first point value that occurs within the one large interval, 1.0 is to the left, not part of solution
    assert max_ts.value(0) == pytest.approx(4.0)  # because 3 hours, starting from 0.5, the 4 value is well within the target range.
