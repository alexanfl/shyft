from shyft.time_series import DtsServer


def test_auth():
    """ test setup of auth tokens for the dtss server web-api """
    s = DtsServer()
    assert not s.auth_needed
    assert len(s.auth_tokens()) == 0
    auth_tokens = ["Basic base64", "Bearer any", "A"]
    s.add_auth_tokens(auth_tokens)
    assert s.auth_needed
    assert len(s.auth_tokens()) == len(auth_tokens)
    assert set(auth_tokens) == set(s.auth_tokens())
    s.remove_auth_tokens(["A", "Bearer any"])
    assert len(s.auth_tokens()) == 1
    assert s.auth_tokens()[0] == "Basic base64"
    assert s.auth_needed
    s.remove_auth_tokens(s.auth_tokens())
    assert not s.auth_needed
