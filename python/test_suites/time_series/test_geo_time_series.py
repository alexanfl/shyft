from shyft.time_series import (GeoPoint, GeoPointVector, Calendar, GeoTimeSeries, GeoTimeSeriesVector, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case)
import numpy as np


def test_geo_ts():
    a = GeoTimeSeries()
    assert a
    p0 = GeoPoint(1, 2, 3)
    utc = Calendar()
    ts = TimeSeries(TimeAxis(utc.time(2020, 1, 1), 3600, 10), fill_value=1.0, point_fx=stair_case)
    a.mid_point = p0
    a.ts = ts
    assert a.mid_point == p0
    assert a.ts == ts
    b = GeoTimeSeries()
    b.ts = 2*ts
    assert a != b
    assert a.mid_point != b.mid_point
    assert a.ts != b.ts
    c = GeoTimeSeries(a.mid_point, ts)
    assert a == c


def test_geo_tsv():
    a = GeoTimeSeriesVector()
    assert a is not None
    assert len(a) == 0
    # test, verify we can create ts-vector from list of geo-ts
    utc = Calendar()
    ta = TimeAxis(utc.time(2020, 1, 1), 3600, 10)
    ts = TimeSeries(ta, fill_value=1.0, point_fx=stair_case)
    g0 = GeoTimeSeries(GeoPoint(1, 2, 3), ts)
    g1 = GeoTimeSeries(GeoPoint(3, 4, 5), 2*ts)
    b = GeoTimeSeriesVector([g0, g1])
    assert b
    assert len(b) == 2
    assert b[0] == g0
    assert b[1] == g1
    # test, verify we can create GeoTsVector from numpy-array
    gpv = GeoPointVector([GeoPoint(1, 2, 3), GeoPoint(2, 3, 4)])
    ts_values = np.array([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5.5, 6, 7, 8, 9]], dtype=np.float64)
    c = GeoTimeSeriesVector(ta, gpv, ts_values, stair_case)  # how to create a GeoTsVector from numpy-array
    assert c
    assert len(c) == 2
    assert len(c[0].ts.time_axis) == 10
    # verify that we can get out values_at_time, that is suitable for birds-view analysis/presentation
    v_nan = c.values_at_time(ta.time(0) - 1).to_numpy()  # should be nan
    assert len(v_nan) == 2
    assert all(np.isnan(v_nan))
    v_0 = c.values_at_time(ta.time(0))
    assert np.allclose(v_0, [0.0, 0.0])
    v_5 = c.values_at_time(ta.time(5) + 1800)  # should also fix interpolation etc.
    assert np.allclose(v_5, [5.0, 5.5])
    v_nan_after = c.values_at_time(ta.total_period().end)
    assert all(np.isnan(v_nan_after))
