import numpy as np
from shyft.time_series import Calendar, deltahours, TimeAxis, DoubleVector, TimeSeries, point_interpretation_policy


def test_partition_by():
    """
    verify bw compat of deprecated partition_by:
    - that can
    be used to produce yearly percentiles statistics for long historical
    time-series

    """
    c = Calendar()
    t0 = c.time(1930, 9, 1)
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2016, 9, 1), dt)

    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector.from_numpy(np.arange(len(ta)))  # increasing values

    src_ts = TimeSeries(ta=ta, values=pattern_values,
                        point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    partition_t0 = c.time(2016, 9, 1)
    n_partitions = 80
    partition_interval = Calendar.YEAR
    # get back TsVector,
    # where all TsVector[i].index_of(partition_t0)
    # is equal to the index ix for which the TsVector[i].value(ix) correspond to start value of that particular partition.
    ts_partitions = src_ts.partition_by(c, t0, partition_interval, n_partitions, partition_t0)
    assert len(ts_partitions) == n_partitions
    ty = t0
    for ts in ts_partitions:
        ix = ts.index_of(partition_t0)
        vix = ts.value(ix)
        expected_value = c.diff_units(t0, ty, dt)
        assert vix == expected_value
        ty = c.add(ty, partition_interval, 1)

    # Now finally, try percentiles on the partitions
    wanted_percentiles = [0, 10, 25, -1, 50, 75, 90, 100]
    ta_percentiles = TimeAxis(partition_t0, deltahours(24), 365)
    percentiles = ts_partitions.percentiles(ta_percentiles, wanted_percentiles)
    assert len(percentiles) == len(wanted_percentiles)


def test_ts_stack():
    """
    demo/test ts.stack(cal,t,n,dt,n_partitions,target_t0,0)->TsVector
    be used to produce yearly percentiles statistics for long historical
    time-series

    """
    c = Calendar()
    t0 = c.time(1930, 9, 1)
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2016, 9, 1), dt)

    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector(np.arange(len(ta)).astype(float))  # increasing values

    src_ts = TimeSeries(ta=ta, values=pattern_values,
                        point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    target_t0 = c.time(2016, 9, 1)
    n_dt = 2  # make every stride 2 years
    p_dt = Calendar.YEAR
    n_partitions = 80//n_dt  # fewer intervals
    snap_dt = deltahours(0)
    # get back TsVector,
    # where all TsVector[i].index_of(partition_t0)
    # is equal to the index ix for which the TsVector[i].value(ix) correspond to start value of that particular partition.
    ts_partitions = src_ts.stack(c, t0, n_dt, p_dt, n_partitions, target_t0, snap_dt)
    assert len(ts_partitions) == n_partitions
    ty = t0
    for ts in ts_partitions:
        ix = ts.index_of(target_t0)
        vix = ts.value(ix)
        expected_value = c.diff_units(t0, ty, dt)
        assert vix == expected_value  # since our pattern is hour increasing
        ty = c.add(ty, p_dt, n_dt)

    # Now finally, try percentiles on the partitions
    wanted_percentiles = [0, 10, 25, -1, 50, 75, 90, 100]
    ta_percentiles = TimeAxis(target_t0, deltahours(24), 365)
    percentiles = ts_partitions.percentiles(ta_percentiles, wanted_percentiles)
    assert len(percentiles) == len(wanted_percentiles)


def test_ts_stack_week_aligned():
    """
    demo/test ts.stack(cal,t0,n,dt,n_partitions,target_t0,week)->TsVector
    so that every partition in the stack are adjusted to starts on a tuesday,
    simply by specifying the snap_dt to a week.
    The offset from week start of target_t0 is also added,
    so that the transformed value corresponds to the closest
    week, and then tuesday
    """
    c = Calendar()
    t0 = c.time(1930, 9, 1)  # happen to be a a monday
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2020, 9, 1), dt)  # is tuesday
    snap_dt = Calendar.WEEK  # we want every partion to align with nearest week, on a tuesday
    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector(np.arange(len(ta)).astype(float))  # just make sz
    for i in range(len(ta)):  # fill in, so that we have yyyyww.day_no
        wu = c.calendar_week_units(ta.time(i))
        pattern_values[i] = wu.iso_year*100.0 + wu.iso_week*1.0 + wu.week_day/10.0  # skip hour + wu.hour/100.0;
    src_ts = TimeSeries(ta=ta, values=pattern_values,
                        point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    target_t0 = c.time(2020, 9, 1)  # a tuesday!
    n_dt = 2  # make every stride 2 years
    p_dt = Calendar.YEAR
    n_partitions = 80//n_dt  # fewer intervals
    ts_partitions = src_ts.stack(c, t0, n_dt, p_dt, n_partitions, target_t0, snap_dt)
    assert len(ts_partitions) == n_partitions
    for ts in ts_partitions:  # verify all partitions start on a tuesday
        v_t0 = ts(target_t0)
        week_day = int((v_t0 - int(v_t0))*10)  # ref encoding above
        assert week_day == 2
