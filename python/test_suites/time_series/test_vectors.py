from builtins import range

from shyft.time_series import (Int64Vector, IntVector, DoubleVector, UtcTimeVector, time, StringVector)
from shyft.hydrology import (GeoCellData, GeoCellDataVector, LandTypeFractions, GeoPoint, GeoPointVector)
import numpy as np
from numpy.testing import assert_array_almost_equal

"""
Some basic test to ensure that the exposure of c++ vector<T> is working as expected
"""


def test_double_vector():
    dv_from_list = DoubleVector([x for x in range(10)])
    dv_np = np.arange(10.0)
    dv_from_np = DoubleVector.from_numpy(dv_np)
    assert len(dv_from_list) == 10
    assert_array_almost_equal(dv_from_list.to_numpy(), dv_np)
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)
    dv_from_np[5] = 8
    dv_from_np.append(11)
    dv_from_np.append(12)
    dv_np[5] = 8
    dv_np = np.concatenate((dv_np, [11, 12]))
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)
    # this does not work yet
    # nv= DoubleVector(dv_np).. would be very nice!


def test_int_vector():
    dv_from_list = IntVector([x for x in range(10)])
    dv_np = np.arange(10, dtype=np.int32)  # notice, default is int64, which does not convert automatically to int32
    dv_from_np = IntVector.from_numpy(dv_np)
    assert len(dv_from_list) == 10
    assert_array_almost_equal(dv_from_list.to_numpy(), dv_np)
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)
    dv_from_np[5] = 8
    dv_from_np.append(11)
    dv_from_np.append(12)
    dv_np[5] = 8
    dv_np = np.concatenate((dv_np, [11, 12]))
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)


def test_int64_vector():
    dv_from_list = Int64Vector([x for x in range(10)])
    dv_np = np.arange(10, dtype=np.int32)  # notice, default is int64, which does not convert automatically to int32
    dv_from_np = Int64Vector.from_numpy(dv_np)
    assert len(dv_from_list) == 10
    assert_array_almost_equal(dv_from_list.to_numpy(), dv_np)
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)
    dv_from_np[5] = 8
    dv_from_np.append(11)
    dv_from_np.append(12)
    dv_np[5] = 8
    dv_np = np.concatenate((dv_np, [11, 12]))
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)


def test_utctime_vector():
    dv_from_list = UtcTimeVector([x for x in range(10)])
    dv_np = np.arange(10, dtype=np.int64)
    dv_from_np = UtcTimeVector.from_numpy(dv_np)
    assert len(dv_from_list) == 10
    assert_array_almost_equal(dv_from_list.to_numpy(), dv_np)
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)
    dv_from_np[5] = time(8)  # it should also have accepted any number here
    dv_from_np[5] = 8.5  # 8.5 seconds
    assert round(abs(dv_from_np[5].seconds - 8.5), 7) == 0  # verify it stores microseconds
    dv_from_np.append(time(11))
    dv_from_np.append(12)  # this one takes any that could go as seconds
    dv_from_np.append(time(13))  # this one takes any that could go as seconds
    dv_np[5] = 8.5  # python/numpy silently ignore float -> int64
    dv_np = np.concatenate((dv_np, [11, 12, 13]))
    assert_array_almost_equal(dv_from_np.to_numpy(), dv_np)
    dv2 = dv_from_np.to_numpy_double()
    assert round(abs(dv2[5] - 8.5), 7) == 0  # verify that to_numpy_double preserves microsecond


def test_string_vector():
    # NOTE: support for string vector is very limited, e.g. numpy does not work, only lists
    #     but for now this is sufficient in Shyft
    s_list = ['abc', 'def']
    dv_from_list = StringVector(s_list)
    assert len(dv_from_list) == 2
    for i in range(len(dv_from_list)):
        assert s_list[i] == dv_from_list[i]


def test_geopoint_vector():
    gpv = GeoPointVector()
    for i in range(5):
        gpv.append(GeoPoint(i, i*2, i*3))
    assert len(gpv) == 5
    for i in range(5):
        assert gpv[i].x == i
        assert gpv[i].y == i*2
        assert gpv[i].z == i*3


def test_geo_cell_data_vector():
    gcdv = GeoCellDataVector()
    for i in range(5):
        p = GeoPoint(100, 200, 300)
        ltf = LandTypeFractions()
        ltf.set_fractions(glacier=0.1, lake=0.1, reservoir=0.1, forest=0.1)
        gcd = GeoCellData(p, 1000000.0, i, 0.9, ltf)
        gcdv.append(gcd)
    assert len(gcdv) == 5
    for i in range(5):
        assert gcdv[i].catchment_id() == i
    g2 = GeoCellDataVector(gcdv)  # copy construct a new
    assert g2 == gcdv
    g2[0].set_catchment_id(10)
    assert g2 != gcdv
    # serialize
    gcdv_s = gcdv.serialize()
    assert len(gcdv_s) > 2
    gcdv_deserialized = GeoCellDataVector.deserialize(gcdv_s)
    assert gcdv_deserialized is not None
    assert gcdv_deserialized == gcdv
