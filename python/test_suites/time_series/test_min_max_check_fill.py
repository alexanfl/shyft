import math

from shyft.time_series import TimeAxis, TimeSeries, DoubleVector, QacParameter, time, POINT_INSTANT_VALUE, UtcTimeVector, POINT_AVERAGE_VALUE


def test_min_max_check_linear_fill():
    ta = TimeAxis(0, 1, 5)
    ts_src = TimeSeries(ta, values=DoubleVector([1.0, -1.0, 2.0, float('nan'), 4.0]), point_fx=POINT_INSTANT_VALUE)
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=-10.0, dt_max=300)
    assert round(abs(ts_qac.value(3) - 3.0), 7) == 0
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=0.0, dt_max=300)
    assert round(abs(ts_qac.value(1) - 1.5), 7) == 0  # -1 out, replaced with linear between
    assert round(abs(ts_qac.value(3) - 3.0), 7) == 0
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=0.0, dt_max=0)
    assert not math.isfinite(ts_qac.value(3))  # should give nan, not allowed to fill in
    assert not math.isfinite(ts_qac.value(1))  # should give nan, not allowed to fill in


def test_min_max_check_linear_fill_w_max_gap():
    """ there is a gap larger than allowed, so we would like to see nan there """
    ta = TimeAxis(UtcTimeVector([0, 1, 3, 6, 7]), 8.0)  # mind the gap 1..3..6, should give nan
    ts_src = TimeSeries(ta, values=DoubleVector([0.0, 1.0, 3.0, 6.0, 7.0]), point_fx=POINT_INSTANT_VALUE)
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=-10.0, dt_max=1)
    ts_v = ts_qac.values.to_numpy()
    assert len(ts_qac) == 5 + 2
    assert round(abs(ts_qac.value(0) - 0.0), 7) == 0
    assert round(abs(ts_qac.value(1) - 1.0), 7) == 0
    assert not math.isfinite(ts_qac(ts_qac.time(2)))
    assert math.isfinite(ts_qac(ts_qac.time(3))), 'changed behaviour'
    assert not math.isfinite(ts_qac(ts_qac.time(4)))
    assert not math.isfinite(ts_qac.value(2))
    assert math.isfinite(ts_qac.value(3)), 'changed behavior'
    assert not math.isfinite(ts_qac.value(4))

    assert round(abs(ts_qac.value(5) - 6.0), 7) == 0
    assert round(abs(ts_qac.value(6) - 7.0), 7) == 0

    assert round(abs(ts_v[0] - 0.0), 7) == 0
    assert round(abs(ts_v[1] - 1.0), 7) == 0
    assert not math.isfinite(ts_v[2])
    assert math.isfinite(ts_v[3]), 'changed and and now consistent..'
    assert not math.isfinite(ts_v[4])
    assert round(abs(ts_v[5] - 6.0), 7) == 0
    assert round(abs(ts_v[6] - 7.0), 7) == 0


def test_min_max_check_ts_fill():
    ta = TimeAxis(0, 1, 5)
    ts_src = TimeSeries(ta, values=DoubleVector([1.0, -1.0, 2.0, float('nan'), 4.0]), point_fx=POINT_AVERAGE_VALUE)
    cts = TimeSeries(ta, values=DoubleVector([1.0, 1.8, 2.0, 2.0, 4.0]), point_fx=POINT_AVERAGE_VALUE)
    ts_qac = ts_src.min_max_check_ts_fill(v_max=10.0, v_min=-10.0, dt_max=300, cts=cts)
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    ts_qac = ts_src.min_max_check_ts_fill(v_max=10.0, v_min=0.0, dt_max=300, cts=cts)
    assert round(abs(ts_qac.value(1) - 1.8), 7) == 0  # -1 out, replaced with linear between
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    # ref dtss test for serialization testing


def test_qac_parameter():
    q = QacParameter()
    assert q is not None
    ta = TimeAxis(0, 1, 5)
    ts_src = TimeSeries(ta, values=DoubleVector([1.0, -1.0, 2.0, float('nan'), 4.0]), point_fx=POINT_AVERAGE_VALUE)
    cts = TimeSeries(ta, values=DoubleVector([1.0, 1.8, 2.0, 2.0, 4.0]), point_fx=POINT_AVERAGE_VALUE)

    ts_qac = ts_src.quality_and_ts_correction(QacParameter(time(300), -10.0, 10.0, time(0), 0.0), cts=cts)
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    ts_qac = ts_src.quality_and_ts_correction(QacParameter(time(300), 0.0, 10.0, time(0), 0.0), cts=cts)
    assert round(abs(ts_qac.value(1) - 1.8), 7) == 0  # -1 out, replaced with linear between
    assert round(abs(ts_qac.value(3) - 2.0), 7) == 0
    ts_qac = ts_src.quality_and_self_correction(QacParameter(time(0), float('nan'), float('nan'), time(0), 0.0, constant_filler=0.0))
    assert ts_qac


def test_min_max_check_linear_fill_w_max_gap_2():
    """ there is a gap larger than allowed, so we would like to see nan there, after dt-step.. """
    ta = TimeAxis(UtcTimeVector([0, 1, 3, 6, 7]), 8.0)  # mind the gap 1..3..6, should give nan afer 1 s
    ts_src = TimeSeries(ta, values=DoubleVector([0.0, 1.0, 3.0, 6.0, 7.0]), point_fx=POINT_AVERAGE_VALUE)
    ts_qac = ts_src.min_max_check_linear_fill(v_max=10.0, v_min=-10.0, dt_max=1)
    ts_v = ts_qac.values.to_numpy()
    assert len(ts_qac) == 5 + 2
    assert ts_qac.time(2) == time(2)
    assert ts_qac.time(3) == time(3)
    assert ts_qac.time(4) == time(4)
    assert ts_qac.time(5) == time(6)
    assert ts_qac.time(6) == time(7)
    assert round(abs(ts_qac.value(0) - 0.0), 7) == 0

    assert not math.isfinite(ts_qac(ts_qac.time(2)))
    assert math.isfinite(ts_qac(ts_qac.time(3)))
    assert not math.isfinite(ts_qac(ts_qac.time(4)))
    assert not math.isfinite(ts_qac.value(2))
    assert math.isfinite(ts_qac.value(3))
    assert not math.isfinite(ts_qac.value(4))
    assert round(abs(ts_qac.value(3) - 3.0), 7) == 0
    assert round(abs(ts_qac.value(5) - 6.0), 7) == 0
    assert round(abs(ts_qac.value(6) - 7.0), 7) == 0

    assert round(abs(ts_v[0] - 0.0), 7) == 0
    assert round(abs(ts_v[1] - 1.0), 7) == 0
    assert not math.isfinite(ts_v[2])
    assert math.isfinite(ts_v[3])
    assert not math.isfinite(ts_v[4])
    assert round(abs(ts_v[3] - 3.0), 7) == 0
    assert round(abs(ts_v[5] - 6.0), 7) == 0
    assert round(abs(ts_v[6] - 7.0), 7) == 0
