import numpy as np
from shyft.time_series import utctime_now, deltahours, TimeAxis, TimeSeries, point_interpretation_policy, max, min, log


def test_basic_timeseries_math_operations():
    """
    Test that timeseries functionality is exposed, and briefly verify correctness
    of operators (the  shyft core do the rest of the test job, not repeated here).
    """
    t0 = utctime_now()
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)

    a = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    assert a  # should evaluate to true
    b = TimeSeries(ta=ta, fill_value=1.0, point_fx=point_interpretation_policy.POINT_INSTANT_VALUE)
    b.fill(2.0)  # demo how to fill a point ts
    assert round(abs((1.0 - b).values.to_numpy().max() - -1.0), 7) == 0
    assert round(abs((b - 1.0).values.to_numpy().max() - 1.0), 7) == 0
    c = a + b*3.0 - a/2.0  # operator + * - /
    d = -a  # unary minus
    e = a.average(ta)  # average
    f = max(c, 300.0)
    g = min(c, -300.0)
    # h = a.max(c, 300) # class static method not supported
    h = c.max(300.0)
    k = c.min(-300)

    log_func = log(c)
    log_ts = c.log()

    assert a.size() == n
    assert b.size() == n
    assert c.size() == n
    assert log_func.size() == n
    assert log_ts.size() == n
    assert round(abs(c.value(0) - (3.0 + 2.0*3.0 - 3.0/2.0)), 7) == 0  # 7.5
    for i in range(n):
        assert abs(c.value(i) - (a.value(i) + b.value(i)*3.0 - a.value(i)/2.0)) < 0.0001
        assert abs(d.value(i) - (- a.value(i))) < 0.0001
        assert abs(e.value(i) - a.value(i)) < 0.00001
        assert abs(f.value(i) - 300.0) < 0.00001
        assert abs(h.value(i) - 300.0) < 0.00001
        assert abs(g.value(i) - (-300.0)) < 0.00001
        assert abs(k.value(i) - (-300.0)) < 0.00001
        assert abs(log_func.value(i) - np.log(c.value(i))) < 1e-5
        assert abs(log_ts.value(i) - np.log(c.value(i))) < 1e-5
    # now some more detailed tests for setting values
    b.set(0, 3.0)
    assert round(abs(b.value(0) - 3.0), 7) == 0
    #  3.0 + 3 * 3 - 3.0/2.0
    assert abs(c.value(1) - 7.5) < 0.0001  # 3 + 3*3  - 1.5 = 10.5
    assert abs(c.value(0) - 10.5) < 0.0001  # 3 + 3*3  - 1.5 = 10.5


def test_empty_ts():
    a = TimeSeries()
    assert a.size() == 0
    assert a.values.size() == 0
    assert len(a.values.to_numpy()) == 0
    assert not a.total_period().valid()
    assert not a  # evaluate to false
    try:
        a.time_axis
        assert False, "Expected exception"
    except RuntimeError as re:
        pass
    try:
        avg = a.average(TimeAxis())
        assert False, "Expected exception"
    except RuntimeError as re:
        pass

def test_ts_bool():
    a = TimeSeries()
    assert not a  # ok empty
    try:
        b = TimeSeries("something")
        x = bool(b)
        assert False, "Expected exception here"
    except RuntimeError as re:
        pass


def test_ts_equal():
    a = TimeSeries('a')
    b = TimeSeries('b')
    try:
        e = 1 if a == b else 2
        assert False, "Expect exception here"
    except RuntimeError as re:
        pass
    assert a == a, 'equal by ref, even if unbound'
    n = TimeSeries()
    assert n == n, 'equal by reference, even if null'
    nn = TimeSeries()
    assert n == nn, 'both null is equal'
    assert n != a, 'compare to null, different'