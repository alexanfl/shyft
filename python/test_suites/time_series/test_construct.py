from shyft.time_series import TimeAxis, TimeSeries, POINT_AVERAGE_VALUE, DoubleVector, UtcTimeVector
import numpy as np
import sys


def test_time_series_constructor() -> None:
    """
    How to create time-series,
    note that since we experienced a problem
    with python ref.count on ta (issue 624)
    we add some extra checks here to ensure
    its ok.
    """
    ta = TimeAxis(0, 60, 60)
    e1 = TimeSeries()  # empty ts
    assert e1 is not None
    e2 = TimeSeries('a')  # symbolic unbound ts
    assert e2 is not None
    e3 = TimeSeries('a', TimeSeries(ta, 15.0, POINT_AVERAGE_VALUE))  # symbolic bound ts
    assert e3
    # time-series can be constructed with a fill value
    n = sys.getrefcount(ta)
    e4 = TimeSeries(ta, 15., POINT_AVERAGE_VALUE)
    assert e4
    assert n == sys.getrefcount(ta)
    # time-series can be constructed with a double vector
    double_vec = DoubleVector([0.]*len(ta))
    e5 = TimeSeries(ta, double_vec, POINT_AVERAGE_VALUE)
    assert e5
    assert n == sys.getrefcount(ta)

    # time-series can be constructed with a python list with numbers
    number_list = [0.]*len(ta)
    e6 = TimeSeries(ta, number_list, POINT_AVERAGE_VALUE)
    assert e6
    assert n == sys.getrefcount(ta)

    npa = np.linspace(0, 1, len(ta))
    e7 = TimeSeries(ta, npa, POINT_AVERAGE_VALUE)
    assert e7
    assert n == sys.getrefcount(ta)
    # pattern ts
    e7 = TimeSeries(DoubleVector([1.0, 2.0, 3.0, 4.0]), 6, ta.time(0), ta)
    assert e7
    assert n == sys.getrefcount(ta)

    e8 = TimeSeries(e7)
    assert e8
    assert n == sys.getrefcount(ta)

    e9 = TimeSeries([1.0, 2.0, 3.0, 4.0], 6, ta)
    assert e9
    assert n == sys.getrefcount(ta)
    # more ct to go:
    e9 = TimeSeries(ta.fixed_dt, npa, POINT_AVERAGE_VALUE)
    assert n == sys.getrefcount(ta)
    tpts = ta.time_points
    tpts[0] = tpts[0] - 3
    tax = TimeAxis(UtcTimeVector(tpts))
    nx = sys.getrefcount(tax)
    npax = np.linspace(0, 1, len(tax))
    e9 = TimeSeries(tax.point_dt, npax, POINT_AVERAGE_VALUE)
    assert nx == sys.getrefcount(tax)
    pass  # put breakpoint here, debugger will take a ref on ta
