import numpy as np

from shyft.time_series import Calendar
from shyft.time_series import UtcPeriod
from shyft.time_series import TsVector
from shyft.time_series import TimeSeries
from shyft.time_series import TimeAxis
from shyft.time_series import point_interpretation_policy as ts_point_fx
from shyft.time_series import deltahours
from shyft.time_series import DoubleVector as dv
from shyft.time_series import DtsServer, DtsClient, StringVector
from shyft.time_series import ts_stringify, time


def _create_forecasts(t0: time, dt: time, n: int, fc_dt: time, fc_n: int) -> TsVector:
    tsv = TsVector()
    stair_case = ts_point_fx.POINT_AVERAGE_VALUE
    for i in range(fc_n):
        ta = TimeAxis(t0 + i*fc_dt, dt, n)
        mrk = (i + 1)/100.0
        v = dv(np.linspace(1 + mrk, 1 + n + mrk, n, endpoint=False))
        tsv.append(TimeSeries(ta, v, stair_case))
    return tsv


def test_merge_arome_forecast():
    utc = Calendar()
    t0 = utc.time(2017, 1, 1)
    dt = deltahours(1)
    n = 66  # typical arome
    fc_dt_n_hours = 6
    fc_dt = deltahours(fc_dt_n_hours)
    fc_n = 4*10  # 4 each day 10 days
    fc_v = _create_forecasts(t0, dt, n, fc_dt, fc_n)

    m0_6 = fc_v.forecast_merge(deltahours(0), fc_dt)
    m1_7 = fc_v.forecast_merge(deltahours(1), fc_dt)
    assert m0_6 is not None
    assert m1_7 is not None
    # check returned sizes
    assert m0_6.size() == (fc_n - 1)*fc_dt_n_hours + n
    assert m1_7.size() == (fc_n - 1)*fc_dt_n_hours - 1 + n
    # some few values (it's fully covered in c++ tests
    assert round(abs(m0_6.value(0) - 1.01), 7) == 0
    assert round(abs(m0_6.value(6) - 1.02), 7) == 0
    assert round(abs(m1_7.value(0) - 2.01), 7) == 0
    assert round(abs(m1_7.value(6) - 2.02), 7) == 0

    def read_callback(ts_ids: StringVector, read_period: UtcPeriod) -> TsVector:
        return fc_v

    s = DtsServer()
    s.cb = read_callback
    port = s.start_server()

    c = DtsClient(f"localhost:{port}")
    fc = TsVector([TimeSeries(f"x://{str(s)}") for s in range(len(fc_v))])
    tsv = TsVector()
    fc_merge_expr = fc.forecast_merge(deltahours(0), fc_dt)
    tsv.append(fc_merge_expr)
    period = UtcPeriod(utc.time(2017), utc.time(2018))
    rtsv = c.evaluate(tsv, period)
    assert (len(rtsv) == len(tsv))


def test_stringify_fc_merge():
    fc = TsVector([TimeSeries("test://x"), TimeSeries("test://y")])
    fc_merge_expr = fc.forecast_merge(deltahours(0), deltahours(6))
    s = ts_stringify(fc_merge_expr)
    assert (s == "nary_op([test://x, test://y], op=MERGE, fc_interval=21600.000000, lead_time=0.000000)")
