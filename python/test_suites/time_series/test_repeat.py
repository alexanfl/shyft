from shyft.time_series import TimeSeries, TimeAxis, Calendar, POINT_AVERAGE_VALUE, DoubleVector, TsVector
import numpy as np


def test_yearly_repeat():
    utc = Calendar()
    a = TimeSeries(TimeAxis(utc.time(2000, 1, 1), Calendar.DAY, 366), DoubleVector.from_numpy(np.linspace(1, 366, 366)), POINT_AVERAGE_VALUE)
    yearly = TimeAxis(utc, utc.time(1931, 1, 1), Calendar.YEAR, 100)  # notice that we pass in calendar to get it calendric repeat
    r = a.repeat(yearly)
    assert yearly.total_period() == r.total_period()
    for i in range(len(r.time_axis)):
        doy = utc.day_of_year(r.time(i))
        assert round(abs(float(doy) - r.value(i)), 7) == 0

    tsv = TsVector([a, 2.0*a])
    rtsv = tsv.repeat(yearly)  # supported on expressions and  vector op as well

    for j in range(len(rtsv)):
        r = rtsv[j]
        for i in range(len(r.time_axis)):
            doy = utc.day_of_year(r.time(i))
            assert round(abs(float(doy)*(j + 1) - r.value(i)), 7) == 0


def test_yearly_repeat_unaligned():
    """ To illustrate challenge as presented in issue Ref https://gitlab.com/shyft-os/shyft/-/issues/1065 """
    utc = Calendar()
    t0 = utc.time(1999, 9, 1)  # say we have values 1..366 from t0
    a = TimeSeries(TimeAxis(t0, Calendar.DAY, 366), DoubleVector.from_numpy(np.linspace(1, 366, 366)), POINT_AVERAGE_VALUE)
    yearly = TimeAxis(utc, utc.time(1931, 9, 1), Calendar.YEAR, 100)  # notice that we pass in calendar to get it calendric repeat
    r = a.repeat(yearly).evaluate()
    # verify that each t0 year, we have 1,2...365 (or 366 in leap years)
    for y_period in yearly:  #
        n_days = utc.diff_units(y_period.start, y_period.end, Calendar.DAY)
        i0 = r.index_of(y_period.start)  # figure out where we start index-wise in this year
        for j in range(n_days):  # 0..365
            assert r.value(i0 + j) == float(j + 1)
